<?php

class accommodation extends db
{
    private $template;
    private $actions;
    private $global;
    private $data;
    private $apps;
    private $uri;

    private $lang_code;
    private $lang_def;
    private $lang_id;

    private $start_price;
    private $schedules;
    private $packages;
    private $rate;

	function __construct( $action, $apps, $uri = array() )
	{
    	parent::__construct();

    	$this->template = new Template( THEME_DIR );
    	$this->global   = new globalFunctions();
		$this->actions  = $action;
		$this->apps     = $apps;
		$this->uri      = $uri;

		$this->init();
		$this->set_meta_desc();
		$this->set_meta_title();
		$this->init_shortcodes();
	}

	function init_shortcodes()
	{
		add_shortcode( 'tabby', 'tab_start' );
		add_shortcode( 'tabbyending', 'tab_ending' );
	}
	
	function load()
	{
		if( isset( $_POST[ 'search' ] ) )
	    {
	        $param = base64_encode( json_encode( array_diff_key( $_POST, array_flip( array( 'search' ) ) ) ) );

	        if( $this->lang_id == $this->lang_def )
			{
		    	header( 'location: ' . sprintf( '%s%s/accommodation/search-result/%s/', HT_SERVER, SITE_URL, $param ) );
	        }
	        else
	        {
		    	header( 'location: ' . sprintf( '%s%s/%s/accommodation/search-result/%s/', HT_SERVER, SITE_URL, $this->lang_code, $param ) );
	        }

	        exit;
	    }

		if( $this->uri[ 0 ] == 'search-result' )
		{
			return $this->search_result();
		}
		else if( empty( $this->uri[ 0 ] ) )
		{
			return $this->archive();
		}
		else
		{
			return $this->detail();
		}
	}

	function archive()
	{
		$this->template->set_file( 'main', 'partials/archive-accommodation.html' );

		$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'site_url', SITE_URL );

		$this->actions->add_actions( 'jvs', $this->init_archive_js() );

		return $this->template->Parse( 'mlBlock', 'mainBlock', false );
	}

	function search_result()
	{
		if( isset( $this->uri[ 1 ] ) && !empty( $this->uri[ 1 ] ) )
		{
			$this->template->set_file( 'main', 'partials/search.html' );

			$this->template->set_block( 'main', 'resultLoopBlock', 'rslBlock' );
			$this->template->set_block( 'main', 'resultBlock', 'rsBlock' );

			$this->template->set_block( 'main', 'noresultBlock', 'nrsBlock' );
			$this->template->set_block( 'main', 'searchBlock', 'scBlock' );
			$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

			$prm  = json_decode( base64_decode( $this->uri[ 1 ] ), true );
			$mapi = $this->global->get_setting_value( 'google_map_api_key' );

			if( $prm !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$w = array();

				if( isset( $prm[ 'searchval' ] ) && !empty( $prm[ 'searchval' ] ) )
				{
					$w[] = parent::prepare_query( '( a.ldescription LIKE %s OR a.ltitle LIKE %s )', '%' . $prm[ 'searchval' ] . '%', '%' . $prm[ 'searchval' ] . '%' );
				}

				if( isset( $prm[ 'dest' ] ) && !empty( $prm[ 'dest' ] ) )
				{
					$w[] = parent::prepare_query( 'FIND_IN_SET( %d, ( SELECT GROUP_CONCAT( a2.ladditional_value ) FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key IN ( %s, %s, %s, %s ) ) )', $prm[ 'dest' ], 'accommodation_continent', 'accommodation_country', 'accommodation_state', 'accommodation_city' );
				}

				if( isset( $prm[ 'term' ] ) && !empty( $prm[ 'term' ] ) )
				{
					$w[] = parent::prepare_query( 'FIND_IN_SET( %d, ( SELECT GROUP_CONCAT( a2.lterm_id ) FROM lumonata_post_relationship AS a2 WHERE a2.lpost_id = a.lpost_id ) )', $prm[ 'term' ] );
				}

				if( empty( $w ) )
				{
					$where = '';
				}
				else
				{
					$where = ' AND ' . implode( ' AND ', $w );
				}

				//-- GET language
				$lang_id = $this->global->get_current_language_id();

				$q = 'SELECT * FROM lumonata_post AS a WHERE a.ltype = "accommodation" AND a.lstatus = 1 AND a.llang_id = ' . $lang_id . $where . ' ORDER BY a.ltitle';
				$r = parent::query( $q );

				if( !is_array( $r ) )
				{
					$n = parent::num_rows( $r );

					if( $n > 0 )
					{
						$c = '';
						$i = 0;

						while( $d = parent::fetch_assoc( $r ) )
						{
							//-- GET additional field
							$sa = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lapp_id = %d AND a.lmodule_id = ( SELECT a2.lmodule_id FROM lumonata_module AS a2 WHERE a2.lapps = %s )';
					        $qa = parent::prepare_query( $sa, $d[ 'lpost_id' ], $d[ 'ltype' ] );
					        $ra = parent::query( $qa );

					        if( parent::num_rows( $ra ) > 0 )
					        {
					            while( $da = parent::fetch_assoc( $ra ) )
					            {
					            	$d[ $da[ 'ladditional_key' ] ] = $da[ 'ladditional_value' ];
					            }
					        }

					        //-- GET term field
							$st = 'SELECT b.* FROM lumonata_post_relationship AS a LEFT JOIN lumonata_post_terms AS b ON a.lterm_id = b.lterm_id WHERE a.lpost_id = %d AND b.lgroup = %s ORDER BY b.lname';
					        $qt = parent::prepare_query( $st, $d[ 'lpost_id' ], $d[ 'ltype' ] );
					        $rt = parent::query( $qt );

					        if( parent::num_rows( $rt ) > 0 )
					        {
					            while( $dt = parent::fetch_assoc( $rt ) )
					            {
					            	//-- GET term additional field
									$sf = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lterm_id = %d';
							        $qf = parent::prepare_query( $sf, $dt[ 'lterm_id' ] );
							        $rf = parent::query( $qf );

							        if( parent::num_rows( $rf ) > 0 )
							        {
							            while( $df = parent::fetch_assoc( $rf ) )
							            {
							            	$dt[ $df[ 'ladditional_key' ] ] = $df[ 'ladditional_value' ];
							            }
							        }

					            	$d[ $dt[ 'lrule' ] ][] = $dt;
					            }
					        }

					        //-- GET map coordinate
					        if( $i == 0 && !empty( $d[ 'map_coordinate' ] ) )
					        {
					        	$c = $d[ 'map_coordinate' ];

					        	$i++;
					        }

					        //-- GET surf trip
					        $stype = array();

					        if( !empty( $d[ 'surf_trip' ] ) )
					        {
					        	foreach( $d[ 'surf_trip' ] as $ds )
					        	{
					        		$stype[] = $ds[ 'lname' ];
					        	}
					        }

			        		//-- GET certified
					        if( isset( $d[ 'is_certified' ] ) && $d[ 'is_certified' ] == 1 )
					        {
					    		$this->template->set_var( 'certified', 1 );
					    		$this->template->set_var( 'certified_css', '' );
					        }
					        else
					        {
					    		$this->template->set_var( 'certified', 2 );
					    		$this->template->set_var( 'certified_css', 'hidden' );
					        }

					        //-- GET accommodation rating
							$rating = $this->global->get_accommodation_rating( $d[ 'lpost_id' ], $d[ 'lref_id' ] );

							if( empty( $rating ) )
							{
								$this->template->set_var( 'review_label', $this->global->translate( 'no-reviews', 'NO REVIEWS', 3 ) );
								$this->template->set_var( 'review_count', 0 );
								$this->template->set_var( 'review_star', 0 );
							}
							else
							{
								$this->template->set_var( 'review_label', $this->global->get_accommodation_rating_label( $rating ) );
								$this->template->set_var( 'review_count', $rating[ 'reviews' ] );
								$this->template->set_var( 'review_star', $rating[ 'rating' ] );
							}

							$start_price = $this->global->get_start_price( $d );
	    
					    	$this->template->set_var( 'mcoordinate', $d[ 'map_coordinate' ] );
					        $this->template->set_var( 'surf_type', empty( $stype ) ? 'z' : implode( ', ', $stype ) );

					       	$this->template->set_var( 'location', $this->global->get_locations( $d ) );
					        $this->template->set_var( 'title', $this->global->get_listing_title( $d ) );
					       	$this->template->set_var( 'hightlight', $this->global->get_hightlight( $d ) );
							$this->template->set_var( 'permalink', $this->global->permalink( $d, array( $this->uri[ 1 ] ) ) );

					        $this->template->set_var( 'start_price', $start_price );
					        $this->template->set_var( 'start_price_format', $this->global->get_format_price( $start_price, $d[ 'lpost_id' ] ) );

					        $this->template->set_var( 'img_thumb', $this->global->get_attachment_url( $d[ 'accommodation_image' ], 198, 145 ) );
							$this->template->set_var( 'img_placeholder', $this->global->get_attachment_url( $d[ 'accommodation_image' ], 10, 10 ) );

							$this->template->Parse( 'rslBlock', 'resultLoopBlock', true );
						}
					    
					    $this->template->set_var( 'coordinate', $c );
						
						$this->template->Parse( 'rsBlock', 'resultBlock', false );
					}
					else
					{
						$this->template->set_var( 'str_1', $this->global->translate( 'accommodation-text-5', 'No accommodation was found in your seach term, please try another terms' ) );

						$this->template->Parse( 'nrsBlock', 'noresultBlock', false );
					}
				}
				else
				{
					$this->template->set_var( 'str_1', $this->global->translate( 'accommodation-text-5', 'No accommodation was found in your seach term, please try another terms' ) );

					$this->template->Parse( 'nrsBlock', 'noresultBlock', false );
				}
			}

			$himage = $this->global->get_setting_value( 'search_header_image' );
			$htitle = $this->global->get_setting_value( 'search_result_title' );

			
			$this->template->set_var( 'title_1', $this->global->translate( 'accommodations-on-search', 'ACCOMODATIONS ON SEARCH', 3 ) );
			$this->template->set_var( 'title_2', $this->global->translate( 'accommodations-list', 'ACCOMODATIONS LIST', 3 ) );
			$this->template->set_var( 'title_3', $this->global->translate( 'accommodations', 'ACCOMODATIONS', 3 ) );

			$this->template->set_var( 'str_1', $this->global->translate( 'max-persons', sprintf( 'max %s persons', 5 ), 2, array( 5 ) ) );
			$this->template->set_var( 'str_2', $this->global->translate( 'type-of-surf-trip', 'TYPE OF SURF TRIPS', 3 ) );
			$this->template->set_var( 'str_3', $this->global->translate( 'change-dates', 'CHANGE DATES', 3 ) );
			$this->template->set_var( 'str_4', $this->global->translate( 'certified', 'CERTIFIED', 3 ) );
			$this->template->set_var( 'str_5', $this->global->translate( 'sort-by', 'Sort By', 1 ) );
			$this->template->set_var( 'str_6', $this->global->translate( 'check-out', 'Check-Out' ) );
			$this->template->set_var( 'str_7', $this->global->translate( 'check-in', 'Check-In' ) );
			$this->template->set_var( 'str_8', $this->global->translate( 'price', 'PRICE', 3 ) );
			$this->template->set_var( 'str_9', $this->global->translate( 'from', 'FROM', 3 ) );

			$this->template->set_var( 'heading_title', $htitle );
			$this->template->set_var( 'header_image', $this->global->get_attachment_url( $himage, 1920, null ) );
			$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $himage, 10, 10 ) );

			$this->template->set_var( 'search_form', $this->global->init_search_form( $prm ) );
			$this->template->set_var( 'checkin', empty( $prm[ 'checkin' ] ) ? '-' : date( 'l F jS Y', strtotime( $prm[ 'checkin' ] ) ) );
			$this->template->set_var( 'checkout', empty( $prm[ 'checkout' ] ) ? '-' : date( 'l F jS Y', strtotime( $prm[ 'checkout' ] ) ) );

			$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/moment@2.29.3/moment.min.js' ) );

			$this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.css' ) );
	        $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.js' ) );

	        $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css' ) );
			$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js' ) );

			$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/tinysort@3.2.7/dist/tinysort.min.js' ) );
			$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery.nicescroll@3.7.6/jquery.nicescroll.min.js' ) );
			$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://maps.googleapis.com/maps/api/js?key=' . $mapi . '&callback=init_map_advance', array( 'async', 'defer' ) ) );

			$this->actions->add_actions( 'jvs', $this->init_search_js() );

			return $this->template->Parse( 'mlBlock', 'mainBlock', false );
		}
		else
		{
			$this->template->set_file( 'main', 'partials/search.html' );
			
			$this->template->set_block( 'main', 'searchBlock', 'scBlock' );
			$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

			$himage = $this->global->get_setting_value( 'search_header_image' );
			$htitle = $this->global->get_setting_value( 'search_title' );

			$this->template->set_var( 'heading_title', $htitle );
			$this->template->set_var( 'header_image', $this->global->get_attachment_url( $himage, 1920, null ) );
			$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $himage, 10, 10 ) );

			$this->template->set_var( 'search_form', $this->global->init_search_form() );
			$this->template->set_var( 'checkout', '-' );
			$this->template->set_var( 'checkin', '-' );

			$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/moment@2.29.3/moment.min.js' ) );

			$this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.css' ) );
	        $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.js' ) );

	        $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css' ) );
			$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js' ) );

			$this->actions->add_actions( 'jvs', $this->init_search_js() );

			return $this->template->Parse( 'scBlock', 'searchBlock', false );
		}
	}

	function detail()
	{
		$this->init();

		//-- Set Template File
		$this->template->set_file( 'main', 'partials/single-accommodation.html' );

		//-- Gallery Section
		$this->init_section_one();

		//-- Skill & Highlight Section
		$this->init_section_two();

		//-- Schedule & Rooms Section
		$this->init_section_three();

		//-- What's Included Section
		$this->init_section_four();

		//-- Foods Section
		$this->init_section_five();

		//-- Surf Info Section
		$this->init_section_six();

		//-- Staf Section
		$this->init_section_seven();

		//-- Activities Section
		$this->init_section_eight();

		//-- Location Section
		$this->init_section_nine();

		//-- Reviews Section
		$this->init_section_ten();

		//-- Partner Section
		$this->init_section_eleven();

		//-- Terms & Condition Section
		$this->init_section_twelve();

		//-- Other Accommodation Section
		$this->init_section_thirteen();

		$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

		$adult_value  = 0;
		$child_value  = 0;
		$infant_value = 0;

		$guest_value  = 0;
		$guest_label  = $this->global->translate( 'guest', 'GUEST', 3 );

		$checkin_value  = '';
		$checkin_label  = $this->global->translate( 'check-in', 'CHECK-IN', 3 );

		$checkout_value = '';
		$checkout_label = $this->global->translate( 'check-out', 'CHECK-OUT', 3 );

		if( isset( $this->uri[ 1 ] ) && !empty( $this->uri[ 1 ] ) )
		{
			$prm = json_decode( base64_decode( $this->uri[ 1 ] ), true );

			if( $prm !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$adult_value    = $prm[ 'adult' ];
				$child_value    = $prm[ 'child' ];
				$infant_value   = $prm[ 'infant' ];
				$checkin_value  = $prm[ 'checkin' ];
				$checkout_value = $prm[ 'checkout' ];
				$guest_value    = $adult_value + $child_value + $infant_value;
				$guest_label    = $guest_value . ' ' . $this->global->translate( 'guest', 'GUEST', 3 );

				if( !empty( $checkin_value  ) )
				{
					$checkin_label  = date( 'd M Y', strtotime( $checkin_value ) );
				}

				if( !empty( $checkout_value  ) )
				{
					$checkout_label = date( 'd M Y', strtotime( $checkout_value ) );
				}
			}
		}

		$this->template->set_var( 'adult_value', $adult_value );
		$this->template->set_var( 'child_value', $child_value );
		$this->template->set_var( 'infant_value', $infant_value );

		$this->template->set_var( 'guest_value', $guest_value );
		$this->template->set_var( 'guest_label', $guest_label );

		$this->template->set_var( 'checkin_value', $checkin_value );
		$this->template->set_var( 'checkin_label', $checkin_label );

		$this->template->set_var( 'checkout_value', $checkout_value );
		$this->template->set_var( 'checkout_label', $checkout_label );

		$this->template->set_var( 'post_id', $this->data[ 'lpost_id' ] );
		$this->template->set_var( 'min_night', $this->data[ 'min_night' ] );
		$this->template->set_var( 'post_action', $this->global->site_url( 'booking/add-ons' ) );
		$this->template->set_var( 'post_title', $this->global->get_listing_title( $this->data ) );
		$this->template->set_var( 'post_content', do_shortcode( $this->data[ 'ldescription' ] ) );

		if( $this->data[ 'page_template' ] == '3' )
		{
			if( $this->data[ 'price_type' ] == '0' )
			{
				$this->template->set_var( 'start_price_label', $this->global->translate( 'price-per-night', 'price per night', 2 ) );
			}
			else
			{
				$this->template->set_var( 'start_price_label', $this->global->translate( 'price-per-package', 'price per package', 2 ) );
			}
		}
		else
		{
			if( $this->data[ 'price_type' ] == '0' )
			{
				$this->template->set_var( 'start_price_label', $this->global->translate( 'price-per-night-from', 'price per night from', 2 ) );
			}
			else
			{
				$this->template->set_var( 'start_price_label', $this->global->translate( 'price-per-package-from', 'price per package from', 2 ) );
			}
		}
		
		$this->template->set_var( 'start_price', $this->start_price );
		$this->template->set_var( 'start_price_format', $this->global->get_format_price( $this->start_price, $this->data[ 'lpost_id' ] ) );

		$this->template->set_var( 'locations', $this->locations() );
		$this->template->set_var( 'breadcrumbs', $this->breadcrumbs() );

		$this->template->set_var( 'email_booking', $this->global->get_setting_value( 'email_booking' ) );
		$this->template->set_var( 'wa_global', $this->global->get_format_phone_number( $this->global->get_setting_value( 'wa_global' ), 'wa' ) );

		$this->template->set_var( 'str_1', $this->global->translate( 'adults', 'Adults', 1 ) );
		$this->template->set_var( 'str_2', $this->global->translate( 'children', 'Children', 1 ) );
		$this->template->set_var( 'str_3', $this->global->translate( 'babies', 'Babies', 1 ) );
		$this->template->set_var( 'str_4', $this->global->translate( 'price-per-night', 'price per night', 2 ) );
		$this->template->set_var( 'str_5', $this->global->translate( 'contact-title', 'Contact us about this property' ) );

		$this->template->set_var( 'str_6', $this->global->translate( 'request-booking', 'REQUEST BOOKING', 3 ) );
		$this->template->set_var( 'str_8', $this->global->translate( 'see-availability', 'SEE AVAILABILITY', 3 ) );
		$this->template->set_var( 'str_7', $this->global->translate( 'contact-for-more-dates', 'CONTACT FOR MORE DATES', 3 ) );
		$this->template->set_var( 'str_9', $this->global->translate( 'inquiry-private-charter', 'INQUIRY PRIVATE CHARTER', 3 ) );

		$this->template->set_var( 'str_10', $this->global->translate( 'popup-title-2', 'Contact us for more dates', 1 ) );
		$this->template->set_var( 'str_11', $this->global->translate( 'popup-title', 'Book Private Charter', 1 ) );
		$this->template->set_var( 'str_12', $this->global->translate( 'first-name', 'First Name', 1 ) );
		$this->template->set_var( 'str_13', $this->global->translate( 'last-name', 'Last Name', 1 ) );
		$this->template->set_var( 'str_14', $this->global->translate( 'email-address', 'Email Address', 1 ) );
		$this->template->set_var( 'str_15', $this->global->translate( 'phone-number', 'Phone Number', 1 ) );
		$this->template->set_var( 'str_16', $this->global->translate( 'date-from', 'Date From', 1 ) );
		$this->template->set_var( 'str_17', $this->global->translate( 'date-to', 'Date To', 1 ) );
		$this->template->set_var( 'str_18', $this->global->translate( 'message', 'Message', 1 ) );
		$this->template->set_var( 'str_19', $this->global->translate( 'submit', 'Submit', 1 ) );
		$this->template->set_var( 'str_20', $this->global->translate( 'waiting', 'Waiting', 1 ) );
		$this->template->set_var( 'str_21', $this->global->translate( 'message-text', 'Your request has been submitted' ) );
		$this->template->set_var( 'str_22', $this->global->translate( 'accommodations', 'ACCOMODATIONS', 3 ) );

		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'site_url', SITE_URL );
		
		$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/moment@2.29.3/moment.min.js' ) );

		$this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.css' ) );
        $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.js' ) );

        $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css' ) );
        $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.js' ) );

        $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css' ) );
		$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js' ) );

		$this->actions->add_actions( 'css', $this->global->get_css( HT_SERVER . THEME_URL . '/assets/css/tabby.css' ) );
		$this->actions->add_actions( 'jvs', $this->global->get_jvs( HT_SERVER . THEME_URL . '/assets/js/tabby.js' ) );
		
		$this->actions->add_actions( 'jvs', $this->init_detail_js() );

		return $this->template->Parse( 'mlBlock', 'mainBlock', false );
	}

	function init_section_one()
	{
		$this->template->set_block( 'main', 'galleryThumbLoopBlock', 'gtlBlock' );
		$this->template->set_block( 'main', 'galleryLoopBlock', 'glBlock' );
		$this->template->set_block( 'main', 'achivementBlock', 'acBlock' );
		$this->template->set_block( 'main', 'galleryBlock', 'gBlock' );

		if( isset( $this->data[ 'accommodation_gallery' ] ) && !empty( $this->data[ 'accommodation_gallery' ] ) )
		{
			$dt = json_decode( $this->data[ 'accommodation_gallery' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$num = 0;

				foreach( $dt as $d )
				{
					$img = $this->global->get_field_value( 'lumonata_attachment', 'lattach', array( 'lattach_id' => $d ) );
        
			        if( !empty( $img ) && file_exists( IMAGE_DIR . '/Uploads/' . $img ) )
			        {
						$this->template->set_var( 'img_id', $d ); 

						$this->template->set_var( 'img_thumb', $this->global->get_attachment_url( $d, 150, 150 ) );
						$this->template->set_var( 'img_preview', $this->global->get_attachment_url( $d, null, 750 ) );
						$this->template->set_var( 'img_placeholder', $this->global->get_attachment_url( $d, 10, 10 ) );

						$this->template->Parse( 'glBlock', 'galleryLoopBlock', true );
						$this->template->Parse( 'gtlBlock', 'galleryThumbLoopBlock', true );

						$num++;
					}
				}

				if( isset( $this->rate[ 'rating' ] ) && $this->rate[ 'rating' ] > 0 )
				{
					$this->template->set_var( 'achievement_rate', $this->rate[ 'rating' ] );
					$this->template->set_var( 'achievement_label', $this->global->get_accommodation_rating_label( $this->rate ) );
					$this->template->set_var( 'achievement_title', $this->global->translate( 'accommodation-2', 'Accommodation' ) );
					$this->template->set_var( 'achievement_bg', $this->global->get_attachment_url( $this->data[ 'achievement_bg_image' ], 540, 180 ) );
					$this->template->set_var( 'achievement_placeholder', $this->global->get_attachment_url( $this->data[ 'achievement_bg_image' ], 10, 10 ) );

					$this->template->Parse( 'acBlock', 'achivementBlock', false );
				}

				$this->template->Parse( 'gBlock', 'galleryBlock', false );
			}
		}
	}

	function init_section_two()
	{			
		if( isset( $this->data[ 'page_template' ] ) && $this->data[ 'page_template' ] == '0' )
		{
			$this->template->set_block( 'main', 'skillLoopBlock', 'sklBlock' );
			$this->template->set_block( 'main', 'skillBlock', 'skBlock' );

			if( isset( $this->data[ 'skill_level' ] ) && !empty( $this->data[ 'skill_level' ] ) )
			{
				$dt = json_decode( $this->data[ 'skill_level' ], true );
				$ls = array( 
					1 => $this->global->translate( 'beginner', 'Beginner', 1 ), 
					2 => $this->global->translate( 'intermediate', 'Intermediate', 1 ), 
					3 => $this->global->translate( 'expert', 'Expert', 1 )
				);

				if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
				{
					foreach( $dt as $d )
					{
						$this->template->set_var( 'skill', $ls[ $d ] );

						$this->template->Parse( 'sklBlock', 'skillLoopBlock', true );
					}

					$this->template->set_var( 'skill_title', $this->global->translate( 'skill-levels', 'Skill Levels' ) );

					$this->template->Parse( 'skBlock', 'skillBlock', false );
				}
			}
		}
		else
		{
			$this->template->set_var( 'skill_css', 'hidden' );
		}

		$this->template->set_block( 'main', 'hightlightLoopBlock', 'hlBlock' );
		$this->template->set_block( 'main', 'hightlightBlock', 'hBlock' );

		if( isset( $this->data[ 'hightlight' ] ) && !empty( $this->data[ 'hightlight' ] ) )
		{
			$dt = json_decode( $this->data[ 'hightlight' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$i = 0;

				foreach( $dt as $d )
				{
					if( count( array_filter( $d ) ) == 0 )
					{
						continue;
					}

					$i++;

					$this->template->set_var( 'hightlight', $d[ 'name' ] );

					$this->template->Parse( 'hlBlock', 'hightlightLoopBlock', true );
				}

				if( $i > 0 )
				{
					$this->template->set_var( 'highlight_title', $this->global->translate( 'highlights', 'Highlights', 3 ) );

					$this->template->Parse( 'hBlock', 'hightlightBlock', false );
				}
			}
		}

		$this->template->set_block( 'main', 'accommodationtextBlock', 'atBlock' );

		if( isset( $this->data[ 'accommodation_text' ] ) && !empty( $this->data[ 'accommodation_text' ] ) )
		{
			$this->template->set_var( 'accommodation_text', do_shortcode( $this->data[ 'accommodation_text' ] ) );

			$this->template->Parse( 'atBlock', 'accommodationtextBlock', false );
		}
	}

	function init_section_three()
	{
		//-- SET Packages Block
		$this->template->set_block( 'main', 'packageAnchorBlock', 'pcABlock' );
		$this->template->set_block( 'main', 'packageLoopBlock', 'pclBlock' );
		$this->template->set_block( 'main', 'packageBlock', 'pcBlock' );

		//-- SET Schedules Block
		$this->template->set_block( 'main', 'scheduleAnchorBlock', 'scABlock' );
		$this->template->set_block( 'main', 'scheduleFilterBlock', 'scFBlock' );
		$this->template->set_block( 'main', 'scheduleLoopBlock', 'sclBlock' );
		$this->template->set_block( 'main', 'scheduleBlock', 'scBlock' );

		//-- SET Rooms/Cabins Block
		$this->template->set_block( 'main', 'roomsAnchorBlock', 'rmABlock' );
		$this->template->set_block( 'main', 'roomsLoopBlock', 'rmlBlock' );
		$this->template->set_block( 'main', 'roomsBlock', 'rmBlock' );

		//-- SET Sidebar Block
		$this->template->set_block( 'main', 'requestBookingBlock', 'rQBlock' );
		$this->template->set_block( 'main', 'roomsBookingBlock', 'rBBlock' );

		//-- SET Sidebar Class
		$this->template->set_var( 'sticky_class', 'osmall-container hidden' );

		//-- SET Packages Content
		if( $this->data[ 'page_template' ] == '1' )
		{
			if( isset( $this->schedules ) && !empty( $this->schedules ) )
			{
				//-- GET period filter
				$s = 'SELECT 
						DATE_FORMAT( a.lcheck_in, %s ) AS period
						FROM lumonata_schedules AS a 
						WHERE a.lpost_id = %d AND MONTH( FROM_UNIXTIME( a.lcheck_in ) ) >= MONTH( CURDATE( ) )
						GROUP BY YEAR( FROM_UNIXTIME( a.lcheck_in ) ) DESC, MONTH( FROM_UNIXTIME( a.lcheck_in ) ) ASC';
				$q = parent::prepare_query( $s, '%M, %Y', $this->data[ 'lpost_id' ] );
				$r = parent::query( $q );

				if( !is_array( $r ) )
				{
					while( $d = parent::fetch_array( $r ) )
					{
						$this->template->set_var( 'period', $d[ 'period' ] );

						$this->template->Parse( 'scFBlock', 'scheduleFilterBlock', true );
					}
				}

				$num = count( $this->schedules );

				foreach( $this->schedules as $d )
				{
					$schedule_price = $this->global->get_schedule_start_price( $d );

					if( $schedule_price <= 0 || $this->global->is_expired_schedule( $d ) )
					{
						$num--;

						continue;
					}

					$this->template->set_var( 'duration', $d[ 'lduration' ] );
					$this->template->set_var( 'departure', $d[ 'ldeparture' ] );
					$this->template->set_var( 'schedule_id', $d[ 'lschedule_id' ] );
					$this->template->set_var( 'check_in_date', date( 'd/m/Y', strtotime( $d[ 'lcheck_in' ] ) ) );
					$this->template->set_var( 'check_out_date', date( 'd/m/Y', strtotime( $d[ 'lcheck_out' ] ) ) );

					$this->template->set_var( 'trip_action', $this->table_action( $d, $this->data ) );
					$this->template->set_var( 'itinerary', $this->trip_itinerary( $d[ 'litinerary' ] ) );
					$this->template->set_var( 'price', $this->global->get_format_price( $schedule_price, $this->data[ 'lpost_id' ] ) );

					$this->template->set_var( 'str_1', $this->global->translate( 'adults', 'Adults', 1 ) );
					$this->template->set_var( 'str_3', $this->global->translate( 'babies', 'Babies', 1 ) );
					$this->template->set_var( 'str_2', $this->global->translate( 'children', 'Children', 1 ) );

					$this->template->Parse( 'sclBlock', 'scheduleLoopBlock', true );
				}

				if( $num > 0 )
				{
					$this->template->set_var( 'schedule_heading', $this->global->translate( 'accommodation-text-14', sprintf( 'Available Dates at %s', $this->data[ 'ltitle' ] ), 0, array( $this->data[ 'ltitle' ] ) ) );
					$this->template->set_var( 'search_label', $this->global->translate( 'search-by-month', 'SEARCH BY MONTH', 3 ) );
					$this->template->set_var( 'sticky_class', 'osmall-container' );

					$this->template->set_var( 'tbl_txt_4', $this->global->translate( 'price-from', 'PRICE FROM', 3 ) );
					$this->template->set_var( 'tbl_txt_2', $this->global->translate( 'check-out', 'CHECK OUT', 3 ) );
					$this->template->set_var( 'tbl_txt_8', $this->global->translate( 'itinerary', 'ITINERARY', 3 ) );
					$this->template->set_var( 'tbl_txt_1', $this->global->translate( 'check-in', 'CHECK IN', 3 ) );
					$this->template->set_var( 'tbl_txt_3', $this->global->translate( 'duration', 'DURATION', 3 ) );
					$this->template->set_var( 'tbl_txt_7', $this->global->translate( 'depature', 'DEPATURE', 3 ) );
					$this->template->set_var( 'tbl_txt_6', $this->global->translate( 'guest', 'GUEST', 3 ) );
					$this->template->set_var( 'tbl_txt_9', $this->global->translate( 'cabin', 'CABIN', 3 ) );
					$this->template->set_var( 'tbl_txt_5', $this->global->translate( 'room', 'ROOM', 3 ) );

					$this->template->Parse( 'scABlock', 'scheduleAnchorBlock', false );
					$this->template->Parse( 'scBlock', 'scheduleBlock', false );
				}
		
				$this->template->Parse( 'rQBlock', 'requestBookingBlock', false );
			}
		}
		else
		{
			if( isset( $this->packages ) && !empty( $this->packages ) )
			{
				$num = count( $this->packages );

				foreach( $this->packages as $d )
				{
					$package_price = $this->global->get_package_start_price( $d );

					if( $package_price <= 0 || $this->global->is_expired_package( $d ) )
					{
						$num--;

						continue;
					}
					
					$this->template->set_var( 'package_name', $d[ 'lname' ] );
					$this->template->set_var( 'package_night', $d[ 'lnight' ] );
					$this->template->set_var( 'package_id', $d[ 'lpackage_id' ] );
					$this->template->set_var( 'package_desc', $d[ 'ldescription' ] );
					$this->template->set_var( 'package_duration', $this->calculate_duration( $d ) );
					$this->template->set_var( 'package_action', $this->table_action( $d, $this->data, true ) );
					$this->template->set_var( 'package_price', $this->global->get_format_price( $package_price, $this->data[ 'lpost_id' ] ) );

					$this->template->set_var( 'str_1', $this->global->translate( 'adults', 'Adults', 1 ) );
					$this->template->set_var( 'str_3', $this->global->translate( 'babies', 'Babies', 1 ) );
					$this->template->set_var( 'str_2', $this->global->translate( 'children', 'Children', 1 ) );

					$this->template->Parse( 'pclBlock', 'packageLoopBlock', true );
				}

				if( $num > 0 )
				{
					$this->template->set_var( 'package_heading', $this->global->translate( 'accommodation-text-11', sprintf( 'Available Packages at %s', $this->data[ 'ltitle' ] ), 0, array( $this->data[ 'ltitle' ] ) ) );
					$this->template->set_var( 'sticky_class', 'osmall-container' );

					$this->template->set_var( 'tbl_txt_4', $this->global->translate( 'price-from', 'PRICE FROM', 3 ) );
					$this->template->set_var( 'tbl_txt_2', $this->global->translate( 'check-out', 'CHECK OUT', 3 ) );
					$this->template->set_var( 'tbl_txt_8', $this->global->translate( 'itinerary', 'ITINERARY', 3 ) );
					$this->template->set_var( 'tbl_txt_1', $this->global->translate( 'check-in', 'CHECK IN', 3 ) );
					$this->template->set_var( 'tbl_txt_3', $this->global->translate( 'duration', 'DURATION', 3 ) );
					$this->template->set_var( 'tbl_txt_7', $this->global->translate( 'depature', 'DEPATURE', 3 ) );
					$this->template->set_var( 'tbl_txt_6', $this->global->translate( 'guest', 'GUEST', 3 ) );
					$this->template->set_var( 'tbl_txt_9', $this->global->translate( 'cabin', 'CABIN', 3 ) );
					$this->template->set_var( 'tbl_txt_5', $this->global->translate( 'room', 'ROOM', 3 ) );

					$this->template->set_var( 'tbl_txt_10', $this->global->translate( 'package-name', 'PACKAGE NAME', 3 ) );
					$this->template->set_var( 'tbl_txt_11', $this->global->translate( 'choose-date', 'Choose Date', 1 ) );

					$this->template->set_var( 'package_title', $this->global->translate( 'packages', 'PACKAGES', 3 ) );
					$this->template->set_var( 'request_action', '#section-package' );

					$this->template->Parse( 'pcABlock', 'packageAnchorBlock', false );
					$this->template->Parse( 'pcBlock', 'packageBlock', false );
				}
		
				$this->template->Parse( 'rQBlock', 'requestBookingBlock', false );
			}
		}

		//-- SET Rooms/Cabins Content
		if( $this->data[ 'page_template' ] == '3' )
		{
			$this->template->set_var( 'sticky_class', 'osmall-container osmall-container-other' );
			$this->template->set_var( 'request_action', '#accommodation-form' );					
			$this->template->set_var( 'add_to_cart_label', $this->global->translate( 'book-now', 'BOOK NOW', 3 ) );
			
			$this->template->Parse( 'rBBlock', 'roomsBookingBlock', false );
		}
		else
		{
			if( isset( $this->data[ 'room_type' ] ) && !empty( $this->data[ 'room_type' ] ) )
			{
				$num = count( $this->data[ 'room_type' ] );

				foreach( $this->data[ 'room_type' ] as $d )
				{
					$room_price = $this->global->room_price( $this->data, $d );

					if( $room_price <= 0 )
					{
						$num--;

						continue;
					}

					if( isset( $d[ 'room_baby_bed' ] ) === false )
					{
						$d[ 'room_baby_bed' ] = 0;
					}

					if( isset( $d[ 'room_max_adult' ] ) === false )
					{
						$d[ 'room_max_adult' ] = 0;
					}

					if( isset( $d[ 'room_max_child' ] ) === false )
					{
						$d[ 'room_max_child' ] = 0;
					}

					if( isset( $d[ 'facilities_repeater' ] ) === false )
					{
						$d[ 'facilities_repeater' ] = null;
					}

					if( isset( $d[ 'room_type_image' ] ) === false )
					{
						$d[ 'room_type_image' ] = null;
					}

					if( isset( $d[ 'room_type_gallery' ] ) === false )
					{
						$d[ 'room_type_gallery' ] = null;
					}

					if( isset( $d[ 'room_min_night' ] ) === false )
					{
						$d[ 'room_min_night' ] = 2;
					}

					$this->template->set_var( 'room_name', $d[ 'lname' ] );
					$this->template->set_var( 'room_id', $d[ 'lterm_id' ] );
					$this->template->set_var( 'room_min_night', $d[ 'room_min_night' ] );

					$this->template->set_var( 'room_gallery', $this->room_gallery( $d ) );
					$this->template->set_var( 'room_baby_bed', $this->room_baby_bed( $d[ 'room_baby_bed' ] ) );
					$this->template->set_var( 'room_max_adult', $this->room_max_adult( $d[ 'room_max_adult' ] ) );
					$this->template->set_var( 'room_max_child', $this->room_max_child( $d[ 'room_max_child' ] ) );
					$this->template->set_var( 'room_facilities', $this->room_facilities( $d[ 'facilities_repeater' ], $this->data ) );

					$this->template->set_var( 'room_price', $room_price );
					$this->template->set_var( 'room_format_price', $this->global->get_format_price( $room_price, $this->data[ 'lpost_id' ] ) );

					$this->template->set_var( 'room_image', $this->global->get_attachment_url( $d[ 'room_type_image' ], 600, null ) );
					$this->template->set_var( 'room_image_placeholder', $this->global->get_attachment_url( $d[ 'room_type_image' ], 10, 10 ) );
					
					$this->template->set_var( 'per_night', $this->global->translate( 'per-night', 'Per night' ) );
					$this->template->set_var( 'per_person', $this->global->translate( 'per-person', 'Per person' ) );

					$this->template->Parse( 'rmlBlock', 'roomsLoopBlock', true );	
				}

				if( $this->data[ 'page_template' ] == '1' )
				{
					$this->template->set_var( 'select_title', $this->global->translate( 'select-this-cabin', 'Select this cabin' ) );
					$this->template->set_var( 'room_heading', $this->global->translate( 'accommodation-text-10', sprintf( 'Available Cabins In %s', $this->data[ 'ltitle' ] ), 1, array( $this->data[ 'ltitle' ] ) ) );
				}
				else
				{
					$this->template->set_var( 'select_title', $this->global->translate( 'select-this-room', 'Select this room' ) );
					$this->template->set_var( 'room_heading', $this->global->translate( 'accommodation-text-9', sprintf( 'Available Rooms In %s', $this->data[ 'ltitle' ] ), 1, array( $this->data[ 'ltitle' ] ) ) );
				}
				
				$this->template->set_var( 'add_to_cart_label', $this->global->translate( 'book-now', 'BOOK NOW', 3 ) );					

				if( $num > 0 )
				{
					if( $this->data[ 'page_template' ] == '1' )
					{
						$this->template->set_var( 'rooms_title', $this->global->translate( 'cabins', 'CABINS', 3 ) );
					}
					else
					{
						$this->template->set_var( 'rooms_title', $this->global->translate( 'rooms', 'ROOMS', 3 ) );
					}

					$this->template->set_var( 'request_action', '#section-room' );
					$this->template->set_var( 'sticky_class', 'osmall-container osmall-container-room' );

					$this->template->Parse( 'rmABlock', 'roomsAnchorBlock', false );
					$this->template->Parse( 'rBBlock', 'roomsBookingBlock', false );
					$this->template->Parse( 'rmBlock', 'roomsBlock', false );
				}
				else
				{
					$this->template->set_var( 'request_action', '#accommodation-form' );
					
					$this->template->Parse( 'rBBlock', 'roomsBookingBlock', false );
				}
			}
		}
	}

	function init_section_four()
	{
		$this->template->set_block( 'main', 'whatsincludedAnchorBlock', 'wiABlock' );
		$this->template->set_block( 'main', 'whatsincludedLoopBlock', 'wilBlock' );
		$this->template->set_block( 'main', 'whatsincludedBlock', 'wiBlock' );

		$empty = true;

		if( isset( $this->data[ 'whats_included_stay' ] ) && !empty( $this->data[ 'whats_included_stay' ] ) )
		{
			$empty = false;

			$dt = json_decode( $this->data[ 'whats_included_stay' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$items = '';

				foreach( $dt as $d )
				{
					$items .= sprintf( '<li>%s</li>', $this->global->get_field_value( 'lumonata_post_terms', 'lname', array( 'lterm_id' => $d ) ) );
				}

				$this->template->set_var( 'whats_included_title', $this->global->translate( 'stay', 'Stay', 1 ) );
				$this->template->set_var( 'whats_included_item', $items );

				$this->template->Parse( 'wilBlock', 'whatsincludedLoopBlock', true );
			}
		}

		if( isset( $this->data[ 'whats_included_transport' ] ) && !empty( $this->data[ 'whats_included_transport' ] ) )
		{
			$empty = false;

			$dt = json_decode( $this->data[ 'whats_included_transport' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$items = '';

				foreach( $dt as $d )
				{
					$items .= sprintf( '<li>%s</li>', $this->global->get_field_value( 'lumonata_post_terms', 'lname', array( 'lterm_id' => $d ) ) );
				}

				$this->template->set_var( 'whats_included_title', $this->global->translate( 'transport', 'Transport', 1 ) );
				$this->template->set_var( 'whats_included_item', $items );

				$this->template->Parse( 'wilBlock', 'whatsincludedLoopBlock', true );
			}
		}

		if( isset( $this->data[ 'whats_included_activities' ] ) && !empty( $this->data[ 'whats_included_activities' ] ) )
		{
			$empty = false;

			$dt = json_decode( $this->data[ 'whats_included_activities' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$items = '';

				foreach( $dt as $d )
				{
					$items .= sprintf( '<li>%s</li>', $this->global->get_field_value( 'lumonata_post_terms', 'lname', array( 'lterm_id' => $d ) ) );
				}

				$this->template->set_var( 'whats_included_title', $this->global->translate( 'activities', 'Activities', 1 ) );
				$this->template->set_var( 'whats_included_item', $items );

				$this->template->Parse( 'wilBlock', 'whatsincludedLoopBlock', true );
			}
		}

		if( $empty === false )
		{
			$this->template->set_var( 'whats_include_title', $this->global->translate( 'what-s-included', 'WHAT\'S INCLUDED', 3 ) );

			$this->template->Parse( 'wiABlock', 'whatsincludedAnchorBlock', false );
			$this->template->Parse( 'wiBlock', 'whatsincludedBlock', false );
		}
	}

	function init_section_five()
	{
		$this->template->set_block( 'main', 'foodmealsAnchorBlock', 'fmABlock' );
		$this->template->set_block( 'main', 'foodmealsLoopBlock', 'fmlBlock' );
		$this->template->set_block( 'main', 'foodmealsBlock', 'fmBlock' );

		$empty = true;

		if( isset( $this->data[ 'food_meals' ] ) && !empty( $this->data[ 'food_meals' ] ) )
		{
			$empty = false;

			$dt = json_decode( $this->data[ 'food_meals' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$items = '';

				foreach( $dt as $d )
				{
					$items .= sprintf( '<li>%s</li>', $this->global->get_field_value( 'lumonata_post_terms', 'lname', array( 'lterm_id' => $d ) ) );
				}

				$this->template->set_var( 'food_title', $this->global->translate( 'accommodation-text-12', 'The following meals are included' ) );
				$this->template->set_var( 'food_item', $items );

				$this->template->Parse( 'fmlBlock', 'foodmealsLoopBlock', true );
			}
		}

		if( isset( $this->data[ 'food_dietary' ] ) && !empty( $this->data[ 'food_dietary' ] ) )
		{
			$empty = false;

			$dt = json_decode( $this->data[ 'food_dietary' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$items = '';

				foreach( $dt as $d )
				{
					$items .= sprintf( '<li>%s</li>', $this->global->get_field_value( 'lumonata_post_terms', 'lname', array( 'lterm_id' => $d ) ) );
				}

				$this->template->set_var( 'food_title', $this->global->translate( 'accommodation-text-13', 'The following dietary requirement(s)' ) );
				$this->template->set_var( 'food_item', $items );

				$this->template->Parse( 'fmlBlock', 'foodmealsLoopBlock', true );
			}
		}

		if( isset( $this->data[ 'food' ] ) && !empty( $this->data[ 'food' ] ) )
		{
			$this->template->set_var( 'food', $this->data[ 'food' ] );
		}

		if( $empty === false )
		{
			$this->template->set_var( 'foods_title', $this->global->translate( 'food', 'FOOD', 3 ) );
			$this->template->set_var( 'foods_title_2', $this->global->translate( 'accommodation-text-8', 'MEALS INCLUDED AND SPECIAL DIET UPON REQUEST', 3 ) );

			$this->template->Parse( 'fmABlock', 'foodmealsAnchorBlock', false );
			$this->template->Parse( 'fmBlock', 'foodmealsBlock', false );
		}
	}

	function init_section_six()
	{
		$this->template->set_block( 'main', 'surfInfoAnchorBlock', 'sfABlock' );
		$this->template->set_block( 'main', 'surfInfoLoopBlock', 'sflBlock' );
		$this->template->set_block( 'main', 'surfInfoBlock', 'sfBlock' );

		if( isset( $this->data[ 'surfing_info' ] ) && !empty( $this->data[ 'surfing_info' ] ) )
		{
			$dt = json_decode( $this->data[ 'surfing_info' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$i = 0;

				foreach( $dt as $d )
				{
					if( count( array_filter( $d ) ) == 0 )
					{
						continue;
					}

					$i++;

					$this->template->set_var( 'surfing_info_title', $d[ 'surf_info_title' ] );
					$this->template->set_var( 'surfing_info', nl2br( $d[ 'surf_info_desc' ] ) );

					$this->template->Parse( 'sflBlock', 'surfInfoLoopBlock', true );
				}

				if( $i > 0 )
				{
					$this->template->set_var( 'surfing_info_title', $this->global->translate( 'surfing-info', 'SURFING INFO', 3 ) );
					$this->template->set_var( 'surfing_info_title_2', $this->global->translate( 'surf-information', 'SURF INFORMATION', 3 ) );

					$this->template->Parse( 'sfABlock', 'surfInfoAnchorBlock', false );
					$this->template->Parse( 'sfBlock', 'surfInfoBlock', false );
				}
			}
		}
	}

	function init_section_seven()
	{
		$this->template->set_block( 'main', 'ourStaffLoopBlock', 'oslBlock' );
		$this->template->set_block( 'main', 'ourStaffBlock', 'osBlock' );

		if( isset( $this->data[ 'staff' ] ) && !empty( $this->data[ 'staff' ] ) )
		{
			$dt = json_decode( $this->data[ 'staff' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$i = 0;

				foreach( $dt as $d )
				{
					if( count( array_filter( $d ) ) == 0 )
					{
						continue;
					}

					$i++;

					$this->template->set_var( 'name', $d[ 'staff_name' ] );
					$this->template->set_var( 'description', $d[ 'staff_desc' ] );
					$this->template->set_var( 'performance', empty( $d[ 'staff_performance' ] ) ? 0 : $d[ 'staff_performance' ] );

					$this->template->set_var( 'avatar', $this->global->get_attachment_url( $d[ 'staff_avatar' ], 200, 200 ) );
					$this->template->set_var( 'avatar_placeholder', $this->global->get_attachment_url( $d[ 'staff_avatar' ], 10, 10 ) );

					$this->template->Parse( 'oslBlock', 'ourStaffLoopBlock', true );
				}

				if( $i > 0 )
				{
					$this->template->set_var( 'staff_title', $this->global->translate( 'our-staff', 'OUR STAFF', 3 ) );

					$this->template->Parse( 'osBlock', 'ourStaffBlock', false );
				}
			}
		}
	}

	function init_section_eight()
	{
		$this->template->set_block( 'main', 'todoAnchorBlock', 'tdABlock' );
		$this->template->set_block( 'main', 'todoLoopBlock', 'tdlBlock' );
		$this->template->set_block( 'main', 'todoBlock', 'tdBlock' );

		if( isset( $this->data[ 'things_to_do' ] ) && !empty( $this->data[ 'things_to_do' ] ) )
		{
			$dt = json_decode( $this->data[ 'things_to_do' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$i = 0;

				foreach( $dt as $d )
				{
					if( count( array_filter( $d ) ) == 0 )
					{
						continue;
					}

					$i++;

					$this->template->set_var( 'title', $d[ 'to_do_title' ] );
					$this->template->set_var( 'description', $d[ 'to_do_desc' ] );
					$this->template->set_var( 'image', $this->global->get_attachment_url( $d[ 'to_do_img' ], 450, 300 ) );

					$this->template->Parse( 'tdlBlock', 'todoLoopBlock', true );
				}

				if( $i > 0 )
				{
					$this->template->set_var( 'to_do_title', $this->global->translate( 'things-to-do', 'THINGS TO DO', 3 ) );

					$this->template->Parse( 'tdABlock', 'todoAnchorBlock', false );
					$this->template->Parse( 'tdBlock', 'todoBlock', false );
				}
			}
		}
	}

	function init_section_nine()
	{
		$this->template->set_block( 'main', 'gethereAnchorBlock', 'ghABlock' );
		$this->template->set_block( 'main', 'gethereLoopBlock', 'ghlBlock' );
		$this->template->set_block( 'main', 'gethereMapBlock', 'gmBlock' );
		$this->template->set_block( 'main', 'gethereBlock', 'ghBlock' );

		$empty = true;

		if( isset( $this->data[ 'arrival_by_plane' ] ) && !empty( $this->data[ 'arrival_by_plane' ] ) )
		{
			$empty = false;

			$this->template->set_var( 'arrival_by_title', $this->global->translate( 'arrival-by-airplane', 'ARRIVAL BY AIRPLANE', 3 ) );
			$this->template->set_var( 'arrival_by_transport', nl2br( $this->data[ 'arrival_by_plane' ] ) );

			$this->template->Parse( 'ghlBlock', 'gethereLoopBlock', true );
		}

		if( isset( $this->data[ 'arrival_by_bus' ] ) && !empty( $this->data[ 'arrival_by_bus' ] ) )
		{
			$empty = false;

			$this->template->set_var( 'arrival_by_title', $this->global->translate( 'arrival-by-bus', 'ARRIVAL BY BUS', 3 ) );
			$this->template->set_var( 'arrival_by_transport', nl2br( $this->data[ 'arrival_by_bus' ] ) );

			$this->template->Parse( 'ghlBlock', 'gethereLoopBlock', true );
		}

		if( isset( $this->data[ 'arrival_by_train' ] ) && !empty( $this->data[ 'arrival_by_train' ] ) )
		{
			$empty = false;

			$this->template->set_var( 'arrival_by_title', $this->global->translate( 'arrival-by-train', 'ARRIVAL BY TRAIN', 3 ) );
			$this->template->set_var( 'arrival_by_transport', nl2br( $this->data[ 'arrival_by_train' ] ) );

			$this->template->Parse( 'ghlBlock', 'gethereLoopBlock', true );
		}

		if( isset( $this->data[ 'arrival_by_boat' ] ) && !empty( $this->data[ 'arrival_by_boat' ] ) )
		{
			$empty = false;

			$this->template->set_var( 'arrival_by_title', $this->global->translate( 'arrival-by-boat', 'ARRIVAL BY BOAT', 3 ) );
			$this->template->set_var( 'arrival_by_transport', nl2br( $this->data[ 'arrival_by_boat' ] ) );

			$this->template->Parse( 'ghlBlock', 'gethereLoopBlock', true );
		}

		if( $empty === false )
		{
			if( $this->data[ 'map_coordinate' ] != '' )
			{
				$mapi = $this->global->get_setting_value( 'google_map_api_key' );

				$this->template->set_var( 'map_coordinate', $this->data[ 'map_coordinate' ] );

				$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://maps.googleapis.com/maps/api/js?key=' . $mapi . '&callback=init_map', array( 'async', 'defer' ) ) );

				$this->template->Parse( 'gmBlock', 'gethereMapBlock', false );
			}

			$this->template->set_var( 'closer_airport_city', $this->data[ 'closer_airport_city' ] );
			$this->template->set_var( 'closer_airport_name', $this->data[ 'closer_airport_name' ] );

			$this->template->set_var( 'how_to_title', $this->global->translate( 'how-to-get-there', 'HOW TO GET THERE', 3 ) );
			$this->template->set_var( 'closer_airport', $this->global->translate( 'closer-airport', 'CLOSER AIRPORT', 3 ) );

			$this->template->Parse( 'ghABlock', 'gethereAnchorBlock', false );
			$this->template->Parse( 'ghBlock', 'gethereBlock', false );
		}
	}

	function init_section_ten()
	{
		$this->template->set_block( 'main', 'reviewAnchorBlock', 'rvABlock' );
		$this->template->set_block( 'main', 'reviewLoopBlock', 'rvlBlock' );
		$this->template->set_block( 'main', 'reviewBlock', 'rvBlock' );

		$sm = 'SELECT * FROM lumonata_reviews AS a WHERE a.lpost_id = %d ORDER BY a.lcreated_date DESC';
        $qm = parent::prepare_query( $sm, $this->data[ 'lpost_id' ] );
        $rm = parent::query( $qm );
        $nm = parent::num_rows( $rm );

        $qf = $qm;// . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
            	$this->template->set_var( 'review_name', $df[ 'lname' ] );
            	$this->template->set_var( 'review_title', $df[ 'lreview_title' ] );
            	$this->template->set_var( 'review_content', $df[ 'lreview_note' ] );
            	$this->template->set_var( 'review_date', date( 'M Y', $df[ 'lcreated_date' ] ) );
            	$this->template->set_var( 'review_avatar', $this->global->get_review_image( $df, 150, 150 ) );
            	$this->template->set_var( 'review_avatar_placeholder', $this->global->get_review_image( $df, 10, 10 ) );

            	$this->template->Parse( 'rvlBlock', 'reviewLoopBlock', true );
            }

            $this->template->set_var( 'num', $nm );

            $this->template->set_var( 'rating', $this->rate[ 'rating' ] );
            $this->template->set_var( 'rating_label', $this->global->get_accommodation_rating_label( $this->rate ) );

            $this->template->set_var( 'staff_rate', $this->rate[ 'staff_rate' ] );
            $this->template->set_var( 'overall_rate', $this->rate[ 'overall_rate' ] );
            $this->template->set_var( 'accuracy_rate', $this->rate[ 'accuracy_rate' ] );
            $this->template->set_var( 'location_rate', $this->rate[ 'location_rate' ] );
            $this->template->set_var( 'activities_rate', $this->rate[ 'activities_rate' ] );
            $this->template->set_var( 'cleanliness_rate', $this->rate[ 'cleanliness_rate' ] );

			$this->template->set_var( 'reviews_title', $this->global->translate( 'reviews', 'REVIEWS', 3 ) );
			$this->template->set_var( 'reviews_title_2', $this->global->translate( 'travellers-reviews-title', sprintf( 'TRAVELLERS REVIEWS OF %s', $this->data[ 'ltitle' ] ), 3, array( $this->data[ 'ltitle' ] ) ) );
			
			$this->template->set_var( 'most_recent', $this->global->translate( 'most-recent', 'MOST RECENT', 3 ) );
			$this->template->set_var( 'num_reviews', $this->global->translate( 'num-reviews', sprintf( '%s reviews', $nm ), 1, array( $nm ) ) );

			$this->template->set_var( 'review_txt_4', $this->global->translate( 'cleanliness', 'Cleanliness', 1 ) );
			$this->template->set_var( 'review_txt_3', $this->global->translate( 'activities', 'Activities', 1 ) );
			$this->template->set_var( 'review_txt_1', $this->global->translate( 'accuracy', 'Accuracy', 1 ) );
			$this->template->set_var( 'review_txt_2', $this->global->translate( 'location', 'Location', 1 ) );
			$this->template->set_var( 'review_txt_6', $this->global->translate( 'overall', 'Overall', 1 ) );
			$this->template->set_var( 'review_txt_5', $this->global->translate( 'staff', 'Staff', 1 ) );

            $this->template->Parse( 'rvABlock', 'reviewAnchorBlock', false );
            $this->template->Parse( 'rvBlock', 'reviewBlock', false );
        }
	}

	function init_section_eleven()
	{
		$this->template->set_block( 'main', 'partnerBlock', 'prBlock' );

		if( isset( $this->data[ 'partner_since' ] ) && !empty( $this->data[ 'partner_since' ] ) )
		{
			$this->template->set_var( 'partner_since', $this->data[ 'partner_since' ] );
			$this->template->set_var( 'partner_location', $this->data[ 'partner_location' ] );
			$this->template->set_var( 'partner_img', $this->global->get_attachment_url( $this->data[ 'partner_img' ], 300 ) );

			$this->template->Parse( 'prBlock', 'partnerBlock', false );
		}		
	}

	function init_section_twelve()
	{
		$this->template->set_block( 'main', 'termconditionAnchorBlock', 'tcABlock' );
		$this->template->set_block( 'main', 'termconditionBlock', 'tcBlock' );

		$tc_payment      = $this->global->get_setting_value( 'tc_payment' );
		$tc_transfer     = $this->global->get_setting_value( 'tc_transfer' );
		$tc_cancellation = $this->global->get_setting_value( 'tc_cancellation' );

		$num = 0;

		if( empty( $tc_payment ) === false )
		{
			$this->template->set_var( 'tc_payment', $tc_payment );

			$num++;
		}
		
		if( empty( $tc_cancellation ) === false )
		{
			$this->template->set_var( 'tc_cancellation', $tc_cancellation );

			$num++;
		}
		
		if( empty( $tc_transfer ) === false )
		{
			$this->template->set_var( 'tc_transfer', $tc_transfer );

			$num++;
		}

		if( $num > 0 )
		{
			$this->template->set_var( 't_c_title', $this->global->translate( 't-c', 'T&C', 3 ) );

			$this->template->set_var( 'tc_title_1', $this->global->translate( 'terms-and-policy-title', 'TRAVEL TERMS AND CANCELATION POLICY', 3 ) );
			$this->template->set_var( 'tc_title_3', $this->global->translate( 'cancellation-policy', 'CANCELLATION POLICY', 3 ) );
			$this->template->set_var( 'tc_title_4', $this->global->translate( 'transfer', 'TRANSFER', 3 ) );
			$this->template->set_var( 'tc_title_2', $this->global->translate( 'payment', 'PAYMENT', 3 ) );


			$this->template->Parse( 'tcABlock', 'termconditionAnchorBlock', false );
			$this->template->Parse( 'tcBlock', 'termconditionBlock', false );
		}
	}

	function init_section_thirteen()
	{
		$this->template->set_block( 'main', 'relatedPostLoopBlock', 'rlpBlock' );
		$this->template->set_block( 'main', 'relatedPostBlock', 'rlBlock' );

		//-- Trending Data
		$related = $this->global->get_related_post( $this->data, $this->lang_id );

		if( !empty( $related ) )
		{
			foreach( $related as $dest => $dta )
			{
				foreach( $dta as $dt )
				{
					$brief = $this->global->get_brief_text( $dt[ 'ldescription' ], 200 );

					if( $brief != '' )
					{
						$this->template->set_var( 'rbrief', sprintf( '<p>%s</p>', $brief ) );
					}
					else
					{
						$this->template->set_var( 'rbrief', '' );
					}

					$this->template->set_var( 'rpermalink', $this->global->permalink( $dt ) );
					$this->template->set_var( 'rtitle', $this->global->get_listing_title( $dt ) );
					$this->template->set_var( 'rimg_thumb', $this->global->get_attachment_url( $dt[ 'accommodation_image' ], 551, 403 ) );
					$this->template->set_var( 'rimg_placeholder', $this->global->get_attachment_url( $dt[ 'accommodation_image' ], 10, 10 ) );

					$this->template->Parse( 'rlpBlock', 'relatedPostLoopBlock', true );
				}
			
				$this->template->set_var( 'related_title', sprintf( 'Similar Accommodations In %s', $dest ) );
			}

			$this->template->Parse( 'rlBlock', 'relatedPostBlock', false );
		}
	}

	function init()
	{
		$this->lang_id     = $this->global->get_current_language_id();
		$this->lang_code   = $this->global->get_current_language_code();
		$this->lang_def    = $this->global->get_setting_value( 'llanguage' );
		$this->start_price = 0;
		
		$this->data = $this->global->get_posts( array( 
			'ltype'    => 'accommodation',
			'lsef_url' => $this->uri[ 0 ],
	    	'llang_id' => $this->lang_id,
			'lstatus'  => 1 
		));

		if( isset( $this->data[ 'lpost_id' ] ) )
		{
			//-- GET schedule trips
			$this->schedules = $this->global->get_schedule( array( 
				'lpost_id' => $this->data[ 'lpost_id' ],
				'lstatus'  => 1 
			), false, array( 'lcheck_in' => 'ASC' ) );

			//-- GET packages
			$this->packages = $this->global->get_packages( array( 
				'lpost_id' => $this->data[ 'lpost_id' ],
				'lstatus'  => 1 
			), false );

			//-- GET rate reviews
            $this->rate = $this->calculate_rate();

			//-- COUNT & SET page view
			$this->global->set_page_views( $this->data[ 'lpost_id' ] );

			//-- GET accommodation start price
			if( isset( $this->data[ 'price_type' ] ) )
			{
				if( $this->data[ 'price_type' ] == '0' )
				{
					$this->start_price = $this->global->get_start_price( $this->data );
				}
				else
				{
					if( $this->data[ 'page_template' ] == '1' )
					{
						$this->start_price = $this->global->get_schedule_start_price( $this->schedules, true );
					}
					else
					{
						$this->start_price = $this->global->get_package_start_price( $this->packages, true );
					}
				}
			}

			//-- GET accommodation minimum night
			if( !isset( $this->data[ 'min_night' ] ) )
			{
				$this->data[ 'min_night' ] = 1;
			}
		}
	}

	function init_detail_js()
	{
		return '
		<script>
			jQuery(document).ready(function(){
				RESPONSIVEUI.responsiveTabs();
				init_accommodation_slide();
				init_accommodation_form();
				init_related_post_slide();
				init_term_accordion();
				init_staff_slide();
				init_lazy_load();
				init_anchor();
			});
		</script>';
	}

	function init_archive_js()
	{
		return '
		<script>
			jQuery(document).ready(function(){
				init_lazy_load();
			});
		</script>';
	}

	function init_search_js()
	{
		return '
		<script>
			jQuery(document).ready(function(){
				init_header_form();
				init_lazy_load();
			});
		</script>';
	}

	function calculate_rate()
	{
		$s = 'SELECT * FROM lumonata_reviews AS a WHERE a.lpost_id = %d ORDER BY a.lcreated_date DESC';
        $q = parent::prepare_query( $s, $this->data[ 'lpost_id' ] );
        $r = parent::query( $q );
        $n = parent::num_rows( $r );

        if( $n > 0 )
        {
        	$arate = array();
        	$brate = array();
        	$crate = array();
        	$drate = array();
        	$erate = array();
        	$frate = array();

            while( $d = parent::fetch_array( $r ) )
            {
	        	$arate[] = isset( $d[ 'laccuracy_score' ] ) ? intval( $d[ 'laccuracy_score' ] ) : 0;
	        	$brate[] = isset( $d[ 'llocation_score' ] ) ? intval( $d[ 'llocation_score' ] ) : 0;
	        	$crate[] = isset( $d[ 'lactivities_score' ] ) ? intval( $d[ 'lactivities_score' ] ) : 0;
	        	$drate[] = isset( $d[ 'lcleanliness_score' ] ) ? intval( $d[ 'lcleanliness_score' ] ) : 0;
	        	$erate[] = isset( $d[ 'lstaff_score' ] ) ? intval( $d[ 'lstaff_score' ] ) : 0;
	        	$frate[] = isset( $d[ 'loverall_score' ] ) ? intval( $d[ 'loverall_score' ] ) : 0;
            }

            $result[ 'accuracy_rate' ] = empty( $arate ) ? 0 : ceil( array_sum( $arate ) / count( $arate ) );
            $result[ 'location_rate' ] = empty( $brate ) ? 0 : ceil( array_sum( $brate ) / count( $brate ) );
            $result[ 'activities_rate' ] = empty( $crate ) ? 0 : ceil( array_sum( $crate ) / count( $crate ) );
            $result[ 'cleanliness_rate' ] = empty( $drate ) ? 0 : ceil( array_sum( $drate ) / count( $drate ) );
            $result[ 'staff_rate' ] = empty( $erate ) ? 0 : ceil( array_sum( $erate ) / count( $erate ) );
            $result[ 'overall_rate' ] = empty( $frate ) ? 0 : ceil( array_sum( $frate ) / count( $frate ) );

            $result[ 'rating' ] = ceil( array_sum( $result ) / count( $result ) );

            return $result;
        }
	}

	function calculate_duration( $data )
	{
		if( $data[ 'lnight' ] > 1 )
		{
			$day = $data[ 'lnight' ] + 1;
		}
		else
		{
			$day = 1;
		}

		return sprintf( '%sD%sN', $day, $data[ 'lnight' ] );
	}

	function breadcrumbs()
	{
		$dest = $this->global->get_field_value( 'lumonata_post', array( 'ltitle', 'lsef_url', 'ltype' ), array( 'lpost_id' => $this->data[ 'accommodation_city' ], 'llang_id' => $this->lang_id ) );
		
		if( empty( $dest ) === false )
		{
			$content = '
	        <ul>
	            <li><a href="' . $this->global->get_home_url() . '">Home</a></li>';

	            if( isset( $this->data[ 'surf_trip' ] ) )
	            {
	            	$content .= '
	            	<li>';

	            		$term = array();

		            	foreach( $this->data[ 'surf_trip' ] as $dt )
		            	{
		            		$term[] = '<a href="' . $this->global->term_link( $dt ) . '">' . $dt[ 'lname' ] . '</a>';
		            	}

		            	$content .= implode( ',&nbsp;', $term );

		            	$content .= '
		            </li>';
	            }

	            $content .= '
	            <li class="asasas"><a href="' . $this->global->permalink( $dest ) . '">' . $dest[ 'ltitle' ] . '</a></li>
	            <li>' . $this->global->get_listing_title( $this->data ) . '</li>
	        </ul>';

	        return $content;
		}
	}

	function locations()
	{
		$include  = array( 'accommodation_state', 'accommodation_country' );
    	$location = array();

    	foreach( $include as $dt )
    	{
    		if( isset( $this->data[ $dt ] ) && !empty( $this->data[ $dt ] ) )
    		{
    			$dest = $this->global->get_field_value( 'lumonata_post', array( 'ltitle', 'lsef_url', 'ltype' ), array( 'lpost_id' => $this->data[ $dt ], 'llang_id' => $this->lang_id ) );

    			if( empty( $dest ) === false )
				{
        			$location[] = '<a href="' . $this->global->permalink( $dest ) . '">' . $dest[ 'ltitle' ] . '</a>';
        		}
        	}
    	}

    	if( !empty( $location ) )
		{
			$content = '
	        <div class="veneu-header-location">
	            <img src="//' . THEME_URL . '/assets/img/pin.svg">
	            <label>';

		            $content .= implode( ',&nbsp;', $location );
	            	$content .= '
	            </label>
	        </div>';

	    	return $content;
	    }
	}

	function table_action( $data = array(), $posts = array(), $is_package = false )
	{
		if( $is_package )
		{
			$btn_text = $this->global->translate( 'select-package', 'SELECT PACKAGE', 3 );
		}
		else
		{
			$btn_text = $this->global->translate( 'select-this-trip', 'SELECT THIS TRIP', 3 );
		}

		$action = '
		<div class="osmall-btn">
            <button class="select-trip" type="button">' . $btn_text . '</button>
        </div>';

		return $action;
	}

	function room_options( $posts = array() )
	{
		$option = '';

		foreach( $posts[ 'room_type' ] as $dt )
		{
			$option .= '<option value="' . $dt[ 'lterm_id' ] . '">' . $dt[ 'lname' ] . '</option>';
		}

		return $option;
	}

	function trip_cabins_option( $posts = array() )
	{
		return $this->room_options( $posts );
	}

    function trip_itinerary( $itinerary = '' )
    {
        if( empty( $itinerary  ) )
        {
            return '-';
        }
        else
        {
            return str_replace( ',', '<br/>', $itinerary );
        }
    }

	function room_max_adult( $max = 0 )
	{
		if( $max == 0 )
		{
			return $this->global->translate( 'no-maximum-value', 'No maximum value' );
		}
		else
		{
			return $this->global->translate( 'max-adult-guests', 'Max ' . $max . ' Adult Guests', 1, array( $max ) );
		}
	}

	function room_max_child( $max = 0 )
	{
		if( $max == 0 )
		{
			return $this->global->translate( 'no-maximum-value', 'No maximum value' );
		}
		else
		{
			return $this->global->translate( 'max-child-guests', $max . ' Children under 12', 1, array( $max ) );
		}
	}

	function room_baby_bed( $bed = 0 )
	{
		if( $bed == 0 )
		{
			return $this->global->translate( 'baby-bed-text-2', 'Baby bed not available' );
		}
		else
		{
			return $this->global->translate( 'baby-bed-text', 'Baby bed available' );
		}
	}

	function room_gallery( $data = array() )
	{
		$content = '';

		if( !empty( $data[ 'room_type_gallery' ] ) )
		{
			$dt = json_decode( $data[ 'room_type_gallery' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				foreach( $dt as $d )
				{
					$img_src = $this->global->get_attachment_url( $d, null, 750 );
					$img_plc = $this->global->get_attachment_url( $d, 10, 10 );

					$content .= '<img data-fancybox="gallery-' . $data[ 'lterm_id' ] . '" data-src="' . $img_src . '" src="' . $img_plc . '" class="b-lazy" alt="">';
				}
			}
		}

		return $content;
	}

	function room_facilities( $items = '', $post_data = array() )
	{
		if( !empty( $items ) )
		{
			$dt = json_decode( $items, true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$title = $this->global->translate( 'facilities', 'FACILITIES', 3 );
				$item  = array_column( $dt, 'facility');
				$item  = array_filter( $item );

				if( empty( $item ) )
				{
					if( isset( $post_data[ 'amenities' ] ) && !empty( $post_data[ 'amenities' ] ) )
					{
						$am = json_decode( $post_data[ 'amenities' ], true );

						if( $am !== null && json_last_error() === JSON_ERROR_NONE )
						{
							$content = '
							<span>' . $title . '</span>
							<ul>';

								foreach( $am as $term_id )
								{
									$content .= '<li>' . $this->global->get_field_value( 'lumonata_post_terms', 'lname', array( 'lterm_id' => $term_id ) ) . '</li>';
								}

								$content .= '
							</ul>';
						}
					}
				}
				else
				{
					$content = '
					<span>' . $title . '</span>
					<ul>';

						foreach( $dt as $d )
						{
							$content .= '<li>' . $d[ 'facility' ] . '</li>';
						}

						$content .= '
					</ul>';
				}

				return $content;
			}
		}
		else
		{
			if( isset( $post_data[ 'amenities' ] ) && !empty( $post_data[ 'amenities' ] ) )
			{
				$am = json_decode( $post_data[ 'amenities' ], true );

				if( $am !== null && json_last_error() === JSON_ERROR_NONE )
				{
					$content = '
					<span>' . $this->global->translate( 'facilities', 'FACILITIES', 3 ) . '</span>
					<ul>';

						foreach( $am as $term_id )
						{
							$content .= '<li>' . $this->global->get_field_value( 'lumonata_post_terms', 'lname', array( 'lterm_id' => $term_id ) ) . '</li>';
						}

						$content .= '
					</ul>';

					return $content;
				}
			}
		}
	}

	function set_meta_title()
	{
		if( isset( $this->data[ 'lmeta_title' ] ) )
		{
			$meta_title = $this->data[ 'lmeta_title' ];
		}
		else
		{
			$meta_title = '';
		}

		if( empty( $meta_title ) )
		{
			$meta_title = $this->global->get_setting_value( 'lmeta_title' );

			if( empty( $meta_title ) )
			{
				$meta_title = 'Accommodation - ' . $this->global->get_setting_value( 'web' );
			}
		}
		
		$this->actions->add_actions( 'meta_title', $meta_title );
	}

	function set_meta_desc()
	{
		$meta_description = $this->global->get_setting_value( 'lmeta_desc' );

		if( empty( $meta_description ) === false )
		{
			$meta_description = sprintf( '<meta name="description" content="%s" />', strip_tags( $meta_description ) );

			$this->actions->add_actions( 'meta_description', $meta_description );
		}
	}
}

?>