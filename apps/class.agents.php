<?php

class agents extends db
{
	function __construct( $action, $uri = '' )
	{
    	parent::__construct();

    	$this->template = new Template( MODULES_THEME_DIR );
    	$this->global   = new globalFunctions();
        $this->flash    = new flash_message();
		$this->uri      = array_filter( $uri );
		$this->actions  = $action;

		$this->import();
		$this->init();
	}
	
	function load()
	{
		if( $this->global->is_agent_login() )
        {
        	if( $this->apps == 'signin' || empty( $this->apps ) )
	        {
	            header( 'Location:' . HT_SERVER . SITE_URL . '/agent/dashboard/' );

	            exit;
	        }
	        else if( $this->apps == 'dashboard' )
	        {
	        	return $this->dashboard();
	        }
	        else if( $this->apps == 'listings' )
	        {
	        	return $this->listings();
	        }
	        else if( $this->apps == 'booking-request' )
	        {
	        	return $this->booking_request();
	        }
	        else if( $this->apps == 'booking-details' )
	        {
	        	return $this->booking_details();
	        }
	        else if( $this->apps == 'reservations' )
	        {
	        	return $this->reservations();
	        }
	        else if( $this->apps == 'calendar' )
	        {
	        	return $this->calendar();
	        }
	        else if( $this->apps == 'profile' )
	        {
	        	return $this->profile();
	        }
	        else if( $this->apps == 'security' )
	        {
	        	return $this->security();
	        }
	        else if( $this->apps == 'help' )
	        {
	        	return $this->help();
	        }
	        else if( $this->apps == 'upload-media' )
	        {
	        	return $this->media();
	        }
	        else if( $this->apps == 'confirm-booking' )
	        {
	        	return $this->confirm_booking();
	        }
	        else if( $this->apps == 'decline-booking' )
	        {
	        	return $this->decline_booking();
	        }
	        else if( $this->apps == 'invalid-booking' )
	        {
	        	return $this->invalid_booking();
	        }
	        else if( $this->apps == 'processed-booking' )
	        {
	        	return $this->processed_booking();
	        }
        	else if( $this->apps == 'signup' )
	        {
	        	return $this->signup();
	        }
        	else if( $this->apps == 'forgot-password' )
	        {
	        	return $this->forgot_password();
	        }
	        else if( $this->apps == 'signout' )
	        {
	        	return $this->signout();
	        }
	        else
	        {
	            header( 'Location:' . HT_SERVER . SITE_URL . '/not-found/' );

	            exit;
	        }
	    }
	    else
	    {
        	if( $this->apps == 'signin' )
	        {
	        	return $this->signin();
	        }
        	else if( $this->apps == 'signup' )
	        {
	        	return $this->signup();
	        }
        	else if( $this->apps == 'forgot-password' )
	        {
	        	return $this->forgot_password();
	        }
	        else
	        {
	        	if( empty( $this->uri ) )
	        	{
	            	header( 'Location:' . HT_SERVER . SITE_URL . '/agent/signin/' );
	        	}
	        	else
	        	{
	        		$ref = base64_encode( HT_SERVER . SITE_URL . '/agent/' . implode( '/', $this->uri ) );

	            	header( 'Location:' . HT_SERVER . SITE_URL . '/agent/signin/?ref=' . $ref );
	        	}

	            exit;
	        }
	    }
	}
	
	function confirm_booking()
	{
	    $bid = json_decode( base64_decode( $this->uri[1] ), true );
	    
	    if( !empty( $bid ) )
	    {
	    	$data = $this->global->get_booking( $bid );

	    	if( !empty( $data ) )
	    	{
	    		if( $data[ 'lstatus' ] == 0 )
	    		{
			        $r = parent::update( 'lumonata_booking', array( 'lstatus' => 1 ), array( 'lbooking_id' => $bid ) );

		            if( is_array( $r ) )
		            {
		                $commit = 0;
		            }

		            if( $commit == 0 )
		            {
		                parent::rollback();
		            }
		            else
		            {
		            	//-- SEND email
		                $this->func->sendBookingAcceptedEmail( $bid );

		                parent::commit();
		            }

        			header( 'Location:' . HT_SERVER . SITE_URL . '/agent/booking-request/' );
	    		}
	    		else
	    		{
	    			header( 'Location:' . HT_SERVER . SITE_URL . '/agent/processed-booking/' );
	    		}
	    	}
	    	else
	    	{
	    		header( 'Location:' . HT_SERVER . SITE_URL . '/agent/invalid-booking/' );
	    	}
	    }
	    else
	    {
	    	header( 'Location:' . HT_SERVER . SITE_URL . '/agent/invalid-booking/' );
	    }

        exit;
	}
	
	function decline_booking()
	{
	    $bid = json_decode( base64_decode( $this->uri[1] ), true );
	    
	    if( !empty( $bid ) )
	    {
	    	$data = $this->global->get_booking( $bid );

	    	if( !empty( $data ) )
	    	{
	    		if( $data[ 'lstatus' ] == 0 )
	    		{
			        $r = parent::update( 'lumonata_booking', array( 'lstatus' => 2 ), array( 'lbooking_id' => $bid ) );

		            if( is_array( $r ) )
		            {
		                $commit = 0;
		            }

		            if( $commit == 0 )
		            {
		                parent::rollback();
		            }
		            else
		            {
		            	//-- SEND email
		                $this->func->sendBookingRefusedEmail( $bid );

		                parent::commit();
		            }

        			header( 'Location:' . HT_SERVER . SITE_URL . '/agent/booking-request/' );
	    		}
	    		else
	    		{
	    			header( 'Location:' . HT_SERVER . SITE_URL . '/agent/processed-booking/' );
	    		}
	    	}
	    	else
	    	{
	    		header( 'Location:' . HT_SERVER . SITE_URL . '/agent/invalid-booking/' );
	    	}
	    }
	    else
	    {
	    	header( 'Location:' . HT_SERVER . SITE_URL . '/agent/invalid-booking/' );
	    }

        exit;
	}

	function signup()
	{
		$this->register();

        $this->flash = new flash_message();

        $this->actions->add_actions( 'css', $this->global->get_css( 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' ) );
        $this->actions->add_actions( 'css', $this->global->get_css( '//' . THEME_URL . '/assets/css/style.css' ) );

		$this->actions->add_actions( 'jvc', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js' ) );
		$this->actions->add_actions( 'jvc', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js' ) );
		$this->actions->add_actions( 'jvc', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js' ) );
		$this->actions->add_actions( 'jvs', $this->init_js() );

		$this->template->set_file( 'home', 'register.html' );

		$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'image_url', IMAGE_URL );
		$this->template->set_var( 'login_url', HT_SERVER . SITE_URL . '/agent/signin/' );

		$this->template->set_var( 'message', $this->flash->message() );
		$this->template->set_var( 'meta_title', 'Create New Account - ' . $this->global->get_setting_value( 'name' ) );

		$this->template->set_var( 'str_1', $this->global->translate( 'sign-up-partner', 'SIGN UP PARTNER', 3 ) );
		$this->template->set_var( 'str_2', $this->global->translate( 'email-address', 'Email Address', 1 ) );
		$this->template->set_var( 'str_3', $this->global->translate( 'first-name', 'First Name', 1 ) );
		$this->template->set_var( 'str_4', $this->global->translate( 'last-name', 'Last Name', 1 ) );
		$this->template->set_var( 'str_5', $this->global->translate( 'create-username', 'Create Username' ) );
		$this->template->set_var( 'str_6', $this->global->translate( 'create-password', 'Create Password' ) );
		$this->template->set_var( 'str_7', $this->global->translate( 'confirm-password', 'Confirm Password' ) );
		$this->template->set_var( 'str_8', $this->global->translate( 'already-have-an-account', 'Already have an account?' ) );
		$this->template->set_var( 'str_9', $this->global->translate( 'log-in', 'Log In' ) );
		$this->template->set_var( 'str_10', $this->global->translate( 'sign-up', 'Sign Up' ) );

		$this->template->set_var( 'css', $this->actions->attemp_actions( 'css' ) );
		$this->template->set_var( 'jvc', $this->actions->attemp_actions( 'jvc' ) );
		$this->template->set_var( 'jvs', $this->actions->attemp_actions( 'jvs' ) );

		$this->template->Parse( 'mBlock', 'mainBlock', false );

		return $this->template->pparse( 'Output', 'home' );
	}

	function signin()
	{
		$this->auth();

        $this->flash = new flash_message();

        $this->actions->add_actions( 'css', $this->global->get_css( 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' ) );
        $this->actions->add_actions( 'css', $this->global->get_css( '//' . THEME_URL . '/assets/css/style.css' ) );

		$this->actions->add_actions( 'jvc', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js' ) );
		$this->actions->add_actions( 'jvc', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js' ) );
		$this->actions->add_actions( 'jvc', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js' ) );
		$this->actions->add_actions( 'jvs', $this->init_js() );

		$this->template->set_file( 'home', 'login.html' );

		$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'back_url', HT_SERVER . SITE_URL );
		$this->template->set_var( 'register_url', HT_SERVER . SITE_URL . '/agent/signup/' );
		$this->template->set_var( 'forgot_url', HT_SERVER . SITE_URL . '/agent/forgot-password/' );

		$this->template->set_var( 'message', $this->flash->message() );
		$this->template->set_var( 'meta_title', 'Login - ' . $this->global->get_setting_value( 'name' ) );

   		$this->template->set_var( 'str_1', $this->global->translate( 'partner-log-in', 'PARTNER LOG IN', 3 ) );
		$this->template->set_var( 'str_2', $this->global->translate( 'email-address-or-username', 'Email Address / Username', 1 ) );
		$this->template->set_var( 'str_3', $this->global->translate( 'password-2', 'Password' ) );
		$this->template->set_var( 'str_4', $this->global->translate( 'stay-log-in', 'Stay log in?' ) );
		$this->template->set_var( 'str_5', $this->global->translate( 'forgot', 'Forgot' ) );
		$this->template->set_var( 'str_6', $this->global->translate( 'username-or-password', 'Username / Password?' ) );		
		$this->template->set_var( 'str_7', $this->global->translate( 'create-an-account', 'Create an account?' ) );
		$this->template->set_var( 'str_8', $this->global->translate( 'sign-up', 'Sign Up' ) );
		$this->template->set_var( 'str_9', $this->global->translate( 'log-in', 'Log In' ) );

		$this->template->set_var( 'str_10', $this->global->translate( 'back', 'BACK', 1 ) );

		$this->template->set_var( 'css', $this->actions->attemp_actions( 'css' ) );
		$this->template->set_var( 'jvc', $this->actions->attemp_actions( 'jvc' ) );
		$this->template->set_var( 'jvs', $this->actions->attemp_actions( 'jvs' ) );


		$this->template->Parse( 'mBlock', 'mainBlock', false );

		return $this->template->pparse( 'Output', 'home' );
	}

	function signout()
	{
		if( isset( $_SESSION[ AGENT_SESSION_NAME ] ) )
		{
			unset( $_SESSION[ AGENT_SESSION_NAME ] );
		}

		header( 'location:'. HT_SERVER . SITE_URL . '/agent/signin/' );

		exit;
	}

	function invalid_booking()
	{
		require_once( MODULES_THEME_DIR . '/apps/page/class.php' );

        $this->mod = $this->global->get_module_id( 'agent' );

        $this->cls = new page( MODULES_THEME_DIR . '/apps/page', $this->actions );

		$this->template->set_file( 'home', 'home.html' );

        $this->template->set_block( 'home', 'memberMenuContent', 'mCBlock' );
        $this->template->set_block( 'home', 'agentMenuContent', 'aCBlock' );
		$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

    	$this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );
		$this->template->set_var( 'menu', $this->global->get_agent_menu( 'profile' ) );

		$this->template->set_var( 'main_body', $this->cls->view( $this->mod, 1 ) );

		$this->template->set_var( 'site_url', SITE_URL );
	    $this->template->set_var( 'admin_url', ADMIN_URL );
	    $this->template->set_var( 'image_url', IMAGE_URL );
	    $this->template->set_var( 'version', '?v=' . VERSION );
	    $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
	    $this->template->set_var( 'template_url', MODULES_THEME_URL );

	    $this->template->set_var( 'jvs_links', $this->actions->attemp_actions( 'jvs' ) );
	    $this->template->set_var( 'css_links', $this->actions->attemp_actions( 'css' ) );

        $this->template->Parse( 'aCBlock', 'agentMenuContent', false );
	    $this->template->Parse( 'mBlock', 'mainBlock', false );

	    return $this->template->pparse( 'Output', 'home' );
	}

	function processed_booking()
	{
		require_once( MODULES_THEME_DIR . '/apps/page/class.php' );

        $this->mod = $this->global->get_module_id( 'agent' );

        $this->cls = new page( MODULES_THEME_DIR . '/apps/page', $this->actions );

		$this->template->set_file( 'home', 'home.html' );

        $this->template->set_block( 'home', 'memberMenuContent', 'mCBlock' );
        $this->template->set_block( 'home', 'agentMenuContent', 'aCBlock' );
		$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

    	$this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );
		$this->template->set_var( 'menu', $this->global->get_agent_menu( 'profile' ) );

		$this->template->set_var( 'main_body', $this->cls->view( $this->mod, 2 ) );

		$this->template->set_var( 'site_url', SITE_URL );
	    $this->template->set_var( 'admin_url', ADMIN_URL );
	    $this->template->set_var( 'image_url', IMAGE_URL );
	    $this->template->set_var( 'version', '?v=' . VERSION );
	    $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
	    $this->template->set_var( 'template_url', MODULES_THEME_URL );

	    $this->template->set_var( 'jvs_links', $this->actions->attemp_actions( 'jvs' ) );
	    $this->template->set_var( 'css_links', $this->actions->attemp_actions( 'css' ) );

        $this->template->Parse( 'aCBlock', 'agentMenuContent', false );
	    $this->template->Parse( 'mBlock', 'mainBlock', false );

	    return $this->template->pparse( 'Output', 'home' );
	}

	function forgot_password()
	{
		$this->reset();

        $this->flash = new flash_message();

        $this->actions->add_actions( 'css', $this->global->get_css( 'https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' ) );
        $this->actions->add_actions( 'css', $this->global->get_css( '//' . THEME_URL . '/assets/css/style.css' ) );

		$this->actions->add_actions( 'jvc', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js' ) );
		$this->actions->add_actions( 'jvc', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js' ) );
		$this->actions->add_actions( 'jvc', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js' ) );
		$this->actions->add_actions( 'jvs', $this->init_js() );

		$this->template->set_file( 'home', 'forgot.html' );

		$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'back_url', HT_SERVER . SITE_URL );
		$this->template->set_var( 'login_url', HT_SERVER . SITE_URL . '/agent/signin/' );

		$this->template->set_var( 'message', $this->flash->message() );
		$this->template->set_var( 'meta_title', 'Forgot Password - ' . $this->global->get_setting_value( 'name' ) );

   		$this->template->set_var( 'str_1', $this->global->translate( 'reset-password', 'RESET PASSWORD', 3 ) );
		$this->template->set_var( 'str_2', $this->global->translate( 'forgot-password-desc', 'Please enter your e-mail address and click button below and you will receive an e-mail with your new password.' ) );
		$this->template->set_var( 'str_3', $this->global->translate( 'email-address', 'Email Address', 1 ) );
		$this->template->set_var( 'str_4', $this->global->translate( 'login', 'Login', 1 ) );
		$this->template->set_var( 'str_5', $this->global->translate( 'get-new-password', 'Get New Password' ) );

		$this->template->set_var( 'css', $this->actions->attemp_actions( 'css' ) );
		$this->template->set_var( 'jvc', $this->actions->attemp_actions( 'jvc' ) );
		$this->template->set_var( 'jvs', $this->actions->attemp_actions( 'jvs' ) );

		$this->template->Parse( 'mBlock', 'mainBlock', false );

		return $this->template->pparse( 'Output', 'home' );
	}

	function dashboard()
	{
		require_once( MODULES_THEME_DIR . '/apps/dashboard/class.php' );

        $this->mod = $this->global->get_module_id( 'dashboard' );

        $this->cls = new dashboard( MODULES_THEME_DIR . '/apps/dashboard', $this->actions );

		if( $this->ruri[ 0 ] == 'request' )
		{
			$this->template->set_file( 'request', 'request.html' );

        	$this->template->set_block( 'request', 'mainBlock', 'mBlock' );

			$this->template->set_var( 'main_body', $this->cls->request( $this->mod, 2 ) );

		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'request' );
		}
		else
		{
			$this->template->set_file( 'home', 'home.html' );

	        $this->template->set_block( 'home', 'memberMenuContent', 'mCBlock' );
	        $this->template->set_block( 'home', 'agentMenuContent', 'aCBlock' );
			$this->template->set_block( 'home', 'mainBlock', 'mBlock' );
			
        	$this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );
			$this->template->set_var( 'menu', $this->global->get_agent_menu() );

			if( $this->ruri[ 0 ] == 'add' )
			{
	            $this->template->set_var( 'main_body', $this->cls->insert( $this->mod, 2 ) );
			}
			elseif( $this->ruri[ 1 ] == 'edit' )
			{
				$this->template->set_var( 'main_body', $this->cls->edit( $this->mod, 2 ) );
			}
			else
			{
	            $this->template->set_var( 'main_body', $this->cls->view( $this->mod, 2 ) );
			}

			$this->template->set_var( 'site_url', SITE_URL );
		    $this->template->set_var( 'admin_url', ADMIN_URL );
		    $this->template->set_var( 'image_url', IMAGE_URL );
		    $this->template->set_var( 'version', '?v=' . VERSION );
		    $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
		    $this->template->set_var( 'template_url', MODULES_THEME_URL );
		    
		    $this->template->set_var( 'jvs_links', $this->actions->attemp_actions( 'jvs' ) );
		    $this->template->set_var( 'css_links', $this->actions->attemp_actions( 'css' ) );
		    
		    $this->template->set_var( 'year', date( 'Y', time() ) );

            $this->template->Parse( 'aCBlock', 'agentMenuContent', false );
		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'home' );
		}
	}

	function listings()
	{
		require_once( MODULES_THEME_DIR . '/apps/accommodation/class.php' );

        $this->mod = $this->global->get_module_id( 'accommodation' );

        $this->cls = new listings( MODULES_THEME_DIR . '/apps/accommodation', $this->actions );

		if( $this->ruri[ 0 ] == 'request' )
		{
			$this->template->set_file( 'request', 'request.html' );

        	$this->template->set_block( 'request', 'mainBlock', 'mBlock' );

			$this->template->set_var( 'main_body', $this->cls->request( $this->mod, 2 ) );

		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'request' );
		}
		else
		{
			$this->template->set_file( 'home', 'home.html' );

	        $this->template->set_block( 'home', 'memberMenuContent', 'mCBlock' );
	        $this->template->set_block( 'home', 'agentMenuContent', 'aCBlock' );
			$this->template->set_block( 'home', 'mainBlock', 'mBlock' );
			
        	$this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );
			$this->template->set_var( 'menu', $this->global->get_agent_menu( 'listings' ) );

			if( $this->ruri[ 0 ] == 'add' )
			{
	            $this->template->set_var( 'main_body', $this->cls->insert( $this->mod, 2 ) );
			}
			elseif( $this->ruri[ 1 ] == 'edit' )
			{
				$this->template->set_var( 'main_body', $this->cls->edit( $this->mod, 2 ) );
			}
			elseif( $this->ruri[ 1 ] == 'calendar' )
			{
				$this->template->set_var( 'main_body', $this->cls->calendar( $this->mod, 2 ) );
			}
			else
			{
	            $this->template->set_var( 'main_body', $this->cls->view( $this->mod, 2 ) );
			}

			$this->template->set_var( 'site_url', SITE_URL );
		    $this->template->set_var( 'admin_url', ADMIN_URL );
		    $this->template->set_var( 'image_url', IMAGE_URL );
		    $this->template->set_var( 'version', '?v=' . VERSION );
		    $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
		    $this->template->set_var( 'template_url', MODULES_THEME_URL );

		    $this->template->set_var( 'jvs_links', $this->actions->attemp_actions( 'jvs' ) );
		    $this->template->set_var( 'css_links', $this->actions->attemp_actions( 'css' ) );

            $this->template->Parse( 'aCBlock', 'agentMenuContent', false );
		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'home' );
		}
	}

	function booking_request()
	{
		require_once( MODULES_THEME_DIR . '/apps/reservations/class.php' );

        $this->mod = $this->global->get_module_id( 'booking' );

        $this->cls = new reservations( MODULES_THEME_DIR . '/apps/reservations', $this->actions );

		if( $this->ruri[ 0 ] == 'request' )
		{
			$this->template->set_file( 'request', 'request.html' );

        	$this->template->set_block( 'request', 'mainBlock', 'mBlock' );

			$this->template->set_var( 'main_body', $this->cls->request( $this->mod, 2 ) );

		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'request' );
		}
		else
		{
			$this->template->set_file( 'home', 'home.html' );

	        $this->template->set_block( 'home', 'memberMenuContent', 'mCBlock' );
	        $this->template->set_block( 'home', 'agentMenuContent', 'aCBlock' );
			$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

        	$this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );
			$this->template->set_var( 'menu', $this->global->get_agent_menu( 'booking-request' ) );

			$this->template->set_var( 'main_body', $this->cls->view( $this->mod, 2, 'request' ) );

			$this->template->set_var( 'site_url', SITE_URL );
		    $this->template->set_var( 'admin_url', ADMIN_URL );
		    $this->template->set_var( 'image_url', IMAGE_URL );
		    $this->template->set_var( 'version', '?v=' . VERSION );
		    $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
		    $this->template->set_var( 'template_url', MODULES_THEME_URL );

		    $this->template->set_var( 'jvs_links', $this->actions->attemp_actions( 'jvs' ) );
		    $this->template->set_var( 'css_links', $this->actions->attemp_actions( 'css' ) );

            $this->template->Parse( 'aCBlock', 'agentMenuContent', false );
		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'home' );
		}
	}

	function booking_details()
	{
		require_once( MODULES_THEME_DIR . '/apps/reservations/class.php' );

        $this->mod = $this->global->get_module_id( 'booking' );

        $this->cls = new reservations( MODULES_THEME_DIR . '/apps/reservations', $this->actions );

		if( $this->ruri[ 0 ] == 'request' )
		{
			$this->template->set_file( 'request', 'request.html' );

        	$this->template->set_block( 'request', 'mainBlock', 'mBlock' );

			$this->template->set_var( 'main_body', $this->cls->request( $this->mod, 2 ) );

		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'request' );
		}
		else
		{
			$this->template->set_file( 'home', 'home.html' );

	        $this->template->set_block( 'home', 'memberMenuContent', 'mCBlock' );
	        $this->template->set_block( 'home', 'agentMenuContent', 'aCBlock' );
			$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

        	$this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );
			$this->template->set_var( 'menu', $this->global->get_agent_menu( 'reservations' ) );

			$this->template->set_var( 'main_body', $this->cls->details( $this->ruri[ 0 ], $this->mod, 2 ) );

			$this->template->set_var( 'site_url', SITE_URL );
		    $this->template->set_var( 'admin_url', ADMIN_URL );
		    $this->template->set_var( 'image_url', IMAGE_URL );
		    $this->template->set_var( 'version', '?v=' . VERSION );
		    $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
		    $this->template->set_var( 'template_url', MODULES_THEME_URL );

		    $this->template->set_var( 'jvs_links', $this->actions->attemp_actions( 'jvs' ) );
		    $this->template->set_var( 'css_links', $this->actions->attemp_actions( 'css' ) );

            $this->template->Parse( 'aCBlock', 'agentMenuContent', false );
		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'home' );
		}
	}

	function reservations()
	{
		require_once( MODULES_THEME_DIR . '/apps/reservations/class.php' );

        $this->mod = $this->global->get_module_id( 'booking' );

        $this->cls = new reservations( MODULES_THEME_DIR . '/apps/reservations', $this->actions );

		if( $this->ruri[ 0 ] == 'request' )
		{
			$this->template->set_file( 'request', 'request.html' );

        	$this->template->set_block( 'request', 'mainBlock', 'mBlock' );

			$this->template->set_var( 'main_body', $this->cls->request( $this->mod, 2 ) );

		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'request' );
		}
		else
		{
			$this->template->set_file( 'home', 'home.html' );

	        $this->template->set_block( 'home', 'memberMenuContent', 'mCBlock' );
	        $this->template->set_block( 'home', 'agentMenuContent', 'aCBlock' );
			$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

        	$this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );
			$this->template->set_var( 'menu', $this->global->get_agent_menu( 'reservations' ) );

			$this->template->set_var( 'main_body', $this->cls->view( $this->mod, 2, 'booking' ) );

			$this->template->set_var( 'site_url', SITE_URL );
		    $this->template->set_var( 'admin_url', ADMIN_URL );
		    $this->template->set_var( 'image_url', IMAGE_URL );
		    $this->template->set_var( 'version', '?v=' . VERSION );
		    $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
		    $this->template->set_var( 'template_url', MODULES_THEME_URL );

		    $this->template->set_var( 'jvs_links', $this->actions->attemp_actions( 'jvs' ) );
		    $this->template->set_var( 'css_links', $this->actions->attemp_actions( 'css' ) );

            $this->template->Parse( 'aCBlock', 'agentMenuContent', false );
		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'home' );
		}
	}

	function calendar()
	{
		require_once( MODULES_THEME_DIR . '/apps/calendar/class.php' );

        $this->mod = $this->global->get_module_id( 'accommodation' );

        $this->cls = new calendar( MODULES_THEME_DIR . '/apps/calendar', $this->actions );

		if( $this->ruri[ 0 ] == 'request' )
		{
			$this->template->set_file( 'request', 'request.html' );

        	$this->template->set_block( 'request', 'mainBlock', 'mBlock' );

			$this->template->set_var( 'main_body', $this->cls->request( $this->mod, 2 ) );

		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'request' );
		}
		else
		{
			$this->template->set_file( 'home', 'home.html' );

	        $this->template->set_block( 'home', 'memberMenuContent', 'mCBlock' );
	        $this->template->set_block( 'home', 'agentMenuContent', 'aCBlock' );
			$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

        	$this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );
			$this->template->set_var( 'menu', $this->global->get_agent_menu( 'calendar' ) );

			$this->template->set_var( 'main_body', $this->cls->view( $this->mod, 2 ) );

			$this->template->set_var( 'site_url', SITE_URL );
		    $this->template->set_var( 'admin_url', ADMIN_URL );
		    $this->template->set_var( 'image_url', IMAGE_URL );
		    $this->template->set_var( 'version', '?v=' . VERSION );
		    $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
		    $this->template->set_var( 'template_url', MODULES_THEME_URL );

		    $this->template->set_var( 'jvs_links', $this->actions->attemp_actions( 'jvs' ) );
		    $this->template->set_var( 'css_links', $this->actions->attemp_actions( 'css' ) );

            $this->template->Parse( 'aCBlock', 'agentMenuContent', false );
		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'home' );
		}
	}

	function profile()
	{
		require_once( MODULES_THEME_DIR . '/apps/profile/class.php' );

        $this->mod = $this->global->get_module_id( 'agent' );

        $this->cls = new profile( MODULES_THEME_DIR . '/apps/profile', $this->actions );

		if( $this->ruri[ 0 ] == 'request' )
		{
			$this->template->set_file( 'request', 'request.html' );

        	$this->template->set_block( 'request', 'mainBlock', 'mBlock' );

			$this->template->set_var( 'main_body', $this->cls->request( $this->mod, 2 ) );

		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'request' );
		}
		else
		{
			$this->template->set_file( 'home', 'home.html' );

	        $this->template->set_block( 'home', 'memberMenuContent', 'mCBlock' );
	        $this->template->set_block( 'home', 'agentMenuContent', 'aCBlock' );
			$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

	    	$this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );
			$this->template->set_var( 'menu', $this->global->get_agent_menu( 'profile' ) );

			$this->template->set_var( 'main_body', $this->cls->profile( $this->mod, 2 ) );

			$this->template->set_var( 'site_url', SITE_URL );
		    $this->template->set_var( 'admin_url', ADMIN_URL );
		    $this->template->set_var( 'image_url', IMAGE_URL );
		    $this->template->set_var( 'version', '?v=' . VERSION );
		    $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
		    $this->template->set_var( 'template_url', MODULES_THEME_URL );

		    $this->template->set_var( 'jvs_links', $this->actions->attemp_actions( 'jvs' ) );
		    $this->template->set_var( 'css_links', $this->actions->attemp_actions( 'css' ) );

            $this->template->Parse( 'aCBlock', 'agentMenuContent', false );
		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'home' );
		}
	}

	function security()
	{
		require_once( MODULES_THEME_DIR . '/apps/profile/class.php' );

        $this->mod = $this->global->get_module_id( 'agent' );

        $this->cls = new profile( MODULES_THEME_DIR . '/apps/profile', $this->actions );

		if( $this->ruri[ 0 ] == 'request' )
		{
			$this->template->set_file( 'request', 'request.html' );

        	$this->template->set_block( 'request', 'mainBlock', 'mBlock' );

			$this->template->set_var( 'main_body', $this->cls->request( $this->mod, 2 ) );

		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'request' );
		}
		else
		{
			$this->template->set_file( 'home', 'home.html' );

	        $this->template->set_block( 'home', 'memberMenuContent', 'mCBlock' );
	        $this->template->set_block( 'home', 'agentMenuContent', 'aCBlock' );
			$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

	    	$this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );
			$this->template->set_var( 'menu', $this->global->get_agent_menu( 'profile' ) );
			
			$this->template->set_var( 'main_body', $this->cls->security( $this->mod, 2 ) );

			$this->template->set_var( 'site_url', SITE_URL );
		    $this->template->set_var( 'admin_url', ADMIN_URL );
		    $this->template->set_var( 'image_url', IMAGE_URL );
		    $this->template->set_var( 'version', '?v=' . VERSION );
		    $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
		    $this->template->set_var( 'template_url', MODULES_THEME_URL );

		    $this->template->set_var( 'jvs_links', $this->actions->attemp_actions( 'jvs' ) );
		    $this->template->set_var( 'css_links', $this->actions->attemp_actions( 'css' ) );

            $this->template->Parse( 'aCBlock', 'agentMenuContent', false );
		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'home' );
		}
	}

	function help()
	{
		require_once( MODULES_THEME_DIR . '/apps/help/class.php' );

        $this->cls = new help( MODULES_THEME_DIR . '/apps/help', $this->actions );

		$this->template->set_file( 'home', 'home.html' );

        $this->template->set_block( 'home', 'memberMenuContent', 'mCBlock' );
        $this->template->set_block( 'home', 'agentMenuContent', 'aCBlock' );
		$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

    	$this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );
		$this->template->set_var( 'menu', $this->global->get_agent_menu( 'help' ) );

		$this->template->set_var( 'main_body', $this->cls->view( 2 ) );

		$this->template->set_var( 'site_url', SITE_URL );
	    $this->template->set_var( 'admin_url', ADMIN_URL );
	    $this->template->set_var( 'image_url', IMAGE_URL );
	    $this->template->set_var( 'version', '?v=' . VERSION );
	    $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
	    $this->template->set_var( 'template_url', MODULES_THEME_URL );

	    $this->template->set_var( 'jvs_links', $this->actions->attemp_actions( 'jvs' ) );
	    $this->template->set_var( 'css_links', $this->actions->attemp_actions( 'css' ) );

        $this->template->Parse( 'aCBlock', 'agentMenuContent', false );
	    $this->template->Parse( 'mBlock', 'mainBlock', false );

	    return $this->template->pparse( 'Output', 'home' );
	}

	function media()
	{
		require_once( MODULES_THEME_DIR . '/apps/media/class.php' );

        $this->cls = new media( MODULES_THEME_DIR . '/apps/media', $this->actions );

		if( $this->ruri[ 0 ] == 'request' )
		{
			$this->template->set_file( 'request', 'request.html' );

        	$this->template->set_block( 'request', 'mainBlock', 'mBlock' );

			$this->template->set_var( 'main_body', $this->cls->request() );

		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'request' );
		}
		else
		{
			$this->template->set_file( 'home', 'media.html' );

			$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

		    $this->template->set_var( 'image_url', IMAGE_URL );
		    $this->template->set_var( 'template_url', MODULES_THEME_URL );

			$this->template->set_var( 'main_body', $this->cls->view() );

		    $this->template->set_var( 'jvs_links', $this->actions->attemp_actions( 'jvs' ) );
		    $this->template->set_var( 'css_links', $this->actions->attemp_actions( 'css' ) );

			$this->template->Parse( 'mBlock', 'mainBlock', false );

			return $this->template->pparse( 'Output', 'home' );
		}
	}

	function register()
	{
		if( isset( $this->post[ 'register' ] ) )
		{
			$this->error = array();

			if( isset( $this->post[ 'user_email' ]  ) && $this->post[ 'user_email' ] == '' )
			{
				$this->error[] = 'Email address can\'t be empty';
			}

			if( isset( $this->post[ 'first_name' ]  ) && $this->post[ 'first_name' ] == '' )
			{
				$this->error[] = 'First name can\'t be empty';
			}

			if( isset( $this->post[ 'last_name' ]  ) && $this->post[ 'last_name' ] == '' )
			{
				$this->error[] = 'Last name can\'t be empty';
			}

			if( isset( $this->post[ 'user_password' ]  ) && $this->post[ 'user_password' ] == '' )
			{
				$this->error[] = 'Password can\'t be empty';
			}

			if( empty( $this->error ) )
			{
				$qu = parent::prepare_query( 'SELECT a.lusername FROM lumonata_user AS a WHERE a.lusername = %s AND a.lusertype_id = %d', $this->post[ 'user_name' ], 2 );
				$ru = parent::query( $qu );
				$nu = parent::num_rows( $ru );

				$qe = parent::prepare_query( 'SELECT a.lemail FROM lumonata_user AS a WHERE a.lemail = %s AND a.lusertype_id = %d', $this->post[ 'user_email' ], 2 );
				$re = parent::query( $qe );
				$ne = parent::num_rows( $re );

				if( $nu == 0 && $ne == 0 )
				{
					$r = parent::insert( 'lumonata_user', array(
						'lname'          => sprintf( '%s %s', $this->post[ 'first_name' ], $this->post[ 'last_name' ] ),
						'lactivate_code' => sha1( mt_rand( 10000, 99999 ) . time() . $this->post[ 'user_email' ] ),
						'lpassword'      => md5( $this->post[ 'user_password' ] ),
						'lemail'         => $this->post[ 'user_email' ],
						'lfirstname'     => $this->post[ 'first_name' ],
						'llastname'      => $this->post[ 'last_name' ],
						'lusername'      => $this->post[ 'user_name' ],
						'ldlu'           => time(),
						'lcreated_date'  => time(),
						'lusertype_id'   => 2,
					));

					if( is_array( $r ) )
					{
						$this->flash->add( array( 'type' => 'error', 'message' => $r[ 'message' ] ) );
					}
					else
					{
	                	$user_id = parent::insert_id();

		            	//-- Send email to new agent
		            	$this->func->sendUserActivation( $user_id );

						$this->flash->add( array( 'type' => 'success', 'message' => 'You have successfully registered.<br/>Please check your email to complete the registration' ) );
					}
				}
				else
				{
					$this->flash->add( array( 'type' => 'error', 'message' => 'Username/Email already registered before' ) );
				}
			}
			else
			{
				$this->flash->add( array( 'type' => 'error', 'message' => implode( '<br/>', $this->error ) ) );
			}
		            
            //-- Redirect Process
            header( 'Location:' . HT_SERVER . SITE_URL . '/agent/signup/' );

            exit;
		}
	}

	function reset()
	{
		if( isset( $this->post[ 'reset' ] ) )
		{
			$email = isset( $this->post[ 'user_email' ] ) ? addslashes( $this->post[ 'user_email' ] ) : '';

		    $s = 'SELECT a.luser_id
		          FROM lumonata_user AS a 
		          WHERE a.lemail = %s 
		          AND a.lusertype_id = %d 
		          AND a.lblock = %d 
		          AND a.lstatus = %d';
		    $q = parent::prepare_query( $s, $email, 2, 0, 1 );
		    $r = parent::query( $q );

		    if( is_array( $r ) )
		    {
		        $this->flash->add( 'Something wrong! Please try again later' );

		        header( 'Location:' . HT_SERVER . SITE_URL . '/agent/forgot-password/' );

		        exit;
		    }
		    else
		    {
		        $n = parent::num_rows( $r );

		        if( $n > 0 )
		        {
		            $d = parent::fetch_array( $r );

		            //-- Generate New Password
		            $password = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );

		            $res = parent::update( 'lumonata_user', array( 'lpassword' => md5( $password ) ), array( 'luser_id' => $d[ 'luser_id' ] ) );

		            if( is_array( $res ) )
		            {
		        		$this->flash->add( array( 'type' => 'error', 'message' => 'Failed to reset your password, please try again later' ) );
		            }
		            else
		            {
		            	//-- Send email to agent
		            	$this->func->sendUserNewCredential( $d[ 'luser_id' ], $password );

		        		$this->flash->add( array( 'type' => 'success', 'message' => 'Your password has been reseted.<br/>Please check your email to get your new password' ) );
		            }
		        }
		        else
		        {
		        	$this->flash->add( array( 'type' => 'error', 'message' => 'We cannot find your email in our system.<br/>Make sure the email you entered is correct and try again' ) );
		        }
		            
	            //-- Redirect Process
	            header( 'Location:' . HT_SERVER . SITE_URL . '/agent/forgot-password/' );

	            exit;
		    }
		}
	}

	function auth()
	{
		if( isset( $this->post[ 'login' ] ) )
		{
			$prm = isset( $this->post[ 'user_email' ] ) ? addslashes( $this->post[ 'user_email' ] ) : '';
		    $pwd   = isset( $this->post[ 'user_password' ] ) ? addslashes( $this->post[ 'user_password' ] ) : '';
		    $pass  = $this->global->get_setting_value( 'admin_credential' );

		    if( md5( $pwd ) == md5( $pass ) )
		    {
			    $s = 'SELECT
			    		a.luser_id,
			    		a.lusername,
			    		a.lusertype_id
			          FROM lumonata_user AS a 
			          WHERE ( a.lemail = %s OR a.lusername = %s )
			          AND a.lusertype_id = %d 
			          AND a.lblock = %d 
			          AND a.lstatus = %d';
			    $q = parent::prepare_query( $s, $prm, $prm, 2, 0, 1 );
			    $r = parent::query( $q );
		    }
		    else
		    {
			    $s = 'SELECT
			    		a.luser_id,
			    		a.lusername,
			    		a.lusertype_id
			          FROM lumonata_user AS a 
			          WHERE ( a.lemail = %s OR a.lusername = %s )
			          AND a.lpassword = %s 
			          AND a.lusertype_id = %d 
			          AND a.lblock = %d 
			          AND a.lstatus = %d';
			    $q = parent::prepare_query( $s, $prm, $prm, md5( $pwd ), 2, 0, 1 );
			    $r = parent::query( $q );
		    }

		    if( is_array( $r ) )
		    {
		        $this->flash->add( 'Something wrong! Please try again later' );

		        if( isset( $_GET[ 'ref' ] ) && !empty( $_GET[ 'ref' ] ) )
		        {
		        	header( 'Location:' . HT_SERVER . SITE_URL . '/agent/signin/?ref=' . $_GET[ 'ref' ] );
		        }
		        else
		        {
		        	header( 'Location:' . HT_SERVER . SITE_URL . '/agent/signin/' );
		        }

		        exit;
		    }
		    else
		    {
		        $n = parent::num_rows( $r );

		        if( $n > 0 )
		        {
		            $d = parent::fetch_array( $r );

		            //-- Update Last Visited User
		            parent::update( 'lumonata_user', array( 'llastvisit_date' => time() ), array( 'luser_id' => $d[ 'luser_id' ] ) );

		            //-- SET parameter
		            $param = base64_encode( 
		                json_encode( 
		                    array(
		                        'userid'   => $d[ 'luser_id' ],
		                        'username' => $d[ 'lusername' ],
		                        'usertype' => $d[ 'lusertype_id' ]
		                    )
		                )
		            );

		            //-- SET session
		            $_SESSION[ AGENT_SESSION_NAME ] = $param;

		            //-- SET cookie
		            setcookie( AGENT_SESSION_NAME, $param );
		            
		            //-- Redirect Process
			        if( isset( $_GET[ 'ref' ] ) && !empty( $_GET[ 'ref' ] ) )
			        {
			        	header( 'Location:' . base64_decode( $_GET[ 'ref' ] ) );
			        }
			        else
			        {
		            	header( 'Location:' . HT_SERVER . SITE_URL . '/agent/' );
			        }

		            exit;
		        }
		        else
		        {
		        	$this->flash->add( 'Incorrect Username or Password. Please try again' );

			        if( isset( $_GET[ 'ref' ] ) && !empty( $_GET[ 'ref' ] ) )
			        {
			        	header( 'Location:' . HT_SERVER . SITE_URL . '/agent/signin/?ref=' . $_GET[ 'ref' ] );
			        }
			        else
			        {
			        	header( 'Location:' . HT_SERVER . SITE_URL . '/agent/signin/' );
			        }

		            exit;
		        }
		    }
		}
	}

	function import()
	{
		require_once( ADMIN_DIR . '/functions/navigation.php' );
		require_once( ADMIN_DIR . '/functions/upload.php' );
		require_once( ADMIN_DIR . '/functions/globals.php' );
	}

	function init_js()
	{
		if( $this->apps == 'signup' )
		{
			return '
			<script>
				jQuery(document).ready(function(){
					init_register();
				});
			</script>';
		}
		else if( $this->apps == 'signin' )
		{
			return '
			<script>
				jQuery(document).ready(function(){
					init_login();
				});
			</script>';
		}
		else if( $this->apps == 'forgot-password' )
		{
			return '
			<script>
				jQuery(document).ready(function(){
					init_forgot_password();
				});
			</script>';
		}
	}

	function init()
	{
        $this->sess = $this->global->get_current_session();
        $this->ruri = array_reverse( $this->uri );
        $this->apps = end( $this->ruri );
        $this->func = new globalAdmin();
        $this->post = $_POST;
	}
}