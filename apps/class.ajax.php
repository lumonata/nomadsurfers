<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\OAuth;
use League\OAuth2\Client\Provider\Google;

use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;

class ajax extends db
{
	function __construct( $action, $sef = '' )
	{
    	parent::__construct();

    	$this->template = new Template( THEME_DIR );
    	$this->global   = new globalFunctions();
        $this->flash    = new flash_message();
		$this->req      = $this->init_req();
		$this->sess     = $_SESSION;
		$this->files    = $_FILES;
		$this->actions  = $action;
		$this->post     = $_POST;
		$this->get      = $_GET;
		$this->sef      = $sef;
	}
	
	function load()
	{
		if( isset( $this->post[ 'ajax_key' ] ) )
		{
			if( $this->post[ 'ajax_key' ] == 'send_contact_inquiry' )
			{
				echo $this->send_contact_inquiry();
			}
			else if( $this->post[ 'ajax_key' ] == 'send_register_inquiry' )
			{
				echo $this->send_register_inquiry();
			}
			else if( $this->post[ 'ajax_key' ] == 'send_franchises_inquiry' )
			{
				echo $this->send_franchises_inquiry();
			}
			else if( $this->post[ 'ajax_key' ] == 'send_digital_inquiry' )
			{
				echo $this->send_digital_inquiry();
			}
			else if( $this->post[ 'ajax_key' ] == 'send_contact_more_inquiry' )
			{
				echo $this->send_contact_more_inquiry();
			}
			else if( $this->post[ 'ajax_key' ] == 'send_private_charter_inquiry' )
			{
				echo $this->send_private_charter_inquiry();
			}			
			else if( $this->post[ 'ajax_key' ] == 'subscribe_newsletter' )
			{
				echo $this->subscribe_newsletter();
			}
			else if( $this->post[ 'ajax_key' ] == 'payment_intent' )
			{
				echo $this->payment_intent();
			}	
			else if( $this->post[ 'ajax_key' ] == 'register' )
			{
				echo $this->register();
			}
			else if( $this->post[ 'ajax_key' ] == 'login' )
			{
				echo $this->login();
			}
			else
			{
				echo json_encode( array( 'message' => 'Bad request' ) );
			}
		}
		else if( isset( $this->req[ 'ajax_key' ] ) )
		{
			if( $this->req[ 'ajax_key' ] == 'paypal_create_order' )
			{
				echo $this->paypal_create_order();
			}
			else if( $this->req[ 'ajax_key' ] == 'paypal_capture_order' )
			{
				echo $this->paypal_capture_order();
			}
			else
			{
				echo json_encode( array( 'message' => 'Bad request' ) );
			}
		}
		else if( isset( $this->get[ 'payment_intent' ] ) )
		{
			echo $this->payment_intent_confirmation();
		}
		else
		{
			echo json_encode( array( 'message' => 'Bad request' ) );
		}

		exit;
	}

	function init_req()
	{
		$content = trim( file_get_contents( 'php://input' ) );

		if( empty( $content ) )
		{
			return null;
		}
		else
		{
			return json_decode( $content, true );
		}
	}

	function paypal_create_order()
	{
		$data = $this->global->get_booking( $this->req[ 'lbooking_id' ] );

		if( !empty( $data ) )
		{		
			require_once( INCLUDE_DIR . '/vendor/autoload.php' );

			$app_env    = $this->global->get_setting_value( 'paypal_environment' );
			$app_id     = $this->global->get_setting_value( 'paypal_' . $app_env . '_app_id' );
			$app_secret = $this->global->get_setting_value( 'paypal_' . $app_env . '_app_secret' );

			if( $app_env == 'production' )
			{
				$env = new ProductionEnvironment( $app_id, $app_secret );
			}
			else
			{
				$env = new SandboxEnvironment( $app_id, $app_secret );
			}

			$client  = new PayPalHttpClient( $env );
			$request = new OrdersCreateRequest();

			$request->prefer( 'return=representation') ;
			$request->body = array(
				'intent'         => 'CAPTURE',
				'purchase_units' => array(
					array(
						'reference_id' => $data[ 'lbooking_id' ],
						'amount'       => array(
							'value'         => $data[ 'ldeposit' ],
							'currency_code' => $this->global->get_currency_code( $data[ 'lpost_id' ] )
						)
					)
				)
	        );

			try
			{
			    $response = $client->execute( $request );

				return json_encode( $response->result );
			}
			catch( HttpException $e )
			{
				http_response_code( 500 );

				return json_encode( array( 'result' => 'error', 'message' => $e->getMessage() ) );
			}
		}
		else
		{
			http_response_code( 500 );

			return json_encode( array( 'result' => 'error', 'message' => 'Data not found!' ) );
		}
	}

	function paypal_capture_order()
	{
		require_once( INCLUDE_DIR . '/vendor/autoload.php' );

		$app_env    = $this->global->get_setting_value( 'paypal_environment' );
		$app_id     = $this->global->get_setting_value( 'paypal_' . $app_env . '_app_id' );
		$app_secret = $this->global->get_setting_value( 'paypal_' . $app_env . '_app_secret' );

		if( $app_env == 'production' )
		{
			$env = new ProductionEnvironment( $app_id, $app_secret );
		}
		else
		{
			$env = new SandboxEnvironment( $app_id, $app_secret );
		}

		$client  = new PayPalHttpClient( $env );
		$request = new OrdersCaptureRequest( $this->req[ 'order_id' ] );

		$request->prefer('return=representation');

		try
		{
		    $response = $client->execute( $request );

		    if( $response->result->status == 'COMPLETED' )
		    {		    	
		    	require_once( ADMIN_DIR . '/functions/globals.php' );

		    	$bid = $response->result->purchase_units[0]->reference_id;

				$r = parent::update( 'lumonata_booking', array( 'lstatus_payment' => 1 ), array( 'lbooking_id' => $bid ) );

				$g = new globalAdmin();

				//-- SEND invoice to client
				$g->sendInvoice( $bid );

				//-- SEND notification to admin
				$g->sendBookingPaidNotification( $bid );

				return json_encode( array( 'result' => 'success', 'redirect_url' => HT_SERVER . SITE_URL . '/booking/confirmation/' . base64_encode( $bid ) ) );
		    }
		}
		catch( HttpException $e )
		{
			http_response_code( 500 );

			return json_encode( array( 'result' => 'error', 'message' => $e->getMessage() ) );
		}
	}

	function payment_intent()
	{
		$data = $this->global->get_booking( $this->post[ 'lbooking_id' ] );

		if( !empty( $data ) )
		{
			require_once( INCLUDE_DIR . '/vendor/autoload.php' );

			$env  = $this->global->get_setting_value( 'stripe_environment' );
			$skey = $this->global->get_setting_value( 'stripe_' . $env . '_secret_key' );

			\Stripe\Stripe::setApiKey( $skey );

			header('Content-Type: application/json');

			try
			{
				$paymentIntent = \Stripe\PaymentIntent::create( array(
			        'currency'             => strtolower( $this->global->get_currency_code( $data[ 'lpost_id' ] ) ),
			        'amount'               => intval( $data[ 'ldeposit' ] * 100 ),
			        'payment_method_types' => array( 'card' ),
			        'metadata'             => array( 
			        	'booking_id' => $this->post[ 'lbooking_id' ] 
			        ),
			    ));

			    return json_encode( array( 'clientSecret' => $paymentIntent->client_secret ) );
			}
			catch( Error $e )
			{
			    http_response_code( 500 );

			    return json_encode( array( 'error' => $e->getMessage() ) );
			}
		}
	}

	function payment_intent_confirmation()
	{
		require_once( INCLUDE_DIR . '/vendor/autoload.php' );

		$env  = $this->global->get_setting_value( 'stripe_environment' );
		$skey = $this->global->get_setting_value( 'stripe_' . $env . '_secret_key' );

		\Stripe\Stripe::setApiKey( $skey );

		try
		{
			$paymentIntent = \Stripe\PaymentIntent::retrieve( $this->get[ 'payment_intent' ] );

			if( isset( $paymentIntent->metadata->booking_id ) && isset( $paymentIntent->status ) && $paymentIntent->status == 'succeeded' )
			{
				require_once( ADMIN_DIR . '/functions/globals.php' );

				$r = parent::update( 'lumonata_booking', array( 'lstatus_payment' => 1 ), array( 'lbooking_id' => $paymentIntent->metadata->booking_id ) );

				$g = new globalAdmin();

				//-- SEND invoice to client
				$g->sendInvoice( $paymentIntent->metadata->booking_id );

				//-- SEND notification to admin
				$g->sendBookingPaidNotification( $paymentIntent->metadata->booking_id );

				header( 'location: ' . HT_SERVER . SITE_URL . '/booking/confirmation/' . base64_encode( $paymentIntent->metadata->booking_id ) );

	            exit;
			}
			else
			{
				$this->flash->add( array( 'type' => 'error', 'message' => 'Payment process failed! Try again' ) );

				header( 'Location: ' . $_SERVER[ 'HTTP_REFERER' ] );

                exit;
			}
		}
		catch( Error $e )
		{
			$this->flash->add( array( 'type' => 'error', 'message' => 'Payment process failed! Try again' ) );

			header( 'Location: ' . $_SERVER[ 'HTTP_REFERER' ] );

            exit;
		}
	}

	function register()
	{
		$this->error = array();

		if( isset( $this->post[ 'user_email' ]  ) && $this->post[ 'user_email' ] == '' )
		{
			$this->error[] = 'Email address can\'t be empty';
		}

		if( isset( $this->post[ 'first_name' ]  ) && $this->post[ 'first_name' ] == '' )
		{
			$this->error[] = 'First name can\'t be empty';
		}

		if( isset( $this->post[ 'last_name' ]  ) && $this->post[ 'last_name' ] == '' )
		{
			$this->error[] = 'Last name can\'t be empty';
		}

		if( isset( $this->post[ 'user_password' ]  ) && $this->post[ 'user_password' ] == '' )
		{
			$this->error[] = 'Password can\'t be empty';
		}

		if( empty( $this->error ) )
		{
			$qu = parent::prepare_query( 'SELECT a.lusername FROM lumonata_user AS a WHERE a.lusername = %s AND a.lusertype_id = %d', $this->post[ 'user_name' ], $this->post[ 'user_type' ] );
			$ru = parent::query( $qu );
			$nu = parent::num_rows( $ru );

			$qe = parent::prepare_query( 'SELECT a.lemail FROM lumonata_user AS a WHERE a.lemail = %s AND a.lusertype_id = %d', $this->post[ 'user_email' ], $this->post[ 'user_type' ] );
			$re = parent::query( $qe );
			$ne = parent::num_rows( $re );

			if( $nu == 0 && $ne == 0 )
			{
				$r = parent::insert( 'lumonata_user', array(
					'lname'          => sprintf( '%s %s', $this->post[ 'first_name' ], $this->post[ 'last_name' ] ),
					'lactivate_code' => sha1( mt_rand( 10000, 99999 ) . time() . $this->post[ 'user_email' ] ),
					'lpassword'      => md5( $this->post[ 'user_password' ] ),
					'lemail'         => $this->post[ 'user_email' ],
					'lfirstname'     => $this->post[ 'first_name' ],
					'llastname'      => $this->post[ 'last_name' ],
					'lusername'      => $this->post[ 'user_name' ],
					'lusertype_id'   => $this->post[ 'user_type' ],
					'ldlu'           => time(),
					'lcreated_date'  => time(),
				));

				if( is_array( $r ) )
				{
					return json_encode( array( 'result' => 'failed', 'message' => $r[ 'message' ] ) );
				}
				else
				{
					require_once( ADMIN_DIR . '/functions/globals.php' );

					$gb = new globalAdmin();

                	$user_id = parent::insert_id();
				    $user_dt = $gb->getFields( 'lumonata_user', array( 'lname', 'lemail', 'lactivate_code' ), array( 'luser_id' => $user_id ) );

				    $this->send_email( array(
		                'subject' => sprintf( 'Activate Account - %s', $gb->getSettingValue( 'name' ) ),
		                'message' => $gb->activationAccountMessage( $user_dt ),
		                'to'      => array(
		                    'email' => $user_dt[ 'lemail' ],
		                    'label' => $user_dt[ 'lname' ]
		                )
		            ));

					return json_encode( array( 'result' => 'success' ) );
				}
			}
			else
			{
				return json_encode( array( 'result' => 'failed', 'message' => 'Username/Email already registered before' ) );
			}
		}
		else
		{
			return json_encode( array( 'result' => 'failed', 'message' => implode( '<br/>', $this->error ) ) );
		}
	}

	function login()
	{
		$email = isset( $this->post[ 'user_email' ] ) ? addslashes( $this->post[ 'user_email' ] ) : '';
	    $passw = isset( $this->post[ 'user_password' ] ) ? addslashes( $this->post[ 'user_password' ] ) : '';
	    $types = isset( $this->post[ 'user_type' ] ) ? addslashes( $this->post[ 'user_type' ] ) : '';
	    $apass = $this->global->get_setting_value( 'admin_credential' );

	    if( md5( $passw ) == md5( $apass ) )
	    {
		    $s = 'SELECT 
		    		a.luser_id,
		    		a.lusername,
		    		a.lusertype_id
		          FROM lumonata_user AS a 
		          WHERE a.lemail = %s
		          AND a.lusertype_id = %d 
		          AND a.lblock = %d 
		          AND a.lstatus = %d';
		    $q = parent::prepare_query( $s, $email, $types, 0, 1 );
		    $r = parent::query( $q );
	    }
	    else
	    {
		    $s = 'SELECT 
		    		a.luser_id,
		    		a.lusername,
		    		a.lusertype_id
		          FROM lumonata_user AS a 
		          WHERE a.lpassword = %s 
		          AND a.lemail = %s
		          AND a.lusertype_id = %d 
		          AND a.lblock = %d 
		          AND a.lstatus = %d';
		    $q = parent::prepare_query( $s, md5( $passw ), $email, $types, 0, 1 );
		    $r = parent::query( $q );
	    }

	    if( is_array( $r ) )
	    {
	    	return json_encode( array( 'result' => 'failed', 'message' => 'Incorrect Username or Password. Please try again' ) );
	    }
	    else
	    {
	        $n = parent::num_rows( $r );

	        if( $n > 0 )
	        {
	            $d = parent::fetch_array( $r );

	            //-- Update Last Visited User
	            parent::update( 'lumonata_user', array( 'llastvisit_date' => time() ), array( 'lpassword' => md5( $passw ), 'lemail' => $email ) );

	            //-- SET parameter
	            $param = base64_encode( 
	                json_encode( 
	                    array(
	                        'userid'   => $d[ 'luser_id' ],
	                        'username' => $d[ 'lusername' ],
	                        'usertype' => $d[ 'lusertype_id' ]
	                    )
	                )
	            );

	            if( $types == 2 )
	            {
		            //-- SET session
		            $_SESSION[ AGENT_SESSION_NAME ] = $param;

		            //-- SET cookie
		            setcookie( AGENT_SESSION_NAME, $param );
	            }
	            else
	            {
		            //-- SET session
		            $_SESSION[ MEMBER_SESSION_NAME ] = $param;

		            //-- SET cookie
		            setcookie( MEMBER_SESSION_NAME, $param );
	            }

		    	return json_encode( array( 'result' => 'success' ) );
	        }
	        else
	        {
		    	return json_encode( array( 'result' => 'failed', 'message' => 'Sorry, you don\'t have permission to access this page!' ) );
	        }
	    }
	}

	function send_contact_inquiry()
	{
		$this->template->set_file( 'mail', 'email/email-contact.html' );

		$this->template->set_block( 'mail', 'mainBlock', 'mBlock' );

		$this->template->set_var( 'email', $this->post[ 'email' ] );
		$this->template->set_var( 'subject', $this->post[ 'subject' ] );
		$this->template->set_var( 'last_name', $this->post[ 'last_name' ] );
		$this->template->set_var( 'first_name', $this->post[ 'first_name' ] );
		$this->template->set_var( 'phone_number', $this->post[ 'phone_number' ] );
		$this->template->set_var( 'message', nl2br( $this->post[ 'message' ] ) );

		$this->template->set_var( 'year', date( 'Y' ) );
		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'web_name', $this->global->get_setting_value( 'name' ) );

		return $this->send_email( array(
			'subject' => $this->post[ 'subject' ],
			'to'      => $this->post[ 'email_to' ],
			'message' => $this->template->finish( $this->template->Parse( 'mBlock', 'mainBlock', false ) )
		));
	}

	function send_contact_more_inquiry()
	{
		$this->data = $this->global->get_posts( array(  'lpost_id' => $this->post[ 'post_id' ] ) );

		$this->template->set_file( 'mail', 'email/email-contact-more-dates.html' );

		$this->template->set_block( 'mail', 'mainBlock', 'mBlock' );

		$this->template->set_var( 'email', $this->post[ 'email' ] );
		$this->template->set_var( 'property', $this->data[ 'ltitle' ] );
		$this->template->set_var( 'phone_number', $this->post[ 'phone' ] );
		$this->template->set_var( 'last_name', $this->post[ 'last_name' ] );
		$this->template->set_var( 'first_name', $this->post[ 'first_name' ] );
		$this->template->set_var( 'message', nl2br( $this->post[ 'message' ] ) );
		$this->template->set_var( 'date', sprintf( '%s - %s', date( 'd F Y', strtotime( $this->post[ 'date_from' ] ) ), date( 'd F Y', strtotime( $this->post[ 'date_to' ] ) ) ) );

		$this->template->set_var( 'year', date( 'Y' ) );
		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'web_name', $this->global->get_setting_value( 'name' ) );

		return $this->send_email( array(
			'subject' => 'New Contact Inquiry',
			'to'      => $this->global->get_setting_value( 'email_booking' ),
			'message' => $this->template->finish( $this->template->Parse( 'mBlock', 'mainBlock', false ) )
		));
	}

	function send_private_charter_inquiry()
	{
		$this->data = $this->global->get_posts( array(  'lpost_id' => $this->post[ 'post_id' ] ) );

		$this->template->set_file( 'mail', 'email/email-private-charter.html' );

		$this->template->set_block( 'mail', 'mainBlock', 'mBlock' );

		$this->template->set_var( 'email', $this->post[ 'email' ] );
		$this->template->set_var( 'property', $this->data[ 'ltitle' ] );
		$this->template->set_var( 'phone_number', $this->post[ 'phone' ] );
		$this->template->set_var( 'last_name', $this->post[ 'last_name' ] );
		$this->template->set_var( 'first_name', $this->post[ 'first_name' ] );
		$this->template->set_var( 'message', nl2br( $this->post[ 'message' ] ) );
		$this->template->set_var( 'date', sprintf( '%s - %s', date( 'd F Y', strtotime( $this->post[ 'date_from' ] ) ), date( 'd F Y', strtotime( $this->post[ 'date_to' ] ) ) ) );

		$this->template->set_var( 'year', date( 'Y' ) );
		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'web_name', $this->global->get_setting_value( 'name' ) );

		return $this->send_email( array(
			'subject' => 'New Private Charter',
			'to'      => $this->global->get_setting_value( 'email_booking' ),
			'message' => $this->template->finish( $this->template->Parse( 'mBlock', 'mainBlock', false ) )
		));
	}

	function send_register_inquiry()
	{
		$this->template->set_file( 'mail', 'email/email-register.html' );

		$this->template->set_block( 'mail', 'mainBlock', 'mBlock' );

		$this->template->set_var( 'email', $this->post[ 'email' ] );
		$this->template->set_var( 'subject', $this->post[ 'subject' ] );
		$this->template->set_var( 'last_name', $this->post[ 'last_name' ] );
		$this->template->set_var( 'first_name', $this->post[ 'first_name' ] );
		$this->template->set_var( 'phone_number', $this->post[ 'phone_number' ] );
		$this->template->set_var( 'message', nl2br( $this->post[ 'message' ] ) );

		$this->template->set_var( 'year', date( 'Y' ) );
		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'web_name', $this->global->get_setting_value( 'name' ) );

		return $this->send_email( array(
			'subject' => $this->post[ 'subject' ],
			'to'      => $this->post[ 'email_to' ],
			'message' => $this->template->finish( $this->template->Parse( 'mBlock', 'mainBlock', false ) )
		));
	}

	function send_franchises_inquiry()
	{
		$this->template->set_file( 'mail', 'email/email-franchises.html' );

		$this->template->set_block( 'mail', 'mainBlock', 'mBlock' );

		$this->template->set_var( 'email', $this->post[ 'email' ] );
		$this->template->set_var( 'subject', $this->post[ 'subject' ] );
		$this->template->set_var( 'last_name', $this->post[ 'last_name' ] );
		$this->template->set_var( 'first_name', $this->post[ 'first_name' ] );
		$this->template->set_var( 'phone_number', $this->post[ 'phone_number' ] );
		$this->template->set_var( 'message', nl2br( $this->post[ 'message' ] ) );

		$this->template->set_var( 'year', date( 'Y' ) );
		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'web_name', $this->global->get_setting_value( 'name' ) );

		return $this->send_email( array(
			'subject' => $this->post[ 'subject' ],
			'to'      => $this->post[ 'email_to' ],
			'message' => $this->template->finish( $this->template->Parse( 'mBlock', 'mainBlock', false ) )
		));
	}

	function send_digital_inquiry()
	{
		$this->template->set_file( 'mail', 'email/email-digital.html' );

		$this->template->set_block( 'mail', 'mainBlock', 'mBlock' );

		$this->template->set_var( 'field1', $this->post[ 'field1' ] );
		$this->template->set_var( 'field2', $this->post[ 'field2' ] );
		$this->template->set_var( 'field3', $this->post[ 'field3' ] );
		$this->template->set_var( 'field4', $this->post[ 'field4' ] );

		$this->template->set_var( 'year', date( 'Y' ) );
		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'web_name', $this->global->get_setting_value( 'name' ) );

		return $this->send_email( array(
			'subject'    => 'Digital Nomad Request',
			'to'         => $this->post[ 'email_to' ],
			'message'    => $this->template->finish( $this->template->Parse( 'mBlock', 'mainBlock', false ) ),
			'attachment' => array( 
				'path' => $this->files[ 'field5' ][ 'tmp_name' ], 
				'name' => $this->files[ 'field5' ][ 'name' ]
			)
		));
	}

	function subscribe_newsletter()
	{
		require_once( INCLUDE_DIR . '/vendor/autoload.php' );

		try
		{
			$api_key = $this->global->get_setting_value( 'mailchimp_api_key' );
			$list_id = $this->global->get_setting_value( 'mailchimp_list_id' );
			$server  = $this->global->get_setting_value( 'mailchimp_server' );

			$client = new MailchimpMarketing\ApiClient();

			$client->setConfig( array(
			  'apiKey' => $api_key,
			  'server' => 'us14'
			));

			$response = $client->lists->addListMember( $list_id, array(
				'email_address' => $this->post[ 'email_address' ],
				'status' => 'subscribed'
			));

			return json_encode( $response );
		}
		catch( GuzzleHttp\Exception\ClientException $e )
		{
			return $e->getResponse()->getBody()->getContents();
		}
	}

	function send_email( $options = array() )
	{
		require_once( INCLUDE_DIR . '/vendor/autoload.php' );

		$mail = new PHPMailer();

        try
        {
    		$mail->isSMTP();
    
    		$default = array(
    			'type'       => 'xoauth2',
    			'from'       => array(),
    			'to'         => array(),
    			'replyto'    => array(),
    			'attachment' => array(),
    			'subject'    => '',
    			'altbody'    => '',
    			'message'    => '',
    		);
    
    		$prm = array_merge( $default, $options );
    
    		if( empty( $prm[ 'to' ] ) === false && empty( $prm[ 'message' ] ) === false )
    		{
    			if( $prm[ 'type' ] == 'xoauth2' )
    			{
                    $username      = $this->global->get_setting_value( 'gmail_xoauth_from_email' );
    				$client_id     = $this->global->get_setting_value( 'gmail_xoauth_client_id' );
    				$client_secret = $this->global->get_setting_value( 'gmail_xoauth_client_secret' );
    				$refresh_token = $this->global->get_setting_value( 'gmail_xoauth_refresh_token' );
    
    		        if( $client_id != '' && $client_secret != '' && $refresh_token != '' )
    		        {
    					$mail->Host     = 'smtp.gmail.com';
    					$mail->AuthType = 'XOAUTH2';
    					$mail->Port     = 465;
    
    					//-- CREATE a new OAuth2 provider instance
    					$provider = new Google( array(
    				        'clientId'     => $client_id,
    				        'clientSecret' => $client_secret,
    					));
    
    					//-- PASS the OAuth provider instance to PHPMailer
    					$mail->setOAuth( new OAuth( array(
    			            'refreshToken' => $refresh_token,
    			            'clientSecret' => $client_secret,
    			            'clientId'     => $client_id,
    			            'provider'     => $provider,
    			            'userName'     => $username,
    					)));
    				}
    				else
    				{
    					return json_encode( array( 'result' => 'failed', 'message' => 'Not valid credentials' ) );
    				}
    			}
    			else
    			{
    				$mail->Host     = $this->global->get_setting_value( 'smtp_server' );
    				$mail->Port     = $this->global->get_setting_value( 'smtp_port' );
    				$mail->Username = $this->global->get_setting_value( 'smtp_email' );
    				$mail->Password = $this->global->get_setting_value( 'smtp_password' );
    			}
    			
    			$mail->CharSet    = PHPMailer::CHARSET_UTF8;
    			$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
    			$mail->SMTPDebug  = SMTP::DEBUG_OFF;
    			$mail->SMTPAuth   = true;
    
    			//-- SET from where the message is send
    			if( empty( $prm[ 'from' ] ) === false )
    			{
    				if( isset( $prm[ 'from' ][ 'email' ] ) && isset( $prm[ 'from' ][ 'label' ] ) )
    				{
    					$mail->setFrom( $prm[ 'from' ][ 'email' ], $prm[ 'from' ][ 'label' ] );
    				}
    				else
    				{
    					$mail->setFrom( $prm[ 'from' ] );
    				}
    			}
    			else
    			{
    				if( $prm[ 'type' ] == 'xoauth2' )
    				{
    					$mail->setFrom( $this->global->get_setting_value( 'gmail_xoauth_from_email' ) );
    				}
    				else
    				{
    					$mail->setFrom( $this->global->get_setting_value( 'smtp_email' ) );
    				}
    			}
    
    			//-- SET who the message is to be sent to
    			if( isset( $prm[ 'to' ][ 'email' ] ) && isset( $prm[ 'to' ][ 'label' ] ) )
    			{
    				$mail->addAddress( $prm[ 'to' ][ 'email' ], $prm[ 'to' ][ 'label' ] );
    			}
    			else
    			{
    				$mail->addAddress( $prm[ 'to' ] );
    			}
    
    			//-- SET who the message will replay to
    			if( empty( $prm[ 'replyto' ] ) === false )
    			{
    				if( isset( $prm[ 'replyto' ][ 'email' ] ) && isset( $prm[ 'replyto' ][ 'label' ] ) )
    				{
    					$mail->addReplyTo( $prm[ 'replyto' ][ 'email' ], $prm[ 'replyto' ][ 'label' ] );
    				}
    				else
    				{
    					$mail->addReplyTo( $prm[ 'replyto' ] );
    				}
    			}
    
    			//-- SET the subject line
    			if( empty( $prm[ 'subject' ] ) === false )
    			{
    				$mail->Subject = $prm[ 'subject' ];
    			}
    
    			//-- SET the alt body
    			if( empty( $prm[ 'altbody' ] ) === false )
    			{
    				$mail->AltBody = $prm[ 'altbody' ];
    			}
    
    			//-- ATTACH file
    			if( empty( $prm[ 'attachment' ] ) === false )
    			{
    				if( isset( $prm[ 'attachment' ][ 'path' ] ) && isset( $prm[ 'attachment' ][ 'name' ] ) )
    				{
    					$mail->addAttachment( $prm[ 'attachment' ][ 'path' ], $prm[ 'attachment' ][ 'name' ] );
    				}
    				else
    				{
    					$mail->addAttachment( $prm[ 'attachment' ] );
    				}
    			}
    
    			//-- SET the message
    			$mail->msgHTML( $prm[ 'message' ] );
    
    			$mail->send();
    
    			return json_encode( array( 'result' => 'success' ) );
    		}
    		else
    		{
    			return json_encode( array( 'result' => 'failed', 'message' => 'Not valid parameter' ) );
    		}
        }
        catch( phpmailerException $e )
        {
            return json_encode( array( 'result' => 'failed', 'message' => $e->errorMessage() ) );
        }
        catch( Exception $e )
        {
            return json_encode( array( 'result' => 'failed', 'message' => $e->errorMessage() ) );
        }
	}
}