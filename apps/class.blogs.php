<?php

class blogs extends db
{
	function __construct( $action, $apps, $uri = array() )
	{
    	parent::__construct();

    	$this->template = new Template( THEME_DIR );
    	$this->global   = new globalFunctions();
		$this->actions  = $action;
		$this->apps     = $apps;
		$this->uri      = $uri;

		$this->init();
		$this->set_meta_desc();
		$this->set_meta_title();
	}
	
	function load()
	{
		if( in_array( $this->uri[ 0 ], array( 'archive', 'category', 'tag' ) ) || empty( $this->uri[ 0 ] ) || isset( $_GET[ 'page' ] ) )
		{
			return $this->archive();
		}
		else
		{
			return $this->detail();
		}
	}

	function archive()
	{
		$this->template->set_file( 'main', 'partials/archive-blog.html' );

		//-- SET trending post
		$this->init_trending_post();

		//-- SET recent post
		$this->init_recent_post();

		$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

		$arc = $this->global->get_setting_value();

		$this->template->set_var( 'post_title', $arc[ 'blog_archive_title' ] );
		$this->template->set_var( 'post_brief', nl2br( $arc[ 'blog_archive_brief_text' ] ) );

		$this->template->set_var( 'header_image', $this->global->get_attachment_url( $arc[ 'blog_archive_header_image' ], null, 462 ) );
		$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $arc[ 'blog_archive_header_image' ], 10, 10 ) );

		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'site_url', SITE_URL );

		$this->template->set_var( 'str_1', $this->global->translate( 'trending-articles', 'TRENDING ARTICLES', 3 ) );
		$this->template->set_var( 'str_2', $this->global->translate( 'posted-at', 'Posted at' ) );
		$this->template->set_var( 'str_3', $this->global->translate( 'author', 'Author' ) );
		$this->template->set_var( 'str_4', $this->global->translate( 'read-more', 'Read More' ) );
		$this->template->set_var( 'str_5', $this->global->translate( 'recent-articles', 'RECENT ARTICLES', 3 ) );
		$this->template->set_var( 'str_6', $this->global->translate( 'archives', 'ARCHIVES', 3 ) );
		$this->template->set_var( 'str_7', $this->global->translate( 'categories', 'CATEGORIES', 3 ) );
		$this->template->set_var( 'str_8', $this->global->translate( 'tags', 'TAGS', 3 ) );

        $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css' ) );
        $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js' ) );

		$this->actions->add_actions( 'jvs', $this->init_archive_js() );

		return $this->template->Parse( 'mlBlock', 'mainBlock', false );
	}

	function detail()
	{
		$this->template->set_file( 'main', 'partials/single-blog.html' );

		$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

		$this->template->set_var( 'post_id', $this->data[ 'lpost_id' ] );
		$this->template->set_var( 'post_title', $this->data[ 'ltitle' ] );
		$this->template->set_var( 'post_content', $this->data[ 'ldescription' ] );
		$this->template->set_var( 'post_brief', nl2br( $this->data[ 'blog_brief' ] ) );
		$this->template->set_var( 'post_nav_link', $this->get_nav_link( $this->data ) );
		$this->template->set_var( 'post_category', $this->get_category_link( $this->data ) );

		$this->template->set_var( 'header_image', $this->global->get_attachment_url( $this->data[ 'blog_image' ], null, 462 ) );
		$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $this->data[ 'blog_image' ], 10, 10 ) );
		
		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'site_url', SITE_URL );
		
		$this->actions->add_actions( 'jvs', $this->init_detail_js() );

		return $this->template->Parse( 'mlBlock', 'mainBlock', false );
	}

	function init_trending_post()
	{
		$this->template->set_block( 'main', 'trendingLoopBlock', 'trlBlock' );
		$this->template->set_block( 'main', 'trendingBlock', 'trBlock' );

		if( in_array( $this->uri[ 0 ], array( 'archive', 'category', 'tag' ) ) === false )
		{
			$data = $this->global->get_posts( array( 'ltype' => 'blog', 'lstatus' => 1, 'llang_id' => $this->lang_id ), false, array( 'lview' => 'DESC' ), 5 );

			if( empty( $data ) === false )
			{
				foreach( $data as $dt )
				{
					$this->template->set_var( 'title', $dt[ 'ltitle' ] );
					$this->template->set_var( 'permalink', $this->global->permalink( $dt ) );
					$this->template->set_var( 'date', date( 'F d, Y', $dt[ 'lcreated_date' ] ) );
					$this->template->set_var( 'author', $this->get_author( $dt[ 'lcreated_by' ] ) );
					$this->template->set_var( 'brief_text', $this->get_brief( $dt[ 'blog_brief' ] ) );

					$this->template->set_var( 'thumbnail_image', $this->global->get_attachment_url( $dt[ 'blog_image' ], null, 462 ) );
					$this->template->set_var( 'thumbnail_image_placeholder', $this->global->get_attachment_url( $dt[ 'blog_image' ], 10, 10 ) );

					$this->template->Parse( 'trlBlock', 'trendingLoopBlock', true );
				}

				$this->template->Parse( 'trBlock', 'trendingBlock', false );
			}
		}
	}

	function init_recent_post()
	{		
		$this->template->set_block( 'main', 'categoriesBlock', 'ctBlock' );
		$this->template->set_block( 'main', 'archiveBlock', 'arBlock' );
		$this->template->set_block( 'main', 'tagsBlock', 'tgBlock' );

		$this->template->set_block( 'main', 'recentLoopBlock', 'rclBlock' );
		$this->template->set_block( 'main', 'recentBlock', 'rcBlock' );

		if( isset( $_GET[ 'page' ] ) )
		{
			$page = $_GET[ 'page' ];
			$view = array( ( $page - 1 ) * 3, 3 );
		}
		else
		{
			$page = 1;
			$view = 3;
		}

		//-- GET recent post
		if( $this->uri[ 0 ] == 'archive' )
		{
			$prm = array( 'ltype' => 'blog', 'lstatus' => 1, 'llang_id' => $this->lang_id );

			if( isset( $this->uri[ 1 ] ) && !empty( $this->uri[ 1 ] ) )
			{
				$prm[ 'MONTHNAME(FROM_UNIXTIME(lcreated_date))' ] = $this->uri[ 1 ];
			}

			if( isset( $this->uri[ 2 ] ) && !empty( $this->uri[ 2 ] ) )
			{
				$prm[ 'YEAR(FROM_UNIXTIME(lcreated_date))' ] = $this->uri[ 2 ];
			}

			//-- REMOVE uri query
			$uri = $this->uri;

			if( isset( $_GET[ 'page' ] ) )
			{
				array_pop( $uri );
			}

			$all  = $this->global->get_posts( $prm, false, array( 'lcreated_date' => 'DESC' ) );
			$data = $this->global->get_posts( $prm, false, array( 'lcreated_date' => 'DESC' ), $view );
			$base = $this->global->archive_link( 'blogs', $uri );
		}
		else if( $this->uri[ 0 ] == 'category' )
		{
			$prm = array( 'b.ltype' => 'blog', 'c.lrule' => 'blog_category', 'b.lstatus' => 1, 'b.llang_id' => $this->lang_id );

			if( isset( $this->uri[ 1 ] ) && !empty( $this->uri[ 1 ] ) )
			{
				$prm[ 'c.lsef' ] = $this->uri[ 1 ];
			}

			//-- REMOVE uri query
			$uri = $this->uri;

			if( isset( $_GET[ 'page' ] ) )
			{
				array_pop( $uri );
			}

			$all  = $this->global->get_posts_relationship( $prm, false, array( 'b.lcreated_date' => 'DESC' ) );
			$data = $this->global->get_posts_relationship( $prm, false, array( 'b.lcreated_date' => 'DESC' ), $view );
			$base = $this->global->archive_link( 'blogs', $uri );
		}
		else if( $this->uri[ 0 ] == 'tag' )
		{
			$prm = array( 'b.ltype' => 'blog', 'c.lrule' => 'blog_tag', 'b.lstatus' => 1, 'b.llang_id' => $this->lang_id );

			if( isset( $this->uri[ 1 ] ) && !empty( $this->uri[ 1 ] ) )
			{
				$prm[ 'c.lsef' ] = $this->uri[ 1 ];
			}

			//-- REMOVE uri query
			$uri = $this->uri;

			if( isset( $_GET[ 'page' ] ) )
			{
				array_pop( $uri );
			}

			$all  = $this->global->get_posts_relationship( $prm, false, array( 'b.lcreated_date' => 'DESC' ) );
			$data = $this->global->get_posts_relationship( $prm, false, array( 'b.lcreated_date' => 'DESC' ), $view );
			$base = $this->global->archive_link( 'blogs', $uri );
		}
		else
		{
			$all  = $this->global->get_posts( array( 'ltype' => 'blog', 'lstatus' => 1, 'llang_id' => $this->lang_id ), false, array( 'lcreated_date' => 'DESC' ) );
			$data = $this->global->get_posts( array( 'ltype' => 'blog', 'lstatus' => 1, 'llang_id' => $this->lang_id ), false, array( 'lcreated_date' => 'DESC' ), $view );
			$base = $this->global->archive_link( 'blogs' );
		}

		if( empty( $data ) === false )
		{
			foreach( $data as $dt )
			{
				$this->template->set_var( 'title', $dt[ 'ltitle' ] );
				$this->template->set_var( 'permalink', $this->global->permalink( $dt ) );
				$this->template->set_var( 'date', date( 'F d, Y', $dt[ 'lcreated_date' ] ) );
				$this->template->set_var( 'author', $this->get_author( $dt[ 'lcreated_by' ] ) );
				$this->template->set_var( 'brief_text', $this->get_brief( $dt[ 'blog_brief' ] ) );

				$this->template->set_var( 'thumbnail_image', $this->global->get_attachment_url( $dt[ 'blog_image' ], null, 462 ) );
				$this->template->set_var( 'thumbnail_image_placeholder', $this->global->get_attachment_url( $dt[ 'blog_image' ], 10, 10 ) );

				$this->template->Parse( 'rclBlock', 'recentLoopBlock', true );
			}

			//-- BUILD pagination
			$count  = ceil( count( $all ) / 3 );
			$paging = $this->global->build_paging( $base, $page, $count, true );

			if( empty( $paging ) === false )
			{
				$paginglink = '
                <div class="blog-paged">';

                	foreach( $paging as $p )
                	{
                		if( $p[ 'type' ] == 'prevbtn' )
                		{
                			$paginglink .= '
                			<a href="' . $p[ 'link' ] . '" class="' . $p[ 'status' ] . '">
                				<img src="//' . THEME_URL . '/assets/img/arrow-black.svg" alt="">
                			</a>';
                		}
                		else if( $p[ 'type' ] == 'nextbtn' )
                		{
                			$paginglink .= '
                			<a href="' . $p[ 'link' ] . '" class="' . $p[ 'status' ] . '">
                				<img src="//' . THEME_URL . '/assets/img/arrow-black.svg" alt="">
                			</a>';
                		}
                		else
                		{
                			$paginglink .= '
                			<a href="' . $p[ 'link' ] . '" class="' . $p[ 'status' ] . '">
                				' . $p[ 'label' ] . 
                			'</a>';
                		}
                	}

                	$paginglink .= '
                </div>';

				$this->template->set_var( 'paging', $paginglink );
			}

			$this->template->Parse( 'rcBlock', 'recentBlock', false );
		}

		//-- GET archive link
		$archive = $this->get_archive();

		if( empty( $archive ) === false )
		{
			$alink = '';

			foreach( $archive as $dt )
			{
				$alink .= '
				<a href="' . $this->global->archive_link( 'blogs', array( 'archive', strtolower( $dt[ 'post_month' ] ), $dt[ 'post_year' ] ) ) . '" class="dot-list">
                    <img src="//' . THEME_URL . '/assets/img/bullet-black.svg">
                    <label>' . $dt[ 'post_archive' ] . '</label>
                </a>';
			}

			$this->template->set_var( 'archive_link', $alink );

			$this->template->Parse( 'arBlock', 'archiveBlock', false );
		}

		//-- GET caregories link
		$categories = $this->get_categories();

		if( empty( $categories ) === false )
		{
			$clink = '';

			foreach( $categories as $dt )
			{
				$clink .= '
				<a href="' . $this->global->term_link( $dt ) . '" class="dot-list">
                    <img src="//' . THEME_URL . '/assets/img/bullet-yellow.svg">
                    <label>' . $dt[ 'lname' ] . ' (' . $dt[ 'lnum' ] . ')</label>
                </a>';
			}

			$this->template->set_var( 'categories_link', $clink );

			$this->template->Parse( 'ctBlock', 'categoriesBlock', false );
		}

		//-- GET tags link
		$tags = $this->get_tags();

		if( empty( $tags ) === false )
		{
			$tlink = '';

			foreach( $tags as $dt )
			{
				$tlink .= '
                <a href="' . $this->global->term_link( $dt ) . '" class="box-tag">
                    <label>' . $dt[ 'lname' ] . '</label>
                </a>';
			}

			$this->template->set_var( 'tags_link', $tlink );

			$this->template->Parse( 'tgBlock', 'tagsBlock', false );
		}
	}

	function init()
	{
		$this->lang_id   = $this->global->get_current_language_id();
		$this->lang_code = $this->global->get_current_language_code();
		$this->lang_def  = $this->global->get_setting_value( 'llanguage' );

		if( isset( $this->uri[ 0 ] ) && empty( $this->uri[ 0 ] ) === false )
		{
			$this->data = $this->global->get_posts( array( 
				'ltype'    => 'blog',
				'lsef_url' => $this->uri[ 0 ],
				'lstatus'  => 1 
			));
		}
		else
		{
			$this->data = array();
		}
	}

	function init_detail_js()
	{
		return '
		<script>
			jQuery(document).ready(function(){
				init_lazy_load();
			});
		</script>';
	}

	function init_archive_js()
	{
		return '
		<script>
			jQuery(document).ready(function(){
				init_blog_slide();
				init_lazy_load();
			});
		</script>';
	}

	function get_archive()
	{
		$s = 'SELECT 
				DATE_FORMAT( 
					FROM_UNIXTIME( lcreated_date ), %s
				) AS post_archive,
				MONTHNAME( 
					FROM_UNIXTIME( lcreated_date )
				) AS post_month,
				YEAR( 
					FROM_UNIXTIME( lcreated_date )
				) AS post_year
			  FROM lumonata_post AS a
			  WHERE a.ltype = %s AND a.lstatus = %s AND a.llang_id = %d
			  GROUP BY post_archive ORDER BY lcreated_date DESC LIMIT 4';
        $q = parent::prepare_query( $s, '%M, %Y', 'blog', 1, $this->lang_id );
        $r = parent::query( $q );

        $data = array();

        if( !is_array( $r ) )
        {
        	while( $d = parent::fetch_assoc( $r ) )
        	{
        		$data[] = $d;
        	}
        }

        return $data;
	}

	function get_categories()
	{
		$s = 'SELECT
				a.lsef,
				a.lname,
				a.lrule,
				COUNT( a.lname ) AS lnum
			  FROM lumonata_post_terms AS a
			  RIGHT JOIN lumonata_post_relationship AS b ON b.lterm_id = a.lterm_id
			  LEFT JOIN lumonata_post AS c ON b.lpost_id = c.lpost_id
			  WHERE c.ltype = %s AND c.lstatus = %s AND a.lrule = %s AND a.llang_id = %d
			  GROUP BY a.lname ORDER BY lnum DESC';
        $q = parent::prepare_query( $s, 'blog', 1, 'blog_category', $this->lang_id );
        $r = parent::query( $q );

        $data = array();

        if( !is_array( $r ) )
        {
        	while( $d = parent::fetch_assoc( $r ) )
        	{
        		$data[] = $d;
        	}
        }

        return $data;
	}

	function get_tags()
	{
		$s = 'SELECT
				a.lsef,
				a.lname,
				a.lrule,
				COUNT( a.lname ) AS lnum
			  FROM lumonata_post_terms AS a
			  RIGHT JOIN lumonata_post_relationship AS b ON b.lterm_id = a.lterm_id
			  LEFT JOIN lumonata_post AS c ON b.lpost_id = c.lpost_id
			  WHERE c.ltype = %s AND c.lstatus = %s AND a.lrule = %s AND a.llang_id = %d
			  GROUP BY a.lname ORDER BY lnum DESC';
        $q = parent::prepare_query( $s, 'blog', 1, 'blog_tag', $this->lang_id );
        $r = parent::query( $q );

        $data = array();

        if( !is_array( $r ) )
        {
        	while( $d = parent::fetch_assoc( $r ) )
        	{
        		$data[] = $d;
        	}
        }

        return $data;
	}

	function get_brief( $brief = '' )
	{
		if( empty( $brief ) === false )
		{
			return sprintf( '<p>%s</p>', $brief );
		}
	}

	function get_author( $username = '' )
	{
		if( empty( $username ) === false )
		{
			$s = 'SELECT * FROM lumonata_user AS a WHERE a.lusername = %s';
            $q = parent::prepare_query( $s, $username );
            $r = parent::query( $q );
            $d = parent::fetch_array( $r );

            if( !empty( $d[ 'llastname' ] ) )
            {
                return $d[ 'llastname' ];
            }
            elseif( !empty( $d[ 'lfirstname' ] ) )
            {
                return $d[ 'lfirstname' ];
            }
            elseif( !empty( $d[ 'lname' ] ) )
            {
                return $d[ 'lname' ];
            }
            else
            {
            	return $username;
            }
		}

		return $username;
	}

	function get_nav_link( $data )
	{
		$pdata = $this->global->get_posts( array( 'ltype' => 'blog', 'lstatus' => 1, 'lcreated_date <' => $data[ 'lcreated_date' ] ), true, array( 'lcreated_date' => 'DESC' ), 1 );
		$ndata = $this->global->get_posts( array( 'ltype' => 'blog', 'lstatus' => 1, 'lcreated_date >' => $data[ 'lcreated_date' ] ), true, array( 'lcreated_date' => 'ASC' ), 1 );

		if( empty( $pdata ) === false )
		{
			$plink  = $this->global->permalink( $pdata );
			$pclass = 'bd-page-left';
		}
		else
		{
			$plink  = 'javascript:;';
			$pclass = 'bd-page-left disabled';
		}

		if( empty( $ndata ) === false )
		{
			$nlink  = $this->global->permalink( $ndata );
			$nclass = 'bd-page-right';
		}
		else
		{
			$nlink  = 'javascript:;';
			$nclass = 'bd-page-right disabled';
		}

		return '
        <a href="' . $plink . '" class="' . $pclass . '">
            <img src="//' . THEME_URL . '/assets/img/arrow-black.svg" alt="">
            <span>PREVIOUS</span>
        </a>
        <a href="' . $this->global->archive_link( 'blogs' ) . '">
            <img src="//' . THEME_URL . '/assets/img/grid.svg" class="bd-grid" alt="">
        </a>
        <a href="' . $nlink . '" class="' . $nclass . '">
            <span>NEXT</span>
            <img src="//' . THEME_URL . '/assets/img/arrow-black.svg" alt="">
        </a>';
	}

	function get_category_link( $data )
	{
		if( isset( $data[ 'blog_category' ] ) && !empty( $data[ 'blog_category' ] ) )
		{
			$terms = '
            <div class="bd-category">';

				foreach( $data[ 'blog_category' ] as $dt )
				{
	                $terms .= '<a href="' . $this->global->term_link( $dt ) . '">' . $dt[ 'lname' ] . '</a>';
				}

				$terms .= '
			</div>';

			return $terms;
		}
	}

	function set_meta_title()
	{
		$meta_title = $this->data[ 'lmeta_title' ];

		if( empty( $meta_title ) )
		{
			$meta_title = $this->global->get_setting_value( 'lmeta_title' );

			if( empty( $meta_title ) )
			{
				$meta_title = 'Blogs - ' . $this->global->get_setting_value( 'name' );
			}
		}
		
		$this->actions->add_actions( 'meta_title', $meta_title );
	}

	function set_meta_desc()
	{
		$meta_description = $this->global->get_setting_value( 'lmeta_desc' );

		if( empty( $meta_description ) === false )
		{
			$meta_description = sprintf( '<meta name="description" content="%s" />', strip_tags( $meta_description ) );

			$this->actions->add_actions( 'meta_description', $meta_description );
		}
	}
}

?>