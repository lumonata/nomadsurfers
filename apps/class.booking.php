<?php

class booking extends db
{
	function __construct( $action, $apps, $uri = array() )
	{
    	parent::__construct();

    	$this->template = new Template( THEME_DIR );
    	$this->global   = new globalFunctions();
        $this->flash    = new flash_message();
		$this->actions  = $action;
		$this->apps     = $apps;
		$this->uri      = $uri;

		$this->set_meta_title();
		$this->set_meta_desc();

		$this->set_meta_title();
		$this->set_meta_desc();

		$this->import();
		$this->init();
	}
	
	function load()
	{
		$this->template->set_file( 'main', 'partials/booking.html' );

		if( $this->cart->isEmpty() && !in_array( $this->uri[ 0 ], array( 'confirmation', 'payment' ) ) )
		{
			$this->template->set_block( 'main', 'emptyBlock', 'emBlock' );
			$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

			$email_booking = $this->global->get_setting_value( 'email_booking' );
			$email_link    = sprintf( '<a href="mailto:%s" target="_blank">%s</a>', $email_booking, $email_booking );
			$accom_link    = sprintf( '<a href="%s"><b>%s</b></a>', $this->global->site_url( 'accommodation' ), $this->global->translate( 'here', 'here' ) );

			$this->template->set_var( 'contact_form_link', 'javascript:;' );

			$this->template->set_var( 'str_2', $this->global->translate( 'guest', 'GUEST', 3 ) );
			$this->template->set_var( 'str_3', $this->global->translate( 'verify', 'VERIFY', 3 ) );
			$this->template->set_var( 'str_1', $this->global->translate( 'add-ons', 'ADD-ONS', 3 ) );
			$this->template->set_var( 'str_4', $this->global->translate( 'confirm', 'CONFIRM', 3 ) );
			$this->template->set_var( 'str_5', $this->global->translate( 'empty-cart', 'Empty Cart' ) );
			$this->template->set_var( 'str_7', $this->global->translate( 'contact-us', 'Contact Us' ) );
			$this->template->set_var( 'str_8', $this->global->translate( 'contact-form', 'Contact Form' ) );
			$this->template->set_var( 'str_9', $this->global->translate( 'booking-text-13', 'Have more questions related to your Booking Request or need help for planning your Surf Holiday?' ) );;
			$this->template->set_var( 'str_6', $this->global->translate( 'booking-text-12', sprintf( 'No item was found in your cart!<br/>Check our accommodation list %s', $accom_link ) ) );

			$this->template->set_var( 'str_10', $this->global->translate( 'booking-text-14', sprintf( 'Send us an email at %s or use the contact form below', $email_link ) ) );

			return $this->template->Parse( 'emBlock', 'emptyBlock', false );
		}
		else
		{
			$this->init_section_guests();
			$this->init_section_addons();
			$this->init_section_sidebar();
			$this->init_section_confirmation();
			$this->init_section_verification();
			$this->init_section_payment();
			
			$this->template->set_block( 'main', 'emptyBlock', 'emBlock' );
			$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

			$this->template->set_var( 'theme_url', THEME_URL );
			$this->template->set_var( 'site_url', SITE_URL );

			$this->actions->add_actions( 'jvs', $this->init_js() );

			return $this->template->Parse( 'mlBlock', 'mainBlock', false );
		}
	}

	function init_section_addons()
	{
		$this->template->set_block( 'main', 'sectionAddOnsLoopBlock', 'saolBlock' );
		$this->template->set_block( 'main', 'sectionAddOnsBlock', 'saoBlock' );

		if( $this->uri[ 0 ] == 'add-ons' || empty( $this->uri[ 0 ] ) )
		{
			$addons  = array();

			foreach( $this->cart->getItems() as $carts )
			{
				foreach( $carts as $cart )
				{
					$post_id = $cart[ 'id' ];

					$data = $this->add_ons_list( $post_id );

					if( empty( $data ) )
					{
						header( 'location: ' . $this->global->site_url( 'booking/guest' ) );

		                exit;
					}
					else
					{
						if( isset( $cart[ 'attributes' ][ 'addons' ] ) )
						{
							$addons = $cart[ 'attributes' ][ 'addons' ];
						}

						$this->template->set_var( 'post_id', $post_id );
						$this->template->set_var( 'nights', $cart[ 'quantity' ]  );

						foreach( $data as $d )
						{
							if( isset( $addons[ $d[ 'lterm_id' ] ] ) )
							{
								$this->template->set_var( 'addons_check', 'checked' );
								$this->template->set_var( 'addons_class', 'addons-overlay addons-overlay-selected' );
			            		$this->template->set_var( 'addons_num_option', $this->num_option( $addons[ $d[ 'lterm_id' ] ] ) );
							}
							else
							{						
								$this->template->set_var( 'addons_check', '' );
								$this->template->set_var( 'addons_class', 'addons-overlay' );
			            		$this->template->set_var( 'addons_num_option', $this->num_option() );
							}

							$this->template->set_var( 'addons_name', $d[ 'lname' ] );
			            	$this->template->set_var( 'addons_id', $d[ 'lterm_id' ] );
			            	$this->template->set_var( 'addons_brief', nl2br( $d[ 'accommodation_add_ons_brief' ] ) );
			            	$this->template->set_var( 'addons_img', $this->global->get_attachment_url( $d[ 'accommodation_add_ons_image' ], 300, 300 ) );

							$this->template->set_var( 'addons_price', $d[ 'addons_price' ] );
							$this->template->set_var( 'addons_format_price', $this->global->get_format_price( $d[ 'addons_price' ], $post_id ) );

							$this->template->Parse( 'saolBlock', 'sectionAddOnsLoopBlock', true );
						}

						$this->template->set_var( 'str_8', $this->global->translate( 'back', 'BACK', 3 ) );
						$this->template->set_var( 'str_9', $this->global->translate( 'next', 'NEXT', 3 ) );
						$this->template->set_var( 'str_2', $this->global->translate( 'guest', 'GUEST', 3 ) );
						$this->template->set_var( 'str_3', $this->global->translate( 'verify', 'VERIFY', 3 ) );
						$this->template->set_var( 'str_1', $this->global->translate( 'add-ons', 'ADD-ONS', 3 ) );
						$this->template->set_var( 'str_4', $this->global->translate( 'confirm', 'CONFIRM', 3 ) );
						$this->template->set_var( 'str_6', $this->global->translate( 'add-now', 'ADD NOW', 3 ) );
						$this->template->set_var( 'str_7', $this->global->translate( 'option-added', 'OPTION ADDED', 3 ) );
						$this->template->set_var( 'str_5', $this->global->translate( 'booking-text', 'Make you trip more comfortable with these add-ons!' ) );

				        $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css' ) );
				        $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js' ) );

						$this->template->Parse( 'saoBlock', 'sectionAddOnsBlock', false );
					}
				}
			}
		}
	}

	function init_section_guests()
	{
		$this->template->set_block( 'main', 'sectionGuestLoopBlock', 'sgelBlock' );
		$this->template->set_block( 'main', 'sectionGuestBlock', 'sgeBlock' );

		if( $this->uri[ 0 ] == 'guest' )
		{
			$list = array(
				'adult'  => $this->global->translate( 'adult', 'Adult' ),
				'child'  => $this->global->translate( 'child', 'Child' ),
				'infant' => $this->global->translate( 'baby', 'Baby' )
			);

			foreach( $this->cart->getItems() as $carts )
			{
				foreach( $carts as $cart )
				{
					if( isset( $cart[ 'attributes' ][ 'guests' ] ) && is_array( $cart[ 'attributes' ][ 'guests' ] ) )
					{
						foreach( $cart[ 'attributes' ][ 'guests' ] as $type => $guests )
						{
							foreach( $guests as $i => $guest )
							{
								$this->template->set_var( 'fieldset', $this->guest_field( $i, $type, $guest ) );
								$this->template->set_var( 'label', sprintf( '%s %s', $list[ $type ], $i + 1 ) );

								$this->template->Parse( 'sgelBlock', 'sectionGuestLoopBlock', true );
							}
						}
					}
					else
					{
						foreach( $list as $type => $label )
						{
							if( $cart[ 'attributes' ][ $type ] > 0 )
							{
								for( $i = 0; $i < $cart[ 'attributes' ][ $type ]; $i++ )
								{
									$this->template->set_var( 'fieldset', $this->guest_field( $i, $type ) );
									$this->template->set_var( 'label', sprintf( '%s %s', $label, $i + 1 ) );

									$this->template->Parse( 'sgelBlock', 'sectionGuestLoopBlock', true );
								}
							}
						}
					}

					$this->template->set_var( 'post_id', $cart[ 'id' ] );
					$this->template->set_var( 'nights', $cart[ 'quantity' ]  );
				}
			}

			$this->template->set_var( 'back_url', $this->global->site_url( 'booking/add-ons' ) );

			$this->template->set_var( 'str_6', $this->global->translate( 'back', 'BACK', 3 ) );
			$this->template->set_var( 'str_7', $this->global->translate( 'next', 'NEXT', 3 ) );
			$this->template->set_var( 'str_2', $this->global->translate( 'guest', 'GUEST', 3 ) );
			$this->template->set_var( 'str_3', $this->global->translate( 'verify', 'VERIFY', 3 ) );
			$this->template->set_var( 'str_1', $this->global->translate( 'add-ons', 'ADD-ONS', 3 ) );
			$this->template->set_var( 'str_4', $this->global->translate( 'confirm', 'CONFIRM', 3 ) );
			$this->template->set_var( 'str_5', $this->global->translate( 'guests-information', 'Guests Information' ) );

			$this->template->Parse( 'sgeBlock', 'sectionGuestBlock', false );
		}
	}

	function init_section_verification()
	{
		$this->template->set_block( 'main', 'sectionVerificationLoopBlock', 'svflBlock' );
		$this->template->set_block( 'main', 'sectionVerificationBlock', 'svfBlock' );

		if( $this->uri[ 0 ] == 'verification' )
		{
			$list = array(
				'adult'  => $this->global->translate( 'adult', 'Adult' ),
				'child'  => $this->global->translate( 'child', 'Child' ),
				'infant' => $this->global->translate( 'baby', 'Baby' )
			);

			foreach( $this->cart->getItems() as $carts )
			{
				foreach( $carts as $cart )
				{
					if( isset( $cart[ 'attributes' ][ 'guests' ] ) && is_array( $cart[ 'attributes' ][ 'guests' ] ) )
					{
						foreach( $cart[ 'attributes' ][ 'guests' ] as $type => $guests )
						{
							foreach( $guests as $i => $guest )
							{
								$this->template->set_var( 'fieldset', $this->verify_guest_field( $i, $type, $guest ) );
								$this->template->set_var( 'label', sprintf( '%s %s', $list[ $type ], $i + 1 ) );

								$this->template->Parse( 'svflBlock', 'sectionVerificationLoopBlock', true );
							}
						}
					}

					$this->template->set_var( 'post_id', $cart[ 'id' ] );
					$this->template->set_var( 'nights', $cart[ 'quantity' ]  );
				}
			}

			$this->template->set_var( 'back_url', $this->global->site_url( 'booking/guest' ) );

			$this->template->set_var( 'str_6', $this->global->translate( 'back', 'BACK', 3 ) );
			$this->template->set_var( 'str_2', $this->global->translate( 'guest', 'GUEST', 3 ) );
			$this->template->set_var( 'str_3', $this->global->translate( 'verify', 'VERIFY', 3 ) );
			$this->template->set_var( 'str_1', $this->global->translate( 'add-ons', 'ADD-ONS', 3 ) );
			$this->template->set_var( 'str_4', $this->global->translate( 'confirm', 'CONFIRM', 3 ) );
			$this->template->set_var( 'str_7', $this->global->translate( 'send-booking-request', 'SEND BOOKING REQUEST', 3 ) );
			$this->template->set_var( 'str_5', $this->global->translate( 'booking-text-3', 'Verify informations before sending the request' ) );
			$this->template->set_var( 'str_8', $this->global->translate( 'booking-text-16', 'This is the last step. Your booking request will be sent to the property.' ) );

			$this->template->Parse( 'svfBlock', 'sectionVerificationBlock', false );
		}
	}

	function init_section_confirmation()
	{
		$this->template->set_block( 'main', 'sectionRequestNotFoundOrCancelledBlock', 'srnfocBlock' );
		$this->template->set_block( 'main', 'sectionRequestPaymentDoneBlock', 'srpdBlock' );
		$this->template->set_block( 'main', 'sectionDetailRequestDoneBlock', 'sdrdBlock' );
		$this->template->set_block( 'main', 'sectionCheckAvailabilityBlock', 'scaBlock' );
		$this->template->set_block( 'main', 'sectionRequestInProcessBlock', 'sripBlock' );
		$this->template->set_block( 'main', 'sectionRequestConfirmBlock', 'srcBlock' );
		$this->template->set_block( 'main', 'sectionDetailRequestBlock', 'sdrBlock' );
		$this->template->set_block( 'main', 'sectionConfirmationBlock', 'scfBlock' );

		if( $this->uri[ 0 ] == 'confirmation' && isset( $this->uri[ 1 ] ) && !empty( $this->uri[ 1 ] ) )
		{
			$data = $this->global->get_booking( base64_decode( $this->uri[ 1 ] ) );

			if( empty( $data ) )
			{
				$this->template->Parse( 'srnfocBlock', 'sectionRequestNotFoundOrCancelledBlock', false );
			}
			else
			{
				$this->template->set_var( 'trips', $this->trips( $data ) );
				$this->template->set_var( 'rooms', $this->rooms( $data ) );
				$this->template->set_var( 'guest', $this->guests( $data ) );
				$this->template->set_var( 'addons', $this->addons( $data ) );

				$this->template->set_var( 'checkin_date', date( 'D, jS M, Y', $data[ 'lcheck_in' ] ) );
				$this->template->set_var( 'checkout_date', date( 'D, jS M, Y', $data[ 'lcheck_out' ] ) );
				
				$this->template->set_var( 'email', $data[ 'lemail' ] );
				$this->template->set_var( 'title', $data[ 'lposts_data' ][ 'ltitle' ] );
				$this->template->set_var( 'num_of_night', $data[ 'lnights' ] . ( $data[ 'lnights' ] > 1 ? ' nights' : ' night' ) );

				$this->template->set_var( 'thumbnail', $this->global->get_attachment_url( $data[ 'lposts_data' ][ 'accommodation_image' ], 70, 70 ) );
				$this->template->set_var( 'thumbnail_placeholder', $this->global->get_attachment_url( $data[ 'lposts_data' ][ 'accommodation_image' ], 10, 10 ) );

				$this->template->set_var( 'currency_code', $this->global->get_currency_code( $data[ 'lposts_data' ][ 'lpost_id' ] ) );
				$this->template->set_var( 'total', $this->global->get_format_price( $data[ 'ltotal' ], $data[ 'lposts_data' ][ 'lpost_id' ] ) );
				$this->template->set_var( 'deposit', $this->global->get_format_price( $data[ 'ldeposit' ], $data[ 'lposts_data' ][ 'lpost_id' ] ) );
				$this->template->set_var( 'balance', $this->global->get_format_price( $data[ 'lbalance' ], $data[ 'lposts_data' ][ 'lpost_id' ] ) );
				
				$this->template->set_var( 'str_2', $this->global->translate( 'guest', 'GUEST', 3 ) );
				$this->template->set_var( 'str_3', $this->global->translate( 'verify', 'VERIFY', 3 ) );
				$this->template->set_var( 'str_1', $this->global->translate( 'add-ons', 'ADD-ONS', 3 ) );
				$this->template->set_var( 'str_4', $this->global->translate( 'confirm', 'CONFIRM', 3 ) );
				$this->template->set_var( 'str_5', $this->global->translate( 'thank-you', 'THANK YOU', 3 ) );
				$this->template->set_var( 'str_7', $this->global->translate( 'booking-text-5', 'This is not a Booking Confirmation.' ) );
				$this->template->set_var( 'str_6', $this->global->translate( 'booking-text-4', 'Your Booking Request has been sent to %s', 0, array( $data[ 'lposts_data' ][ 'ltitle' ] ) ) );
				$this->template->set_var( 'str_8', $this->global->translate( 'booking-text-6', 'You will receive an answer within 24 hours.<br/>Once your Booking Request has been confirmed, a payment link will be sent to finalize your booking with only 1 click. Only the deposit will be required.<br/>The accommodation will be hold for you payment for 24 hours after confirmation. Be ready!' ) );
				$this->template->set_var( 'str_9', $this->global->translate( 'booking-text-21', 'Your Booking Request to %s is still processing', 0, array( $data[ 'lposts_data' ][ 'ltitle' ] ) ) );

				$this->template->set_var( 'str_10', $this->global->translate( 'booking-text-22', 'Your Booking Request to %s has been confirmed', 0, array( $data[ 'lposts_data' ][ 'ltitle' ] ) ) );
				$this->template->set_var( 'str_11', $this->global->translate( 'booking-text-23', 'Process Your Payment Now' ) );
				$this->template->set_var( 'str_12', $this->global->translate( 'booking-text-24', 'Please click this <a class="payment-link" href="%s"><b>payment link</b></a> to finalize your booking.<br/> Only the deposit will be required.', 0, array( $this->global->get_payment_url( $data[ 'lbooking_id' ] ) ) ) );
				$this->template->set_var( 'str_13', $this->global->translate( 'sorry', 'SORRY', 3 ) );
				$this->template->set_var( 'str_14', $this->global->translate( 'booking-text-25', 'Your Booking Request is not found or has been cancelled' ) );
				$this->template->set_var( 'str_15', $this->global->translate( 'booking-text-26', 'Should you need any help or any question, please contact us' ) );
				$this->template->set_var( 'str_16', $this->global->translate( 'booking-text-27', 'Your Payment has been successfully processed' ) );
				$this->template->set_var( 'str_17', $this->global->translate( 'booking-text-28', 'You will receive an answer within 24 hours.<br/>Once your payment has been confirmed by our team, you will get other information from us about your request.' ) );
				$this->template->set_var( 'str_18', $this->global->translate( 'details-of-your-request', 'Details of your Request' ) );

				$this->template->set_var( 'str_19', $this->global->translate( 'guests', 'GUESTS', 3 ) );
				$this->template->set_var( 'str_20', $this->global->translate( 'duration', 'DURATION', 3 ) );
				$this->template->set_var( 'str_21', $this->global->translate( 'check-in', 'CHECK-IN', 3 ) );
				$this->template->set_var( 'str_22', $this->global->translate( 'check-out', 'CHECK-OUT', 3 ) );
				$this->template->set_var( 'str_23', $this->global->translate( 'total', 'TOTAL', 3 ) );
				$this->template->set_var( 'str_25', $this->global->translate( 'paid-deposit', 'PAID DEPOSIT', 3 ) );
				$this->template->set_var( 'str_26', $this->global->translate( 'required-deposit', 'REQUIRED DEPOSIT', 3 ) );
				$this->template->set_var( 'str_27', $this->global->translate( 'booking-text-7', 'After confirmation, the Link to Deposit Payment will be sent at your email' ) );
				$this->template->set_var( 'str_28', $this->global->translate( 'booking-text-8', 'Payment methods accepted (Transfer or Credit card to be signaled) ' ) );

				if( $data[ 'lposts_data' ][ 'payment_policy' ] == '0' )
                {
                    $this->template->set_var( 'str_24', $this->global->translate( 'booking-text-17', 'Pay immediately after reservation', 3 ) );
                }
                elseif( $data[ 'lposts_data' ][ 'payment_policy' ] == '1' )
                {
                    $this->template->set_var( 'str_24', $this->global->translate( 'booking-text-18', 'Pay 2 months prior to arrival', 3 ) );
                }
                elseif( $data[ 'lposts_data' ][ 'payment_policy' ] == '2' )
                {
                    $this->template->set_var( 'str_24', $this->global->translate( 'booking-text-19', 'Pay 1 month prior to arrival', 3 ) );
                }
                elseif( $data[ 'lposts_data' ][ 'payment_policy' ] == '3' )
                {
                    $this->template->set_var( 'str_24', $this->global->translate( 'booking-text-20', 'Pay upon arrival', 3 ) );
                }

				if( $data[ 'lnights' ] > 1 )
				{
					$this->template->set_var( 'num_of_night', $data[ 'lnights' ] . ' ' . $this->global->translate( 'nights', 'nights' ) );
				}
				else
				{
					$this->template->set_var( 'num_of_night', $data[ 'lnights' ] . ' ' . $this->global->translate( 'night', 'night' ) );
				}

				if( $data[ 'lstatus' ] == 0 )
				{
					$this->template->Parse( 'sdrBlock', 'sectionDetailRequestBlock', false );
					$this->template->Parse( 'scaBlock', 'sectionCheckAvailabilityBlock', false );
				}
				else if( $data[ 'lstatus' ] == 1 && $data[ 'lstatus_payment' ] == 0 )
				{
					$this->template->Parse( 'srcBlock', 'sectionRequestConfirmBlock', false );
				}
				else if( $data[ 'lstatus' ] == 1 && $data[ 'lstatus_payment' ] == 1 )
				{
					$this->template->Parse( 'srpdBlock', 'sectionRequestPaymentDoneBlock', false );
					$this->template->Parse( 'sdrdBlock', 'sectionDetailRequestDoneBlock', false );
				}
				else if( $data[ 'lstatus' ] == 2 )
				{
					$this->template->Parse( 'srnfocBlock', 'sectionRequestNotFoundOrCancelledBlock', false );
				}
				else
				{
					$this->template->Parse( 'sdrBlock', 'sectionDetailRequestBlock', false );
					$this->template->Parse( 'sripBlock', 'sectionRequestInProcessBlock', false );
				}
				
				$this->template->Parse( 'scfBlock', 'sectionConfirmationBlock', false );
			}
		}
	}

	function init_section_payment()
	{
		$this->template->set_block( 'main', 'sectionPaymentExpiredLinkBlock', 'spyelBlock' );
		$this->template->set_block( 'main', 'sectionPaymentNotFoundBlock', 'spynfBlock' );
		$this->template->set_block( 'main', 'sectionPaymentDoneBlock', 'spydnBlock' );
		$this->template->set_block( 'main', 'sectionPaymentFormBlock', 'spyfrBlock' );
		$this->template->set_block( 'main', 'sectionPaymentBlock', 'spyBlock' );

		if( $this->uri[ 0 ] == 'payment' && isset( $this->uri[ 1 ] ) && !empty( $this->uri[ 1 ] ) )
		{
			$param = json_decode( base64_decode( $this->uri[ 1 ] ), true );

			if( isset( $param[ 'expired' ] ) )
			{
				if( empty( $param[ 'expired' ] ) === false && time() <= $param[ 'expired' ] )
				{
					if( isset( $param[ 'id' ] ) )
					{
						$data = $this->global->get_booking( $param[ 'id' ] );
						
						if( empty( $data ) === false )
						{
							//-- GET accommodation data
							$posts = $this->global->get_posts( array( 'lpost_id' => $data[ 'lpost_id' ] ) );

							if( $data[ 'lstatus' ] == 1 && $data[ 'lstatus_payment' ] == 0 )
							{
								$this->template->set_var( 'lbooking_id', $data[ 'lbooking_id' ] );
								$this->template->set_var( 'payment_intent', $this->init_payment_intent() );

								$this->template->Parse( 'spyfrBlock', 'sectionPaymentFormBlock', false );
							}
							else
							{
								$this->template->Parse( 'spydnBlock', 'sectionPaymentDoneBlock', false );
							}
						}
						else
						{
							$this->template->Parse( 'spynfBlock', 'sectionPaymentNotFoundBlock', false );
						}
					}
					else
					{
						$this->template->Parse( 'spynfBlock', 'sectionPaymentNotFoundBlock', false );
					}
				}
				else
				{
					$this->template->Parse( 'spynfBlock', 'sectionPaymentExpiredLinkBlock', false );
				}
			}
			else
			{
				$this->template->Parse( 'spyelBlock', 'sectionPaymentExpiredLinkBlock', false );
			}

			$this->template->set_var( 'onri_class', 'onri-payment' );

			$this->template->Parse( 'spyBlock', 'sectionPaymentBlock', false );
		}
	}

	function init_payment_intent()
	{		
		$payment = $this->global->get_setting_value( 'default_payment' );
		$content = '';

		if( $payment == 'stripe' )
		{
			$env  = $this->global->get_setting_value( 'stripe_environment' );
			$pkey = $this->global->get_setting_value( 'stripe_' . $env . '_public_key' );

			$content .= '
            <h2>' . $this->global->translate( 'credit-card', 'Credit Card' ) . '</h2>
            <div class="datadetailguest">
                <div class="headatadetail">
                    <h3>' . $this->global->translate( 'payment-information', 'PAYMENT INFORMATION', 3 ) . '</h3>
                </div>
                <div class="ddtl-formcon">
                    <div class="daform">
						<div id="stripe-payment-element" class="payment-element" data-public-key="' . $pkey . '">
				            <span>' . $this->global->translate( 'load-payment', 'Load Payment ...' ) . '</span>
				        </div>
                    </div>
                </div>
            </div>
            <div class="btnnextprev btnnextprev_end flex">
                <button type="submit" class="nxt-b flex" name="checkout" disabled>
                    <span>' . $this->global->translate( 'proceed-payment', 'PROCEED PAYMENT' ) . '</span>
                    <img src="//' . THEME_URL . '/assets/img/arrow-btm.svg">
                </button>
            </div>';

			$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://js.stripe.com/v3/' ) );
		}
		elseif( $payment == 'paypal' )
		{
			$env = $this->global->get_setting_value( 'paypal_environment' );
			$aid = $this->global->get_setting_value( 'paypal_' . $env . '_app_id' );

			$content .= '
			<div id="paypal-payment-element" class="payment-element">
	            <h2>' . $this->global->translate( 'choose-payment', 'Choose Payment' ) . '</h2>
	            <div class="datadetailguest">
	                <div class="headatadetail">
	                    <h3>' . $this->global->translate( 'payment-information', 'PAYMENT INFORMATION', 3 ) . '</h3>
	                </div>
	                <div class="ddtl-formcon">
	                    <div class="daform">
							<div id="paypal-button-container" class="paypal-button-container"></div>
							<span style="display:block; text-align:center; font-weight:700; padding:10px 0 15px;">OR</span>
						    <div id="checkout-form">
								<div id="card-name-field-container"></div>
								<div id="card-number-field-container"></div>
								<div id="card-expiry-field-container"></div>
								<div id="card-cvv-field-container"></div>
						    </div>
					    </div>
	                </div>
	            </div>
	            <div class="btnnextprev btnnextprev_end flex">
	                <button id="multi-card-field-button" type="button" class="nxt-b flex" name="checkout" disabled>
	                    <span>' . $this->global->translate( 'proceed-payment', 'PROCEED PAYMENT' ) . '</span>
	                    <img src="//' . THEME_URL . '/assets/img/arrow-btm.svg">
	                </button>
	            </div>
			</div>';

			$this->actions->add_actions( 'css', $this->global->get_css( 'https://www.paypalobjects.com/webstatic/en_US/developer/docs/css/cardfields.css' ) );
			$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://www.paypal.com/sdk/js?components=buttons,card-fields&client-id=' . $aid ) );
			$this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . THEME_URL . '/assets/js/paypal.js' ) );
		}

		return $content;
	}

	function init_section_sidebar()
	{
		$this->template->set_block( 'main', 'whatsappInternationalBlock', 'sowiBlock' );
		$this->template->set_block( 'main', 'phoneInternationalBlock', 'sopiBlock' );

		$this->template->set_block( 'main', 'whatsappEuropeBlock', 'soweBlock' );
		$this->template->set_block( 'main', 'phoneEuropeBlock', 'sopeBlock' );

		$this->template->set_block( 'main', 'whatsappGlobalBlock', 'sowgBlock' );
		$this->template->set_block( 'main', 'phoneGlobalBlock', 'sopgBlock' );

		$this->template->set_block( 'main', 'whatsappAsiaBlock', 'sowaBlock' );
		$this->template->set_block( 'main', 'phoneAsiaBlock', 'sopaBlock' );

		$this->template->set_block( 'main', 'skypeAmericaBlock', 'sosaBlock' );
		$this->template->set_block( 'main', 'skypeEuropeBlock', 'soseBlock' );
		$this->template->set_block( 'main', 'skypeAsiaBlock', 'sosiBlock' );

		$this->template->set_block( 'main', 'sectionSidebarConfirmationBlock', 'sscBlock' );
		$this->template->set_block( 'main', 'sectionSidebarEmptyBlock', 'sseBlock' );
		$this->template->set_block( 'main', 'sectionSidebarLoopBlock', 'ssdlBlock' );
		$this->template->set_block( 'main', 'sectionSidebarBlock', 'ssdBlock' );

		$this->template->set_var( 'sidebar_lbl_1', $this->global->translate( 'contact-us', 'Contact Us' ) );
		$this->template->set_var( 'sidebar_lbl_2', $this->global->translate( 'contact-form', 'Contact Form' ) );

		$this->template->set_var( 'sidebar_lbl_5', $this->global->translate( 'asia', 'ASIA' ) );
		$this->template->set_var( 'sidebar_lbl_4', $this->global->translate( 'phone-2', 'Phone' ) );
		$this->template->set_var( 'sidebar_lbl_6', $this->global->translate( 'europe', 'EUROPE' ) );
		$this->template->set_var( 'sidebar_lbl_3', $this->global->translate( 'call-us', 'CALL US' ) );
		$this->template->set_var( 'sidebar_lbl_7', $this->global->translate( 'international', 'INTERNATIONAL' ) );

		$this->template->set_var( 'sidebar_lbl_8', $this->global->translate( 'call-skype-america', 'CALL SKYPE AMERICA' ) );
		$this->template->set_var( 'sidebar_lbl_9', $this->global->translate( 'call-skype-europe', 'CALL SKYPE EUROPE' ) );
		$this->template->set_var( 'sidebar_lbl_10', $this->global->translate( 'call-skype-asia', 'CALL SKYPE ASIA' ) );

		$this->template->set_var( 'sidebar_lbl_11', $this->global->translate( 'guests', 'GUESTS', 3 ) );
		$this->template->set_var( 'sidebar_lbl_12', $this->global->translate( 'check-in', 'CHECK-IN', 3 ) );
		$this->template->set_var( 'sidebar_lbl_13', $this->global->translate( 'check-out', 'CHECK-OUT', 3 ) );

		$this->template->set_var( 'sidebar_lbl_14', $this->global->translate( 'total', 'Total' ) );
		$this->template->set_var( 'sidebar_lbl_15', $this->global->translate( 'deposit', 'Deposit' ) );
		$this->template->set_var( 'sidebar_lbl_16', $this->global->translate( 'booking-text-2', 'Deposit Payment will only be required upon Confirmation from the Property.' ) );

		if( $this->uri[ 0 ] == 'confirmation' )
		{
			$sidebar_text    = $this->global->get_setting_value( 'sidebar_contact_us_text' );
			$sidebar_subtext = $this->global->get_setting_value( 'sidebar_contact_us_subtext' );

			if( empty( $sidebar_subtext ) === false )
			{
				$sidebar_subtext = sprintf( '<span>%s</span>', $sidebar_subtext );
			}

			if( isset( $this->uri[ 1 ] ) && !empty( $this->uri[ 1 ] ) )
			{
				$data = $this->global->get_booking( base64_decode( $this->uri[ 1 ] ) );

				$this->template->set_var( 'contact_form_link', 'javascript:;' );
				$this->template->set_var( 'sidebar_contact_us_text', $sidebar_text );
				$this->template->set_var( 'sidebar_contact_us_subtext', $sidebar_subtext );

				if( empty( $data ) )
				{
					$this->template->Parse( 'sseBlock', 'sectionSidebarEmptyBlock', false );
				}
				else
				{
					if( in_array( $data[ 'lstatus' ], array( 6, 8 ) ) )
					{
						$this->template->Parse( 'sseBlock', 'sectionSidebarEmptyBlock', false );
					}
					else
					{
						//-- GET Phone, Whatsapp, and Skype setting
						$phone_international = $this->global->get_setting_value( 'phone_international' );
						$phone_global        = $this->global->get_setting_value( 'phone_global' );
						$phone_europe        = $this->global->get_setting_value( 'phone_europe' );
						$phone_asia          = $this->global->get_setting_value( 'phone_asia' );

						$wa_international    = $this->global->get_setting_value( 'wa_international' );
						$wa_global           = $this->global->get_setting_value( 'wa_global' );
						$wa_europe           = $this->global->get_setting_value( 'wa_europe' );
						$wa_asia             = $this->global->get_setting_value( 'wa_asia' );

						$skype_america       = $this->global->get_setting_value( 'skype_america' );
						$skype_europe        = $this->global->get_setting_value( 'skype_europe' );
						$skype_asia          = $this->global->get_setting_value( 'skype_asia' );
			 
						if( $phone_international == '' && $wa_international  == '' )
						{
							$this->template->set_var( 'call_international_css', 'sr-only' );
						}
						else
						{
							$this->template->set_var( 'call_international_css', 'ek-cta' );

							if( $phone_international !== '' )
							{
								$this->template->set_var( 'tel_international', $this->global->get_format_phone_number( $phone_international, 'tel' ) );
								$this->template->set_var( 'phone_international', $this->global->get_format_phone_number( $phone_international ) );

								$this->template->Parse( 'sopiBlock', 'phoneInternationalBlock', false );
							}
							
							if( $wa_international !== '' )
							{
								$this->template->set_var( 'chat_international', $this->global->get_format_phone_number( $wa_international, 'wa' ) );
								$this->template->set_var( 'wa_international', $this->global->get_format_phone_number( $wa_international ) );

								$this->template->Parse( 'sowiBlock', 'whatsappInternationalBlock', false );
							}
						}

						if( $phone_global == '' && $wa_global  == '' )
						{
							$this->template->set_var( 'call_global_css', 'sr-only' );
						}
						else
						{
							$this->template->set_var( 'call_global_css', 'ek-cta' );

							if( $phone_global !== '' )
							{
								$this->template->set_var( 'tel_global', $this->global->get_format_phone_number( $phone_global, 'tel' ) );
								$this->template->set_var( 'phone_global', $this->global->get_format_phone_number( $phone_global ) );

								$this->template->Parse( 'sopgBlock', 'phoneGlobalBlock', false );
							}
							
							if( $wa_global !== '' )
							{
								$this->template->set_var( 'chat_global', $this->global->get_format_phone_number( $wa_global, 'wa' ) );
								$this->template->set_var( 'wa_global', $this->global->get_format_phone_number( $wa_global ) );

								$this->template->Parse( 'sowgBlock', 'whatsappGlobalBlock', false );
							}
						}

						if( $phone_europe == '' && $wa_europe  == '' )
						{
							$this->template->set_var( 'call_europe_css', 'sr-only' );
						}
						else
						{
							$this->template->set_var( 'call_europe_css', 'ek-cta' );

							if( $phone_europe !== '' )
							{
								$this->template->set_var( 'tel_europe', $this->global->get_format_phone_number( $phone_europe, 'tel' ) );
								$this->template->set_var( 'phone_europe', $this->global->get_format_phone_number( $phone_europe ) );

								$this->template->Parse( 'sopeBlock', 'phoneEuropeBlock', false );
							}
							
							if( $wa_europe !== '' )
							{
								$this->template->set_var( 'chat_europe', $this->global->get_format_phone_number( $wa_europe, 'wa' ) );
								$this->template->set_var( 'wa_europe', $this->global->get_format_phone_number( $wa_europe ) );

								$this->template->Parse( 'soweBlock', 'whatsappEuropeBlock', false );
							}
						}

						if( $phone_asia == '' && $wa_asia  == '' )
						{
							$this->template->set_var( 'call_asia_css', 'sr-only' );
						}
						else
						{
							$this->template->set_var( 'call_asia_css', 'ek-cta' );

							if( $phone_asia !== '' )
							{
								$this->template->set_var( 'tel_asia', $this->global->get_format_phone_number( $phone_asia, 'tel' ) );
								$this->template->set_var( 'phone_asia', $this->global->get_format_phone_number( $phone_asia ) );

								$this->template->Parse( 'sopaBlock', 'phoneAsiaBlock', false );
							}
							
							if( $wa_asia !== '' )
							{
								$this->template->set_var( 'chat_asia', $this->global->get_format_phone_number( $wa_asia, 'wa' ) );
								$this->template->set_var( 'wa_asia', $this->global->get_format_phone_number( $wa_asia ) );

								$this->template->Parse( 'sowaBlock', 'whatsappAsiaBlock', false );
							}
						}

						if( $skype_america == '' && $skype_europe  == '' && $skype_asia  == '' )
						{
							$this->template->set_var( 'call_skype_css', 'sr-only' );
						}
						else
						{
							$this->template->set_var( 'call_skype_css', 'call-us-skype' );

							if( $skype_america !== '' )
							{
								$this->template->set_var( 'skype_america', $this->global->get_format_skype_link( $skype_america ) );

								$this->template->Parse( 'sosaBlock', 'skypeAmericaBlock', false );
							}
							
							if( $skype_europe !== '' )
							{
								$this->template->set_var( 'skype_europe', $this->global->get_format_skype_link( $skype_europe ) );

								$this->template->Parse( 'soseBlock', 'skypeEuropeBlock', false );
							}
							
							if( $skype_asia !== '' )
							{
								$this->template->set_var( 'skype_asia', $this->global->get_format_skype_link( $skype_asia ) );

								$this->template->Parse( 'sosiBlock', 'skypeAsiaBlock', false );
							}
						}

						$this->template->Parse( 'sscBlock', 'sectionSidebarConfirmationBlock', false );
					}
				}
			}
			else
			{
				$this->template->Parse( 'sseBlock', 'sectionSidebarEmptyBlock', false );
			}
		}
		else if( $this->uri[ 0 ] == 'payment' )
		{
			$sidebar_text    = $this->global->get_setting_value( 'sidebar_contact_us_text' );
			$sidebar_subtext = $this->global->get_setting_value( 'sidebar_contact_us_subtext' );

			if( empty( $sidebar_subtext ) === false )
			{
				$sidebar_subtext = sprintf( '<span>%s</span>', $sidebar_subtext );
			}

			if( isset( $this->uri[ 1 ] ) && !empty( $this->uri[ 1 ] ) )
			{
				$data = $this->global->get_booking( base64_decode( $this->uri[ 1 ] ) );

				$this->template->set_var( 'contact_form_link', 'javascript:;' );
				$this->template->set_var( 'sidebar_contact_us_text', $sidebar_text );
				$this->template->set_var( 'sidebar_contact_us_subtext', $sidebar_subtext );

				if( empty( $data ) )
				{
					$this->template->Parse( 'sseBlock', 'sectionSidebarEmptyBlock', false );
				}
				else
				{
					if( $data[ 'lstatus' ] == 1 && $data[ 'lstatus_payment' ] == 0 )
					{
						$this->template->set_var( 'trips', $this->trips( $data ) );
						$this->template->set_var( 'rooms', $this->rooms( $data ) );
						$this->template->set_var( 'guest', $this->guests( $data ) );
						$this->template->set_var( 'addons', $this->addons( $data ) );
						$this->template->set_var( 'location', $this->locations( $data[ 'lposts_data' ] ) );

						$this->template->set_var( 'checkin_date', date( 'l, F jS, Y', $data[ 'lcheck_in' ] ) );
						$this->template->set_var( 'checkout_date', date( 'l, F jS, Y', $data[ 'lcheck_out' ] ) );
						
						$this->template->set_var( 'num_of_night', $data[ 'lnights' ] . ( $data[ 'lnights' ] > 1 ? ' nights' : ' night' ) );

			            $this->template->set_var( 'thumbnail', $this->global->get_attachment_url( $data[ 'lposts_data' ][ 'accommodation_image' ], 70, 70 ) );
						$this->template->set_var( 'thumbnail_placeholder', $this->global->get_attachment_url( $data[ 'lposts_data' ][ 'accommodation_image' ], 10, 10 ) );

						$this->template->set_var( 'name', $this->global->get_listing_title( $data[ 'lposts_data' ] ) );
						$this->template->set_var( 'currency_code', $this->global->get_currency_code( $data[ 'lpost_id' ] ) );
						$this->template->set_var( 'total', $this->global->get_format_price( $data[ 'ltotal' ], $data[ 'lpost_id' ] ) );
						$this->template->set_var( 'deposit', $this->global->get_format_price( $data[ 'ldeposit' ], $data[ 'lpost_id' ] ) );
						
						$this->template->Parse( 'ssdlBlock', 'sectionSidebarLoopBlock', false );
						$this->template->Parse( 'ssdBlock', 'sectionSidebarBlock', false );
					}
					else
					{
						$this->template->Parse( 'sseBlock', 'sectionSidebarEmptyBlock', false );
					}
				}
			}
			else
			{
				$this->template->Parse( 'sseBlock', 'sectionSidebarEmptyBlock', false );
			}
		}
		else
		{
			$post_id = '';

			foreach( $this->cart->getItems() as $carts )
			{
				foreach( $carts as $cart )
				{
					$post_id = $cart[ 'id' ];

					$post = $this->global->get_posts( array( 'lpost_id' => $post_id ) );
					
					if( $cart[ 'attributes' ][ 'nights' ] > 1 )
					{
						$this->template->set_var( 'num_of_night', $cart[ 'attributes' ][ 'nights' ] . ' ' . $this->global->translate( 'nights', 'nights' ) );
					}
					else
					{
						$this->template->set_var( 'num_of_night', $cart[ 'attributes' ][ 'nights' ] . ' ' . $this->global->translate( 'night', 'night' ) );
					}

					$rate = $this->global->get_accommodation_rating( $post_id );

					$this->template->set_var( 'trips', $this->trips( $cart ) );
					$this->template->set_var( 'rooms', $this->rooms( $cart ) );
					$this->template->set_var( 'guest', $this->guests( $cart ) );
					$this->template->set_var( 'addons', $this->addons( $cart ) );
					$this->template->set_var( 'location', $this->locations( $post ) );

					$this->template->set_var( 'checkin_date', date( 'l, F jS, Y', strtotime( $cart[ 'attributes' ][ 'checkin' ] ) ) );
					$this->template->set_var( 'checkout_date', date( 'l, F jS, Y', strtotime( $cart[ 'attributes' ][ 'checkout' ] ) ) );

					$this->template->set_var( 'rnum', $rate[ 'reviews' ] );
					$this->template->set_var( 'reviews', $this->global->translate( 'reviews', 'Reviews' ) );
					$this->template->set_var( 'rating', $this->global->get_accommodation_rating_label( $rate ) );

					$this->template->set_var( 'name', $this->global->get_listing_title( $post ) );
		            $this->template->set_var( 'thumbnail', $this->global->get_attachment_url( $post[ 'accommodation_image' ], 70, 70 ) );
					$this->template->set_var( 'thumbnail_placeholder', $this->global->get_attachment_url( $post[ 'accommodation_image' ], 10, 10 ) );
				}

				$this->template->Parse( 'ssdlBlock', 'sectionSidebarLoopBlock', false );
			}

			$total   = $this->cart_total();
			$deposit = $this->deposit();

			$this->template->set_var( 'total', $this->global->get_format_price( $total, $post_id ) );
			$this->template->set_var( 'currency_code', $this->global->get_currency_code( $post_id ) );
			$this->template->set_var( 'deposit', $this->global->get_format_price( $deposit, $post_id ) );
			
			$this->template->Parse( 'ssdBlock', 'sectionSidebarBlock', false );
		}
	}

	function init()
	{
		$this->lang_id   = $this->global->get_current_language_id();
		$this->lang_code = $this->global->get_current_language_code();
		$this->lang_def  = $this->global->get_setting_value( 'llanguage' );

		$this->func = new globalAdmin();
    	$this->cart = new Carts();

    	if( isset( $_POST[ 'add_to_cart' ] ) )
		{
			//-- GET accommodation data
			$posts = $this->global->get_posts( array( 'lpost_id' => $_POST[ 'post_id' ] ) );

			if( $posts[ 'lref_id' ] != 0 && ( $posts[ 'lref_id' ] != '' || is_null( $posts[ 'lref_id' ] ) === false ) )
			{
				$posts = $this->global->get_posts( array( 'lpost_id' => $posts[ 'lref_id' ] ) );
			}

			//-- CHECK availability if policy = 0 ( automatic )
			if( $posts[ 'policy' ] == '0' && $this->check_availability() === false )
			{
				$this->flash->add( array( 'type' => 'error', 'message' => 'Room that you choice not available on that date, please choose another date' ) );

				header( 'Location: ' . $_SERVER[ 'HTTP_REFERER' ] );

				exit;
			}

			//-- REMOVE previous cart
			$this->cart->destroy();

			//-- SET attribute
			$checkin  = date( 'Y-m-d', strtotime( $_POST[ 'checkin' ] ) );
			$checkout = date( 'Y-m-d', strtotime( $_POST[ 'checkout' ] ) );
			$nights   = $this->num_night( $checkin, $checkout );

			$attribute = array(
				'agent'    => $posts[ 'lcreated_by' ],
				'name'     => $posts[ 'ltitle' ],
				'infant'   => $_POST[ 'infant' ],
				'child'    => $_POST[ 'child' ],
				'adult'    => $_POST[ 'adult' ],
				'guest'    => $_POST[ 'guest' ],
				'checkout' => $checkout,
				'checkin'  => $checkin,
				'nights'   => $nights,
			);

			if( isset( $_POST[ 'room' ] ) )
			{
				$rooms = array();
				$price = 0;

				foreach( $_POST[ 'room' ] as $room_id => $room )
				{
					if( $room[ 'num' ] > 0 )
					{
						$dprice = $this->global->room_price( $posts,  array( 'lterm_id' => $room_id, 'room_price' => $room[ 'price' ] ), $_POST[ 'checkin' ], $_POST[ 'checkout' ] );
						$rprice = $dprice * $room[ 'num' ] * $_POST[ 'guest' ];
						$total  = $nights * $rprice;
						$price += $rprice;

						$rooms[ $room_id ] = array(
							'name'  => $room[ 'name' ],
							'num'   => $room[ 'num' ],
							'id'    => $room_id,
							'price' => $dprice,
							'total' => $total
						);
					}
				}

				$attribute = array_merge( $attribute, array(
					'rooms' => $rooms,
					'price' => $price
				));
			}
			else
			{
				$pprice = $_POST[ 'post_start_price' ];
				$price  = $pprice;

				$attribute = array_merge( $attribute, array(
					'price' => $price
				));
			}

			$this->cart->add( $_POST[ 'post_id' ], $nights, $attribute );
			
			header( 'Refresh:0' );

			exit;
		}
		elseif( isset( $_POST[ 'add_trip' ] ) )
		{
			$sc = $this->global->get_schedule( array( 'lschedule_id' => $_POST[ 'sid' ] ) );

			if( !empty( $sc ) )
			{
				//-- REMOVE previous cart
				$this->cart->destroy();

				//-- GET accommodation data
				$posts = $this->global->get_posts( array( 'lpost_id' => $sc[ 'lpost_id' ] ) );

				//-- SET attribute
				$checkin  = date( 'Y-m-d', strtotime( $sc[ 'lcheck_in' ] ) );
				$checkout = date( 'Y-m-d', strtotime( $sc[ 'lcheck_out' ] ) );
				$nights   = $this->num_night( $checkin, $checkout );

				$attribute = array(
					'agent'    => $posts[ 'lcreated_by' ],
					'name'     => $posts[ 'ltitle' ],
					'infant'   => $_POST[ 'infant' ],
					'child'    => $_POST[ 'child' ],
					'adult'    => $_POST[ 'adult' ],
					'guest'    => $_POST[ 'guest' ],
					'sid'      => $_POST[ 'sid' ],
					'checkout' => $checkout,
					'checkin'  => $checkin,
					'nights'   => $nights,
				);

				$trips[ $sc[ 'lschedule_id' ] ] = array(
					'itinerary' => $sc[ 'litinerary' ],
					'id'        => $sc[ 'lschedule_id' ],
					'departure' => $sc[ 'ldeparture' ],
					'duration'  => $sc[ 'lduration' ]
				);

				$attribute = array_merge( $attribute, array(
					'price' => $sc[ 'lprice' ],
					'trips' => $trips
				));

				$this->cart->add( $sc[ 'lpost_id' ], 1, $attribute );
			}

			header( 'Refresh:0' );

			exit;
		}
		elseif( isset( $_POST[ 'add_package' ] ) )
		{			
			$pc = $this->global->get_packages( array( 'lpackage_id' => $_POST[ 'pid' ] ) );

			if( !empty( $pc ) )
			{
				//-- REMOVE previous cart
				$this->cart->destroy();

				//-- GET accommodation data
				$posts = $this->global->get_posts( array( 'lpost_id' => $pc[ 'lpost_id' ] ) );

				//-- SET attribute
				$checkout = date( 'Y-m-d', strtotime( $_POST[ 'checkout' ] ) );
				$checkin  = date( 'Y-m-d', strtotime( $_POST[ 'checkin' ] ) );
				$nights   = $this->num_night( $checkin, $checkout );

				$attribute = array(
					'agent'    => $posts[ 'lcreated_by' ],
					'name'     => $posts[ 'ltitle' ],
					'infant'   => $_POST[ 'infant' ],
					'child'    => $_POST[ 'child' ],
					'adult'    => $_POST[ 'adult' ],
					'guest'    => $_POST[ 'guest' ],
					'pid'      => $_POST[ 'pid' ],
					'checkout' => $checkout,
					'checkin'  => $checkin,
					'nights'   => $nights,
				);

				$attribute = array_merge( $attribute, array(
					'price' => $pc[ 'lprice' ]
				));

				$this->cart->add( $pc[ 'lpost_id' ], 1, $attribute );
			}

			header( 'Refresh:0' );

			exit;
		}
		elseif( isset( $_POST[ 'add_addons' ] ) )
		{
			if( $this->cart->isEmpty() === false )
			{
				$add_ons_list = array();

				foreach( $_POST[ 'addons' ] as $addons_id => $addons )
				{
					if( isset( $addons[ 'check' ] ) )
					{
						$atotal = $addons[ 'price' ] * $addons[ 'num' ];

						$add_ons_list[ 'addons' ][ $addons_id ] = array(
							'price' => $addons[ 'price' ],
							'name'  => $addons[ 'name' ],
							'num'   => $addons[ 'num' ],
							'id'    => $addons_id,
							'total' => $atotal
						);
					}
				}

				foreach( $this->cart->getItems() as $carts )
				{
					foreach( $carts as $cart )
					{
						if( $cart[ 'id' ] == $_POST[ 'post_id' ] )
						{
							//-- UNSET previous addons
							if( isset( $cart[ 'attributes' ][ 'addons' ] ) )
							{
								unset( $cart[ 'attributes' ][ 'addons' ] );
							}

							if( empty( $add_ons_list ) === false )
							{
								$attribute = array_merge( $cart[ 'attributes' ], $add_ons_list );
							}
							else
							{
								$attribute = $cart[ 'attributes' ];
							}
							$this->cart->clear();
							$this->cart->add( $_POST[ 'post_id' ], $_POST[ 'nights' ], $attribute );
						}
					}
				}

				header( 'location: ' . $this->global->site_url( 'booking/guest' ) );

                exit;
			}
		}
		elseif( isset( $_POST[ 'add_guests' ] ) )
		{
			if( $this->cart->isEmpty() === false )
			{
				foreach( $this->cart->getItems() as $carts )
				{
					foreach( $carts as $cart )
					{
						if( $cart[ 'id' ] == $_POST[ 'post_id' ] )
						{
							//-- UNSET previous addons
							if( isset( $cart[ 'attributes' ][ 'guests' ] ) )
							{
								unset( $cart[ 'attributes' ][ 'guests' ] );
							}

							if( empty( $_POST[ 'guests' ] ) === false )
							{
								$attribute = array_merge( $cart[ 'attributes' ], array( 'guests' => $_POST[ 'guests' ] ) );
							}
							else
							{
								$attribute = $cart[ 'attributes' ];
							}

							$this->cart->clear();
							$this->cart->add( $_POST[ 'post_id' ], $_POST[ 'nights' ], $attribute );
						}
					}
				}

				header( 'location: ' . $this->global->site_url( 'booking/verification' ) );

                exit;
            }
		}
		elseif( isset( $_POST[ 'checkout' ] ) )
		{
			if( $this->cart->isEmpty() === false )
			{
				foreach( $this->cart->getItems() as $carts )
				{
					foreach( $carts as $cart )
					{
						//-- GET accommodation data
						$posts = $this->global->get_posts( array( 'lpost_id' => $cart[ 'id' ] ) );

						$trips  = isset( $cart[ 'attributes' ][ 'trips' ] )  ? json_encode( $cart[ 'attributes' ][ 'trips' ] ) : '';
						$rooms  = isset( $cart[ 'attributes' ][ 'rooms' ] )  ? json_encode( $cart[ 'attributes' ][ 'rooms' ] ) : '';
						$addons = isset( $cart[ 'attributes' ][ 'addons' ] ) ? json_encode( $cart[ 'attributes' ][ 'addons' ] ) : '';
						$guest  = isset( $cart[ 'attributes' ][ 'guests' ] ) ? json_encode( $cart[ 'attributes' ][ 'guests' ] ) : '';

						$lfname  = $cart[ 'attributes' ][ 'guests' ][ 'adult' ][ 0 ][ 'firstname' ];
						$llname  = $cart[ 'attributes' ][ 'guests' ][ 'adult' ][ 0 ][ 'lastname' ];
						$ltitle  = $cart[ 'attributes' ][ 'guests' ][ 'adult' ][ 0 ][ 'title' ];
						$lphone  = $cart[ 'attributes' ][ 'guests' ][ 'adult' ][ 0 ][ 'phone' ];
						$lemail  = $cart[ 'attributes' ][ 'guests' ][ 'adult' ][ 0 ][ 'email' ];
						$lnote   = $cart[ 'attributes' ][ 'guests' ][ 'adult' ][ 0 ][ 'note' ];
						$lflname = sprintf( '%s %s', $lfname, $llname );
						$lstatus = $posts[ 'policy' ] == '0' ? 1 : 0;

						$prm = array(
							'lbooking_code' => $this->global->unique_code( 'NSF', 'lumonata_booking', 'lbooking_code' ),
		                    'lcurrency_id'  => $this->global->get_currency_id( $cart[ 'id' ] ),
		                    'lcheck_out'    => strtotime( $cart[ 'attributes' ][ 'checkout' ] ),
		                    'lcheck_in'     => strtotime( $cart[ 'attributes' ][ 'checkin' ] ),
		                    'linfant'       => $cart[ 'attributes' ][ 'infant' ],
		                    'ladult'        => $cart[ 'attributes' ][ 'adult' ],
		                    'lchild'        => $cart[ 'attributes' ][ 'child' ],
		                    'lagent'        => $cart[ 'attributes' ][ 'agent' ],
		                    'lnights'       => $cart[ 'quantity' ],
		                    'lpost_id'      => $cart[ 'id' ],
		                    'laddons_total' => $this->cart_addons_total(),
		                    'lroom_total'   => $this->cart_rooms_total(),
		                    'lsubtotal'     => $this->cart_total(),
		                    'ltotal'        => $this->cart_total(),
		                    'ldeposit'      => $this->deposit(),
		                    'lbalance'      => $this->balance(),
		                    'lstatus'       => $lstatus,
		                    'lfullname'     => $lflname,
		                    'ltitle'        => $ltitle,
		                    'lfname'        => $lfname,
		                    'llname'        => $llname,
		                    'laddons'       => $addons,
		                    'lphone'        => $lphone,
		                    'lemail'        => $lemail,
		                    'lnote'         => $lnote,
		                    'lguests'       => $guest,
		                    'ltrips'        => $trips,
		                    'lrooms'        => $rooms,
		                    'lcreated_date' => time(),
		                    'ldlu'          => time(),
		                );
						
						if( isset( $cart[ 'attributes' ][ 'sid' ] ) && !empty( $cart[ 'attributes' ][ 'sid' ] ) )
						{
							$prm[ 'lprice' ]       = $cart[ 'attributes' ][ 'price' ];
							$prm[ 'lschedule_id' ] = $cart[ 'attributes' ][ 'sid' ];
						}
						
						if( isset( $cart[ 'attributes' ][ 'pid' ] ) && !empty( $cart[ 'attributes' ][ 'pid' ] ) )
						{
							$prm[ 'lprice' ]      = $cart[ 'attributes' ][ 'price' ];
							$prm[ 'lpackage_id' ] = $cart[ 'attributes' ][ 'pid' ];
						}

						$r = parent::insert( 'lumonata_booking', $prm );

		                if( is_array( $r ) )
		                {
							header( 'location: ' . $this->global->site_url( 'booking/verification' ) );

			                exit;
			            }
			            else
			            {
                    		$bid = parent::insert_id();							
							
                			$this->func->sendBookingRequestEmail( $bid );

                    		if( $posts[ 'policy' ] == '0' )
                    		{                    			
								header( 'location: ' . $this->global->get_payment_url( $bid ) );

				                exit;
                    		}
                    		else
                    		{
								header( 'location: ' . $this->global->site_url( 'booking/confirmation/' . base64_encode( $bid ) . '/' ) );

				                exit;
                    		}
			            }
					}
				}
			}
		}
	}

	function check_availability()
	{
		$valid = true;

		if( isset( $_POST[ 'room' ] ) && !empty( $_POST[ 'room' ] ) )
		{
			foreach( $_POST[ 'room' ] as $id => $dt )
			{
				$s = 'SELECT * FROM lumonata_calendar AS a WHERE a.lpost_id = %d AND a.lterm_id = %d AND a.ldate BETWEEN %s AND %s';
		        $q = parent::prepare_query( $s, $_POST[ 'post_id' ], $id, $_POST[ 'checkin' ], $_POST[ 'checkout' ] );
		        $r = parent::query( $q );

		        if( parent::num_rows( $r ) > 0 )
		        {
		        	while( $d = parent::fetch_array( $r ) )
		        	{
		        		if( $d[ 'lstatus' ] == 2 )
		        		{
							$valid = false;

							break;
		        		}
		     //    		else if( $d[ 'lstatus' ] == 1 && $dt[ 'num' ] > $d[ 'lavailable' ] )
		     //    		{
							// $valid = false;

							// break;
		     //    		}
		        	}

		        	if( $valid == false )
		        	{
		        		break;
		        	}
		        }
			}
		}
		else
		{
			$s = 'SELECT * FROM lumonata_calendar AS a WHERE a.lpost_id = %d AND ISNULL( a.lterm_id ) AND a.ldate BETWEEN %s AND %s';
	        $q = parent::prepare_query( $s, $_POST[ 'post_id' ], $_POST[ 'checkin' ], $_POST[ 'checkout' ] );
	        $r = parent::query( $q );

	        if( parent::num_rows( $r ) > 0 )
	        {
	        	while( $d = parent::fetch_array( $r ) )
	        	{
	        		if( $d[ 'lstatus' ] == 2 )
	        		{
						$valid = false;

						break;
	        		}
	     //    		else if( $d[ 'lstatus' ] == 1 && $dt[ 'num' ] > $d[ 'lavailable' ] )
	     //    		{
						// $valid = false;

						// break;
	     //    		}
	        	}
	        }
		}

		return $valid;
	}

	function add_ons_list( $post_id = '' )
	{
		if( empty( $post_id ) )
		{
			$s = 'SELECT * FROM lumonata_post AS a
				  LEFT JOIN lumonata_post_relationship AS b ON b.lpost_id = a.lpost_id
				  LEFT JOIN lumonata_post_terms AS c ON b.lterm_id = c.lterm_id
				  WHERE c.lrule = %s AND c.lgroup = %s AND c.lstatus = %d';
	        $q = parent::prepare_query( $s, 'accommodation_add_ons', 'accommodation', 1 );
	        $r = parent::query( $q );
		}
		else
		{
			$s = 'SELECT * FROM lumonata_post AS a
				  LEFT JOIN lumonata_post_relationship AS b ON b.lpost_id = a.lpost_id
				  LEFT JOIN lumonata_post_terms AS c ON b.lterm_id = c.lterm_id
				  WHERE c.lrule = %s AND c.lgroup = %s AND c.lstatus = %d AND a.lpost_id = %d';
	        $q = parent::prepare_query( $s, 'accommodation_add_ons', 'accommodation', 1, $post_id );
	        $r = parent::query( $q );
		}

		if( parent::num_rows( $r ) > 0 )
        {
        	$data = array();

            while( $d = parent::fetch_assoc( $r ) )
            {
            	//-- GET additional field
				$sa = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lterm_id = %d AND a.lmodule_id = ( SELECT a2.lmodule_id FROM lumonata_module AS a2 WHERE a2.lapps = %s )';
		        $qa = parent::prepare_query( $sa, $d[ 'lterm_id' ], 'accommodation_add_ons' );
		        $ra = parent::query( $qa );

		        if( parent::num_rows( $ra ) > 0 )
		        {
		            while( $da = parent::fetch_assoc( $ra ) )
		            {
		            	$d[ $da[ 'ladditional_key' ] ] = $da[ 'ladditional_value' ];
		            }
		        }

		        $data[] = $d;
		    }

		    return $data;
		}
	}

	function num_option( $addons = array() )
	{
		$options = '';

		for( $i = 0; $i < 11; $i++ )
		{ 
			if( isset( $addons[ 'num' ] ) && $i == $addons[ 'num' ] )
			{
				$options .= '<option value="' . $i . '" selected>' . $i . '</option>';
			}
			else
			{
				$options .= '<option value="' . $i . '">' . $i . '</option>';
			}
		}

		return $options;
	}

	function num_night( $checkin, $checkout )
	{
		$checkin  = new DateTime( $checkin );
		$checkout = new DateTime( $checkout );

		$nights = $checkout->diff( $checkin )->format( '%a' );
		$nights = $nights == 0 ? 1 : $nights;

		if( $formated )
		{
			if( $nights > 1 )
			{
				return $nights . ' ' . $this->global->translate( 'nights', 'nights' );
			}
			else
			{
				return $nights . ' ' . $this->global->translate( 'night', 'night' );
			}
		}
		else
		{
			return $nights;
		}
	}

	function guest_field( $index = 0, $type = 'adult', $guest = array() )
	{
		$fname = isset( $guest[ 'firstname' ] ) ? $guest[ 'firstname' ] : '';
		$lname = isset( $guest[ 'lastname' ] ) ? $guest[ 'lastname' ] : '';
		$title = isset( $guest[ 'title' ] ) ? $guest[ 'title' ] : '';

		if( $type == 'adult' )
		{
			$fields = '
			<fieldset>
                <div class="rdbox flex">
                    <div class="erd">
                        <input type="radio" id="mr" name="guests[' . $type . '][' . $index . '][title]" value="Mr." autocomplete="off" required ' . ( $title == '' || $title == 'Mr.' ? 'checked' : '' ) . ' />
                        <label for="mr">' . $this->global->translate( 'mr', 'Mr.' ) . '</label>
                    </div>
                    <div class="erd">
                        <input type="radio" id="mrs" name="guests[' . $type . '][' . $index . '][title]" value="Mrs." autocomplete="off" required ' . ( $title == 'Mrs.' ? 'checked' : '' ) . ' />
                        <label for="mrs">' . $this->global->translate( 'mrs', 'Mrs.' ) . '</label>
                    </div>
                    <div class="erd">
                        <input type="radio" id="miss" name="guests[' . $type . '][' . $index . '][title]" value="Miss." autocomplete="off" required ' . ( $title == 'Miss.' ? 'checked' : '' ) . ' />
                        <label for="miss">' . $this->global->translate( 'miss', 'Miss.' ) . '</label>
                    </div>
                </div>
                <input class="hf__" type="text" placeholder="' . $this->global->translate( 'first-name', 'first name' ) . '" name="guests[' . $type . '][' . $index . '][firstname]" value="' . $fname . '" autocomplete="off" required />
                <input class="hf__" type="text" placeholder="' . $this->global->translate( 'last-name', 'last name' ) . '" name="guests[' . $type . '][' . $index . '][lastname]" value="' . $lname . '" autocomplete="off" required />';

                if( $index == 0 )
                {
					$email = isset( $guest[ 'email' ] ) ? $guest[ 'email' ] : '';
					$phone = isset( $guest[ 'phone' ] ) ? $guest[ 'phone' ] : '';
					$note  = isset( $guest[ 'note' ] ) ? $guest[ 'note' ] : '';

                	$fields .= '
	                <input class="hf__" type="email" placeholder="' . $this->global->translate( 'email-address', 'email address' ) . '" name="guests[' . $type . '][' . $index . '][email]" value="' . $email . '" autocomplete="off" required />
	                <input class="hf__ phone" type="text" placeholder="' . $this->global->translate( 'phone-number', 'phone number' ) . '" name="guests[' . $type . '][' . $index . '][phone]" value="' . $phone . '" autocomplete="off" required />
	                <p class="ddtl-label">
	                    ' . $this->global->translate( 'special-request', 'Special Request' ) . '
	                </p>
	                <textarea placeholder="' . $this->global->translate( 'message', 'message' ) . '" rows="3" name="guests[' . $type . '][' . $index . '][note]" autocomplete="off">' . $note . '</textarea>';
                }

                $fields .= '
            </fieldset>';
		}
		else
		{
			$fields = '
			<fieldset>
                <input class="hf__" type="text" placeholder="' . $this->global->translate( 'first-name', 'first name' ) . '" name="guests[' . $type . '][' . $index . '][firstname]" value="' . $fname . '" autocomplete="off" required />
                <input class="hf__" type="text" placeholder="' . $this->global->translate( 'last-name', 'last name' ) . '" name="guests[' . $type . '][' . $index . '][lastname]" value="' . $lname . '" autocomplete="off" required />
            </fieldset>';
		}

		return $fields;
	}

	function verify_guest_field( $index = 0, $type = 'adult', $guest = array() )
	{
		$fname = isset( $guest[ 'firstname' ] ) ? $guest[ 'firstname' ] : '';
		$lname = isset( $guest[ 'lastname' ] ) ? $guest[ 'lastname' ] : '';
		$title = isset( $guest[ 'title' ] ) ? $guest[ 'title' ] : '';

		if( $type == 'adult' )
		{
			$fields = '
			<div class="veridetail flex">
                <h2>' . $title . '<br>' . sprintf( '%s %s', $fname, $lname ) . '</h2>';

                if( $index == 0 )
                {
					$email = isset( $guest[ 'email' ] ) ? $guest[ 'email' ] : '';
					$phone = isset( $guest[ 'phone' ] ) ? $guest[ 'phone' ] : '';
					$note  = isset( $guest[ 'note' ] ) ? $guest[ 'note' ] : '';

                	$fields .= '
	                <div class="veridata">
	                    <div class="edataveri flex">
	                        <h3>' . $this->global->translate( 'email', 'Email' ) . ':</h3>
	                        <p>' . $email . '</p>
	                        <input type="email" name="current_email" class="hidden" value="' . $email . '">
	                    </div>
	                    <div class="edataveri flex">
	                        <h3>' . $this->global->translate( 'confirm-email', 'Confirm Email' ) . ':</h3>
	                        <input type="email" name="confirm_email" placeholder="' . $this->global->translate( 'confirm-your-email', 'Confirm your email' ) . '" required>
	                    </div>
	                    <div class="edataveri flex">
	                        <h3>' . $this->global->translate( 'phone-number', 'Phone Number' ) . ':</h3>
	                        <p>' . $phone . '</p>
	                    </div>
	                    {info}
	                    <div class="edataveri">
	                        <h3>' . $this->global->translate( 'special-request', 'Special Request' ) . '</h3>
	                        <textarea name="note" >' . $note . '</textarea>
	                    </div>
	                </div>';
	            }

	            $fields .= '
            </div>';
        }
        else
        {
			$fields = '
			<div class="veridetail flex">
                <h2>' . sprintf( '%s %s', $fname, $lname ) . '</h2>
            </div>';
        }

        return $fields;
	}

	function cart_rooms_total()
	{
		return $this->cart->getAttributeSummary( 'rooms', 'total' );
	}

	function cart_addons_total()
	{
		return $this->cart->getAttributeSummary( 'addons', 'total' );
	}

	function cart_subtotal()
	{
		return $this->cart->getAttributeTotal();
	}

	function cart_total()
	{
		$addons   = $this->cart_addons_total();
		$subtotal = $this->cart_subtotal();
		$total    = $subtotal + $addons;

		return $total;
	}

	function deposit()
	{
		$dval  = $this->global->get_setting_value( 'deposit_value' );
		$total = $this->cart_total();

		return round( ( $total * $dval ) / 100 );
	}

	function balance()
	{
		$total   = $this->cart_total();
		$deposit = $this->deposit();
		$balance = $total - $deposit;

		return $balance;
	}

	function trips( $cart = array() )
	{
		$trips = '';

		if( isset( $cart[ 'attributes' ][ 'trips' ] ) && !empty( $cart[ 'attributes' ][ 'trips' ] ) )
		{
			foreach( $cart[ 'attributes' ][ 'trips' ] as $dt )
			{
                if( isset( $dt[ 'itinerary' ] ) && !empty( $dt[ 'itinerary' ] ) )
                {
                	$trips .= '
                    <div class="ci1 flex">
                        <span>' . $this->global->translate( 'itinerary', 'ITINERARY', 3 ) . '</span>
                        <p>' . $dt[ 'itinerary' ] . '</p>
                    </div>';
                }

                if( isset( $dt[ 'departure' ] ) && !empty( $dt[ 'departure' ] ) )
                {
                	$trips .= '
                    <div class="ci1 flex">
                        <span>' . $this->global->translate( 'departure', 'DEPARTURE', 3 ) . '</span>
                        <p>' . $dt[ 'departure' ] . '</p>
                    </div>';
                }
	        }
		}
		elseif( isset( $cart[ 'ltrips' ] ) && !empty( $cart[ 'ltrips' ] ) )
		{
            $trips .= '
            <div class="cart-tr flex">
                <h5>' . $this->global->translate( 'trips', 'Trips', 3 ) . '</h5>
	            <div class="cart-td flex">';

	            	$list = array();

					foreach( $cart[ 'ltrips' ] as $dt )
					{
						$list[] = sprintf( '%s : %s', $this->global->translate( 'duration', 'Duration', 1 ), $dt[ 'duration' ] );

						if( !empty( $dt[ 'departure' ] ) )
						{
							$list[] = sprintf( '%s : %s', $this->global->translate( 'departure', 'Departure', 1 ), $dt[ 'departure' ] );
						}

						if( !empty( $dt[ 'itinerary' ] ) )
						{
							$list[] = sprintf( '%s : %s', $this->global->translate( 'itinerary', 'Itinerary', 1 ), $dt[ 'itinerary' ] );
						}
					}

					$trips .= implode( '<br/>', $list );					
					$trips .= '
				</div>
		    </div>';
		}

		return $trips;
	}

	function rooms( $cart = array() )
	{
		$rooms = '';

		if( isset( $cart[ 'attributes' ][ 'rooms' ] ) && !empty( $cart[ 'attributes' ][ 'rooms' ] ) )
		{
			foreach( $cart[ 'attributes' ][ 'rooms' ] as $dt )
			{
				$rooms .= '
                <div class="o-dest">
                    <h3>' . sprintf( '%s %s', $dt[ 'num' ], $dt[ 'name' ] ) . '</h3>
                    <div class="ci1 flex">
                        <p>' . $this->global->get_format_price( $dt[ 'price' ], $cart[ 'id' ] ) . '/' . $this->global->translate( 'night', 'night' ) . '</p>
                        <span class="prc8">' . $this->global->get_format_price( $dt[ 'total' ], $cart[ 'id' ] ) . '</span>
                    </div>
                </div>';
			}
		}
		elseif( isset( $cart[ 'lrooms' ] ) && !empty( $cart[ 'lrooms' ] ) )
		{
			$rooms .= '
            <div class="cart-tr flex">
                <h5>' . $this->global->translate( 'rooms', 'ROOMS', 3 ) . '</h5>
	            <div class="cart-td flex">';

	            	$list = array();

					foreach( $cart[ 'lrooms' ] as $dt )
					{
						$list[] = sprintf( '%s x %s', $dt[ 'num' ], $dt[ 'name' ] );
					}

					$rooms .= implode( '<br/>', $list );					
					$rooms .= '
				</div>
		    </div>';
		}	

		return $rooms;
	}

	function addons( $cart = array() )
	{
		$addons = '';

		if( isset( $cart[ 'attributes' ][ 'addons' ] ) && !empty( $cart[ 'attributes' ][ 'addons' ] ) )
		{
			foreach( $cart[ 'attributes' ][ 'addons' ] as $dt )
			{
				$addons .= '
                <div class="o-dest">
                    <h3>' . sprintf( '%s %s', $dt[ 'num' ], $dt[ 'name' ] ) . '</h3>
                    <div class="ci1 flex">
                        <p>' . $this->global->get_format_price( $dt[ 'price' ], $cart[ 'id' ] ) . '</p>
                        <span class="prc8">' . $this->global->get_format_price( $dt[ 'total' ], $cart[ 'id' ] ) . '</span>
                    </div>
                </div>';
			}
		}
		elseif( isset( $cart[ 'laddons' ] ) && !empty( $cart[ 'laddons' ] ) )
		{
			$addons .= '
            <div class="cart-tr flex">
                <h5>' . $this->global->translate( 'add-ons', 'ADD-ONS', 3 ) . '</h5>
	            <div class="cart-td flex">';

	            	$list = array();

					foreach( $cart[ 'laddons' ] as $dt )
					{
						$list[] = sprintf( '%s x %s', $dt[ 'num' ], $dt[ 'name' ] );
					}

					$addons .= implode( '<br/>', $list );					
					$addons .= '
				</div>
		    </div>';
		}

		return $addons;
	}

	function guests( $cart = array() )
	{
		$guest = array();

		if( isset( $cart[ 'attributes' ] ) )
		{
			if( isset( $cart[ 'attributes' ][ 'adult' ] ) && $cart[ 'attributes' ][ 'adult' ] > 0 )
			{
				if( $cart[ 'attributes' ][ 'adult' ] > 1 )
				{
					$guest[] = $cart[ 'attributes' ][ 'adult' ] . ' ' . $this->global->translate( 'adults', 'adults' );
				}
				else
				{
					$guest[] = $cart[ 'attributes' ][ 'adult' ] . ' ' . $this->global->translate( 'adult', 'adult' );
				}
			}

			if( isset( $cart[ 'attributes' ][ 'child' ] ) && $cart[ 'attributes' ][ 'child' ] > 0 )
			{
				if( $cart[ 'attributes' ][ 'child' ] > 1 )
				{
					$guest[] = $cart[ 'attributes' ][ 'child' ] . ' ' . $this->global->translate( 'childs', 'childs' );
				}
				else
				{
					$guest[] = $cart[ 'attributes' ][ 'child' ] . ' ' . $this->global->translate( 'child', 'child' );
				}
			}

			if( isset( $cart[ 'attributes' ][ 'infant' ] ) && $cart[ 'attributes' ][ 'infant' ] > 0 )
			{
				if( $cart[ 'attributes' ][ 'infant' ] > 1 )
				{
					$guest[] = $cart[ 'attributes' ][ 'infant' ] . ' ' . $this->global->translate( 'babies', 'babies' );
				}
				else
				{
					$guest[] = $cart[ 'attributes' ][ 'infant' ] . ' ' . $this->global->translate( 'baby', 'baby' );
				}
			}
		}
		else
		{
			if( isset( $cart[ 'ladult' ] ) && $cart[ 'ladult' ] > 0 )
			{
				if( $cart[ 'ladult' ] > 1 )
				{
					$guest[] = $cart[ 'ladult' ] . ' ' . $this->global->translate( 'adults', 'adults' );
				}
				else
				{
					$guest[] = $cart[ 'ladult' ] . ' ' . $this->global->translate( 'adult', 'adult' );
				}
			}

			if( isset( $cart[ 'lchild' ] ) && $cart[ 'lchild' ] > 0 )
			{
				if( $cart[ 'ladult' ] > 1 )
				{
					$guest[] = $cart[ 'lchild' ] . ' ' . $this->global->translate( 'childs', 'childs' );
				}
				else
				{
					$guest[] = $cart[ 'lchild' ] . ' ' . $this->global->translate( 'child', 'child' );
				}
			}

			if( isset( $cart[ 'linfant' ] ) && $cart[ 'linfant' ] > 0 )
			{
				if( $cart[ 'ladult' ] > 1 )
				{
					$guest[] = $cart[ 'linfant' ] . ' ' . $this->global->translate( 'babies', 'babies' );
				}
				else
				{
					$guest[] = $cart[ 'linfant' ] . ' ' . $this->global->translate( 'baby', 'baby' );
				}
			}
		}

		return implode( ', ', $guest );
	}

	function locations( $data = array() )
	{
		$content = '
        <span class="o-place flex">';

        	$include  = array( 'accommodation_state', 'accommodation_country' );
        	$location = array();

        	foreach( $include as $dt )
        	{
        		if( isset( $data[ $dt ] ) && !empty( $data[ $dt ] ) )
        		{
        			$dest = $this->global->get_field_value( 'lumonata_post', array( 'ltitle', 'lsef_url' ), array( 'lpost_id' => $data[ $dt ] ) );

            		$location[] = '<a href="//' . SITE_URL . '/destination/' . $dest[ 'lsef_url' ] . '/">' . $dest[ 'ltitle' ] . '</a>';
            	}
        	}

            $content .= implode( ',&nbsp;', $location );

        	$content .= '
        </span>';

        return $content;
	}

    function trip_itinerary( $itinerary = '', $implode = true, $separator = '<br />' )
    {
        if( empty( $itinerary  ) === false )
        {
        	if( $implode === true )
        	{
        		return str_replace( ',', '<br/>', $itinerary );
        	}
        	else
        	{
        		$itinerary = explode( $itinerary );

                foreach( $itinerary as $d )
                {
                    $list[] = trim( $d );
                }
        	}
        }
    }

	function import()
	{
		require_once( ADMIN_DIR . '/functions/globals.php' );
	}

	function init_js()
	{
		return '
		<script>
			jQuery(document).ready(function(){
				init_booking_payment();
				init_booking_form();
				init_lazy_load();
			});
		</script>';
	}

	function set_meta_title()
	{
		$meta_title = $this->data[ 'lmeta_title' ];

		if( empty( $meta_title ) )
		{
			$meta_title = $this->global->get_setting_value( 'lmeta_title' );

			if( empty( $meta_title ) )
			{
				$meta_title = 'Booking - ' . $this->global->get_setting_value( 'web' );
			}
		}
		
		$this->actions->add_actions( 'meta_title', $meta_title );
	}

	function set_meta_desc()
	{
		$meta_description = $this->global->get_setting_value( 'lmeta_desc' );

		if( empty( $meta_description ) === false )
		{
			$meta_description = sprintf( '<meta name="description" content="%s" />', strip_tags( $meta_description ) );

			$this->actions->add_actions( 'meta_description', $meta_description );
		}
	}
}

?>