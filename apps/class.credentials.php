<?php

use League\OAuth2\Client\Provider\Google;

class credentials extends db
{
	function __construct( $action, $sef = '' )
	{
    	parent::__construct();

    	$this->template = new Template( THEME_DIR );
    	$this->global   = new globalFunctions();
		$this->actions  = $action;
		$this->sef      = $sef;
	}
	
	function load()
	{
		if( $this->sef == 'google-authentication' )
		{
			if( isset( $_GET[ 'code' ] ) )
			{
        		require_once( INCLUDE_DIR . '/vendor/autoload.php' );

				$client_id     = $this->global->get_setting_value( 'google_client_id' );
		        $client_secret = $this->global->get_setting_value( 'google_client_secret' );
		        $redirect_uri  = $this->global->get_setting_value( 'google_redirect_uri' );

		        if( $client_id != '' && $client_secret != '' && $redirect_uri != '' )
		        {
		            //-- LOAD google API
		            $client = new Google_Client();

		            $client->setClientId( $client_id );
		            $client->setClientSecret( $client_secret );
		            $client->setRedirectUri( $redirect_uri );

		            $client->addScope( 'email' );
		            $client->addScope( 'profile' );

					$token = $client->fetchAccessTokenWithAuthCode( $_GET[ 'code' ] );

					if( isset( $token[ 'access_token' ] ) )
					{
						$client->setAccessToken( $token[ 'access_token' ] );

						//-- GET profile info
						$oauth = new Google_Service_Oauth2( $client );
						$info  = $oauth->userinfo->get();
						$email = $info->email;
						$name  = $info->name;

						$s = 'SELECT 
					    		a.luser_id,
					    		a.lusername,
					    		a.lusertype_id
					          FROM lumonata_user AS a 
					          WHERE a.lemail = %s
					          AND a.lusertype_id = %d 
					          AND a.lblock = %d 
					          AND a.lstatus = %d';
					    $q = parent::prepare_query( $s, $email, 7, 0, 1 );
					    $r = parent::query( $q );

					    if( is_array( $r ) )
					    {
					    	header( 'location: ' . $this->global->site_url() );

	            			exit;
					    }
					    else
					    {
					        $n = parent::num_rows( $r );

					        if( $n > 0 )
					        {
					            $d = parent::fetch_array( $r );

					            //-- Update Last Visited User
					            parent::update( 'lumonata_user', array( 'llastvisit_date' => time() ), array( 'luser_id' => $d[ 'luser_id' ] ) );

					            //-- SET parameter
					            $param = base64_encode( 
					                json_encode( 
					                    array(
					                        'userid'   => $d[ 'luser_id' ],
					                        'username' => $d[ 'lusername' ],
					                        'usertype' => $d[ 'lusertype_id' ]
					                    )
					                )
					            );

					            //-- SET session
					            $_SESSION[ MEMBER_SESSION_NAME ] = $param;

					            //-- SET cookie
					            setcookie( MEMBER_SESSION_NAME, $param );

					            header( 'location: ' . $this->global->site_url() );

	                			exit;
					        }
					        else
					        {
					            header( 'location: ' . $this->global->site_url() );

	                			exit;
					        }
					    }
					}
					else
					{
					   	header( 'location: ' . $this->global->site_url() );

            			exit;
					}
		        }
			}
			else
			{
				header( 'location: ' . $this->global->site_url() );

                exit;
			}
		}
		elseif( $this->sef == 'facebook-authentication' )
		{

		}
		elseif( $this->sef == 'gmail-oauth-token' )
		{
			$mid = $this->global->get_module_id();

			if( isset( $_GET[ 'code' ] ) )
			{
	        	require_once( INCLUDE_DIR . '/vendor/autoload.php' );

	        	$sett = $this->global->get_setting_value();

				$client_id     = $sett[ 'gmail_xoauth_client_id' ];
		        $client_secret = $sett[ 'gmail_xoauth_client_secret' ];
		        $redirect_uri  = $sett[ 'gmail_xoauth_redirect_uri' ];

		        if( $client_id != '' && $client_secret != '' && $redirect_uri != '' )
		        {
		            //-- LOAD google API
		            $client = new Google( array(
					    'clientId'     => $client_id,
					    'clientSecret' => $client_secret,
					    'redirectUri'  => $redirect_uri,
					    'accessType'   => 'offline'
		            ));

		            //-- GET token
		            $token = $client->getAccessToken( 'authorization_code', array(
				        'code' => $_GET['code']
				    ));

		            //-- GET refresh token
				    $refresh_token = $token->getRefreshToken();

					if( isset( $refresh_token ) && !empty( $refresh_token ) )
					{
						//-- Check Additonal Field Exist or Not
						$aid = $this->global->get_setting_id( $sett[ 'lpost_id' ], 'gmail_xoauth_refresh_token' );

                        if( empty( $aid ) )
                        {
                        	parent::insert( 'lumonata_additional_field', array(
	                            'ladditional_key'   => 'gmail_xoauth_refresh_token',
	                            'lapp_id'           => $sett[ 'lpost_id' ],
	                            'ladditional_value' => $refresh_token,
	                            'lmodule_id'        => $mid,
	                        ));
                        }
                        else
                        {
                        	$param = array( 'ladditional_value' => $refresh_token );
                            $where = array( 'ladditional_id' => $aid );

                            parent::update( 'lumonata_additional_field', $param, $where );
                        }
					}
				}
			}

			header( 'location: ' . HT_SERVER . ADMIN_URL . '/home.php?mod=' . $mid . '&prc=view' );

            exit;
		}
		elseif( $this->sef == 'ig-oauth-token' )
		{
			$mid = $this->global->get_module_id();

			if( isset( $_GET[ 'code' ] ) )
			{
	        	require_once( INCLUDE_DIR . '/vendor/autoload.php' );

	        	$sett = $this->global->get_setting_value();

				$client_id     = $sett[ 'ig_app_id' ];
		        $client_secret = $sett[ 'ig_app_secret' ];
		        $redirect_uri  = $sett[ 'ig_redirect_uri' ];

		        if( $client_id != '' && $client_secret != '' && $redirect_uri != '' )
		        {               
                    //-- LOAD google API
                    $client = new League\OAuth2\Client\Provider\Instagram( array(
                        'clientId'     => $client_id,
                        'clientSecret' => $client_secret,
                        'redirectUri'  => $redirect_uri
                    ));

		            //-- GET token
		        	$token = $client->getAccessToken( 'authorization_code', array(
					    'code' => $_GET[ 'code' ]
					));

		            //-- GET long lived token
					$longlived_token = $client->getLongLivedAccessToken( $token );

					if( isset( $longlived_token ) && !empty( $longlived_token ) )
					{
						//-- Check Additonal Field Exist or Not
						$aid = $this->global->get_setting_id( $sett[ 'lpost_id' ], 'ig_token' );

                        if( empty( $aid ) )
                        {
                        	parent::insert( 'lumonata_additional_field', array(
	                            'ladditional_key'   => 'ig_token',
	                            'lapp_id'           => $sett[ 'lpost_id' ],
	                            'ladditional_value' => $longlived_token,
	                            'lmodule_id'        => $mid,
	                        ));
                        }
                        else
                        {
                        	$param = array( 'ladditional_value' => $longlived_token );
                            $where = array( 'ladditional_id' => $aid );

                            parent::update( 'lumonata_additional_field', $param, $where );
                        }
					}
				}
			}

			header( 'location: ' . HT_SERVER . ADMIN_URL . '/home.php?mod=' . $mid . '&prc=view' );

            exit;
		}
		elseif( $this->sef == 'account-activation' )
		{
			if( isset( $_GET[ 'code' ] ) && !empty( $_GET[ 'code' ] ) )
			{
				$s = 'SELECT a.lstatus FROM lumonata_user AS a WHERE a.lactivate_code = %s';
			    $q = parent::prepare_query( $s, $_GET[ 'code' ] );
			    $r = parent::query( $q );

			    if( !is_array( $r ) )
			    {
			        $n = parent::num_rows( $r );

			        if( $n > 0 )
			        {
			            $d = parent::fetch_array( $r );

			            if( $d[ 'lstatus' ] == 0 )
			            {
                        	$param  = array( 'lstatus' => 1 );
                            $where  = array( 'lactivate_code' => $_GET[ 'code' ] );
                            $result = parent::update( 'lumonata_user', $param, $where );

                            if( is_array( $result ) )
				            {
						    	header( 'location: ' . $this->global->site_url( 'account-activation-failed/?code=' . base64_encode( 3 ) ) );
				            }
				            else
				            {
					    		header( 'location: ' . $this->global->site_url( 'account-activation-success' ) );
				            }

							exit;
			            }
			            else
			            {
					    	header( 'location: ' . $this->global->site_url( 'account-activation-failed/?code=' . base64_encode( 2 ) ) );

							exit;
			            }
			       	}
			    }
			}
			else
			{
		    	header( 'location: ' . $this->global->site_url( 'account-activation-failed/?code=' . base64_encode( 1 ) ) );

				exit;
			}
		}
	}
}