<?php

class destination extends db
{
	function __construct( $action, $apps, $uri = array() )
	{
    	parent::__construct();

    	$this->template = new Template( THEME_DIR );
    	$this->global   = new globalFunctions();
		$this->actions  = $action;
		$this->apps     = $apps;
		$this->uri      = $uri;

		$this->init();
		$this->set_meta_desc();
		$this->set_meta_title();
	}
	
	function load()
	{
		if( isset( $_POST[ 'search' ] ) )
	    {
	        $param = base64_encode( json_encode( array_diff_key( $_POST, array_flip( array( 'search' ) ) ) ) );

	        if( $this->lang_id == $this->lang_def )
	        {
	        	header( 'location: ' . sprintf( '%s%s/accommodation/search-result/%s/', HT_SERVER, SITE_URL, $param ) );
	        }
	        else
	        {
	        	header( 'location: ' . sprintf( '%s%s/%s/accommodation/search-result/%s/', HT_SERVER, SITE_URL, $this->lang_code, $param ) );
	        }

	        exit;
	    }

		if( in_array( $this->apps, array( 'surf-trips', 'accommodation-type' ) ) )
		{
			return $this->init_destination_term();
		}
		else if( $this->apps == 'destinations' )
		{
			return $this->init_destination_archive();
		}
		else if( $this->apps == 'destination' )
		{
			return $this->init_destination();
		}
	}

	function init_destination_archive()
	{
		$s = 'SELECT a.ltitle, a.lpost_id, a.lsef_url, a.ltype FROM lumonata_post AS a WHERE a.ltype = %s AND(
					SELECT a2.ladditional_value 
					FROM lumonata_additional_field AS a2 
					WHERE a2.lapp_id = a.lpost_id  AND a2.ladditional_key = %s
			  ) = 0 AND a.lstatus = %d AND a.llang_id = %d ORDER BY a.ltitle';
		$q = parent::prepare_query( $s, 'destination', 'destination_type', 1, $this->lang_id );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
			$this->template->set_file( 'main', 'partials/destination.html' );

			$this->template->set_block( 'main', 'destinationLoopBlock', 'dslBlock' );
			$this->template->set_block( 'main', 'destinationBlock', 'dsBlock' );
			$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

			$this->template->set_var( 'search_form', $this->global->init_search_form() );
			$this->template->set_var( 'theme_url', THEME_URL );
			$this->template->set_var( 'site_url', SITE_URL );

            while( $d = parent::fetch_array( $r ) )
            {
            	//-- GET featured image
            	$sa = 'SELECT a2.ladditional_value FROM lumonata_additional_field AS a2
                       WHERE a2.lapp_id = %d AND a2.ladditional_key = %s AND a2.lmodule_id = ( 
                        	SELECT a3.lmodule_id FROM lumonata_module AS a3 WHERE a3.lapps = %s 
                       )';
                $qa = parent::prepare_query( $sa, $d[ 'lpost_id' ], 'destination_image', 'destination' );
                $ra = parent::query( $qa ); 
                $da = parent::fetch_array( $ra );

                if( isset( $da[ 'ladditional_value' ] ) === false )
                {
                	$da[ 'ladditional_value' ] = null;
                }

            	//-- GET short description
            	$st = 'SELECT a.ladditional_value FROM lumonata_additional_field AS a
                       WHERE a.lapp_id = %d AND a.ladditional_key = %s AND a.lmodule_id = ( 
                        	SELECT a2.lmodule_id FROM lumonata_module AS a2 WHERE a2.lapps = %s 
                       )';
                $qt = parent::prepare_query( $st, $d[ 'lpost_id' ], 'destination_brief', 'destination' );
                $rt = parent::query( $qt ); 
                $dt = parent::fetch_array( $rt );

                if( isset( $dt[ 'ladditional_value' ] ) && $dt[ 'ladditional_value' ] != '' )
                {
					$this->template->set_var( 'description', sprintf( '<p>%s</p>', $dt[ 'ladditional_value' ] ) );
                }
                else
                {
					$this->template->set_var( 'description', '' );
                }

				$this->template->set_var( 'title', $d[ 'ltitle' ] );
				$this->template->set_var( 'permalink', $this->global->permalink( $d ) );
				$this->template->set_var( 'image', $this->global->get_attachment_url( $da[ 'ladditional_value' ], 600, 600 ) );
				$this->template->set_var( 'image_placeholder', $this->global->get_attachment_url( $da[ 'ladditional_value' ], 10, 10 ) );

				$this->template->Parse( 'dslBlock', 'destinationLoopBlock', true );
            }

            $this->template->Parse( 'dsBlock', 'destinationBlock', false );

			$st = $this->global->get_setting_value();
					
			$this->template->set_var( 'title', $st[ 'destination_archive_title' ] );
			$this->template->set_var( 'brief_text', $st[ 'destination_archive_brief_text' ] );
			$this->template->set_var( 'header_title', $st[ 'destination_archive_header_title' ] );

			$this->template->set_var( 'header_image', $this->global->get_attachment_url( $st[ 'destination_archive_header_image' ], 1920, 500 ) );
			$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $st[ 'destination_archive_header_image' ], 10, 10 ) );
			
			$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/moment@2.29.3/moment.min.js' ) );

			$this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.css' ) );
	        $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.js' ) );

	        $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css' ) );
			$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js' ) );

	        $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css' ) );
	        $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js' ) );

	        $this->actions->add_actions( 'jvs', $this->init_js() );

			return $this->template->Parse( 'mlBlock', 'mainBlock', false );
        }
        else
        {
	        if( $this->lang_id == $this->lang_def )
	        {
	        	header( 'location: ' . sprintf( '%s%s/not-found/', HT_SERVER, SITE_URL ) );
	        }
	        else
	        {
	        	header( 'location: ' . sprintf( '%s%s/%s/destinations/', HT_SERVER, SITE_URL, $this->lang_code ) );
	        }

	        exit;
        }
	}

	function init_destination_term()
	{
		if( isset( $this->uri[ 0 ] ) && empty( $this->uri[ 0 ] ) === false )
		{
			$data = $this->global->get_post_terms( array(
				'lsef'     => $this->uri[ 0 ],
				'llang_id' => $this->lang_id,
				'lstatus'  => 1 
			));

			if( is_array( $data ) && !empty( $data ) )
			{
				foreach( $data as $dt )
				{
					$this->template->set_file( 'main', 'partials/archive-surf-trips.html' );
					
					$this->template->set_block( 'main', 'sectionBestSpotLoopBlock', 'sbslBlock' );
					$this->template->set_block( 'main', 'sectionBestSpotBlock', 'sbsBlock' );

					$this->template->set_var( 'search_form', $this->global->init_search_form() );
					$this->template->set_var( 'theme_url', THEME_URL );
					$this->template->set_var( 'site_url', SITE_URL );

					$this->template->set_var( 'accommodation_title', $this->global->translate( 'accommodation-text-7', sprintf( 'Accommodation For %s', $dt[ 'lname' ] ), 1, array( $dt[ 'lname' ] ) ) );
					$this->template->set_var( 'accommodation_map', $this->global->init_accommodation_map( $dt, array( 'term' => $dt[ 'lterm_id' ] ) ) );

					if( isset( $dt[ 'best_spot_title' ] ) && $dt[ 'best_spot_title' ] != '' )
					{
						$this->template->set_var( 'best_spot_title', $dt[ 'best_spot_title' ] );
					}
					else
					{
						$this->template->set_var( 'best_spot_title', $this->global->translate( 'destinations-text-9', sprintf( 'Best %s Spots', $dt[ 'lname' ] ), 1, array( $dt[ 'lname' ] ) ) );
					}

					if( isset( $dt[ 'best_spot_destination' ] ) && $dt[ 'best_spot_destination' ] != '' )
					{
						$bs = json_decode( $dt[ 'best_spot_destination' ], true );

						if( $bs !== null && json_last_error() === JSON_ERROR_NONE )
						{
							if( $dt[ 'best_spot_short_description' ] != '' )
							{
								$this->template->set_var( 'best_spot_short_description', sprintf( '<p class="o-desc">%s</p>', $dt[ 'best_spot_short_description' ] ) );
							}

							$num = 0;

							foreach( $bs as $id )
							{
								$spot = $this->global->get_posts( array(
									'lpost_id' => $id,
									'lstatus'  => 1
								));

								if( is_array( $spot ) && !empty( $spot ) )
								{
									if( isset( $spot[ 'destination_brief' ] ) )
									{
										$description = $spot[ 'destination_brief' ];
									}
									else
									{
										$description = '';
									}

									if( isset( $spot[ 'destination_image' ] ) )
									{
										$destination_image = $spot[ 'destination_image' ];
									}
									else
									{
										$destination_image = null;
									}

									$this->template->set_var( 'title', $spot[ 'ltitle' ] );
									$this->template->set_var( 'description', $description );
									$this->template->set_var( 'permalink', $this->global->permalink( $spot ) );

									$this->template->set_var( 'image', $this->global->get_attachment_url( $destination_image, 400, 672 ) );
									$this->template->set_var( 'image_placeholder', $this->global->get_attachment_url( $destination_image, 10, 10 ) );

									$this->template->Parse( 'sbslBlock', 'sectionBestSpotLoopBlock', true );

									$num++;
								}
							}

							if( $num > 0 )
							{
								$this->template->Parse( 'sbsBlock', 'sectionBestSpotBlock', false );
							}
						}
					}

					if( isset( $dt[ 'short_brief_content' ] ) && $dt[ 'short_brief_content' ] != '' )
					{
						if( isset( $dt[ 'short_brief_title' ] ) && $dt[ 'short_brief_title' ] != '' )
						{
							$this->template->set_var( 'short_brief_title', strtoupper( $dt[ 'short_brief_title' ] ) );
						}
						else
						{
							$this->template->set_var( 'short_brief_title', sprintf( 'Why %s', $dt[ 'lname' ] ) );
						}

						if( $dt[ 'short_brief_btn_link' ] != '' && $dt[ 'short_brief_btn_text' ] != '' )
						{
							$this->template->set_var( 'short_brief_button_css', 'f-btn nf__' );
							$this->template->set_var( 'short_brief_button', $dt[ 'short_brief_btn_text' ] );
							$this->template->set_var( 'short_brief_button_link', $dt[ 'short_brief_btn_link' ] );
						}
						else
						{
							$this->template->set_var( 'short_brief_button', '' );
							$this->template->set_var( 'short_brief_button_link', 'javascript:;' );
							$this->template->set_var( 'short_brief_button_css', 'f-btn nf__ hidden' );
						}

						$this->template->set_var( 'short_brief_content', $dt[ 'short_brief_content' ] );
						$this->template->set_var( 'short_brief_class', 'obox halform nowf_' );
					}
					else
					{
						$this->template->set_var( 'short_brief_class', 'obox halform nowf_ hidden' );
					}

					if( isset( $dt[ 'header_title' ] ) && $dt[ 'header_title' ] != '' )
					{
						$this->template->set_var( 'header_title', $dt[ 'header_title' ] );
					}
					else
					{
						$this->template->set_var( 'header_title', $dt[ 'lname' ] );
					}

					$mapi = $this->global->get_setting_value( 'google_map_api_key' );
					
					$this->template->set_var( 'title', $dt[ 'lname' ] );
					$this->template->set_var( 'content', $dt[ 'ldescription' ] );

					$this->template->set_var( 'header_image', $this->global->get_attachment_url( $dt[ 'header_image' ], 1920, 500 ) );
					$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $dt[ 'header_image' ], 10, 10 ) );

					$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );
			
					$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/moment@2.29.3/moment.min.js' ) );

					$this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.css' ) );
			        $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.js' ) );

			        $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css' ) );
					$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js' ) );

			        $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css' ) );
			        $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js' ) );

					$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/tinysort@3.2.7/dist/tinysort.min.js' ) );
					$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery.nicescroll@3.7.6/jquery.nicescroll.min.js' ) );
					$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://maps.googleapis.com/maps/api/js?key=' . $mapi . '&callback=init_map_advance', array( 'async', 'defer' ) ) );

			        $this->actions->add_actions( 'jvs', $this->init_js() );

					return $this->template->Parse( 'mlBlock', 'mainBlock', false );
				}
			}
		}
	}

	function init_destination()
	{
		if( empty( $this->uri[ 0 ] ) )
		{
			return $this->init_destination_archive();
		}
		else
		{
			$data = $this->global->get_posts( array(
				'lsef_url' => $this->uri[ 0 ],
				'llang_id' => $this->lang_id,
				'lstatus'  => 1 
			));

			if( is_array( $data ) && !empty( $data ) )
			{
				$this->template->set_file( 'main', 'partials/archive-destination.html' );

				$this->template->set_block( 'main', 'sectionTrendingPostLoopBlock', 'stplBlock' );
				$this->template->set_block( 'main', 'sectionTrendingPostBlock', 'stpBlock' );

				$this->template->set_block( 'main', 'sectionBestSurfAreaLoopBlock', 'sbsalBlock' );
				$this->template->set_block( 'main', 'sectionBestSurfAreaBlock', 'sbsaBlock' );

				$this->template->set_block( 'main', 'sectionBestSurfSpotLoopBlock', 'sbsslBlock' );
				$this->template->set_block( 'main', 'sectionBestSurfSpotBlock', 'sbssBlock' );

				$this->template->set_block( 'main', 'sectionIndicatorLoopBlock', 'silBlock' );
				$this->template->set_block( 'main', 'sectionIndicatorBlock', 'siBlock' );

				$this->template->set_block( 'main', 'sectionSafetyInfoLoop2Block', 'ssil2Block' );
				$this->template->set_block( 'main', 'sectionSafetyInfoLoopBlock', 'ssilBlock' );
				$this->template->set_block( 'main', 'sectionSafetyInfoBlock', 'ssiBlock' );

				$this->template->set_block( 'main', 'sectionSwellForcastBlock', 'ssfBlock' );
				$this->template->set_block( 'main', 'sectionClimateBlock', 'sclBlock' );

				$this->template->set_block( 'main', 'sectionBriefBlock', 'sbfBlock' );

				$this->template->set_var( 'search_form', $this->global->init_search_form() );
				$this->template->set_var( 'theme_url', THEME_URL );
				$this->template->set_var( 'site_url', SITE_URL );

				$this->template->set_var( 'accommodation_title', $this->global->translate( 'accommodation-text-6', sprintf( 'Accommodation In %s', $data[ 'ltitle' ] ), 1, array( $data[ 'ltitle' ] ) ) );
				$this->template->set_var( 'accommodation_map', $this->global->init_accommodation_map( $data, array( 'dest' => $data[ 'lpost_id' ] ) ) );

				if( isset( $data[ 'short_brief_title' ] ) && $data[ 'short_brief_title' ] != '' )
				{
					$this->template->set_var( 'surfing_in_title', strtoupper( $data[ 'short_brief_title' ] ) );
				}
				else
				{
					$this->template->set_var( 'surfing_in_title', $this->global->translate( 'destinations-text', sprintf( 'Surfing In %s', $data[ 'ltitle' ] ), 1, array( $data[ 'ltitle' ] ) ) );
				}

				if( isset( $data[ 'short_brief_content' ] ) && $data[ 'short_brief_content' ] != '' )
				{
					if( isset( $data[ 'short_brief_title' ] ) && $data[ 'short_brief_title' ] != '' )
					{
						$this->template->set_var( 'short_brief_title', strtoupper( $data[ 'short_brief_title' ] ) );
					}
					else
					{
						if( isset( $data[ 'surf_spot_destination' ] ) && $data[ 'surf_spot_destination' ] == '1' )
						{
							$this->template->set_var( 'short_brief_title', $this->global->translate( 'destinations-text-7', sprintf( '%s Surf Spot', $data[ 'ltitle' ] ), 1, array( $data[ 'ltitle' ] ) ) );
						}
						else
						{
							$this->template->set_var( 'short_brief_title', $this->global->translate( 'destinations-text', sprintf( 'Surfing In %s', $data[ 'ltitle' ] ), 1, array( $data[ 'ltitle' ] ) ) );
						}
					}

					if( ( isset( $data[ 'short_brief_btn_link' ] ) && $data[ 'short_brief_btn_link' ] != '' ) && ( isset( $data[ 'short_brief_btn_text' ] ) && $data[ 'short_brief_btn_text' ] != '' ) )
					{
						$this->template->set_var( 'short_brief_button_css', 'f-btn nf__' );
						$this->template->set_var( 'short_brief_button', $data[ 'short_brief_btn_text' ] );
						$this->template->set_var( 'short_brief_button_link', $data[ 'short_brief_btn_link' ] );
					}
					else
					{
						$this->template->set_var( 'short_brief_button', '' );
						$this->template->set_var( 'short_brief_button_link', 'javascript:;' );
						$this->template->set_var( 'short_brief_button_css', 'f-btn nf__ hidden' );
					}

					if( isset( $data[ 'short_brief_map' ] ) && $data[ 'short_brief_map' ] != '' )
					{
						$this->template->set_var( 'short_brief_class', 'o-container flex inhalf_' );
					}
					else
					{
						$this->template->set_var( 'short_brief_class', 'o-container flex' );
					}

					if( isset( $data[ 'short_brief_content' ] ) === false )
					{
						$data[ 'short_brief_content' ] = '';
					}

					if( isset( $data[ 'short_brief_map' ] ) === false )
					{
						$data[ 'short_brief_map' ] = null;
					}

					$this->template->set_var( 'short_brief_content', $data[ 'short_brief_content' ] );
					$this->template->set_var( 'short_brief_map', $this->global->get_attachment_url( $data[ 'short_brief_map' ], 750, 467 ) );
					$this->template->set_var( 'short_brief_map_placeholder', $this->global->get_attachment_url( $data[ 'short_brief_map' ], 10, 10 ) );

					$this->template->Parse( 'sbfBlock', 'sectionBriefBlock', false );
				}

				if( isset( $data[ 'climate_short_description' ] ) && empty( $data[ 'climate_short_description' ] ) === false )
				{
					if( isset( $data[ 'climate_title' ] ) && $data[ 'climate_title' ] != '' )
					{
						$this->template->set_var( 'climate_title', strtoupper( $data[ 'climate_title' ] ) );
					}
					else
					{
						$this->template->set_var( 'climate_title', $this->global->translate( 'destinations-text-2', sprintf( 'Climate In %s', $data[ 'ltitle' ] ), 1, array( $data[ 'ltitle' ] ) ) );
					}

					$this->template->set_var( 'climate_short_description', $data[ 'climate_short_description' ] );

					//-- $this->template->Parse( 'sclBlock', 'sectionClimateBlock', false );
				}

				if( ( isset( $data[ 'swell_forcast_short_description' ] ) && empty( $data[ 'swell_forcast_short_description' ] ) === false ) || ( isset( $data[ 'swell_forcast_iframe' ] ) && empty( $data[ 'swell_forcast_iframe' ] ) === false ) )
				{
					if( isset( $data[ 'swell_forcast_title' ] ) && $data[ 'swell_forcast_title' ] != '' )
					{
						$this->template->set_var( 'swell_forcast_title', strtoupper( $data[ 'swell_forcast_title' ] ) );
					}
					else
					{
						$this->template->set_var( 'swell_forcast_title', $this->global->translate( 'destinations-text-3', sprintf( 'Swell Forecast In %s', $data[ 'ltitle' ] ), 1, array( $data[ 'ltitle' ] ) ) );
					}

					$this->template->set_var( 'swell_forcast_iframe', $data[ 'swell_forcast_iframe' ] );
					$this->template->set_var( 'swell_forcast_short_description', $data[ 'swell_forcast_short_description' ] );

					//-- $this->template->Parse( 'ssfBlock', 'sectionSwellForcastBlock', false );
				}

				if( $data[ 'destination_type' ] == 1 )
				{
					$surfdata = $this->global->get_posts( array(
						'lparent_id' => $data[ 'lpost_id' ],
						'llang_id'   => $this->lang_id,
						'lstatus'    => 1 
					), false );

					if( is_array( $surfdata ) && !empty( $surfdata ) )
					{
						foreach( $surfdata as $sd )
						{
							$this->template->set_var( 'title', $sd[ 'ltitle' ] );
							$this->template->set_var( 'description', $sd[ 'destination_brief' ] );
							$this->template->set_var( 'permalink', $this->global->permalink( $sd ) );
							$this->template->set_var( 'image', $this->global->get_attachment_url( $sd[ 'destination_image' ], 400, 672 ) );
							$this->template->set_var( 'image_placeholder', $this->global->get_attachment_url( $sd[ 'destination_image' ], 10, 10 ) );

							$this->template->Parse( 'sbsalBlock', 'sectionBestSurfAreaLoopBlock', true );
						}

						if( $data[ 'surf_area_title' ] == '' )
						{
							$this->template->set_var( 'surf_area_title', $this->global->translate( 'destinations-text-4', sprintf( 'Best Surf Areas In %s', $data[ 'ltitle' ] ), 1, array( $data[ 'ltitle' ] ) ) );
						}
						else
						{
							$this->template->set_var( 'surf_area_title', strtoupper( $data[ 'surf_area_title' ] ) );
						}
						
						$this->template->set_var( 'surf_area_short_description', strtoupper( $data[ 'surf_area_short_description' ] ) );

						$this->template->Parse( 'sbsaBlock', 'sectionBestSurfAreaBlock', false );
					}
				}
				else if( $data[ 'destination_type' ] == 2 || $data[ 'destination_type' ] == 3 )
				{
					$spotdata = $this->global->get_posts_child( $data[ 'lpost_id' ] );

					if( is_array( $spotdata ) && !empty( $spotdata ) )
					{
						$mapi = $this->global->get_setting_value( 'google_map_api_key' );

						$c = '';
						$i = 0;

						foreach( $spotdata as $sp )
						{
							if( isset( $sp[ 'surf_spot_destination' ] ) && $sp[ 'surf_spot_destination' ] == '0' )
							{
								continue;
							}

					        //-- GET map coordinate
					        if( $i == 0 && !empty( $sp[ 'surf_spot_map_coordinate' ] ) )
					        {
					        	$c = $sp[ 'surf_spot_map_coordinate' ];

					        	$i++;
					        }

				    		$this->template->set_var( 'type', $sp[ 'surf_spot_type' ] );
				    		$this->template->set_var( 'quality', $sp[ 'surf_spot_quality' ] );
				    		$this->template->set_var( 'direction', $sp[ 'surf_spot_direction' ] );
				    		$this->template->set_var( 'experience', $sp[ 'surf_spot_experience' ] );
				    		$this->template->set_var( 'mcoordinate', $sp[ 'surf_spot_map_coordinate' ] );
				    		$this->template->set_var( 'quality_star', $this->global->get_quality_star( $sp[ 'surf_spot_quality' ] ) );

							$this->template->set_var( 'title', $sp[ 'ltitle' ] );
							$this->template->set_var( 'permalink', $this->global->permalink( $sp ) );
							$this->template->set_var( 'image', $this->global->get_attachment_url( $sp[ 'destination_image' ], 400, 283 ) );
							$this->template->set_var( 'image_placeholder', $this->global->get_attachment_url( $sp[ 'destination_image' ], 10, 10 ) );

							$this->template->Parse( 'sbsslBlock', 'sectionBestSurfSpotLoopBlock', true );
						}

						if( $data[ 'surf_spot_title' ] == '' )
						{
							$this->template->set_var( 'surf_spot_title', $this->global->translate( 'destinations-text-5', sprintf( 'Best Surf Spots In %s', $data[ 'ltitle' ] ), 1, array( $data[ 'ltitle' ] ) ) );
						}
						else
						{
							$this->template->set_var( 'surf_spot_title', strtoupper( $data[ 'surf_spot_title' ] ) );
						}
				    
				    	$this->template->set_var( 'surf_spot_coordinate', $c );					
						$this->template->set_var( 'surf_spot_short_description', strtoupper( $data[ 'surf_spot_short_description' ] ) );

						$this->template->set_var( 'str_1', $this->global->translate( 'experience', 'EXPERIENCE', 3 ) );
						$this->template->set_var( 'str_2', $this->global->translate( 'direction', 'DIRECTION', 3 ) );
						$this->template->set_var( 'str_3', $this->global->translate( 'quality', 'QUALITY', 3 ) );
						$this->template->set_var( 'str_4', $this->global->translate( 'type', 'TYPE', 3 ) );

						$this->template->set_var( 'str_5', $this->global->translate( 'experience', 'Experience', 1 ) );
						$this->template->set_var( 'str_6', $this->global->translate( 'direction', 'Direction', 1 ) );
						$this->template->set_var( 'str_7', $this->global->translate( 'quality', 'Quality', 1 ) );
						$this->template->set_var( 'str_8', $this->global->translate( 'type', 'Type', 1 ) );

						$this->template->Parse( 'sbssBlock', 'sectionBestSurfSpotBlock', false );
					}
				}

				$indicator = $this->global->get_setting_value( 'indicator_repeater' );

				if( !empty( $indicator ) )
				{
					$ind = json_decode( $indicator, true );

					if( $ind !== null && json_last_error() === JSON_ERROR_NONE )
					{
						$i = 0;

						foreach( $ind as $d )
						{
							if( count( array_filter( $d ) ) == 0 )
							{
								continue;
							}

							$i++;

							$this->template->set_var( 'title', $d[ 'indicator_title' ] );
							$this->template->set_var( 'content', $d[ 'indicator_description' ] );
							$this->template->set_var( 'image', $this->global->get_attachment_url( $d[ 'indicator_img' ] ) );
							$this->template->set_var( 'image_placeholder', $this->global->get_attachment_url( $d[ 'indicator_img' ] ) );
							
							$this->template->Parse( 'silBlock', 'sectionIndicatorLoopBlock', true );
						}

						if( $i > 0 )
						{
							$heading = $this->global->get_setting_value( 'indicator_heading' );

							if( $heading != '' )
							{
								$this->template->set_var( 'heading_title', strtoupper( $heading ) );
							}

							$this->template->Parse( 'siBlock', 'sectionIndicatorBlock', false );
						}
					}
				}

				if( isset( $data[ 'safety_info_repeater' ] ) && !empty( $data[ 'safety_info_repeater' ] ) )
				{
					$dt = json_decode( $data[ 'safety_info_repeater' ], true );

					if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
					{
						$i = 0;

						foreach( $dt as $d )
						{
							if( count( array_filter( $d ) ) == 0 )
							{
								continue;
							}

							$this->template->set_var( 'idx', $i );
							$this->template->set_var( 'class2', $i == 0 ? 'active' : '' );
							$this->template->set_var( 'class', $i == 0 ? 'class="active"' : '' );

							$this->template->set_var( 'title', $d[ 'safety_info_title' ] );
							$this->template->set_var( 'content', $d[ 'safety_info_description' ] );

							$i++;

							$this->template->Parse( 'ssilBlock', 'sectionSafetyInfoLoopBlock', true );
							$this->template->Parse( 'ssil2Block', 'sectionSafetyInfoLoop2Block', true );
						}

						if( $i > 0 )
						{						
							if( $data[ 'surf_area_title' ] == '' )
							{
								$this->template->set_var( 'heading_title', $this->global->translate( 'destinations-text-6', sprintf( 'Safety Information About Surfing In %s', $data[ 'ltitle' ] ), 1, array( $data[ 'ltitle' ] ) ) );
							}
							else
							{
								$this->template->set_var( 'heading_title', $data[ 'safety_info_heading' ] );
							}

							$this->template->Parse( 'ssiBlock', 'sectionSafetyInfoBlock', false );
						}
					}
				}

				if( isset( $data[ 'header_title' ] ) && $data[ 'header_title' ] != '' )
				{
					$this->template->set_var( 'header_title', $data[ 'header_title' ] );
				}
				else
				{
					if( isset( $data[ 'surf_spot_destination' ] ) && $data[ 'surf_spot_destination' ] == '1' )
					{
						$this->template->set_var( 'header_title', $this->global->translate( 'destinations-text-7', sprintf( '%s Surf Spot', $data[ 'ltitle' ] ), 1, array( $data[ 'ltitle' ] ) ) );
					}
					else
					{
						$this->template->set_var( 'header_title', $this->global->translate( 'destinations-text', sprintf( 'Surfing In %s', $data[ 'ltitle' ] ), 1, array( $data[ 'ltitle' ] ) ) );
					}
				}

				//-- Trending Data
				$trending = $this->global->get_trending_post( $data, $this->lang_id );

				if( !empty( $trending ) )
				{
					foreach( $trending as $td )
					{
						$brief = $this->global->get_brief_text( $td[ 'ldescription' ], 200 );

						if( $brief != '' )
						{
							$this->template->set_var( 'rbrief', sprintf( '<p>%s</p>', $brief ) );
						}
						else
						{
							$this->template->set_var( 'rbrief', '' );
						}

						$this->template->set_var( 'rpermalink', $this->global->permalink( $td ) );
						$this->template->set_var( 'rtitle', $this->global->get_listing_title( $td ) );
						$this->template->set_var( 'rimg_thumb', $this->global->get_attachment_url( $td[ 'accommodation_image' ], 551, 403 ) );
						$this->template->set_var( 'rimg_placeholder', $this->global->get_attachment_url( $td[ 'accommodation_image' ], 10, 10 ) );

						$this->template->Parse( 'stplBlock', 'sectionTrendingPostLoopBlock', true );
					}
					
					$this->template->set_var( 'trending_title', sprintf( 'Trending Surf Camps In %s', $data[ 'ltitle' ] ) );

					$this->template->Parse( 'stpBlock', 'sectionTrendingPostBlock', false );
				}

				$mapi = $this->global->get_setting_value( 'google_map_api_key' );

				$this->template->set_var( 'title', $data[ 'ltitle' ] );
				$this->template->set_var( 'content', $data[ 'ldescription' ] );

				$this->template->set_var( 'header_image', $this->global->get_attachment_url( $data[ 'header_image' ], 1208, 750 ) );
				$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $data[ 'header_image' ], 10, 10 ) );

				$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );
		
				$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/moment@2.29.3/moment.min.js' ) );

				$this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.css' ) );
		        $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.js' ) );

		        $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css' ) );
				$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js' ) );

		        $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css' ) );
		        $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js' ) );

				$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/tinysort@3.2.7/dist/tinysort.min.js' ) );
				$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery.nicescroll@3.7.6/jquery.nicescroll.min.js' ) );
				$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://maps.googleapis.com/maps/api/js?key=' . $mapi . '&callback=init_map_advance', array( 'async', 'defer' ) ) );

		        $this->actions->add_actions( 'jvs', $this->init_js() );

				return $this->template->Parse( 'mlBlock', 'mainBlock', false );
			}
		}
	}

	function init()
	{
		$this->lang_id   = $this->global->get_current_language_id();
		$this->lang_code = $this->global->get_current_language_code();
		$this->lang_def  = $this->global->get_setting_value( 'llanguage' );
	}

	function init_js()
	{
		return '
		<script>
			jQuery(document).ready(function(){
				init_related_post_slide();
				init_header_form();
				init_home_slide();
				init_tab_slide();
				init_lazy_load();
				init_select2();
				init_anchor();
			});
		</script>';
	}

	function set_meta_title()
	{
		if( isset( $this->data[ 'lmeta_title' ] ) )
		{
			$meta_title = $this->data[ 'lmeta_title' ];
		}
		else
		{
			$meta_title = '';
		}

		if( empty( $meta_title ) )
		{
			$meta_title = $this->global->get_setting_value( 'lmeta_title' );

			if( empty( $meta_title ) )
			{
				$meta_title = 'Destination - ' . $this->global->get_setting_value( 'web' );
			}
		}
		
		$this->actions->add_actions( 'meta_title', $meta_title );
	}

	function set_meta_desc()
	{
		$meta_description = $this->global->get_setting_value( 'lmeta_desc' );

		if( empty( $meta_description ) === false )
		{
			$meta_description = sprintf( '<meta name="description" content="%s" />', strip_tags( $meta_description ) );

			$this->actions->add_actions( 'meta_description', $meta_description );
		}
	}
}

?>