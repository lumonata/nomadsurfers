<?php

class homepage extends db
{
	function __construct( $action )
	{
    	parent::__construct();

    	$this->template = new Template( THEME_DIR );
    	$this->global   = new globalFunctions();
		$this->actions  = $action;
		
		$this->init();
		$this->set_meta_desc();
		$this->set_meta_title();
	}
	
	function load()
	{
		if( isset( $_POST[ 'search' ] ) )
	    {
	        $param = base64_encode( json_encode( array_diff_key( $_POST, array_flip( array( 'search' ) ) ) ) );

	        if( $this->lang_id == $this->lang_def )
			{
		    	header( 'location: ' . sprintf( '%s%s/accommodation/search-result/%s/', HT_SERVER, SITE_URL, $param ) );
	        }
	        else
	        {
		    	header( 'location: ' . sprintf( '%s%s/%s/accommodation/search-result/%s/', HT_SERVER, SITE_URL, $this->lang_code, $param ) );
	        }

	        exit;
	    }

		$this->template->set_file( 'main', 'partials/homepage.html' );

		$this->init_section_one();	
		$this->init_section_two();

		$this->init_section_three();
		$this->init_section_four();

		$this->init_section_five();
		$this->init_section_six();

		$this->init_section_seven();
		$this->init_section_eight();

		$this->init_section_nine();
		$this->init_section_ten();

		$this->init_section_eleven();

		$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

		$this->template->set_var( 'site_url', SITE_URL );
		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'search_form', $this->global->init_search_form() );

		$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/moment@2.29.3/moment.min.js' ) );

		$this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.css' ) );
        $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.js' ) );

        $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css' ) );
		$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js' ) );

        $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css' ) );
        $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js' ) );

        $this->actions->add_actions( 'jvs', $this->init_js() );

		return $this->template->Parse( 'mlBlock', 'mainBlock', false );
	}

	function init_section_one()
	{
		$this->template->set_block( 'main', 'sectionOneLoopBlock', 'solBlock' );
		$this->template->set_block( 'main', 'sectionOneBlock', 'soBlock' );

		if( isset( $this->data[ 'section_1_repeater' ] ) && !empty( $this->data[ 'section_1_repeater' ] ) )
		{
			$dt = json_decode( $this->data[ 'section_1_repeater' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				foreach( $dt as $d )
				{
					if( current( $dt ) == $d )
					{
						if( isset( $d[ 'subtitle' ] ) && !empty( $d[ 'subtitle' ] ) )
						{
							$this->template->set_var( 'first_subtitle', sprintf( '<div class="subtitle">%s</div>', $d[ 'subtitle' ] ) );
						}

						if( isset( $d[ 'big_title' ] ) && !empty( $d[ 'big_title' ] ) )
						{
							$this->template->set_var( 'first_big_title', sprintf( '<div class="big-title">%s</div>', $d[ 'big_title' ] ) );
						}

						if( isset( $d[ 'middle_title' ] ) && !empty( $d[ 'middle_title' ] ) )
						{
							$this->template->set_var( 'first_middle_title', sprintf( '<div class="middle-title">%s</div>', $d[ 'middle_title' ] ) );
						}

						if( isset( $d[ 'bottom_title' ] ) && !empty( $d[ 'bottom_title' ] ) )
						{
							$this->template->set_var( 'first_bottom_title', sprintf( '<div class="bottom-title">%s</div>', $d[ 'bottom_title' ] ) );
						}
					}

					$this->template->set_var( 'slide_subtitle', $d[ 'subtitle' ] );
					$this->template->set_var( 'slide_big_title', $d[ 'big_title' ] );
					$this->template->set_var( 'slide_middle_title', $d[ 'middle_title' ] );
					$this->template->set_var( 'slide_bottom_title', $d[ 'bottom_title' ] );

					if( isset( $d[ 'slide_image' ] ) && !empty( $d[ 'slide_image' ] ) )
					{
						$this->template->set_var( 'slide_img', sprintf( '<img data-src="%s" src="%s" class="cover lazy-load" alt="">', $this->global->get_attachment_url( $d[ 'slide_image' ], 1920, 768 ), $this->global->get_attachment_url( $d[ 'slide_image' ], 10, 10 ) ) );
					}

					if( isset( $d[ 'slide_video' ] ) && !empty( $d[ 'slide_video' ] ) )
					{
						$this->template->set_var( 'slide_video', sprintf( '<video loop muted autoplay preload="auto">%s</video>', $this->global->get_video_source( $d[ 'slide_video' ] ) ) );
			        }

					$this->template->Parse( 'solBlock', 'sectionOneLoopBlock', true );
				}

				$this->template->Parse( 'soBlock', 'sectionOneBlock', false );
			}
		}
	}

	function init_section_two()
	{
		$this->template->set_block( 'main', 'sectionTwoLoopBlock', 'stlBlock' );
		$this->template->set_block( 'main', 'sectionTwoBlock', 'stBlock' );

		if( isset( $this->data[ 'section_2_repeater' ] ) && !empty( $this->data[ 'section_2_repeater' ] ) )
		{
			$dt = json_decode( $this->data[ 'section_2_repeater' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$num = 0;

				foreach( $dt as $d )
				{
					if( $d[ 'title' ] == '' && $d[ 'description' ] == '' )
					{
						continue;
					}

					$this->template->set_var( 'item_title', $d[ 'title' ] );
					$this->template->set_var( 'item_description', $d[ 'description' ] );

					$this->template->Parse( 'stlBlock', 'sectionTwoLoopBlock', true );

					$num++;
				}

				if( $num > 0 )
				{
					if( empty( $this->data[ 'section_2_big_title' ] ) )
					{
						$this->template->set_var( 'heading_title', $this->data[ 'section_2_heading' ] );
					}
					else
					{
						$this->template->set_var( 'heading_title', sprintf( '<span>%s</span><small>%s</small>', $this->data[ 'section_2_big_title' ], $this->data[ 'section_2_heading' ] ) );
					}

					$this->template->Parse( 'stBlock', 'sectionTwoBlock', false );
				}
			}
		}
	}

	function init_section_three()
	{
		$this->template->set_block( 'main', 'sectionThreeLoopBlock', 'sthlBlock' );
		$this->template->set_block( 'main', 'sectionThreeBlock', 'sthBlock' );

    	$s = 'SELECT a.lname, a.lterm_id, a.lsef, a.lrule FROM lumonata_post_terms AS a WHERE a.lrule = %s AND a.lgroup = %s AND a.lstatus = %d AND a.llang_id = %d';
		$q = parent::prepare_query( $s, 'surf_trip', 'accommodation', 1, $this->lang_id );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
	        while( $d = parent::fetch_array( $r ) )
	        {
	        	//-- GET featured image
	        	$sa = 'SELECT a2.ladditional_value FROM lumonata_additional_field AS a2
	                   WHERE a2.lterm_id = %d AND a2.ladditional_key = %s AND a2.lmodule_id = ( 
	                    	SELECT a3.lmodule_id FROM lumonata_module AS a3 WHERE a3.lapps = %s 
	                   )';
	            $qa = parent::prepare_query( $sa, $d[ 'lterm_id' ], 'surf_trip_image', 'accommodation_surf_trips' );
	            $ra = parent::query( $qa ); 
	            $da = parent::fetch_array( $ra );

				$this->template->set_var( 'title', $d[ 'lname' ] );
				$this->template->set_var( 'permalink', $this->global->term_link( $d ) );
				$this->template->set_var( 'btn_text', $this->global->translate( 'see-more', 'See More', 1 ) );
				$this->template->set_var( 'image', $this->global->get_attachment_url( $da[ 'ladditional_value' ], 400, 579 ) );
				$this->template->set_var( 'image_placeholder', $this->global->get_attachment_url( $da[ 'ladditional_value' ], 10, 10 ) );

	        	$this->template->Parse( 'sthlBlock', 'sectionThreeLoopBlock', true );
	        }

	        $this->template->set_var( 'heading_title', $this->data[ 'section_3_heading' ] );
	        $this->template->set_var( 'heading_short_description', $this->data[ 'section_3_short_description' ] );

	        $this->template->Parse( 'sthBlock', 'sectionThreeBlock', false );
        }
	}

	function init_section_four()
	{
		$this->template->set_block( 'main', 'sectionFourLoopBlock', 'sflBlock' );
		$this->template->set_block( 'main', 'sectionFourBlock', 'sfBlock' );

		$s = 'SELECT a.lname, a.lterm_id, a.lsef, a.lrule FROM lumonata_post_terms AS a WHERE a.lrule = %s AND a.lgroup = %s AND a.lstatus = %d AND a.llang_id = %d';
		$q = parent::prepare_query( $s, 'accommodation_type', 'accommodation', 1, $this->lang_id );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_array( $r ) )
            {
            	//-- GET featured image
            	$sa = 'SELECT a2.ladditional_value FROM lumonata_additional_field AS a2
                       WHERE a2.lterm_id = %d AND a2.ladditional_key = %s AND a2.lmodule_id = ( 
                        	SELECT a3.lmodule_id FROM lumonata_module AS a3 WHERE a3.lapps = %s 
                       )';
                $qa = parent::prepare_query( $sa, $d[ 'lterm_id' ], 'accommodation_type_image', 'accommodation_type' );
                $ra = parent::query( $qa ); 
                $da = parent::fetch_array( $ra );

            	//-- GET short description
            	$st = 'SELECT a.ladditional_value FROM lumonata_additional_field AS a
                       WHERE a.lterm_id = %d AND a.ladditional_key = %s AND a.lmodule_id = ( 
                        	SELECT a2.lmodule_id FROM lumonata_module AS a2 WHERE a2.lapps = %s 
                       )';
                $qt = parent::prepare_query( $st, $d[ 'lterm_id' ], 'accommodation_type_brief', 'accommodation_type' );
                $rt = parent::query( $qt ); 
                $dt = parent::fetch_array( $rt );

                if( $dt[ 'ladditional_value' ] != '' )
                {
					$this->template->set_var( 'description', sprintf( '<p title="%s">%s</p>', $dt[ 'ladditional_value' ], $dt[ 'ladditional_value' ] ) );
                }
                else
                {

					$this->template->set_var( 'description', '' );
                }

				$this->template->set_var( 'title', $d[ 'lname' ] );
				$this->template->set_var( 'permalink', $this->global->term_link( $d ) );
				$this->template->set_var( 'image', $this->global->get_attachment_url( $da[ 'ladditional_value' ], 400, 579 ) );
				$this->template->set_var( 'image_placeholder', $this->global->get_attachment_url( $da[ 'ladditional_value' ], 10, 10 ) );

            	$this->template->Parse( 'sflBlock', 'sectionFourLoopBlock', true );
            }

            $this->template->set_var( 'heading_title', $this->data[ 'section_4_heading' ] );
            $this->template->set_var( 'heading_short_description', $this->data[ 'section_4_short_description' ] );

            $this->template->Parse( 'sfBlock', 'sectionFourBlock', false );
        }
	}

	function init_section_five()
	{
		$this->template->set_block( 'main', 'sectionFiveBlock', 'sfiBlock' );

		if( $this->data[ 'section_5_bg_image' ] != '' )
		{
            if( empty( $this->data[ 'section_5_link' ] ) )
            {
            	$this->template->set_var( 'call_to_action_link',  'javascript:;' );
            }
            else
            {
            	$this->template->set_var( 'call_to_action_link',  $this->data[ 'section_5_link' ] );
            }

			if( isset( $this->data[ 'section_5_big_title' ] ) && !empty( $this->data[ 'section_5_big_title' ] ) )
			{
				$this->template->set_var( 'big_title', sprintf( '<span class="big-title">%s</span>', $this->data[ 'section_5_big_title' ] ) );
			}

			if( isset( $this->data[ 'section_5_middle_title' ] ) && !empty( $this->data[ 'section_5_middle_title' ] ) )
			{
				$this->template->set_var( 'middle_title', sprintf( '<span class="middle-title">%s</span>', $this->data[ 'section_5_middle_title' ] ) );
			}

			if( isset( $this->data[ 'section_5_bottom_title' ] ) && !empty( $this->data[ 'section_5_bottom_title' ] ) )
			{
				$this->template->set_var( 'bottom_title', sprintf( '<span class="bottom-title">%s</span>', $this->data[ 'section_5_bottom_title' ] ) );
			}

            $this->template->set_var( 'background_image', $this->global->get_attachment_url( $this->data[ 'section_5_bg_image' ], 1920, 650 ) );
            $this->template->set_var( 'background_placeholder', $this->global->get_attachment_url( $this->data[ 'section_5_bg_image' ], 10, 10 ) );

			$this->template->Parse( 'sfiBlock', 'sectionFiveBlock', false );
		}
	}

	function init_section_six()
	{
		$this->template->set_block( 'main', 'sectionSixLoopBlock', 'sslBlock' );
		$this->template->set_block( 'main', 'sectionSixBlock', 'ssBlock' );

		$s = 'SELECT a.ltitle, a.lpost_id, a.lsef_url, a.ltype FROM lumonata_post AS a WHERE a.ltype = %s AND(
					SELECT a2.ladditional_value 
					FROM lumonata_additional_field AS a2 
					WHERE a2.lapp_id = a.lpost_id  AND a2.ladditional_key = %s
			  ) = 1 AND a.lstatus = %d AND a.llang_id = %d ORDER BY rand() LIMIT 10';
		$q = parent::prepare_query( $s, 'destination', 'destination_type', 1, $this->lang_id );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_array( $r ) )
            {
            	//-- GET featured image
            	$sa = 'SELECT a2.ladditional_value FROM lumonata_additional_field AS a2
                       WHERE a2.lapp_id = %d AND a2.ladditional_key = %s AND a2.lmodule_id = ( 
                        	SELECT a3.lmodule_id FROM lumonata_module AS a3 WHERE a3.lapps = %s 
                       )';
                $qa = parent::prepare_query( $sa, $d[ 'lpost_id' ], 'destination_image', 'destination' );
                $ra = parent::query( $qa ); 
                $da = parent::fetch_array( $ra );

            	//-- GET short description
            	$st = 'SELECT a.ladditional_value FROM lumonata_additional_field AS a
                       WHERE a.lapp_id = %d AND a.ladditional_key = %s AND a.lmodule_id = ( 
                        	SELECT a2.lmodule_id FROM lumonata_module AS a2 WHERE a2.lapps = %s 
                       )';
                $qt = parent::prepare_query( $st, $d[ 'lpost_id' ], 'destination_brief', 'destination' );
                $rt = parent::query( $qt ); 
                $dt = parent::fetch_array( $rt );

                if( $dt[ 'ladditional_value' ] != '' )
                {
					$this->template->set_var( 'description', sprintf( '<p title="%s">%s</p>', $dt[ 'ladditional_value' ], $dt[ 'ladditional_value' ] ) );
                }
                else
                {

					$this->template->set_var( 'description', '' );
                }

				$this->template->set_var( 'title', $d[ 'ltitle' ] );
				$this->template->set_var( 'permalink', $this->global->permalink( $d ) );
				$this->template->set_var( 'image', $this->global->get_attachment_url( $da[ 'ladditional_value' ], 600, null ) );
				$this->template->set_var( 'image_placeholder', $this->global->get_attachment_url( $da[ 'ladditional_value' ], 10, 10 ) );

				$this->template->Parse( 'sslBlock', 'sectionSixLoopBlock', true );
            }

            $this->template->set_var( 'heading_title', $this->data[ 'section_6_heading' ] );

            $this->template->Parse( 'ssBlock', 'sectionSixBlock', false );
        }
	}

	function init_section_seven()
	{
		$this->template->set_block( 'main', 'sectionSevenLoopBlock', 'sselBlock' );
		$this->template->set_block( 'main', 'sectionSevenBlock', 'sseBlock' );

		$s = 'SELECT a.ltitle, a.lpost_id, a.lsef_url, a.ltype FROM lumonata_post AS a WHERE a.ltype = %s AND(
					SELECT a2.ladditional_value 
					FROM lumonata_additional_field AS a2 
					WHERE a2.lapp_id = a.lpost_id  AND a2.ladditional_key = %s
			  ) = 3 AND a.lstatus = %d AND a.llang_id = %d ORDER BY rand() LIMIT 10';
		$q = parent::prepare_query( $s, 'destination', 'destination_type', 1, $this->lang_id );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_array( $r ) )
            {
            	//-- GET featured image
            	$sa = 'SELECT a2.ladditional_value FROM lumonata_additional_field AS a2
                       WHERE a2.lapp_id = %d AND a2.ladditional_key = %s AND a2.lmodule_id = ( 
                        	SELECT a3.lmodule_id FROM lumonata_module AS a3 WHERE a3.lapps = %s 
                       )';
                $qa = parent::prepare_query( $sa, $d[ 'lpost_id' ], 'destination_image', 'destination' );
                $ra = parent::query( $qa ); 
                $da = parent::fetch_assoc( $ra );

                if( isset( $da[ 'ladditional_value' ] ) === false )
                {
                	$da[ 'ladditional_value' ] = null;
                }

            	//-- GET short description
            	$st = 'SELECT a.ladditional_value FROM lumonata_additional_field AS a
                       WHERE a.lapp_id = %d AND a.ladditional_key = %s AND a.lmodule_id = ( 
                        	SELECT a2.lmodule_id FROM lumonata_module AS a2 WHERE a2.lapps = %s 
                       )';
                $qt = parent::prepare_query( $st, $d[ 'lpost_id' ], 'destination_brief', 'destination' );
                $rt = parent::query( $qt ); 
                $dt = parent::fetch_array( $rt );

                if( isset( $dt[ 'ladditional_value' ] ) && $dt[ 'ladditional_value' ] != '' )
                {
					$this->template->set_var( 'description', sprintf( '<p title="%s">%s</p>', $dt[ 'ladditional_value' ], $dt[ 'ladditional_value' ] ) );
                }
                else
                {
					$this->template->set_var( 'description', '' );
                }

				$this->template->set_var( 'title', $d[ 'ltitle' ] );
				$this->template->set_var( 'permalink', $this->global->permalink( $d ) );
				$this->template->set_var( 'image', $this->global->get_attachment_url( $da[ 'ladditional_value' ], 400, 579 ) );
				$this->template->set_var( 'image_placeholder', $this->global->get_attachment_url( $da[ 'ladditional_value' ], 10, 10 ) );

				$this->template->Parse( 'sselBlock', 'sectionSevenLoopBlock', true );
            }

            $this->template->set_var( 'heading_title', $this->data[ 'section_7_heading' ] );

            $this->template->Parse( 'sseBlock', 'sectionSevenBlock', false );
        }
	}

	function init_section_eight()
	{
		$this->template->set_block( 'main', 'sectionEightLoopBlock', 'seglBlock' );
		$this->template->set_block( 'main', 'sectionEightBlock', 'segBlock' );

		if( isset( $this->data[ 'section_8_repeater' ] ) && !empty( $this->data[ 'section_8_repeater' ] ) )
		{
			$dt = json_decode( $this->data[ 'section_8_repeater' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$num = 0;

				foreach( $dt as $d )
				{
					if( $d[ 'name' ] == '' && $d[ 'desc' ] == '' && $d[ 'review' ] == '' )
					{
						continue;
					}

					$this->template->set_var( 'item_name', $d[ 'name' ] );
					$this->template->set_var( 'item_desc', $d[ 'desc' ] );
					$this->template->set_var( 'item_star', $d[ 'star' ] );
					$this->template->set_var( 'item_review', $d[ 'review' ] );
					$this->template->set_var( 'item_avatar', $this->global->get_attachment_url( $d[ 'avatar' ], 150, 150 ) );
					$this->template->set_var( 'item_avatar_placeholder', $this->global->get_attachment_url( $d[ 'avatar' ], 10, 10 ) );

					$this->template->Parse( 'seglBlock', 'sectionEightLoopBlock', true );

					$num++;
				}

				if( $num > 0 )
				{
					$this->template->set_var( 'heading_title', $this->data[ 'section_8_heading' ] );

					$this->template->Parse( 'segBlock', 'sectionEightBlock', false );
				}
			}
		}
	}

	function init_section_nine()
	{
		$this->template->set_block( 'main', 'sectionNineBlock', 'snBlock' );

		if( ( isset( $this->data[ 'section_9_heading' ] ) && $this->data[ 'section_9_heading' ] != '' ) || ( isset( $this->data[ 'section_9_bg_image' ] ) && $this->data[ 'section_9_bg_image' ] != '' ) )
		{
            $this->template->set_var( 'heading_title', $this->data[ 'section_9_heading' ] );
            $this->template->set_var( 'description', $this->data[ 'section_9_description' ] );

			$this->template->Parse( 'snBlock', 'sectionNineBlock', false );
		}
	}

	function init_section_ten()
	{
		$this->template->set_block( 'main', 'sectionTenLoopBlock', 'stelBlock' );
		$this->template->set_block( 'main', 'sectionTenBlock', 'steBlock' );

		$token   = $this->global->get_setting_value( 'ig_token' );
		$api_url = 'https://graph.instagram.com/me/media?fields=id,media_type,media_url,permalink,thumbnail_url,username,timestamp&access_token=' . $token;

        $connection_c = curl_init();

        curl_setopt( $connection_c, CURLOPT_URL, $api_url );
        curl_setopt( $connection_c, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $connection_c, CURLOPT_TIMEOUT, 20 );

        $json_return = curl_exec( $connection_c );

        curl_close( $connection_c );
        
        $media = json_decode( $json_return );

        if( $media !== null && isset( $media->data ) && json_last_error() === JSON_ERROR_NONE )
        {
            foreach( $media->data as $i => $d )
            {  
                if( $i == 10 )
                {
                    break;
                }

            	$this->template->set_var( 'permalink', $d->permalink );
            	$this->template->set_var( 'media_url', $d->media_url );
            	$this->template->set_var( 'media_type', $d->media_type );

                $this->template->Parse( 'stelBlock', 'sectionTenLoopBlock', true );
            }

			$this->template->set_var( 'ig_account', $this->data[ 'section_10_ig_account' ] );

			$this->template->Parse( 'steBlock', 'sectionTenBlock', false );
        }
	}

	function init_section_eleven()
	{
		$this->template->set_block( 'main', 'sectionElevenBlock', 'sevBlock' );

		if( $this->data[ 'section_11_heading' ] != '' && $this->data[ 'section_11_content' ] != '' )
		{
			$this->template->set_var( 'title', $this->data[ 'section_11_heading' ] );
			$this->template->set_var( 'content', $this->data[ 'section_11_content' ] );
			$this->template->set_var( 'btn_text', $this->data[ 'section_11_btn_text' ] );
			$this->template->set_var( 'btn_link', $this->data[ 'section_11_btn_link' ] );
			$this->template->set_var( 'subtitle', nl2br( $this->data[ 'section_11_subtitle' ] ) );
			$this->template->set_var( 'brief_text', nl2br( $this->data[ 'section_11_brief_text' ] ) );

			$this->template->set_var( 'background_image', $this->global->get_attachment_url( $this->data[ 'section_11_bg_image' ], 1920, 650 ) );
			$this->template->set_var( 'background_placeholder', $this->global->get_attachment_url( $this->data[ 'section_11_bg_image' ], 10, 10 ) );

			$this->template->Parse( 'sevBlock', 'sectionElevenBlock', false );
		}
	}

	function init()
	{
		$this->lang_id   = $this->global->get_current_language_id();
		$this->lang_code = $this->global->get_current_language_code();
		$this->lang_def  = $this->global->get_setting_value( 'llanguage' );

	    $this->data  = $this->global->get_posts( array(
	    	'llang_id' => $this->lang_id,
	    	'ltype'    => 'landing',
	    	'lstatus'  => 1
	    ));

	    if( empty( $this->data ) )
	    {
		    header( 'location: ' . sprintf( '%s%s/', HT_SERVER, SITE_URL ) );

		    exit;
	    }
	}

	function init_js()
	{
		return '
		<script>
			jQuery(document).ready(function(){
				init_header_form();
				init_home_slide();
				init_lazy_load();
			});
		</script>';
	}

	function set_meta_title()
	{
		$meta_title = $this->data[ 'lmeta_title' ];

		if( empty( $meta_title ) )
		{
			$meta_title = $this->global->get_setting_value( 'lmeta_title' );

			if( empty( $meta_title ) )
			{
				$meta_title = 'Homepage - ' . $this->global->get_setting_value( 'web' );
			}
		}
		
		$this->actions->add_actions( 'meta_title', $meta_title );
	}

	function set_meta_desc()
	{
		$meta_desc = $this->global->get_setting_value( 'lmeta_desc' );

		if( empty( $meta_desc ) === false )
		{
			$meta_desc = sprintf( '<meta name="description" content="%s" />', strip_tags( $meta_desc ) );

			$this->actions->add_actions( 'meta_description', $meta_description );
		}
	}
}

?>