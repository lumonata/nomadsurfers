<?php

class import extends db
{
	function __construct( $action, $sef = '' )
	{
    	parent::__construct();

    	$this->template  = new Template( THEME_DIR );
    	$this->global    = new globalFunctions();
    	$this->adminfunc = new globalAdmin();
		$this->upload    = new upload();

		$this->actions  = $action;
		$this->sef      = $sef;
		$this->alert    = [];

		if( isset( $_POST[ 'accommodation' ] ) )
		{
			$this->accommodation = json_decode( $_POST[ 'accommodation' ], true );
		}
		else
		{
			$this->accommodation = [];
		}

		if( isset( $_POST[ 'continent' ] ) )
		{
			$this->continent  = json_decode( $_POST[ 'continent' ], true );
		}
		else
		{
			$this->continent = [ 180, 181, 182, 183, 184 ];
		}

		$this->meta = [
			'title_part_1'          => 'booking_search_category_activity',
			'page_template'        	=> 'type_of_accommodation',
			'map_coordinate'        => 'google_maps_location',
			'currency'              => 'preferred_currency',		
			'title_part_2'          => 'closest_beach_name',
			'accommodation_brief'   => 'quick_introduction',
			'accommodation_text'    => 'the_accommodation',
			'commission'            => 'commision_rate',
			'start_price'           => 'starting_price',
			'accommodation_image'   => '_thumbnail_id',
			'accommodation_address' => 'full_address',
			'fb_link'               => 'facebook_url',
			'title_part_3'          => 'closest_town',
			'accommodation_gallery' => 'full_gallery',
			'is_certified'          => 'recommended',
			'title_part_4'          => 'province',
			'website'               => 'website',
		];

		$this->dest = [
			'accommodation_city'    => 'closest_town',
			'accommodation_state'   => 'province'
		];

		$this->post = [ 
			'lmeta_desc'   => '_yoast_wpseo_metadesc',
			'lmeta_key'    => '_yoast_wpseo_focuskw',
			'lmeta_title'  => '_yoast_wpseo_title',
 			'ltitle'       => 'surf_camp_name', 
			'ldescription' => 'introduction',
		];

		$this->page_template = [
			'In Qamea Island' => 0,
			'Bungalows'		  => 0,
			'Glamping' 		  => 0,
			'Resort' 		  => 0,
			'Hostel' 		  => 0,
			'Hotel' 		  => 0,
			'Lodge' 		  => 0,
			'Rooms' 		  => 0,
			'Villa' 		  => 0,
			'Charter' 		  => 1,
			'Trips' 		  => 1,
			'Trip' 			  => 1,
			'Teens Camps' 	  => 2,
			'Surf camp' 	  => 2,
			'Camp' 			  => 2,
			'Apartment' 	  => 3,
			'Retreat' 		  => 3,
			'School' 		  => 3,
			'House' 		  => 3,
			'Club' 			  => 3,
			'Spot' 		      => 3,
		];

		$this->accommodation_type = [
			'Villa'                     => 12,
			'Hostel'                    => 12,
			'Hotel'                     => 12,
			'Apartment'                 => 12,
			'Surf Resorts'              => 12,
			'Surf Camps'                => 13,
			'Summer Camps for Children' => 14,
			'Surf Charters'             => 15,
			'Campervan'                 => 16,
			'Trips'                     => 16,
			'Diving Charter' 		    => 16,
			'Surf House' 			    => 16,
			'Retreat' 					=> 16
		];

		$this->surf_trips = [
			'Surf Charters'         => 2,
			'Exclusive Surfing'     => 3,
			'Chasing Barrels'       => 4,
			'Learn To Surf'         => 5,
			'Family Surf Trips'     => 7,
			'Uncrowded Surf Spots'  => 8,
			'Group Surfing Holiday' => 9,
			'Surf And Yoga'         => 10,
			'Kite Surfing'          => 11,
		];
	}

    function sef_url( $string = '' )
    {
        $num = $this->adminfunc->getNumRows( 'lumonata_post', 'ltitle', $string );

        if( $num > 0 )
        {
            for( $i = 2; $i <= $num + 1; $i++ )
            {
                $sef = $this->adminfunc->sef_url( $string ) . '-' . $i;
                
                if( $this->adminfunc->getNumRows( 'lumonata_post', 'lsef_url', $sef ) < 1 )
                {
                    $sef = $sef;

                    break;
                }
            }
        }
        else
        {
            $sef = $this->adminfunc->sef_url( $string );
        }
        
        return $sef;
    }

	function import_continent()
	{
		$module = $this->global->get_field_value( 'lumonata_module', 'lmodule_id', array( 'lapps' => 'destination' ) );

		$q = 'SELECT * FROM wpn_terms AS a WHERE a.term_id IN( ' . implode( ',', $this->continent ) . ' )';
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
        	$data = [];

            while( $d = parent::fetch_assoc( $r ) )
            {
	            //-- GET destination image
        		$s2 = 'SELECT a.option_value FROM wpn_options AS a WHERE a.option_name = %s';
				$q2 = parent::prepare_query( $s2, 'z_taxonomy_image' . $d[ 'term_id' ] );
		        $r2 = parent::query( $q2 );

		        if( parent::num_rows( $r2 ) > 0 )
		        {
		            while( $d2 = parent::fetch_assoc( $r2 ) )
		            {
		            	$d[ 'images' ] = $d2[ 'option_value' ];
		            }
		        }
		        else
		        {
		            $d[ 'images' ] = '';
		        }

        		//-- GET destination content
        		$s2 = 'SELECT a.description FROM wpn_term_taxonomy AS a WHERE a.term_id = %s';
				$q2 = parent::prepare_query( $s2, $d[ 'term_id' ] );
		        $r2 = parent::query( $q2 );

		        if( parent::num_rows( $r2 ) > 0 )
		        {
		            while( $d2 = parent::fetch_assoc( $r2 ) )
		            {
		            	$d[ 'description' ] = $d2[ 'description' ];
		            }
		        }
		        else
		        {
		            $d[ 'description' ] = '';
		        }

        		$data[] = $d;
            }

            if( empty( $data ) === false )
            {
            	foreach( $data as $d )
            	{
            		$term_id = $this->global->get_field_value( 'lumonata_post', 'lpost_id', array( 'lold_id' => $d[ 'term_id' ] ) );

            		if( empty( $term_id ) )
		    		{
		    			$p = parent::insert( 'lumonata_post', [
							'ldescription'  => $d[ 'description' ],
							'lold_id'       => $d[ 'term_id' ],
							'ltitle'        => $d[ 'name' ],
							'lsef_url'      => $d[ 'slug' ],
							'ltype'         => 'destination',
							'lusername'     => 'admin',
							'lcreated_by'   => 'admin',
							'lcreated_date' => time(),
							'ldlu'          => time(),
							'lstatus'       => 1,
							'llang_id'      => 1,
							'lparent_id'    => 0,
							'lorder_id'     => 0
			    		]);

				        if( !is_array( $p ) )
				        {
			        		$term_id = parent::insert_id();

				        	parent::insert( 'lumonata_additional_field', array(
								'ladditional_key'   => 'destination_type',
								'lapp_id'           => $term_id,
								'lmodule_id'        => $module,
								'ladditional_value' => 0
			        		));

						    $attach_id = '';

				    		if( empty( $d[ 'images' ] ) === false )
			        		{
			        			$file     = pathinfo( parse_url( $d[ 'images' ], PHP_URL_PATH ) );
					            $filetype = sprintf( 'image/%s', $file[ 'extension' ] );
					            $title    = $file[ 'filename' ];
					            $filename = $file[ 'basename' ];		        		

				        		if( !file_exists( IMAGE_DIR . '/Uploads/' . $filename ) )
				        		{
			            			$img_file = $this->get_content( $d[ 'images' ] );

			            			if( !empty( $img_file ) )
			            			{
					        			file_put_contents( IMAGE_DIR . '/Uploads/' . $filename, $img_file );
			            			}
					        	}

				        		if( file_exists( IMAGE_DIR . '/Uploads/' . $filename ) )
				        		{
					                $r = parent::insert( 'lumonata_attachment', array(
					                    'lcreated_by'   => 'admin',
					                    'lusername'     => 'admin',
					                    'lmimetype'     => $filetype,
					                    'lattach'       => $filename,
					                    'lapp_id'       => $term_id,
					                    'lmodule_id'    => $module,
					                    'lsef_url'      => $title,
					                    'ltitle'        => $title,
					                    'lcreated_date' => time(),
					                    'ldlu'          => time(),
					                    'lorder_id'     => 0,
										'llang_id'      => 1,
					                    'lstatus'       => 2
					                ));

					                if( is_array( $r ) )
					                {
					                    $this->upload->delete_file_thumb( $filename );
					                }
					                else
					                {		
					                	$attach_id = parent::insert_id();
					                }
				        		}
				        	}

				        	parent::insert( 'lumonata_additional_field', array(
								'ladditional_key'   => 'destination_image',
								'ladditional_value' => $attach_id,
								'lapp_id'           => $term_id,
								'lmodule_id'        => $module,
			        		));
				        }
			    	}
            	}
            }
        }
        else
        {
        	$this->alert[] = 'No continent has been import';
        }
	}

	function import_country()
	{
		$module = $this->global->get_field_value( 'lumonata_module', 'lmodule_id', array( 'lapps' => 'destination' ) );

        //-- GET destination
        $s = 'SELECT 
        		b.slug,
        		a.parent,
        		b.term_id,
        		CONVERT( binary CONVERT( b.name USING UTF8 ) USING latin1 ) AS name
			   FROM wpn_term_taxonomy AS a
			   LEFT JOIN wpn_terms AS b ON a.term_id = b.term_id
			   LEFT JOIN wpn_term_relationships AS c ON a.term_taxonomy_id = c.term_taxonomy_id
			   LEFT JOIN wpn_posts AS d ON c.object_id = d.ID
			   WHERE a.taxonomy = %s
			   AND a.parent IN( ' . implode( ',', $this->continent ) . ' ) 
			   AND d.ID IN( ' . implode( ',', $this->accommodation ) . ' )
			   GROUP BY a.term_id';
		$q = parent::prepare_query( $s, 'category' );
        $r = parent::query( $q );        

        if( parent::num_rows( $r ) > 0 )
        {
	        while( $d = parent::fetch_assoc( $r ) )
	        {
				$pterm_id = $this->global->get_field_value( 'lumonata_post', 'lpost_id', array( 'lold_id' => $d[ 'parent' ] ) );
				$dref_id  = $this->global->get_field_value( 'lumonata_post', 'lold_id', array( 'lold_id' => $d[ 'term_id' ] ) );

	        	if( empty( $dref_id ) )
	        	{
	        		//-- GET destination image
	        		$s3 = 'SELECT a.option_value FROM wpn_options AS a WHERE a.option_name = %s';
					$q3 = parent::prepare_query( $s3, 'z_taxonomy_image' . $d[ 'term_id' ] );
			        $r3 = parent::query( $q3 );

			        if( parent::num_rows( $r3 ) > 0 )
			        {
			            while( $d3 = parent::fetch_assoc( $r3 ) )
			            {
			            	$d[ 'images' ] = $d[ 'option_value' ];
			            }
			        }
			        else
			        {
			            $d[ 'images' ] = '';
			        }

	        		//-- GET destination content
	        		$s3 = 'SELECT CONVERT( binary CONVERT( a.description USING UTF8 ) USING latin1 ) AS description
	        		       FROM wpn_term_taxonomy AS a WHERE a.term_id = %s';
					$q3 = parent::prepare_query( $s3, $d[ 'term_id' ] );
			        $r3 = parent::query( $q3 );

			        if( parent::num_rows( $r3 ) > 0 )
			        {
			            while( $d3 = parent::fetch_assoc( $r3 ) )
			            {
			            	$d[ 'description' ] = $d3[ 'description' ];
			            }
			        }
			        else
			        {
			            $d[ 'description' ] = '';
			        }
				
					$p = parent::insert( 'lumonata_post', [
						'ldescription'  => $d[ 'description' ],
						'lold_id'       => $d[ 'term_id' ],
						'ltitle'        => $d[ 'name' ],
						'lsef_url'      => $d[ 'slug' ],
						'ltype'         => 'destination',
						'lusername'     => 'admin',
						'lcreated_by'   => 'admin',
						'lparent_id'    => $pterm_id,
						'lcreated_date' => time(),
						'ldlu'          => time(),
						'lstatus'       => 1,
						'llang_id'      => 1,
						'lorder_id'     => 0
		    		]);

			        if( !is_array( $p ) )
			        {
	            		$term_id = parent::insert_id();

			        	parent::insert( 'lumonata_additional_field', array(
							'ladditional_key'   => 'destination_type',
							'lapp_id'           => $term_id,
							'lmodule_id'        => $module,
							'ladditional_value' => 1
		        		));

		        		$attach_id = '';

			    		if( empty( $d[ 'images' ] ) === false )
		        		{
		        			$file     = pathinfo( parse_url( $d[ 'images' ], PHP_URL_PATH ) );
				            $filetype = sprintf( 'image/%s', $file[ 'extension' ] );
				            $title    = $file[ 'filename' ];
				            $filename = $file[ 'basename' ];		        		

			        		if( !file_exists( IMAGE_DIR . '/Uploads/' . $filename ) )
			        		{
		            			$img_file = $this->get_content( $d[ 'images' ] );

		            			if( !empty( $img_file ) )
		            			{
				        			file_put_contents( IMAGE_DIR . '/Uploads/' . $filename, $img_file );
		            			}
				        	}

			        		if( file_exists( IMAGE_DIR . '/Uploads/' . $filename ) )
			        		{
				                $r = parent::insert( 'lumonata_attachment', array(
				                    'lcreated_by'   => 'admin',
				                    'lusername'     => 'admin',
				                    'lmimetype'     => $filetype,
				                    'lattach'       => $filename,
				                    'lsef_url'      => $sef_img,
				                    'lapp_id'       => $term_id,
				                    'lmodule_id'    => $module,
				                    'ltitle'        => $title,
				                    'lcreated_date' => time(),
				                    'ldlu'          => time(),
				                    'lorder_id'     => 0,
									'llang_id'      => 1,
				                    'lstatus'       => 2
				                ));

				                if( is_array( $r ) )
				                {
				                    $this->upload->delete_file_thumb( $filename );
				                }
				                else
				                {		
				                	$attach_id = parent::insert_id();
				                }
			        		}
			        	}

			        	parent::insert( 'lumonata_additional_field', array(
							'ladditional_key'   => 'destination_image',
							'ladditional_value' => $attach_id,
							'lapp_id'           => $term_id,
							'lmodule_id'        => $module,
		        		));
			        }
	        	}
	        }
	    }
        else
        {
        	$this->alert[] = 'No country has been import';
        }
	}

	function import_post()
	{
        //-- GET all posts
        //-- INSERT to new table
		$q = 'SELECT * FROM wpn_posts AS a WHERE a.ID IN( ' . implode( ',', $this->accommodation ) . ' )';
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
		    $amodid = $this->global->get_field_value( 'lumonata_module', 'lmodule_id', array( 'lapps' => 'accommodation' ) );
		    $dmodid = $this->global->get_field_value( 'lumonata_module', 'lmodule_id', array( 'lapps' => 'destination' ) );

            while( $d = parent::fetch_assoc( $r ) )
            {
            	$ref_id = $this->global->get_field_value( 'lumonata_post', 'lold_id', array( 'lold_id' => $d[ 'ID' ] ) );

            	if( empty( $ref_id ) )
            	{			
					$p = parent::insert( 'lumonata_post', [
						'lsef_url'      => $d[ 'post_name' ],
						'lold_id'       => $d[ 'ID' ],
						'ltype'         => 'accommodation',
						'lusername'     => 'admin',
						'lcreated_by'   => 'admin',
						'lcreated_date' => time(),
						'ldlu'          => time(),
						'lstatus'       => 1,
						'llang_id'      => 1,
						'lorder_id'     => 0
		    		]);

			        if( !is_array( $p ) )
			        {
            			//-- SET variable
				        $additional = [];
				        $galleries  = [];
				        $featured   = [];
				        $postdata   = [];
				        $terms      = [];
				        $activity   = [];

	            		$post_id = parent::insert_id();

		            	//-- GET post meta
		            	$s2 = 'SELECT a.meta_key, CONVERT( binary CONVERT( a.meta_value USING UTF8 ) USING latin1 ) AS meta_value FROM wpn_postmeta AS a WHERE a.post_id = %d AND a.meta_value NOT LIKE %s';
						$q2 = parent::prepare_query( $s2, $d[ 'ID' ], '%field_%' );
				        $r2 = parent::query( $q2 );

			            while( $d2 = parent::fetch_assoc( $r2 ) )
			            {
			            	if( in_array( $d2[ 'meta_key' ], $this->meta ) )
			            	{
			            		$key = array_search( $d2[ 'meta_key' ], $this->meta );

			            		if( $key == 'page_template' )
			            		{
			            			if( $d2[ 'meta_value' ] != '' && isset( $this->page_template[ $d2[ 'meta_value' ] ] ) )
			            			{
			            				$additional[ $key ] = $this->page_template[ $d2[ 'meta_value' ] ];
			            			}
			            			else
			            			{
			            				$additional[ $key ] = 0;
			            			}
			            		}
			            		else if( $key == 'map_coordinate' && $d2[ 'meta_value' ] != '' )
			            		{
			            			$loc = unserialize( $d2[ 'meta_value' ] );

			            			$additional[ $key ] = sprintf( '%s,%s', $loc[ 'lat' ], $loc[ 'lng' ] );
			            		}
			            		else if( $key == 'currency' && $d2[ 'meta_value' ] != '' )
			            		{
			            			$additional[ $key ] = $this->global->get_field_value( 'lumonata_currency', 'lcurrency_id', [ 'lcode' => strtoupper( $d2[ 'meta_value' ] ) ] );
			            		}
			            		else if( $key == 'accommodation_gallery' && $d2[ 'meta_value' ] != '' )
			            		{
			            			$gall = unserialize( $d2[ 'meta_value' ] );

			            			foreach( $gall as $gid )
			            			{
			            				$s3 = 'SELECT a.ID, a.post_title, a.post_mime_type, b.meta_value FROM wpn_posts AS a LEFT JOIN wpn_postmeta AS b ON a.ID = b.post_id WHERE a.ID = %d AND b.meta_key = %s';
										$q3 = parent::prepare_query( $s3, $gid, '_wp_attached_file' );
								        $r3 = parent::query( $q3 );

								        if( parent::num_rows( $r3 ) > 0 )
							            {								        
								            while( $d3 = parent::fetch_assoc( $r3 ) )
								            {
								            	$galleries[] = $d3;
								            }
								        }
			            			}
			            		}
			            		else if( $key == 'accommodation_image' && $d2[ 'meta_value' ] != '' )
			            		{
		            				$s3 = 'SELECT a.ID, a.post_title, a.post_mime_type, b.meta_value FROM wpn_posts AS a LEFT JOIN wpn_postmeta AS b ON a.ID = b.post_id WHERE a.ID = %d AND b.meta_key = %s';
									$q3 = parent::prepare_query( $s3, $d2[ 'meta_value' ], '_wp_attached_file' );
							        $r3 = parent::query( $q3 );

							        if( parent::num_rows( $r3 ) > 0 )
						            {
						            	while( $d3 = parent::fetch_assoc( $r3 ) )
							            {
							            	$featured = $d3;
							            }
							        }
			            		}
			            		else
			            		{
			            			$additional[ $key ] = $d2[ 'meta_value' ];
			            		}
			            	}

			            	if( in_array( $d2[ 'meta_key' ], $this->dest ) )
			            	{
			            		$key = array_search( $d2[ 'meta_key' ], $this->dest );	

			            		$additional[ $key ] = $d2[ 'meta_value' ];
			            	}

			            	if( in_array( $d2[ 'meta_key' ], $this->post ) )
			            	{
			            		$key = array_search( $d2[ 'meta_key' ], $this->post );
			            		
			            		$postdata[ $key ] = $d2[ 'meta_value' ];
			            	}
			            }
			            
			            //-- SET accommodation region, country, type, activity, surf trips
			            $s3 = 'SELECT 
			            		b.name,
			            		a.parent,
			            		CONVERT( binary CONVERT( a.description USING UTF8 ) USING latin1 ) AS description
							   FROM wpn_term_taxonomy AS a
							   LEFT JOIN wpn_terms AS b ON a.term_id = b.term_id
							   LEFT JOIN wpn_term_relationships AS c ON a.term_taxonomy_id = c.term_taxonomy_id
							   LEFT JOIN wpn_posts AS d ON c.object_id = d.ID
							   WHERE a.taxonomy = %s AND ID = %d';
						$q3 = parent::prepare_query( $s3, 'category', $d[ 'ID' ] );
				        $r3 = parent::query( $q3 );

				        if( parent::num_rows( $r3 ) > 0 )
				        {
				            while( $d3 = parent::fetch_assoc( $r3 ) )
				            {
				            	if( in_array( $d3[ 'parent' ], $this->continent ) )
					        	{
					        		$country = $this->global->get_field_value( 'lumonata_post', 'lpost_id', array( 'ltitle' => $d3[ 'name' ], 'ltype' => 'destination' ) );
						        	$region  = $this->global->get_field_value( 'lumonata_post', 'lpost_id', array( 'lold_id' => $d3[ 'parent' ], 'ltype' => 'destination' ) );

						        	if( empty( $region ) )
						        	{
							        	$additional[ 'accommodation_continent' ] = '';
						        	}
						        	else
						        	{
						        		$additional[ 'accommodation_continent' ] = $region;
						        	}

						        	if( empty( $country ) )
						        	{
							        	$additional[ 'accommodation_country' ] = '';
						        	}
						        	else
						        	{
						        		$additional[ 'accommodation_country' ] = $country;
						        	}
						        }						        
						        else if( $d3[ 'parent' ] == 215 && isset( $this->accommodation_type[ $d3[ 'name' ] ] ) && !isset( $terms[ 'accommodation_type' ] ) )
						        {
						        	$terms[ 'accommodation_type' ][] = $this->accommodation_type[ $d3[ 'name' ] ];
						        }						        
						        else if( $d3[ 'parent' ] == 6595 && isset( $this->surf_trips[ $d3[ 'name' ] ] ) )
						        {
						        	$terms[ 'surf_trip' ][] = $this->surf_trips[ $d3[ 'name' ] ];
						        }						        
						        else if( $d3[ 'parent' ] == 216 )
						        {
						        	$activity[ substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 ) ] = [
						        		'to_do_desc'  => strip_tags( $d3[ 'description' ] ),
						        		'to_do_title' => $d3[ 'name' ],
						        		'to_do_img'   => ''
						        	];
						        }
					        }
				        }
				        else
				        {				        	
					        $additional[ 'accommodation_continent' ] = '';
					        $additional[ 'accommodation_country' ]   = '';
				        }

		            	//-- SET accommodation activity
		            	if( empty( $activity ) === false )
		            	{
		            		$additional[ 'things_to_do' ] = json_encode( $activity );
		            	}

		            	//-- SET accommodation terms
		            	if( empty( $terms ) === false )
		            	{
		            		foreach( $terms as $term )
		            		{
			            		foreach( $term as $term_id )
			            		{
                                    parent::insert( 'lumonata_post_relationship', array(
                                        'lpost_id' => $post_id,
                                        'lterm_id' => $term_id,
                                    ));
			            		}
		            		}
		            	}

		            	//-- SET accommodation state
			            if( isset( $additional[ 'accommodation_state' ] ) && $additional[ 'accommodation_state' ] != '' )
			            {
	            			//-- FIND state/province name
	            			$s3 = 'SELECT 
	            					a.lpost_id
	            			       FROM lumonata_post AS a
								   LEFT JOIN lumonata_additional_field AS b ON b.lapp_id = a.lpost_id
								   WHERE a.ltype = %s AND b.ladditional_key = %s AND b.ladditional_value = %d AND a.ltitle = %s';
							$q3 = parent::prepare_query( $s3, 'destination', 'destination_type', '2', $additional[ 'accommodation_state' ] );
							$r3 = parent::query( $q3 );

							if( parent::num_rows( $r3 ) > 0 )
							{
								$d3 = parent::fetch_assoc( $r3 );

	            				$additional[ 'accommodation_state' ] = $d3[ 'lpost_id' ];
							}
							else
							{
	            				//-- INSERT new state/province
	            				$sefurl = $this->global->get_sef_url( 'lumonata_post', 'lsef_url', $additional[ 'accommodation_state' ] );
					        	$parent = empty( $additional[ 'accommodation_country' ] ) ? 0 : $additional[ 'accommodation_country' ];

				        		$sresult = parent::insert( 'lumonata_post', array(
									'ltitle'        => $additional[ 'accommodation_state' ],
									'ltype'         => 'destination',
									'lusername'     => 'admin',
									'lcreated_by'   => 'admin',
									'lsef_url'      => $sefurl,
									'lparent_id'    => $parent,
									'lcreated_date' => time(),
									'ldlu'          => time(),
									'lstatus'       => 1,
									'llang_id'      => 1,
									'lorder_id'     => 0
				        		));

				        		if( is_array( $sresult ) )
				        		{						        		
	            					$additional[ 'accommodation_state' ] = '';
				        		}
				        		else
				        		{
				        			$state = parent::insert_id();
				        		
	            					$additional[ 'accommodation_state' ] = $state;

				        			parent::insert( 'lumonata_additional_field', array(
										'ladditional_key'   => 'destination_type',
										'lmodule_id'        => $dmodid,
										'lapp_id'           => $state,
										'ladditional_value' => 2
					        		));
				        		}
							}
			            }

		            	//-- SET accommodation city
			            if( isset( $additional[ 'accommodation_city' ] ) && $additional[ 'accommodation_city' ] != '' )
			            {
	            			//-- FIND city/area name
	            			$s3 = 'SELECT 
	            					a.lpost_id
	            			       FROM lumonata_post AS a
								   LEFT JOIN lumonata_additional_field AS b ON b.lapp_id = a.lpost_id
								   WHERE a.ltype = %s AND b.ladditional_key = %s AND b.ladditional_value = %d AND a.ltitle = %s';
							$q3 = parent::prepare_query( $s3, 'destination', 'destination_type', '3', $additional[ 'accommodation_city' ] );
							$r3 = parent::query( $q3 );

							if( parent::num_rows( $r3 ) > 0 )
							{
								$d3 = parent::fetch_assoc( $r3 );

	            				$additional[ 'accommodation_city' ] = $d3[ 'lpost_id' ];
							}
							else
							{
	            				//-- INSERT new city/area
	            				$sefurl = $this->global->get_sef_url( 'lumonata_post', 'lsef_url', $additional[ 'accommodation_city' ] );
					        	$parent = empty( $additional[ 'accommodation_state' ] ) ? 0 : $additional[ 'accommodation_state' ];

				        		$sresult = parent::insert( 'lumonata_post', array(
									'ltitle'        => $additional[ 'accommodation_city' ],
									'ltype'         => 'destination',
									'lusername'     => 'admin',
									'lcreated_by'   => 'admin',
									'lsef_url'      => $sefurl,
									'lparent_id'    => $parent,
									'lcreated_date' => time(),
									'ldlu'          => time(),
									'lstatus'       => 1,
									'llang_id'      => 1,
									'lorder_id'     => 0
				        		));

				        		if( is_array( $sresult ) )
				        		{						        		
	            					$additional[ 'accommodation_city' ] = '';
				        		}
				        		else
				        		{
				        			$city = parent::insert_id();
				        		
	            					$additional[ 'accommodation_city' ] = $city;

				        			parent::insert( 'lumonata_additional_field', array(
										'ladditional_key'   => 'destination_type',
										'lmodule_id'        => $dmodid,
										'lapp_id'           => $city,
										'ladditional_value' => 3
					        		));
				        		}
							}
			            }

		            	//-- UPDATE post data			
						parent::update( 'lumonata_post', $postdata, [ 'lpost_id' => $post_id ] );

		            	//-- INSERT post additional data
		            	foreach( $additional as $field => $value )
		            	{
				        	parent::insert( 'lumonata_additional_field', array(
								'lapp_id'           => $post_id,
								'lmodule_id'        => $amodid,
								'ladditional_key'   => $field,
								'ladditional_value' => $value
			        		));
		            	}

		            	//-- SET accommodation image
		            	if( empty( $featured ) === false )
		            	{		            		
	            			$sefurl = $this->global->get_sef_url( 'lumonata_attachment', 'lsef_url', $featured[ 'post_title' ] );
		            		$source = sprintf( 'https://nomadsurfers.com/wp-content/uploads/%s', $featured[ 'meta_value' ] );
		        			$file   = pathinfo( parse_url( $source, PHP_URL_PATH ) );

			        		if( !file_exists( IMAGE_DIR . '/Uploads/' . $file[ 'basename' ] ) )
			        		{
		            			$img_file = $this->get_content( $source );

		            			if( !empty( $img_file ) )
		            			{
				        			file_put_contents( IMAGE_DIR . '/Uploads/' . $file[ 'basename' ], $img_file );
		            			}
				        	}

			        		if( file_exists( IMAGE_DIR . '/Uploads/' . $file[ 'basename' ] ) )
			        		{
				                $res = parent::insert( 'lumonata_attachment', array(
				                    'lmimetype'     => $featured[ 'post_mime_type' ],
				                    'ltitle'        => $featured[ 'post_title' ],
				                    'lattach'       => $file[ 'basename' ],
				                    'lapp_id'       => $post_id,
				                    'lmodule_id'    => $amodid,
				                    'lsef_url'      => $sefurl,
				                    'lcreated_by'   => 'admin',
				                    'lusername'     => 'admin',
				                    'lcreated_date' => time(),
				                    'ldlu'          => time(),
				                    'lorder_id'     => 0,
									'llang_id'      => 1,
				                    'lstatus'       => 2
				                ));

				                if( is_array( $res ) )
				                {
				                    $this->upload->delete_file_thumb( $file[ 'basename' ] );
				                }
				                else
				                {		
				                	$attach_id = parent::insert_id();

						        	parent::insert( 'lumonata_additional_field', [
										'ladditional_key'   => 'accommodation_image',
										'ladditional_value' => $attach_id,
										'lapp_id'           => $post_id,
										'lmodule_id'        => $amodid,
					        		]);
				                }
			        		}
		            	}

		            	//-- SET accommodation gallery
		            	if( empty( $galleries ) === false )
		            	{
		            		$gallery = [];

		            		foreach( $galleries as $g )
		            		{		            		
		            			$sefurl = $this->global->get_sef_url( 'lumonata_attachment', 'lsef_url', $g[ 'post_title' ] );
			            		$source = sprintf( 'https://nomadsurfers.com/wp-content/uploads/%s', $g[ 'meta_value' ] );
			        			$file   = pathinfo( parse_url( $source, PHP_URL_PATH ) );

				        		if( !file_exists( IMAGE_DIR . '/Uploads/' . $file[ 'basename' ] ) )
				        		{
			            			$img_file = $this->get_content( $source );

			            			if( !empty( $img_file ) )
			            			{
					        			file_put_contents( IMAGE_DIR . '/Uploads/' . $file[ 'basename' ], $img_file );
			            			}
					        	}

				        		if( file_exists( IMAGE_DIR . '/Uploads/' . $file[ 'basename' ] ) )
				        		{
					                $res = parent::insert( 'lumonata_attachment', array(
					                    'lattach'       => $file[ 'basename' ],
					                    'lmimetype'     => $g[ 'post_mime_type' ],
					                    'ltitle'        => $g[ 'post_title' ],
					                    'lapp_id'       => $post_id,
					                    'lmodule_id'    => $amodid,
					                    'lsef_url'      => $sefurl,
					                    'lcreated_by'   => 'admin',
					                    'lusername'     => 'admin',
					                    'lcreated_date' => time(),
					                    'ldlu'          => time(),
					                    'lorder_id'     => 0,
										'llang_id'      => 1,
					                    'lstatus'       => 2
					                ));

					                if( is_array( $res ) )
					                {
					                    $this->upload->delete_file_thumb( $file[ 'basename' ] );
					                }
					                else
					                {		
					                	$gallery[] = parent::insert_id();
					                }
				        		}
		            		}

				        	parent::insert( 'lumonata_additional_field', [
								'ladditional_value' => json_encode( $gallery ),
								'ladditional_key'   => 'accommodation_gallery',
								'lapp_id'           => $post_id,
								'lmodule_id'        => $amodid,
			        		]);
		            	}
	            	}
            	}
            }
        }
        else
        {
        	$this->alert[] = 'No accommodation has been import';
        }
	}
	
	function get_content( $url )
	{
		$curl = curl_init();

        curl_setopt_array( $curl, array(
			CURLOPT_URL            => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING       => '',
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_CUSTOMREQUEST  => 'GET',
            CURLOPT_HTTPHEADER     => array( 'Accept: application/json' ),
        ));
        
        $response = curl_exec( $curl );
        
        curl_close( $curl );

        return $response;
	}
	
	function load()
	{
		if( $this->sef == 'import-data' )
		{
			if( !empty( $this->continent ) && !empty( $this->accommodation ) )
			{
				//-- IMPORT continent
				$this->import_continent();

				//-- IMPORT country
				$this->import_country();

				//-- IMPORT post
				$this->import_post();

				if( empty( $this->alert ) )
				{
					echo json_encode( [ 'result' => 'succes', 'message' => 'New data has been import' ] );
				}
				else
				{
					echo json_encode( [ 'result' => 'error', 'message' => $this->alert ] );
				}
			}
			else
			{
				echo json_encode( [ 'result' => 'error', 'message' => 'Empty parameter, no data has been import' ] );
			}

			exit;
		}
	}
}