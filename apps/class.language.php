<?php

class language extends db
{
	function __construct( $action, $apps )
	{
    	parent::__construct();

    	$this->global  = new globalFunctions();
		$this->actions = $action;
		$this->apps    = $apps;
		$this->get     = $_GET;
	}
	
	function load()
	{
		if( $this->apps == 'switch-language' )
		{
			$this->switch_language();
		}
		else if( $this->apps == 'generate-language-data' )
		{
			$this->generate_language_data();
		}
		else if( $this->apps == 'adjust-language-data' )
		{
			$this->adjust_language_data();
		}
	}

	function switch_language()
	{
		if( isset( $this->get[ 'prm' ] ) )
		{
			$prm = json_decode( base64_decode( $this->get[ 'prm' ] ), true );

			if( isset( $prm[ 'data' ] ) && isset( $prm[ 'path' ] ) )
			{
				$path = explode( '/', ltrim( $prm[ 'path' ], '/' ) );

				foreach( array_keys( $path, 'nomad', true ) as $key )
				{
				    unset( $path[ $key ] );
				}

				$mod = array_shift( $path );

				if( $this->global->is_translation( $mod ) )
				{
				    if( isset( $path[ 0 ] ) )
				    {
				        $mod = $path[ 0 ];

				        unset( $path[ 0 ] );

				        $path = array_values( $path );
				    }
				}

				if( empty( $mod ) )
				{
					if( $prm[ 'data' ][ 'llang_id' ] == $this->global->get_setting_value( 'llanguage' ) )
					{
				        header( 'location: ' . sprintf( '%s%s/', HT_SERVER, SITE_URL ) );
					}
					else
					{
				        header( 'location: ' . sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ] ) );
					}
				}
				else
				{
					$mod = urldecode( $mod );

				    if( in_array( $mod, array( 'destinations', 'destination', 'surf-trips', 'accommodation-type' ) ) )
				    {
    					//-- Desinations Modules
				    	$this->switch_destination( $mod, $path, $prm );
				    }
				    else if( in_array( $mod, array( 'accommodation' ) ) )
				    {
    					//-- Accommodation Modules
				    	$this->switch_accommodation( $mod, $path, $prm );
				    }
				    else if( in_array( $mod, array( 'blogs' ) ) )
				    {
    					//-- Blogs Modules
				    	$this->switch_blogs( $mod, $path, $prm );
				    }
				    else if( in_array( $mod, array( 'booking' ) ) )
				    {
    					//-- Booking Modules
				    	$this->switch_booking( $mod, $path, $prm );
				    }
				    else
				    {
    					//-- Pages Modules
				    	$this->switch_page( $mod, $prm );
				    }
				}
			}
		}
	}

	function switch_destination( $mod, $path = '', $prm = array() )
	{
		if( in_array( $mod, array( 'surf-trips', 'accommodation-type' ) ) )
		{
			if( empty( $path[ 0 ] ) === false )
			{
				$dta = $this->global->get_field_value( 'lumonata_post_terms', '*', array( 'lsef' => $path[ 0 ] ) );

		    	if( empty( $dta ) === false )
				{
					if( $prm[ 'data' ][ 'llang_id' ] == $this->global->get_setting_value( 'llanguage' ) )
					{
						if( empty( $dta[ 'lref_id' ] ) === false )
						{
							$sef = $this->global->get_field_value( 'lumonata_post_terms', 'lsef', array( 'lterm_id' => $dta[ 'lref_id' ] ) );
							
							if( empty( $sef ) === false )
							{
				        		header( 'location: ' . sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $mod, $sef ) );
							}
							else
							{
		        				header( 'location: ' . sprintf( '%s%s/', HT_SERVER, SITE_URL ) );
							}
						}
						else
						{
				        	header( 'location: ' . sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $mod, $path[ 0 ] ) );				    				
						}
					}
					else
					{
						if( $prm[ 'data' ][ 'llang_id' ] == $dta[ 'llang_id' ] )
						{
							header( 'location: ' . sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ], $mod, $dta[ 'lsef' ] ) );
						}
						else
						{
							if( empty( $dta[ 'lref_id' ] ) === false )
							{
								$lref_id = $dta[ 'lref_id' ];
							}
							else
							{
								$lref_id = $dta[ 'lterm_id' ];
							}

							$sef = $this->global->get_field_value( 'lumonata_post_terms', 'lsef', array( 'lref_id' => $lref_id, 'llang_id' => $prm[ 'data' ][ 'llang_id' ] ) );

							if( empty( $sef ) === false )
							{
								header( 'location: ' . sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ], $mod, $sef ) );
							}
							else
							{
								$sef = $this->global->get_field_value( 'lumonata_post_terms', 'lsef', array( 'lterm_id' => $lref_id ) );

								if( empty( $sef ) === false )
								{
					        		header( 'location: ' . sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $mod, $sef ) );
								}
								else
								{
			        				header( 'location: ' . sprintf( '%s%s/', HT_SERVER, SITE_URL ) );
								}
							}
						}
					}
				}
				else
				{
					if( $prm[ 'data' ][ 'llang_id' ] == $this->global->get_setting_value( 'llanguage' ) )
					{
				    	header( 'location: ' . sprintf( '%s%s/', HT_SERVER, SITE_URL ) );
			        }
			        else
			        {
				    	header( 'location: ' . sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ] ) );
			        }
				}
			}
		}
		else if( $mod == 'destinations' )
		{
			if( $prm[ 'data' ][ 'llang_id' ] == $this->global->get_setting_value( 'llanguage' ) )
			{
		    	header( 'location: ' . sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $mod ) );
	        }
	        else
	        {
		    	header( 'location: ' . sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ], $mod ) );
	        }
		}
		else if( $mod == 'destination' )
		{
			if( empty( $path[ 0 ] ) === false )
			{
				$dta = $this->global->get_field_value( 'lumonata_post', '*', array( 'lsef_url' => $path[ 0 ] ) );

		    	if( empty( $dta ) === false )
				{
					if( $prm[ 'data' ][ 'llang_id' ] == $this->global->get_setting_value( 'llanguage' ) )
					{
						if( empty( $dta[ 'lref_id' ] ) === false )
						{
							$sef = $this->global->get_field_value( 'lumonata_post', 'lsef_url', array( 'lpost_id' => $dta[ 'lref_id' ] ) );
							
							if( empty( $sef ) === false )
							{
				        		header( 'location: ' . sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $mod, $sef ) );
							}
							else
							{
		        				header( 'location: ' . sprintf( '%s%s/', HT_SERVER, SITE_URL ) );
							}
						}
						else
						{
				        	header( 'location: ' . sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $mod, $path[ 0 ] ) );				    				
						}
					}
					else
					{
						if( $prm[ 'data' ][ 'llang_id' ] == $dta[ 'llang_id' ] )
						{
							header( 'location: ' . sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ], $mod, $dta[ 'lsef_url' ] ) );
						}
						else
						{
							if( empty( $dta[ 'lref_id' ] ) === false )
							{
								$lref_id = $dta[ 'lref_id' ];
							}
							else
							{
								$lref_id = $dta[ 'lpost_id' ];
							}

							$sef = $this->global->get_field_value( 'lumonata_post', 'lsef_url', array( 'lref_id' => $lref_id, 'llang_id' => $prm[ 'data' ][ 'llang_id' ] ) );

							if( empty( $sef ) === false )
							{
								header( 'location: ' . sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ], $mod, $sef ) );
							}
							else
							{
								$sef = $this->global->get_field_value( 'lumonata_post', 'lsef_url', array( 'lpost_id' => $lref_id ) );

								if( empty( $sef ) === false )
								{
					        		header( 'location: ' . sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $mod, $sef ) );
								}
								else
								{
			        				header( 'location: ' . sprintf( '%s%s/', HT_SERVER, SITE_URL ) );
								}
							}
						}
					}
				}
				else
				{
					if( $prm[ 'data' ][ 'llang_id' ] == $this->global->get_setting_value( 'llanguage' ) )
					{
				    	header( 'location: ' . sprintf( '%s%s/', HT_SERVER, SITE_URL ) );
			        }
			        else
			        {
				    	header( 'location: ' . sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ] ) );
			        }
				}
			}
			else
			{
				if( $prm[ 'data' ][ 'llang_id' ] == $this->global->get_setting_value( 'llanguage' ) )
				{
			    	header( 'location: ' . sprintf( '%s%s/', HT_SERVER, SITE_URL ) );
		        }
		        else
		        {
			    	header( 'location: ' . sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ] ) );
		        }
			}
		}

	    exit;
	}

	function switch_accommodation( $mod, $path = '', $prm = array() )
	{
		if( empty( $path[ 0 ] ) === false )
		{
			if( $path[ 0 ] == 'search-result' )
			{
				if( isset( $path[ 1 ] ) && !empty( $path[ 1 ] ) )
				{
					if( $prm[ 'data' ][ 'llang_id' ] == $this->global->get_setting_value( 'llanguage' ) )
					{
						header( 'location: ' . sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $mod, $path[ 0 ], $path[ 1 ] ) );
					}
					else
					{
						header( 'location: ' . sprintf( '%s%s/%s/%s/%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ], $mod, $path[ 0 ], $path[ 1 ] ) );
					}
				}
				else
				{
					if( $prm[ 'data' ][ 'llang_id' ] == $this->global->get_setting_value( 'llanguage' ) )
					{
						header( 'location: ' . sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $mod, $path[ 0 ] ) );
					}
					else
					{
						header( 'location: ' . sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ], $mod, $path[ 0 ] ) );
					}
				}
			}
			else
			{
				$dta = $this->global->get_field_value( 'lumonata_post', '*', array( 'lsef_url' => $path[ 0 ] ) );

		    	if( empty( $dta ) === false )
				{
					if( $prm[ 'data' ][ 'llang_id' ] == $this->global->get_setting_value( 'llanguage' ) )
					{
						if( empty( $dta[ 'lref_id' ] ) === false )
						{
							$sef = $this->global->get_field_value( 'lumonata_post', 'lsef_url', array( 'lpost_id' => $dta[ 'lref_id' ] ) );
							
							if( empty( $sef ) === false )
							{
				        		header( 'location: ' . sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $mod, $sef ) );
							}
							else
							{
		        				header( 'location: ' . sprintf( '%s%s/', HT_SERVER, SITE_URL ) );
							}
						}
						else
						{
				        	header( 'location: ' . sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $mod, $path[ 0 ] ) );				    				
						}
					}
					else
					{
						if( $prm[ 'data' ][ 'llang_id' ] == $dta[ 'llang_id' ] )
						{
							header( 'location: ' . sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ], $mod, $dta[ 'lsef_url' ] ) );
						}
						else
						{
							if( empty( $dta[ 'lref_id' ] ) === false )
							{
								$lref_id = $dta[ 'lref_id' ];
							}
							else
							{
								$lref_id = $dta[ 'lpost_id' ];
							}

							$sef = $this->global->get_field_value( 'lumonata_post', 'lsef_url', array( 'lref_id' => $lref_id, 'llang_id' => $prm[ 'data' ][ 'llang_id' ] ) );

							if( empty( $sef ) === false )
							{
								header( 'location: ' . sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ], $mod, $sef ) );
							}
							else
							{
								$sef = $this->global->get_field_value( 'lumonata_post', 'lsef_url', array( 'lpost_id' => $lref_id ) );

								if( empty( $sef ) === false )
								{
					        		header( 'location: ' . sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $mod, $sef ) );
								}
								else
								{
			        				header( 'location: ' . sprintf( '%s%s/', HT_SERVER, SITE_URL ) );
								}
							}
						}
					}
				}
			}
		}
		else
		{
			if( $prm[ 'data' ][ 'llang_id' ] == $this->global->get_setting_value( 'llanguage' ) )
			{
		    	header( 'location: ' . sprintf( '%s%s/', HT_SERVER, SITE_URL ) );
	        }
	        else
	        {
		    	header( 'location: ' . sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ] ) );
	        }
		}

	    exit;
	}

	function switch_blogs( $mod, $path = '', $prm = array() )
	{
		if( in_array( $path[ 0 ], array( 'archive', 'category', 'tag' ) ) || empty( $path[ 0 ] ) || strpos( $path[ 0 ], '?page=' ) !== false )
		{
			if( empty( $path[ 0 ] ) )
			{
				if( $prm[ 'data' ][ 'llang_id' ] == $this->global->get_setting_value( 'llanguage' ) )
				{
			    	header( 'location: ' . sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $mod ) );
		        }
		        else
		        {
			    	header( 'location: ' . sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ], $mod ) );
		        }
		    }
		    else
		    {
				if( in_array( $path[ 0 ], array( 'category', 'tag' ) ) && isset( $path[ 1 ] ) && empty( $path[ 1 ] ) === false )
				{
					$dta = $this->global->get_field_value( 'lumonata_post_terms', '*', array( 'lsef' => $path[ 1 ] ) );

			    	if( empty( $dta ) === false )
					{
						if( $prm[ 'data' ][ 'llang_id' ] == $this->global->get_setting_value( 'llanguage' ) )
						{
					    	if( empty( $dta[ 'lref_id' ] ) === false )
							{
								$sef = $this->global->get_field_value( 'lumonata_post_terms', 'lsef', array( 'lterm_id' => $dta[ 'lref_id' ] ) );

								if( empty( $sef ) === false )
								{
									header( 'location: ' . sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $mod, $path[ 0 ], $sef ) );
						    	}
								else
								{
									header( 'location: ' . sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $mod ) );
								}
							}
							else
							{
								header( 'location: ' . sprintf( '%s%s/%s/%s', HT_SERVER, SITE_URL, $mod, implode( '/', $path ) ) );
							}
				        }
				        else
				        {
							if( $prm[ 'data' ][ 'llang_id' ] == $dta[ 'llang_id' ] )
							{
								header( 'location: ' . sprintf( '%s%s/%s/%s/%s', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ], $mod, implode( '/', $path ) ) );
							}
							else
							{
								if( empty( $dta[ 'lref_id' ] ) === false )
								{
									$lref_id = $dta[ 'lref_id' ];
								}
								else
								{
									$lref_id = $dta[ 'lterm_id' ];
								}

								$sef = $this->global->get_field_value( 'lumonata_post_terms', 'lsef', array( 'lref_id' => $lref_id, 'llang_id' => $prm[ 'data' ][ 'llang_id' ] ) );
								
								if( empty( $sef ) === false )
								{
									header( 'location: ' . sprintf( '%s%s/%s/%s/%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ], $mod, $path[ 0 ], $sef ) );
								}
								else
								{
									$sef = $this->global->get_field_value( 'lumonata_post_terms', 'lsef', array( 'lterm_id' => $lref_id ) );

									if( empty( $sef ) === false )
									{
						        		header( 'location: ' . sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $mod, $path[ 0 ], $sef ) );
									}
									else
									{
			        					header( 'location: ' . sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $mod ) );
									}
								}
							}
				        }
					}
					else
					{
						header( 'location: ' . sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $mod ) );
					}
				}
				else
				{
					if( $prm[ 'data' ][ 'llang_id' ] == $this->global->get_setting_value( 'llanguage' ) )
					{
				    	header( 'location: ' . sprintf( '%s%s/%s/%s', HT_SERVER, SITE_URL, $mod, implode( '/', $path ) ) );
			        }
			        else
			        {
				    	header( 'location: ' . sprintf( '%s%s/%s/%s/%s', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ], $mod, implode( '/', $path ) ) );
			        }
				}
		    }
		}
		else
		{
			$dta = $this->global->get_field_value( 'lumonata_post', '*', array( 'lsef_url' => $path[ 0 ] ) );

	    	if( empty( $dta ) === false )
			{
				if( $prm[ 'data' ][ 'llang_id' ] == $this->global->get_setting_value( 'llanguage' ) )
				{
					if( empty( $dta[ 'lref_id' ] ) === false )
					{
						$sef = $this->global->get_field_value( 'lumonata_post', 'lsef_url', array( 'lpost_id' => $dta[ 'lref_id' ] ) );

						if( empty( $sef ) === false )
						{
			        		header( 'location: ' . sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $mod, $sef ) );
						}
						else
						{
	        				header( 'location: ' . sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $mod ) );
						}
					}
					else
					{
			        	header( 'location: ' . sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $mod, $dta[ 'lsef_url' ] ) );
					}
				}
				else
				{
					if( $prm[ 'data' ][ 'llang_id' ] == $dta[ 'llang_id' ] )
					{
						header( 'location: ' . sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ], $mod, $dta[ 'lsef_url' ] ) );
					}
					else
					{
						if( empty( $dta[ 'lref_id' ] ) === false )
						{
							$lref_id = $dta[ 'lref_id' ];
						}
						else
						{
							$lref_id = $dta[ 'lpost_id' ];
						}

						$sef = $this->global->get_field_value( 'lumonata_post', 'lsef_url', array( 'lref_id' => $lref_id, 'llang_id' => $prm[ 'data' ][ 'llang_id' ] ) );

						if( empty( $sef ) === false )
						{
							header( 'location: ' . sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ], $mod, $sef ) );
						}
						else
						{
							$sef = $this->global->get_field_value( 'lumonata_post', 'lsef_url', array( 'lpost_id' => $lref_id ) );

							if( empty( $sef ) === false )
							{
				        		header( 'location: ' . sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $mod, $sef ) );
							}
							else
							{
		        				header( 'location: ' . sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $mod ) );
							}
						}
					}
				}
			}
			else
			{
				header( 'location: ' . sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $mod ) );
			}
		}

		exit;
	}

	function switch_booking( $mod, $path = '', $prm = array() )
	{
		if( $prm[ 'data' ][ 'llang_id' ] == $this->global->get_setting_value( 'llanguage' ) )
		{
	    	header( 'location: ' . sprintf( '%s%s/%s/%s', HT_SERVER, SITE_URL, $mod, implode( '/', $path ) ) );
        }
        else
        {
	    	header( 'location: ' . sprintf( '%s%s/%s/%s/%s', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ], $mod, implode( '/', $path ) ) );
        }
        
		exit;
	}

	function switch_page( $mod, $prm = array() )
	{
    	$dta = $this->global->get_field_value( 'lumonata_post', '*', array( 'lsef_url' => $mod ) );

    	if( empty( $dta ) === false )
		{
			if( $prm[ 'data' ][ 'llang_id' ] == $this->global->get_setting_value( 'llanguage' ) )
			{
				if( empty( $dta[ 'lref_id' ] ) === false )
				{
					$sef = $this->global->get_field_value( 'lumonata_post', 'lsef_url', array( 'lpost_id' => $dta[ 'lref_id' ] ) );

					if( empty( $sef ) === false )
					{
		        		header( 'location: ' . sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $sef ) );
					}
					else
					{
        				header( 'location: ' . sprintf( '%s%s/', HT_SERVER, SITE_URL ) );
					}
				}
				else
				{
		        	header( 'location: ' . sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $mod ) );
				}
			}
			else
			{
				if( $prm[ 'data' ][ 'llang_id' ] == $dta[ 'llang_id' ] )
				{
					header( 'location: ' . sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ], $dta[ 'lsef_url' ] ) );
				}
				else
				{
					if( empty( $dta[ 'lref_id' ] ) === false )
					{
						$lref_id = $dta[ 'lref_id' ];
					}
					else
					{
						$lref_id = $dta[ 'lpost_id' ];
					}

					$sef = $this->global->get_field_value( 'lumonata_post', 'lsef_url', array( 'lref_id' => $lref_id, 'llang_id' => $prm[ 'data' ][ 'llang_id' ] ) );

					if( empty( $sef ) === false )
					{
						header( 'location: ' . sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $prm[ 'data' ][ 'llang_code' ], $sef ) );
					}
					else
					{
						$sef = $this->global->get_field_value( 'lumonata_post', 'lsef_url', array( 'lpost_id' => $lref_id ) );

						if( empty( $sef ) === false )
						{
			        		header( 'location: ' . sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $sef ) );
						}
						else
						{
	        				header( 'location: ' . sprintf( '%s%s/', HT_SERVER, SITE_URL ) );
						}
					}
				}
			}
		}
		else
		{
        	header( 'location: ' . sprintf( '%s%s/', HT_SERVER, SITE_URL ) );
		}

		exit;
	}

	function generate_language_data()
	{
        parent::begin();

        $commit = 1;

		$s = 'SELECT * FROM lumonata_language AS a WHERE a.lstatus = 1 AND a.llang_id <> %d';
        $q = parent::prepare_query( $s, $this->global->get_setting_value( 'llanguage' ) );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {        	
        	while( $d = parent::fetch_assoc( $r ) )
			{
				//-- INSERT lumonata_post_terms
				$s2 = 'SELECT * FROM lumonata_post_terms AS a WHERE a.lref_id IS NULL';
		        $q2 = parent::prepare_query( $s2 );
		        $r2 = parent::query( $q2 );

		        if( parent::num_rows( $r2 ) > 0 )
		        {
		        	while( $d2 = parent::fetch_assoc( $r2 ) )
					{
						//-- Check terms already have translation or not, if yes skip this iteration
						$num = $this->global->get_num_rows( 'lumonata_post_terms', array( 'lref_id' => $d2[ 'lterm_id' ], 'llang_id' => $d[ 'llang_id' ] ) );

						if( $num > 0 )
						{
							continue;
						}

			            $result = parent::insert( 'lumonata_post_terms', array_filter( array_diff_key( array_merge( $d2, array(
							'lsef'     => $this->global->slugify( $d2[ 'lname' ] . ' ' . $d[ 'llang_code' ] ),
							'lref_id'  => $d2[ 'lterm_id' ],
							'llang_id' => $d[ 'llang_id' ]
						)), array_flip( array( 'lterm_id' ) ) ) ), function( $v ){
                            return ( $v !== null && $v !== false && $v !== '' ); 
                        });

			            if( is_array( $result ) )
			            {
			              	$commit = 0;
			            }
			            else
			            {
			            	$term_id = parent::insert_id();

			            	$terms[ $d2[ 'lterm_id' ] ] = $term_id;

						    //-- INSERT lumonata_additional_field
							$s3 = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lterm_id = %d';
					        $q3 = parent::prepare_query( $s3, $d2[ 'lterm_id' ] );
					        $r3 = parent::query( $q3 );

					        if( parent::num_rows( $r3 ) > 0 )
					        {
					        	while( $d3 = parent::fetch_assoc( $r3 ) )
								{
									$result = parent::insert( 'lumonata_additional_field', array_filter( array_diff_key( array_merge( $d3, array( 'lterm_id' => $term_id ) ), array_flip( array( 'ladditional_id' ) ) ), function( $v ){
			                            return ( $v !== null && $v !== false && $v !== '' ); 
			                        }));

						            if( is_array( $result ) )
						            {
						              	$commit = 0;
						            }
								}
							}
			            }
					}
				}

			    //-- INSERT lumonata_post
				$s2 = 'SELECT * FROM lumonata_post AS a WHERE a.lref_id IS NULL';
		        $q2 = parent::prepare_query( $s2 );
		        $r2 = parent::query( $q2 );

		        if( parent::num_rows( $r2 ) > 0 )
		        {
		        	while( $d2 = parent::fetch_assoc( $r2 ) )
					{
						//-- Skip global setting
						if( $d2[ 'ltype' ] == 'global_setting' )
						{
							continue;
						}

						//-- Check post already have translation or not, if yes skip this iteration
						$num = $this->global->get_num_rows( 'lumonata_post', array( 'lref_id' => $d2[ 'lpost_id' ], 'llang_id' => $d[ 'llang_id' ] ) );

						if( $num > 0 )
						{
							continue;
						}

			            $result = parent::insert( 'lumonata_post', array_filter( array_diff_key( array_merge( $d2, array(
							'lsef_url' => $this->global->slugify( $d2[ 'ltitle' ] . ' ' . $d[ 'llang_code' ] ),
							'lref_id'  => $d2[ 'lpost_id' ],
							'llang_id' => $d[ 'llang_id' ]
						)), array_flip( array( 'lpost_id' ) ) ) ), function( $v ){
                            return ( $v !== null && $v !== false && $v !== '' ); 
                        });

			            if( is_array( $result ) )
			            {
			              	$commit = 0;
			            }
			            else
			            {
			            	$app_id = parent::insert_id();

						    //-- INSERT lumonata_additional_field
							$s3 = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lapp_id = %d';
					        $q3 = parent::prepare_query( $s3, $d2[ 'lpost_id' ] );
					        $r3 = parent::query( $q3 );

					        if( parent::num_rows( $r3 ) > 0 )
					        {
					        	while( $d3 = parent::fetch_assoc( $r3 ) )
								{
									$result = parent::insert( 'lumonata_additional_field',array_filter( array_diff_key( array_merge( $d3, array( 'lapp_id' => $app_id ) ), array_flip( array( 'ladditional_id' ) ) ), function( $v ){
			                            return ( $v !== null && $v !== false && $v !== '' ); 
			                        }));

						            if( is_array( $result ) )
						            {
						              	$commit = 0;
						            }
								}
							}

						    //-- INSERT lumonata_post_relationship
							$s4 = 'SELECT * FROM lumonata_post_relationship AS a WHERE a.lpost_id = %d';
					        $q4 = parent::prepare_query( $s4, $d2[ 'lpost_id' ] );
					        $r4 = parent::query( $q4 );

					        if( parent::num_rows( $r4 ) > 0 )
					        {
					        	while( $d4 = parent::fetch_assoc( $r4 ) )
								{
									if( isset( $terms[ $d4[ 'lterm_id' ] ] ) )
									{
										$result = parent::insert( 'lumonata_post_relationship', array_filter( array_diff_key( array_merge( $d4, array( 'lpost_id' => $app_id, 'lterm_id' => $terms[ $d4[ 'lterm_id' ] ] ) ), array_flip( array( 'ltaxo_id' ) ) ) ), function( $v ){
				                            return ( $v !== null && $v !== false && $v !== '' ); 
				                        });

							            if( is_array( $result ) )
							            {
							              	$commit = 0;
							            }
									}
								}
					        }
			            }
					}
				}

    			$groups = array();

			    //-- INSERT lumonata_menu_group
				$s2 = 'SELECT * FROM lumonata_menu_group AS a WHERE a.lref_id IS NULL';
		        $q2 = parent::prepare_query( $s2 );
		        $r2 = parent::query( $q2 );

		        if( parent::num_rows( $r2 ) > 0 )
		        {
		        	while( $d2 = parent::fetch_assoc( $r2 ) )
					{
						//-- Check group already have translation or not, if yes skip this iteration
						$num = $this->global->get_num_rows( 'lumonata_menu_group', array( 'lref_id' => $d2[ 'lgroup_id' ], 'llang_id' => $d[ 'llang_id' ] ) );

						if( $num > 0 )
						{
							$groups[ $d2[ 'lgroup_id' ] ] = $this->global->get_field_value( 'lumonata_menu_group', 'lgroup_id', array( 'lref_id' => $d2[ 'lgroup_id' ], 'llang_id' => $d[ 'llang_id' ] ) );
						}
						else
						{
				            $result = parent::insert( 'lumonata_menu_group', array_filter( array_diff_key( array_merge( $d2, array(
								'lref_id'  => $d2[ 'lgroup_id' ],
								'llang_id' => $d[ 'llang_id' ]
							)), array_flip( array( 'lgroup_id' ) ) ) ), function( $v ){
	                            return ( $v !== null && $v !== false && $v !== '' ); 
	                        });

				            if( is_array( $result ) )
				            {
				              	$commit = 0;
				            }
				            else
				            {
				            	$groupid = parent::insert_id();

								$groups[ $d2[ 'lgroup_id' ] ] = $groupid;
				            }
						}
					}
		        }

			    //-- INSERT lumonata_menu
				$s2 = 'SELECT * FROM lumonata_menu AS a WHERE a.lref_id IS NULL';
		        $q2 = parent::prepare_query( $s2 );
		        $r2 = parent::query( $q2 );

		        if( parent::num_rows( $r2 ) > 0 )
		        {
		        	while( $d2 = parent::fetch_assoc( $r2 ) )
					{
						if( $d2[ 'lapp_id' ] != '' )
						{
							if( $d2[ 'llink_type' ] == '6' )
							{
								$lapp_id = $this->global->get_field_value( 'lumonata_post_terms', 'lterm_id', array( 'lref_id' => $d2[ 'lapp_id' ], 'llang_id' => $d[ 'llang_id' ] ) );
							}
							else
							{
								$lapp_id = $this->global->get_field_value( 'lumonata_post', 'lpost_id', array( 'lref_id' => $d2[ 'lapp_id' ], 'llang_id' => $d[ 'llang_id' ] ) );
							}

							$result = parent::insert( 'lumonata_menu', array_filter( array_diff_key( array_merge( $d2, array(
								'lgroup_id' => $groups[ $d2[ 'lgroup_id' ] ],
								'lref_id'   => $d2[ 'lmenu_id' ],
								'llang_id'  => $d[ 'llang_id' ],
								'lapp_id'   => $lapp_id
							)), array_flip( array( 'lmenu_id' ) ) ) ), function( $v ){
	                            return ( $v !== null && $v !== false && $v !== '' ); 
	                        });
						}
						else
						{
							$result = parent::insert( 'lumonata_menu', array_filter( array_diff_key( array_merge( $d2, array(
								'lgroup_id' => $groups[ $d2[ 'lgroup_id' ] ],
								'lref_id'   => $d2[ 'lmenu_id' ],
								'llang_id'  => $d[ 'llang_id' ]
							)), array_flip( array( 'lmenu_id' ) ) ) ), function( $v ){
	                            return ( $v !== null && $v !== false && $v !== '' ); 
	                        });
						}

			            if( is_array( $result ) )
			            {
			              	$commit = 0;
			            }
					}
				}
			}
		}
                
        if( $commit == 0 )
        {
            parent::rollback();

            echo json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            parent::commit();

            echo json_encode( array( 'result' => 'success' ) );
        }

        exit;
	}

	function adjust_language_data()
	{
        parent::begin();

        $commit = 1;

		$s = 'SELECT * FROM lumonata_language AS a WHERE a.lstatus = 1 AND a.llang_id <> %d';
        $q = parent::prepare_query( $s, $this->global->get_setting_value( 'llanguage' ) );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {        	
        	while( $d = parent::fetch_assoc( $r ) )
			{
				//-- ADJUST additional value
				$s2 = 'SELECT * FROM lumonata_additional_field AS a LEFT JOIN lumonata_post_terms AS b ON a.lterm_id = b.lterm_id WHERE b.llang_id = %d AND a.ladditional_key IN ( %s )';
		        $q2 = parent::prepare_query( $s2, $d[ 'llang_id' ], 'best_spot_destination' );
		        $r2 = parent::query( $q2 );

		        if( parent::num_rows( $r2 ) > 0 )
		        {
		        	while( $d2 = parent::fetch_assoc( $r2 ) )
					{
						$bs = json_decode( $d2[ 'ladditional_value' ], true );

						if( $bs !== null && json_last_error() === JSON_ERROR_NONE )
						{
							foreach( $bs as $i => $b )
							{
								$bs[ $i ] = $this->global->get_field_value( 'lumonata_post_terms', 'lpost_id', array( 'lterm_id' => $b, 'llang_id' => $d[ 'llang_id' ] ) );
							}

							$result = parent::update( 'lumonata_additional_field', array( 'ladditional_value' => json_encode( $bs ) ), array( 'ladditional_id' => $d2[ 'ladditional_id' ] ) );

				            if( is_array( $result ) )
				            {
				              	$commit = 0;
				            }
						}
					}
				}

				$s2 = 'SELECT * FROM lumonata_additional_field AS a LEFT JOIN lumonata_post AS b ON a.lapp_id = b.lpost_id WHERE b.llang_id = %d AND a.ladditional_key IN ( %s, %s, %s, %s, %s, %s, %s, %s, %s, %s )';
		        $q2 = parent::prepare_query( $s2, $d[ 'llang_id' ], 'accommodation_continent', 'accommodation_country', 'accommodation_state', 'accommodation_city', 'amenities', 'whats_included_stay', 'whats_included_transport', 'whats_included_activities', 'food_meals', 'food_dietary' );
		        $r2 = parent::query( $q2 );

		        if( parent::num_rows( $r2 ) > 0 )
		        {
		        	while( $d2 = parent::fetch_assoc( $r2 ) )
					{
						if( in_array( $d2[ 'ladditional_key' ], array( 'accommodation_continent', 'accommodation_country', 'accommodation_state', 'accommodation_city' ) ) )
						{
							$postid = $this->global->get_field_value( 'lumonata_post', 'lpost_id', array( 'lref_id' => $d2[ 'ladditional_value' ], 'llang_id' => $d[ 'llang_id' ] ) );

							$result = parent::update( 'lumonata_additional_field', array( 'ladditional_value' => $postid ), array( 'ladditional_id' => $d2[ 'ladditional_id' ] ) );

				            if( is_array( $result ) )
				            {
				              	$commit = 0;
				            }
				        }
				        else if( in_array( $d2[ 'ladditional_key' ], array( 'amenities', 'whats_included_stay', 'whats_included_transport', 'whats_included_activities', 'food_meals', 'food_dietary' ) ) )
				        {
							$bs = json_decode( $d2[ 'ladditional_value' ], true );

							if( $bs !== null && json_last_error() === JSON_ERROR_NONE )
							{
								foreach( $bs as $i => $b )
								{
									$bs[ $i ] = $this->global->get_field_value( 'lumonata_post_terms', 'lterm_id', array( 'lref_id' => $b, 'llang_id' => $d[ 'llang_id' ] ) );
								}

								$result = parent::update( 'lumonata_additional_field', array( 'ladditional_value' => json_encode( $bs ) ), array( 'ladditional_id' => $d2[ 'ladditional_id' ] ) );

					            if( is_array( $result ) )
					            {
					              	$commit = 0;
					            }
							}
				        }
					}
				}

				//-- ADJUST lumonata_post
				$s2 = 'SELECT * FROM lumonata_post AS a WHERE a.lparent_id <> %d AND a.llang_id = %d';
		        $q2 = parent::prepare_query( $s2, 0, $d[ 'llang_id' ] );
		        $r2 = parent::query( $q2 );

		        if( parent::num_rows( $r2 ) > 0 )
		        {
		        	while( $d2 = parent::fetch_assoc( $r2 ) )
					{
						$pid = $this->global->get_field_value( 'lumonata_post', 'lpost_id', array( 'lref_id' => $d2[ 'lparent_id' ], 'llang_id' => $d[ 'llang_id' ] ) );

						$result = parent::update( 'lumonata_post', array( 'lparent_id' => $pid ), array( 'lpost_id' => $d2[ 'lpost_id' ] ) );

			            if( is_array( $result ) )
			            {
			              	$commit = 0;
			            }
					}
				}

				//-- ADJUST lumonata_post_terms
				$s2 = 'SELECT * FROM lumonata_post_terms AS a WHERE a.lparent_id <> %d AND a.llang_id = %d';
		        $q2 = parent::prepare_query( $s2, 0, $d[ 'llang_id' ] );
		        $r2 = parent::query( $q2 );

		        if( parent::num_rows( $r2 ) > 0 )
		        {
		        	while( $d2 = parent::fetch_assoc( $r2 ) )
					{
						$tid = $this->global->get_field_value( 'lumonata_post_terms', 'lterm_id', array( 'lref_id' => $d2[ 'lparent_id' ], 'llang_id' => $d[ 'llang_id' ] ) );

						$result = parent::update( 'lumonata_post_terms', array( 'lparent_id' => $tid ), array( 'lterm_id' => $d2[ 'lterm_id' ] ) );

			            if( is_array( $result ) )
			            {
			              	$commit = 0;
			            }
					}
				}

				//-- ADJUST lumonata_menu
				$s2 = 'SELECT * FROM lumonata_menu AS a WHERE a.lparent_id <> %d AND a.llang_id = %d';
		        $q2 = parent::prepare_query( $s2, 0, $d[ 'llang_id' ] );
		        $r2 = parent::query( $q2 );

		        if( parent::num_rows( $r2 ) > 0 )
		        {
		        	while( $d2 = parent::fetch_assoc( $r2 ) )
					{
						$tid = $this->global->get_field_value( 'lumonata_menu', 'lmenu_id', array( 'lref_id' => $d2[ 'lparent_id' ], 'llang_id' => $d[ 'llang_id' ] ) );

						$result = parent::update( 'lumonata_menu', array( 'lparent_id' => $tid ), array( 'lmenu_id' => $d2[ 'lmenu_id' ] ) );

			            if( is_array( $result ) )
			            {
			              	$commit = 0;
			            }
					}
				}
			}
		}
                
        if( $commit == 0 )
        {
            parent::rollback();

            echo json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            parent::commit();

            echo json_encode( array( 'result' => 'success' ) );
        }

        exit;
	}
}