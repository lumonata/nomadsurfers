<?php

class members extends db
{
	function __construct( $action, $uri = '' )
	{
    	parent::__construct();

    	$this->template = new Template( MODULES_THEME_DIR );
    	$this->global   = new globalFunctions();
        $this->flash    = new flash_message();
		$this->uri      = array_filter( $uri );
		$this->actions  = $action;

		$this->import();
		$this->init();
		$this->auth();
	}
	
	function load()
	{
		if( $this->global->is_member_login() )
        {
        	if( $this->apps == 'login' || empty( $this->apps ) )
	        {
	            header( 'Location:' . HT_SERVER . SITE_URL . '/member/trips/' );

	            exit;
	        }
	        else if( $this->apps == 'trips' )
	        {
	        	return $this->trips();
	        }
	        else if( $this->apps == 'trip-details' )
	        {
	        	return $this->trip_details();
	        }
	        else if( $this->apps == 'trip-review' )
	        {
	        	return $this->trip_review();
	        }
	        else if( $this->apps == 'profile' )
	        {
	        	return $this->profile();
	        }
	        else if( $this->apps == 'security' )
	        {
	        	return $this->security();
	        }
	        else if( $this->apps == 'help' )
	        {
	        	return $this->help();
	        }
	        else if( $this->apps == 'logout' )
	        {
	        	return $this->logout();
	        }
	        else
	        {
	            header( 'Location:' . HT_SERVER . SITE_URL . '/not-found/' );

	            exit;
	        }
	    }
	    else
	    {
        	if( $this->apps == 'login' )
	        {
	        	return $this->login();
	        }
	        else
	        {
	            header( 'Location:' . HT_SERVER . SITE_URL . '/member/login/' );

	            exit;
	        }
	    }
	}

	function login()
	{
        $this->flash = new flash_message();

		$this->template->set_file( 'home', 'login.html' );

		$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

		$this->template->set_var( 'theme_url', ADMIN_THEME_URL );
		$this->template->set_var( 'message', $this->flash->message() );

		$this->template->Parse( 'mBlock', 'mainBlock', false );

		return $this->template->pparse( 'Output', 'home' );
	}

	function logout()
	{
		if( isset( $_SESSION[ MEMBER_SESSION_NAME ] ) )
		{
			unset( $_SESSION[ MEMBER_SESSION_NAME ] );
		}

		header( 'location:'. HT_SERVER . SITE_URL );

		exit;
	}

	function trips()
	{
		require_once( MODULES_THEME_DIR . '/apps/trips/class.php' );

        $this->mod = $this->global->get_module_id( 'booking' );

        $this->cls = new trips( MODULES_THEME_DIR . '/apps/trips', $this->actions );

		if( $this->ruri[ 0 ] == 'request' )
		{
			$this->template->set_file( 'request', 'request.html' );

        	$this->template->set_block( 'request', 'mainBlock', 'mBlock' );

			$this->template->set_var( 'main_body', $this->cls->request( $this->mod, 4 ) );

		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'request' );
		}
		else
		{
			$this->template->set_file( 'home', 'home.html' );

	        $this->template->set_block( 'home', 'memberMenuContent', 'mCBlock' );
	        $this->template->set_block( 'home', 'agentMenuContent', 'aCBlock' );
			$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

        	$this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );
			$this->template->set_var( 'main_body', $this->cls->view( $this->mod, 4 ) );

			$this->template->set_var( 'site_url', SITE_URL );
		    $this->template->set_var( 'admin_url', ADMIN_URL );
		    $this->template->set_var( 'image_url', IMAGE_URL );
		    $this->template->set_var( 'version', '?v=' . VERSION );
		    $this->template->set_var( 'template_url', MODULES_THEME_URL );
		    $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );

		    $this->template->set_var( 'jvs_links', $this->actions->attemp_actions( 'jvs' ) );
		    $this->template->set_var( 'css_links', $this->actions->attemp_actions( 'css' ) );

            $this->template->Parse( 'mCBlock', 'memberMenuContent', false );
		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'home' );
		}
	}

	function trip_details()
	{
		require_once( MODULES_THEME_DIR . '/apps/trips/class.php' );

        $this->mod = $this->global->get_module_id( 'booking' );

        $this->cls = new trips( MODULES_THEME_DIR . '/apps/trips', $this->actions );

		if( $this->ruri[ 0 ] == 'request' )
		{
			$this->template->set_file( 'request', 'request.html' );

        	$this->template->set_block( 'request', 'mainBlock', 'mBlock' );

        	$this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );
			$this->template->set_var( 'main_body', $this->cls->request( $this->mod, 4 ) );

		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'request' );
		}
		else
		{
			$this->template->set_file( 'home', 'home.html' );

	        $this->template->set_block( 'home', 'memberMenuContent', 'mCBlock' );
	        $this->template->set_block( 'home', 'agentMenuContent', 'aCBlock' );
			$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

			$this->template->set_var( 'main_body', $this->cls->details( $this->ruri[ 0 ], $this->mod, 4 ) );

			$this->template->set_var( 'site_url', SITE_URL );
		    $this->template->set_var( 'admin_url', ADMIN_URL );
		    $this->template->set_var( 'image_url', IMAGE_URL );
		    $this->template->set_var( 'version', '?v=' . VERSION );
		    $this->template->set_var( 'template_url', MODULES_THEME_URL );
		    $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );

		    $this->template->set_var( 'jvs_links', $this->actions->attemp_actions( 'jvs' ) );
		    $this->template->set_var( 'css_links', $this->actions->attemp_actions( 'css' ) );

            $this->template->Parse( 'mCBlock', 'memberMenuContent', false );
		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'home' );
		}
	}

	function trip_review()
	{
		require_once( MODULES_THEME_DIR . '/apps/reviews/class.php' );

        $this->mod = $this->global->get_module_id( 'accommodation_reviews' );

        $this->cls = new reviews( MODULES_THEME_DIR . '/apps/reviews', $this->actions );

		if( $this->ruri[ 0 ] == 'request' )
		{
			$this->template->set_file( 'request', 'request.html' );

        	$this->template->set_block( 'request', 'mainBlock', 'mBlock' );

        	$this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );
			$this->template->set_var( 'main_body', $this->cls->request( $this->mod, 4 ) );

		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'request' );
		}
		else
		{
			$this->template->set_file( 'home', 'home.html' );

	        $this->template->set_block( 'home', 'memberMenuContent', 'mCBlock' );
	        $this->template->set_block( 'home', 'agentMenuContent', 'aCBlock' );
			$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

        	$this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );
			$this->template->set_var( 'main_body', $this->cls->view( $this->mod, 4 ) );

			$this->template->set_var( 'site_url', SITE_URL );
		    $this->template->set_var( 'admin_url', ADMIN_URL );
		    $this->template->set_var( 'image_url', IMAGE_URL );
		    $this->template->set_var( 'version', '?v=' . VERSION );
		    $this->template->set_var( 'template_url', MODULES_THEME_URL );
		    $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );

		    $this->template->set_var( 'jvs_links', $this->actions->attemp_actions( 'jvs' ) );
		    $this->template->set_var( 'css_links', $this->actions->attemp_actions( 'css' ) );

            $this->template->Parse( 'mCBlock', 'memberMenuContent', false );
		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'home' );
		}
	}

	function profile()
	{
		require_once( MODULES_THEME_DIR . '/apps/profile/class.php' );

        $this->mod = $this->global->get_module_id( 'agent' );

        $this->cls = new profile( MODULES_THEME_DIR . '/apps/profile', $this->actions );

		if( $this->ruri[ 0 ] == 'request' )
		{
			$this->template->set_file( 'request', 'request.html' );

        	$this->template->set_block( 'request', 'mainBlock', 'mBlock' );

        	$this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );
			$this->template->set_var( 'main_body', $this->cls->request( $this->mod, 4 ) );

		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'request' );
		}
		else
		{
			$this->template->set_file( 'home', 'home.html' );

	        $this->template->set_block( 'home', 'memberMenuContent', 'mCBlock' );
	        $this->template->set_block( 'home', 'agentMenuContent', 'aCBlock' );
			$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

        	$this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );
			$this->template->set_var( 'main_body', $this->cls->profile( $this->mod, 4 ) );

			$this->template->set_var( 'site_url', SITE_URL );
		    $this->template->set_var( 'admin_url', ADMIN_URL );
		    $this->template->set_var( 'image_url', IMAGE_URL );
		    $this->template->set_var( 'version', '?v=' . VERSION );
		    $this->template->set_var( 'template_url', MODULES_THEME_URL );
		    $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );

		    $this->template->set_var( 'jvs_links', $this->actions->attemp_actions( 'jvs' ) );
		    $this->template->set_var( 'css_links', $this->actions->attemp_actions( 'css' ) );

            $this->template->Parse( 'mCBlock', 'memberMenuContent', false );
		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'home' );
		}
	}

	function security()
	{
		require_once( MODULES_THEME_DIR . '/apps/profile/class.php' );

        $this->mod = $this->global->get_module_id( 'agent' );

        $this->cls = new profile( MODULES_THEME_DIR . '/apps/profile', $this->actions );

		if( $this->ruri[ 0 ] == 'request' )
		{
			$this->template->set_file( 'request', 'request.html' );

        	$this->template->set_block( 'request', 'mainBlock', 'mBlock' );

			$this->template->set_var( 'main_body', $this->cls->request( $this->mod, 4 ) );

		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'request' );
		}
		else
		{
			$this->template->set_file( 'home', 'home.html' );

	        $this->template->set_block( 'home', 'memberMenuContent', 'mCBlock' );
	        $this->template->set_block( 'home', 'agentMenuContent', 'aCBlock' );
			$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

			$this->template->set_var( 'main_body', $this->cls->security( $this->mod, 4 ) );

			$this->template->set_var( 'site_url', SITE_URL );
		    $this->template->set_var( 'admin_url', ADMIN_URL );
		    $this->template->set_var( 'image_url', IMAGE_URL );
		    $this->template->set_var( 'version', '?v=' . VERSION );
		    $this->template->set_var( 'template_url', MODULES_THEME_URL );
		    $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );

		    $this->template->set_var( 'jvs_links', $this->actions->attemp_actions( 'jvs' ) );
		    $this->template->set_var( 'css_links', $this->actions->attemp_actions( 'css' ) );

            $this->template->Parse( 'mCBlock', 'memberMenuContent', false );
		    $this->template->Parse( 'mBlock', 'mainBlock', false );

		    return $this->template->pparse( 'Output', 'home' );
		}
	}

	function help()
	{
		require_once( MODULES_THEME_DIR . '/apps/help/class.php' );

        $this->cls = new help( MODULES_THEME_DIR . '/apps/help', $this->actions );

		$this->template->set_file( 'home', 'home.html' );

        $this->template->set_block( 'home', 'memberMenuContent', 'mCBlock' );
        $this->template->set_block( 'home', 'agentMenuContent', 'aCBlock' );
		$this->template->set_block( 'home', 'mainBlock', 'mBlock' );

		$this->template->set_var( 'main_body', $this->cls->view( 4 ) );

		$this->template->set_var( 'site_url', SITE_URL );
	    $this->template->set_var( 'admin_url', ADMIN_URL );
	    $this->template->set_var( 'image_url', IMAGE_URL );
	    $this->template->set_var( 'version', '?v=' . VERSION );
	    $this->template->set_var( 'template_url', MODULES_THEME_URL );
	    $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );

	    $this->template->set_var( 'jvs_links', $this->actions->attemp_actions( 'jvs' ) );
	    $this->template->set_var( 'css_links', $this->actions->attemp_actions( 'css' ) );

       	$this->template->Parse( 'mCBlock', 'memberMenuContent', false );
	    $this->template->Parse( 'mBlock', 'mainBlock', false );

	    return $this->template->pparse( 'Output', 'home' );
	}

	function auth()
	{
		if( isset( $this->post[ 'login' ] ) )
		{
			$user = isset( $this->post[ 'user_name' ] ) ? addslashes( $this->post[ 'user_name' ] ) : '';
		    $pwd  = isset( $this->post[ 'pwd' ] ) ? addslashes( $this->post[ 'pwd' ] ) : '';

		    $s = 'SELECT 
		    		a.luser_id,
		    		a.lusername,
		    		a.lusertype_id
		          FROM lumonata_user AS a 
		          WHERE a.lpassword = %s 
		          AND a.lusername = %s 
		          AND a.lusertype_id = %d 
		          AND a.lblock = %d 
		          AND a.lstatus = %d';
		    $q = parent::prepare_query( $s, md5( $pwd ), $user, 4, 0, 1 );
		    $r = parent::query( $q );

		    if( is_array( $r ) )
		    {
		        $this->flash->add( 'Incorrect Username or Password. Please try again' );

		        header( 'Location:' . HT_SERVER . SITE_URL . '/member/login/' );

		        exit;
		    }
		    else
		    {
		        $n = parent::num_rows( $r );

		        if( $n > 0 )
		        {
		            $d = parent::fetch_array( $r );

		            //-- Update Last Visited User
		            parent::update( 'lumonata_user', array( 'llastvisit_date' => time() ), array( 'lpassword' => md5( $pwd ), 'lusername' => $user ) );

		            //-- SET parameter
		            $param = base64_encode( 
		                json_encode( 
		                    array(
		                        'userid'   => $d[ 'luser_id' ],
		                        'username' => $d[ 'lusername' ],
		                        'usertype' => $d[ 'lusertype_id' ]
		                    )
		                )
		            );

		            //-- SET session
		            $_SESSION[ MEMBER_SESSION_NAME ] = $param;

		            //-- SET cookie
		            setcookie( MEMBER_SESSION_NAME, $param );
		            
		            //-- Redirect Process
		            header( 'Location:' . HT_SERVER . SITE_URL . '/member/' );

		            exit;
		        }
		        else
		        {
		            $this->flash->add( 'Sorry, you don\'t have permission to access this page!' );

		            header( 'Location:' . HT_SERVER . SITE_URL . '/member/login/' );

		            exit;
		        }
		    }
		}
	}

	function import()
	{
		require_once( ADMIN_DIR . '/functions/navigation.php' );
		require_once( ADMIN_DIR . '/functions/upload.php' );
		require_once( ADMIN_DIR . '/functions/globals.php' );
	}

	function init()
	{
        $this->ruri = array_reverse( $this->uri );
        $this->apps = end( $this->ruri );
        $this->post = $_POST;
	}
}