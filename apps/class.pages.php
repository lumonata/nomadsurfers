<?php

class pages extends db
{		
	function __construct( $action, $sef = '', $uri = '' )
	{
    	parent::__construct();

    	$this->template = new Template( THEME_DIR );
    	$this->global   = new globalFunctions();
		$this->uri      = array_filter( $uri );
		$this->sef      = urldecode( $sef );
		$this->actions  = $action;
		
		$this->init();
		$this->set_meta_desc();
		$this->set_meta_title();
	}
	
	function load()
	{
		if( empty( $this->data ) )
		{
			$param = [ 
				'account-activation-success', 
				'account-activation-failed',
				'accept-alternative-date'
			];

			if( in_array( $this->sef, $param ) )
			{
				return $this->load_page_custom();
			}
			else
			{
				return $this->load_page_not_found();
			}
		}
		else
		{
			if( $this->data[ 'page_template' ] == 1 )
			{
				return $this->load_page_template_1();
			}
			else if( $this->data[ 'page_template' ] == 2 )
			{
				return $this->load_page_template_2();
			}
			else if( $this->data[ 'page_template' ] == 3 )
			{
				return $this->load_page_template_3();
			}
			else if( $this->data[ 'page_template' ] == 4 )
			{
				return $this->load_page_template_4();
			}
			else if( $this->data[ 'page_template' ] == 5 )
			{
				return $this->load_page_template_5();
			}
			else if( $this->data[ 'page_template' ] == 6 )
			{
				return $this->load_page_template_6();
			}
			else if( $this->data[ 'page_template' ] == 7 )
			{
				return $this->load_page_template_7();
			}
			else if( $this->data[ 'page_template' ] == 8 )
			{
				return $this->load_page_template_8();
			}
			else if( $this->data[ 'page_template' ] == 9 )
			{
				return $this->load_page_template_9();
			}
			else
			{
				return $this->load_page();
			}
		}
	}

	function load_page()
	{
		$this->template->set_file( 'main', 'partials/single-page.html' );

		$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

		$this->template->set_var( 'post_title', $this->data[ 'ltitle' ] );
		$this->template->set_var( 'post_content', $this->data[ 'ldescription' ] );
		$this->template->set_var( 'post_brief', $this->data[ 'page_brief_text' ] );

		$this->template->set_var( 'header_image', $this->global->get_attachment_url( $this->data[ 'header_image' ], null, 462 ) );
		$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $this->data[ 'header_image' ], 10, 10 ) );

		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'site_url', SITE_URL );

		$this->actions->add_actions( 'jvs', $this->init_js() );

		return $this->template->Parse( 'mlBlock', 'mainBlock', false );
	}

	function load_page_template_1()
	{
		$this->template->set_file( 'main', 'partials/single-page-template-1.html' );

		//-- SECTION 1 content		
		$this->template->set_block( 'main', 'sectionOneLoopBlock', 'solBlock' );
		$this->template->set_block( 'main', 'sectionOneBlock', 'soBlock' );

		if( isset( $this->data[ 'section_1_1_repeater' ] ) && !empty( $this->data[ 'section_1_1_repeater' ] ) )
		{
			$dt = json_decode( $this->data[ 'section_1_1_repeater' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$i = 0;

				foreach( $dt as $d )
				{
					if( count( array_filter( $d ) ) == 0 )
					{
						continue;
					}

					$i++;

					$this->template->set_var( 'title', $d[ 'box_title' ] );
					$this->template->set_var( 'content', $d[ 'box_content' ] );

					$this->template->set_var( 'thumbnail_image', $this->global->get_attachment_url( $d[ 'box_image' ], 544, 359 ) );
					$this->template->set_var( 'thumbnail_image_placeholder', $this->global->get_attachment_url( $d[ 'box_image' ], 10, 10 ) );

					$this->template->Parse( 'solBlock', 'sectionOneLoopBlock', true );
				}

				if( $i > 0 )
				{
					$this->template->Parse( 'soBlock', 'sectionOneBlock', false );
				}
			}
		}

		//-- SECTION 2 content
		$this->template->set_block( 'main', 'sectionTwoLoopBlock', 'stlBlock' );
		$this->template->set_block( 'main', 'sectionTwoBlock', 'stBlock' );

		if( isset( $this->data[ 'section_2_1_repeater' ] ) && !empty( $this->data[ 'section_2_1_repeater' ] ) )
		{
			$dt = json_decode( $this->data[ 'section_2_1_repeater' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$i = 0;

				foreach( $dt as $d )
				{
					if( count( array_filter( $d ) ) == 0 )
					{
						continue;
					}

					$i++;

					$this->template->set_var( 'title', $d[ 'box_title' ] );
					$this->template->set_var( 'content', $d[ 'box_content' ] );

					$this->template->Parse( 'stlBlock', 'sectionTwoLoopBlock', true );
				}

				if( $i > 0 )
				{
					$this->template->set_var( 'heading_title', $this->data[ 'section_2_1_heading' ] );
					$this->template->set_var( 'short_desc', $this->data[ 'section_2_1_short_desc' ] );

					$this->template->Parse( 'stBlock', 'sectionTwoBlock', false );
				}
			}
		}

		//-- SECTION 3 content
		$this->template->set_block( 'main', 'sectionThreeLoopBlock', 'sthlBlock' );
		$this->template->set_block( 'main', 'sectionThreeBlock', 'sthBlock' );

		if( isset( $this->data[ 'section_3_1_gallery' ] ) && !empty( $this->data[ 'section_3_1_gallery' ] ) )
		{
			$dt = json_decode( $this->data[ 'section_3_1_gallery' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				foreach( $dt as $d )
				{
					$this->template->set_var( 'gallery_image', $this->global->get_attachment_url( $d, null, 241 ) );
					$this->template->set_var( 'gallery_image_placeholder', $this->global->get_attachment_url( $d, 10, 10 ) );

					$this->template->Parse( 'sthlBlock', 'sectionThreeLoopBlock', true );
				}

				$this->template->set_var( 'heading_title', $this->data[ 'section_3_1_heading' ] );

				$this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css' ) );
        		$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js' ) );

				$this->template->Parse( 'sthBlock', 'sectionThreeBlock', false );
			}
		}

		//-- SECTION 4 content
		$this->template->set_block( 'main', 'sectionTermGridLoopBlock', 'stglBlock' );
		$this->template->set_block( 'main', 'sectionTermGridBlock', 'stgBlock' );

		$terms = $this->global->get_post_terms( array(
			'lgroup'   => 'accommodation',
			'lrule'    => 'surf_trip',
			'llang_id' => $this->lang,
			'lstatus'  => 1
		));

		if( empty( $terms ) === false )
		{
			foreach( $terms as $d )
			{
				$this->template->set_var( 'title', $d[ 'lname' ] );
				$this->template->set_var( 'description', $d[ 'short_brief_content' ] );
				$this->template->set_var( 'permalink', $this->global->term_link( $d ) );
				$this->template->set_var( 'image', $this->global->get_attachment_url( $d[ 'surf_trip_image' ], 400, 579 ) );
				$this->template->set_var( 'image_placeholder', $this->global->get_attachment_url( $d[ 'surf_trip_image' ], 10, 10 ) );

				$this->template->Parse( 'stglBlock', 'sectionTermGridLoopBlock', true );
			}

			$this->template->Parse( 'stgBlock', 'sectionTermGridBlock', false );
		}

		//-- SECTION 5 content
		$this->template->set_block( 'main', 'sectionFourBlock', 'sfBlock' );

		if( isset( $this->data[ 'section_4_1_content' ] ) && !empty( $this->data[ 'section_4_1_content' ] ) )
		{
			$this->template->set_var( 'heading_title', $this->data[ 'section_4_1_heading' ] );
			$this->template->set_var( 'content', $this->data[ 'section_4_1_content' ] );

			$this->template->set_var( 'btn_text', $this->data[ 'section_4_1_btn_text' ] );
			$this->template->set_var( 'btn_link', $this->data[ 'section_4_1_btn_link' ] );

			$this->template->Parse( 'sfBlock', 'sectionFourBlock', false );
		}

		$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

		$this->template->set_var( 'post_title', $this->data[ 'ltitle' ] );
		$this->template->set_var( 'post_brief', nl2br( $this->data[ 'page_brief_text' ] ) );

		$this->template->set_var( 'header_image', $this->global->get_attachment_url( $this->data[ 'header_image' ], null, 462 ) );
		$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $this->data[ 'header_image' ], 10, 10 ) );

		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'site_url', SITE_URL );

		$this->actions->add_actions( 'jvs', $this->init_js() );

		return $this->template->Parse( 'mlBlock', 'mainBlock', false );
	}

	function load_page_template_2()
	{
		$this->template->set_file( 'main', 'partials/single-page-template-2.html' );

		//-- TAB content
		$this->template->set_block( 'main', 'sectionOneLoop2Block', 'sol2Block' );
		$this->template->set_block( 'main', 'sectionOneLoopBlock', 'solBlock' );
		$this->template->set_block( 'main', 'sectionOneBlock', 'soBlock' );

		if( isset( $this->data[ 'section_1_2_repeater' ] ) && !empty( $this->data[ 'section_1_2_repeater' ] ) )
		{
			$dt = json_decode( $this->data[ 'section_1_2_repeater' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$i = 0;

				foreach( $dt as $d )
				{
					if( count( array_filter( $d ) ) == 0 )
					{
						continue;
					}

					$this->template->set_var( 'idx', $i );
					$this->template->set_var( 'title', $d[ 'tab_title' ] );
					$this->template->set_var( 'class2', $i == 0 ? 'active' : '' );
					$this->template->set_var( 'class', $i == 0 ? 'class="active"' : '' );
					$this->template->set_var( 'content', $this->global->do_shortcode( $d[ 'tab_content' ] ) );

					$i++;

					$this->template->Parse( 'solBlock', 'sectionOneLoopBlock', true );
					$this->template->Parse( 'sol2Block', 'sectionOneLoop2Block', true );
				}

				if( $i > 0 )
				{
					$this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css' ) );
	        		$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js' ) );

					$this->template->Parse( 'soBlock', 'sectionOneBlock', false );
				}
			}
		}

		$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

		$this->template->set_var( 'post_title', $this->data[ 'ltitle' ] );
		$this->template->set_var( 'post_brief', nl2br( $this->data[ 'page_brief_text' ] ) );

		$this->template->set_var( 'header_image', $this->global->get_attachment_url( $this->data[ 'header_image' ], null, 462 ) );
		$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $this->data[ 'header_image' ], 10, 10 ) );

		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'site_url', SITE_URL );

		$this->actions->add_actions( 'jvs', $this->init_js() );

		return $this->template->Parse( 'mlBlock', 'mainBlock', false );
	}

	function load_page_template_3()
	{
		$this->template->set_file( 'main', 'partials/single-page-template-3.html' );

		//-- SECTION 1 content
		$this->template->set_block( 'main', 'sectionOneLoopBlock', 'solBlock' );
		$this->template->set_block( 'main', 'sectionOneBlock', 'soBlock' );

		if( isset( $this->data[ 'section_1_3_repeater' ] ) && !empty( $this->data[ 'section_1_3_repeater' ] ) )
		{
			$dt = json_decode( $this->data[ 'section_1_3_repeater' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$i = 0;

				foreach( $dt as $d )
				{
					if( count( array_filter( $d ) ) == 0 )
					{
						continue;
					}

					$i++;

					$this->template->set_var( 'title', $d[ 'title' ] );
					$this->template->set_var( 'description', $d[ 'description' ] );

					$this->template->Parse( 'solBlock', 'sectionOneLoopBlock', true );
				}

				if( $i > 0 )
				{
					if( empty( $this->data[ 'section_1_3_big_title' ] ) )
					{
						$this->template->set_var( 'heading_title', $this->data[ 'section_1_3_heading' ] );
					}
					else
					{
						$this->template->set_var( 'heading_title', sprintf( '<span>%s</span><small>%s</small>', $this->data[ 'section_1_3_big_title' ], $this->data[ 'section_1_3_heading' ] ) );
					}

					$this->template->Parse( 'soBlock', 'sectionOneBlock', false );
				}
			}
		}
		
		//-- SECTION 2 content
		$this->template->set_block( 'main', 'sectionTwoLoopBlock', 'stlBlock' );
		$this->template->set_block( 'main', 'sectionTwoBlock', 'stBlock' );

		if( isset( $this->data[ 'section_2_3_repeater' ] ) && !empty( $this->data[ 'section_2_3_repeater' ] ) )
		{
			$dt = json_decode( $this->data[ 'section_2_3_repeater' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$i = 0;

				foreach( $dt as $d )
				{
					if( count( array_filter( $d ) ) == 0 )
					{
						continue;
					}

					$i++;

					$this->template->set_var( 'title', $d[ 'title' ] );
					$this->template->set_var( 'subtitle', $d[ 'subtitle' ] );
					$this->template->set_var( 'description', $d[ 'description' ] );

					$this->template->set_var( 'image', $this->global->get_attachment_url( $d[ 'image' ], null, 200 ) );
					$this->template->set_var( 'image_placeholder', $this->global->get_attachment_url( $d[ 'image' ], 10, 10 ) );

					$this->template->Parse( 'stlBlock', 'sectionTwoLoopBlock', true );
				}

				if( $i > 0 )
				{
					$this->template->set_var( 'heading_title', $this->data[ 'section_2_3_heading' ] );

					$this->template->Parse( 'stBlock', 'sectionTwoBlock', false );
				}
			}
		}

		//-- SECTION 3 content
		$this->template->set_block( 'main', 'sectionThreeBlock', 'sthBlock' );

		if( isset( $this->data[ 'section_3_3_content' ] ) && !empty( $this->data[ 'section_3_3_content' ] ) )
		{
			$this->template->set_var( 'heading_title', $this->data[ 'section_3_3_heading' ] );
			$this->template->set_var( 'content', $this->data[ 'section_3_3_content' ] );

			$this->template->Parse( 'sthBlock', 'sectionThreeBlock', false );
		}

		//-- SECTION 4 content
		$this->template->set_block( 'main', 'sectionFourBlock', 'stfBlock' );

		if( isset( $this->data[ 'section_4_3_content' ] ) && !empty( $this->data[ 'section_4_3_content' ] ) )
		{
			$this->template->set_var( 'heading_title', $this->data[ 'section_4_3_heading' ] );
			$this->template->set_var( 'btn_text', $this->data[ 'section_4_3_btn_text' ] );
			$this->template->set_var( 'btn_link', $this->data[ 'section_4_3_btn_link' ] );
			$this->template->set_var( 'content', $this->data[ 'section_4_3_content' ] );

			$this->template->Parse( 'stfBlock', 'sectionFourBlock', false );
		}

		$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

		$this->template->set_var( 'post_title', $this->data[ 'ltitle' ] );
		$this->template->set_var( 'post_brief', nl2br( $this->data[ 'page_brief_text' ] ) );

		$this->template->set_var( 'header_image', $this->global->get_attachment_url( $this->data[ 'header_image' ], null, 462 ) );
		$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $this->data[ 'header_image' ], 10, 10 ) );

		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'site_url', SITE_URL );

		$this->actions->add_actions( 'jvs', $this->init_js() );

		return $this->template->Parse( 'mlBlock', 'mainBlock', false );
	}

	function load_page_template_4()
	{
		$this->template->set_file( 'main', 'partials/single-page-template-4.html' );

		//-- SECTION 1 content
		$this->template->set_block( 'main', 'sectionOneLoopBlock', 'solBlock' );
		$this->template->set_block( 'main', 'sectionOneBlock', 'soBlock' );

		if( isset( $this->data[ 'section_1_4_repeater' ] ) && !empty( $this->data[ 'section_1_4_repeater' ] ) )
		{
			$dt = json_decode( $this->data[ 'section_1_4_repeater' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$i = 0;

				foreach( $dt as $d )
				{
					if( count( array_filter( $d ) ) == 0 )
					{
						continue;
					}

					$i++;

					$this->template->set_var( 'title', $d[ 'title' ] );
					$this->template->set_var( 'float_title', $d[ 'float_title' ] );
					$this->template->set_var( 'description', $d[ 'description' ] );

					$this->template->set_var( 'image', $this->global->get_attachment_url( $d[ 'image' ], null, 250 ) );
					$this->template->set_var( 'image_placeholder', $this->global->get_attachment_url( $d[ 'image' ], 10, 10 ) );

					$this->template->Parse( 'solBlock', 'sectionOneLoopBlock', true );
				}

				if( $i > 0 )
				{
					$this->template->set_var( 'more_desc', $this->data[ 'section_1_4_more_desc' ] );
					$this->template->set_var( 'more_title', $this->data[ 'section_1_4_more_title' ] );
					$this->template->set_var( 'more_btn_text', $this->data[ 'section_1_4_more_btn_text' ] );
					$this->template->set_var( 'more_btn_link', $this->data[ 'section_1_4_more_btn_link' ] );

					$this->template->Parse( 'soBlock', 'sectionOneBlock', false );
				}
			}
		}

		//-- SECTION 2 content
		$this->template->set_block( 'main', 'sectionTwoBlock', 'stBlock' );

		if( isset( $this->data[ 'section_2_4_content' ] ) && !empty( $this->data[ 'section_2_4_content' ] ) )
		{
			$this->template->set_var( 'heading_title', $this->data[ 'section_2_4_heading' ] );
			$this->template->set_var( 'content', $this->data[ 'section_2_4_content' ] );

			$this->template->Parse( 'stBlock', 'sectionTwoBlock', false );
		}

		if( isset( $this->data[ 'section_2_4_email_to' ] ) && !empty( $this->data[ 'section_2_4_email_to' ] ) )
		{
			$this->template->set_var( 'email_to', $this->data[ 'section_2_4_email_to' ] );
		}
		else
		{
			$this->template->set_var( 'email_to', $this->global->get_setting_value( 'email' ) );
		}

		$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

		$this->template->set_var( 'post_title', $this->data[ 'ltitle' ] );
		$this->template->set_var( 'post_brief', nl2br( $this->data[ 'page_brief_text' ] ) );

		$this->template->set_var( 'header_image', $this->global->get_attachment_url( $this->data[ 'header_image' ], null, 462 ) );
		$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $this->data[ 'header_image' ], 10, 10 ) );

		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'site_url', SITE_URL );

		$this->actions->add_actions( 'jvs', $this->init_js() );

		return $this->template->Parse( 'mlBlock', 'mainBlock', false );
	}

	function load_page_template_5()
	{
		$this->template->set_file( 'main', 'partials/single-page-template-5.html' );

		//-- SECTION 1 content		
		$this->template->set_block( 'main', 'sectionOneLoopBlock', 'solBlock' );
		$this->template->set_block( 'main', 'sectionOneBlock', 'soBlock' );

		if( isset( $this->data[ 'section_1_5_repeater' ] ) && !empty( $this->data[ 'section_1_5_repeater' ] ) )
		{
			$dt = json_decode( $this->data[ 'section_1_5_repeater' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$i = 0;

				foreach( $dt as $d )
				{
					if( count( array_filter( $d ) ) == 0 )
					{
						continue;
					}

					$i++;

					$this->template->set_var( 'content', $d[ 'box_content' ] );

					$this->template->set_var( 'thumbnail_image', $this->global->get_attachment_url( $d[ 'box_image' ], 544, 359 ) );
					$this->template->set_var( 'thumbnail_image_placeholder', $this->global->get_attachment_url( $d[ 'box_image' ], 10, 10 ) );

					$this->template->Parse( 'solBlock', 'sectionOneLoopBlock', true );
				}

				if( $i > 0 )
				{
					$this->template->set_var( 'heading_title', $this->data[ 'section_1_5_heading' ] );

					$this->template->Parse( 'soBlock', 'sectionOneBlock', false );
				}
			}
		}

		//-- SECTION 2 content
		$this->template->set_block( 'main', 'sectionTwoBlock', 'stBlock' );

		if( isset( $this->data[ 'section_2_5_content' ] ) && !empty( $this->data[ 'section_2_5_content' ] ) )
		{
			$this->template->set_var( 'heading_title', $this->data[ 'section_2_5_heading' ] );
			$this->template->set_var( 'content', $this->global->do_shortcode( $this->data[ 'section_2_5_content' ] ) );

			$this->template->Parse( 'stBlock', 'sectionTwoBlock', false );
		}

		if( isset( $this->data[ 'section_2_5_email_to' ] ) && !empty( $this->data[ 'section_2_5_email_to' ] ) )
		{
			$this->template->set_var( 'email_to', $this->data[ 'section_2_5_email_to' ] );
		}
		else
		{
			$this->template->set_var( 'email_to', $this->global->get_setting_value( 'email' ) );
		}

		$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

		$this->template->set_var( 'post_title', $this->data[ 'ltitle' ] );
		$this->template->set_var( 'post_brief', nl2br( $this->data[ 'page_brief_text' ] ) );

		$this->template->set_var( 'header_image', $this->global->get_attachment_url( $this->data[ 'header_image' ], null, 462 ) );
		$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $this->data[ 'header_image' ], 10, 10 ) );

		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'site_url', SITE_URL );

		$this->actions->add_actions( 'jvs', $this->init_js() );

		return $this->template->Parse( 'mlBlock', 'mainBlock', false );
	}

	function load_page_template_6()
	{
		$this->template->set_file( 'main', 'partials/single-page-template-6.html' );

		//-- SECTION 1 content
		$this->template->set_block( 'main', 'sectionOneLoopBlock', 'solBlock' );
		$this->template->set_block( 'main', 'sectionOneBlock', 'soBlock' );

		if( isset( $this->data[ 'section_1_6_repeater' ] ) && !empty( $this->data[ 'section_1_6_repeater' ] ) )
		{
			$dt = json_decode( $this->data[ 'section_1_6_repeater' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$i = 0;

				foreach( $dt as $d )
				{
					if( count( array_filter( $d ) ) == 0 )
					{
						continue;
					}

					$i++;

					$this->template->set_var( 'title', $d[ 'title' ] );
					$this->template->set_var( 'description', $d[ 'description' ] );

					$this->template->Parse( 'solBlock', 'sectionOneLoopBlock', true );
				}

				if( $i > 0 )
				{
					if( empty( $this->data[ 'section_1_6_big_title' ] ) )
					{
						if( isset( $this->data[ 'section_1_6_heading' ] ) === false )
						{
							$this->data[ 'section_1_6_heading' ] = '';
						}

						$this->template->set_var( 'heading_title', $this->data[ 'section_1_6_heading' ] );
					}
					else
					{
						$this->template->set_var( 'heading_title', sprintf( '<span>%s</span><small>%s</small>', $this->data[ 'section_1_6_big_title' ], $this->data[ 'section_1_6_heading' ] ) );
					}

					$this->template->Parse( 'soBlock', 'sectionOneBlock', false );
				}
			}
		}

		//-- SECTION 2 content
		$this->template->set_block( 'main', 'sectionTwoBlock', 'stwBlock' );

		if( isset( $this->data[ 'section_2_6_content' ] ) && !empty( $this->data[ 'section_2_6_content' ] ) )
		{
			if( isset( $this->data[ 'section_2_6_heading' ] ) === false )
			{
				$this->data[ 'section_2_6_heading' ] = '';
			}

			$this->template->set_var( 'heading_title', $this->data[ 'section_2_6_heading' ] );
			$this->template->set_var( 'content', $this->data[ 'section_2_6_content' ] );

			$this->template->Parse( 'stwBlock', 'sectionTwoBlock', false );
		}

		//-- SECTION 3 content
		$this->template->set_block( 'main', 'sectionThreeBlock', 'sthBlock' );

		if( isset( $this->data[ 'section_3_6_content' ] ) && !empty( $this->data[ 'section_3_6_content' ] ) )
		{
			if( isset( $this->data[ 'section_3_6_heading' ] ) === false )
			{
				$this->data[ 'section_3_6_heading' ] = '';
			}

			$this->template->set_var( 'heading_title', $this->data[ 'section_3_6_heading' ] );
			$this->template->set_var( 'content', $this->data[ 'section_3_6_content' ] );

			$this->template->Parse( 'sthBlock', 'sectionThreeBlock', false );
		}

		//-- SECTION 4 content
		$this->template->set_block( 'main', 'sectionFourBlock', 'sfoBlock' );

		if( isset( $this->data[ 'section_4_6_content' ] ) && !empty( $this->data[ 'section_4_6_content' ] ) )
		{
			if( isset( $this->data[ 'section_4_6_heading' ] ) === false )
			{
				$this->data[ 'section_4_6_heading' ] = '';
			}

			$this->template->set_var( 'heading_title', $this->data[ 'section_4_6_heading' ] );
			$this->template->set_var( 'content', $this->data[ 'section_4_6_content' ] );

			$this->template->Parse( 'sfoBlock', 'sectionFourBlock', false );
		}

		//-- SECTION 5 content
		$this->template->set_block( 'main', 'sectionFiveBlock', 'sfvBlock' );

		if( isset( $this->data[ 'section_5_6_content' ] ) && !empty( $this->data[ 'section_5_6_content' ] ) )
		{
			if( isset( $this->data[ 'section_5_6_heading' ] ) === false )
			{
				$this->data[ 'section_5_6_heading' ] = '';
			}

			$this->template->set_var( 'heading_title', $this->data[ 'section_5_6_heading' ] );
			$this->template->set_var( 'content', $this->data[ 'section_5_6_content' ] );

			$this->template->Parse( 'sfvBlock', 'sectionFiveBlock', false );
		}

		//-- SECTION 6 content
		$this->template->set_block( 'main', 'sectionSixBlock', 'ssxBlock' );

		if( isset( $this->data[ 'section_6_6_content' ] ) && !empty( $this->data[ 'section_6_6_content' ] ) )
		{
			if( isset( $this->data[ 'section_6_6_heading' ] ) === false )
			{
				$this->data[ 'section_6_6_heading' ] = '';
			}

			$this->template->set_var( 'heading_title', $this->data[ 'section_6_6_heading' ] );
			$this->template->set_var( 'content', $this->data[ 'section_6_6_content' ] );

			$this->template->Parse( 'ssxBlock', 'sectionSixBlock', false );
		}

		//-- SECTION 7 content
		$this->template->set_block( 'main', 'sectionSevenLoopBlock', 'ssvlBlock' );
		$this->template->set_block( 'main', 'sectionSevenBlock', 'ssvBlock' );

		if( isset( $this->data[ 'section_7_6_repeater' ] ) && !empty( $this->data[ 'section_7_6_repeater' ] ) )
		{
			$dt = json_decode( $this->data[ 'section_7_6_repeater' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$i = 0;

				foreach( $dt as $d )
				{
					if( count( array_filter( $d ) ) == 0 )
					{
						continue;
					}

					$i++;

					$this->template->set_var( 'title', $d[ 'title' ] );
					$this->template->set_var( 'quotes', $d[ 'quotes' ] );
					$this->template->set_var( 'description', $d[ 'description' ] );

					$this->template->set_var( 'avatar_img', $this->global->get_attachment_url( $d[ 'avatar' ], 150, 150 ) );
					$this->template->set_var( 'avatar_img_placeholder', $this->global->get_attachment_url( $d[ 'avatar' ], 10, 10 ) );

					$this->template->Parse( 'ssvlBlock', 'sectionSevenLoopBlock', true );
				}

				if( $i > 0 )
				{
					if( isset( $this->data[ 'section_7_6_heading' ] ) === false )
					{
						$this->data[ 'section_7_6_heading' ] = '';
					}

					$this->template->set_var( 'heading_title', $this->data[ 'section_7_6_heading' ] );

					$this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css' ) );
	        		$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js' ) );

					$this->template->Parse( 'ssvBlock', 'sectionSevenBlock', false );
				}
			}
		}

		//-- SECTION 8 content
		$this->template->set_block( 'main', 'sectionEightBlock', 'segBlock' );

		if( isset( $this->data[ 'section_8_6_content' ] ) && !empty( $this->data[ 'section_8_6_content' ] ) )
		{
			if( isset( $this->data[ 'section_8_6_heading' ] ) === false )
			{
				$this->data[ 'section_8_6_heading' ] = '';
			}

			$this->template->set_var( 'heading_title', $this->data[ 'section_8_6_heading' ] );
			$this->template->set_var( 'content', $this->data[ 'section_8_6_content' ] );

			$this->template->Parse( 'segBlock', 'sectionEightBlock', false );
		}

		if( isset( $this->data[ 'section_8_6_email_to' ] ) && !empty( $this->data[ 'section_8_6_email_to' ] ) )
		{
			$this->template->set_var( 'email_to', $this->data[ 'section_8_6_email_to' ] );
		}
		else
		{
			$this->template->set_var( 'email_to', $this->global->get_setting_value( 'email' ) );
		}

		if( isset( $this->data[ 'page_brief_text' ] ) === false )
		{
			$this->data[ 'page_brief_text' ] = '';
		}
		
		if( isset( $this->data[ 'header_image' ] ) === false )
		{
			$this->data[ 'header_image' ] = null;
		}

		$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

		$this->template->set_var( 'post_title', $this->data[ 'ltitle' ] );
		$this->template->set_var( 'post_content', $this->data[ 'ldescription' ] );
		$this->template->set_var( 'post_brief', $this->data[ 'page_brief_text' ] );

		$this->template->set_var( 'header_image', $this->global->get_attachment_url( $this->data[ 'header_image' ], null, 462 ) );
		$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $this->data[ 'header_image' ], 10, 10 ) );

		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'site_url', SITE_URL );

		$this->actions->add_actions( 'jvs', $this->init_js() );

		return $this->template->Parse( 'mlBlock', 'mainBlock', false );
	}

	function load_page_template_7()
	{
		$this->template->set_file( 'main', 'partials/single-page-template-7.html' );

		//-- SECTION 1 content
		$this->template->set_block( 'main', 'whatsappInternationalBlock', 'sowiBlock' );
		$this->template->set_block( 'main', 'phoneInternationalBlock', 'sopiBlock' );

		$this->template->set_block( 'main', 'whatsappEuropeBlock', 'soweBlock' );
		$this->template->set_block( 'main', 'phoneEuropeBlock', 'sopeBlock' );

		$this->template->set_block( 'main', 'whatsappGlobalBlock', 'sowgBlock' );
		$this->template->set_block( 'main', 'phoneGlobalBlock', 'sopgBlock' );

		$this->template->set_block( 'main', 'whatsappAsiaBlock', 'sowaBlock' );
		$this->template->set_block( 'main', 'phoneAsiaBlock', 'sopaBlock' );

		$this->template->set_block( 'main', 'skypeAmericaBlock', 'sosaBlock' );
		$this->template->set_block( 'main', 'skypeEuropeBlock', 'soseBlock' );
		$this->template->set_block( 'main', 'skypeAsiaBlock', 'sosiBlock' );
		
		$this->template->set_block( 'main', 'sectionOneBlock', 'soBlock' );

		if( isset( $this->data[ 'section_1_7_content' ] ) && !empty( $this->data[ 'section_1_7_content' ] ) )
		{
			//-- GET Phone, Whatsapp, and Skype setting
			$phone_international = $this->global->get_setting_value( 'phone_international' );
			$phone_global        = $this->global->get_setting_value( 'phone_global' );
			$phone_europe        = $this->global->get_setting_value( 'phone_europe' );
			$phone_asia          = $this->global->get_setting_value( 'phone_asia' );

			$wa_international    = $this->global->get_setting_value( 'wa_international' );
			$wa_global           = $this->global->get_setting_value( 'wa_global' );
			$wa_europe           = $this->global->get_setting_value( 'wa_europe' );
			$wa_asia             = $this->global->get_setting_value( 'wa_asia' );

			$skype_america       = $this->global->get_setting_value( 'skype_america' );
			$skype_europe        = $this->global->get_setting_value( 'skype_europe' );
			$skype_asia          = $this->global->get_setting_value( 'skype_asia' );
 
			if( $phone_international == '' && $wa_international  == '' )
			{
				$this->template->set_var( 'call_international_css', 'sr-only' );
			}
			else
			{
				$this->template->set_var( 'call_international_css', 'call-us-content' );

				if( $phone_international !== '' )
				{
					$this->template->set_var( 'tel_international', $this->global->get_format_phone_number( $phone_international, 'tel' ) );
					$this->template->set_var( 'phone_international', $this->global->get_format_phone_number( $phone_international ) );

					$this->template->Parse( 'sopiBlock', 'phoneInternationalBlock', false );
				}
				
				if( $wa_international !== '' )
				{
					$this->template->set_var( 'chat_international', $this->global->get_format_phone_number( $wa_international, 'wa' ) );
					$this->template->set_var( 'wa_international', $this->global->get_format_phone_number( $wa_international ) );

					$this->template->Parse( 'sowiBlock', 'whatsappInternationalBlock', false );
				}
			}

			if( $phone_global == '' && $wa_global  == '' )
			{
				$this->template->set_var( 'call_global_css', 'sr-only' );
			}
			else
			{
				$this->template->set_var( 'call_global_css', 'call-us-content' );

				if( $phone_global !== '' )
				{
					$this->template->set_var( 'tel_global', $this->global->get_format_phone_number( $phone_global, 'tel' ) );
					$this->template->set_var( 'phone_global', $this->global->get_format_phone_number( $phone_global ) );

					$this->template->Parse( 'sopgBlock', 'phoneGlobalBlock', false );
				}
				
				if( $wa_global !== '' )
				{
					$this->template->set_var( 'chat_global', $this->global->get_format_phone_number( $wa_global, 'wa' ) );
					$this->template->set_var( 'wa_global', $this->global->get_format_phone_number( $wa_global ) );

					$this->template->Parse( 'sowgBlock', 'whatsappGlobalBlock', false );
				}
			}

			if( $phone_europe == '' && $wa_europe  == '' )
			{
				$this->template->set_var( 'call_europe_css', 'sr-only' );
			}
			else
			{
				$this->template->set_var( 'call_europe_css', 'call-us-content' );

				if( $phone_europe !== '' )
				{
					$this->template->set_var( 'tel_europe', $this->global->get_format_phone_number( $phone_europe, 'tel' ) );
					$this->template->set_var( 'phone_europe', $this->global->get_format_phone_number( $phone_europe ) );

					$this->template->Parse( 'sopeBlock', 'phoneEuropeBlock', false );
				}
				
				if( $wa_europe !== '' )
				{
					$this->template->set_var( 'chat_europe', $this->global->get_format_phone_number( $wa_europe, 'wa' ) );
					$this->template->set_var( 'wa_europe', $this->global->get_format_phone_number( $wa_europe ) );

					$this->template->Parse( 'soweBlock', 'whatsappEuropeBlock', false );
				}
			}

			if( $phone_asia == '' && $wa_asia  == '' )
			{
				$this->template->set_var( 'call_asia_css', 'sr-only' );
			}
			else
			{
				$this->template->set_var( 'call_asia_css', 'call-us-content' );

				if( $phone_asia !== '' )
				{
					$this->template->set_var( 'tel_asia', $this->global->get_format_phone_number( $phone_asia, 'tel' ) );
					$this->template->set_var( 'phone_asia', $this->global->get_format_phone_number( $phone_asia ) );

					$this->template->Parse( 'sopaBlock', 'phoneAsiaBlock', false );
				}
				
				if( $wa_asia !== '' )
				{
					$this->template->set_var( 'chat_asia', $this->global->get_format_phone_number( $wa_asia, 'wa' ) );
					$this->template->set_var( 'wa_asia', $this->global->get_format_phone_number( $wa_asia ) );

					$this->template->Parse( 'sowaBlock', 'whatsappAsiaBlock', false );
				}
			}

			if( $skype_america == '' && $skype_europe  == '' && $skype_asia  == '' )
			{
				$this->template->set_var( 'call_skype_css', 'sr-only' );
			}
			else
			{
				$this->template->set_var( 'call_skype_css', 'call-us-skype' );

				if( $skype_america !== '' )
				{
					$this->template->set_var( 'skype_america', $this->global->get_format_skype_link( $skype_america ) );

					$this->template->Parse( 'sosaBlock', 'skypeAmericaBlock', false );
				}
				
				if( $skype_europe !== '' )
				{
					$this->template->set_var( 'skype_europe', $this->global->get_format_skype_link( $skype_europe ) );

					$this->template->Parse( 'soseBlock', 'skypeEuropeBlock', false );
				}
				
				if( $skype_asia !== '' )
				{
					$this->template->set_var( 'skype_asia', $this->global->get_format_skype_link( $skype_asia ) );

					$this->template->Parse( 'sosiBlock', 'skypeAsiaBlock', false );
				}
			}			

			$this->template->set_var( 'email_booking', $this->global->get_setting_value( 'email_booking' ) );
			$this->template->set_var( 'heading_title', $this->data[ 'section_1_7_heading' ] );
			$this->template->set_var( 'content', $this->data[ 'section_1_7_content' ] );
			$this->template->set_var( 'button', $this->data[ 'section_1_7_btn_text' ] );

			$this->template->Parse( 'soBlock', 'sectionOneBlock', false );
		}

		if( isset( $this->data[ 'section_1_7_email_to' ] ) && !empty( $this->data[ 'section_1_7_email_to' ] ) )
		{
			$this->template->set_var( 'email_to', $this->data[ 'section_1_7_email_to' ] );
		}
		else
		{
			$this->template->set_var( 'email_to', $this->global->get_setting_value( 'email' ) );
		}

		//-- SECTION 2 content
		$this->template->set_block( 'main', 'sectionTwoLoopBlock', 'stlBlock' );
		$this->template->set_block( 'main', 'sectionTwoBlock', 'stBlock' );

		if( isset( $this->data[ 'section_2_7_repeater' ] ) && !empty( $this->data[ 'section_2_7_repeater' ] ) )
		{
			$dt = json_decode( $this->data[ 'section_2_7_repeater' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$i = 0;

				foreach( $dt as $d )
				{
					if( count( array_filter( $d ) ) == 0 )
					{
						continue;
					}

					$i++;

					$this->template->set_var( 'title', $d[ 'title' ] );
					$this->template->set_var( 'description', $d[ 'description' ] );

					$this->template->Parse( 'stlBlock', 'sectionTwoLoopBlock', true );
				}

				if( $i > 0 )
				{
					if( empty( $this->data[ 'section_2_7_big_title' ] ) )
					{
						$this->template->set_var( 'heading_title', $this->data[ 'section_2_7_heading' ] );
					}
					else
					{
						$this->template->set_var( 'heading_title', sprintf( '<span>%s</span><small>%s</small>', $this->data[ 'section_2_7_big_title' ], $this->data[ 'section_2_7_heading' ] ) );
					}

					$this->template->Parse( 'stBlock', 'sectionTwoBlock', false );
				}
			}
		}

		//-- SECTION 3 content
		$this->template->set_block( 'main', 'sectionThreeBlock', 'sthBlock' );

		if( isset( $this->data[ 'section_3_7_content' ] ) && !empty( $this->data[ 'section_3_7_content' ] ) )
		{
			$this->template->set_var( 'heading_title', $this->data[ 'section_3_7_heading' ] );
			$this->template->set_var( 'content', $this->data[ 'section_3_7_content' ] );

			$this->template->set_var( 'image', $this->global->get_attachment_url( $this->data[ 'section_3_7_image' ], 600, null ) );
			$this->template->set_var( 'image_placeholder', $this->global->get_attachment_url( $this->data[ 'section_3_7_image' ], 10, 10 ) );

			$this->template->Parse( 'sthBlock', 'sectionThreeBlock', false );
		}

		$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

		$this->template->set_var( 'post_title', $this->data[ 'ltitle' ] );
		$this->template->set_var( 'post_brief', $this->data[ 'page_brief_text' ] );

		$this->template->set_var( 'header_image', $this->global->get_attachment_url( $this->data[ 'header_image' ], null, 462 ) );
		$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $this->data[ 'header_image' ], 10, 10 ) );

		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'site_url', SITE_URL );

		$this->actions->add_actions( 'jvs', $this->init_js() );

		return $this->template->Parse( 'mlBlock', 'mainBlock', false );
	}

	function load_page_template_8()
	{
		$this->template->set_file( 'main', 'partials/single-page-template-8.html' );

		//-- TAB content
		$this->template->set_block( 'main', 'sectionOneLoop2Block', 'sol2Block' );
		$this->template->set_block( 'main', 'sectionOneLoopBlock', 'solBlock' );
		$this->template->set_block( 'main', 'sectionOneBlock', 'soBlock' );

		if( isset( $this->data[ 'section_1_8_repeater' ] ) && !empty( $this->data[ 'section_1_8_repeater' ] ) )
		{
			$dt = json_decode( $this->data[ 'section_1_8_repeater' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$i = 0;

				foreach( $dt as $d )
				{
					if( count( array_filter( $d ) ) == 0 )
					{
						continue;
					}

					$this->template->set_var( 'idx', $i );
					$this->template->set_var( 'title', $d[ 'tab_title' ] );
					$this->template->set_var( 'class2', $i == 0 ? 'active' : '' );
					$this->template->set_var( 'class', $i == 0 ? 'class="active"' : '' );
					$this->template->set_var( 'content', $this->global->do_shortcode( $d[ 'tab_content' ] ) );

					$i++;

					$this->template->Parse( 'solBlock', 'sectionOneLoopBlock', true );
					$this->template->Parse( 'sol2Block', 'sectionOneLoop2Block', true );
				}

				if( $i > 0 )
				{
					$this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css' ) );
	        		$this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js' ) );

					$this->template->Parse( 'soBlock', 'sectionOneBlock', false );
				}
			}
		}

		$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

		$this->template->set_var( 'post_title', $this->data[ 'ltitle' ] );
		$this->template->set_var( 'post_brief', nl2br( $this->data[ 'page_brief_text' ] ) );

		$this->template->set_var( 'header_image', $this->global->get_attachment_url( $this->data[ 'header_image' ], null, 462 ) );
		$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $this->data[ 'header_image' ], 10, 10 ) );

		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'site_url', SITE_URL );

		$this->actions->add_actions( 'jvs', $this->init_js() );

		return $this->template->Parse( 'mlBlock', 'mainBlock', false );
	}

	function load_page_template_9()
	{
		$this->template->set_file( 'main', 'partials/single-page-template-9.html' );

		//-- SECTION 1 content
		$this->template->set_block( 'main', 'whatsappInternationalBlock', 'sowiBlock' );
		$this->template->set_block( 'main', 'phoneInternationalBlock', 'sopiBlock' );

		$this->template->set_block( 'main', 'whatsappEuropeBlock', 'soweBlock' );
		$this->template->set_block( 'main', 'phoneEuropeBlock', 'sopeBlock' );

		$this->template->set_block( 'main', 'whatsappGlobalBlock', 'sowgBlock' );
		$this->template->set_block( 'main', 'phoneGlobalBlock', 'sopgBlock' );

		$this->template->set_block( 'main', 'whatsappAsiaBlock', 'sowaBlock' );
		$this->template->set_block( 'main', 'phoneAsiaBlock', 'sopaBlock' );

		$this->template->set_block( 'main', 'skypeAmericaBlock', 'sosaBlock' );
		$this->template->set_block( 'main', 'skypeEuropeBlock', 'soseBlock' );
		$this->template->set_block( 'main', 'skypeAsiaBlock', 'sosiBlock' );

		$this->template->set_block( 'main', 'sectionOneBlock', 'soBlock' );

		if( isset( $this->data[ 'section_1_9_content' ] ) && !empty( $this->data[ 'section_1_9_content' ] ) )
		{
			//-- GET Phone, Whatsapp, and Skype setting
			$phone_international = $this->global->get_setting_value( 'phone_international' );
			$phone_global        = $this->global->get_setting_value( 'phone_global' );
			$phone_europe        = $this->global->get_setting_value( 'phone_europe' );
			$phone_asia          = $this->global->get_setting_value( 'phone_asia' );

			$wa_international    = $this->global->get_setting_value( 'wa_international' );
			$wa_global           = $this->global->get_setting_value( 'wa_global' );
			$wa_europe           = $this->global->get_setting_value( 'wa_europe' );
			$wa_asia             = $this->global->get_setting_value( 'wa_asia' );

			$skype_america       = $this->global->get_setting_value( 'skype_america' );
			$skype_europe        = $this->global->get_setting_value( 'skype_europe' );
			$skype_asia          = $this->global->get_setting_value( 'skype_asia' );
 
			if( $phone_international == '' && $wa_international  == '' )
			{
				$this->template->set_var( 'call_international_css', 'sr-only' );
			}
			else
			{
				$this->template->set_var( 'call_international_css', 'call-us-content' );

				if( $phone_international !== '' )
				{
					$this->template->set_var( 'tel_international', $this->global->get_format_phone_number( $phone_international, 'tel' ) );
					$this->template->set_var( 'phone_international', $this->global->get_format_phone_number( $phone_international ) );

					$this->template->Parse( 'sopiBlock', 'phoneInternationalBlock', false );
				}
				
				if( $wa_international !== '' )
				{
					$this->template->set_var( 'chat_international', $this->global->get_format_phone_number( $wa_international, 'wa' ) );
					$this->template->set_var( 'wa_international', $this->global->get_format_phone_number( $wa_international ) );

					$this->template->Parse( 'sowiBlock', 'whatsappInternationalBlock', false );
				}
			}

			if( $phone_global == '' && $wa_global  == '' )
			{
				$this->template->set_var( 'call_global_css', 'sr-only' );
			}
			else
			{
				$this->template->set_var( 'call_global_css', 'call-us-content' );

				if( $phone_global !== '' )
				{
					$this->template->set_var( 'tel_global', $this->global->get_format_phone_number( $phone_global, 'tel' ) );
					$this->template->set_var( 'phone_global', $this->global->get_format_phone_number( $phone_global ) );

					$this->template->Parse( 'sopgBlock', 'phoneGlobalBlock', false );
				}
				
				if( $wa_global !== '' )
				{
					$this->template->set_var( 'chat_global', $this->global->get_format_phone_number( $wa_global, 'wa' ) );
					$this->template->set_var( 'wa_global', $this->global->get_format_phone_number( $wa_global ) );

					$this->template->Parse( 'sowgBlock', 'whatsappGlobalBlock', false );
				}
			}

			if( $phone_europe == '' && $wa_europe  == '' )
			{
				$this->template->set_var( 'call_europe_css', 'sr-only' );
			}
			else
			{
				$this->template->set_var( 'call_europe_css', 'call-us-content' );

				if( $phone_europe !== '' )
				{
					$this->template->set_var( 'tel_europe', $this->global->get_format_phone_number( $phone_europe, 'tel' ) );
					$this->template->set_var( 'phone_europe', $this->global->get_format_phone_number( $phone_europe ) );

					$this->template->Parse( 'sopeBlock', 'phoneEuropeBlock', false );
				}
				
				if( $wa_europe !== '' )
				{
					$this->template->set_var( 'chat_europe', $this->global->get_format_phone_number( $wa_europe, 'wa' ) );
					$this->template->set_var( 'wa_europe', $this->global->get_format_phone_number( $wa_europe ) );

					$this->template->Parse( 'soweBlock', 'whatsappEuropeBlock', false );
				}
			}

			if( $phone_asia == '' && $wa_asia  == '' )
			{
				$this->template->set_var( 'call_asia_css', 'sr-only' );
			}
			else
			{
				$this->template->set_var( 'call_asia_css', 'call-us-content' );

				if( $phone_asia !== '' )
				{
					$this->template->set_var( 'tel_asia', $this->global->get_format_phone_number( $phone_asia, 'tel' ) );
					$this->template->set_var( 'phone_asia', $this->global->get_format_phone_number( $phone_asia ) );

					$this->template->Parse( 'sopaBlock', 'phoneAsiaBlock', false );
				}
				
				if( $wa_asia !== '' )
				{
					$this->template->set_var( 'chat_asia', $this->global->get_format_phone_number( $wa_asia, 'wa' ) );
					$this->template->set_var( 'wa_asia', $this->global->get_format_phone_number( $wa_asia ) );

					$this->template->Parse( 'sowaBlock', 'whatsappAsiaBlock', false );
				}
			}

			if( $skype_america == '' && $skype_europe  == '' && $skype_asia  == '' )
			{
				$this->template->set_var( 'call_skype_css', 'sr-only' );
			}
			else
			{
				$this->template->set_var( 'call_skype_css', 'call-us-skype' );

				if( $skype_america !== '' )
				{
					$this->template->set_var( 'skype_america', $this->global->get_format_skype_link( $skype_america ) );

					$this->template->Parse( 'sosaBlock', 'skypeAmericaBlock', false );
				}
				
				if( $skype_europe !== '' )
				{
					$this->template->set_var( 'skype_europe', $this->global->get_format_skype_link( $skype_europe ) );

					$this->template->Parse( 'soseBlock', 'skypeEuropeBlock', false );
				}
				
				if( $skype_asia !== '' )
				{
					$this->template->set_var( 'skype_asia', $this->global->get_format_skype_link( $skype_asia ) );

					$this->template->Parse( 'sosiBlock', 'skypeAsiaBlock', false );
				}
			}			

			$this->template->set_var( 'email_booking', $this->global->get_setting_value( 'email_booking' ) );
			$this->template->set_var( 'heading_title', $this->data[ 'section_1_9_heading' ] );
			$this->template->set_var( 'content', $this->data[ 'section_1_9_content' ] );
			$this->template->set_var( 'button', $this->data[ 'section_1_9_btn_text' ] );

			$this->template->Parse( 'soBlock', 'sectionOneBlock', false );
		}

		if( isset( $this->data[ 'section_1_9_email_to' ] ) && !empty( $this->data[ 'section_1_9_email_to' ] ) )
		{
			$this->template->set_var( 'email_to', $this->data[ 'section_1_9_email_to' ] );
		}
		else
		{
			$this->template->set_var( 'email_to', $this->global->get_setting_value( 'email' ) );
		}

		//-- SECTION 2 content
		$this->template->set_block( 'main', 'sectionTwoLoopBlock', 'stlBlock' );
		$this->template->set_block( 'main', 'sectionTwoBlock', 'stBlock' );

		if( isset( $this->data[ 'section_2_9_repeater' ] ) && !empty( $this->data[ 'section_2_9_repeater' ] ) )
		{
			$dt = json_decode( $this->data[ 'section_2_9_repeater' ], true );

			if( $dt !== null && json_last_error() === JSON_ERROR_NONE )
			{
				$i = 0;

				foreach( $dt as $d )
				{
					if( count( array_filter( $d ) ) == 0 )
					{
						continue;
					}

					$i++;

					$this->template->set_var( 'title', $d[ 'title' ] );
					$this->template->set_var( 'description', $d[ 'description' ] );

					$this->template->Parse( 'stlBlock', 'sectionTwoLoopBlock', true );
				}

				if( $i > 0 )
				{
					if( empty( $this->data[ 'section_2_9_big_title' ] ) )
					{
						$this->template->set_var( 'heading_title', $this->data[ 'section_2_9_heading' ] );
					}
					else
					{
						$this->template->set_var( 'heading_title', sprintf( '<span>%s</span><small>%s</small>', $this->data[ 'section_2_9_big_title' ], $this->data[ 'section_2_9_heading' ] ) );
					}

					$this->template->Parse( 'stBlock', 'sectionTwoBlock', false );
				}
			}
		}

		$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

		$this->template->set_var( 'post_title', $this->data[ 'ltitle' ] );
		$this->template->set_var( 'post_brief', $this->data[ 'page_brief_text' ] );

		$this->template->set_var( 'header_image', $this->global->get_attachment_url( $this->data[ 'header_image' ], null, 462 ) );
		$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $this->data[ 'header_image' ], 10, 10 ) );

		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'site_url', SITE_URL );
		
		$this->actions->add_actions( 'jvs', $this->init_js() );

		return $this->template->Parse( 'mlBlock', 'mainBlock', false );
	}

	function load_page_custom()
	{
		$this->template->set_file( 'main', 'partials/single-page-custom.html' );

		$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );

		if( $this->sef == 'account-activation-success' )
		{
			$himage   = $this->global->get_setting_value( 'activation_header_image' );
			$htitle   = $this->global->get_setting_value( 'activation_success_title' );
			$hcontent = $this->global->get_setting_value( 'activation_success_content' );
		}
		else if( $this->sef == 'account-activation-failed' )
		{
			$himage   = $this->global->get_setting_value( 'activation_header_image' );
			$htitle   = $this->global->get_setting_value( 'activation_failed_title' );
			$hcontent = $this->global->get_setting_value( 'activation_failed_content' );
		}
		else if( $this->sef == 'accept-alternative-date' )
		{ 
			$stt = $this->global->get_setting_value();
			$bid = json_decode( base64_decode( $this->uri[0] ), true );
	    
		    if( !empty( $bid ) )
		    {
		    	$data = $this->global->get_booking( $bid );

		    	if( !empty( $data ) )
		    	{
		    		if( $data[ 'lstatus' ] == 3 )
		    		{
			            //-- GET module
		                $mod = $this->global->get_field_value( 'lumonata_module', 'lmodule_id', array( 'lappsagent' => 'reservations' ) );
		                
		                //-- CHECK data already exist or not
						$prm = json_decode( $this->global->get_field_value( 'lumonata_additional_field', 'ladditional_value', array( 
							'ladditional_key' => 'alternative_rsv_date', 
							'lapp_id'         => $data[ 'lbooking_id' ], 
							'lmodule_id'      => $mod
						)), true );
			
						if( $prm !== null && json_last_error() === JSON_ERROR_NONE )
						{
							if( isset( $prm[ 'lcheck_in' ] ) && isset( $prm[ 'lcheck_out' ] ) )
							{
								$r = parent::update( 'lumonata_booking', array( 
									'lcheck_out' => $prm[ 'lcheck_out' ],
									'lcheck_in' => $prm[ 'lcheck_in' ],
									'lstatus' => 1,
								), array( 'lbooking_id' => $bid ) );

								if( !is_array( $r ) )
					            {
					            	header( 'Location:' . $this->global->get_payment_url( $bid ) );

									exit;
					            }
					            else
					            {
									$htitle   = 'Not Valid Request';
									$himage   = $stt[ 'activation_header_image' ];
									$hcontent = sprintf( 'Sorry, it seems you have some trouble<br/>If you have any questions regarding the issue, please contact us via email at<br/><a href="mailto:%s">%s</a>', $stt[ 'email' ], $stt[ 'email' ] );
					            }
							}
				            else
				            {
								$htitle   = 'Not Valid Request';
								$himage   = $stt[ 'activation_header_image' ];
								$hcontent = sprintf( 'Sorry, it seems you have some trouble<br/>If you have any questions regarding the issue, please contact us via email at<br/><a href="mailto:%s">%s</a>', $stt[ 'email' ], $stt[ 'email' ] );
				            }
						}
			            else
			            {
							$htitle   = 'Not Valid Request';
							$himage   = $stt[ 'activation_header_image' ];
							$hcontent = sprintf( 'Sorry, it seems you have some trouble<br/>If you have any questions regarding the issue, please contact us via email at<br/><a href="mailto:%s">%s</a>', $stt[ 'email' ], $stt[ 'email' ] );
			            }
		    		}
		    		else
		    		{
						$htitle   = 'Not Valid Request';
						$himage   = $stt[ 'activation_header_image' ];
						$hcontent = sprintf( 'Sorry, it seems you have some trouble<br/>If you have any questions regarding the issue, please contact us via email at<br/><a href="mailto:%s">%s</a>', $stt[ 'email' ], $stt[ 'email' ] );
		    		}
		    	}
	    		else
	    		{
					$htitle   = 'Invalid Booking';
					$himage   = $stt[ 'activation_header_image' ];
					$hcontent = sprintf( 'Sorry, it seems the booking code you want to access is not registered in our system. If you have any questions regarding the issue, please contact us via email at <a href="mailto:%s">%s</a>', $stt[ 'email' ], $stt[ 'email' ] );
	    		}
		    }
    		else
    		{
				$htitle   = 'Invalid Booking';
				$himage   = $stt[ 'activation_header_image' ];
				$hcontent = sprintf( 'Sorry, it seems the booking code you want to access is not registered in our system. If you have any questions regarding the issue, please contact us via email at <a href="mailto:%s">%s</a>', $stt[ 'email' ], $stt[ 'email' ] );
    		}
		}

		$this->template->set_var( 'post_title', $htitle );
		$this->template->set_var( 'post_content', $hcontent );

		$this->template->set_var( 'header_image', $this->global->get_attachment_url( $himage, 1024, null ) );
		$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $himage, 10, 10 ) );

		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'site_url', SITE_URL );

		$this->actions->add_actions( 'jvs', $this->init_js() );

		return $this->template->Parse( 'mlBlock', 'mainBlock', false );

	}

	function load_page_not_found()
	{
		$this->template->set_file( 'main', 'partials/404.html' );

		$this->template->set_block( 'main', 'mainBlock', 'mlBlock' );
		
		$himage   = $this->global->get_setting_value( '404_header_image' );
		$htitle   = $this->global->get_setting_value( '404_title' );
		$hcontent = $this->global->get_setting_value( '404_content' );

		$this->template->set_var( 'post_title', $htitle );
		$this->template->set_var( 'post_content', $hcontent );

		$this->template->set_var( 'header_image', $this->global->get_attachment_url( $himage, 1024, null ) );
		$this->template->set_var( 'header_image_placeholder', $this->global->get_attachment_url( $himage, 10, 10 ) );

		$this->template->set_var( 'theme_url', THEME_URL );
		$this->template->set_var( 'site_url', SITE_URL );

		$this->actions->add_actions( 'jvs', $this->init_js() );

		return $this->template->Parse( 'mlBlock', 'mainBlock', false );
	}

	function init()
	{
		$this->lang_id   = $this->global->get_current_language_id();
		$this->lang_code = $this->global->get_current_language_code();
		$this->lang_def  = $this->global->get_setting_value( 'llanguage' );
		
		$this->data = $this->global->get_posts( array(
	    	'llang_id' => $this->lang_id,
			'lsef_url' => $this->sef,
			'ltype'    => 'pages',
			'lstatus'  => 1
		));
	}

	function init_js()
	{
		if( $this->data[ 'page_template' ] == 1 )
		{
			return '
			<script>
				jQuery(document).ready(function(){
					init_home_slide();
					init_lazy_load();
				});
			</script>';
		}
		else if( $this->data[ 'page_template' ] == 2 )
		{
			return '
			<script>
				jQuery(document).ready(function(){
					init_tab_slide();
					init_lazy_load();
				});
			</script>';
		}
		else if( $this->data[ 'page_template' ] == 4 )
		{
			return '
			<script>
				jQuery(document).ready(function(){
					init_lazy_load();
					init_contact();
				});
			</script>';
		}
		else if( $this->data[ 'page_template' ] == 5 )
		{
			return '
			<script>
				jQuery(document).ready(function(){
					init_lazy_load();
					init_digital();
				});
			</script>';
		}
		else if( $this->data[ 'page_template' ] == 6 )
		{
			return '
			<script>
				jQuery(document).ready(function(){
					init_comment_slider();
					init_franchises();
					init_lazy_load();
				});
			</script>';
		}
		else if( $this->data[ 'page_template' ] == 7 )
		{
			return '
			<script>
				jQuery(document).ready(function(){
					init_lazy_load();
					init_contact();
				});
			</script>';
		}
		else if( $this->data[ 'page_template' ] == 8 )
		{
			return '
			<script>
				jQuery(document).ready(function(){
					init_tab_slide();
					init_lazy_load();
				});
			</script>';
		}
		else if( $this->data[ 'page_template' ] == 9 )
		{
			return '
			<script>
				jQuery(document).ready(function(){
					init_lazy_load();
					init_register();
				});
			</script>';
		}
		else if( in_array( $this->data[ 'page_template' ], array( 3, 5, 6 ) ) )
		{
			return '
			<script>
				jQuery(document).ready(function(){
					init_lazy_load();
				});
			</script>';
		}
		else
		{
			if( $this->sef != 'agent' )
			{
				return '
				<script>
					jQuery(document).ready(function(){
						init_lazy_load();
					});
				</script>';
			}
		}
	}

	function set_meta_title()
	{
		$meta_title = $this->data[ 'lmeta_title' ];

		if( empty( $meta_title ) )
		{
			$meta_title = $this->global->get_setting_value( 'lmeta_title' );

			if( empty( $meta_title ) )
			{
				$meta_title = 'Page - ' . $this->global->get_setting_value( 'web' );
			}
		}
		
		$this->actions->add_actions( 'meta_title', $meta_title );
	}

	function set_meta_desc()
	{
		$meta_description = $this->global->get_setting_value( 'lmeta_desc' );

		if( empty( $meta_description ) === false )
		{
			$meta_description = sprintf( '<meta name="description" content="%s" />', strip_tags( $meta_description ) );

			$this->actions->add_actions( 'meta_description', $meta_description );
		}
	}
}

?>