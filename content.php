<?php

ob_start();

$sess = $gb->get_member_session();

if( $sess[ 'userid' ] === null )
{
    require_once( 'includes/vendor/autoload.php' );

    //-- LOAD global function
    $global = new globalFunctions();
    
    $client_id     = $global->get_setting_value( 'google_client_id' );
    $client_secret = $global->get_setting_value( 'google_client_secret' );
    $redirect_uri  = $global->get_setting_value( 'google_redirect_uri' );

    if( $client_id != '' && $client_secret != '' && $redirect_uri != '' )
    {
        //-- LOAD google API
        $client = new Google_Client();

        $client->setClientId( $client_id );
        $client->setClientSecret( $client_secret );
        $client->setRedirectUri( $redirect_uri );

        $client->addScope( 'email' );
        $client->addScope( 'profile' );

        $tp->set_var( 'google_auth_url', $client->createAuthUrl() );
    }
}

$path = explode( '/', ltrim( $_SERVER['REQUEST_URI'], '/' ) );

foreach( array_keys( $path, 'nomad', true ) as $key )
{
    unset( $path[ $key ] );
}

$mod = array_shift( $path );

if( $gb->is_translation( $mod ) )
{
    if( isset( $path[ 0 ] ) )
    {
        $mod = $path[ 0 ];

        unset( $path[ 0 ] );

        $path = array_values( $path );
    }
}

if( empty( $mod ) )
{
    if( file_exists( APPS_DIR . '/class.homepage.php' ) )
    {
        require_once( APPS_DIR . '/class.homepage.php' );

        $obj = new homepage( $ac );

        $tp->set_var( 'content', $obj->load() );
    }
}
elseif( in_array( $mod, array( 'google-authentication', 'facebook-authentication', 'gmail-oauth-token', 'ig-oauth-token', 'account-activation' ) ) )
{
    require_once( APPS_DIR . '/class.credentials.php' );

    $obj = new credentials( $ac, $mod );

    $obj->load();
}
else if( in_array( $mod, array( 'switch-language', 'generate-language-data', 'adjust-language-data' ) ) )
{
    require_once( APPS_DIR . '/class.language.php' );

    $obj = new language( $ac, $mod );

    $obj->load();
}
else if( in_array( $mod, array( 'import-data' ) ) )
{
    require_once( ADMIN_DIR . '/functions/globals.php' );
    require_once( ADMIN_DIR . '/functions/upload.php' );
    require_once( APPS_DIR . '/class.import.php' );

    $obj = new Import( $ac, $mod );

    $obj->load();
}
else if( in_array( $mod, array( 'ajax-request' ) ) )
{
    header('Content-type: application/json; charset=utf-8');

    require_once( APPS_DIR . '/class.ajax.php' );

    $obj = new ajax( $ac, $mod );

    $obj->load();
}
else if( in_array( $mod, array( 'debug' ) ) )
{
    require_once( ADMIN_DIR . '/functions/globals.php' );

    $gb = new globalAdmin();
    $data  = $gb->getBooking( 6 );
    $agent = $gb->getFields( 'lumonata_user', array( 'lname', 'lemail' ), array( 'lusername' => $data[ 'lposts_data' ][ 'lcreated_by' ] ) );


    $mod = $gb->getFields( 'lumonata_module', 'lmodule_id', array( 'lappsagent' => 'reservations' ) );
    $prm = json_decode( $gb->getFields( 'lumonata_additional_field', 'ladditional_value', array( 
        'ladditional_key' => 'alternative_rsv_date', 
        'lapp_id'         => $data[ 'lbooking_id' ], 
        'lmodule_id'      => $mod
    )), true );

    echo $gb->bookingRequestMessage( $data, $agent );
    echo $gb->bookingAcceptedMessage( $data );
    echo $gb->bookingRefuseMessage( $data );
    echo $gb->bookingInvoiceMessage( $data, $agent );
    echo $gb->bookingPaidMessage( $data, $agent );
    echo $gb->bookingChangedDateMessage( $data, $prm );

    $user = $gb->getFields( 'lumonata_user', array( 'lusername', 'lemail', 'lactivate_code' ), array( 'luser_id' => 2 ) );

    echo $gb->activationAccountMessage( $user );
    exit;
}
else
{
    $app  = '';

    if( in_array( $mod, array( 'destinations', 'destination', 'surf-trips', 'accommodation-type' ) ) )
    {
        $app = 'destination';
    }
    else if( in_array( $mod, array( 'accommodation', 'accommodations' ) ) )
    {
        $app = 'accommodation';
    }
    else if( in_array( $mod, array( 'blogs' ) ) )
    {
        $app = 'blogs';
    }
    else if( in_array( $mod, array( 'booking' ) ) )
    {
        $app = 'booking';
    }

    if( empty( $app ) && file_exists( APPS_DIR . '/class.pages.php' ) )
    {
        require_once( APPS_DIR . '/class.pages.php' );

        $obj = new pages( $ac, $mod, $path );

        $tp->set_var( 'content', $obj->load() );
    }
    else if( file_exists( APPS_DIR . '/class.' . $app . '.php' ) )
    {
        require_once( APPS_DIR . '/class.' . $app . '.php' );

        $obj = new $app( $ac, $mod, $path );

        $tp->set_var( 'content', $obj->load() );
    }
}

?>