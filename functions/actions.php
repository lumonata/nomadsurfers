<?php

class actions
{
    function add_actions( $args = array() )
    {
        $args = func_get_args();

        $label     = $args[ 0 ];
        $func_name = $args[ 1 ];

        if( count( $args ) > 2 )
        {
            $args_shift = array_shift( $args );
            $args_shift = array_shift( $args );
        }
        else
        {
            $args = '';
        }

        $this->action[ $label ][ 'func_name' ][] = $func_name;
        $this->action[ $label ][ 'args' ][]      = $args;
    }

    function attemp_actions( $lbl )
    {
        $result = array();

        if( empty( $this->action[ $lbl ][ 'func_name' ] ) )
        {
            return;
        }

        for( $j = 0; $j < count( $this->action[ $lbl ][ 'func_name' ] ); $j++ )
        {
            if( function_exists( $this->action[ $lbl ][ 'func_name' ][ $j ] ) )
            {
                $result[] = call_user_func_array( $this->action[ $lbl ][ 'func_name' ][ $j ], ( is_array( $this->action[ $lbl ][ 'args' ][ $j ] ) ) ? $this->action[ $lbl ][ 'args' ][ $j ] : array( $this->action[ $lbl ][ 'args' ][ $j ] ) );
            }
            else
            {
                if( count( $this->action[ $lbl ][ 'func_name' ] ) > 1 )
                {
                    $result = array();

                    foreach( $this->action[ $lbl ][ 'func_name' ] as $obj )
                    {
                        $result[] = $obj;
                    }

                    return implode( "\n", $result );
                }
                else
                {
                    return $this->action[ $lbl ][ 'func_name' ][ $j ];
                }
            }
        }

        return implode( "\n", $result );
    }

    function run_actions( $lbl )
    {
        $result = '';

        if( empty( $this->action[ $lbl ][ 'func_name' ] ) )
        {
            return;
        }

        for( $j = 0; $j < count( $this->action[ $lbl ][ 'func_name' ] ); $j++ )
        {
            if( function_exists( $this->action[ $lbl ][ 'func_name' ][ $j ] ) )
            {
                return call_user_func_array( $this->action[ $lbl ][ 'func_name' ][ $j ], ( is_array( $this->action[ $lbl ][ 'args' ][ $j ] ) ) ? $this->action[ $lbl ][ 'args' ][ $j ] : array( $this->action[ $lbl ][ 'args' ][ $j ] ) );
            }
            else
            {
                return $this->action[ $lbl ][ 'func_name' ][ $j ];
            }
        }
    }
}