<?php

if( isset( $_POST[ 'login' ] ) )
{
    $user = isset( $_POST[ 'user_name' ] ) ? addslashes( $_POST[ 'user_name' ] ) : '';
    $pwd  = isset( $_POST[ 'pwd' ] ) ? addslashes( $_POST[ 'pwd' ] ) : '';

    $s = 'SELECT * FROM lumonata_user AS a WHERE a.lpassword = %s AND a.lusername = %s AND a.lblock = %d AND a.lstatus = %d';
    $q = $db->prepare_query( $s, md5( $pwd ), $user, 0, 1 );
    $r = $db->query( $q );

    if( is_array( $r ) )
    {
        $flash->add( 'Incorrect Username or Password. Please try again' );

        header( 'Location:' . HT_SERVER . ADMIN_URL . '/login.php' );

        exit;
    }
    else
    {
        $n = $db->num_rows( $r );

        if( $n > 0 )
        {
            $d = $db->fetch_array( $r );

            //-- BLOCK agent user login from here
            if( $d[ 'lusertype_id' ] == 2 )
            {
                $flash->add( 'Sorry, you don\'t have permission to access this page!' );

                header( 'Location:' . HT_SERVER . ADMIN_URL . '/login.php' );

                exit;
            }

            //-- UPDATE last visited user
            $db->update( 'lumonata_user', array( 'llastvisit_date' => time() ), array( 'lpassword' => md5( $pwd ), 'lusername' => $user ) );

            //-- SET parameter
            $param = base64_encode( 
                json_encode( 
                    array(
                        'userid'   => $d[ 'luser_id' ],
                        'username' => $d[ 'lusername' ],
                        'usertype' => $d[ 'lusertype_id' ]
                    )
                )
            );

            //-- SET session
            $_SESSION[ SESSION_NAME ] = $param;

            //-- SET cookie
            setcookie( SESSION_NAME, $param );
            
            //-- Redirect Process
            header( 'Location:' . HT_SERVER . ADMIN_URL . '/home.php' );

            exit;
        }
        else
        {
            $flash->add( 'Sorry, you don\'t have permission to access this page!' );

            header( 'Location:' . HT_SERVER . ADMIN_URL . '/login.php' );

            exit;
        }
    }
}