<?php

class db
{
	var $hostname;
	
    function __construct( $hostname = HOSTNAME, $username = DBUSER, $password = DBPASSWORD, $database = DBNAME )
    {
        $this->database    = $database;
        $this->hostname    = $hostname;
        $this->username    = $username;
        $this->password    = $password;

		$this->dbc = $this->db_connect();

		$this->select_db();
    }

    function db_connect()
    {
        $this->result = mysqli_connect( $this->hostname, $this->username, $this->password );
        
        if( !$this->result )
        {
            return false;
        }
        
        return $this->result;
    }
    
    function select_db()
    {
        if( !mysqli_select_db( $this->dbc, $this->database ) )
        {
            return false;
        }
    }
    
    function query( $query )
    {
        $result = mysqli_query( $this->dbc, $query );

        if( $result === false )
        {
            return array( 
                'result'     => 'error-query', 
                'error_code' => mysqli_errno( $this->dbc ), 
                'message'    => mysqli_error( $this->dbc )
            );
        }
        else
        {
            return $result;
        }
    }
    
    function fetch_array( $result )
    {
        return mysqli_fetch_array( $result );
    }
    
    function fetch_assoc( $result )
    {
        return mysqli_fetch_assoc( $result );
    }
    
    function num_rows( $result )
    {
        mysqli_store_result( $this->dbc );

        return mysqli_num_rows( $result );
    }

    function begin()
    {
        $null = $this->query( 'START TRANSACTION' );

        return $this->query( 'BEGIN' );
    }

    function commit()
    {
        $this->query( 'COMMIT' );
    }

    function rollback()
    {
        $this->query( 'ROLLBACK' );
    }
    
    function order_id( $field, $table )
    {
        $query_order_id  = "SELECT " . $field . " FROM " . $table . " ORDER BY " . $field . " DESC LIMIT 1";
        $result_order_id = $this->query( $query_order_id );
        $row_order_id    = $this->fetch_array( $result_order_id );
        $order_id        = $row_order_id[ $field ] + 1;

        return $order_id;
    }    
    
    function get_order_id( $field, $table )
    {
        $query_order_id  = "SELECT " . $field . " FROM " . $table . " ORDER BY " . $field . " DESC LIMIT 1";
        $result_order_id = $this->query( $query_order_id );
        $row_order_id    = $this->fetch_array( $result_order_id );
        $order_id        = is_numeric( $row_order_id[ $field ] ) + 1;

        return $order_id;
    }
    
    function update_order_id( $table, $order_id )
    {
        $qry    = "UPDATE " . $table . " SET lorder_id = lorder_id + 1 WHERE lorder_id >= " . $order_id;
        $result = $this->query( $qry );

        return ( $result == DB_OK ) ? TRUE : FALSE;
    }
    
    function get_upd_by( $upd_by )
    {
        $query_usr  = "SELECT lname FROM lumonata_user WHERE lusername = '$upd_by'";
        $result_usr = $this->query( $query_usr );
        $row_usr    = $this->fetch_array( $result_usr );
        $usr        = $row_usr[ 'lname' ];

        return $usr;
    }
    
    function success_alert_file( $file = "home.php", $page, $prc, $view )
    {
		?>
		<script type="text/javascript">
			location = "../../../<?= $file; ?>?app=<?= $page; ?>&prc=<?= $prc; ?>&p=<?= $view; ?>";
		</script>
		<?php
    }
    
    function success_alert( $page, $prc, $view )
    {
		?>
		<script type="text/javascript">
			location = "../../../home.php?mod=<?= $page; ?>&prc=<?= $prc; ?>&p=<?= $view; ?>";
		</script>
		<?php
    }
    
    function fail_alert( $alert )
    {
		?>
		<script type="text/javascript">
			alert( "<?= $alert; ?>" );
			history.back();
		</script>
		<?php
    }
    
    function get_num_rows( $table, $field, $id )
    {
        $query_num  = "SELECT * FROM " . $table . " WHERE " . $field . "='" . $id . "'";
        $result_num = $this->query( $query_num );
        $num        = $this->num_rows( $result_num );

        return $num;
    }

    function get_num_rows2( $table, $field, $id, $field2, $id2 )
    {
        $query_num  = "SELECT * FROM " . $table . " WHERE " . $field . "='" . $id . "' AND " . $field2 . "='" . $id2 . "'";
        $result_num = $this->query( $query_num );
        $num        = $this->num_rows( $result_num );

        return $num;
    }

    function get_value_field( $table, $field_get, $field, $id )
    {
        $query_num  = "SELECT * FROM " . $table . " WHERE " . $field . "='" . $id . "'";
        $result_num = $this->query( $query_num );
        $row_num    = $this->fetch_array( $result_num );
        $value      = $row_num[ $field_get ];

        return $value;
    }

    function get_view( $usrtype, $sef_url )
    {
        $query_pri  = "SELECT p.lusertype_id as lusertype_id,
						p.lmodule_id as lmodule_id,
						p.lview as lview,
						p.linsert as linsert,
						p.ledit as ledit,
						p.ldelete as ldelete,
						m.lsef_url as lsef_url
					  FROM lumonata_privilege p, lumonata_module m 
					  WHERE p.lusertype_id = '$usrtype' AND m.lsef_url = '$sef_url' 
					  AND p.lmodule_id = m.lmodule_id ";
        $result_pri = $this->query( $query_pri );
        $row_pri    = $this->fetch_array( $result_pri );
        $view       = $row_pri[ 'lview' ];

        if( $view == 1 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function get_insert( $usrtype, $sef_url )
    {
        $query_pri  = "SELECT p.lusertype_id as lusertype_id,
						p.lmodule_id as lmodule_id,
						p.lview as lview,
						p.linsert as linsert,
						p.ledit as ledit,
						p.ldelete as ldelete,
						m.lsef_url as lsef_url
					  FROM lumonata_privilege p,lumonata_module m 
					  WHERE p.lusertype_id = '$usrtype' AND m.lsef_url = '$sef_url' 
					  AND p.lmodule_id = m.lmodule_id ";
        $result_pri = $this->query( $query_pri );
        $row_pri    = $this->fetch_array( $result_pri );
        $insert     = $row_pri[ 'linsert' ];

        if( $insert == 1 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function get_edit( $usrtype, $sef_url )
    {
        $query_pri2  = "SELECT p.lusertype_id as lusertype_id,
							p.lmodule_id as lmodule_id,
							p.lview as lview,
							p.linsert as linsert,
							p.ledit as ledit,
							p.ldelete as ldelete,
							m.lsef_url as lsef_url
					   FROM lumonata_privilege p,lumonata_module m 
					   WHERE p.lusertype_id = '$usrtype' AND m.lsef_url = '$sef_url' 
					   AND p.lmodule_id = m.lmodule_id ";
        $result_pri2 = $this->query( $query_pri2 );
        $row_pri2    = $this->fetch_array( $result_pri2 );
        $edit        = $row_pri2[ 'ledit' ];

        if( $edit == 1 )
        {
            return true;
        }
        else
        {
            return false;
        }        
    }

    function get_delete( $usrtype, $sef_url )
    {
        $query_pri  = "SELECT p.lusertype_id as lusertype_id,
							p.lmodule_id as lmodule_id,
							p.lview as lview,
							p.linsert as linsert,
							p.ledit as ledit,
							p.ldelete as ldelete,
							m.lsef_url as lsef_url
					   FROM lumonata_privilege p,lumonata_module m 
					   WHERE p.lusertype_id = '$usrtype' AND m.lsef_url = '$sef_url' 
					   AND p.lmodule_id = m.lmodule_id ";
        $result_pri = $this->query( $query_pri );
        $row_pri    = $this->fetch_array( $result_pri );
        $delete     = $row_pri[ 'ldelete' ];

        if( $delete == 1 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function str2time( $str )
    {
        $str    = "/" . $str;
        $jml    = substr_count( $str, "/" );
        $pre    = explode( "/", $str );
        $result = $pre[ 2 ] . "/" . $pre[ 1 ] . "/" . $pre[ 3 ];
        $result = strtotime( $result );

        return $result;
    }
    
    function insert( $table, $data )
    {
        $data = $this->add_magic_quotes( $data );

        $fields = array_keys( $data );
        
        return $this->query( "INSERT INTO $table (`" . implode( '`,`', $fields ) . "`) VALUES ('" . implode( "','", $data ) . "')" );
    }
    
    function delete( $table, $where )
    {
        if( is_array( $where ) )
        {
            foreach( $where as $c => $v )
            {
                $wheres[] = "$c = '" . $this->escape( $v ) . "'";
            }
        }
        else
        {
            return false;
        }
        
        return $this->query( "DELETE FROM $table WHERE " . implode( ' AND ', $wheres ) );
    }

    function insert_id()
    {
        return mysqli_insert_id( $this->dbc );
    }

    function update( $table, $data, $where )
    {
        $data = $this->add_magic_quotes( $data );
        $bits = $wheres = array();

        foreach( array_keys( $data ) as $k )
        {
            if( is_null( $data[$k] ) )
            {
                $bits[] = "`$k` = null";
            }
            else
            {
                $bits[] = "`$k` = '$data[$k]'";
            }
        }
        
        if( is_array( $where ) )
        {
            foreach( $where as $c => $v )
            {
                $wheres[] = "$c = '" . $this->escape( $v ) . "'";
            }
        }
        else
        {
            return false;
        }
        
        return $this->query( "UPDATE $table SET " . implode( ', ', $bits ) . ' WHERE ' . implode( ' AND ', $wheres ) );
    }

    function prepare_query( $args = NULL )
    {
        if( NULL === $args )
        {
            return;
        }

        $args  = func_get_args();
        $query = array_shift( $args );
        $query = str_replace( "'%s'", '%s', $query ); //-- in case someone mistakenly already singlequoted it
        $query = str_replace( '"%s"', '%s', $query ); //-- doublequote unquoting
        $query = str_replace( '%s', "'%s'", $query ); //-- quote the strings

        array_walk( $args, array( &$this, 'escape_by_ref' ) );
        
        return @vsprintf( $query, $args );
    }
    
    function add_magic_quotes( $array )
    {
        foreach( $array as $k => $v )
        {
            if( is_null( $v ) )
            {
                $array[$k] = $v;
            }
            else if( is_array( $v ) )
            {
                $array[$k] = $this->add_magic_quotes( $v );
            }
            else
            {
                $array[$k] = $this->escape( $v );
            }
        }

        return $array;
    }
    
    function _weak_escape( $string )
    {
        return addslashes( $string );
    }
    
    function _real_escape( $string )
    {
        if( $this->result )
        {
            return mysqli_real_escape_string( $this->dbc, $string );
        }
        else
        {
            return addslashes( $string );
        }
    }
    
    function _escape( $data )
    {
        if( is_array( $data ) )
        {
            foreach( (array) $data as $k => $v )
            {
                if( is_array( $v ) )
                {
                    $data[$k] = $this->_escape( $v );
                }
                else
                {
                    $data[$k] = $this->_real_escape( $v );
                }
            }
        }
        else
        {
            $data = $this->_real_escape( $data );
        }
        
        return $data;
    }

    function escape( $data )
    {
        if( is_array( $data ) )
        {
            foreach( (array) $data as $k => $v )
            {
                if( is_array( $v ) )
                {
                    $data[$k] = $this->escape( $v );
                }
                else
                {
                    $data[$k] = $this->_weak_escape( $v );
                }
            }
        }
        else
        {
            $data = $this->_weak_escape( $data );
        }
        
        return $data;
    }

    function escape_by_ref( &$string )
    {
        $string = $this->_real_escape( $string ); 
    }
}

$db = new db( HOSTNAME, DBUSER, DBPASSWORD, DBNAME, SITE_URL );

?>