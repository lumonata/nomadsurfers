<?php

class searchDir
{
    function getDir( $path, $maxdepth = -1, $mode = 'FULL', $d = 0 )
    {
        if( substr( $path, strlen( $path ) - 1 ) != '/' )
        {
            $path .= '/';
        }

        $dirlist = array();

        if( $mode != 'FILES' )
        {
            $dirlist[] = $path;
        }

        if( $handle = opendir( $path ) )
        {
            while( false !== ( $file = readdir( $handle ) ) )
            {
                if( $file != '.' && $file != '..' )
                {
                    $file = $path . $file;

                    if( !is_dir( $file ) )
                    {
                        if( $mode != 'DIRS' )
                        {
                            $dirlist[] = $file;
                        }
                    }
                    elseif( $d >= 0 && ( $d < $maxdepth || $maxdepth < 0 ) )
                    {
                        $result  = searchdir( $file . '/', $maxdepth, $mode, $d + 1 );
                        $dirlist = array_merge( $dirlist, $result );
                    }
                }
            }

            closedir( $handle );
        }

        if( $d == 0 )
        {
            natcasesort( $dirlist );
        }

        return ( $dirlist );
    }
    
    function spesificDir( $dir )
    {
        $dh     = opendir( $dir );
        $ignore = array( 'cgi-bin', '.', '..' );

        while( false !== ( $filename = readdir( $dh ) ) )
        {
            if( !in_array( $filename, $ignore ) )
            {
                $files[] = $filename;
            }
        }

        return $files;
    }
    
    function getClassName( $dir )
    {
        $dh     = opendir( $dir );
        $ignore = array( 'cgi-bin', '.', '..' );

        while( false !== ( $filename = readdir( $dh ) ) )
        {
            if( !in_array( $filename, $ignore ) )
            {
                $ifileName = substr( $filename, 0, 5 );

                if( $ifileName == 'class' )
                {
                    list( $class, $fileName, $ext ) = explode( '.', $filename );

                    return $fileName;
                }
            }
        }
        
    }
    
    function isApp( $dir, $app )
    {
        for( $i = 0; $i < sizeof( $this->spesificDir( $dir ) ); $i++ )
        {
            if( in_array( $app, $this->spesificDir( $dir ) ) )
            {
                return true;
            }
        }
    }
    
    function isDirectory( $path )
    {
        return is_dir( $path ) ? true : false;
    }
    
    function getDirectory( $path )
    {
        $path = opendir( $path );

        while( $dir = readdir( $path ) )
        {
            if( $dir != '..' && $dir != '.' && $dir != '' && $dir != 'Thumbs.db' )
            {
                $dirlist[] = $dir;
            }
        }

        closedir( $path );

        return $dirlist;
    }
}

?> 