<?php

class flash_message
{
    public function render()
    {
        if( !isset( $_SESSION[ 'messages' ] ) )
        {
            return null;
        }

        $messages = $_SESSION[ 'messages' ];

        unset( $_SESSION[ 'messages' ] );

        return $messages;
    }

    public function add( $message )
    {
        if( !isset( $_SESSION[ 'messages' ] ) )
        {
            $_SESSION[ 'messages' ] = array();
        }

        $_SESSION[ 'messages' ] = $message;
    }

    public function message( $session = array() )
    {
        $content = '';

        if( empty( $session ) )
        {
            $session = $this->render();
        }

        if( empty( $session ) === false )
        {
            if( isset( $session[ 'type' ] ) && isset( $session[ 'message' ] ) && !empty( $session[ 'message' ] ) )
            {
                if( is_array( $session[ 'message' ] ) )
                {
                    $content .= '
                    <div class="alert alert-' . $session[ 'type' ] .  '">
                        <ul>';
                            foreach( $session[ 'message' ] as $f )
                            {
                                $content .= '<li>' . $f . '</li>';
                            }

                            $content .= '
                        </ul>
                    </div>';   
                }
                else
                {
                    $content .= '
                    <div class="alert alert-' . $session[ 'type' ] .  '">
                        ' . $session[ 'message' ] . '
                    </div>';
                }
            }
            else
            {
                if( is_array( $session ) )
                {
                    $content .= '
                    <ul class="alert">';
                        foreach( $session as $f )
                        {
                            $content .= '<li>' . $f . '</li>';
                        }

                        $content .= '
                    </ul>';  
                }
                else
                {
                    $content .= '
                    <div class="alert">
                        <p>' . $session . '</p>
                    </div>';
                }
            }
        }

        return $content;
    }
}

$flash = new flash_message();

?>