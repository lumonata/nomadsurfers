<?php

use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

class globalFunctions extends db
{
    public function __construct()
    {
        $this->template = new Template( THEME_DIR );

        parent::__construct();
    }

    public function init_accommodation_map( $data = array(), $prm = array() )
    {
        $this->template->set_file( 'maps', 'partials/accommodation-map-list.html' );

        $this->template->set_block( 'maps', 'resultLoopBlock', 'rslBlock' );
        $this->template->set_block( 'maps', 'resultBlock', 'rsBlock' );

        $this->template->set_block( 'maps', 'noresultBlock', 'nrsBlock' );
        $this->template->set_block( 'maps', 'mapsBlock', 'mplBlock' );

        if( isset( $data[ 'ltitle' ] ) )
        {
            $this->template->set_var( 'head_title', $this->translate( 'accommodation-text-6', sprintf( 'Accommodation In %s', $data[ 'ltitle' ] ), 1, array( $data[ 'ltitle' ] ) ) );
        }
        else
        {
            $this->template->set_var( 'head_title', $this->translate( 'accommodation-text-7', sprintf( 'Accommodation For %s', $data[ 'lname' ] ), 1, array( $data[ 'lname' ] ) ) );
        }

        $w = array();

        if( isset( $prm[ 'dest' ] ) && !empty( $prm[ 'dest' ] ) )
        {
            $w[] = parent::prepare_query( 'FIND_IN_SET( %d, ( SELECT GROUP_CONCAT( a2.ladditional_value ) FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key IN ( %s, %s, %s, %s ) ) )', $prm[ 'dest' ], 'accommodation_continent', 'accommodation_country', 'accommodation_state', 'accommodation_city' );
        }

        if( isset( $prm[ 'term' ] ) && !empty( $prm[ 'term' ] ) )
        {
            $w[] = parent::prepare_query( 'FIND_IN_SET( %d, ( SELECT GROUP_CONCAT( a2.lterm_id ) FROM lumonata_post_relationship AS a2 WHERE a2.lpost_id = a.lpost_id ) )', $prm[ 'term' ] );
        }

        if( empty( $w ) )
        {
            $where = '';
        }
        else
        {
            $where = ' AND ' . implode( ' AND ', $w );
        }

        //-- GET language
        $lang_id = $this->get_current_language_id();

        $q = 'SELECT * FROM lumonata_post AS a WHERE a.ltype = "accommodation" AND a.lstatus = 1 AND a.llang_id = ' . $lang_id . $where . ' ORDER BY a.ltitle';
        $r = parent::query( $q );

        if( !is_array( $r ) )
        {
            $n = parent::num_rows( $r );

            if( $n > 0 )
            {
                $c = '';
                $i = 0;

                while( $d = parent::fetch_assoc( $r ) )
                {
                    //-- GET additional field
                    $sa = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lapp_id = %d AND a.lmodule_id = ( SELECT a2.lmodule_id FROM lumonata_module AS a2 WHERE a2.lapps = %s )';
                    $qa = parent::prepare_query( $sa, $d[ 'lpost_id' ], $d[ 'ltype' ] );
                    $ra = parent::query( $qa );

                    if( parent::num_rows( $ra ) > 0 )
                    {
                        while( $da = parent::fetch_assoc( $ra ) )
                        {
                            $d[ $da[ 'ladditional_key' ] ] = $da[ 'ladditional_value' ];
                        }
                    }

                    //-- GET term field
                    $st = 'SELECT b.* FROM lumonata_post_relationship AS a LEFT JOIN lumonata_post_terms AS b ON a.lterm_id = b.lterm_id WHERE a.lpost_id = %d AND b.lgroup = %s ORDER BY b.lname';
                    $qt = parent::prepare_query( $st, $d[ 'lpost_id' ], $d[ 'ltype' ] );
                    $rt = parent::query( $qt );

                    if( parent::num_rows( $rt ) > 0 )
                    {
                        while( $dt = parent::fetch_assoc( $rt ) )
                        {
                            //-- GET term additional field
                            $sf = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lterm_id = %d';
                            $qf = parent::prepare_query( $sf, $dt[ 'lterm_id' ] );
                            $rf = parent::query( $qf );

                            if( parent::num_rows( $rf ) > 0 )
                            {
                                while( $df = parent::fetch_assoc( $rf ) )
                                {
                                    $dt[ $df[ 'ladditional_key' ] ] = $df[ 'ladditional_value' ];
                                }
                            }

                            $d[ $dt[ 'lrule' ] ][] = $dt;
                        }
                    }

                    //-- GET map coordinate
                    if( $i == 0 && !empty( $d[ 'map_coordinate' ] ) )
                    {
                        $c = $d[ 'map_coordinate' ];

                        $i++;
                    }

                    //-- GET surf trip
                    $stype = array();

                    if( !empty( $d[ 'surf_trip' ] ) )
                    {
                        foreach( $d[ 'surf_trip' ] as $ds )
                        {
                            $stype[] = $ds[ 'lname' ];
                        }
                    }

                    //-- GET certified
                    if( isset( $d[ 'is_certified' ] ) && $d[ 'is_certified' ] == 1 )
                    {
                        $this->template->set_var( 'certified', 1 );
                        $this->template->set_var( 'certified_css', '' );
                    }
                    else
                    {
                        $this->template->set_var( 'certified', 2 );
                        $this->template->set_var( 'certified_css', 'hidden' );
                    }

                    //-- GET accommodation rating
                    $rating = $this->get_accommodation_rating( $d[ 'lpost_id' ], $d[ 'lref_id' ] );

                    if( empty( $rating ) )
                    {
                        $this->template->set_var( 'review_label', $this->translate( 'no-reviews', 'NO REVIEWS', 3 ) );
                        $this->template->set_var( 'review_count', 0 );
                        $this->template->set_var( 'review_star', 0 );
                    }
                    else
                    {
                        $this->template->set_var( 'review_label', $this->get_accommodation_rating_label( $rating ) );
                        $this->template->set_var( 'review_count', $rating[ 'reviews' ] );
                        $this->template->set_var( 'review_star', $rating[ 'rating' ] );
                    }

                    $start_price = $this->get_start_price( $d );

                    $this->template->set_var( 'mcoordinate', $d[ 'map_coordinate' ] );
                    $this->template->set_var( 'surf_type', empty( $stype ) ? 'z' : implode( ', ', $stype ) );

                    $this->template->set_var( 'permalink', $this->permalink( $d ) );
                    $this->template->set_var( 'location', $this->get_locations( $d ) );
                    $this->template->set_var( 'title', $this->get_listing_title( $d ) );
                    $this->template->set_var( 'hightlight', $this->get_hightlight( $d ) );

                    $this->template->set_var( 'start_price', $start_price );
                    $this->template->set_var( 'start_price_format', $this->get_format_price( $start_price, $d[ 'lpost_id' ] ) );

                    $this->template->set_var( 'img_thumb', $this->get_attachment_url( $d[ 'accommodation_image' ], 198, 145 ) );
                    $this->template->set_var( 'img_placeholder', $this->get_attachment_url( $d[ 'accommodation_image' ], 10, 10 ) );

                    $this->template->Parse( 'rslBlock', 'resultLoopBlock', true );
                }

                $this->template->set_var( 'coordinate', $c );

                $this->template->set_var( 'str_1', $this->translate( 'max-persons', sprintf( 'max %s persons', 5 ), 2, array( 5 ) ) );
                $this->template->set_var( 'str_2', $this->translate( 'type-of-surf-trip', 'TYPE OF SURF TRIPS', 3 ) );
                $this->template->set_var( 'str_3', $this->translate( 'change-dates', 'CHANGE DATES', 3 ) );
                $this->template->set_var( 'str_4', $this->translate( 'certified', 'CERTIFIED', 3 ) );
                $this->template->set_var( 'str_5', $this->translate( 'sort-by', 'Sort By', 1 ) );
                $this->template->set_var( 'str_6', $this->translate( 'check-out', 'Check-Out' ) );
                $this->template->set_var( 'str_7', $this->translate( 'check-in', 'Check-In' ) );
                $this->template->set_var( 'str_8', $this->translate( 'price', 'PRICE', 3 ) );
                $this->template->set_var( 'str_9', $this->translate( 'from', 'FROM', 3 ) );

                $this->template->Parse( 'rsBlock', 'resultBlock', false );
            }
            else
            {
                $this->template->set_var( 'str_1', $this->translate( 'accommodation-text-5', 'No accommodation was found in your seach term, please try another terms' ) );

                $this->template->Parse( 'nrsBlock', 'noresultBlock', false );
            }
        }
        else
        {
            $this->template->set_var( 'str_1', $this->translate( 'accommodation-text-5', 'No accommodation was found in your seach term, please try another terms' ) );

            $this->template->Parse( 'nrsBlock', 'noresultBlock', false );
        }

        return $this->template->Parse( 'mplBlock', 'mapsBlock', false );
    }

    public function init_search_form( $prm = array() )
    {
        $this->template->set_file( 'search', 'partials/search-form.html' );

        $this->template->set_block( 'search', 'sectionAccommodationTypeLoopBlock', 'atlBlock' );
        $this->template->set_block( 'search', 'sectionAccommodationTypeBlock', 'atBlock' );

        $this->template->set_block( 'search', 'sectionSurfTypeLoopBlock', 'sftlBlock' );
        $this->template->set_block( 'search', 'sectionSurfTypeBlock', 'sftBlock' );

        $this->template->set_block( 'search', 'sectionPopularDestinationLoopBlock', 'spdlBlock' );
        $this->template->set_block( 'search', 'sectionPopularDestinationBlock', 'spdBlock' );

        $this->template->set_block( 'search', 'sectionContinentBlock', 'scBlock' );

        $this->template->set_block( 'search', 'searchBlock', 'schBlock' );

        //-- GET language
        $lang_id = $this->get_current_language_id();

        //-- GET options
        $acco_types = $this->accommodation_type_option( $lang_id );
        $surf_trips = $this->surf_trips_option( $lang_id );
        $continent  = $this->continent_option( $lang_id );
        $popular    = $this->popular_option( $lang_id );

        $this->template->set_var( 'term_value', '' );
        $this->template->set_var( 'term_label', $this->translate( 'type-of-surf-trip', 'TYPE OF SURF TRIP' ) );

        $this->template->set_var( 'dest_value', '' );
        $this->template->set_var( 'dest_label', $this->translate( 'where-are-you-going', 'WHERE ARE YOU GOING?' ) );

        if( isset( $prm[ 'dest' ] ) && $prm[ 'dest' ] == 0 )
        {
            $this->template->set_var( 'dest_value', 0 );
            $this->template->set_var( 'dest_label', $this->translate( 'i-don-t-know-where-to-go', 'I Don\'t Know Where To Go' ) );
        }

        if( !empty( $continent ) )
        {
            foreach( $continent as $id => $name )
            {
                if( isset( $prm[ 'dest' ] ) && $prm[ 'dest' ] == $id )
                {
                    $this->template->set_var( 'dest_value', $id );
                    $this->template->set_var( 'dest_label', $name );
                }

                $this->template->set_var( 'dest_id', $id );
                $this->template->set_var( 'dest_name', $name );

                $this->template->Parse( 'scBlock', 'sectionContinentBlock', true );
            }
        }

        if( !empty( $popular ) )
        {
            foreach( $popular as $id => $name )
            {
                if( isset( $prm[ 'dest' ] ) && $prm[ 'dest' ] == $id )
                {
                    $this->template->set_var( 'dest_value', $id );
                    $this->template->set_var( 'dest_label', $name );
                }

                $this->template->set_var( 'dest_id', $id );
                $this->template->set_var( 'dest_name', $name );

                $this->template->Parse( 'spdlBlock', 'sectionPopularDestinationLoopBlock', true );
            }

            $this->template->Parse( 'spdBlock', 'sectionPopularDestinationBlock', false );
        }

        if( !empty( $acco_types ) )
        {
            foreach( $acco_types as $id => $name )
            {
                if( isset( $prm[ 'term' ] ) && $prm[ 'term' ] == $id )
                {
                    $this->template->set_var( 'term_value', $id );
                    $this->template->set_var( 'term_label', $name );
                }

                $this->template->set_var( 'term_id', $id );
                $this->template->set_var( 'term_name', $name );

                $this->template->Parse( 'atlBlock', 'sectionAccommodationTypeLoopBlock', true );
            }

            $this->template->Parse( 'atBlock', 'sectionAccommodationTypeBlock', false );
        }

        if( !empty( $surf_trips ) )
        {
            foreach( $surf_trips as $id => $name )
            {
                if( isset( $prm[ 'term' ] ) && $prm[ 'term' ] == $id )
                {
                    $this->template->set_var( 'term_value', $id );
                    $this->template->set_var( 'term_label', $name );
                }

                $this->template->set_var( 'term_id', $id );
                $this->template->set_var( 'term_name', $name );

                $this->template->Parse( 'sftlBlock', 'sectionSurfTypeLoopBlock', true );
            }

            $this->template->Parse( 'sftBlock', 'sectionSurfTypeBlock', false );
        }

        $guest = 0;

        if( isset( $prm[ 'adult' ] ) && $prm[ 'adult' ] )
        {
            $this->template->set_var( 'adult_value', $prm[ 'adult' ] );

            $guest += $prm[ 'adult' ];
        }
        else
        {
            $guest += 1;

            $this->template->set_var( 'adult_value', 1 );
        }

        if( isset( $prm[ 'child' ] ) && $prm[ 'child' ] )
        {
            $this->template->set_var( 'child_value', $prm[ 'child' ] );

            $guest += $prm[ 'child' ];
        }
        else
        {
            $this->template->set_var( 'child_value', 0 );
        }

        if( isset( $prm[ 'infant' ] ) && $prm[ 'infant' ] )
        {
            $this->template->set_var( 'infant_value', $prm[ 'infant' ] );

            $guest += $prm[ 'infant' ];
        }
        else
        {
            $this->template->set_var( 'infant_value', 0 );
        }

        $this->template->set_var( 'guest_value', $guest );

        if( isset( $prm[ 'checkin' ] ) && $prm[ 'checkin' ] )
        {
            $this->template->set_var( 'checkin_label', date( 'd M Y', strtotime( $prm[ 'checkin' ] ) ) );
            $this->template->set_var( 'checkin_value', $prm[ 'checkin' ] );
        }
        else
        {
            $this->template->set_var( 'checkin_label', $this->translate( 'check-in', 'Check-in' ) );
            $this->template->set_var( 'checkin_value', '' );
        }

        if( isset( $prm[ 'checkout' ] ) && $prm[ 'checkout' ] )
        {
            $this->template->set_var( 'checkout_label', date( 'd M Y', strtotime( $prm[ 'checkout' ] ) ) );
            $this->template->set_var( 'checkout_value', $prm[ 'checkout' ] );
        }
        else
        {
            $this->template->set_var( 'checkout_label', $this->translate( 'check-out', 'Check-out' ) );
            $this->template->set_var( 'checkout_value', '' );
        }

        $this->template->set_var( 'str_1', $this->translate( 'where-are-you-going', 'WHERE ARE YOU GOING?' ) );
        $this->template->set_var( 'str_2', $this->translate( 'type-of-surf-trip', 'TYPE OF SURF TRIP' ) );
        $this->template->set_var( 'str_3', $this->translate( 'check-in', 'CHECK-IN' ) );
        $this->template->set_var( 'str_4', $this->translate( 'check-out', 'CHECK-OUT' ) );
        $this->template->set_var( 'str_5', $this->translate( 'adults', 'ADULTS' ) );
        $this->template->set_var( 'str_6', $this->translate( 'children', 'CHILDREN' ) );
        $this->template->set_var( 'str_7', $this->translate( 'babies', 'BABIES' ) );
        $this->template->set_var( 'str_8', $this->translate( 'done', 'DONE' ) );
        $this->template->set_var( 'str_9', $this->translate( 'search', 'Search' ) );

        $this->template->set_var( 'str_10', $this->translate( 'i-don-t-know-where-to-go', 'I Don\'t Know Where To Go' ) );
        $this->template->set_var( 'str_11', $this->translate( 'popular-destinations', 'Popular Destinations' ) );
        $this->template->set_var( 'str_12', $this->translate( 'type-of-surfing', 'Type of Surfing' ) );
        $this->template->set_var( 'str_13', $this->translate( 'accomodation-type', 'Accomodation Type' ) );

        $this->template->set_var( 'terms_option', $this->terms_option( $surf_trips, $acco_types, $prm ) );
        $this->template->set_var( 'location_option', $this->location_option( $popular, $continent, $prm ) );

        return $this->template->Parse( 'schBlock', 'searchBlock', false );
    }

    public function init_popup_message()
    {
        //-- GET message notification
        if( !isset( $_SESSION[ 'messages' ] ) )
        {
            return null;
        }

        $content = '';

        if( isset( $_SESSION[ 'messages' ][ 'type' ] ) && isset( $_SESSION[ 'messages' ][ 'message' ] ) && !empty( $_SESSION[ 'messages' ][ 'message' ] ) )
        {
            if( is_array( $_SESSION[ 'messages' ][ 'message' ] ) )
            {
                $content .= '
		        <div id="popup-alert" class="popup-alert">
	                <div class="alert alert-' . $_SESSION[ 'messages' ][ 'type' ] .  '">
	                    <ul>';

                foreach( $_SESSION[ 'messages' ][ 'message' ] as $f )
                {
                    $content .= '<li>' . $f . '</li>';
                }

                $content .= '
	                    </ul>
	                </div>
		        </div>';
            }
            else
            {
                $content .= '
		        <div id="popup-alert" class="popup-alert">
	                <div class="alert alert-' . $_SESSION[ 'messages' ][ 'type' ] .  '">
	                    ' . $_SESSION[ 'messages' ][ 'message' ] . '
	                </div>
		        </div>';
            }
        }

        unset( $_SESSION[ 'messages' ] );

        return $content;
    }

    public function init_switcher_lang()
    {
        require_once( INCLUDE_DIR . '/vendor/autoload.php' );

        $s = 'SELECT * FROM lumonata_language AS a WHERE a.lstatus = 1 ORDER BY a.lorder_id';
        $q = parent::prepare_query( $s );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $dlang = $this->get_field_value( 'lumonata_language', 'llang_code', array( 'llang_id' => $this->get_setting_value( 'llanguage' ) ) );
            $lang  = $this->get_current_language_code();

            $path     = explode( '/', ltrim( $_SERVER['REQUEST_URI'], '/' ) );
            $option   = array();
            $selected = '';

            if( empty( $lang ) == false )
            {
                unset( $path[ 0 ] );
            }

            while( $d = parent::fetch_assoc( $r ) )
            {
                try
                {
                    $c = country( $d[ 'lcountry_code' ] );

                    $link = sprintf( '%s%s/switch-language/?prm=%s', HT_SERVER, SITE_URL, base64_encode( json_encode( array( 'data' => $d, 'path' => $_SERVER['REQUEST_URI'] ) ) ) );

                    if( $lang == $d[ 'llang_code' ] || $dlang == $d[ 'llang_code' ] )
                    {
                        $option[ $d[ 'llang_id' ] ] = sprintf( '<li><a href="%s"><i class="sl-flag"><img src="data:image/svg+xml;base64,%s" alt="" width="24" /></i><span class="active">%s</span></a></li>', $link, base64_encode( $c->getFlag() ), $d[ 'llanguage' ] );

                        $selected = sprintf( '<b><i class="sl-flag"><img src="data:image/svg+xml;base64,%s" alt="" width="24" /></i><span>%s</span></b>', base64_encode( $c->getFlag() ), $d[ 'llanguage' ] );
                    }
                    else
                    {
                        $option[ $d[ 'llang_id' ] ] = sprintf( '<li><a href="%s"><i class="sl-flag"><img src="data:image/svg+xml;base64,%s" alt="" width="24" /></i><span>%s</span></a></li>', $link, base64_encode( $c->getFlag() ), $d[ 'llanguage' ] );
                    }
                }
                catch( Exception $e )
                {
                    //-- do nothing
                }
            }

            if( !empty( $option ) )
            {
                return '
	            <div class="nav-wrapper">
	                <div class="sl-nav">
	                    <ul>
	                        <li>
	                            ' . $selected . '
	                            <i class="fa fa-angle-down" aria-hidden="true"></i>
	                            <div class="triangle"></div>
	                            <ul>' . implode( '', $option ) . '</ul>
	                        </li>
	                    </ul>
	                </div>
	            </div>';
            }
        }
    }

    public function init_nav_menu( $group, $show_group_title = false )
    {
        $s = 'SELECT * FROM lumonata_menu_group AS a WHERE a.ltitle = %s AND a.lref_id IS NULL';
        $q = parent::prepare_query( $s, $group );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $dta = parent::fetch_assoc( $r );

            $lang_def  = $this->get_setting_value( 'llanguage' );
            $lang_code = $this->get_current_language_code();
            $lang_id   = $this->get_current_language_id();

            if( $lang_id != $lang_def )
            {
                $dta = $this->get_field_value( 'lumonata_menu_group', '*', array( 'lref_id' => $dta[ 'lgroup_id' ], 'llang_id' => $lang_id ) );
            }

            if( isset( $dta[ 'lgroup_id' ] ) && empty( $lang_id ) === false )
            {
                $qr = parent::prepare_query( 'SELECT * FROM lumonata_menu AS a WHERE a.lgroup_id = %d AND a.llang_id = %d ORDER BY a.lorder_id', $dta[ 'lgroup_id' ], $lang_id );
                $dt = $this->get_menu_recursive_field( $qr, 'lmenu_id', 'lparent_id' );

                if( empty( $dt ) === false )
                {
                    $attr = array_filter( array( 'id' => $dta[ 'lattr_id' ], 'class' => $dta[ 'lattr_class' ] ) );
                    $list = '
		            <ul' . ( empty( $attr ) === false ? $this->concat_attribute( $attr ) : '' ) . '>';

                    foreach( $dt as $d )
                    {
                        if( $d[ 'llink_type' ] == '0' )
                        {
                            $link = $d[ 'lcustom_url' ];
                        }
                        elseif( $d[ 'llink_type' ] == '1' )
                        {
                            if( $lang_id == $lang_def )
                            {
                                $link = sprintf( '%s%s/', HT_SERVER, SITE_URL );
                            }
                            else
                            {
                                $link = sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $lang_code );
                            }
                        }
                        elseif( $d[ 'llink_type' ] == '2' )
                        {
                            $sef = $this->get_field_value( 'lumonata_post', 'lsef_url', array( 'lpost_id' => $d[ 'lapp_id' ] ) );

                            if( empty( $sef ) )
                            {
                                $link = 'javascript:;';
                            }
                            else
                            {
                                if( $lang_id == $lang_def )
                                {
                                    $link = sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $sef );
                                }
                                else
                                {
                                    $link = sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $lang_code, $sef );
                                }
                            }
                        }
                        elseif( $d[ 'llink_type' ] == '3' )
                        {
                            $apps = $this->get_module_apps( $d[ 'lmodule_id' ], 'front' );

                            if( empty( $apps ) )
                            {
                                $link = 'javascript:;';
                            }
                            else
                            {
                                if( $lang_id == $lang_def )
                                {
                                    $link = sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $apps );
                                }
                                else
                                {
                                    $link = sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $lang_code, $apps );
                                }
                            }
                        }
                        elseif( $d[ 'llink_type' ] == '4' )
                        {
                            $sef  = $this->get_field_value( 'lumonata_post', 'lsef_url', array( 'lpost_id' => $d[ 'lapp_id' ] ) );
                            $apps = $this->get_module_apps( $d[ 'lmodule_id' ], 'front' );

                            if( empty( $apps ) || empty( $sef ) )
                            {
                                $link = 'javascript:;';
                            }
                            else
                            {
                                if( $lang_id == $lang_def )
                                {
                                    $link = sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $apps, $sef );
                                }
                                else
                                {
                                    $link = sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $lang_code, $apps, $sef );
                                }
                            }
                        }
                        elseif( $d[ 'llink_type' ] == '5' )
                        {
                            $apps = $this->get_module_apps( $d[ 'lmodule_id' ], 'front' );

                            if( empty( $apps ) )
                            {
                                $link = 'javascript:;';
                            }
                            else
                            {
                                if( $lang_id == $lang_def )
                                {
                                    $link = sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $apps );
                                }
                                else
                                {
                                    $link = sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $lang_code, $apps );
                                }
                            }
                        }
                        elseif( $d[ 'llink_type' ] == '6' )
                        {
                            $sef  = $this->get_field_value( 'lumonata_post_terms', 'lsef', array( 'lterm_id' => $d[ 'lapp_id' ] ) );
                            $apps = $this->get_module_apps( $d[ 'lmodule_id' ], 'front' );

                            if( empty( $apps ) || empty( $sef ) )
                            {
                                $link = 'javascript:;';
                            }
                            else
                            {
                                if( $lang_id == $lang_def )
                                {
                                    $link = sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $apps, $sef );
                                }
                                else
                                {
                                    $link = sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $lang_code, $apps, $sef );
                                }
                            }
                        }
                        elseif( $d[ 'llink_type' ] == '7' )
                        {
                            $link = 'javascript:;';
                        }

                        if( $d[ 'llink_type' ] == '7' )
                        {
                            //-- Popup Link
                            if( isset( $d[ 'child' ] ) && !empty( $d[ 'child' ] ) )
                            {
                                $list .= sprintf( '<li><a href="%s" target="%s" data-trigger="%s">%s</a>%s</li>', $link, $d[ 'ltarget' ], $d[ 'ltrigger' ], $d[ 'lmenu' ], $this->init_child_nav_menu( $d[ 'child' ] ) );
                            }
                            else
                            {
                                $list .= sprintf( '<li><a href="%s" target="%s" data-trigger="%s">%s</a></li>', $link, $d[ 'ltarget' ], $d[ 'ltrigger' ], $d[ 'lmenu' ] );
                            }
                        }
                        else
                        {
                            //-- Other
                            if( isset( $d[ 'child' ] ) && !empty( $d[ 'child' ] ) )
                            {
                                $list .= sprintf( '<li><a href="%s" target="%s">%s</a>%s</li>', $link, $d[ 'ltarget' ], $d[ 'lmenu' ], $this->init_child_nav_menu( $d[ 'child' ] ) );
                            }
                            else
                            {
                                $list .= sprintf( '<li><a href="%s" target="%s">%s</a></li>', $link, $d[ 'ltarget' ], $d[ 'lmenu' ] );
                            }
                        }
                    }

                    $list .= '
		            </ul>';

                    if( $show_group_title === true )
                    {
                        $list = sprintf( '<ul><li><a class="group-title" href="javascript:;">%s</a>%s</li></ul>', $dta[ 'ltitle' ], $list );
                    }

                    return $list;
                }
            }
        }
    }

    public function init_child_nav_menu( $data, $list = '' )
    {
        $lang_def  = $this->get_setting_value( 'llanguage' );
        $lang_code = $this->get_current_language_code();
        $lang_id   = $this->get_current_language_id();

        if( empty( $data ) === false )
        {
            $list .= '
			<ul>';

            foreach( $data as $d )
            {
                if( $d[ 'llink_type' ] == '0' )
                {
                    $link = $d[ 'lcustom_url' ];
                }
                elseif( $d[ 'llink_type' ] == '1' )
                {
                    if( $lang_id == $lang_def )
                    {
                        $link = sprintf( '%s%s/', HT_SERVER, SITE_URL );
                    }
                    else
                    {
                        $link = sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $lang_code );
                    }
                }
                elseif( $d[ 'llink_type' ] == '2' )
                {
                    $sef = $this->get_field_value( 'lumonata_post', 'lsef_url', array( 'lpost_id' => $d[ 'lapp_id' ] ) );

                    if( empty( $sef ) )
                    {
                        $link = 'javascript:;';
                    }
                    else
                    {
                        if( $lang_id == $lang_def )
                        {
                            $link = sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $sef );
                        }
                        else
                        {
                            $link = sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $lang_code, $sef );
                        }
                    }
                }
                elseif( $d[ 'llink_type' ] == '3' )
                {
                    $apps = $this->get_module_apps( $d[ 'lmodule_id' ], 'front' );

                    if( empty( $apps ) )
                    {
                        $link = 'javascript:;';
                    }
                    else
                    {
                        if( $lang_id == $lang_def )
                        {
                            $link = sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $apps );
                        }
                        else
                        {
                            $link = sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $lang_code, $apps );
                        }
                    }
                }
                elseif( $d[ 'llink_type' ] == '4' )
                {
                    $sef  = $this->get_field_value( 'lumonata_post', 'lsef_url', array( 'lpost_id' => $d[ 'lapp_id' ] ) );
                    $apps = $this->get_module_apps( $d[ 'lmodule_id' ], 'front' );

                    if( empty( $apps ) || empty( $sef ) )
                    {
                        $link = 'javascript:;';
                    }
                    else
                    {
                        if( $lang_id == $lang_def )
                        {
                            $link = sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $apps, $sef );
                        }
                        else
                        {
                            $link = sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $lang_code, $apps, $sef );
                        }
                    }
                }
                elseif( $d[ 'llink_type' ] == '5' )
                {
                    $apps = $this->get_module_apps( $d[ 'lmodule_id' ], 'front' );

                    if( empty( $apps ) )
                    {
                        $link = 'javascript:;';
                    }
                    else
                    {
                        if( $lang_id == $lang_def )
                        {
                            $link = sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $apps );
                        }
                        else
                        {
                            $link = sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $lang_code, $apps );
                        }
                    }
                }
                elseif( $d[ 'llink_type' ] == '6' )
                {
                    $sef  = $this->get_field_value( 'lumonata_post_terms', 'lsef', array( 'lterm_id' => $d[ 'lapp_id' ] ) );
                    $apps = $this->get_module_apps( $d[ 'lmodule_id' ], 'front' );

                    if( empty( $apps ) || empty( $sef ) )
                    {
                        $link = 'javascript:;';
                    }
                    else
                    {
                        if( $lang_id == $lang_def )
                        {
                            $link = sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $apps, $sef );
                        }
                        else
                        {
                            $link = sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $lang_code, $apps, $sef );
                        }
                    }
                }
                elseif( $d[ 'llink_type' ] == '7' )
                {
                    $link = 'javascript:;';
                }

                if( $d[ 'llink_type' ] == '7' )
                {
                    //-- Popup Link
                    if( isset( $d[ 'child' ] ) && !empty( $d[ 'child' ] ) )
                    {
                        $list .= sprintf( '<li><a href="%s" target="%s" data-trigger="%s">%s</a>%s</li>', $link, $d[ 'ltarget' ], $d[ 'ltrigger' ], $d[ 'lmenu' ], $this->init_child_nav_menu( $d[ 'child' ] ) );
                    }
                    else
                    {
                        $list .= sprintf( '<li><a href="%s" target="%s" data-trigger="%s">%s</a></li>', $link, $d[ 'ltarget' ], $d[ 'ltrigger' ], $d[ 'lmenu' ] );
                    }
                }
                else
                {
                    //-- Other
                    if( isset( $d[ 'child' ] ) && !empty( $d[ 'child' ] ) )
                    {
                        $list .= sprintf( '<li><a href="%s" target="%s">%s</a>%s</li>', $link, $d[ 'ltarget' ], $d[ 'lmenu' ], $this->init_child_nav_menu( $d[ 'child' ] ) );
                    }
                    else
                    {
                        $list .= sprintf( '<li><a href="%s" target="%s">%s</a></li>', $link, $d[ 'ltarget' ], $d[ 'lmenu' ] );
                    }
                }
            }

            $list .= '
			</ul>';
        }

        return $list;
    }

    public function concat_attribute( $attrs = array() )
    {
        if( empty( $attrs ) )
        {
            return '';
        }

        $attributes = '';

        foreach( $attrs as $attr => $val )
        {
            if( $val != '' && 'value' !== $attr )
            {
                $val = is_array( $val ) ? implode( ',', $val ) : $val;

                //-- if data attribute, use single quote wraps, else double.
                $attributes .= sprintf( ' %1$s=%3$s%2$s%3$s', $attr, $val, '"' );
            }
        }

        return $attributes;
    }

    public function translate( $code, $string, $output = '', $param = array() )
    {
        //-- GET language
        $lang_code = $this->get_current_language_code();
        $translate = $this->get_field_value( 'lumonata_translations', 'ltranslate_content', array( 'ltranslate_code' => $code ) );

        if( empty( $translate ) === false )
        {
            $d = json_decode( $translate, true );

            if( isset( $d[ $lang_code ] ) )
            {
                $string = nl2br( $d[ $lang_code ] );

                if( empty( $param ) === false )
                {
                    $string = vsprintf( $string, $param );
                }
            }
        }
        else
        {
            if( empty( $param ) === false )
            {
                $string = vsprintf( $string, $param );
            }
        }

        //-- GET output type
        if( $output == 1 )
        {
            return ucwords( strtolower( $string ) );
        }
        elseif( $output == 2 )
        {
            return strtolower( $string );
        }
        elseif( $output == 3 )
        {
            return strtoupper( $string );
        }
        else
        {
            return $string;
        }
    }

    public function accommodation_type_option( $lang = '' )
    {
        $s = 'SELECT a.lterm_id, a.lname FROM lumonata_post_terms AS a WHERE a.lrule = %s AND a.lgroup = %s AND a.lstatus = %d AND a.llang_id = %d';
        $q = parent::prepare_query( $s, 'accommodation_type', 'accommodation', 1, $lang );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $option = array();

            while( $d = parent::fetch_array( $r ) )
            {
                $option[ $d[ 'lterm_id' ] ] = $d[ 'lname' ];
            }

            return $option;
        }
    }

    public function surf_trips_option( $lang = '' )
    {
        $s = 'SELECT a.lterm_id, a.lname FROM lumonata_post_terms AS a WHERE a.lrule = %s AND a.lgroup = %s AND a.lstatus = %d AND a.llang_id = %d';
        $q = parent::prepare_query( $s, 'surf_trip', 'accommodation', 1, $lang );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $option = array();

            while( $d = parent::fetch_array( $r ) )
            {
                $option[ $d[ 'lterm_id' ] ] = $d[ 'lname' ];
            }

            return $option;
        }
    }

    public function continent_option( $lang = '' )
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = "destination" AND ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d AND a.lstatus = %d AND a.llang_id = %d';
        $q = parent::prepare_query( $s, 'destination_type', 0, 1, $lang );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $option = array();

            while( $d = parent::fetch_array( $r ) )
            {
                $option[ $d[ 'lpost_id' ] ] = $d[ 'ltitle' ];
            }

            return $option;
        }
    }

    public function popular_option( $lang = '' )
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = "destination" AND ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d AND a.lstatus = %d AND a.llang_id = %d';
        $q = parent::prepare_query( $s, 'popular_destination', 1, 1, $lang );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $option = array();

            while( $d = parent::fetch_array( $r ) )
            {
                $option[ $d[ 'lpost_id' ] ] = $d[ 'ltitle' ];
            }

            return $option;
        }
    }

    public function location_option( $popular, $continent, $prm )
    {
        $option = '';

        if( !empty( $popular ) )
        {
            $option .= '
			<optgroup label="' . $this->translate( 'popular-destinations', 'Popular Destinations' ) . '">';

            foreach( $popular as $id => $name )
            {
                $option .= '
			    	<option value="' . $id . '"' . ( isset( $prm[ 'dest' ] ) && $prm[ 'dest' ] == $id ? ' selected' : '' ) . '>' . $name . '</option>';
            }

            $option .= '
			</optgroup>';
        }

        if( !empty( $continent ) )
        {
            foreach( $continent as $id => $name )
            {
                $option .= '
			    <option value="' . $id . '"' . ( isset( $prm[ 'dest' ] ) && $prm[ 'dest' ] == $id ? ' selected' : '' ) . '>' . $name . '</option>';
            }
        }

        return $option;
    }

    public function terms_option( $surf_trips, $acco_types )
    {
        $option = '';

        if( !empty( $surf_trips ) )
        {
            $option .= '
			<optgroup label="' . $this->translate( 'type-of-surfing', 'Type of Surfing' ) . '">';

            foreach( $surf_trips as $id => $name )
            {
                $option .= '
			    	<option value="' . $id . '"' . ( isset( $prm[ 'term' ] ) && $prm[ 'term' ] == $id ? ' selected' : '' ) . '>' . $name . '</option>';
            }

            $option .= '
			</optgroup>';
        }

        if( !empty( $acco_types ) )
        {
            $option .= '
			<optgroup label="' . $this->translate( 'accomodation-type', 'Accomodation Type' ) . '">';

            foreach( $acco_types as $id => $name )
            {
                $option .= '
			    	<option value="' . $id . '"' . ( isset( $prm[ 'term' ] ) && $prm[ 'term' ] == $id ? ' selected' : '' ) . '>' . $name . '</option>';
            }

            $option .= '
			</optgroup>';
        }

        return $option;
    }

    public function unique_code( $prefix = '', $table = '', $field = '' )
    {
        $p = $prefix . date( 'y' ) . date( 'm' );
        $n = strlen( $p );
        $l = $n + 1;

        $q = parent::prepare_query( 'SELECT MAX( substring( ' . $field . ', ' . $l . ', 4 ) ) as tnumber FROM ' . $table . ' WHERE substring( ' . $field . ', 1, ' . $n . ' ) = %s', $p );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            $number = 1;
        }
        else
        {
            $d = parent::fetch_array( $r );

            $number = $d['tnumber'] == null ? 1 : intval( $d['tnumber'] ) + 1;
        }

        if( $number > 9999 )
        {
            $number = 1;
        }
        elseif( $number < 10 )
        {
            $number = '000' . $number;
        }
        elseif( $number < 100 )
        {
            $number = '00' . $number;
        }
        elseif( $number < 1000 )
        {
            $number = '0' . $number;
        }
        else
        {
            $number = $number;
        }

        $unique_code = $p . $number;

        return $unique_code;
    }

    public function get_posts( $conditions = array(), $fetch = true, $order_by = array(), $limit = 0, $operator = 'AND' )
    {
        //-- PREPARE parameter
        $w = array();

        if( is_array( $conditions ) && empty( $conditions ) === false )
        {
            foreach( $conditions as $field => $value )
            {
                if( !preg_match( '/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
                {
                    $field .= ' = ';
                }

                $w[] = parent::prepare_query( $field . '%s', $value );
            }
        }

        if( empty( $w ) === false )
        {
            $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
        }
        else
        {
            $where = '';
        }

        if( empty( $order_by ) === false )
        {
            if( array_key_exists( 0, $order_by ) )
            {
                $order = ' ORDER BY '. implode( ', ', $order_by );
            }
            else
            {
                $order = ' ORDER BY ' . str_replace( '=', ' ', http_build_query( $order_by, null, ',' ) );
            }
        }
        else
        {
            $order = ' ORDER BY lorder_id ASC';
        }

        if( empty( $limit ) === false )
        {
            if( is_array( $limit ) )
            {
                $view = ' LIMIT '. implode( ', ', $limit );
            }
            else
            {
                $view = ' LIMIT '. $limit;
            }
        }
        else
        {
            $view = '';
        }

        $data = array();

        $r = parent::query( 'SELECT * FROM lumonata_post' . $where . $order . $view );

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_assoc( $r ) )
            {
                //-- GET additional field
                $sa = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lapp_id = %d AND a.lmodule_id = ( SELECT a2.lmodule_id FROM lumonata_module AS a2 WHERE a2.lapps = %s )';
                $qa = parent::prepare_query( $sa, $d[ 'lpost_id' ], $d[ 'ltype' ] );
                $ra = parent::query( $qa );

                if( parent::num_rows( $ra ) > 0 )
                {
                    while( $da = parent::fetch_assoc( $ra ) )
                    {
                        $d[ $da[ 'ladditional_key' ] ] = $da[ 'ladditional_value' ];
                    }
                }

                //-- GET term field
                $st = 'SELECT b.* FROM lumonata_post_relationship AS a LEFT JOIN lumonata_post_terms AS b ON a.lterm_id = b.lterm_id WHERE a.lpost_id = %d AND b.lgroup = %s';
                $qt = parent::prepare_query( $st, $d[ 'lpost_id' ], $d[ 'ltype' ] );
                $rt = parent::query( $qt );

                if( parent::num_rows( $rt ) > 0 )
                {
                    while( $dt = parent::fetch_assoc( $rt ) )
                    {
                        //-- GET term additional field
                        $sf = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lterm_id = %d';
                        $qf = parent::prepare_query( $sf, $dt[ 'lterm_id' ] );
                        $rf = parent::query( $qf );

                        if( parent::num_rows( $rf ) > 0 )
                        {
                            while( $df = parent::fetch_assoc( $rf ) )
                            {
                                $dt[ $df[ 'ladditional_key' ] ] = $df[ 'ladditional_value' ];
                            }
                        }

                        $d[ $dt[ 'lrule' ] ][] = $dt;
                    }
                }

                if( $fetch )
                {
                    return $d;

                    break;
                }

                $data[] = $d;
            }
        }

        return $data;
    }

    public function get_posts_relationship( $conditions = array(), $fetch = true, $order_by = array(), $limit = 0, $operator = 'AND' )
    {
        //-- PREPARE parameter
        $w = array();

        if( is_array( $conditions ) && empty( $conditions ) === false )
        {
            foreach( $conditions as $field => $value )
            {
                if( !preg_match( '/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
                {
                    $field .= ' = ';
                }

                $w[] = parent::prepare_query( $field . '%s', $value );
            }
        }

        if( empty( $w ) === false )
        {
            $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
        }
        else
        {
            $where = '';
        }

        if( empty( $order_by ) === false )
        {
            if( array_key_exists( 0, $order_by ) )
            {
                $order = ' ORDER BY '. implode( ', ', $order_by );
            }
            else
            {
                $order = ' ORDER BY ' . str_replace( '=', ' ', http_build_query( $order_by, null, ',' ) );
            }
        }

        if( empty( $limit ) === false )
        {
            if( is_array( $limit ) )
            {
                $view = ' LIMIT '. implode( ', ', $limit );
            }
            else
            {
                $view = ' LIMIT '. $limit;
            }
        }

        $data = array();

        $q = 'SELECT * FROM lumonata_post_relationship AS a
			  LEFT JOIN lumonata_post AS b ON a.lpost_id = b.lpost_id
			  LEFT JOIN lumonata_post_terms AS c ON a.lterm_id = c.lterm_id' . $where . $order . $view;
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_assoc( $r ) )
            {
                //-- GET additional field
                $sa = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lapp_id = %d AND a.lmodule_id = ( SELECT a2.lmodule_id FROM lumonata_module AS a2 WHERE a2.lapps = %s )';
                $qa = parent::prepare_query( $sa, $d[ 'lpost_id' ], $d[ 'ltype' ] );
                $ra = parent::query( $qa );

                if( parent::num_rows( $ra ) > 0 )
                {
                    while( $da = parent::fetch_assoc( $ra ) )
                    {
                        $d[ $da[ 'ladditional_key' ] ] = $da[ 'ladditional_value' ];
                    }
                }

                //-- GET term field
                $st = 'SELECT b.* FROM lumonata_post_relationship AS a LEFT JOIN lumonata_post_terms AS b ON a.lterm_id = b.lterm_id WHERE a.lpost_id = %d AND b.lgroup = %s';
                $qt = parent::prepare_query( $st, $d[ 'lpost_id' ], $d[ 'ltype' ] );
                $rt = parent::query( $qt );

                if( parent::num_rows( $rt ) > 0 )
                {
                    while( $dt = parent::fetch_assoc( $rt ) )
                    {
                        //-- GET term additional field
                        $sf = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lterm_id = %d';
                        $qf = parent::prepare_query( $sf, $dt[ 'lterm_id' ] );
                        $rf = parent::query( $qf );

                        if( parent::num_rows( $rf ) > 0 )
                        {
                            while( $df = parent::fetch_assoc( $rf ) )
                            {
                                $dt[ $df[ 'ladditional_key' ] ] = $df[ 'ladditional_value' ];
                            }
                        }

                        $d[ $dt[ 'lrule' ] ][] = $dt;
                    }
                }

                if( $fetch )
                {
                    return $d;

                    break;
                }

                $data[] = $d;
            }
        }

        return $data;
    }

    public function get_posts_child( $parent_id, $child = array() )
    {
        $lang = $this->get_current_language_id();
        $data = $this->get_posts( array( 'lparent_id' => $parent_id, 'llang_id' => $lang ), false );

        if( !empty( $data ) )
        {
            foreach( $data as $dt )
            {
                $child[] = $dt;

                $child = $this->get_posts_child( $dt[ 'lpost_id' ], $child );
            }
        }

        return $child;
    }

    public function get_post_terms( $conditions = array(), $operator = 'AND' )
    {
        //-- PREPARE parameter
        $w = array();

        if( is_array( $conditions ) && empty( $conditions ) === false )
        {
            foreach( $conditions as $field => $value )
            {
                if( !preg_match( '/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
                {
                    $field .= ' = ';
                }

                $w[] = parent::prepare_query( $field . '%s', $value );
            }
        }

        if( empty( $w ) === false )
        {
            $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
        }
        else
        {
            $where = '';
        }

        $data = array();

        $r = parent::query( 'SELECT * FROM lumonata_post_terms' . $where );

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_assoc( $r ) )
            {
                //-- GET additional field
                $sa = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lterm_id = %d';
                $qa = parent::prepare_query( $sa, $d[ 'lterm_id' ] );
                $ra = parent::query( $qa );

                if( parent::num_rows( $ra ) > 0 )
                {
                    while( $da = parent::fetch_assoc( $ra ) )
                    {
                        $d[ $da[ 'ladditional_key' ] ] = $da[ 'ladditional_value' ];
                    }
                }

                $data[] = $d;
            }
        }

        return $data;
    }

    public function get_packages( $conditions = array(), $fetch = true, $order_by = array(), $limit = 0, $operator = 'AND' )
    {
        //-- PREPARE parameter
        $w = array();

        if( is_array( $conditions ) && empty( $conditions ) === false )
        {
            foreach( $conditions as $field => $value )
            {
                if( !preg_match( '/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
                {
                    $field .= ' = ';
                }

                $w[] = parent::prepare_query( $field . '%s', $value );
            }
        }

        if( empty( $w ) === false )
        {
            $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
        }
        else
        {
            $where = '';
        }

        if( empty( $order_by ) === false )
        {
            if( array_key_exists( 0, $order_by ) )
            {
                $order = ' ORDER BY '. implode( ', ', $order_by );
            }
            else
            {
                $order = ' ORDER BY ' . str_replace( '=', ' ', http_build_query( $order_by, null, ',' ) );
            }
        }
        else
        {
            $order = ' ORDER BY lorder_id ASC';
        }

        if( empty( $limit ) === false )
        {
            if( is_array( $limit ) )
            {
                $view = ' LIMIT '. implode( ', ', $limit );
            }
            else
            {
                $view = ' LIMIT '. $limit;
            }
        }
        else
        {
            $view = '';
        }

        $data = array();

        $r = parent::query( 'SELECT * FROM lumonata_packages' . $where . $order . $view );

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_assoc( $r ) )
            {
                $q2 = parent::prepare_query( 'SELECT * FROM lumonata_package_details WHERE lpackage_id = %d', $d[ 'lpackage_id' ] );
                $r2 = parent::query( $q2 );

                if( parent::num_rows( $r2 ) > 0 )
                {
                    while( $d2 = parent::fetch_assoc( $r2 ) )
                    {
                        $d[ 'details' ][] = $d2;
                    }
                }

                if( $fetch )
                {
                    return $d;

                    break;
                }

                $data[] = $d;
            }
        }

        return $data;
    }

    public function get_schedule( $conditions = array(), $fetch = true, $order_by = array(), $limit = 0, $operator = 'AND' )
    {
        //-- PREPARE parameter
        $w = array();

        if( is_array( $conditions ) && empty( $conditions ) === false )
        {
            foreach( $conditions as $field => $value )
            {
                if( !preg_match( '/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
                {
                    $field .= ' = ';
                }

                $w[] = parent::prepare_query( $field . '%s', $value );
            }
        }

        if( empty( $w ) === false )
        {
            $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
        }
        else
        {
            $where = '';
        }

        if( empty( $order_by ) === false )
        {
            if( array_key_exists( 0, $order_by ) )
            {
                $order = ' ORDER BY '. implode( ', ', $order_by );
            }
            else
            {
                $order = ' ORDER BY ' . str_replace( '=', ' ', http_build_query( $order_by, null, ',' ) );
            }
        }
        else
        {
            $order = ' ORDER BY lorder_id ASC';
        }

        if( empty( $limit ) === false )
        {
            if( is_array( $limit ) )
            {
                $view = ' LIMIT '. implode( ', ', $limit );
            }
            else
            {
                $view = ' LIMIT '. $limit;
            }
        }
        else
        {
            $view = '';
        }

        $data = array();

        $r = parent::query( 'SELECT * FROM lumonata_schedules' . $where . $order . $view );

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_assoc( $r ) )
            {
                if( $fetch )
                {
                    return $d;

                    break;
                }

                $data[] = $d;
            }
        }

        return $data;
    }

    public function get_payment_url( $id )
    {
        return HT_SERVER . SITE_URL . '/booking/payment/' . base64_encode( json_encode( array( 'id' => $id, 'expired' => strtotime( '+48 hours' ) ) ) );
    }

    public function build_paging( $base_url, $cur_page, $number_of_pages, $prev_next = false )
    {
        $ends_count   = 1;  //-- how many items at the ends (before and after [...])
        $middle_count = 2;  //-- how many items before and after current page

        $dots = false;
        $item = array();

        if( $prev_next )
        {
            if( $cur_page && 1 < $cur_page )
            {
                $link = $base_url . '?page=' . ( $cur_page - 1 );
                $stat = 'active';
            }
            else
            {
                $link = 'javascript:;';
                $stat = 'disabled';
            }

            $item[] = array(
                'label'  => '&laquo; Previous',
                'type'   => 'prevbtn',
                'status' => $stat,
                'link'   => $link
            );
        }

        for( $i = 1; $i <= $number_of_pages; $i++ )
        {
            if( $i == $cur_page )
            {
                $item[] = array(
                    'label'  => $i,
                    'type'   => 'link',
                    'status' => 'current',
                    'link'   => 'javascript:;'
                );

                $dots = true;
            }
            else
            {
                if( $i <= $ends_count || ( $cur_page && $i >= $cur_page - $middle_count && $i <= $cur_page + $middle_count ) || $i > $number_of_pages - $ends_count )
                {
                    $item[] = array(
                        'label'  => $i,
                        'type'   => 'link',
                        'status' => 'active',
                        'link'   => $base_url . '?page=' . $i
                    );

                    $dots = true;
                }
                elseif( $dots )
                {
                    $item[] = array(
                        'label'  => '&hellip;',
                        'type'   => 'dots',
                        'status' => 'active',
                        'link'   => 'javascript:;'
                    );

                    $dots = false;
                }
            }
        }

        if( $prev_next )
        {
            if( $cur_page && ( $cur_page < $number_of_pages || -1 == $number_of_pages ) )
            {
                $link = $base_url . '?page=' . ( $cur_page + 1 );
                $stat = 'active';
            }
            else
            {
                $link = 'javascript:;';
                $stat = 'disabled';
            }

            $item[] = array(
                'label'  => 'Next &raquo;',
                'type'   => 'nextbtn',
                'status' => $stat,
                'link'   => $link
            );
        }

        return $item;
    }

    public function get_field_value( $table, $fields = '*', $conditions = array(), $operator = 'AND' )
    {
        //-- CHECK table exist
        $s = 'SELECT count(*) AS num FROM information_schema.TABLES WHERE TABLE_SCHEMA = %s AND TABLE_NAME = %s';
        $q = parent::prepare_query( $s, DBNAME, $table );
        $r = parent::query( $q );

        $data = array();

        if( !is_array( $r ) )
        {
            $d = parent::fetch_array( $r );

            if( $d[ 'num' ] > 0 )
            {
                //-- PREPARE parameter
                $w = array();

                if( is_array( $conditions ) && empty( $conditions ) === false )
                {
                    foreach( $conditions as $field => $value )
                    {
                        if( !preg_match( '/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
                        {
                            $field .= ' = ';
                        }

                        $w[] = parent::prepare_query( $field . '%s', $value );
                    }
                }

                if( is_array( $fields ) && empty( $fields ) === false )
                {
                    $param = implode( ', ', $fields );
                }
                else
                {
                    $param = $fields;
                }

                if( empty( $w ) === false )
                {
                    $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
                }
                else
                {
                    $where = '';
                }

                $r = parent::query( 'SELECT ' . $param . ' FROM ' . $table . $where );

                if( is_array( $r ) === false )
                {
                    $n = parent::num_rows( $r );

                    if( $n > 1 )
                    {
                        while( $d = parent::fetch_assoc( $r ) )
                        {
                            $data[] = $d;
                        }
                    }
                    elseif( $n > 0 )
                    {
                        $data = parent::fetch_assoc( $r );

                        if( $fields != '*' && is_string( $fields ) )
                        {
                            if( isset( $data[ $fields ] ) )
                            {
                                $data = $data[ $fields ];
                            }
                            else
                            {
                                $data = '';
                            }
                        }
                    }
                }
            }
        }

        return $data;
    }

    public function get_num_rows( $table, $conditions = array(), $operator = 'AND' )
    {
        //-- CHECK table exist
        $s = 'SELECT count(*) AS num FROM information_schema.TABLES WHERE TABLE_SCHEMA = %s AND TABLE_NAME = %s';
        $q = parent::prepare_query( $s, DBNAME, $table );
        $r = parent::query( $q );

        $n = 0;

        if( !is_array( $r ) )
        {
            $d = parent::fetch_array( $r );

            if( $d[ 'num' ] > 0 )
            {
                //-- PREPARE parameter
                $w = array();

                if( is_array( $conditions ) && empty( $conditions ) === false )
                {
                    foreach( $conditions as $field => $value )
                    {
                        if( !preg_match( '/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
                        {
                            $field .= ' = ';
                        }

                        $w[] = parent::prepare_query( $field . '%s', $value );
                    }
                }

                if( empty( $w ) === false )
                {
                    $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
                }
                else
                {
                    $where = '';
                }

                $r = parent::query( 'SELECT * FROM ' . $table . $where );

                if( is_array( $r ) === false )
                {
                    $n = parent::num_rows( $r );
                }
            }
        }

        return $n;
    }

    public function get_setting_value( $option_name = '', $app_name = 'global_setting' )
    {
        $d = $this->get_posts( array( 'ltype' => $app_name ) );

        if( isset( $d[ $option_name ] ) && $option_name != '' )
        {
            return $d[ $option_name ];
        }
        else
        {
            return $d;
        }
    }

    public function get_user_session()
    {
        $sess = array( 'userid' => null, 'username' => null, 'usertype' => null );

        if( isset( $_SESSION[ SESSION_NAME ] ) )
        {
            $prm = json_decode( base64_decode( $_SESSION[ SESSION_NAME ] ), true );

            if( $prm !== null && json_last_error() === JSON_ERROR_NONE )
            {
                $sess = $prm;
            }
        }

        return $sess;
    }

    public function get_agent_session()
    {
        $sess = array( 'userid' => null, 'username' => null, 'usertype' => null );

        if( isset( $_SESSION[ AGENT_SESSION_NAME ] ) )
        {
            $prm = json_decode( base64_decode( $_SESSION[ AGENT_SESSION_NAME ] ), true );

            if( $prm !== null && json_last_error() === JSON_ERROR_NONE )
            {
                $sess = $prm;
            }
        }

        return $sess;
    }

    public function get_member_session()
    {
        $sess = array( 'userid' => null, 'username' => null, 'usertype' => null );

        if( isset( $_SESSION[ MEMBER_SESSION_NAME ] ) )
        {
            $prm = json_decode( base64_decode( $_SESSION[ MEMBER_SESSION_NAME ] ), true );

            if( $prm !== null && json_last_error() === JSON_ERROR_NONE )
            {
                $sess = $prm;
            }
        }

        return $sess;
    }

    public function get_current_session()
    {
        $path = explode( '/', ltrim( $_SERVER['REQUEST_URI'], '/' ) );

        if( in_array( 'lumonata-admin', $path ) )
        {
            return $this->get_user_session();
        }
        elseif( in_array( 'agent', $path ) )
        {
            return $this->get_agent_session();
        }
        elseif( in_array( 'member', $path ) )
        {
            return $this->get_member_session();
        }
    }

    public function get_current_language_id()
    {
        $path = explode( '/', ltrim( $_SERVER['REQUEST_URI'], '/' ) );

        foreach( array_keys( $path, 'nomad', true ) as $key )
        {
            unset( $path[ $key ] );
        }

        $mod = array_shift( $path );

        if( $this->is_translation( $mod ) && isset( $path[ 0 ] ) )
        {
            $lang = $this->get_field_value( 'lumonata_language', 'llang_id', array( 'llang_code' => $mod ) );
        }
        else
        {
            $lang = $this->get_setting_value( 'llanguage' );
        }

        return $lang;
    }

    public function get_current_language_code()
    {
        $path = explode( '/', ltrim( $_SERVER['REQUEST_URI'], '/' ) );

        foreach( array_keys( $path, 'nomad', true ) as $key )
        {
            unset( $path[ $key ] );
        }

        $mod = array_shift( $path );

        if( $this->is_translation( $mod ) && isset( $path[ 0 ] ) )
        {
            $lang = $this->get_field_value( 'lumonata_language', 'llang_code', array( 'llang_code' => $mod ) );
        }
        else
        {
            $lang = $this->get_field_value( 'lumonata_language', 'llang_code', array( 'llang_id' => $this->get_setting_value( 'llanguage' ) ) );
        }

        return $lang;
    }

    public function get_setting_id( $id, $option_name )
    {
        return $this->get_field_value( 'lumonata_additional_field', 'ladditional_id', array( 'ladditional_key' => $option_name, 'lapp_id' => $id ) );
    }

    public function get_module_id( $app_name = 'global_setting' )
    {
        return $this->get_field_value( 'lumonata_module', 'lmodule_id', array( 'lapps' => $app_name ) );
    }

    public function get_module_apps( $module_id, $type = '' )
    {
        if( $type == 'front' )
        {
            $apps = $this->get_field_value( 'lumonata_module', 'lappsfront', array( 'lmodule_id' => $module_id ) );
        }
        elseif( $type == 'agent' )
        {
            $apps = $this->get_field_value( 'lumonata_module', 'lappsagent', array( 'lmodule_id' => $module_id ) );
        }
        elseif( $type == 'member' )
        {
            $apps = $this->get_field_value( 'lumonata_module', 'lappsmember', array( 'lmodule_id' => $module_id ) );
        }
        else
        {
            $apps = $this->get_field_value( 'lumonata_module', 'lapps', array( 'lmodule_id' => $module_id ) );
        }

        if( empty( $apps ) === false )
        {
            return $apps;
        }
        else
        {
            return '';
        }
    }

    public function get_booking( $booking_id = null )
    {
        $s = 'SELECT * FROM lumonata_booking AS a WHERE a.lbooking_id = %d';
        $q = parent::prepare_query( $s, $booking_id );
        $r = parent::query( $q );

        $booking = array();

        if( parent::num_rows( $r ) > 0 )
        {
            $d = parent::fetch_assoc( $r );

            $d[ 'lposts_data' ] = $this->get_posts( array(
                'lpost_id' => $d[ 'lpost_id' ]
            ) );

            if( empty( $d[ 'ltrips' ] ) === false )
            {
                $d[ 'ltrips' ] = json_decode( $d[ 'ltrips' ], true );
            }

            if( empty( $d[ 'lrooms' ] ) === false )
            {
                $d[ 'lrooms' ] = json_decode( $d[ 'lrooms' ], true );
            }

            if( empty( $d[ 'laddons' ] ) === false )
            {
                $d[ 'laddons' ] = json_decode( $d[ 'laddons' ], true );
            }

            if( empty( $d[ 'lguests' ] ) === false )
            {
                $d[ 'lguests' ] = json_decode( $d[ 'lguests' ], true );
            }

            $booking = $d;
        }

        return $booking;
    }

    public function get_related_post( $post_data, $lang = 0 )
    {
        $w = array();

        if( isset( $post_data[ 'accommodation_city' ] ) && $post_data[ 'accommodation_city' ] != '' )
        {
            $dest = $post_data[ 'accommodation_city' ];

            $w[] = parent::prepare_query( '( SELECT GROUP_CONCAT( a2.ladditional_value ) FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d ', 'accommodation_city', $post_data[ 'accommodation_city' ] );
        }
        elseif( isset( $post_data[ 'accommodation_state' ] ) && $post_data[ 'accommodation_state' ] != '' )
        {
            $dest = $post_data[ 'accommodation_state' ];

            $w[] = parent::prepare_query( '( SELECT GROUP_CONCAT( a2.ladditional_value ) FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d ', 'accommodation_state', $post_data[ 'accommodation_state' ] );
        }
        elseif( isset( $post_data[ 'accommodation_country' ] ) && $post_data[ 'accommodation_country' ] != '' )
        {
            $dest = $post_data[ 'accommodation_country' ];

            $w[] = parent::prepare_query( '( SELECT GROUP_CONCAT( a2.ladditional_value ) FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d ', 'accommodation_country', $post_data[ 'accommodation_country' ] );
        }
        elseif( isset( $post_data[ 'accommodation_continent' ] ) && $post_data[ 'accommodation_continent' ] != '' )
        {
            $dest = $post_data[ 'accommodation_continent' ];

            $w[] = parent::prepare_query( '( SELECT GROUP_CONCAT( a2.ladditional_value ) FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d ', 'accommodation_continent', $post_data[ 'accommodation_continent' ] );
        }

        if( empty( $w ) === false )
        {
            $where = ' AND ' . implode( ' AND ', $w );

            $q = 'SELECT * FROM lumonata_post AS a WHERE a.ltype = "accommodation" AND a.llang_id = ' . $lang . ' AND a.lstatus = 1 AND a.lpost_id <> ' . $post_data[ 'lpost_id' ] . $where . ' ORDER BY rand()';
            $r = parent::query( $q );

            if( !is_array( $r ) )
            {
                if( parent::num_rows( $r ) > 0 )
                {
                    $data = array();

                    while( $d = parent::fetch_assoc( $r ) )
                    {
                        //-- GET additional field
                        $sa = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lapp_id = %d AND a.lmodule_id = ( SELECT a2.lmodule_id FROM lumonata_module AS a2 WHERE a2.lapps = %s )';
                        $qa = parent::prepare_query( $sa, $d[ 'lpost_id' ], $d[ 'ltype' ] );
                        $ra = parent::query( $qa );

                        if( parent::num_rows( $ra ) > 0 )
                        {
                            while( $da = parent::fetch_assoc( $ra ) )
                            {
                                $d[ $da[ 'ladditional_key' ] ] = $da[ 'ladditional_value' ];
                            }
                        }

                        //-- GET term field
                        $st = 'SELECT b.* FROM lumonata_post_relationship AS a LEFT JOIN lumonata_post_terms AS b ON a.lterm_id = b.lterm_id WHERE a.lpost_id = %d AND b.lgroup = %s ORDER BY b.lname';
                        $qt = parent::prepare_query( $st, $d[ 'lpost_id' ], $d[ 'ltype' ] );
                        $rt = parent::query( $qt );

                        if( parent::num_rows( $rt ) > 0 )
                        {
                            while( $dt = parent::fetch_assoc( $rt ) )
                            {
                                //-- GET term additional field
                                $sf = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lterm_id = %d';
                                $qf = parent::prepare_query( $sf, $dt[ 'lterm_id' ] );
                                $rf = parent::query( $qf );

                                if( parent::num_rows( $rf ) > 0 )
                                {
                                    while( $df = parent::fetch_assoc( $rf ) )
                                    {
                                        $dt[ $df[ 'ladditional_key' ] ] = $df[ 'ladditional_value' ];
                                    }
                                }

                                $d[ $dt[ 'lrule' ] ][] = $dt;
                            }
                        }

                        $data[] = $d;
                    }

                    $dta = $this->get_field_value( 'lumonata_post', array( 'ltitle' ), array( 'lpost_id' => $dest ) );

                    return array( $dta[ 'ltitle' ] => $data );
                }
            }
        }
    }

    public function get_trending_post( $post_data, $lang = 0 )
    {
        $w = array();

        if( isset( $post_data[ 'destination_type' ] ) && $post_data[ 'destination_type' ] == 0 )
        {
            $w[] = parent::prepare_query( '( SELECT GROUP_CONCAT( a2.ladditional_value ) FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d ', 'accommodation_continent', $post_data[ 'lpost_id' ] );
        }
        elseif( isset( $post_data[ 'destination_type' ] ) && $post_data[ 'destination_type' ] == 1 )
        {
            $w[] = parent::prepare_query( '( SELECT GROUP_CONCAT( a2.ladditional_value ) FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d ', 'accommodation_country', $post_data[ 'lpost_id' ] );
        }
        elseif( isset( $post_data[ 'destination_type' ] ) && $post_data[ 'destination_type' ] == 2 )
        {
            $w[] = parent::prepare_query( '( SELECT GROUP_CONCAT( a2.ladditional_value ) FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d ', 'accommodation_state', $post_data[ 'lpost_id' ] );
        }
        elseif( isset( $post_data[ 'destination_type' ] ) && $post_data[ 'destination_type' ] == 3 )
        {
            $w[] = parent::prepare_query( '( SELECT GROUP_CONCAT( a2.ladditional_value ) FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d ', 'accommodation_city', $post_data[ 'lpost_id' ] );
        }

        if( empty( $w ) )
        {
            $where = '';
        }
        else
        {
            $where = ' AND ' . implode( ' AND ', $w );
        }

        $q = 'SELECT * FROM lumonata_post AS a WHERE a.ltype = "accommodation" AND a.llang_id = ' . $lang . ' AND a.lstatus = 1' . $where . ' ORDER BY rand()';
        $r = parent::query( $q );

        if( !is_array( $r ) )
        {
            if( parent::num_rows( $r ) > 0 )
            {
                $data = array();

                while( $d = parent::fetch_assoc( $r ) )
                {
                    //-- GET additional field
                    $sa = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lapp_id = %d AND a.lmodule_id = ( SELECT a2.lmodule_id FROM lumonata_module AS a2 WHERE a2.lapps = %s )';
                    $qa = parent::prepare_query( $sa, $d[ 'lpost_id' ], $d[ 'ltype' ] );
                    $ra = parent::query( $qa );

                    if( parent::num_rows( $ra ) > 0 )
                    {
                        while( $da = parent::fetch_assoc( $ra ) )
                        {
                            $d[ $da[ 'ladditional_key' ] ] = $da[ 'ladditional_value' ];
                        }
                    }

                    //-- GET term field
                    $st = 'SELECT b.* FROM lumonata_post_relationship AS a LEFT JOIN lumonata_post_terms AS b ON a.lterm_id = b.lterm_id WHERE a.lpost_id = %d AND b.lgroup = %s ORDER BY b.lname';
                    $qt = parent::prepare_query( $st, $d[ 'lpost_id' ], $d[ 'ltype' ] );
                    $rt = parent::query( $qt );

                    if( parent::num_rows( $rt ) > 0 )
                    {
                        while( $dt = parent::fetch_assoc( $rt ) )
                        {
                            //-- GET term additional field
                            $sf = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lterm_id = %d';
                            $qf = parent::prepare_query( $sf, $dt[ 'lterm_id' ] );
                            $rf = parent::query( $qf );

                            if( parent::num_rows( $rf ) > 0 )
                            {
                                while( $df = parent::fetch_assoc( $rf ) )
                                {
                                    $dt[ $df[ 'ladditional_key' ] ] = $df[ 'ladditional_value' ];
                                }
                            }

                            $d[ $dt[ 'lrule' ] ][] = $dt;
                        }
                    }

                    $data[] = $d;
                }

                return $data;
            }
        }
    }

    public function get_rate_price( $post_data, $checkin = '', $checkout = '' )
    {
        $s = 'SELECT 
    			b.lstart_period, 
    			b.lend_period, 
    			a.lrate_price,
    			a.lrate_id
    		  FROM lumonata_rates AS a 
    		  LEFT JOIN lumonata_rate_periods AS b ON b.lrate_id = a.lrate_id
    		  WHERE a.lpost_id = %d';
        $q = parent::prepare_query( $s, $post_data[ 'lpost_id' ] );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $rates = array();

            while( $d = parent::fetch_assoc( $r ) )
            {
                $start = date( 'Y-m-d', strtotime( $d[ 'lstart_period' ] ) );
                $end   = date( 'Y-m-d', strtotime( $d[ 'lend_period' ] ) );

                if( empty( $checkin ) && empty( $checkout ) )
                {
                    $dstart = date( 'Y-m-d' );
                    $dend   = date( 'Y-m-d' );
                }
                else
                {
                    $dstart = date( 'Y-m-d', strtotime( $checkin ) );
                    $dend   = date( 'Y-m-d', strtotime( $checkout ) );
                }

                if( $dstart >= $start && $dend <= $end )
                {
                    $rates[ 'price' ] = $d[ 'lrate_price' ];

                    //-- GET detail rate
                    $s2 = 'SELECT a.lterm_id, a.lrate_price FROM lumonata_rate_details AS a WHERE a.lrate_id = %d';
                    $q2 = parent::prepare_query( $s2, $d[ 'lrate_id' ] );
                    $r2 = parent::query( $q2 );

                    if( parent::num_rows( $r2 ) > 0 )
                    {
                        while( $d2 = parent::fetch_assoc( $r2 ) )
                        {
                            $rates[ 'rooms_price' ][ $d2[ 'lterm_id' ] ] = $d2[ 'lrate_price' ];
                        }
                    }

                    break;
                }
            }

            return $rates;
        }
    }

    public function get_start_price( $post_data )
    {
        if( $post_data[ 'page_template' ] == '3' )
        {
            $rates = $this->get_rate_price( $post_data );

            if( empty( $rates ) )
            {
                //-- Use default price
                if( empty( $post_data[ 'room_price' ] ) )
                {
                    return 0;
                }
                else
                {
                    return floatval( $post_data[ 'room_price' ] );
                }
            }
            else
            {
                //-- Use rate price
                if( empty( $rates[ 'price' ] ) )
                {
                    return 0;
                }
                else
                {
                    return floatval( $rates[ 'price' ] );
                }
            }
        }
        else
        {
            if( isset( $post_data[ 'room_type' ] ) && !empty( $post_data[ 'room_type' ] ) )
            {
                $stack = array();

                foreach( $post_data[ 'room_type' ] as $d )
                {
                    $stack[] = $this->room_price( $post_data, $d );
                }

                return min( $stack );
            }
            else
            {
                $rates = $this->get_rate_price( $post_data );

                if( empty( $rates ) )
                {
                    //-- Use default price
                    if( empty( $post_data[ 'room_price' ] ) )
                    {
                        return 0;
                    }
                    else
                    {
                        return floatval( $post_data[ 'room_price' ] );
                    }
                }
                else
                {
                    //-- Use rate price
                    if( empty( $rates[ 'price' ] ) )
                    {
                        return 0;
                    }
                    else
                    {
                        return floatval( $rates[ 'price' ] );
                    }
                }
            }
        }
    }

    public function get_package_start_price( $data, $all = false )
    {
        if( $all === true )
        {
            $stack = array();

            if( empty( $data ) )
            {
                return 0;
            }
            else
            {
                foreach( $data as $dt )
                {
                    $stack[] = $this->get_package_start_price( $dt );
                }

                return min( $stack );
            }
        }
        else
        {
            if( isset( $data[ 'lprice' ] ) && !empty( $data[ 'lprice' ] ) )
            {
                return $data[ 'lprice' ];
            }
            else
            {
                return 0;
            }
        }
    }

    public function get_schedule_start_price( $data, $all = false )
    {
        if( $all === true )
        {
            $stack = array();

            foreach( $data as $dt )
            {
                $stack[] = $this->get_schedule_start_price( $dt );
            }

            return min( $stack );
        }
        else
        {
            if( isset( $data[ 'lprice' ] ) && !empty( $data[ 'lprice' ] ) )
            {
                return $data[ 'lprice' ];
            }
            else
            {
                return 0;
            }
        }
    }

    public function is_expired_package( $data )
    {
        if( isset( $data[ 'ltype' ] ) && $data[ 'ltype' ] == '1' )
        {
            if( $data[ 'ldate_end' ] != '' && strtotime( $data[ 'ldate_end' ] ) < time() )
            {
                return true;
            }
        }

        return false;
    }

    public function is_expired_schedule( $data )
    {
        if( $data[ 'lcheck_out' ] != '' && strtotime( $data[ 'lcheck_out' ] ) < time() )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function get_schedule_price( $post_data, $dt )
    {
        $price    = $this->get_start_price( $post_data );
        $checkin  = new DateTime( date( 'Y-m-d', $dt[ 'lcheck_in' ] ) );
        $checkout = new DateTime( date( 'Y-m-d', $dt[ 'lcheck_out' ] ) );

        $nights = $checkout->diff( $checkin )->format( '%a' );
        $nights = $nights == 0 ? 1 : $nights;
        $price  = $price * $nights;

        return $price;
    }

    public function room_price( $acco, $room, $checkin = '', $checkout = '' )
    {
        $rates = $this->get_rate_price( $acco, $checkin, $checkout );

        if( empty( $rates ) )
        {
            //-- Use default room price
            if( isset( $room[ 'room_price' ] ) && !empty( $room[ 'room_price' ] ) )
            {
                $rate = $room[ 'room_price' ];
            }
            else
            {
                $rate = 0;
            }
        }
        else
        {
            //-- Use rate room price
            $rid = $room[ 'lterm_id' ];

            if( isset( $rates[ 'rooms_price' ][ $rid ] ) && !empty( $rates[ 'rooms_price' ][ $rid ] ) )
            {
                $rate = $rates[ 'rooms_price' ][ $rid ];
            }
            elseif( isset( $room[ 'room_price' ] ) && !empty( $room[ 'room_price' ] ) )
            {
                $rate = $room[ 'room_price' ];
            }
            else
            {
                $rate = 0;
            }
        }

        if( isset( $acco[ 'policy' ] ) && !empty( $acco[ 'policy' ] ) )
        {
            $policy = $acco[ 'policy' ];
        }
        else
        {
            $policy = '0';
        }

        if( $policy == '0' )
        {
            $crate = 0;

            if( $checkin != '' && $checkout != '' )
            {
                $s = 'SELECT a.lrate FROM lumonata_calendar as a WHERE a.lpost_id = %d AND a.lterm_id = %d AND a.ldate BETWEEN %s AND %s';
                $q = parent::prepare_query( $s, $acco[ 'lpost_id' ], $room[ 'lterm_id' ], $checkin, $checkout );
                $r = parent::query( $q );

                if( parent::num_rows( $r ) > 0 )
                {
                    $arate = array();

                    while( $d = parent::fetch_array( $r ) )
                    {
                        $arate[] = $d[ 'lrate' ];
                    }

                    $crate = max( $arate );
                }
            }
            else
            {
                $crate = $this->get_field_value( 'lumonata_calendar', 'lrate', array(
                    'lpost_id' => $acco[ 'lpost_id' ],
                    'lterm_id' => $room[ 'lterm_id' ],
                    'ldate'    => date( 'Y-m-d' ),
                ) );
            }

            if( empty( $crate ) === false )
            {
                $rate = $crate;
            }
        }

        return $rate;
    }

    public function get_home_url()
    {
        $lang = $this->get_current_language_id();

        if( $lang == $this->get_setting_value( 'llanguage' ) )
        {
            return sprintf( '%s%s/', HT_SERVER, SITE_URL );
        }
        else
        {
            $code = $this->get_field_value( 'lumonata_language', 'llang_code', array( 'llang_id' => $lang ) );

            return sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $code );
        }
    }

    public function get_currency_sign( $id = '' )
    {
        if( empty( $id ) )
        {
            $currency = $this->get_setting_value( 'lcurrency' );
        }
        else
        {
            $currency = $this->get_field_value( 'lumonata_additional_field', 'ladditional_value', array( 'ladditional_key' => 'currency', 'lapp_id' => $id ) );

            if( empty( $currency ) )
            {
                $currency = $this->get_setting_value( 'lcurrency' );
            }
        }

        $display = $this->get_setting_value( 'lcurrency_display' );

        if( $currency != '' && $display != '' )
        {
            if( $display == '0' )
            {
                return $this->get_field_value( 'lumonata_currency', 'lsymbol', array( 'lcurrency_id' => $currency ) );
            }
            else
            {
                return  $this->get_field_value( 'lumonata_currency', 'lcode', array( 'lcurrency_id' => $currency ) );
            }
        }
    }

    public function get_currency_code( $id = '' )
    {
        $currency = $this->get_setting_value( 'lcurrency' );

        if( empty( $id ) )
        {
            $currency = $this->get_setting_value( 'lcurrency' );
        }
        else
        {
            $currency = $this->get_field_value( 'lumonata_additional_field', 'ladditional_value', array( 'ladditional_key' => 'currency', 'lapp_id' => $id ) );
        }

        if( $currency != '' )
        {
            return $this->get_field_value( 'lumonata_currency', 'lcode', array( 'lcurrency_id' => $currency ) );
        }
    }

    public function get_currency_id( $id = '' )
    {
        $currency = $this->get_setting_value( 'lcurrency' );

        if( empty( $id ) )
        {
            $currency = $this->get_setting_value( 'lcurrency' );
        }
        else
        {
            $currency = $this->get_field_value( 'lumonata_additional_field', 'ladditional_value', array( 'ladditional_key' => 'currency', 'lapp_id' => $id ) );
        }

        return $currency;
    }

    public function get_format_date_range( $start, $end )
    {
        $syear = date( 'Y', $start );
        $eyear = date( 'Y', $end );

        if( $syear == $eyear )
        {
            return sprintf( '%s - %s', date( 'M d', $start ), date( 'M d, Y', $end ) );
        }
        else
        {
            return sprintf( '%s - %s', date( 'M d, Y', $start ), date( 'M d, Y', $end ) );
        }
    }

    public function get_format_nights( $nights )
    {
        if( $nights > 1 )
        {
            return sprintf( '%s nights', $nights );
        }
        else
        {
            return sprintf( '%s night', $nights );
        }
    }

    public function get_format_price( $price = 0, $id = null )
    {
        // echo $price;
        // exit;
        $sign  = $this->get_currency_sign( $id );
        $price = number_format( $price, 0, ',', '.' );

        return sprintf( '%s%s', $sign, $price );
    }

    public function get_video_source( $lattach_id = '' )
    {
        $a = $this->get_field_value( 'lumonata_attachment', array( 'lattach', 'lmimetype' ), array( 'lattach_id' => $lattach_id ) );

        if( !empty( $a[ 'lattach' ] ) && file_exists( IMAGE_DIR . '/Uploads/' . $a[ 'lattach' ] ) )
        {
            return '<source src="' . HT_SERVER . SITE_URL . '/images/Uploads/' . $a[ 'lattach' ] . '" type="' . $a[ 'lmimetype' ] . '">';
        }
    }

    public function get_quality_star( $value = 0 )
    {
        return sprintf( '<img src="%s/assets/img/stars-%s.svg" alt="" />', HT_SERVER . THEME_URL, $value );
    }

    public function get_attachment_url( $lattach_id = '', $width = null, $height = null )
    {
        $attach = $this->get_field_value( 'lumonata_attachment', 'lattach', array( 'lattach_id' => $lattach_id ) );
        $param  = array();

        if( $width !== null && $width > 0 )
        {
            $param[] = 'w=' . $width;
        }

        if( $height !== null && $height > 0 )
        {
            $param[] = 'h=' . $height;
        }

        if( !empty( $attach ) && file_exists( IMAGE_DIR . '/Uploads/' . $attach ) )
        {
            if( empty( $param ) )
            {
                return HT_SERVER . SITE_URL . '/images/Uploads/' . $attach;
            }
            else
            {
                return HT_SERVER . ADMIN_URL . '/functions/mthumb.php?' . implode( '&', $param ) . '&src=' . HT_SERVER . SITE_URL . '/images/Uploads/' . $attach;
            }
        }
        else
        {
            if( $width === null )
            {
                $width = $height;
            }

            if( $height === null )
            {
                $height = $width;
            }

            return 'https://via.placeholder.com/' . $width . 'x' . $height . '/666/666';
        }
    }

    public function get_agent_menu( $sef = 'dashboard' )
    {
        $menu = array(
            'dashboard'       => array( 'title' => 'DASHBOARD', 'permalink' => '//' . SITE_URL . '/agent/dashboard' ),
            'booking-request' => array( 'title' => 'BOOKING REQUEST', 'permalink' => '//' . SITE_URL . '/agent/booking-request' ),
            'reservations'    => array( 'title' => 'RESERVATIONS', 'permalink' => '//' . SITE_URL . '/agent/reservations' ),
            'listings'        => array( 'title' => 'LISTINGS', 'permalink' => '//' . SITE_URL . '/agent/listings' ),
            'calendar'        => array( 'title' => 'CALENDAR', 'permalink' => '//' . SITE_URL . '/agent/calendar' ),
            'help'            => array( 'title' => 'HELP', 'permalink' => '//' . SITE_URL . '/agent/help' ),
        );

        $result = '';

        foreach( $menu as $key => $d )
        {
            if( $sef == $key )
            {
                $class = ' class="actv"';
            }
            else
            {
                $class = '';
            }

            $result .= sprintf( '<a href="%s"%s>%s</a>', $d[ 'permalink' ], $class, $d[ 'title' ] );
        }

        return $result;
    }

    public function get_user_avatar( $userid = '', $width = null, $height = null )
    {
        $param = array();

        if( is_array( $userid ) )
        {
            $attach = $this->get_field_value( 'lumonata_user', 'limage', $userid );
        }
        else
        {
            $attach = $this->get_field_value( 'lumonata_user', 'limage', array( 'luser_id' => $userid ) );
        }

        if( $width !== null && $width > 0 )
        {
            $param[] = 'w=' . $width;
        }

        if( $height !== null && $height > 0 )
        {
            $param[] = 'h=' . $height;
        }

        if( !empty( $attach ) && file_exists( IMAGE_DIR . '/Users/' . $attach ) )
        {
            if( empty( $param ) )
            {
                return HT_SERVER . SITE_URL . '/images/Users/' . $attach;
            }
            else
            {
                return HT_SERVER . ADMIN_URL . '/functions/mthumb.php?' . implode( '&', $param ) . '&src=' . HT_SERVER . SITE_URL . '/images/Users/' . $attach;
            }
        }
        else
        {
            $name = $this->get_field_value( 'lumonata_user', 'lname', array( 'luser_id' => $userid ) );

            return 'https://ui-avatars.com/api/?name=' . $name . 'background=random';
        }
    }

    public function get_review_image( $data = array(), $width = null, $height = null )
    {
        if( empty( $data[ 'luser_id' ] ) )
        {
            return 'https://ui-avatars.com/api/?name=' . $data[ 'lname' ] . 'background=random';
        }
        else
        {
            return $this->get_user_avatar( $data[ 'luser_id' ] );
        }
    }

    public function get_listing_title( $data = array() )
    {
        $title = array();

        if( isset( $data[ 'title_part_1' ] ) && !empty( $data[ 'title_part_1' ] ) )
        {
            $title[] = $data[ 'title_part_1' ];
        }

        if( isset( $data[ 'title_part_2' ] ) && !empty( $data[ 'title_part_2' ] ) )
        {
            $title[] = $data[ 'title_part_2' ];
        }

        if( isset( $data[ 'title_part_3' ] ) && !empty( $data[ 'title_part_3' ] ) )
        {
            $title[] = $data[ 'title_part_3' ];
        }

        if( isset( $data[ 'title_part_4' ] ) && !empty( $data[ 'title_part_4' ] ) )
        {
            $title[] = $data[ 'title_part_4' ];
        }

        if( empty( $title ) )
        {
            return $data[ 'ltitle' ];
        }
        else
        {
            return implode( ' ', $title );
        }
    }

    public function get_brief_text( $str = '', $n = 500, $end_char = '&#8230;' )
    {
        if( strlen( $str ) < $n )
        {
            return strip_tags( $str );
        }

        $str = preg_replace( "/\s+/", ' ', str_replace( array( "\r\n", "\r", "\n" ), ' ', $str ) );

        if ( strlen( $str ) <= $n )
        {
            return strip_tags( $str );
        }

        $out = '';

        foreach( explode( ' ', trim( $str ) ) as $val )
        {
            $out .= $val.' ';

            if( strlen( $out ) >= $n )
            {
                $out = trim( $out );

                return strip_tags( ( strlen( $out ) == strlen( $str ) ) ? $out : $out . $end_char );
            }
        }
    }

    public function get_locations( $data = array() )
    {
        $include  = array( 'accommodation_state', 'accommodation_country' );
        $location = array();

        foreach( $include as $dt )
        {
            if( isset( $data[ $dt ] ) && !empty( $data[ $dt ] ) )
            {
                $dest = $this->get_field_value( 'lumonata_post', array( 'ltitle', 'lsef_url' ), array( 'lpost_id' => $data[ $dt ] ) );

                if( empty( $dest ) === false )
                {
                    $location[] = $dest[ 'ltitle' ];
                }
            }
        }

        if( !empty( $location ) )
        {
            return implode( ',&nbsp;', $location );
        }
    }

    public function get_hightlight( $data )
    {
        $content = '';

        if( isset( $data[ 'hightlight' ] ) && !empty( $data[ 'hightlight' ] ) )
        {
            $dth = json_decode( $data[ 'hightlight' ], true );

            if( $dth !== null && json_last_error() === JSON_ERROR_NONE )
            {
                $con = '';
                $num = 4;

                foreach( $dth as $h )
                {
                    if( empty( $h[ 'name' ] ) === false )
                    {
                        if( $num == 0 )
                        {
                            break;
                        }

                        $con .= '
	                    <span class="ic2 highlight" title="' . $h[ 'name' ] . '">
	                        ' . $h[ 'name' ] . '
	                    </span>';

                        $num--;
                    }
                }

                if( $con != '' )
                {
                    $content .= '
	                <div class="adt-mo flex">
	                	' . $con . '
	                </div>';
                }
            }
        }

        return $content;
    }

    public function get_format_phone_number( $number = '', $type = '' )
    {
        if( $number != '' )
        {
            $lib = \libphonenumber\PhoneNumberUtil::getInstance();

            if( empty( $type ) )
            {
                return $lib->format( $lib->parse( $number ), \libphonenumber\PhoneNumberFormat::INTERNATIONAL );
            }
            elseif( $type == 'tel' )
            {
                return $lib->format( $lib->parse( $number ), \libphonenumber\PhoneNumberFormat::RFC3966 );
            }
            elseif( $type == 'wa' )
            {
                $number = $lib->format( $lib->parse( $number ), \libphonenumber\PhoneNumberFormat::INTERNATIONAL );
                $number = str_replace( ' ', '', $number );
                $number = str_replace( '+', '', $number );

                return sprintf( 'https://api.whatsapp.com/send?phone=%s', $number );
            }
            else
            {
                return $number;
            }
        }
    }

    public function get_format_skype_link( $username = '', $action = 'call' )
    {
        //-- Action list, default "call"
        //-- call, chat, userinfo, sendfile, add, voicemail

        if( $username != '' )
        {
            return sprintf( 'skype:%s?%s', $username, $action );
        }
    }

    public function set_page_views( $lpost_id )
    {
        $param = array(
            'lvisitor_ip' => $_SERVER[ 'REMOTE_ADDR' ],
            'lpvdate'     => date( 'Y-m-d' ),
            'lpost_id'    => $lpost_id
        );

        $rows = $this->get_num_rows( 'lumonata_page_view', $param );

        if( $rows == 0 )
        {
            $r = parent::insert( 'lumonata_page_view', $param );

            if( is_array( $r ) )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    public function do_shortcode( $content )
    {
        if( false === strpos( $content, '[' ) )
        {
            return $content;
        }

        $pattern = $this->get_shortcode_regex();

        return preg_replace_callback( "/$pattern/s", array( $this, 'do_shortcode_tag' ), $content );
    }

    public function get_shortcode_regex( $tagnames = null )
    {
        $tagnames  = array_keys( array( 'tnc ' => '' ) );
        $tagregexp = join( '|', array_map( 'preg_quote', $tagnames ) );

        return
              '\\['                              // Opening bracket
            . '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
            . "($tagregexp)"                     // 2: Shortcode name
            . '\\b'                              // Word boundary
            . '('                                // 3: Unroll the loop: Inside the opening shortcode tag
            .     '[^\\]\\/]*'                   // Not a closing bracket or forward slash
            .     '(?:'
            .         '\\/(?!\\])'               // A forward slash not followed by a closing bracket
            .         '[^\\]\\/]*'               // Not a closing bracket or forward slash
            .     ')*?'
            . ')'
            . '(?:'
            .     '(\\/)'                        // 4: Self closing tag ...
            .     '\\]'                          // ... and closing bracket
            . '|'
            .     '\\]'                          // Closing bracket
            .     '(?:'
            .         '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
            .             '[^\\[]*+'             // Not an opening bracket
            .             '(?:'
            .                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
            .                 '[^\\[]*+'         // Not an opening bracket
            .             ')*+'
            .         ')'
            .         '\\[\\/\\2\\]'             // Closing shortcode tag
            .     ')?'
            . ')'
            . '(\\]?)';                          // 6: Optional second closing brocket for escaping shortcodes: [[tag]]
    }

    public function do_shortcode_tag( $m )
    {
        // allow [[foo]] syntax for escaping a tag
        if ( $m[1] == '[' && $m[6] == ']' )
        {
            return substr( $m[0], 1, -1 );
        }

        $tag  = $m[2];
        $attr = $this->shortcode_parse_atts( $m[3] );

        if ( isset( $m[5] ) && !empty( $m[5] ) )
        {
            // enclosing tag - extra parameter
            return $m[1] . call_user_func( array( $this, 'shortcode_callback' ), $attr, $m[5], $tag ) . $m[6];
        }
        else
        {
            // self-closing tag
            return $m[1] . call_user_func( array( $this, 'shortcode_callback' ), $attr, null, $tag ) . $m[6];
        }
    }

    public function shortcode_callback( $attr, $extra, $tag )
    {
        if( trim( $tag ) == 'tnc' )
        {
            if( isset( $attr[ 'title' ] ) && isset( $attr[ 'content' ] ) )
            {
                return '
				<div class="ei-fdo flex">
					<h3>' . $attr[ 'title' ] . '</h3>
					<div class="ei-caption">
						<p>' . $attr[ 'content' ] . '</p>
					</div>
				</div>';
            }
        }
    }

    public function shortcode_parse_atts( $text )
    {
        $atts    = array();
        $pattern = '/(\w+)\s*=\s*"([^"]*)"(?:\s|$)|(\w+)\s*=\s*\'([^\']*)\'(?:\s|$)|(\w+)\s*=\s*([^\s\'"]+)(?:\s|$)|"([^"]*)"(?:\s|$)|(\S+)(?:\s|$)/';
        $text    = preg_replace( "/[\x{00a0}\x{200b}]+/u", " ", $text );
        if ( preg_match_all( $pattern, $text, $match, PREG_SET_ORDER ) )
        {
            foreach ( $match as $m )
            {
                if ( !empty( $m[1] ) )
                {
                    $atts[strtolower( $m[1] )] = stripcslashes( $m[2] );
                }
                elseif ( !empty( $m[3] ) )
                {
                    $atts[strtolower( $m[3] )] = stripcslashes( $m[4] );
                }
                elseif ( !empty( $m[5] ) )
                {
                    $atts[strtolower( $m[5] )] = stripcslashes( $m[6] );
                }
                elseif ( isset( $m[7] ) and strlen( $m[7] ) )
                {
                    $atts[] = stripcslashes( $m[7] );
                }
                elseif ( isset( $m[8] ) )
                {
                    $atts[] = stripcslashes( $m[8] );
                }
            }
        }
        else
        {
            $atts = ltrim( $text );
        }
        return $atts;
    }

    public function slugify( $uri )
    {
        $char = array(
            "\\",
            "/",
            ":",
            "*",
            "?",
            "<",
            ">",
            "`",
            "~",
            "!",
            "@",
            "#",
            "$",
            "%",
            "^",
            "&",
            "(",
            ")",
            "_",
            "+",
            "=",
            "|",
            "}",
            "{",
            "[",
            "]",
            ";",
            "\"",
            "'",
            ".",
            " "
        );

        $sef_url = $uri;

        for( $i = 0; $i < count( $char ); $i++ )
        {
            $sef_url = str_replace( $char[ $i ], '-', $sef_url );
            $sef_url = str_replace( '--', '-', $sef_url );
        }

        $sef_url = str_replace( '--', '-', $sef_url );
        $char2nd = array( ',' );

        for( $i = 0; $i < count( $char2nd ); $i++ )
        {
            $sef_url = str_replace( $char2nd[ $i ], '-', $sef_url );
        }

        $strlen = strlen( $sef_url );

        if( substr( $sef_url, -1 ) == '-' )
        {
            $sef_url = substr( $sef_url, 0, ( $strlen - 1 ) );
        }

        $strlen = strlen( $sef_url );

        if( substr( $sef_url, 0, 1 ) == '-' )
        {
            $sef_url = substr( $sef_url, 1, $strlen );
        }

        $sef_url = str_replace( 'ä', 'a', $sef_url );
        $sef_url = str_replace( 'Ä', 'A', $sef_url );
        $sef_url = str_replace( 'é', 'e', $sef_url );
        $sef_url = str_replace( 'ö', 'o', $sef_url );
        $sef_url = str_replace( 'Ö', 'O', $sef_url );
        $sef_url = str_replace( 'ü', 'u', $sef_url );
        $sef_url = str_replace( 'Ü', 'U', $sef_url );
        $sef_url = str_replace( 'ß', 'B', $sef_url );
        $sef_url = str_replace( 'ä', 'a', $sef_url );

        return strtolower( $sef_url );
    }

    public function archive_link( $type = '', $query = array() )
    {
        if( empty( $type ) === false )
        {
            $lang_id   = $this->get_current_language_id();
            $lang_code = $this->get_current_language_code();
            $lang_def  = $this->get_setting_value( 'llanguage' );

            if( $lang_id == $lang_def )
            {
                if( empty( $query ) )
                {
                    return sprintf( '%s%s/%s/', HT_SERVER, SITE_URL, $type );
                }
                else
                {
                    return sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $type, rtrim( implode( '/', $query ), '/' ) );
                }
            }
            else
            {
                if( empty( $query ) )
                {
                    return sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $lang_code, $type );
                }
                else
                {
                    return sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $lang_code, $type, rtrim( implode( '/', $query ), '/' ) );
                }
            }
        }
    }

    public function site_url( $uri = '' )
    {
        $ldef  = $this->get_setting_value( 'llanguage' );
        $lcode = $this->get_current_language_code();
        $lid   = $this->get_current_language_id();

        if( $lid == $ldef )
        {
            $lcode = '';
        }
        else
        {
            $lcode .= '/';
        }

        if( isset( $_SERVER[ 'HTTPS' ] ) && !empty( $_SERVER[ 'HTTPS' ] ) )
        {
            $http = 'https://';
        }
        else
        {
            $http = 'http://';
        }

        $part = explode( '.', $_SERVER[ 'HTTP_HOST' ] );

        if( isset( $part[ 0 ] ) && $part[ 0 ] == 'www' )
        {
            $surl = rtrim( $http . 'www.' . SITE_URL, '/' ) . '/';
        }
        else
        {
            $surl = rtrim( $http . SITE_URL, '/' ) . '/';
        }

        return $surl . $lcode . ltrim( $uri, '/' );
    }

    public function permalink( $data = array(), $query = array() )
    {
        $lang_id   = $this->get_current_language_id();
        $lang_code = $this->get_current_language_code();
        $lang_def  = $this->get_setting_value( 'llanguage' );

        $path = array( 'destination' => 'destination', 'accommodation' => 'accommodation', 'blog' => 'blogs' );

        if( isset( $path[ $data[ 'ltype' ] ] ) && !empty( $path[ $data[ 'ltype' ] ] ) )
        {
            if( $lang_id == $lang_def )
            {
                if( empty( $query ) )
                {
                    return sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $path[ $data[ 'ltype' ] ], $data[ 'lsef_url' ] );
                }
                else
                {
                    return sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $path[ $data[ 'ltype' ] ], $data[ 'lsef_url' ], implode( '/', $query ) );
                }
            }
            else
            {
                if( empty( $query ) )
                {
                    return sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $lang_code, $path[ $data[ 'ltype' ] ], $data[ 'lsef_url' ] );
                }
                else
                {
                    return sprintf( '%s%s/%s/%s/%s/%s/', HT_SERVER, SITE_URL, $lang_code, $path[ $data[ 'ltype' ] ], $data[ 'lsef_url' ], implode( '/', $query ) );
                }
            }
        }
    }

    public function term_link( $data = array() )
    {
        if( empty( $data ) === false )
        {
            $lang_id   = $this->get_current_language_id();
            $lang_code = $this->get_current_language_code();
            $lang_def  = $this->get_setting_value( 'llanguage' );

            $path = array( 'accommodation_type' => 'accommodation-type', 'surf_trip' => 'surf-trips', 'blog_category' => 'blogs/category', 'blog_tag' => 'blogs/tag' );

            if( isset( $path[ $data[ 'lrule' ] ] ) && !empty( $path[ $data[ 'lrule' ] ] ) )
            {
                if( $lang_id == $lang_def )
                {
                    return sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $path[ $data[ 'lrule' ] ], $data[ 'lsef' ] );
                }
                else
                {
                    return sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $lang_code, $path[ $data[ 'lrule' ] ], $data[ 'lsef' ] );
                }
            }
        }
    }

    public function build_tree( array $data, $id, $parent_id, $parent = 0 )
    {
        $tree = array();

        foreach( $data as $d )
        {
            if( $d[ $parent_id ] == $parent )
            {
                $children = $this->build_tree( $data, $id, $parent_id, $d[ $id ] );

                if( $children )
                {
                    $d[ 'child' ] = $children;
                }

                $tree[ $d[ $id ] ] = $d;
            }
        }

        return $tree;
    }

    public function get_menu_recursive_field( $query, $field_id, $field_parent )
    {
        $r = parent::query( $query );

        if( parent::num_rows( $r ) == 0 )
        {
            return array();
        }

        $data = array();

        while( $d = parent::fetch_assoc( $r ) )
        {
            $data[] = $d;
        }

        return $this->build_tree( $data, $field_id, $field_parent );
    }

    public function get_sef_url( $table, $field_sef = '', $string = '' )
    {
        $suffix = 1;

        do
        {
            $sef_url = $this->slugify( $string );

            if( $suffix > 1 )
            {
                $sef_url .= '-' . $suffix;
            }

            $sef_check = $this->get_num_rows( $table, array( $field_sef => $sef_url ) );

            $suffix++;
        }
        while( $sef_check );

        return $sef_url;
    }

    public function get_css( $url = '', $data = array(), $media = 'screen' )
    {
        if( empty( $url ) === false )
        {
            if( empty( $data ) )
            {
                return sprintf( '<link rel="stylesheet" href="%s" media="%s" />', $url, $media );
            }
            else
            {
                return sprintf( '<link rel="stylesheet" href="%s" media="%s" %s />', $url, $media, implode( ', ', array_map( function( $v, $k ) { return $k . '="' . $v . '"'; }, $data, array_keys( $data ) ) ) );
            }
        }
    }

    public function get_jvs( $url = '', $data = array() )
    {
        if( empty( $url ) === false )
        {
            if( empty( $data ) )
            {
                return sprintf( '<script src="%s"></script>', $url );
            }
            else
            {
                return sprintf( '<script src="%s" %s></script>', $url, http_build_query( $data, null, ', ' ) );
            }
        }
    }

    public function get_accommodation_rating( $lpost_id, $lref_id = null )
    {
        $s = 'SELECT * FROM lumonata_reviews AS a WHERE a.lpost_id = %d ORDER BY a.lcreated_date DESC';
        $q = parent::prepare_query( $s, $lpost_id );
        $r = parent::query( $q );
        $n = parent::num_rows( $r );

        if( $n <= 0 )
        {
            $s = 'SELECT * FROM lumonata_reviews AS a WHERE a.lpost_id = %d ORDER BY a.lcreated_date DESC';
            $q = parent::prepare_query( $s, $lref_id );
            $r = parent::query( $q );
            $n = parent::num_rows( $r );
        }

        if( $n > 0 )
        {
            $arate = array();
            $brate = array();
            $crate = array();
            $drate = array();
            $erate = array();
            $frate = array();

            while( $d = parent::fetch_array( $r ) )
            {
                $arate[] = isset( $d[ 'laccuracy_score' ] ) ? intval( $d[ 'laccuracy_score' ] ) : 0;
                $brate[] = isset( $d[ 'llocation_score' ] ) ? intval( $d[ 'llocation_score' ] ) : 0;
                $crate[] = isset( $d[ 'lactivities_score' ] ) ? intval( $d[ 'lactivities_score' ] ) : 0;
                $drate[] = isset( $d[ 'lcleanliness_score' ] ) ? intval( $d[ 'lcleanliness_score' ] ) : 0;
                $erate[] = isset( $d[ 'lstaff_score' ] ) ? intval( $d[ 'lstaff_score' ] ) : 0;
                $frate[] = isset( $d[ 'loverall_score' ] ) ? intval( $d[ 'loverall_score' ] ) : 0;
            }

            $result[ 'cleanliness_rate' ] = empty( $drate ) ? 0 : ceil( array_sum( $drate ) / count( $drate ) );
            $result[ 'activities_rate' ]  = empty( $crate ) ? 0 : ceil( array_sum( $crate ) / count( $crate ) );
            $result[ 'accuracy_rate' ]    = empty( $arate ) ? 0 : ceil( array_sum( $arate ) / count( $arate ) );
            $result[ 'location_rate' ]    = empty( $brate ) ? 0 : ceil( array_sum( $brate ) / count( $brate ) );
            $result[ 'overall_rate' ]     = empty( $frate ) ? 0 : ceil( array_sum( $frate ) / count( $frate ) );
            $result[ 'staff_rate' ]       = empty( $erate ) ? 0 : ceil( array_sum( $erate ) / count( $erate ) );

            $result[ 'rating' ]  = ceil( array_sum( $result ) / count( $result ) );
            $result[ 'reviews' ] = $n;
        }
        else
        {
            $result[ 'cleanliness_rate' ] = 0;
            $result[ 'activities_rate' ]  = 0;
            $result[ 'accuracy_rate' ]    = 0;
            $result[ 'location_rate' ]    = 0;
            $result[ 'overall_rate' ]     = 0;
            $result[ 'staff_rate' ]       = 0;

            $result[ 'rating' ]  = 0;
            $result[ 'reviews' ] = 0;
        }

        return $result;
    }

    public function get_accommodation_rating_label( $data )
    {
        $label = array(
            0 => $this->translate( 'not-rated', 'Not Rated' ),
            1 => $this->translate( 'bad', 'Bad' ),
            2 => $this->translate( 'kinda-bad', 'Kinda Bad' ),
            3 => $this->translate( 'good', 'Good' ),
            4 => $this->translate( 'pretty-good', 'Pretty Good' ),
            5 => $this->translate( 'excellent', 'Excellent' )
        );

        if( isset( $data[ 'rating' ] ) )
        {
            return strtoupper( $label[ $data[ 'rating' ] ] );
        }
        else
        {
            return '';
        }
    }

    public function is_agent_login()
    {
        $sess = $this->get_agent_session();

        if( $sess[ 'usertype' ] == 2 && empty( $sess[ 'username' ] ) === false )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function is_agent_listing( $where = array() )
    {
        if( empty( $where ) )
        {
            $dta = $this->get_field_value( 'lumonata_user', '*' );
        }
        else
        {
            $dta = $this->get_field_value( 'lumonata_user', '*', $where );
        }

        if( isset( $dta[ 'lusertype_id' ] ) && $dta[ 'lusertype_id' ] == 2 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function is_member_login()
    {
        $sess = $this->get_member_session();

        if( $sess[ 'usertype' ] == 4 && empty( $sess[ 'username' ] ) === false )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function is_translation( $lang = '' )
    {
        $s = 'SELECT * FROM lumonata_language AS a WHERE a.lstatus = 1 AND a.llang_code = %s';
        $q = parent::prepare_query( $s, $lang );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
