<?php

function tab_start( $atts, $content = null )
{
    global $reset_firsttab_flag;

    static $firsttab = true;

    if ( $GLOBALS["reset_firsttab_flag"] === true )
    {
        $firsttab = true;

        $GLOBALS["reset_firsttab_flag"] = false;
    }

    $args = shortcode_atts( array(
        'title'    => '',
        'open'     => '',
        'icon'     => '',
        'ico'      => '',
        'class'    => '',
        'required' => false,
    ), $atts );


    $tabtarget = sanitize_title_with_dashes( remove_accents( wp_kses_decode_entities( $args['title'] ) ) );
    $class     = sanitize_html_class( $args['class'] );

    //* initialise urltarget
    $urltarget = '';

    //* grab the value of the 'target' url parameter if there is one
    if ( isset( $_REQUEST['target'] ) )
    {
        $urltarget = sanitize_title_with_dashes( $_REQUEST['target'] );
    }

    //* Set Tab Panel Class - add active class if the open attribute is set or the target url parameter matches the dashed version of the tab title
    $tabcontentclass = "tabcontent";

    if ( $class != '' )
    {
        $class = ' ' . $class;
        $tabcontentclass .= " " . $class . "-content";
    }

    if ( ( ( $args['open'] ) && ( $urltarget == '' ) ) || ( isset( $urltarget ) && ( $urltarget == $tabtarget ) ) )
    {
        $tabcontentclass .= " responsive-tabs__panel--active";
    }

    //* test whether this is the first tab in the group
    if ( $firsttab )
    {

        //* Set flag so we know subsequent tabs are not the first in the tab group
        $firsttab = false;

        //* Build output if we are making the first tab
        return '<div class="responsive-tabs">' . "\n" . '<h2 class="tabtitle' . $class . '">' . wp_kses( $args['title'], array( 'br' => array(), 'strong' => array(), 'em' => array(), 'i' => array() ) ) . '</h2>' . "\n" . '<div class="' . sanitize_text_field( $tabcontentclass ) . '">' . "\n";
    }
    else
    {
        //* Build output if we are making a subsequent (non-first tab)
        return  "\n" . '</div><h2 class="tabtitle' . $class . '">' . wp_kses( $args['title'], array( 'br' => array(), 'strong' => array(), 'em' => array(), 'i' => array() ) ) . '</h2>' . "\n" . '<div class="' . sanitize_text_field( $tabcontentclass ) . '">' . "\n";
    }
}

function tab_ending( $atts, $content = null )
{
    $GLOBALS["reset_firsttab_flag"] = true;

    return '</div></div>';
}
