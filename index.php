<?php

ob_start();

session_name();

session_start();

require_once( 'config.php' );
require_once( 'functions/db.php' );
require_once( 'functions/cart.php' );
require_once( 'functions/actions.php' );
require_once( 'functions/template.php' );
require_once( 'functions/flash-message.php' );
require_once( 'functions/global-functions.php' );

require_once( 'functions/includes/tabs.php' );
require_once( 'functions/includes/kses.php' );
require_once( 'functions/includes/plugin.php' );
require_once( 'functions/includes/functions.php' );
require_once( 'functions/includes/formatting.php' );
require_once( 'functions/includes/shortcodes.php' );

require( 'template.php' );

?>