var calendar;

jQuery(document).ready(function (){
    var selector = document.getElementById('wrap-list-form-calendar');
    
    calendar = new FullCalendar.Calendar( selector, {
        initialView: 'dayGridMonth',
        selectable: true,
        headerToolbar: {
            end: 'prevYear nextYear',
            start: 'prev next',
            center: 'title',
        },
        events: function( e, successCallback, failureCallback ) {
            var url = jQuery('#ajax_url').val();
            var prm = new Object;
                prm.pKEY    = 'availability_calendar';
                prm.acco_id = jQuery('#lpost_id').val();
                prm.room_id = jQuery('#lroom_type').val();
                prm.start   = moment( e.start ).format( 'YYYY-MM-DD' );
                prm.end     = moment( e.end ).subtract(1, 'days').format( 'YYYY-MM-DD' );

            jQuery.ajax({
                url: url,
                data: prm,
                type: 'POST',
                dataType : 'json',
                success: function( e ){
                    if( e.length > 0 )
                    {
                        successCallback( e )
                    }
                },
                error: function( e ){
                    show_popup( 'Something wrong please reload the page!' );
                }
            });
        },
        eventDidMount: function( arg ) {
            var dt = arg.event.extendedProps;

            if( dt.status == 1 )
            {
                jQuery(arg.el).parents('.fc-day').addClass('fc-available');
                jQuery(arg.el).parents('.fc-day').removeClass('fc-booked');
            }
            else if( dt.status == 2 )
            {
                jQuery(arg.el).parents('.fc-day').removeClass('fc-available');
                jQuery(arg.el).parents('.fc-day').addClass('fc-booked');
            }

            tippy( arg.el, {
                content: '<p>Allotment : ' + dt.allotment + '<br/>Available : ' + dt.available + '<br/>Booked : ' + dt.booked + '</p>',
                allowHTML: true,
            });
        },
        eventContent: function( arg ){
            var dt = arg.event.extendedProps;
            var el = document.createElement('div');

            el.innerHTML = 
            '<p class="allotment">' + dt.booked + '/' + dt.available + '/' + dt.allotment + '</p>'+
            '<p class="rate">Rate : ' + dt.rate + '</p>';

            return { domNodes: [ el ] }
        },
        selectAllow: function( e ){
            return moment().diff( e.start, 'days' ) <= 0;
        },
        select: function( e ){
            var src = jQuery('#ajax_url').val();

            jQuery.fancybox.open({
                type: 'ajax',
                src: src,
                opts: {
                    baseClass: 'popup-booking popup-change-rate-availability',
                    closeExisting: true,
                    ajax: { 
                        settings: { 
                            type: 'POST',
                            data: {
                                pKEY    : 'popup_availability',
                                acco_id : jQuery('#lpost_id').val(),
                                room_id : jQuery('#lroom_type').val(),
                                start   : moment( e.start ).format( 'YYYY-MM-DD' ),
                                end     : moment( e.end ).subtract(1, 'days').format( 'YYYY-MM-DD' )
                            }
                        }
                    },
                    afterLoad : function( instance, current ){
                        var form = jQuery(current.$content).find('form');
                        var nsel = jQuery(current.$content).find('.text-num');
                        var psel = jQuery(current.$content).find('.text-price');
                        var csel = jQuery(current.$content).find('.chzn-select');

                        init_number( nsel );
                        init_number( psel );
                        csel.chosen();

                        jQuery(current.$content).find('[name=lstatus]').on('change', function(){
                            if( jQuery(this).val() == '' )
                            {
                                jQuery(current.$content).find('[name=lavailable]').prop('disabled', true);
                                jQuery(current.$content).find('[name=lbooked]').prop('disabled', true);
                            }
                            else if( jQuery(this).val() == '1' )
                            {
                                jQuery(current.$content).find('[name=lavailable]').prop('disabled', false);
                                jQuery(current.$content).find('[name=lbooked]').prop('disabled', false);
                            }
                            else if( jQuery(this).val() == '2' )
                            {
                                jQuery(current.$content).find('[name=lavailable]').prop('disabled', true);
                                jQuery(current.$content).find('[name=lbooked]').prop('disabled', true);
                            }
                        });

                        jQuery(current.$content).find('[name=lrate_change]').on('change', function(){
                            if( jQuery(this).val() == '0' )
                            {
                                jQuery(current.$content).find('[name=lrate_id]').prop('disabled', true).trigger('chosen:updated');
                            }
                            else
                            {
                                jQuery(current.$content).find('[name=lrate_id]').prop('disabled', false).trigger('chosen:updated');
                            }
                        });

                        jQuery(current.$content).find('[name=lrate_id]').on('change', function(){
                            if( jQuery(this).val() != '' )
                            {
                                var arr = jQuery(this).val().split('|');

                                AutoNumeric.set( '#lrate', arr[1] );

                                jQuery(current.$content).find('[name=lrate]').prop('readonly', true);
                            }
                            else
                            {
                                AutoNumeric.set( '#lrate', 0 );

                                jQuery(current.$content).find('[name=lrate]').prop('readonly', false);
                            }
                        });

                        form.unbind();
                        form.on('submit', function(){
                            jQuery.ajax({
                                url: jQuery('#ajax_url').val(),
                                data: form.serializeArray(),
                                dataType : 'json',
                                method: 'POST',
                                beforeSend: function( xhr ){
                                    form.parent().addClass('processing');
                                },
                                success: function( e ){ 
                                    eval( 'refetch()' );

                                    alert( e.message );
                                },
                                error: function( e ){
                                    alert( 'Change rate and availability is failed' );
                                },
                                complete: function( e ){
                                    form.parent().removeClass('processing');
                                }
                            });

                            return false;
                        });
                    }
                }
            });
        }
    });

    calendar.render();

    jQuery('#lroom_type').on('change', function(){
        eval( 'refetch()' );
    });
});

function refetch()
{
    calendar.refetchEvents();
}