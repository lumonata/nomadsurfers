<?php

class accommodation extends db
{
    private $template;
    private $actions;
    private $upload;
    private $global;
    private $flash;

    private $message;
    private $fields;
    private $terms;
    private $lang;
    private $ref;

    private $notif;
    private $valid;
    private $post;
    private $sess;

    function __construct( $template_dir = '.', $actions )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->upload   = new upload();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }

    function view( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $this->global->getPViewBoth( $mod, $usertype ) )
        {
            $this->template->set_var( 'mod', $mod );
            $this->template->set_var( 'lang', $this->lang );
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'limit', $this->global->getSettingValue( 'list_admin' ) );

            $this->template->set_var( 'lcity', $this->get_city_option() );
            $this->template->set_var( 'lstate', $this->get_state_option() );
            $this->template->set_var( 'lstatus', $this->get_status_option() );
            $this->template->set_var( 'lcountry', $this->get_country_option() );
            $this->template->set_var( 'lcontinent', $this->get_continent_option() );
            $this->template->set_var( 'llang_id', $this->get_language_option( $mod ) );
            $this->template->set_var( 'laccommodation_type', $this->get_accommodation_type_option() );
            $this->template->set_var( 'laccommodation_surf_trip', $this->get_accommodation_surf_trip_option() );

            $this->template->set_var( 'add_url', $this->global->getAddUrl( $mod, $usertype ) );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Accommodations/accommodation/view.js' ) );

            $this->template->Parse( 'vC', 'viewContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function insert( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->global->getPInsertBoth( $mod, $usertype ) )
        {
            //-- INIT custom field
            $this->custom_field( $mod );

            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->create( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            //-- EXTRACT terms
            extract( $this->terms );

            $this->template->set_var( 'act', 'add' );
            $this->template->set_var( 'ltitle', $ltitle );
            $this->template->set_var( 'lsef_url', $lsef_url );
            $this->template->set_var( 'lpost_id', $lpost_id );
            $this->template->set_var( 'llang_id', $llang_id );
            $this->template->set_var( 'lorder_id', $lorder_id );
            $this->template->set_var( 'ldescription', $ldescription );
            $this->template->set_var( 'switcher_lang', $this->switcher_language( $mod, $llang_id, $this->ref ) );

            $this->template->set_var( 'lmeta_title', $lmeta_title );
            $this->template->set_var( 'lmeta_desc', $lmeta_desc );
            $this->template->set_var( 'lmeta_key', $lmeta_key );
            
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'lperiod', $this->get_period_option( $lpost_id ) );
            $this->template->set_var( 'lcreated_by', $this->get_agent_option( $lcreated_by ) );
            $this->template->set_var( 'laccommodation_type', $this->get_accommodation_type_option( $accommodation_type ) );
            $this->template->set_var( 'laccommodation_surf_trip', $this->get_accommodation_surf_trip_option( $surf_trip, $llang_id ) );

            $this->template->set_var( 'lcustom', $this->actions->attemp_actions( 'accommodation_custom_field' ) );
            $this->template->set_var( 'ladditional', $this->actions->attemp_actions( 'accommodation_additional_field' ) );

            $this->template->set_var( 'site_url', SITE_URL );
            $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
            $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );

            $this->template->set_var( 'preview_class', 'button hidden' );
            $this->template->set_var( 'preview_url' , 'javascript:;' );

            $this->template->set_var( 'add_url', $this->global->getAddUrl( $mod, $usertype ) );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/moment@2.29.1/moment.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js' ) );
            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.css' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/blueimp-file-upload@10.32.0/js/jquery.fileupload.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/sortablejs@1.14.0/Sortable.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/tinymce@5.10.2/tinymce.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( '//' . ADMIN_THEME_URL . '/assets/dropbox.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Accommodations/accommodation/form.js' ) );

            $this->template->Parse( 'fC', 'formContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function edit( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->valid )
        {
            if( $this->global->getPEditBoth( $mod, $usertype ) )
            {
                //-- INIT custom field
                $this->custom_field( $mod );

                //-- EXECUTE update action
                if( isset( $this->post[ 'save' ] ) )
                {
                    $this->change( $mod );
                }

                //-- EXTRACT fields
                extract( $this->fields );

                //-- EXTRACT terms
                extract( $this->terms );

                $this->template->set_var( 'act', 'edit' );
                $this->template->set_var( 'ltitle', $ltitle );
                $this->template->set_var( 'lsef_url', $lsef_url );
                $this->template->set_var( 'lpost_id', $lpost_id );
                $this->template->set_var( 'llang_id', $llang_id );
                $this->template->set_var( 'lorder_id', $lorder_id );
                $this->template->set_var( 'lcreated_by', $lcreated_by );
                $this->template->set_var( 'ldescription', $ldescription );
                $this->template->set_var( 'switcher_lang', $this->switcher_language( $mod, $llang_id, $this->ref ) );

                $this->template->set_var( 'lmeta_title', $lmeta_title );
                $this->template->set_var( 'lmeta_desc', $lmeta_desc );
                $this->template->set_var( 'lmeta_key', $lmeta_key );

                $this->template->set_var( 'message', $this->message );
                $this->template->set_var( 'lperiod', $this->get_period_option( $lpost_id ) );
                $this->template->set_var( 'lcreated_by', $this->get_agent_option( $lcreated_by ) );
                $this->template->set_var( 'laccommodation_type', $this->get_accommodation_type_option( $accommodation_type ) );
                $this->template->set_var( 'laccommodation_surf_trip', $this->get_accommodation_surf_trip_option( $surf_trip, $llang_id ) );
            
                $this->template->set_var( 'lcustom', $this->actions->attemp_actions( 'accommodation_custom_field' ) );
                $this->template->set_var( 'ladditional', $this->actions->attemp_actions( 'accommodation_additional_field' ) );

                $this->template->set_var( 'site_url', SITE_URL );
                $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
                $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );

                $this->template->set_var( 'preview_class', 'button' );
                $this->template->set_var( 'preview_url' , $this->global->getPermalink( $this->fields, 'accommodation' ) );

                $this->template->set_var( 'add_url', $this->global->getAddUrl( $mod, $usertype ) );
                $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_THEME_URL . '/assets/chosen-order.js' ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js' ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/moment@2.29.1/moment.min.js' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js' ) );
                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.css' ) );

                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/blueimp-file-upload@10.32.0/js/jquery.fileupload.min.js' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/tinymce@5.10.2/tinymce.min.js' ) );

                $this->actions->add_actions( 'css', $this->global->get_css( '//' . ADMIN_THEME_URL . '/assets/dropbox.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Accommodations/accommodation/form.js' ) );

                $this->template->Parse( 'fC', 'formContent', true );
            }
            else
            {
                $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
            }
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NOT_FOUND ) );
        }
        
        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function duplicate( $mod, $usertype )
    {
        echo $mod;
        exit;
    }

    function calendar( $mod, $usertype )
    {
        $this->template->set_file( 'calendar', 'calendar.html' );

        $this->template->set_block( 'calendar', 'calendarContent', 'cC' );
        $this->template->set_block( 'calendar', 'calendarBlock', 'cB' );

        if( $this->global->getPEditBoth( $mod, $usertype ) )
        {
            extract( $this->fields );

            $this->template->set_var( 'ltitle', $ltitle );
            $this->template->set_var( 'lpost_id', $lpost_id );

            if( empty( $this->terms[ 'room_type' ] ) )
            {
                $this->template->set_var( 'lroom_type_class', 'hidden' );
                $this->template->set_var( 'lroom_type', '' );
            }
            else
            {
                $this->template->set_var( 'lroom_type_class', '' );
                $this->template->set_var( 'lroom_type', $this->get_accommodation_room_type_option( $lpost_id ) );
            }

            $this->template->set_var( 'site_url', SITE_URL );
            $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
            $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/fullcalendar@5.10.1/main.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/fullcalendar@5.10.1/main.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/moment@2.29.1/moment.min.js' ) );
            
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.2/dist/umd/popper.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/tippy.js@6.3.7/dist/tippy-bundle.umd.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( '//' . ADMIN_THEME_URL . '/assets/calendar.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Accommodations/accommodation/calendar.js' ) );

            $this->template->Parse( 'cC', 'calendarContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'cB', 'calendarBlock', false );
    }

    function popup_availability()
    {
        $this->template->set_file( 'popup', 'popup.html' );

        $this->template->set_block( 'popup', 'popupAvailabilityBlock', 'pavB' );
        $this->template->set_block( 'popup', 'popupEditBlock', 'peB' );
        $this->template->set_block( 'popup', 'popupAddBlock', 'paB' );
        $this->template->set_block( 'popup', 'notfoundBlock', 'nfB' );

        $dt = $this->global->getPosts( array( 'lpost_id' => $this->post[ 'acco_id' ] ) );

        if( empty( $dt ) === false )
        {
            if( empty( $dt[ 'room_type' ] ) )
            {
                $this->template->set_var( 'lrate', $dt[ 'start_price' ] );
                $this->template->set_var( 'lallotment', $dt[ 'room_allotment' ] );
            }
            else
            {
                $key = array_search( $this->post[ 'room_id' ], array_column( $dt[ 'room_type' ], 'lterm_id' ) );

                if( isset( $dt[ 'room_type' ][ $key ][ 'room_allotment' ] ) )
                {
                    $this->template->set_var( 'lallotment', $dt[ 'room_type' ][ $key ][ 'room_allotment' ] );
                }
                else
                {
                    $this->template->set_var( 'lallotment', $dt[ 'room_allotment' ] );
                }

                $this->template->set_var( 'lrate', $dt[ 'start_price' ] );
            }

            $this->template->set_var( 'ldlu', time() );
            $this->template->set_var( 'lcreated_date', time() );
            $this->template->set_var( 'lcurrency_id', $dt[ 'currency' ] );
            $this->template->set_var( 'lusername', $this->sess[ 'username' ] );
            $this->template->set_var( 'lcreated_by', $this->sess[ 'username' ] );

            $this->template->set_var( 'lpost_id', $this->post[ 'acco_id' ] );
            $this->template->set_var( 'lterm_id', $this->post[ 'room_id' ] );
            $this->template->set_var( 'lrate_id', $this->get_accommodation_rates_option( $dt[ 'lpost_id' ] ) );

            $this->template->set_var( 'sign', $this->global->get_currency_sign( $dt[ 'lpost_id' ] ) );

            $this->template->set_var( 'end', date( 'd F Y', strtotime( $this->post[ 'end' ] ) ) );
            $this->template->set_var( 'start', date( 'd F Y', strtotime( $this->post[ 'start' ] ) ) );

            $this->template->set_var( 'lend', date( 'Y-m-d', strtotime( $this->post[ 'end' ] ) ) );
            $this->template->set_var( 'lstart', date( 'Y-m-d', strtotime( $this->post[ 'start' ] ) ) );

            $this->template->parse( 'pavB', 'popupAvailabilityBlock', false );
        }

        return $this->template->finish( $this->template->parse( 'Output', 'popup', false ) );
    }

    function load( $mod, $usertype )
    {
        $cols  = array(
            1 => 'a.ltitle',
            3 => 'a.lstatus'
        );

        //-- Query Limit
        $limit = parent::prepare_query( ' LIMIT ' . $this->post[ 'start' ] . ', ' . $this->post[ 'length' ] );
    
        //-- Query Order By
        if( empty( $this->post[ 'order' ] ) )
        {
            $order = parent::prepare_query( ' ORDER BY a.llang_id, a.lorder_id ASC' );
        }
        else
        {
            $order = array();

            foreach( $this->post[ 'order' ] as $i => $od )
            {
                if( isset( $cols[ $od[ 'column' ] ] ) )
                {
                    $order[] = $cols[ $od[ 'column' ] ] . ' ' . $od[ 'dir' ];
                }
            }

            if( empty( $order ) )
            {
                $order = parent::prepare_query( ' ORDER a.llang_id, BY a.lorder_id ASC' );
            }
            else
            {
                $order = parent::prepare_query( ' ORDER BY ' . implode( ', ', $order ) );
            }
        }

        //-- Query Additional Where
        $w = array();

        if( $this->post[ 'laccommodation_type' ] != '' )
        {
            $w[] = parent::prepare_query( 'laccommodation_type = %d', $this->post[ 'laccommodation_type' ] );
        }

        if( $this->post[ 'lsurf_trip' ] != '' )
        {
            $w[] = parent::prepare_query( 'FIND_IN_SET( %d, lsurf_trip )', $this->post[ 'lsurf_trip' ] );
        }

        if( $this->post[ 'lcontinent' ] != '' )
        {
            $w[] = parent::prepare_query( 'lcontinent = %d', $this->post[ 'lcontinent' ] );
        }

        if( $this->post[ 'lcountry' ] != '' )
        {
            $w[] = parent::prepare_query( 'lcountry = %d', $this->post[ 'lcountry' ] );
        }

        if( $this->post[ 'lstate' ] != '' )
        {
            $w[] = parent::prepare_query( 'lstate = %d', $this->post[ 'lstate' ] );
        }

        if( $this->post[ 'lcity' ] != '' )
        {
            $w[] = parent::prepare_query( 'lcity = %d', $this->post[ 'lcity' ] );
        }

        if( $this->post[ 'lstatus' ] != '' )
        {
            $w[] = parent::prepare_query( 'a.lstatus = %d', $this->post[ 'lstatus' ] );
        }

        if( $this->post[ 'llang_id' ] != '' )
        {
            $w[] = parent::prepare_query( 'a.llang_id = %d', $this->post[ 'llang_id' ] );
        }

        if( $this->post[ 'lsearch' ] != '' )
        {
            $s = array();

            foreach( $cols as $col )
            {
                $s[] = parent::prepare_query( $col . ' LIKE %s', '%' . $this->post[ 'lsearch' ] . '%' );
            }

            $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
        }

        if( empty( $w ) === false )
        {
            $having = ' HAVING ' . implode( ' AND ', $w );
        }
        else
        {
            $having = '';
        }

        if( $this->sess[ 'usertype' ] == 2 )
        {
            $where = parent::prepare_query( ' WHERE a.ltype = %s AND ( SELECT a2.lusertype_id FROM lumonata_user AS a2 WHERE a2.lusername = a.lusername ) = %d', 'accommodation', 2 );
        }
        else
        {
            $where = parent::prepare_query( ' WHERE a.ltype = %s', 'accommodation' );
        }

        //-- Main Query
        $qm = 'SELECT 
                a.ltitle,
                a.lstatus,
                a.lpost_id,
                a.llang_id,
                a.lsef_url,
                ( SELECT GROUP_CONCAT( a2.lterm_id ) FROM lumonata_post_relationship AS a2 LEFT JOIN lumonata_post_terms AS b2 ON a2.lterm_id = b2.lterm_id WHERE a2.lpost_id = a.lpost_id AND b2.lrule = "accommodation_type" ) AS laccommodation_type,
                ( SELECT GROUP_CONCAT( a2.lterm_id ) FROM lumonata_post_relationship AS a2 LEFT JOIN lumonata_post_terms AS b2 ON a2.lterm_id = b2.lterm_id WHERE a2.lpost_id = a.lpost_id AND b2.lrule = "surf_trip" ) AS lsurf_trip,
                ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id  AND a2.ladditional_key = "accommodation_continent" ) AS lcontinent,
                ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id  AND a2.ladditional_key = "accommodation_country" ) AS lcountry,
                ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id  AND a2.ladditional_key = "accommodation_state" ) AS lstate,
                ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id  AND a2.ladditional_key = "accommodation_city" ) AS lcity,
                ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id  AND a2.ladditional_key = "policy" ) AS policy
               FROM lumonata_post AS a' . $where . $having . $order;
        $rm = parent::query( $qm );
        $nm = parent::num_rows( $rm );

        $qf = $qm . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        $data = array();

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
                $data[] = array(
                    'lduplicatelink' => $this->global->getDuplicateUrl( $mod, $usertype, $df[ 'lpost_id' ] ),
                    'lcalendarlink'  => $this->global->getCalendarUrl( $mod, $usertype, $df[ 'lpost_id' ] ),
                    'leditlink'      => $this->global->getEditUrl( $mod, $usertype, $df[ 'lpost_id' ] ),
                    'lflag'          => $this->global->get_language_flag( $df[ 'llang_id' ] ),
                    'lviewlink'      => $this->global->getPermalink( $df, 'accommodation' ),
                    'lacco_type'     => $this->accommodation_type( $df[ 'laccommodation_type' ] ),
                    'lsurf_trip'     => $this->accommodation_surf_trip( $df[ 'lsurf_trip' ] ),
                    'ltitle'         => ucwords( strtolower( $df[ 'ltitle' ] ) ),
                    'lpost_id'       => $df[ 'lpost_id' ],
                    'lstatus'        => $df[ 'lstatus' ],
                    'lpolicy'        => $df[ 'policy' ]
                );
            }
        }

        return json_encode( array(
            'draw'            => intval( $this->post[ 'draw' ] ),
            'recordsFiltered' => intval( $nm ),
            'recordsTotal'    => intval( $nf ),
            'data'            => $data
        ));
    }

    function availability_calendar()
    {
        $gap   = new DateInterval('P1D');  
        $start = new DateTime( $this->post[ 'start' ] );
        $end   = new DateTime( $this->post[ 'end' ] );
        $range = new DatePeriod( $start, $gap, $end->add( $gap ) );

        $ac = $this->global->getPosts( array( 'lpost_id' => $this->post[ 'acco_id' ] ) );
        
        if( empty( $ac ) === false )
        {
            if( empty( $ac[ 'room_type' ] ) )
            {
                $lrate      = 0;
                $lallotment = 0;
            }
            else
            {
                $key = array_search( $this->post[ 'room_id' ], array_column( $ac[ 'room_type' ], 'lterm_id' ) );

                if( isset( $ac[ 'room_type' ][ $key ][ 'room_price' ] ) )
                {
                    $lrate = $ac[ 'room_type' ][ $key ][ 'room_price' ];
                }
                else
                {
                    $lrate = 0;
                }

                if( isset( $ac[ 'room_type' ][ $key ][ 'room_allotment' ] ) )
                {
                    $lallotment = $ac[ 'room_type' ][ $key ][ 'room_allotment' ];
                }
                else
                {
                    $lallotment = $ac[ 'room_allotment' ];
                }
            }
        }

        $list = array();

        foreach( $range as $d )
        {
            $dt = $this->global->getFields( 'lumonata_calendar', '*', array(
                'lpost_id' => $this->post[ 'acco_id' ],
                'lterm_id' => $this->post[ 'room_id' ],
                'ldate'    => $d->format( 'Y-m-d' ),
            ));

            if( empty( $dt ) === false )
            {
                $list[] = array(
                    'rate'      => number_format( $dt[ 'lrate' ], 0 ),
                    'status'    => intval( $dt[ 'lstatus' ] ),
                    'start'     => $d->format( 'Y-m-d' ),
                    'available' => $dt[ 'lavailable' ],
                    'allotment' => $dt[ 'lallotment' ],
                    'booked'    => $dt[ 'lbooked' ],
                ); 
            }
            else
            {
                $list[] = array(
                    'start'     => $d->format( 'Y-m-d' ),
                    'allotment' => $lallotment,
                    'available' => $lallotment,
                    'rate'      => $lrate,
                    'status'    => 1,
                    'booked'    => 0
                ); 
            }
        }

        return json_encode( $list );
    }

    function validate( $data = array() )
    {
        $error = array();

        foreach( $data as $field => $dt )
        {
            if( $field == 'ltitle' && $dt == '' )
            {
                array_push( $error, 'Destination name must have value' );
            }
        }

        return $error;
    }

    function create( $mod )
    {
        $terms = $this->terms;
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- MERGE lsef_url INTO data array
            if( empty( $data[ 'lsef_url' ] ) )
            {
                $arr = array();

                foreach( array( 'title_part_1', 'title_part_2', 'title_part_3', 'title_part_4' ) as $dt )
                {
                    if( isset( $data[ 'ladditional' ][ $dt ] ) )
                    {
                        $arr[] = $data[ 'ladditional' ][ $dt ]; 
                    }
                }

                if( empty( $arr ) )
                {
                    $data = array_merge( $data, array( 'lsef_url' => $this->sef_url( $data[ 'ltitle' ] ) ) );
                }
                else
                {
                    $data = array_merge( $data, array( 'lsef_url' => $this->sef_title( implode( ' ', $arr ) ) ) );
                }
            }
            else
            {
                $data = array_merge( $data, array( 'lsef_url' => $this->sef_url( $data[ 'lsef_url' ] ) ) );
            }

            //-- INSERT lumonata_post
            $param  = array_diff_key( $data, array_flip( array( 'lpost_id', 'ladditional' ) ) );
            $result = parent::insert( 'lumonata_post', array_filter( $param ) );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to add new data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

                exit;
            }
            else
            {
                $lapp_id = parent::insert_id();

                //-- SYNC lumonata_post_relationship
                if( empty( $terms ) === false )
                {
                    foreach( $terms as $rule => $term )
                    {
                        if( is_array( $term ) )
                        {
                            foreach( $term as $t )
                            {
                                if( empty( $t ) === false )
                                {
                                    parent::insert( 'lumonata_post_relationship', array(
                                        'lpost_id' => $lapp_id,
                                        'lterm_id' => $t,
                                    ));
                                }
                            }
                        }
                        else
                        {
                            if( empty( $term ) === false )
                            {
                                parent::insert( 'lumonata_post_relationship', array(
                                    'lpost_id' => $lapp_id,
                                    'lterm_id' => $term,
                                ));
                            }
                        }
                    }
                }
                
                //-- SYNC lumonata_attachment
                $param = array( 'lapp_id' => $lapp_id );
                $where = array( 'lapp_id' => $data[ 'lpost_id' ], 'lmodule_id' => $mod );

                parent::update( 'lumonata_attachment', $param, $where );

                //-- SYNC lumonata_additional_field
                if( isset( $data[ 'ladditional' ] ) )
                {
                    foreach( $data[ 'ladditional' ] as $key => $value )
                    {
                        if( is_array( $value ) )
                        {
                            $value = json_encode( $value );
                        }

                        parent::insert( 'lumonata_additional_field', array(
                            'ladditional_value' => $value,
                            'ladditional_key' => $key,
                            'lapp_id' => $lapp_id,
                            'lmodule_id' => $mod,
                        ));
                    }
                }

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully add new data' ) );
                
                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $lapp_id );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

            exit;
        }
    }

    function change( $mod )
    {
        $terms = $this->terms;
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- MERGE lsef_url INTO data array
            if( empty( $data[ 'lsef_url' ] ) )
            {
                $data = array_merge( $data, array( 'lsef_url' => $this->sef_url( $data[ 'ltitle' ] ) ) );
            }

            //-- UPDATE lumonata_post
            $param  = array_diff_key( $data, array_flip( array( 'lpost_id', 'ladditional' ) ) );
            $where  = array( 'lpost_id' => $data[ 'lpost_id' ] );

            $result = parent::update( 'lumonata_post', array_filter( $param ), $where );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to update existing data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

                exit;
            }
            else
            {
                //-- SYNC lumonata_post_relationship
                if( empty( $terms ) === false )
                {
                    $rt = parent::delete( 'lumonata_post_relationship', array( 'lpost_id' => $data[ 'lpost_id' ]  ) );

                    if( is_array( $rt ) === false )
                    {
                        foreach( $terms as $rule => $term )
                        {
                            if( is_array( $term ) )
                            {
                                foreach( $term as $t )
                                {
                                    if( empty( $t ) === false )
                                    {
                                        parent::insert( 'lumonata_post_relationship', array(
                                            'lpost_id' => $data[ 'lpost_id' ],
                                            'lterm_id' => $t,
                                        ));
                                    }
                                }
                            }
                            else
                            {
                                if( empty( $term ) === false )
                                {
                                    parent::insert( 'lumonata_post_relationship', array(
                                        'lpost_id' => $data[ 'lpost_id' ],
                                        'lterm_id' => $term,
                                    ));
                                }
                            }
                        }
                    }
                }

                //-- SYNC lumonata_additional_field
                if( isset( $data[ 'ladditional' ] ) )
                {
                    foreach( $data[ 'ladditional' ] as $key => $value )
                    {
                        if( is_array( $value ) )
                        {
                            $value = json_encode( $value );
                        }

                        //-- Check Additonal Field Exist or Not
                        $aid = $this->global->getValueField3( 'lumonata_additional_field', 'ladditional_id', 'lapp_id', $data[ 'lpost_id' ], 'ladditional_key', $key, 'lmodule_id', $mod );

                        if( empty( $aid ) )
                        {
                            parent::insert( 'lumonata_additional_field', array(
                                'lapp_id' => $data[ 'lpost_id' ],
                                'ladditional_value' => $value,
                                'ladditional_key' => $key,
                                'lmodule_id' => $mod,
                            ));
                        }
                        else
                        {
                            $param = array( 'ladditional_value' => $value );
                            $where = array( 'ladditional_id' => $aid );

                            parent::update( 'lumonata_additional_field', $param, $where );
                        }
                    }
                }
                
                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully updated existing data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

            exit;
        }
    }

    function do_delete( $mod )
    {
        parent::begin();

        $commit = 1;

        $s = 'DELETE FROM lumonata_post WHERE lpost_id = %s';
        $q = parent::prepare_query( $s, $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            $commit = 0;
        }
        else
        {
            $s = 'SELECT * FROM lumonata_attachment AS a WHERE a.lapp_id = %d AND a.lmodule_id = %d';
            $q = parent::prepare_query( $s, $this->post[ 'id' ], $mod );
            $r = parent::query( $q );

            while( $d = parent::fetch_array( $r ) )
            {
                $r2 = parent::delete( 'lumonata_attachment', array( 'lattach_id' => $d[ 'lattach_id' ] ) );

                if( is_array( $r2 ) )
                {
                    $commit = 0;
                }
                else
                {
                    if( empty( $d[ 'lattach' ] ) === false )
                    {
                        $this->upload->upload_constructor( IMAGE_DIR . '/Uploads/' );
                        $this->upload->delete_file_thumb( $d[ 'lattach' ] );
                    }
                }
            }
        }
                
        if( $commit == 0 )
        {
            parent::rollback();

            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            parent::commit();

            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function do_change_status()
    {
        $s = 'UPDATE lumonata_post AS a SET a.lstatus = %d WHERE a.lpost_id = %s';
        $q = parent::prepare_query( $s, $this->post[ 'status' ], $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function update_availability()
    {
        if( isset( $this->post[ 'lallotment' ] ) && isset( $this->post[ 'lallotment_booked' ] ) )
        {
            $summary   = $this->post[ 'lallotment' ] + $this->post[ 'lallotment_booked' ];
            $allotment = $this->post[ 'room_allotment' ];

            if( $allotment < $summary )
            {
                return json_encode( array( 'result' => 'error', 'message' => 'Number of available + booked room can\'t greater than "' . $allotment . '"' ) );
            }
        }

        if( $this->post[ 'lstatus' ] == '' && $this->post[ 'lrate_change' ] == '0' )
        {
            return json_encode( array( 'result' => 'error', 'message' => 'Sorry no change has been made, make sure you choose the correct data' ) );  
        }

        $gap   = new DateInterval('P1D');  
        $start = new DateTime( $this->post[ 'lstart' ] );
        $end   = new DateTime( $this->post[ 'lend' ] );
        $range = new DatePeriod( $start, $gap, $end->add( $gap ) );

        if( empty( $range ) === false )
        {
            parent::begin();

            $commit = 1;

            if( isset( $this->post[ 'lrate_id' ] ) && !empty( $this->post[ 'lrate_id' ] ) )
            {
                list( $rid, $rate ) = explode( '|', $this->post[ 'lrate_id' ] );

                $this->post[ 'lrate_id' ] = $rid;
                $this->post[ 'lrate' ] = $rate;
            }

            $param = array_diff_key( $this->post, array_flip( array( 'pKEY', 'lstart', 'lend', 'lrate_change', 'room_allotment' ) ) );
            $param = array_filter( $param );

            foreach( $range as $d )
            {
                $param[ 'ldate' ] = $d->format( 'Y-m-d' );

                $cid = $this->global->getFields( 'lumonata_calendar', 'lcalendar_id', array(
                    'lpost_id' => $this->post[ 'lpost_id' ],
                    'lterm_id' => $this->post[ 'lterm_id' ],
                    'ldate'    => $param[ 'ldate' ],
                ));

                if( empty( $cid ) )
                {
                    $r = parent::insert( 'lumonata_calendar', $param );
                }
                else
                {
                    $r = parent::update( 'lumonata_calendar', $param, array( 'lcalendar_id' => $cid ) );
                }

                if( is_array( $r ) )
                {
                    $commit = 0;
                }
            }

            if( $commit == 0 )
            {
                parent::rollback();

                return json_encode( array( 'result' => 'failed', 'message' => 'Failed to change rate/availability' ) ); 
            }
            else
            {
                parent::commit();

                return json_encode( array( 'result' => 'success', 'message' => 'Successfully change rate/availability' ) ); 
            }
        }
        else
        {
            return json_encode( array( 'result' => 'failed', 'message' => 'No date selected' ) ); 
        }
    }

    function upload_file( $mod )
    {
        if( isset( $_FILES[ 'attachment' ] ) && $_FILES[ 'attachment' ][ 'error' ] == 0 )
        {
            global $db;

            extract( $this->post );

            $g = new globalAdmin();
            $u = new upload();

            $file_name = $_FILES[ 'attachment' ][ 'name' ];
            $file_size = $_FILES[ 'attachment' ][ 'size' ];
            $file_type = $_FILES[ 'attachment' ][ 'type' ];
            $file_tmp  = $_FILES[ 'attachment' ][ 'tmp_name' ];

            $sef_img  = $this->upload->file_name_filter( $file_name ) . '-' . time();
            $file     = $this->upload->rename_file( $file_name, $sef_img );

            $this->upload->upload_constructor( IMAGE_DIR . '/Uploads/' );

            if( $this->post[ 'file_type' ] == 'images' )
            {
                if( in_array( $file_type, array( 'image/jpg', 'image/jpeg', 'image/pjpeg', 'image/gif', 'image/png' ) ) )
                {
                    $this->upload->upload_resize( $file_name, $sef_img, $file_tmp, $file_type, 940, 940, 1 );
                }
                else
                {
                    return json_encode( array( 'result' => 'not-supported' ) );
                }
            }
            elseif( $this->post[ 'file_type' ] == 'word' && !in_array( $file_type, array( 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ) ) )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }
            elseif( $this->post[ 'file_type' ] == 'pdf' && $file_type != 'application/pdf' )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }
            elseif( $this->post[ 'file_type' ] == 'svg' && $file_type != 'image/svg+xml' )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }

            if( $this->upload->upload_file( $file_name, $sef_img, $file_tmp, 0 ) )
            {
                $order_id = $this->global->setCode( 'lumonata_attachment', 'lorder_id' );

                $r = $db->insert( 'lumonata_attachment', array(
                    'lcreated_by'   => $this->sess[ 'username' ],
                    'lusername'     => $this->sess[ 'username' ],
                    'llang_id'      => $this->lang,
                    'lmimetype'     => $file_type,
                    'lorder_id'     => $order_id,
                    'lsef_url'      => $sef_img,
                    'lapp_id'       => $app_id,
                    'ltitle'        => $title,
                    'lattach'       => $file,
                    'lmodule_id'    => $mod,
                    'lcreated_date' => time(),
                    'ldlu'          => time(),
                    'lstatus'       => 2
                ));

                if( is_array( $r ) )
                {
                    $this->upload->delete_file_thumb( $file );

                    return json_encode( array( 'result' => 'failed' ) );
                }
                else
                {
                    $id = $db->insert_id();

                    if( $file_type == 'image/jpg' || $file_type == 'image/jpeg' || $file_type == 'image/pjpeg' || $file_type == 'image/gif' || $file_type == 'image/png' )
                    {
                        //-- If file type is an image show the thumbnail
                        $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=180&h=180&src=' . HT_SERVER . IMAGE_URL . '/Uploads/' . $file;
                    }
                    else
                    {
                        if( in_array( $file_type, array( 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ) ) )
                        {
                            //-- Word file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-doc.png';
                        }
                        elseif( $file_type == 'application/pdf' )
                        {
                            //-- PDF file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-pdf.png';
                        }
                        elseif( $file_type == 'image/svg+xml' )
                        {
                            //-- SVG file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-svg.png';
                        }
                        else
                        {
                            //-- Other file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-file.png';
                        }
                    }

                    return json_encode( array( 'result' => 'success', 'src' => $src, 'id' => $id ) );
                }
            }
            else
            {
                $this->upload->delete_thumb( $file );

                return json_encode( array( 'result' => 'failed' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'error' ) );
        }
    }

    function delete_file()
    {
        if( isset( $this->post[ 'lattach_id' ] ) && !empty( $this->post[ 'lattach_id' ] ) )
        {
            $image  = $this->global->getValueField( 'lumonata_attachment', 'lattach', 'lattach_id', $this->post[ 'lattach_id' ] );

            $result = parent::delete( 'lumonata_attachment', array( 'lattach_id' => $this->post[ 'lattach_id' ] ) );

            if( is_array( $result ) )
            {
                return json_encode( array( 'result' => 'error' ) );
            }
            else
            {
                if( empty( $image ) === false )
                {
                    $this->upload->upload_constructor( IMAGE_DIR . '/Uploads/' );
                    $this->upload->delete_file_thumb( $image );
                }

                return json_encode( array( 'result' => 'success' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
    }

    function order_file()
    {
        if( isset( $this->post[ 'image' ] ) && !empty( $this->post[ 'image' ] ) )
        {
            $error = 0;

            foreach( $this->post[ 'image' ] as $order_id => $attach_id )
            {
                $param  = array( 'lorder_id' => $order_id );
                $where  = array( 'lattach_id' => $attach_id );

                $result = parent::update( 'lumonata_attachment', $param, $where );

                if( is_array( $result ) )
                {
                    $error++;
                }
            }

            if( empty( $error ) == false )
            {
                return json_encode( array( 'result' => 'error', 'message' => 'Failed to reorder some image' ) );
            }
            else
            {
                return json_encode( array( 'result' => 'success' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'error', 'message' => 'No images was found to reorder' ) );
        }
    }

    function add_group()
    {
        if( isset( $this->post[ 'items' ] ) && !empty( $this->post[ 'items' ] ) )
        {
            $items  = json_decode( base64_decode( $this->post[ 'items' ] ), true );
            $object = json_decode( base64_decode( $this->post[ 'object' ] ), true );

            return $this->global->add_repeater_items( $items, $object );
        }
    }

    function sef_url( $string = '' )
    {
        $num = $this->global->getNumRows( 'lumonata_post', 'ltitle', $string );

        if( $num > 0 )
        {
            for( $i = 2; $i <= $num + 1; $i++ )
            {
                $sef = $this->global->sef_url( $string ) . '-' . $i;
                
                if( $this->global->getNumRows( 'lumonata_post', 'lsef_url', $sef ) < 1 )
                {
                    $sef = $sef;

                    break;
                }
            }
        }
        else
        {
            $sef = $this->global->sef_url( $string );
        }
        
        return $sef;
    }

    function sef_title( $string = '' )
    {
        $sef = $this->global->sef_url( $string, 'lumonata_post', 'lsef_url' );

        $s = 'SELECT COUNT(a.lpost_id) AS num FROM lumonata_post AS a WHERE a.lsef_url LIKE %s';
        $q = parent::prepare_query( $s, '%' . $sef . '%' );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $d = parent::fetch_assoc( $r );
            
            if( $d['num'] > 1 );
            {
                $sef = $sef . '-' . time();
            }
        }

        return $sef;
    }

    function switcher_language( $mod, $llang_id, $lref_id )
    {
        $s = 'SELECT a.llang_id FROM lumonata_post AS a WHERE a.lref_id IS NOT NULL AND a.lref_id = %d ORDER BY a.lorder_id';
        $q = parent::prepare_query( $s, $lref_id );
        $r = parent::query( $q );

        $translation = array();

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_assoc( $r ) )
            {
                array_push( $translation, $d[ 'llang_id' ] );
            }
        }

        if( empty( $lref_id ) )
        {
            return $this->global->get_switcher_language( $mod, 'lumonata_post', $llang_id, $lref_id, $translation, false );
        }
        else
        {
            return $this->global->get_switcher_language( $mod, 'lumonata_post', $llang_id, $lref_id, $translation, false, true );
        }
    }

    function custom_field( $mod )
    {
        $id = $this->fields[ 'lpost_id' ];
        $lg = $this->fields[ 'llang_id' ];

        //-- Custom Field
        $cparam = array(
            'types' => 1,
            'field' => array(
                array(
                    'id'      => 'page_template',
                    'name'    => 'page_template',
                    'label'   => 'Template',
                    'type'    => 'select',
                    'options' => array(
                        0 => 'Multiple Room',
                        1 => 'Boat Charter',
                        2 => 'Surf Camps',
                        3 => 'Entire Place'
                    ),
                    'attributes' => array(
                        'class' => 'chzn-select',
                        'autocomplete' => 'off'
                    )
                ),
                array(
                    'id'      => 'is_certified',
                    'name'    => 'is_certified',
                    'label'   => 'Certified?',
                    'type'    => 'select',
                    'options' => array(
                        0 => 'No',
                        1 => 'Yes'
                    ),
                    'attributes' => array(
                        'class' => 'chzn-select',
                        'autocomplete' => 'off'
                    )
                ),
            )
        );

        //-- Additional Field
        $aparam = array(
            array( 
                'group' => 'Base Information',
                'types' => 1,
                'field' => array(
                    array(
                        'id'      => 'currency',
                        'name'    => 'currency',
                        'label'   => 'Currency',
                        'type'    => 'select',
                        'options' => $this->global->get_currency_option(),
                        'attributes' => array(
                            'class' => 'chzn-select',
                            'autocomplete' => 'off',
                            'data-placeholder' => 'Choose Currency'
                        )
                    ),
                    array(
                        'id'    => 'skill_level',
                        'name'  => 'skill_level',
                        'label' => 'Skill Levels',
                        'type'  => 'select',
                        'multiple' => true,
                        'options'  => array(
                            1 => 'Beginner',
                            2 => 'Intermediate',
                            3 => 'Expert'
                        ),
                        'attributes' => array(
                            'data-placeholder' => 'Choose skill levels',
                            'class' => 'chzn-select',
                            'autocomplete' => 'off',
                            'style' => 'width:100%',
                        ),
                        'show_on' => array( 
                            'key'   => 'page_template', 
                            'value' => 0 
                        )
                    ),
                    array(
                        'id'    => 'title_part_1',
                        'name'  => 'title_part_1',
                        'label' => 'Title Part 1',
                        'desc'  => 'How would you best describe your listing?',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'placeholder' => 'Example: Tropical, Fun, Luxurious'
                        )
                    ),
                    array(
                        'id'    => 'title_part_2',
                        'name'  => 'title_part_2',
                        'label' => 'Title Part 2',
                        'desc'  => 'What is the closest beach or surf spot?',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'placeholder' => 'Example: Echo Beach, Batu Bolong'
                        )
                    ),
                    array(
                        'id'    => 'title_part_3',
                        'name'  => 'title_part_3',
                        'label' => 'Title Part 3',
                        'desc'  => 'What is your listing\'s most prominent feature?',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'placeholder' => 'Example: Big Pool, Family Friendly'
                        )
                    ),
                    array(
                        'id'    => 'title_part_4',
                        'name'  => 'title_part_4',
                        'label' => 'Title Part 4',
                        'desc'  => 'Where is the exact location of your listing?',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'placeholder' => 'Example:  Canggu, Seminyak'
                        )
                    ),
                    array(
                        'id'    => 'fb_link',
                        'name'  => 'fb_link',
                        'label' => 'Facebook URL',
                        'desc'  => 'Your property Facebook page (Optional)',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'website',
                        'name'  => 'website',
                        'label' => 'Website URL',
                        'desc'  => 'Your property website page (Optional)',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                           
                    array(
                        'id'      => 'commission',
                        'name'    => 'commission',
                        'label'   => 'Commission',
                        'default' => '15',
                        'type'    => 'radio',
                        'options' => array(
                            '15' => '15%',
                            '20' => '20%',
                            '25' => '25%',
                            '30' => '30%'
                        ),
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                           
                    array(
                        'id'    => 'price_type',
                        'name'  => 'price_type',
                        'label' => 'Pricing',
                        'type'  => 'radio',
                        'options' => array(
                            'Per Night',
                            'Per Package'
                        ),
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                          
                    array(
                        'id'    => 'policy',
                        'name'  => 'policy',
                        'label' => 'Policy',
                        'desc'  => 'Define your booking policy',
                        'type'  => 'radio',
                        'options' => array(
                            'Automatic',
                            'Manual'
                        ),
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                )
            ),
            array( 
                'group' => 'General Information',
                'types' => 1,
                'field' => array(
                    array(
                        'id'     => 'accommodation_image',
                        'name'   => 'accommodation_image',
                        'label'  => 'Featured/Cover Image',
                        'type'   => 'image',
                        'desc'   => 'Recommended size 300 * 300',
                        'btntxt' => 'Choose Image',
                        'attributes' => array(
                            'autocomplete'  => 'off'
                        )
                    ),
                    array(
                        'id'     => 'achievement_bg_image',
                        'name'   => 'achievement_bg_image',
                        'label'  => 'Achievement Image',
                        'type'   => 'image',
                        'desc'   => 'Recommended size 540 * 180',
                        'btntxt' => 'Choose Image',
                        'attributes' => array(
                            'autocomplete'  => 'off'
                        )
                    ),
                    array(
                        'id'    => 'accommodation_gallery',
                        'name'  => 'accommodation_gallery',
                        'label' => 'Gallery',
                        'type'  => 'gallery',
                        'desc'  => 'Recommended size 1920 * 768'
                    ),
                    array(
                        'id'     => 'hightlight',
                        'name'   => 'hightlight',
                        'label'  => 'Hightlight',
                        'type'   => 'repeater',
                        'option' => array(
                            'title'       => 'Item',
                            'add_text'    => 'Add Item',
                            'remove_text' => 'Delete Item',
                        ),
                        'items'  => array(
                            array(
                                'id'    => 'name',
                                'name'  => 'name',
                                'label' => 'Name',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            )
                        )
                    ),
                    array(
                        'id'    => 'accommodation_address',
                        'name'  => 'accommodation_address',
                        'label' => 'Address',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'      => 'accommodation_continent',
                        'name'    => 'accommodation_continent',
                        'label'   => 'Continent',
                        'type'    => 'select',
                        'options' => $this->continent_option( $lg ),
                        'attributes' => array(
                            'class' => 'chzn-select',
                            'autocomplete' => 'off',
                            'data-placeholder' => 'Choose continent'
                        )
                    ),
                    array(
                        'id'      => 'accommodation_country',
                        'name'    => 'accommodation_country',
                        'label'   => 'Country/Region',
                        'type'    => 'select',
                        'options' => $this->country_option( $lg ),
                        'attributes' => array(
                            'class' => 'chzn-select',
                            'autocomplete' => 'off',
                            'data-placeholder' => 'Choose country/region'
                        )
                    ),
                    array(
                        'id'      => 'accommodation_state',
                        'name'    => 'accommodation_state',
                        'label'   => 'State/Province',
                        'type'    => 'select',
                        'options' => $this->state_option( $lg ),
                        'attributes' => array(
                            'class' => 'chzn-select',
                            'autocomplete' => 'off',
                            'data-placeholder' => 'Choose state/province'
                        )
                    ),
                    array(
                        'id'      => 'accommodation_city',
                        'name'    => 'accommodation_city',
                        'label'   => 'City/Area',
                        'type'    => 'select',
                        'options' => $this->city_option( $lg ),
                        'attributes' => array(
                            'class' => 'chzn-select',
                            'autocomplete' => 'off',
                            'data-placeholder' => 'Choose city/area'
                        )
                    ),
                    array(
                        'id'    => 'post_code',
                        'name'  => 'post_code',
                        'label' => 'Post code',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'map_coordinate',
                        'name'  => 'map_coordinate',
                        'label' => 'Coordinate on Map',
                        'type'  => 'text',
                        'desc'  => 'Use comma separator, ex: -8.7155833,115.1796264',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'closer_airport_name',
                        'name'  => 'closer_airport_name',
                        'label' => 'Closer Airport Name',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'closer_airport_city',
                        'name'  => 'closer_airport_city',
                        'label' => 'Closer Airport City',
                        'type'  => 'text',
                        'desc'  => 'Use 3 letter code, ex: DPS',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'arrival_by_plane',
                        'name'  => 'arrival_by_plane',
                        'label' => 'Arrival by Plane',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    ),
                    array(
                        'id'    => 'arrival_by_bus',
                        'name'  => 'arrival_by_bus',
                        'label' => 'Arrival by Bus',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    ),
                    array(
                        'id'    => 'arrival_by_train',
                        'name'  => 'arrival_by_train',
                        'label' => 'Arrival by Train',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    ),
                    array(
                        'id'    => 'arrival_by_boat',
                        'name'  => 'arrival_by_boat',
                        'label' => 'Arrival by Boat',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    ),                            
                    array(
                        'id'    => 'amenities',
                        'name'  => 'amenities',
                        'label' => 'Amenities',
                        'type'  => 'checkbox',
                        'options'  => $this->terms_options( array(
                            'lgroup'   => 'accommodation',
                            'lrule'    => 'amenities',
                            'llang_id' => $lg,
                            'lstatus'  => 1
                        )),
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                            
                    array(
                        'id'    => 'whats_included_stay',
                        'name'  => 'whats_included_stay',
                        'label' => 'Stay',
                        'type'  => 'checkbox',
                        'options'  => $this->terms_options( array(
                            'lgroup'     => 'inclusions',
                            'lrule'      => 'stay',
                            'llang_id'   => $lg,
                            'lstatus'    => 1
                        )),
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                           
                    array(
                        'id'    => 'whats_included_transport',
                        'name'  => 'whats_included_transport',
                        'label' => 'Transport',
                        'type'  => 'checkbox',
                        'options'  => $this->terms_options( array(
                            'lgroup'     => 'inclusions',
                            'lrule'      => 'transport',
                            'llang_id'   => $lg,
                            'lstatus'    => 1
                        )),
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                           
                    array(
                        'id'    => 'whats_included_activities',
                        'name'  => 'whats_included_activities',
                        'label' => 'Activities',
                        'type'  => 'checkbox',
                        'options'  => $this->terms_options( array(
                            'lgroup'     => 'inclusions',
                            'lrule'      => 'activities',
                            'llang_id'   => $lg,
                            'lstatus'    => 1
                        )),
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'food',
                        'name'  => 'food',
                        'label' => 'Short Introduction',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off'
                        )
                    ),         
                    array(
                        'id'    => 'accommodation_text',
                        'name'  => 'accommodation_text',
                        'label' => 'Accommodation',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off'
                        )
                    ),                           
                    array(
                        'id'    => 'food_meals',
                        'name'  => 'food_meals',
                        'label' => 'Meals',
                        'type'  => 'checkbox',
                        'options'  => $this->terms_options( array(
                            'lgroup'     => 'inclusions',
                            'lrule'      => 'meals',
                            'llang_id'   => $lg,
                            'lstatus'    => 1
                        )),
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                         
                    array(
                        'id'    => 'food_dietary',
                        'name'  => 'food_dietary',
                        'label' => 'Dietary Needs',
                        'type'  => 'checkbox',
                        'options'  => $this->terms_options( array(
                            'lgroup'     => 'inclusions',
                            'lrule'      => 'dietary',
                            'llang_id'   => $lg,
                            'lstatus'    => 1
                        )),
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                )
            ),
            array( 
                'group' => 'Activity',
                'types' => 1,
                'field' => array(
                    array(
                        'id'     => 'things_to_do',
                        'name'   => 'things_to_do',
                        'label'  => 'Things To Do',
                        'type'   => 'repeater',
                        'option' => array(
                            'title'       => 'To do',
                            'add_text'    => 'Add',
                            'remove_text' => 'Delete',
                        ),
                        'items'  => array(                            
                            array(
                                'id'    => 'to_do_title',
                                'name'  => 'to_do_title',
                                'label' => 'Title',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'to_do_desc',
                                'name'  => 'to_do_desc',
                                'label' => 'Description',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            ),             
                            array(
                                'id'     => 'to_do_img',
                                'name'   => 'to_do_img',
                                'label'  => 'Images',
                                'type'   => 'image',
                                'desc'   => 'Recommended size 450 * 750',
                                'attributes' => array(
                                    'autocomplete'  => 'off'
                                )
                            ),
                        )
                    ),
                    array(
                        'id'    => 'surfing_info',
                        'name'  => 'surfing_info',
                        'label' => 'Surfing Info',
                        'type'  => 'repeater',
                        'option' => array(
                            'title'       => 'Info',
                            'add_text'    => 'Add Info',
                            'remove_text' => 'Delete Info',
                        ),
                        'items'  => array(                            
                            array(
                                'id'    => 'surf_info_title',
                                'name'  => 'surf_info_title',
                                'label' => 'Surf Spot',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),                            
                            array(
                                'id'    => 'surf_info_desc',
                                'name'  => 'surf_info_desc',
                                'label' => 'Description',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            ),
                        )
                    ),
                )
            ),
            array( 
                'group' => 'Booking Settings',
                'types' => 1,
                'field' => array(
                    array(
                        'id'    => 'room_price',
                        'name'  => 'room_price',
                        'label' => 'Price',
                        'type'  => 'text',
                        'attributes' => array(
                            'class'        => 'text text-number',
                            'autocomplete' => 'off',
                            'data-a-sep'   => '.',
                            'data-a-dec'   => ',',
                            'data-m-dec'   => '0'
                        )
                    ),  
                    array(
                        'id'    => 'min_night',
                        'name'  => 'min_night',
                        'label' => 'Minimum Nights',
                        'type'  => 'text',
                        'attributes' => array(
                            'class'        => 'text text-number',
                            'autocomplete' => 'off',
                            'data-a-sep'   => '.',
                            'data-a-dec'   => ',',
                            'data-m-dec'   => '0'
                        ),
                    ),
                    array(
                        'id'    => 'early_bird_discount',
                        'name'  => 'early_bird_discount',
                        'label' => 'Early Bird Discount',
                        'type'  => 'text',
                        'attributes' => array(
                            'class'        => 'text text-number',
                            'autocomplete' => 'off',
                            'data-a-sign'  => '%',
                            'data-a-pos'   => 's',
                            'data-a-sep'   => '.',
                            'data-a-dec'   => ',',
                            'data-m-dec'   => '0'
                        )
                    ),
                    array(
                        'id'    => 'last_minute_discount',
                        'name'  => 'last_minute_discount',
                        'label' => 'Last Minute Discount',
                        'type'  => 'text',
                        'attributes' => array(
                            'class'        => 'text text-number',
                            'autocomplete' => 'off',
                            'data-a-sign'  => '%',
                            'data-a-pos'   => 's',
                            'data-a-sep'   => '.',
                            'data-a-dec'   => ',',
                            'data-m-dec'   => '0'
                        ),
                    ),
                    array(
                        'id'    => 'monthly_discount',
                        'name'  => 'monthly_discount',
                        'label' => 'Monthly Discount',
                        'type'  => 'text',
                        'attributes' => array(
                            'class'        => 'text text-number',
                            'autocomplete' => 'off',
                            'data-a-sign'  => '%',
                            'data-a-pos'   => 's',
                            'data-a-sep'   => '.',
                            'data-a-dec'   => ',',
                            'data-m-dec'   => '0'
                        ),
                    ),
                    array(
                        'id'    => 'bi_weekly_discount',
                        'name'  => 'bi_weekly_discount',
                        'label' => 'Bi-Weekly Discount',
                        'type'  => 'text',
                        'attributes' => array(
                            'class'        => 'text text-number',
                            'autocomplete' => 'off',
                            'data-a-sign'  => '%',
                            'data-a-pos'   => 's',
                            'data-a-sep'   => '.',
                            'data-a-dec'   => ',',
                            'data-m-dec'   => '0'
                        ),
                    ),  
                    array(
                        'id'    => 'min_age',
                        'name'  => 'min_age',
                        'label' => 'Minimum Age',
                        'type'  => 'text',
                        'attributes' => array(
                            'class'        => 'text text-number',
                            'autocomplete' => 'off',
                            'data-a-sep'   => '.',
                            'data-a-dec'   => ',',
                            'data-m-dec'   => '0'
                        ),
                    ),
                    array(
                        'id'    => 'max_age',
                        'name'  => 'max_age',
                        'label' => 'Maximum Age',
                        'type'  => 'text',
                        'attributes' => array(
                            'class'        => 'text text-number',
                            'autocomplete' => 'off',
                            'data-a-sep'   => '.',
                            'data-a-dec'   => ',',
                            'data-m-dec'   => '0'
                        ),
                    ),                           
                    array(
                        'id'    => 'check_in_time',
                        'name'  => 'check_in_time',
                        'label' => 'Check In',
                        'type'  => 'select',
                        'options'  => $this->time_options(),
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                     
                    array(
                        'id'    => 'check_out_time',
                        'name'  => 'check_out_time',
                        'label' => 'Check Out',
                        'type'  => 'select',
                        'options'  => $this->time_options(),
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                            
                    array(
                        'id'    => 'payment_method',
                        'name'  => 'payment_method',
                        'label' => 'Payout',
                        'type'  => 'radio',
                        'options' => array(
                            'Bank Transfer',
                            'Upon Arrival'
                        ),
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                       
                    array(
                        'id'    => 'bank_detail',
                        'name'  => 'bank_detail',
                        'label' => 'Bank Transfer Detail',
                        'type'  => 'textarea',
                        'desc'  => 'Please fill with your bank account data, if you choose "Bank Transfer" as payout method',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                                
                    array(
                        'id'    => 'payment_policy',
                        'name'  => 'payment_policy',
                        'label' => 'Payout Policy',
                        'type'  => 'radio',
                        'options' => array(
                            'Immediately after reservation',
                            '2 months prior to arrival',
                            '1 month prior to arrival',
                            'Upon arrival',
                        ),
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                           
                    array(
                        'id'    => 'cancellation_policy',
                        'name'  => 'cancellation_policy',
                        'label' => 'Cancellation Policy',
                        'type'  => 'radio',
                        'options' => array(
                            'Flexible',
                            'Moderate',
                            'Strict',
                        ),
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                )
            ),
            array( 
                'group' => 'Nomad Partner & Staff',
                'types' => 1,
                'field' => array( 
                    array(
                        'id'     => 'partner_img',
                        'name'   => 'partner_img',
                        'label'  => 'Cover Image',
                        'type'   => 'image',
                        'desc'   => 'Recommended size 300 * 300',
                        'btntxt' => 'Choose Image',
                        'attributes' => array(
                            'autocomplete'  => 'off'
                        )
                    ),                           
                    array(
                        'id'    => 'partner_since',
                        'name'  => 'partner_since',
                        'label' => 'Partner Since',
                        'type'  => 'text',
                        'desc'  => 'Fill with year, ex: 2021',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'partner_location',
                        'name'  => 'partner_location',
                        'label' => 'Location',
                        'type'  => 'text',
                        'desc'  => 'Fill with partner location, ex: Established at City, Destination',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'     => 'staff',
                        'name'   => 'staff',
                        'label'  => 'Staff',
                        'type'   => 'repeater',
                        'option' => array(
                            'title'       => 'Staff',
                            'add_text'    => 'Add Staff',
                            'remove_text' => 'Delete Staff',
                        ),
                        'items'  => array(                            
                            array(
                                'id'    => 'staff_name',
                                'name'  => 'staff_name',
                                'label' => 'Name',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'staff_performance',
                                'name'  => 'staff_performance',
                                'label' => 'Performance',
                                'type'  => 'text',
                                'desc'  => 'Please input value from 1 - 5',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'staff_desc',
                                'name'  => 'staff_desc',
                                'label' => 'Description',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            ),             
                            array(
                                'id'     => 'staff_avatar',
                                'name'   => 'staff_avatar',
                                'label'  => 'Photos',
                                'type'   => 'image',
                                'btntxt' => 'Choose Photo',
                                'desc'   => 'Recommended size 300 * 300',
                                'attributes' => array(
                                    'autocomplete'  => 'off'
                                )
                            ),
                        )
                    ),
                )
            ),
        );

        $this->actions->add_actions( 'accommodation_custom_field', $this->global->init_custom_field( $id, $mod, $cparam, $lg ) );
        $this->actions->add_actions( 'accommodation_additional_field', $this->global->init_custom_field( $id, $mod, $aparam, $lg ) );
    }

    function language()
    {
        if( isset( $_GET[ 'lang' ] ) && $_GET[ 'lang' ] != '' )
        {
            $this->lang = $_GET[ 'lang' ];
        }
        else
        {
            $this->lang = $this->global->getSettingValue( 'llanguage' );
        }
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();
        $this->ref     = null;
        $this->valid   = true;

        if( isset( $_GET[ 'id' ] ) )
        {
            $s = 'SELECT * FROM lumonata_post AS a WHERE a.lpost_id = %d';
            $q = parent::prepare_query( $s, $_GET[ 'id' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $d = parent::fetch_array( $r );

                $fields = array(
                    'lcreated_by'   => $d[ 'lcreated_by' ],
                    'lusername'     => $d[ 'lusername' ],
                    'llang_id'      => $d[ 'llang_id' ],
                    'lref_id'       => $d[ 'lref_id' ],
                    'lpost_id'      => $d[ 'lpost_id' ],
                    'lcreated_date' => $d[ 'lcreated_date' ],
                    'ldlu'          => $d[ 'ldlu' ],
                    'ltitle'        => $d[ 'ltitle' ],
                    'ldescription'  => $d[ 'ldescription' ],
                    'lmeta_title'   => $d[ 'lmeta_title' ],
                    'lmeta_key'     => $d[ 'lmeta_key' ],
                    'lmeta_desc'    => $d[ 'lmeta_desc' ],
                    'limage'        => $d[ 'limage' ],
                    'lsef_url'      => $d[ 'lsef_url' ],
                    'lorder_id'     => $d[ 'lorder_id' ],
                    'lparent_id'    => $d[ 'lparent_id' ],
                    'ltype'         => $d[ 'ltype' ],
                    'lstatus'       => $d[ 'lstatus' ]
                );

                if( is_null( $d[ 'lref_id' ] ) )
                {
                    $this->ref = $d[ 'lpost_id' ];
                }
                else
                {
                    $this->ref = $d[ 'lref_id' ];
                }
            }
            else
            {
                $this->valid = false;
            }
            
            //-- GET post term relationship
            $s = 'SELECT a.lterm_id, b.lrule FROM lumonata_post_relationship AS a LEFT JOIN lumonata_post_terms AS b ON a.lterm_id = b.lterm_id WHERE a.lpost_id = %d';
            $q = parent::prepare_query( $s, $_GET[ 'id' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                while( $d = parent::fetch_array( $r ) )
                {
                    $terms[ $d[ 'lrule' ] ][] = $d[ 'lterm_id' ];
                }

                $terms = array_merge( array( 
                    'accommodation_type' => '', 
                    'surf_trip'          => '' 
                ), $terms );
            }
            else
            {
                $terms = array( 
                    'accommodation_type' => '', 
                    'surf_trip'          => '' 
                );
            }
        }
        else
        {
            if( isset( $_GET[ 'ref' ] ) )
            {
                $this->ref = $_GET[ 'ref' ];
            }
            
            $fields = array(
                'lcreated_by'   => $this->sess[ 'username' ],
                'lusername'     => $this->sess[ 'username' ],
                'llang_id'      => $this->lang,
                'lref_id'       => $this->ref,
                'lpost_id'      => time(),
                'lcreated_date' => time(),
                'ldlu'          => time(),
                'ltype'         => 'accommodation',
                'ltitle'        => '',
                'ldescription'  => '',
                'lmeta_title'   => '',
                'lmeta_key'     => '',
                'lmeta_desc'    => '',
                'limage'        => '',
                'lsef_url'      => '',
                'lorder_id'     => 0,
                'lparent_id'    => 0,
                'lstatus'       => 1
            );

            if( isset( $_GET[ 'ref' ] ) )
            {
                $s = 'SELECT * FROM lumonata_post AS a WHERE a.lpost_id = %d';
                $q = parent::prepare_query( $s, $_GET[ 'ref' ] );
                $r = parent::query( $q );

                if( parent::num_rows( $r ) > 0 )
                {
                    $d = parent::fetch_array( $r );

                    $fields = array_merge( $fields, array( 'ltitle' => $d[ 'ltitle' ] ) );
                }
            }

            $terms = array( 
                'accommodation_type' => '', 
                'surf_trip'          => '' 
            );
        }

        if( empty( $_POST ) )
        {
            if( empty( $this->notif ) === false && isset( $this->notif[ 'post' ] ) )
            {
                $this->fields = array_merge( $fields, $this->notif[ 'post' ] );
            }
            else
            {
                if( isset( $fields ) )
                {
                    $this->fields = $fields;   
                }
            }

            $this->terms = $terms;

            $this->post = array();
        }
        else
        {
            if( isset( $_POST[ 'fields' ] ) )
            {
                $this->fields = array_merge( $fields, $_POST[ 'fields' ] );
            }
            else
            {
                $this->fields = $fields;
            }

            if( isset( $_POST[ 'terms' ] ) )
            {
                $this->terms = array_merge( $terms, $_POST[ 'terms' ] );
            }
            else
            {
                $this->terms = $terms;
            }

            $this->post = array_diff_key( $_POST, array_flip( array( 'fields', 'terms' ) ) );
        }
    }

    function accommodation_type( $term_id = '' )
    {
        if( empty( $term_id  ) )
        {
            return '-';
        }
        else
        {
            $s = 'SELECT a.lname FROM lumonata_post_terms AS a WHERE a.lterm_id = %d';
            $q = parent::prepare_query( $s, $term_id );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $d = parent::fetch_array( $r );

                return $d[ 'lname' ];
            }
        }
    }

    function accommodation_surf_trip( $term_id = '' )
    {
        if( empty( $term_id  ) )
        {
            return '-';
        }
        else
        {
            $q = 'SELECT GROUP_CONCAT( a.lname SEPARATOR ", " ) AS lname FROM lumonata_post_terms AS a WHERE a.lterm_id IN( ' . $term_id . ' )';
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $d = parent::fetch_array( $r );

                return $d[ 'lname' ];
            }
        }
    }

    function schedule_trip_departure( $destination = '' )
    {
        if( empty( $destination  ) )
        {
            return '-';
        }
        else
        {
            return $this->global->getValueField( 'lumonata_post', 'ltitle', 'lpost_id', $destination );
        }
    }

    function schedule_trip_itinerary( $itinerary = '' )
    {
        if( empty( $itinerary  ) )
        {
            return '-';
        }
        else
        {
            $itinerary = json_decode( $itinerary, true );

            if( $itinerary === null || json_last_error() !== JSON_ERROR_NONE )
            {
                return '-';
            }
            else
            {
                $list = array();

                foreach( $itinerary as $d )
                {
                    $list[] = $this->global->getValueField( 'lumonata_post', 'ltitle', 'lpost_id', $d );
                }

                return implode( '<br/>', $list );
            }
        }
    }

    function continent_option( $lang = '' )
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = "destination" AND a.llang_id = %d AND ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d';
        $q = parent::prepare_query( $s, $lang, 'destination_type', 0 );
        $r = parent::query( $q );

        $option = array();

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_array( $r ) )
            {
                $option[ $d[ 'lpost_id' ] ] = $d[ 'ltitle' ];
            }
        }

        return $option;
    }

    function country_option( $lang = '' )
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = "destination" AND a.llang_id = %d AND ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d';
        $q = parent::prepare_query( $s, $lang, 'destination_type', 1 );
        $r = parent::query( $q );

        $option = array();

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_array( $r ) )
            {
                $option[ $d[ 'lpost_id' ] ] = $d[ 'ltitle' ];
            }
        }

        return $option;
    }

    function state_option( $lang = '' )
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = "destination" AND a.llang_id = %d AND ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d';
        $q = parent::prepare_query( $s, $lang, 'destination_type', 2 );
        $r = parent::query( $q );

        $option = array();

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_array( $r ) )
            {
                $option[ $d[ 'lpost_id' ] ] = $d[ 'ltitle' ];
            }
        }

        return $option;
    }

    function city_option( $lang = '' )
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = "destination" AND a.llang_id = %d AND ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d';
        $q = parent::prepare_query( $s, $lang, 'destination_type', 3 );
        $r = parent::query( $q );

        $option = array();

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_array( $r ) )
            {
                $option[ $d[ 'lpost_id' ] ] = $d[ 'ltitle' ];
            }
        }

        return $option;
    }

    function terms_options( $conditions = array() )
    {
        //-- PREPARE parameter
        $w = array();

        if( is_array( $conditions ) && empty( $conditions ) === false )
        {
            foreach( $conditions as $field => $value )
            {
                if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
                {
                    $field .= ' = ';
                }

                $w[] = parent::prepare_query( $field . '%s', $value );
            }
        }

        if( empty( $w ) === false )
        {
            $where = ' WHERE ' . implode( ' AND ', $w );
        }
        else
        {
            $where = '';
        }

        $q = 'SELECT a.lterm_id, a.lname FROM lumonata_post_terms AS a' . $where;
        $r = parent::query( $q );

        $option = array();

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_array( $r ) )
            {
                $option[ $d[ 'lterm_id' ] ] = $d[ 'lname' ];
            }
        }

        return $option;
    }

    function time_options()
    {
        $tstart = strtotime( '00:00' );
        $tend   = strtotime( '23:30' );
        $option = array();
        $tnow   = $tstart;

        while( $tnow <= $tend ) 
        {
            $tformat = date( 'H:i', $tnow );
            $tnow    = strtotime( '+30 minutes', $tnow );

            $option[ $tformat ] = $tformat;
        }

        return $option;
    }

    function get_agent_option( $username = '' )
    {
        $s = 'SELECT * FROM lumonata_user AS a WHERE a.lusertype_id = 2 ORDER BY a.lname';
        $q = parent::prepare_query( $s );

        return $this->global->set_option( $q, 'lusername', 'lname', $username, false );
    }

    function get_destination_option( $lpost_id = '' )
    {
        if( is_array( $lpost_id ) && !empty( $lpost_id ) )
        {
            $q = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = "destination" AND a.lstatus = 1 ORDER BY FIELD( a.lpost_id, ' . implode( ',', $lpost_id ) . ' )';
        }
        else
        {
            $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = %s AND a.lstatus = %d';
            $q = parent::prepare_query( $s, 'destination', 1 );
        }

        return $this->global->set_option( $q, 'lpost_id', 'ltitle', $lpost_id, true, null, null );
    }

    function get_continent_option( $continent_id = '' )
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = "destination" AND a.llang_id = %d AND ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d';
        $q = parent::prepare_query( $s, $this->lang, 'destination_type', 0 );

        return $this->global->set_option( $q, 'lpost_id', 'ltitle', $continent_id, false );
    }

    function get_country_option( $country_id = '' )
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = "destination" AND a.llang_id = %d AND ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d';
        $q = parent::prepare_query( $s, $this->lang, 'destination_type', 1 );

        return $this->global->set_option( $q, 'lpost_id', 'ltitle', $country_id, false );
    }

    function get_state_option( $city_id = '' )
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = "destination" AND a.llang_id = %d AND ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d';
        $q = parent::prepare_query( $s, $this->lang, 'destination_type', 2 );

        return $this->global->set_option( $q, 'lpost_id', 'ltitle', $city_id, false );
    }

    function get_city_option( $area_id = '' )
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = "destination" AND a.llang_id = %d AND ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d';
        $q = parent::prepare_query( $s, $this->lang, 'destination_type', 3 );

        return $this->global->set_option( $q, 'lpost_id', 'ltitle', $area_id, false );
    }

    function get_accommodation_option( $lacco_id = NULL )
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = %s AND a.lstatus = %d AND a.llang_id = %d';
        $q = parent::prepare_query( $s, 'accommodation', 1, $this->lang );

        return $this->global->set_option( $q, 'lpost_id', 'ltitle', $lacco_id, false );
    }

    function get_accommodation_type_option( $lacco_type = NULL )
    {
        $s = 'SELECT a.lterm_id, a.lname FROM lumonata_post_terms AS a WHERE a.lrule = %s AND a.lgroup = %s AND a.lstatus = %d AND a.llang_id = %d';
        $q = parent::prepare_query( $s, 'accommodation_type', 'accommodation', 1, $this->lang );

        return $this->global->set_option( $q, 'lterm_id', 'lname', $lacco_type, false );
    }

    function get_accommodation_surf_trip_option( $lsurf_trip = NULL )
    {
        $s = 'SELECT a.lterm_id, a.lname FROM lumonata_post_terms AS a WHERE a.lrule = %s AND a.lgroup = %s AND a.lstatus = %d AND a.llang_id = %d';
        $q = parent::prepare_query( $s, 'surf_trip', 'accommodation', 1, $this->lang );

        return $this->global->set_option( $q, 'lterm_id', 'lname', $lsurf_trip, false );
    }

    function get_accommodation_room_type_option( $lpost_id, $lroom_type = NULL )
    {
        $s = 'SELECT b.* FROM lumonata_post_relationship AS a LEFT JOIN lumonata_post_terms AS b ON a.lterm_id = b.lterm_id WHERE a.lpost_id = %d AND b.lrule = %s AND b.lgroup = %s AND b.lstatus = %d AND b.llang_id = %d';
        $q = parent::prepare_query( $s, $lpost_id, 'room_type', 'accommodation', 1, $this->lang );

        return $this->global->set_option( $q, 'lterm_id', 'lname', $lroom_type, false );
    }

    function get_accommodation_rates_option( $lpost_id, $lrate_id = NULL )
    {
        $s = 'SELECT CONCAT( a.lrate_id, "|", a.lrate_price ) AS lrate_value, a.lrate_name FROM lumonata_rates AS a WHERE a.lpost_id = %d AND a.lstatus = %d';
        $q = parent::prepare_query( $s, $lpost_id, 1 );

        return $this->global->set_option( $q, 'lrate_value', 'lrate_name', $lrate_id, false );
    }

    function get_period_option( $lpost_id, $date = NULL )
    {
        $s = 'SELECT DATE_FORMAT( FROM_UNIXTIME( a.lcheck_in ), %s ) AS period FROM lumonata_schedules AS a WHERE a.lpost_id = %d GROUP BY YEAR( FROM_UNIXTIME( a.lcheck_in ) ) DESC, MONTH( FROM_UNIXTIME( a.lcheck_in ) ) ASC';
        $q = parent::prepare_query( $s, '%M, %Y', $lpost_id );

        return $this->global->set_option( $q, 'period', 'period', $date, false );
    }

    function get_status_option( $lstatus = NULL )
    {
        return $this->global->set_static_option( array( 'Inactive', 'Active' ), $lstatus, false );
    }

    function get_language_option( $mod )
    {
        return $this->global->set_filter_language_option( $mod, $this->lang );
    }

    function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'load' )
            {
                echo $this->load( $mod, $usertype );
            }

            if( $this->post[ 'pKEY' ] == 'do_change_status' )
            {
                echo $this->do_change_status();
            }

            if( $this->post[ 'pKEY' ] == 'do_delete' )
            {
                echo $this->do_delete( $mod );
            }

            if( $this->post[ 'pKEY' ] == 'order_file' )
            {
                echo $this->order_file();
            }

            if( $this->post[ 'pKEY' ] == 'delete_file' )
            {
                echo $this->delete_file();
            }

            if( $this->post[ 'pKEY' ] == 'upload_file' )
            {
                echo $this->upload_file( $mod );
            }

            if( $this->post[ 'pKEY' ] == 'add_group' )
            {
                echo $this->add_group();
            }

            if( $this->post[ 'pKEY' ] == 'popup_availability' )
            {
                echo $this->popup_availability();
            }

            if( $this->post[ 'pKEY' ] == 'availability_calendar' )
            {
                echo $this->availability_calendar();
            }

            if( $this->post[ 'pKEY' ] == 'update_availability' )
            {
                echo $this->update_availability();
            }
        }

        exit;
    }
}

?> 