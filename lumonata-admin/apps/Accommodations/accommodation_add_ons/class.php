<?php

class accommodation_add_ons extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->upload   = new upload();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }

    function view( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $this->global->getPViewBoth( $mod, $usertype ) )
        {
            $this->template->set_var( 'mod', $mod );
            $this->template->set_var( 'lang', $this->lang );
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'limit', $this->global->getSettingValue( 'list_admin' ) );

            $this->template->set_var( 'lstatus', $this->get_status_option() );
            $this->template->set_var( 'llang_id', $this->get_language_option( $mod ) );
            $this->template->set_var( 'laccommodation', $this->get_accommodation_option( null, $this->lang ) );

            $this->template->set_var( 'add_url', $this->global->getAddUrl( $mod, $usertype ) );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Accommodations/accommodation_add_ons/view.js' ) );

            $this->template->Parse( 'vC', 'viewContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function insert( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->global->getPInsertBoth( $mod, $usertype ) )
        {
            //-- INIT custom field
            $this->custom_field( $mod );

            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->create( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'lsef', $lsef );
            $this->template->set_var( 'lname', $lname );
            $this->template->set_var( 'lterm_id', $lterm_id );
            $this->template->set_var( 'llang_id', $llang_id );
            $this->template->set_var( 'lorder_id', $lorder_id );
            $this->template->set_var( 'lcreated_by', $lcreated_by );
            $this->template->set_var( 'ldescription', $ldescription );
            $this->template->set_var( 'switcher_lang', $this->switcher_language( $mod, $llang_id, $this->ref ) );
            $this->template->set_var( 'laccommodation', $this->get_accommodation_option( $this->apps[ 'laccommodation' ], $llang_id ) );

            $this->template->set_var( 'lmeta_title', $lmeta_title );
            $this->template->set_var( 'lmeta_desc', $lmeta_desc );
            $this->template->set_var( 'lmeta_key', $lmeta_key );
            
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'lcustom', $this->actions->attemp_actions( 'accommodation_add_ons_custom_field' ) );
            $this->template->set_var( 'ladditional', $this->actions->attemp_actions( 'accommodation_add_ons_additional_field' ) );

            $this->template->set_var( 'site_url', SITE_URL );
            $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
            $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/blueimp-file-upload@10.32.0/js/jquery.fileupload.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/tinymce@5.10.2/tinymce.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( '//' . ADMIN_THEME_URL . '/assets/dropbox.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Accommodations/accommodation_add_ons/form.js' ) );

            $this->template->Parse( 'fC', 'formContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function edit( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->valid )
        {
            if( $this->global->getPEditBoth( $mod, $usertype ) )
            {
                //-- INIT custom field
                $this->custom_field( $mod );

                //-- EXECUTE update action
                if( isset( $this->post[ 'save' ] ) )
                {
                    $this->change( $mod );
                }

                //-- EXTRACT fields
                extract( $this->fields );

                $this->template->set_var( 'lsef', $lsef );
                $this->template->set_var( 'lname', $lname );
                $this->template->set_var( 'lterm_id', $lterm_id );
                $this->template->set_var( 'llang_id', $llang_id );
                $this->template->set_var( 'lorder_id', $lorder_id );
                $this->template->set_var( 'lcreated_by', $lcreated_by );
                $this->template->set_var( 'ldescription', $ldescription );
                $this->template->set_var( 'switcher_lang', $this->switcher_language( $mod, $llang_id, $this->ref ) );
                $this->template->set_var( 'laccommodation', $this->get_accommodation_option( $this->apps[ 'laccommodation' ], $llang_id ) );

                $this->template->set_var( 'lmeta_title', $lmeta_title );
                $this->template->set_var( 'lmeta_desc', $lmeta_desc );
                $this->template->set_var( 'lmeta_key', $lmeta_key );

                $this->template->set_var( 'message', $this->message );
                $this->template->set_var( 'lcustom', $this->actions->attemp_actions( 'accommodation_add_ons_custom_field' ) );
                $this->template->set_var( 'ladditional', $this->actions->attemp_actions( 'accommodation_add_ons_additional_field' ) );

                $this->template->set_var( 'site_url', SITE_URL );
                $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
                $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
                $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/blueimp-file-upload@10.32.0/js/jquery.fileupload.min.js' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/tinymce@5.10.2/tinymce.min.js' ) );

                $this->actions->add_actions( 'css', $this->global->get_css( '//' . ADMIN_THEME_URL . '/assets/dropbox.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Accommodations/accommodation_add_ons/form.js' ) );

                $this->template->Parse( 'fC', 'formContent', true );
            }
            else
            {
                $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
            }
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NOT_FOUND ) );
        }
        
        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function load( $mod, $usertype )
    {
        $cols  = array(
            1 => 'a.lname',
            3 => 'a.lstatus'
        );

        //-- Query Limit
        $limit = parent::prepare_query( ' LIMIT ' . $this->post[ 'start' ] . ', ' . $this->post[ 'length' ] );
    
        //-- Query Order By
        if( empty( $this->post[ 'order' ] ) )
        {
            $order = parent::prepare_query( ' ORDER BY a.llang_id, a.lorder_id ASC' );
        }
        else
        {
            $order = array();

            foreach( $this->post[ 'order' ] as $i => $od )
            {
                if( isset( $cols[ $od[ 'column' ] ] ) )
                {
                    $order[] = $cols[ $od[ 'column' ] ] . ' ' . $od[ 'dir' ];
                }
            }

            if( empty( $order ) )
            {
                $order = parent::prepare_query( ' ORDER BY a.llang_id, a.lorder_id ASC' );
            }
            else
            {
                $order = parent::prepare_query( ' ORDER BY ' . implode( ', ', $order ) );
            }
        }

        //-- Query Additional Where
        $w = array();

        if( $this->post[ 'laccommodation' ] != '' )
        {
            $w[] = parent::prepare_query( 'laccommodation = %d', $this->post[ 'laccommodation' ] );
        }

        if( $this->post[ 'llang_id' ] != '' )
        {
            $w[] = parent::prepare_query( 'a.llang_id = %d', $this->post[ 'llang_id' ] );
        }

        if( $this->post[ 'lstatus' ] != '' )
        {
            $w[] = parent::prepare_query( 'a.lstatus = %d', $this->post[ 'lstatus' ] );
        }
        else
        {
            $w[] = parent::prepare_query( 'a.lstatus <> %d', 2 );
        }

        if( $this->post[ 'search' ][ 'value' ] != '' )
        {
            $s = array();

            foreach( $cols as $col )
            {
                $s[] = parent::prepare_query( $col . ' LIKE %s', '%' . $this->post[ 'search' ][ 'value' ] . '%' );
            }

            $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
        }

        if( empty( $w ) === false )
        {
            $having = ' HAVING ' . implode( ' AND ', $w );
        }
        else
        {
            $having = '';
        }

        if( $this->sess[ 'usertype' ] == 2 )
        {
            $where = parent::prepare_query( ' WHERE a.lrule = %s AND a.lgroup = %s AND ( SELECT a2.lusertype_id FROM lumonata_user AS a2 WHERE a2.lusername = a.lusername ) = %d', 'accommodation_add_ons', 'accommodation', 2 );
        }
        else
        {
            $where = parent::prepare_query( ' WHERE a.lrule = %s AND a.lgroup = %s', 'accommodation_add_ons', 'accommodation' );
        }

        //-- Main Query
        $qm = 'SELECT 
                a.lname,
                a.lstatus,
                a.llang_id,
                a.lterm_id,
                ( 
                    SELECT a2.lpost_id 
                    FROM lumonata_post_relationship AS a2 
                    LEFT JOIN lumonata_post_terms AS b2 ON a2.lterm_id = b2.lterm_id 
                    WHERE a2.lterm_id = a.lterm_id AND b2.lrule = "accommodation_add_ons" 
                ) AS laccommodation
               FROM lumonata_post_terms AS a' . $where . $having . $order;
        $rm = parent::query( $qm );
        $nm = parent::num_rows( $rm );

        $qf = $qm . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        $data = array();

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
                $si = 'SELECT 
                        a.lattach 
                       FROM lumonata_attachment AS a 
                       WHERE a.lattach_id = (
                            SELECT a2.ladditional_value 
                            FROM lumonata_additional_field AS a2
                            WHERE a2.lterm_id = %d 
                            AND a2.lmodule_id = %d 
                            AND a2.ladditional_key = %s
                       )';
                $qi = parent::prepare_query( $si, $df[ 'lterm_id' ], $mod, 'accommodation_add_ons_image' );
                $ri = parent::query( $qi ); 
                $di = parent::fetch_array( $ri );

                if( empty( $di[ 'lattach' ] ) || ( !empty( $di[ 'lattach' ] ) && !file_exists( IMAGE_DIR . '/Uploads/' . $di[ 'lattach' ] ) ) )
                {
                    $img = 'default.png';
                }
                else
                {
                    $img = $di[ 'lattach' ];
                }

                $data[] = array(
                    'lattach'   => HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=90&h=67&src=' . HT_SERVER . SITE_URL . '/images/Uploads/' . $img,
                    'leditlink' => $this->global->getEditUrl( $mod, $usertype, $df[ 'lterm_id' ] ),
                    'lflag'     => $this->global->get_language_flag( $df[ 'llang_id' ] ),
                    'lname'     => ucwords( strtolower( $df[ 'lname' ] ) ),
                    'lterm_id'  => $df[ 'lterm_id' ],
                    'lstatus'   => $df[ 'lstatus' ]
                );
            }
        }

        return json_encode( array(
            'draw'            => intval( $this->post[ 'draw' ] ),
            'recordsFiltered' => intval( $nm ),
            'recordsTotal'    => intval( $nf ),
            'data'            => $data
        ));
    }

    function validate( $data = array() )
    {
        $error = array();

        foreach( $data as $field => $dt )
        {
            if( $field == 'lname' && $dt == '' )
            {
                array_push( $error, 'Surf trip name must have value' );
            }
        }

        return $error;
    }

    function create( $mod )
    {
        $apps  = $this->apps;
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- MERGE lsef INTO data array
            if( empty( $data[ 'lsef' ] ) )
            {
                $data = array_merge( $data, array( 'lsef' => $this->sef_url( $data[ 'lname' ] ) ) );
            }
            else
            {
                $data = array_merge( $data, array( 'lsef' => $this->sef_url( $data[ 'lsef' ] ) ) );
            }

            //-- INSERT lumonata_post_terms
            $param  = array_diff_key( $data, array_flip( array( 'lterm_id', 'ladditional' ) ) );
            $result = parent::insert( 'lumonata_post_terms', array_filter( $param ) );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to add new data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

                exit;
            }
            else
            {
                $lterm_id = parent::insert_id();

                //-- SYNC lumonata_post_relationship
                if( empty( $apps ) === false )
                {
                    foreach( $apps as $app )
                    {
                        if( is_array( $app ) )
                        {
                            foreach( $app as $a )
                            {
                                if( empty( $a ) === false )
                                {
                                    parent::insert( 'lumonata_post_relationship', array(
                                        'lterm_id' => $lterm_id,
                                        'lpost_id' => $a,
                                    ));
                                }
                            }
                        }
                        else
                        {
                            if( empty( $app ) === false )
                            {
                                parent::insert( 'lumonata_post_relationship', array(
                                    'lterm_id' => $lterm_id,
                                    'lpost_id' => $app,
                                ));
                            }
                        }
                    }
                }
                
                //-- SYNC lumonata_attachment
                $param = array( 'lapp_id' => $lterm_id );
                $where = array( 'lapp_id' => $data[ 'lterm_id' ], 'lmodule_id' => $mod );

                parent::update( 'lumonata_attachment', $param, $where );

                //-- SYNC lumonata_additional_field
                if( isset( $data[ 'ladditional' ] ) )
                {
                    foreach( $data[ 'ladditional' ] as $key => $value )
                    {
                        if( is_array( $value ) )
                        {
                            $value = json_encode( $value );
                        }

                        $rr = parent::insert( 'lumonata_additional_field', array(
                            'ladditional_value' => $value,
                            'ladditional_key' => $key,
                            'lterm_id' => $lterm_id,
                            'lmodule_id' => $mod,
                        ));
                    }
                }

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully add new data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $lterm_id );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

            exit;
        }
    }

    function change( $mod )
    {
        $apps  = $this->apps;
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- MERGE lsef INTO data array
            if( empty( $data[ 'lsef' ] ) )
            {
                $data = array_merge( $data, array( 'lsef' => $this->sef_url( $data[ 'lname' ] ) ) );
            }

            //-- UPDATE lumonata_post_terms
            $param  = array_diff_key( $data, array_flip( array( 'lterm_id', 'ladditional' ) ) );
            $where  = array( 'lterm_id' => $data[ 'lterm_id' ] );

            $result = parent::update( 'lumonata_post_terms', array_filter( $param ), $where );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to update existing data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

                exit;
            }
            else
            {
                //-- SYNC lumonata_post_relationship
                if( empty( $apps ) === false )
                {
                    $rt = parent::delete( 'lumonata_post_relationship', array( 'lterm_id' => $data[ 'lterm_id' ]  ) );

                    if( is_array( $rt ) === false )
                    {
                        foreach( $apps as $app )
                        {
                            if( is_array( $term ) )
                            {
                                foreach( $app as $a )
                                {
                                    if( empty( $a ) === false )
                                    {
                                        parent::insert( 'lumonata_post_relationship', array(
                                            'lterm_id' => $data[ 'lterm_id' ],
                                            'lpost_id' => $a
                                        ));
                                    }
                                }
                            }
                            else
                            {
                                if( empty( $app ) === false )
                                {
                                    parent::insert( 'lumonata_post_relationship', array(
                                        'lterm_id' => $data[ 'lterm_id' ],
                                        'lpost_id' => $app
                                    ));
                                }
                            }
                        }
                    }
                }

                //-- SYNC lumonata_additional_field
                if( isset( $data[ 'ladditional' ] ) )
                {
                    foreach( $data[ 'ladditional' ] as $key => $value )
                    {
                        if( is_array( $value ) )
                        {
                            $value = json_encode( $value );
                        }

                        //-- Check Additonal Field Exist or Not
                        $aid = $this->global->getValueField3( 'lumonata_additional_field', 'ladditional_id', 'lterm_id', $data[ 'lterm_id' ], 'ladditional_key', $key, 'lmodule_id', $mod );

                        if( empty( $aid ) )
                        {
                            parent::insert( 'lumonata_additional_field', array(
                                'lterm_id' => $data[ 'lterm_id' ],
                                'ladditional_value' => $value,
                                'ladditional_key' => $key,
                                'lmodule_id' => $mod,
                            ));
                        }
                        else
                        {
                            $param = array( 'ladditional_value' => $value );
                            $where = array( 'ladditional_id' => $aid );

                            parent::update( 'lumonata_additional_field', $param, $where );
                        }
                    }
                }

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully updated existing data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

            exit;
        }
    }

    function do_delete( $mod )
    {
        parent::begin();

        $commit = 1;

        $s = 'DELETE FROM lumonata_post_terms WHERE lterm_id = %d';
        $q = parent::prepare_query( $s, $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            $commit = 0;
        }
        else
        {
            $s = 'SELECT * FROM lumonata_attachment AS a WHERE a.lapp_id = %d AND a.lmodule_id = %d';
            $q = parent::prepare_query( $s, $this->post[ 'id' ], $mod );
            $r = parent::query( $q );

            while( $d = parent::fetch_array( $r ) )
            {
                $r2 = parent::delete( 'lumonata_attachment', array( 'lattach_id' => $d[ 'lattach_id' ] ) );

                if( is_array( $r2 ) )
                {
                    $commit = 0;
                }
                else
                {
                    if( empty( $d[ 'lattach' ] ) === false )
                    {
                        $this->upload->upload_constructor( IMAGE_DIR . '/Uploads/' );
                        $this->upload->delete_file_thumb( $d[ 'lattach' ] );
                    }
                }
            }
        }
                
        if( $commit == 0 )
        {
            parent::rollback();

            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            parent::commit();

            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function do_change_status()
    {
        $s = 'UPDATE lumonata_post_terms AS a SET a.lstatus = %d WHERE a.lterm_id = %s';
        $q = parent::prepare_query( $s, $this->post[ 'status' ], $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function upload_file( $mod )
    {
        if( isset( $_FILES[ 'attachment' ] ) && $_FILES[ 'attachment' ][ 'error' ] == 0 )
        {
            global $db;

            extract( $this->post );

            $g = new globalAdmin();
            $u = new upload();

            $file_name = $_FILES[ 'attachment' ][ 'name' ];
            $file_size = $_FILES[ 'attachment' ][ 'size' ];
            $file_type = $_FILES[ 'attachment' ][ 'type' ];
            $file_tmp  = $_FILES[ 'attachment' ][ 'tmp_name' ];

            $sef_img  = $this->upload->file_name_filter( $file_name ) . '-' . time();
            $file     = $this->upload->rename_file( $file_name, $sef_img );

            $this->upload->upload_constructor( IMAGE_DIR . '/Uploads/' );

            if( $this->post[ 'file_type' ] == 'images' )
            {
                if( in_array( $file_type, array( 'image/jpg', 'image/jpeg', 'image/pjpeg', 'image/gif', 'image/png' ) ) )
                {
                    $this->upload->upload_resize( $file_name, $sef_img, $file_tmp, $file_type, 940, 940, 1 );
                }
                else
                {
                    return json_encode( array( 'result' => 'not-supported' ) );
                }
            }
            elseif( $this->post[ 'file_type' ] == 'word' && !in_array( $file_type, array( 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ) ) )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }
            elseif( $this->post[ 'file_type' ] == 'pdf' && $file_type != 'application/pdf' )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }
            elseif( $this->post[ 'file_type' ] == 'svg' && $file_type != 'image/svg+xml' )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }

            if( $this->upload->upload_file( $file_name, $sef_img, $file_tmp, 0 ) )
            {
                $order_id = $this->global->setCode( 'lumonata_attachment', 'lorder_id' );

                $r = $db->insert( 'lumonata_attachment', array(
                    'lcreated_by'   => $this->sess[ 'username' ],
                    'lusername'     => $this->sess[ 'username' ],
                    'llang_id'      => $this->lang,
                    'lmimetype'     => $file_type,
                    'lorder_id'     => $order_id,
                    'lsef_url'      => $sef_img,
                    'lapp_id'       => $app_id,
                    'ltitle'        => $title,
                    'lattach'       => $file,
                    'lmodule_id'    => $mod,
                    'lcreated_date' => time(),
                    'ldlu'          => time(),
                    'lstatus'       => 2
                ));

                if( is_array( $r ) )
                {
                    $this->upload->delete_file_thumb( $file );

                    return json_encode( array( 'result' => 'failed' ) );
                }
                else
                {
                    $id = $db->insert_id();

                    if( $file_type == 'image/jpg' || $file_type == 'image/jpeg' || $file_type == 'image/pjpeg' || $file_type == 'image/gif' || $file_type == 'image/png' )
                    {
                        //-- If file type is an image show the thumbnail
                        $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=180&h=180&src=' . HT_SERVER . IMAGE_URL . '/Uploads/' . $file;
                    }
                    else
                    {
                        if( in_array( $file_type, array( 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ) ) )
                        {
                            //-- Word file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-doc.png';
                        }
                        elseif( $file_type == 'application/pdf' )
                        {
                            //-- PDF file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-pdf.png';
                        }
                        elseif( $file_type == 'image/svg+xml' )
                        {
                            //-- SVG file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-svg.png';
                        }
                        else
                        {
                            //-- Other file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-file.png';
                        }
                    }

                    return json_encode( array( 'result' => 'success', 'src' => $src, 'id' => $id ) );
                }
            }
            else
            {
                $this->upload->delete_thumb( $file );

                return json_encode( array( 'result' => 'failed' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'error' ) );
        }
    }

    function delete_file()
    {
        if( isset( $this->post[ 'lattach_id' ] ) && !empty( $this->post[ 'lattach_id' ] ) )
        {
            $image  = $this->global->getValueField( 'lumonata_attachment', 'lattach', 'lattach_id', $this->post[ 'lattach_id' ] );

            $result = parent::delete( 'lumonata_attachment', array( 'lattach_id' => $this->post[ 'lattach_id' ] ) );

            if( is_array( $result ) )
            {
                return json_encode( array( 'result' => 'error' ) );
            }
            else
            {
                if( empty( $image ) === false )
                {
                    $this->upload->upload_constructor( IMAGE_DIR . '/Uploads/' );
                    $this->upload->delete_file_thumb( $image );
                }

                return json_encode( array( 'result' => 'success' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
    }

    function order_file()
    {
        if( isset( $this->post[ 'image' ] ) && !empty( $this->post[ 'image' ] ) )
        {
            $error = 0;

            foreach( $this->post[ 'image' ] as $order_id => $attach_id )
            {
                $param  = array( 'lorder_id' => $order_id );
                $where  = array( 'lattach_id' => $attach_id );

                $result = parent::update( 'lumonata_attachment', $param, $where );

                if( is_array( $result ) )
                {
                    $error++;
                }
            }

            if( empty( $error ) == false )
            {
                return json_encode( array( 'result' => 'error', 'message' => 'Failed to reorder some image' ) );
            }
            else
            {
                return json_encode( array( 'result' => 'success' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'error', 'message' => 'No images was found to reorder' ) );
        }
    }

    function add_group()
    {
        if( isset( $this->post[ 'items' ] ) && !empty( $this->post[ 'items' ] ) )
        {
            $items  = json_decode( base64_decode( $this->post[ 'items' ] ), true );
            $object = json_decode( base64_decode( $this->post[ 'object' ] ), true );

            return $this->global->add_repeater_items( $items, $object );
        }
    }

    function sef_url( $string = '' )
    {
        $num = $this->global->getNumRows( 'lumonata_post_terms', 'lname', $string );

        if( $num > 0 )
        {
            for( $i = 2; $i <= $num + 1; $i++ )
            {
                $sef = $this->global->sef_url( $string ) . '-' . $i;
                
                if( $this->global->getNumRows( 'lumonata_post_terms', 'lsef', $sef ) < 1 )
                {
                    $sef = $sef;

                    break;
                }
            }
        }
        else
        {
            $sef = $this->global->sef_url( $string );
        }
        
        return $sef;
    }

    function switcher_language( $mod, $llang_id, $lref_id )
    {
        $s = 'SELECT a.llang_id FROM lumonata_post_terms AS a WHERE a.lref_id IS NOT NULL AND a.lref_id = %d ORDER BY a.lorder_id';
        $q = parent::prepare_query( $s, $lref_id );
        $r = parent::query( $q );

        $translation = array();

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_assoc( $r ) )
            {
                array_push( $translation, $d[ 'llang_id' ] );
            }
        }

        if( empty( $lref_id ) )
        {
            return $this->global->get_switcher_language( $mod, 'lumonata_post_terms', $llang_id, $lref_id, $translation, false );
        }
        else
        {
            return $this->global->get_switcher_language( $mod, 'lumonata_post_terms', $llang_id, $lref_id, $translation, false, true );
        }
    }

    function custom_field( $mod )
    {
        //-- Custom Field
        $cparam = array(
            'types' => 2,
            'field' => array(
                array(
                    'id'    => 'accommodation_add_ons_brief',
                    'name'  => 'accommodation_add_ons_brief',
                    'label' => 'Brief Text',
                    'type'  => 'textarea',
                    'attributes' => array(
                        'autocomplete' => 'off'
                    )
                ),
                array(
                    'id'     => 'accommodation_add_ons_image',
                    'name'   => 'accommodation_add_ons_image',
                    'label'  => 'Featured Image',
                    'type'   => 'image',
                    'desc'   => 'Recommended size 300 * 300',
                    'btntxt' => 'Choose Image',
                    'attributes' => array(
                        'autocomplete'  => 'off'
                    )
                ),
                array(
                    'id'    => 'addons_price',
                    'name'  => 'addons_price',
                    'label' => 'Price',
                    'type'  => 'text',
                    'attributes' => array(
                        'data-a-sign'  => $this->global->get_currency_sign(),
                        'class'        => 'text text-number',
                        'autocomplete' => 'off',
                        'data-a-sep'   => '.',
                        'data-a-dec'   => ',',
                        'data-m-dec'   => '0'
                    )
                ),
                array(
                    'id'    => 'addons_type_of_price',
                    'name'  => 'addons_type_of_price',
                    'label' => 'Type of price',
                    'type'  => 'select',
                    'options' => array(
                        0 => 'Per Item',
                        1 => 'Per Hour'
                    ),
                    'attributes' => array(
                        'data-placeholder' => 'Select type of price',
                        'class' => 'chzn-select',
                        'autocomplete' => 'off',
                        'style' => 'width:100%'
                    )
                )
            )
        );

        //-- Additional Field
        $aparam = array();

        $id = $this->fields[ 'lterm_id' ];
        $lg = $this->fields[ 'llang_id' ];

        $this->actions->add_actions( 'accommodation_add_ons_custom_field', $this->global->init_custom_field( $id, $mod, $cparam, $lg ) );
        $this->actions->add_actions( 'accommodation_add_ons_additional_field', $this->global->init_custom_field( $id, $mod, $aparam, $lg ) );
    }

    function language()
    {
        if( isset( $_GET[ 'lang' ] ) && $_GET[ 'lang' ] != '' )
        {
            $this->lang = $_GET[ 'lang' ];
        }
        else
        {
            $this->lang = $this->global->getSettingValue( 'llanguage' );
        }
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif ); 
        $this->sess    = $this->global->getCurrentSession();
        $this->ref     = null;
        $this->valid   = true;

        if( isset( $_GET[ 'id' ] ) )
        {
            $s = 'SELECT * FROM lumonata_post_terms AS a WHERE a.lterm_id = %d';
            $q = parent::prepare_query( $s, $_GET[ 'id' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $d = parent::fetch_array( $r );

                $fields = array(
                    'lcreated_by'   => $d[ 'lcreated_by' ],
                    'lusername'     => $d[ 'lusername' ],
                    'llang_id'      => $d[ 'llang_id' ],
                    'lref_id'       => $d[ 'lref_id' ],
                    'lterm_id'      => $d[ 'lterm_id' ],
                    'lcreated_date' => $d[ 'lcreated_date' ],
                    'ldlu'          => $d[ 'ldlu' ],
                    'lgroup'        => $d[ 'lgroup' ],
                    'lrule'         => $d[ 'lrule' ],
                    'lname'         => $d[ 'lname' ],
                    'ldescription'  => $d[ 'ldescription' ],
                    'lmeta_title'   => $d[ 'lmeta_title' ],
                    'lmeta_key'     => $d[ 'lmeta_key' ],
                    'lmeta_desc'    => $d[ 'lmeta_desc' ],
                    'lsef'          => $d[ 'lsef' ],
                    'lorder_id'     => $d[ 'lorder_id' ],
                    'lparent_id'    => $d[ 'lparent_id' ],
                    'lstatus'       => $d[ 'lstatus' ]
                );

                if( is_null( $d[ 'lref_id' ] ) )
                {
                    $this->ref = $d[ 'lterm_id' ];
                }
                else
                {
                    $this->ref = $d[ 'lref_id' ];
                }
            }
            else
            {
                $this->valid = false;
            }

            //-- GET post relationship
            $s = 'SELECT a.lpost_id FROM lumonata_post_relationship AS a LEFT JOIN lumonata_post AS b ON a.lpost_id = b.lpost_id WHERE a.lterm_id = %d';
            $q = parent::prepare_query( $s, $_GET[ 'id' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                while( $d = parent::fetch_array( $r ) )
                {
                    $apps[ 'laccommodation' ][] = $d[ 'lpost_id' ];
                }
            }
            else
            {
                $apps = array( 'laccommodation' => '' );
            }
        }
        else
        {
            if( isset( $_GET[ 'ref' ] ) )
            {
                $this->ref = $_GET[ 'ref' ];
            }

            $fields = array(
                'lcreated_by'   => $this->sess[ 'username' ],
                'lusername'     => $this->sess[ 'username' ],
                'llang_id'      => $this->lang,
                'lref_id'       => $this->ref,
                'lterm_id'      => time(),
                'lcreated_date' => time(),
                'ldlu'          => time(),
                'lgroup'        => 'accommodation',
                'lrule'         => 'accommodation_add_ons',
                'lname'         => '',
                'ldescription'  => '',
                'lmeta_title'   => '',
                'lmeta_key'     => '',
                'lmeta_desc'    => '',
                'lsef'          => '',
                'lorder_id'     => 0,
                'lparent_id'    => 0,
                'lstatus'       => 1
            );

            if( isset( $_GET[ 'ref' ] ) )
            {
                $s = 'SELECT * FROM lumonata_post_terms AS a WHERE a.lterm_id = %d';
                $q = parent::prepare_query( $s, $_GET[ 'ref' ] );
                $r = parent::query( $q );

                if( parent::num_rows( $r ) > 0 )
                {
                    $d = parent::fetch_array( $r );

                    $fields = array_merge( $fields, array( 'lname' => $d[ 'lname' ] ) );
                }
            }

            $apps = array( 'laccommodation' => '' );
        }

        if( empty( $_POST ) )
        {
            if( empty( $this->notif ) === false && isset( $this->notif[ 'post' ] ) )
            {
                $this->fields = array_merge( $fields, $this->notif[ 'post' ] );
            }
            else
            {
                if( isset( $fields ) )
                {
                    $this->fields = $fields;   
                }
            }

            $this->apps = $apps;

            $this->post = array();
        }
        else
        {
            if( isset( $_POST[ 'fields' ] ) )
            {
                $this->fields = array_merge( $fields, $_POST[ 'fields' ] );
            }
            else
            {
                $this->fields = $fields;
            }

            if( isset( $_POST[ 'apps' ] ) )
            {
                $this->apps = array_merge( $apps, $_POST[ 'apps' ] );
            }
            else
            {
                $this->apps = $apps;
            }

            $this->post = array_diff_key( $_POST, array_flip( array( 'fields' ) ) );
        }
    }

    function accommodation_option()
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = %s AND a.lstatus = %d';
        $q = parent::prepare_query( $s, 'accommodation', 1 );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $option = array();

            while( $d = parent::fetch_array( $r ) )
            {
                $option[ $d[ 'lpost_id' ] ] = $d[ 'ltitle' ];
            }

            return $option;
        }
    }

    function accommodation_add_ons_option()
    {
        $s = 'SELECT a.lterm_id, a.lname FROM lumonata_post_terms AS a WHERE a.lrule = %s AND a.lgroup = %s AND a.lstatus = %d';
        $q = parent::prepare_query( $s, 'accommodation_add_ons', 'accommodation', 1 );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $option = array();

            while( $d = parent::fetch_array( $r ) )
            {
                $option[ $d[ 'lterm_id' ] ] = $d[ 'lname' ];
            }

            return $option;
        }
    }

    function get_accommodation_option( $laccommodation = NULL, $llang_id = '' )
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = %s AND a.llang_id = %d AND a.lstatus = %d';
        $q = parent::prepare_query( $s, 'accommodation', $llang_id, 1 );

        return $this->global->set_option( $q, 'lpost_id', 'ltitle', $laccommodation, false );
    }

    function get_status_option( $lstatus = NULL )
    {
        return $this->global->set_static_option( array( 'Off', 'On' ), $lstatus, false );
    }

    function get_language_option( $mod )
    {
        return $this->global->set_filter_language_option( $mod, $this->lang );
    }

    function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'load' )
            {
                echo $this->load( $mod, $usertype );
            }

            if( $this->post[ 'pKEY' ] == 'do_change_status' )
            {
                echo $this->do_change_status();
            }

            if( $this->post[ 'pKEY' ] == 'do_delete' )
            {
                echo $this->do_delete( $mod );
            }

            if( $this->post[ 'pKEY' ] == 'order_file' )
            {
                echo $this->order_file();
            }

            if( $this->post[ 'pKEY' ] == 'delete_file' )
            {
                echo $this->delete_file();
            }

            if( $this->post[ 'pKEY' ] == 'upload_file' )
            {
                echo $this->upload_file( $mod );
            }

            if( $this->post[ 'pKEY' ] == 'add_group' )
            {
                echo $this->add_group();
            }
        }

        exit;
    }
}

?> 