<?php

class accommodation_included extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->upload   = new upload();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }
    
    function view( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );
        
        if( $this->global->getPViewBothP( $mod, $usertype ) )
        {
            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->create( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'mod', $mod );
            $this->template->set_var( 'cancel_css', 'hidden' );
            $this->template->set_var( 'action_label', 'CREATE' );
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'limit', $this->global->getSettingValue( 'list_admin' ) );

        	$this->template->set_var( 'lname', $lname );
            $this->template->set_var( 'lterm_id', $lterm_id );
            $this->template->set_var( 'lorder_id', $lorder_id );
            $this->template->set_var( 'llang_id', $this->get_language_option() );
            $this->template->set_var( 'lrule', $this->get_rule_option( $lrule ) );
            $this->template->set_var( 'lparent_id', $this->get_parent_option( $lparent_id ) );
            $this->template->set_var( 'switcher_lang', $this->switcher_language( $mod, $llang_id, $this->ref ) );

            $this->template->set_var( 'site_url', SITE_URL );
            $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Accommodations/accommodation_included/view.js' ) );
            
            $this->template->Parse( 'vC', 'viewContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }
    
    function insert( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );
        
        if( $this->global->getPViewBothP( $mod, $usertype ) )
        {
            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->create( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'mod', $mod );
            $this->template->set_var( 'cancel_css', 'hidden' );
            $this->template->set_var( 'action_label', 'CREATE' );
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'limit', $this->global->getSettingValue( 'list_admin' ) );

            $this->template->set_var( 'lname', $lname );
            $this->template->set_var( 'lterm_id', $lterm_id );
            $this->template->set_var( 'lorder_id', $lorder_id );
            $this->template->set_var( 'llang_id', $this->get_language_option() );
            $this->template->set_var( 'lrule', $this->get_rule_option( $lrule ) );
            $this->template->set_var( 'lparent_id', $this->get_parent_option( $lparent_id, $llang_id ) );
            $this->template->set_var( 'switcher_lang', $this->switcher_language( $mod, $llang_id, $this->ref ) );

            $this->template->set_var( 'site_url', SITE_URL );
            $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Accommodations/accommodation_included/view.js' ) );
            
            $this->template->Parse( 'vC', 'viewContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function edit( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $this->valid )
        {
            if( $this->global->getPEditBoth( $mod, $usertype ) )
            {
                //-- EXECUTE update action
                if( isset( $this->post[ 'save' ] ) )
                {
                    $this->change( $mod );
                }

                //-- EXTRACT fields
                extract( $this->fields );

                $this->template->set_var( 'mod', $mod );
                $this->template->set_var( 'action_label', 'CHANGE' );
                $this->template->set_var( 'message', $this->message );
                $this->template->set_var( 'limit', $this->global->getSettingValue( 'list_admin' ) );

                $this->template->set_var( 'lname', $lname );
                $this->template->set_var( 'lterm_id', $lterm_id );
                $this->template->set_var( 'lorder_id', $lorder_id );
                $this->template->set_var( 'llang_id', $this->get_language_option() );
                $this->template->set_var( 'lrule', $this->get_rule_option( $lrule ) );
                $this->template->set_var( 'lparent_id', $this->get_parent_option( $lparent_id, $llang_id ) );
                $this->template->set_var( 'switcher_lang', $this->switcher_language( $mod, $llang_id, $this->ref ) );

                $this->template->set_var( 'site_url', SITE_URL );
                $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
                $this->template->set_var( 'cancel_url', HT_SERVER . ADMIN_URL . '/home.php?mod=' . $mod . '&prc=view' );
                $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

                $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Accommodations/accommodation_included/view.js' ) );
                
                $this->template->Parse( 'vC', 'viewContent', true );
            }
            else
            {
                $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
            }
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NOT_FOUND ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function load( $mod, $usertype )
    {
        $cols  = array(
            0 => 'a.lname',
            1 => 'a.lrule',
            4 => 'a.lstatus'
        );

        //-- Query Limit
        $limit = parent::prepare_query( ' LIMIT ' . $this->post[ 'start' ] . ', ' . $this->post[ 'length' ] );
    
        //-- Query Order By
        if( empty( $this->post[ 'order' ] ) )
        {
            $order = parent::prepare_query( ' ORDER BY a.llang_id, a.lrule, a.lparent_id, a.lorder_id ASC' );
        }
        else
        {
            $order = array();

            foreach( $this->post[ 'order' ] as $i => $od )
            {
                if( isset( $cols[ $od[ 'column' ] ] ) )
                {
                    $order[] = $cols[ $od[ 'column' ] ] . ' ' . $od[ 'dir' ];
                }
            }

            if( empty( $order ) )
            {
                $order = parent::prepare_query( ' ORDER BY a.llang_id, a.lrule, a.lparent_id, a.lorder_id ASC' );
            }
            else
            {
                $order = parent::prepare_query( ' ORDER BY ' . implode( ', ', $order ) );
            }
        }

        //-- Query Additional Where
        $w = array();

        if( $this->post[ 'llang_id' ] != '' )
        {
            $w[] = parent::prepare_query( 'a.llang_id = %d', $this->post[ 'llang_id' ] );
        }

        if( isset( $this->post[ 'lstatus' ] ) && $this->post[ 'lstatus' ] != '' )
        {
            $w[] = parent::prepare_query( 'a.lstatus = %d', $this->post[ 'lstatus' ] );
        }
        else
        {
            $w[] = parent::prepare_query( 'a.lstatus <> %d', 2 );
        }

        if( $this->post[ 'lsearch' ] != '' )
        {
            $s = array();

            foreach( $cols as $col )
            {
                $s[] = parent::prepare_query( $col . ' LIKE %s', '%' . $this->post[ 'lsearch' ] . '%' );
            }

            $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
        }

        if( empty( $w ) === false )
        {
            $having = ' HAVING ' . implode( ' AND ', $w );
        }
        else
        {
            $having = '';
        }

        $where = parent::prepare_query( ' WHERE a.lgroup = %s', 'inclusions' );

        //-- Main Query
        $qm = 'SELECT 
                a.lname,
                a.lrule,
                a.lstatus,
                a.llang_id,
                a.lterm_id,
                a.lparent_id
               FROM lumonata_post_terms AS a' . $where . $having . $order;
        $rm = parent::query( $qm );
        $nm = parent::num_rows( $rm );

        $qf = $qm . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        $data = array();

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
                $data[] = array(
                    'leditlink' => $this->global->getEditUrl( $mod, $usertype, $df[ 'lterm_id' ] ),
                    'lflag'     => $this->global->get_language_flag( $df[ 'llang_id' ] ),
                    'lparent'   => $this->get_menu_parent( $df[ 'lparent_id' ] ),
                    'lname'     => ucwords( strtolower( $df[ 'lname' ] ) ),
                    'lrule'     => ucwords( strtolower( $df[ 'lrule' ] ) ),
                    'lterm_id'  => $df[ 'lterm_id' ],
                    'lstatus'   => $df[ 'lstatus' ]
                );
            }
        }

        return json_encode( array(
            'draw'            => intval( $this->post[ 'draw' ] ),
            'recordsFiltered' => intval( $nm ),
            'recordsTotal'    => intval( $nf ),
            'data'            => $data
        ));
    }

    function validate( $data = array() )
    {
        $error = array();

        foreach( $data as $field => $dt )
        {
            if( $field == 'lname' && $dt == '' )
            {
                array_push( $error, 'Inclusions title must have value' );
            }

            if( $field == 'lrule' && $dt == '' )
            {
                array_push( $error, 'Inclusions group must have value' );
            }
        }

        return $error;
    }

    function create( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- MERGE lsef INTO data array
            if( empty( $data[ 'lsef' ] ) )
            {
                $data = array_merge( $data, array( 'lsef' => $this->sef_url( $data[ 'lname' ] ) ) );
            }
            else
            {
                $data = array_merge( $data, array( 'lsef' => $this->sef_url( $data[ 'lsef' ] ) ) );
            }

            //-- INSERT lumonata_post_terms
            $param  = array_diff_key( $data, array_flip( array( 'lterm_id' ) ) );
            $result = parent::insert( 'lumonata_post_terms', array_filter( $param ) );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to add new data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=view' );

                exit;
            }
            else
            {
                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully add new data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=view' );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=view' );

            exit;
        }
    }

    function change( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- MERGE lsef INTO data array
            if( empty( $data[ 'lsef' ] ) )
            {
                $data = array_merge( $data, array( 'lsef' => $this->sef_url( $data[ 'lname' ] ) ) );
            }

            //-- UPDATE lumonata_post_terms
            $param  = array_diff_key( $data, array_flip( array( 'lterm_id' ) ) );
            $where  = array( 'lterm_id' => $data[ 'lterm_id' ] );

            $result = parent::update( 'lumonata_post_terms', array_filter( $param ), $where );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to update existing data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $data[ 'lterm_id' ] );

                exit;
            }
            else
            {
                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully updated existing data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $data[ 'lterm_id' ] );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $data[ 'lterm_id' ] );

            exit;
        }
    }

    function do_delete()
    {
        parent::begin();

        $commit = 1;

        $s = 'DELETE FROM lumonata_post_terms WHERE lterm_id = %d';
        $q = parent::prepare_query( $s, $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            $commit = 0;
        }
                
        if( $commit == 0 )
        {
            parent::rollback();

            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            parent::commit();

            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function do_change_status()
    {
        $s = 'UPDATE lumonata_post_terms AS a SET a.lstatus = %d WHERE a.lterm_id = %s';
        $q = parent::prepare_query( $s, $this->post[ 'status' ], $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function sef_url( $string = '' )
    {
        $num = $this->global->getNumRows( 'lumonata_post_terms', 'lname', $string );

        if( $num > 0 )
        {
            for( $i = 2; $i <= $num + 1; $i++ )
            {
                $sef = $this->global->sef_url( $string ) . '-' . $i;
                
                if( $this->global->getNumRows( 'lumonata_post_terms', 'lsef', $sef ) < 1 )
                {
                    $sef = $sef;

                    break;
                }
            }
        }
        else
        {
            $sef = $this->global->sef_url( $string );
        }
        
        return $sef;
    }

    function switcher_language( $mod, $llang_id, $lref_id )
    {
        $s = 'SELECT a.llang_id FROM lumonata_post_terms AS a WHERE a.lref_id IS NOT NULL AND a.lref_id = %d ORDER BY a.lorder_id';
        $q = parent::prepare_query( $s, $lref_id );
        $r = parent::query( $q );

        $translation = array();

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_assoc( $r ) )
            {
                array_push( $translation, $d[ 'llang_id' ] );
            }
        }

        if( empty( $lref_id ) )
        {
            return $this->global->get_switcher_language( $mod, 'lumonata_post_terms', $llang_id, $lref_id, $translation, false, false, true );
        }
        else
        {
            return $this->global->get_switcher_language( $mod, 'lumonata_post_terms', $llang_id, $lref_id, $translation, false, true, true );
        }
    }

    function language()
    {
        if( isset( $_GET[ 'lang' ] ) && $_GET[ 'lang' ] != '' )
        {
            $this->lang = $_GET[ 'lang' ];
        }
        else
        {
            $this->lang = $this->global->getSettingValue( 'llanguage' );
        }
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();
        $this->ref     = null;
        $this->valid   = true;

        if( isset( $_GET[ 'id' ] ) )
        {
            $s = 'SELECT * FROM lumonata_post_terms AS a WHERE a.lterm_id = %d';
            $q = parent::prepare_query( $s, $_GET[ 'id' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $d = parent::fetch_array( $r );

                $fields = array(
                    'lcreated_by'   => $d[ 'lcreated_by' ],
                    'lusername'     => $d[ 'lusername' ],
                    'llang_id'      => $d[ 'llang_id' ],
                    'lref_id'       => $d[ 'lref_id' ],
                    'lterm_id'      => $d[ 'lterm_id' ],
                    'lcreated_date' => $d[ 'lcreated_date' ],
                    'ldlu'          => $d[ 'ldlu' ],
                    'lgroup'        => $d[ 'lgroup' ],
                    'lrule'         => $d[ 'lrule' ],
                    'lname'         => $d[ 'lname' ],
                    'ldescription'  => $d[ 'ldescription' ],
                    'lmeta_title'   => $d[ 'lmeta_title' ],
                    'lmeta_key'     => $d[ 'lmeta_key' ],
                    'lmeta_desc'    => $d[ 'lmeta_desc' ],
                    'lsef'          => $d[ 'lsef' ],
                    'lorder_id'     => $d[ 'lorder_id' ],
                    'lparent_id'    => $d[ 'lparent_id' ],
                    'lstatus'       => $d[ 'lstatus' ]
                );

                if( is_null( $d[ 'lref_id' ] ) )
                {
                    $this->ref = $d[ 'lterm_id' ];
                }
                else
                {
                    $this->ref = $d[ 'lref_id' ];
                }
            }
            else
            {
                $this->valid = false;
            }
        }
        else
        {
            if( isset( $_GET[ 'ref' ] ) )
            {
                $this->ref = $_GET[ 'ref' ];
            }

            $fields = array(
                'lcreated_by'   => $this->sess[ 'username' ],
                'lusername'     => $this->sess[ 'username' ],
                'llang_id'      => $this->lang,
                'lref_id'       => $this->ref,
                'lterm_id'      => time(),
                'lcreated_date' => time(),
                'ldlu'          => time(),
                'lgroup'        => 'inclusions',
                'lrule'         => '',
                'lname'         => '',
                'ldescription'  => '',
                'lmeta_title'   => '',
                'lmeta_key'     => '',
                'lmeta_desc'    => '',
                'lsef'          => '',
                'lorder_id'     => 0,
                'lparent_id'    => 0,
                'lstatus'       => 1
            );

            if( isset( $_GET[ 'ref' ] ) )
            {
                $s = 'SELECT * FROM lumonata_post_terms AS a WHERE a.lterm_id = %d';
                $q = parent::prepare_query( $s, $_GET[ 'ref' ] );
                $r = parent::query( $q );

                if( parent::num_rows( $r ) > 0 )
                {
                    $d = parent::fetch_array( $r );

                    $fields = array_merge( $fields, array(
                        'lname' => $d[ 'lname' ],
                        'lsef'  => '',
                    ));
                }
            }
        }

        if( empty( $_POST ) )
        {
            if( empty( $this->notif ) === false && isset( $this->notif[ 'post' ] ) )
            {
                $this->fields = array_merge( $fields, $this->notif[ 'post' ] );
            }
            else
            {
                if( isset( $fields ) )
                {
                    $this->fields = $fields;   
                }
            }

            $this->post = array();
        }
        else
        {
            if( isset( $_POST[ 'fields' ] ) )
            {
                $this->fields = array_merge( $fields, $_POST[ 'fields' ] );
            }
            else
            {
                $this->fields = $fields;
            }

            $this->post = array_diff_key( $_POST, array_flip( array( 'fields' ) ) );
        }
    }

    function get_status_option( $lstatus = NULL )
    {
        return $this->global->set_static_option( array( 'Off', 'On' ), $lstatus, false );
    }

    function get_rule_option( $selected_id )
    {
        return $this->global->set_static_option( array( 
            'activities' => 'Activities',
            'dietary'    => 'Dietary Needs',
            'meals'      => 'Meals',
            'stay'       => 'Stay',
            'transport'  => 'Transport',
        ), $selected_id, false );
    }

    function get_parent_option( $selected_id, $llang_id = '' )
    {
        $q = parent::prepare_query( 'SELECT a.lterm_id, a.lname, a.lparent_id FROM lumonata_post_terms AS a WHERE a.lgroup = "inclusions" AND a.llang_id = %d', $llang_id );

        return $this->global->set_recursive_option( $selected_id, $q, 'lname', 'lterm_id', 'lparent_id' );
    }

    function get_language_option( $llang_id = NULL )
    {
        return $this->global->set_language_option( $llang_id, false );
    }

    function get_menu_parent( $lparent_id = 0 )
    {
        if( $lparent_id == 0 )
        {
            return 'Root';
        }
        else
        {
            return $this->global->set_recursive_label( $lparent_id, 'lumonata_post_terms', 'lname', 'lterm_id', 'lparent_id' );
        }
    }

    function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'load' )
            {
                echo $this->load( $mod, $usertype );
            }

            if( $this->post[ 'pKEY' ] == 'do_change_status' )
            {
                echo $this->do_change_status();
            }

            if( $this->post[ 'pKEY' ] == 'do_delete' )
            {
                echo $this->do_delete();
            }
        }

        exit;
    }
}

?> 