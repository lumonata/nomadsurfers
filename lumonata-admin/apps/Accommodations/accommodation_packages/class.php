<?php

class accommodation_packages extends db
{
    private $template;
    private $actions;
    private $global;
    private $flash;

    private $message;
    private $fields;
    private $lang;
    private $ref;

    private $notif;
    private $valid;
    private $post;
    private $sess;

    function __construct( $template_dir = '.', $actions )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }

    function view( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $this->global->getPViewBoth( $mod, $usertype ) )
        {
            $this->template->set_var( 'mod', $mod );
            $this->template->set_var( 'lang', $this->lang );
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'limit', $this->global->getSettingValue( 'list_admin' ) );

            $this->template->set_var( 'lstatus', $this->get_status_option() );
            $this->template->set_var( 'llang_id', $this->get_language_option( $mod ) );
            $this->template->set_var( 'laccommodation', $this->get_accommodation_option( null, $this->lang ) );
            
            $this->template->set_var( 'add_url', $this->global->getAddUrl( $mod, $usertype ) );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Accommodations/accommodation_packages/view.js' ) );

            $this->template->Parse( 'vC', 'viewContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function insert( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->global->getPInsertBoth( $mod, $usertype ) )
        {
            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->create( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'lname', $lname );
            $this->template->set_var( 'lnight', $lnight );
            $this->template->set_var( 'lprice', $lprice );
            $this->template->set_var( 'llang_id', $llang_id );
            $this->template->set_var( 'lorder_id', $lorder_id );
            $this->template->set_var( 'lpackage_id', $lpackage_id );
            $this->template->set_var( 'lcreated_by', $lcreated_by );
            $this->template->set_var( 'ldescription', $ldescription );
            $this->template->set_var( 'laccommodation_status', '' );

            $this->template->set_var( 'ldate_start', $ldate_start );
            $this->template->set_var( 'ldate_end', $ldate_end );

            $this->template->set_var( 'ltype', $this->get_accommodation_package_period_type_option( $ltype ) );
            $this->template->set_var( 'laccommodation', $this->get_accommodation_option( $lpost_id, $llang_id ) );
            
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'switcher_lang', $this->switcher_language( $mod, $llang_id, $this->ref ) );

            $this->template->set_var( 'site_url', SITE_URL );
            $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
            $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );

            $this->template->set_var( 'add_url', $this->global->getAddUrl( $mod, $usertype ) );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/moment@2.29.3/moment.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/tinymce@5.10.2/tinymce.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Accommodations/accommodation_packages/form.js' ) );

            $this->template->Parse( 'fC', 'formContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function edit( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->valid )
        {
            if( $this->global->getPEditBoth( $mod, $usertype ) )
            {
                //-- EXECUTE update action
                if( isset( $this->post[ 'save' ] ) )
                {
                    $this->change( $mod );
                }

                //-- EXTRACT fields
                extract( $this->fields );

                $this->template->set_var( 'lname', $lname );
                $this->template->set_var( 'lnight', $lnight );
                $this->template->set_var( 'lprice', $lprice );
                $this->template->set_var( 'llang_id', $llang_id );
                $this->template->set_var( 'lorder_id', $lorder_id );
                $this->template->set_var( 'lpackage_id', $lpackage_id );
                $this->template->set_var( 'lcreated_by', $lcreated_by );
                $this->template->set_var( 'ldescription', $ldescription );
                $this->template->set_var( 'laccommodation_status', 'disabled' );

                $this->template->set_var( 'ldate_start', $ldate_start );
                $this->template->set_var( 'ldate_end', $ldate_end );

                $this->template->set_var( 'ltype', $this->get_accommodation_package_period_type_option( $ltype ) );
                $this->template->set_var( 'laccommodation', $this->get_accommodation_option( $lpost_id, $llang_id ) );
                $this->template->set_var( 'lpackage_detail', $this->get_accommodation_package_rate( $lpackage_id, $lpost_id, $llang_id ) );

                $this->template->set_var( 'message', $this->message );
                $this->template->set_var( 'switcher_lang', $this->switcher_language( $mod, $llang_id, $this->ref ) );

                $this->template->set_var( 'site_url', SITE_URL );
                $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
                $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );

                $this->template->set_var( 'add_url', $this->global->getAddUrl( $mod, $usertype ) );
                $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/moment@2.29.3/moment.min.js' ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/daterangepicker@3.1.0/daterangepicker.min.js' ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/tinymce@5.10.2/tinymce.min.js' ) );

                $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Accommodations/accommodation_packages/form.js' ) );

                $this->template->Parse( 'fC', 'formContent', true );
            }
            else
            {
                $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
            }
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NOT_FOUND ) );
        }
        
        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function load( $mod, $usertype )
    {
        $cols  = array(
            0 => 'a.lname',
            3 => 'a.lstatus'
        );

        //-- Query Limit
        $limit = parent::prepare_query( ' LIMIT ' . $this->post[ 'start' ] . ', ' . $this->post[ 'length' ] );
    
        //-- Query Order By
        if( empty( $this->post[ 'order' ] ) )
        {
            $order = parent::prepare_query( ' ORDER BY a.llang_id, a.lorder_id ASC' );
        }
        else
        {
            $order = array();

            foreach( $this->post[ 'order' ] as $i => $od )
            {
                if( isset( $cols[ $od[ 'column' ] ] ) )
                {
                    $order[] = $cols[ $od[ 'column' ] ] . ' ' . $od[ 'dir' ];
                }
            }

            if( empty( $order ) )
            {
                $order = parent::prepare_query( ' ORDER BY a.llang_id, a.lorder_id ASC' );
            }
            else
            {
                $order = parent::prepare_query( ' ORDER BY ' . implode( ', ', $order ) );
            }
        }

        //-- Query Additional Where
        $w = array();

        if( $this->post[ 'laccommodation' ] != '' )
        {
            $w[] = parent::prepare_query( 'a.lpost_id = %d', $this->post[ 'laccommodation' ] );
        }

        if( $this->post[ 'lstatus' ] != '' )
        {
            $w[] = parent::prepare_query( 'a.lstatus = %d', $this->post[ 'lstatus' ] );
        }
        else
        {
            $w[] = parent::prepare_query( 'a.lstatus <> %d', 2 );
        }

        if( $this->post[ 'llang_id' ] != '' )
        {
            $w[] = parent::prepare_query( 'a.llang_id = %d', $this->post[ 'llang_id' ] );
        }

        if( $this->post[ 'lsearch' ] != '' )
        {
            $s = array();

            foreach( $cols as $col )
            {
                $s[] = parent::prepare_query( $col . ' LIKE %s', '%' . $this->post[ 'lsearch' ] . '%' );
            }

            $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
        }

        if( empty( $w ) === false )
        {
            $having = ' HAVING ' . implode( ' AND ', $w );
        }
        else
        {
            $having = '';
        }

        if( $this->sess[ 'usertype' ] == 2 )
        {
            $where = parent::prepare_query( ' WHERE ( SELECT a2.lusertype_id FROM lumonata_user AS a2 WHERE a2.lusername = a.lusername ) = %d', 2 );
        }
        else
        {
            $where = '';
        }

        //-- Main Query
        $qm = 'SELECT 
                a.lname,
                a.lstatus,
                a.llang_id,
                a.lpost_id,
                a.lpackage_id
               FROM lumonata_packages AS a' . $where . $having . $order;
        $rm = parent::query( $qm );
        $nm = parent::num_rows( $rm );

        $qf = $qm . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        $data = array();

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
                $data[] = array(
                    'leditlink'      => $this->global->getEditUrl( $mod, $usertype, $df[ 'lpackage_id' ] ),
                    'lflag'          => $this->global->get_language_flag( $df[ 'llang_id' ] ),
                    'laccommodation' => $this->accommodation_name( $df[ 'lpost_id' ] ),
                    'lname'          => ucwords( strtolower( $df[ 'lname' ] ) ),
                    'lpackage_id'    => $df[ 'lpackage_id' ],
                    'lstatus'        => $df[ 'lstatus' ]
                );
            }
        }

        return json_encode( array(
            'draw'            => intval( $this->post[ 'draw' ] ),
            'recordsFiltered' => intval( $nm ),
            'recordsTotal'    => intval( $nf ),
            'data'            => $data
        ));
    }

    function validate( $data = array() )
    {
        $error = array();

        foreach( $data as $field => $dt )
        {
            if( $field == 'lname' && $dt == '' )
            {
                array_push( $error, 'Package name must have value' );
            }
            else if( $field == 'lpost_id' && $dt == '' )
            {
                array_push( $error, 'Accommodation must have value' );
            }
        }

        return $error;
    }

    function create( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            parent::begin();

            $commit = 1;

            //-- INSERT lumonata_packages
            $param  = array_diff_key( $data, array_flip( array( 'lpackage_id', 'lroom_rate', 'ldaterange' ) ) );
            $result = parent::insert( 'lumonata_packages', array_filter( $param ) );

            if( is_array( $result ) )
            {
                $commit = 0;
            }
            else
            {
                $lpackage_id = parent::insert_id();

                if( isset( $data[ 'lroom_rate' ] ) )
                {
                    foreach( $data[ 'lroom_rate' ] as $lterm_id => $dt )
                    {
                        $result = parent::insert( 'lumonata_package_details', array(
                            'lprice'      => $dt[ 'price' ],
                            'lpackage_id' => $lpackage_id,
                            'lterm_id'    => $lterm_id,
                        ));

                        if( is_array( $result ) )
                        {
                            $commit = 0;

                            break;
                        }
                    }
                }
            }
                
            if( $commit == 0 )
            {
                parent::rollback();

                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to add new data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );
            }
            else
            {
                parent::commit();

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully add new data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $lpackage_id );
            }

            exit;
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

            exit;
        }
    }

    function change( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            parent::begin();

            $commit = 1;

            //-- UPDATE lumonata_packages
            $param  = array_diff_key( $data, array_flip( array( 'lpackage_id', 'lroom_rate', 'ldaterange' ) ) );
            $where  = array( 'lpackage_id' => $data[ 'lpackage_id' ] );

            $result = parent::update( 'lumonata_packages', $param, $where );

            if( is_array( $result ) )
            {
                $commit = 0;
            }
            else
            {
                if( isset( $data[ 'lroom_rate' ] ) )
                {
                    foreach( $data[ 'lroom_rate' ] as $lterm_id => $dt )
                    {
                        $num = parent::get_num_rows2( 'lumonata_package_details', 'lpackage_id', $data[ 'lpackage_id' ], 'lterm_id', $lterm_id );

                        if( $num > 0 )
                        {
                            $result = parent::update( 'lumonata_package_details', array( 'lprice' => $dt[ 'price' ] ), array(
                                'lpackage_id' => $data[ 'lpackage_id' ],
                                'lterm_id'    => $lterm_id
                            ));
                        }
                        else
                        {
                            $result = parent::insert( 'lumonata_package_details', array(
                                'lpackage_id' => $data[ 'lpackage_id' ],
                                'lprice'      => $dt[ 'price' ],
                                'lterm_id'    => $lterm_id,
                            ));
                        }

                        if( is_array( $result ) )
                        {
                            $commit = 0;

                            break;
                        }
                    }
                }
            }
                
            if( $commit == 0 )
            {
                parent::rollback();

                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to update existing data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );
            }
            else
            {
                parent::commit();

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully updated existing data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );
            }

            exit;
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

            exit;
        }
    }

    function do_delete( $mod )
    {
        parent::begin();

        $commit = 1;

        $s = 'DELETE FROM lumonata_packages WHERE lpackage_id = %d';
        $q = parent::prepare_query( $s, $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            $commit = 0;
        }
                
        if( $commit == 0 )
        {
            parent::rollback();

            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            parent::commit();

            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function do_change_status()
    {
        $s = 'UPDATE lumonata_packages AS a SET a.lstatus = %d WHERE a.lpackage_id = %s';
        $q = parent::prepare_query( $s, $this->post[ 'status' ], $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function switcher_language( $mod, $llang_id, $lref_id )
    {
        $s = 'SELECT a.llang_id FROM lumonata_packages AS a WHERE a.lref_id IS NOT NULL AND a.lref_id = %d ORDER BY a.lorder_id';
        $q = parent::prepare_query( $s, $lref_id );
        $r = parent::query( $q );

        $translation = array();

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_assoc( $r ) )
            {
                array_push( $translation, $d[ 'llang_id' ] );
            }
        }

        if( empty( $lref_id ) )
        {
            return $this->global->get_switcher_language( $mod, 'lumonata_packages', $llang_id, $lref_id, $translation, false );
        }
        else
        {
            return $this->global->get_switcher_language( $mod, 'lumonata_packages', $llang_id, $lref_id, $translation, false, true );
        }
    }

    function language()
    {
        if( isset( $_GET[ 'lang' ] ) && $_GET[ 'lang' ] != '' )
        {
            $this->lang = $_GET[ 'lang' ];
        }
        else
        {
            $this->lang = $this->global->getSettingValue( 'llanguage' );
        }
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif ); 
        $this->sess    = $this->global->getCurrentSession();
        $this->ref     = null;
        $this->valid   = true;

        if( isset( $_GET[ 'id' ] ) )
        {
            $s = 'SELECT * FROM lumonata_packages AS a WHERE a.lpackage_id = %d';
            $q = parent::prepare_query( $s, $_GET[ 'id' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $d = parent::fetch_array( $r );

                $fields = array(
                    'lcreated_by'   => $d[ 'lcreated_by' ],
                    'lusername'     => $d[ 'lusername' ],
                    'llang_id'      => $d[ 'llang_id' ],
                    'lref_id'       => $d[ 'lref_id' ],
                    'lpackage_id'   => $d[ 'lpackage_id' ],
                    'lcreated_date' => $d[ 'lcreated_date' ],
                    'ldlu'          => $d[ 'ldlu' ],
                    'lpost_id'      => $d[ 'lpost_id' ],
                    'ldescription'  => $d[ 'ldescription' ],
                    'lname'         => $d[ 'lname' ],
                    'ldate_start'   => $d[ 'ldate_start' ],
                    'ldate_end'     => $d[ 'ldate_end' ],
                    'lprice'        => $d[ 'lprice' ],
                    'ltype'         => $d[ 'ltype' ],
                    'lnight'        => $d[ 'lnight' ],
                    'lorder_id'     => $d[ 'lorder_id' ],
                    'lstatus'       => $d[ 'lstatus' ]
                );

                if( is_null( $d[ 'lref_id' ] ) )
                {
                    $this->ref = $d[ 'lpackage_id' ];
                }
                else
                {
                    $this->ref = $d[ 'lref_id' ];
                }
            }
            else
            {
                $this->valid = false;
            }
        }
        else
        {
            if( isset( $_GET[ 'ref' ] ) )
            {
                $this->ref = $_GET[ 'ref' ];
            }

            $fields = array(
                'lcreated_by'   => $this->sess[ 'username' ],
                'lusername'     => $this->sess[ 'username' ],
                'llang_id'      => $this->lang,
                'lref_id'       => $this->ref,
                'lpackage_id'   => time(),
                'lcreated_date' => time(),
                'ldlu'          => time(),
                'ldescription'  => '',
                'lpost_id'      => '',
                'lname'         => '',
                'ldate_start'   => '',
                'ldate_end'     => '',
                'lprice'        => 0,
                'ltype'         => 0,
                'lnight'        => 1,
                'lorder_id'     => 0,
                'lstatus'       => 1
            );

            if( isset( $_GET[ 'ref' ] ) )
            {
                $s = 'SELECT * FROM lumonata_packages AS a WHERE a.lpackage_id = %d';
                $q = parent::prepare_query( $s, $_GET[ 'ref' ] );
                $r = parent::query( $q );

                if( parent::num_rows( $r ) > 0 )
                {
                    $d = parent::fetch_array( $r );

                    $fields = array_merge( $fields, array( 'lname' => $d[ 'lname' ] ) );
                }
            }
        }

        if( empty( $_POST ) )
        {
            if( empty( $this->notif ) === false && isset( $this->notif[ 'post' ] ) )
            {
                $this->fields = array_merge( $fields, $this->notif[ 'post' ] );
            }
            else
            {
                if( isset( $fields ) )
                {
                    $this->fields = $fields;   
                }
            }

            $this->post = array();
        }
        else
        {
            if( isset( $_POST[ 'fields' ] ) )
            {
                $this->fields = array_merge( $fields, $_POST[ 'fields' ] );
            }
            else
            {
                $this->fields = $fields;
            }

            $this->post = array_diff_key( $_POST, array_flip( array( 'fields' ) ) );
        }
    }

    function accommodation_name( $lpost_id = NULL )
    {
        $s = 'SELECT a.ltitle FROM lumonata_post AS a WHERE a.lpost_id = %d';
        $q = parent::prepare_query( $s, $lpost_id );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $d = parent::fetch_array( $r );

            return $d[ 'ltitle' ];
        }
        else
        {
            return '-';
        }
    }

    function get_accommodation_package_rate( $lpackage_id = NULL, $lpost_id = NULL, $llang_id = NULL )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            extract( $this->post );
        }

        $data = $this->global->getPosts( array( 'lpost_id' => $lpost_id ) );

        if( empty( $data ) === false )
        {
            $sign  = $this->global->get_currency_sign( $lpost_id );
            $price = $this->global->getFields( 'lumonata_packages', 'lprice', array( 'lpackage_id' => $lpackage_id ) );

            if( $price == '' || ( is_array( $price ) && empty( $price ) ) )
            {
                $price = $this->global->get_start_price( $data );
            }

            $list = '
            <div class="list list-field list-field-rate">
                <label class="list-field-title">Rates</label>
                <input value="' . $price . '" name="fields[lprice]" class="text text-rate text-small" autocomplete="off" data-a-sign="' . $sign . '" data-a-sep="." data-a-dec="," data-m-dec="0">
            </div>';

            if( isset( $data[ 'room_type' ] ) && !empty( $data[ 'room_type' ] ) )
            {
                $list .= '
                <div class="list hidden">
                    <div class="clear"></div>
                    <hr class="style4">
                    <table class="display dataTable no-footer">
                        <thead>
                            <tr>
                                <th style="vertical-align:middle;">Room Name</th>
                                <th style="vertical-align:middle;">Rates</th>
                            </tr>
                        </thead>
                        <tbody>';

                            foreach( $data[ 'room_type' ] as $dt )
                            {
                                $dta = $this->global->getFields( 'lumonata_package_details', array( 'lterm_id', 'lprice' ), array( 'lpackage_id' => $lpackage_id, 'lterm_id' => $dt[ 'lterm_id' ] ) );

                                if( isset( $dta[ 'lprice' ] ) && !empty( $dta[ 'lprice' ] ) )
                                {
                                    $rprice = $dta[ 'lprice' ];
                                }
                                else
                                {
                                    $rprice = $price;
                                }

                                $list .= '
                                <tr>
                                    <td style="vertical-align:middle;">' . $dt[ 'lname' ] . '</td>
                                    <td style="vertical-align:middle;">
                                        <input value="' . $rprice . '" name="fields[lroom_rate][' . $dt[ 'lterm_id' ] . '][price]" class="text text-rate text-small" style="margin:0;" autocomplete="off" data-a-sign="' . $sign . '" data-a-sep="." data-a-dec="," data-m-dec="0">
                                    </td>
                                </tr>';
                            }
                        
                            $list .= '
                        </tbody>
                    </table>
                </div>';
            }

            if( isset( $this->post[ 'pKEY' ] ) )
            {
                return json_encode( array( 'result' => 'success', 'data' => $list ) );
            }
            else
            {
                return $list;
            }
        }
        else
        {
            if( isset( $this->post[ 'pKEY' ] ) )
            {
                return json_encode( array( 'result' => 'failed' ) );
            }
            else
            {
                return '';
            }
        }
    }

    function get_accommodation_option( $lpost_id = NULL, $llang_id = '' )
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = %s AND a.llang_id = %d AND a.lstatus = %d';
        $q = parent::prepare_query( $s, 'accommodation', $llang_id, 1 );

        return $this->global->set_option( $q, 'lpost_id', 'ltitle', $lpost_id, false );
    }

    function get_status_option( $lstatus = NULL )
    {
        return $this->global->set_static_option( array( 'Off', 'On' ), $lstatus, false );
    }

    function get_accommodation_package_period_type_option( $ltype = NULL )
    {
        return $this->global->set_static_option( array( 'All time periods', 'Use a date range' ), $ltype, false );
    }

    function get_language_option( $mod )
    {
        return $this->global->set_filter_language_option( $mod, $this->lang );
    }

    function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'load' )
            {
                echo $this->load( $mod, $usertype );
            }

            if( $this->post[ 'pKEY' ] == 'do_change_status' )
            {
                echo $this->do_change_status();
            }

            if( $this->post[ 'pKEY' ] == 'do_delete' )
            {
                echo $this->do_delete( $mod );
            }

            if( $this->post[ 'pKEY' ] == 'get_accommodation_package_rate' )
            {
                echo $this->get_accommodation_package_rate();
            }
        }

        exit;
    }
}

?> 