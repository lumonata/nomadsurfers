<?php

class accommodation_rates extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->upload   = new upload();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }

    function view( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $this->global->getPViewBoth( $mod, $usertype ) )
        {
            $this->template->set_var( 'mod', $mod );
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'limit', $this->global->getSettingValue( 'list_admin' ) );

            $this->template->set_var( 'lstatus', $this->get_status_option() );
            $this->template->set_var( 'laccommodation', $this->get_accommodation_option() );
            
            $this->template->set_var( 'add_url', $this->global->getAddUrl( $mod, $usertype ) );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Accommodations/accommodation_rates/view.js' ) );

            $this->template->Parse( 'vC', 'viewContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function insert( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->global->getPInsertBoth( $mod, $usertype ) )
        {
            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->create( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'lrate_id', $lrate_id );
            $this->template->set_var( 'lrate_name', $lrate_name );
            $this->template->set_var( 'lrate_price', $lrate_price );

            $this->template->set_var( 'llang_id', $llang_id );
            $this->template->set_var( 'lorder_id', $lorder_id );
            $this->template->set_var( 'lcreated_by', $lcreated_by );
            
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'lroom_rate', $this->get_accommodation_rate() );
            $this->template->set_var( 'lpost_id', $this->get_accommodation_option( $lpost_id ) );

            $this->template->set_var( 'site_url', SITE_URL );
            $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
            $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Accommodations/accommodation_rates/form.js' ) );

            $this->template->Parse( 'fC', 'formContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function edit( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->valid )
        {
            if( $this->global->getPEditBoth( $mod, $usertype ) )
            {
                //-- EXECUTE update action
                if( isset( $this->post[ 'save' ] ) )
                {
                    $this->change( $mod );
                }

                //-- EXTRACT fields
                extract( $this->fields );

                $this->template->set_var( 'lrate_id', $lrate_id );
                $this->template->set_var( 'lrate_name', $lrate_name );
                $this->template->set_var( 'lrate_price', $lrate_price );

                $this->template->set_var( 'llang_id', $llang_id );
                $this->template->set_var( 'lorder_id', $lorder_id );
                $this->template->set_var( 'lcreated_by', $lcreated_by );
                
                $this->template->set_var( 'message', $this->message );
                $this->template->set_var( 'lroom_rate', $this->get_accommodation_rate() );
                $this->template->set_var( 'lpost_id', $this->get_accommodation_option( $lpost_id ) );

                $this->template->set_var( 'site_url', SITE_URL );
                $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
                $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
                $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );

                $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Accommodations/accommodation_rates/form.js' ) );

                $this->template->Parse( 'fC', 'formContent', true );
            }
            else
            {
                $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
            }
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NOT_FOUND ) );
        }
        
        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function load( $mod, $usertype )
    {
        $cols  = array(
            0 => 'a.lrate_name',
            2 => 'a.lstatus'
        );

        //-- Query Limit
        $limit = parent::prepare_query( ' LIMIT ' . $this->post[ 'start' ] . ', ' . $this->post[ 'length' ] );
    
        //-- Query Order By
        if( empty( $this->post[ 'order' ] ) )
        {
            $order = parent::prepare_query( ' ORDER BY a.lorder_id ASC' );
        }
        else
        {
            $order = array();

            foreach( $this->post[ 'order' ] as $i => $od )
            {
                if( isset( $cols[ $od[ 'column' ] ] ) )
                {
                    $order[] = $cols[ $od[ 'column' ] ] . ' ' . $od[ 'dir' ];
                }
            }

            if( empty( $order ) )
            {
                $order = parent::prepare_query( ' ORDER BY a.lorder_id ASC' );
            }
            else
            {
                $order = parent::prepare_query( ' ORDER BY ' . implode( ', ', $order ) );
            }
        }

        //-- Query Additional Where
        $w = array();

        if( $this->post[ 'laccommodation' ] != '' )
        {
            $w[] = parent::prepare_query( 'laccommodation = %d', $this->post[ 'laccommodation' ] );
        }

        if( $this->post[ 'lstatus' ] != '' )
        {
            $w[] = parent::prepare_query( 'a.lstatus = %d', $this->post[ 'lstatus' ] );
        }

        if( $this->post[ 'lsearch' ] != '' )
        {
            $s = array();

            foreach( $cols as $col )
            {
                $s[] = parent::prepare_query( $col . ' LIKE %s', '%' . $this->post[ 'lsearch' ] . '%' );
            }

            $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
        }

        if( empty( $w ) === false )
        {
            $having = ' HAVING ' . implode( ' AND ', $w );
        }
        else
        {
            $having = '';
        }

        if( $this->sess[ 'usertype' ] == 2 )
        {
            $where = parent::prepare_query( ' WHERE ( SELECT a2.lusertype_id FROM lumonata_user AS a2 WHERE a2.lusername = a.lusername ) = %d', 'room_type', 'accommodation', 2 );
        }
        else
        {
            $where = '';
        }

        //-- Main Query
        $qm = 'SELECT
                a.lrate_id,
                a.lrate_name,
                a.lpost_id,
                b.ltitle,
                a.lrate_price,
                a.lstatus
              FROM lumonata_rates AS a
              LEFT JOIN lumonata_post AS b ON a.lpost_id = b.lpost_id' . $where . $having . $order;
        $rm = parent::query( $qm );
        $nm = parent::num_rows( $rm );

        $qf = $qm . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        $data = array();

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
                $data[] = array(
                    'leditlink'  => $this->global->getEditUrl( $mod, $usertype, $df[ 'lrate_id' ] ),
                    'lrate'      => $df[ 'lrate_price' ],
                    'lrate_name' => $df[ 'lrate_name' ],
                    'lrate_id'   => $df[ 'lrate_id' ],
                    'ltitle'     => $df[ 'ltitle' ],
                    'lstatus'    => $df[ 'lstatus' ]
                );
            }
        }

        return json_encode( array(
            'draw'            => intval( $this->post[ 'draw' ] ),
            'recordsFiltered' => intval( $nm ),
            'recordsTotal'    => intval( $nf ),
            'data'            => $data
        ));
    }

    function validate( $data = array() )
    {
        $error = array();

        if( !isset( $data[ 'lperiod' ] ) && !isset( $this->post[ 'period' ] ) )
        {
            array_push( $error, 'You must setup rate period first' );
        }

        foreach( $data as $field => $dt )
        {
            if( $field == 'lrate_name' && $dt == '' )
            {
                array_push( $error, 'Rate name must have value' );
            }
            else if( $field == 'lpost_id' && $dt == '' )
            {
                array_push( $error, 'Accommodation must have value' );
            }
            else if( $field == 'lperiod' )
            {
                $err = 0;

                foreach( $dt[ 'start' ] as $i => $d )
                {
                    if( $d == '' || $dt[ 'end' ][ $i ] == '' )
                    {
                        $err++;

                        break;
                    }
                }

                if( $err > 0 )
                {
                    array_push( $error, 'You must setup rate period with valid data' );
                }
            }
        }

        return $error;
    }

    function create( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            parent::begin();

            $commit = 1;

            //-- INSERT lumonata_rates
            $param  = array_diff_key( $data, array_flip( array( 'lrate_id', 'lroom_rate', 'lperiod' ) ) );
            $result = parent::insert( 'lumonata_rates', $param );

            if( is_array( $result ) )
            {
                $commit = 0;
            }
            else
            {
                $lrate_id = parent::insert_id();

                if( isset( $data[ 'lroom_rate' ] ) )
                {
                    foreach( $data[ 'lroom_rate' ] as $lterm_id => $dt )
                    {
                        $result = parent::insert( 'lumonata_rate_details', array(
                            'lrate_id'    => $lrate_id,
                            'lterm_id'    => $lterm_id,
                            'lrate_price' => $dt[ 'price' ]
                        ));

                        if( is_array( $result ) )
                        {
                            $commit = 0;

                            break;
                        }
                    }
                }

                if( isset( $data[ 'lperiod' ] ) )
                {
                    foreach( $data[ 'lperiod' ][ 'start' ] as $i => $dt )
                    {
                        if( !empty( $dt ) && !empty( $data[ 'lperiod' ][ 'end' ][ $i ] ) )
                        {
                            $result = parent::insert( 'lumonata_rate_periods', array(
                                'lrate_id'      => $lrate_id,
                                'lstart_period' => $dt,
                                'lend_period'   => $data[ 'lperiod' ][ 'end' ][ $i ]
                            ));

                            if( is_array( $result ) )
                            {
                                $commit = 0;

                                break;
                            }
                        }
                    }
                }
            }
                
            if( $commit == 0 )
            {
                parent::rollback();

                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to add new data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

                exit;
            }
            else
            {
                parent::commit();

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully add new data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $lrate_id );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

            exit;
        }
    }

    function change( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            parent::begin();

            $commit = 1;

            //-- UPDATE lumonata_rates
            $param  = array_diff_key( $data, array_flip( array( 'lrate_id', 'lroom_rate', 'lperiod' ) ) );
            $where  = array( 'lrate_id' => $data[ 'lrate_id' ] );

            $result = parent::update( 'lumonata_rates', $param, $where );

            if( is_array( $result ) )
            {
                $commit = 0;
            }
            else
            {
                if( isset( $data[ 'lroom_rate' ] ) )
                {
                    foreach( $data[ 'lroom_rate' ] as $lterm_id => $dt )
                    {
                        $num = parent::get_num_rows2( 'lumonata_rate_details', 'lrate_id', $data[ 'lrate_id' ], 'lterm_id', $lterm_id );

                        if( $num > 0 )
                        {
                            $result = parent::update( 'lumonata_rate_details', array( 'lrate_price' => $dt[ 'price' ] ), array(
                                'lrate_id' => $data[ 'lrate_id' ],
                                'lterm_id' => $lterm_id
                            ));
                        }
                        else
                        {
                            $result = parent::insert( 'lumonata_rate_details', array(
                                'lrate_id'    => $data[ 'lrate_id' ],
                                'lrate_price' => $dt[ 'price' ],
                                'lterm_id'    => $lterm_id,
                            ));
                        }

                        if( is_array( $result ) )
                        {
                            $commit = 0;

                            break;
                        }
                    }
                }

                if( isset( $data[ 'lperiod' ] ) )
                {
                    foreach( $data[ 'lperiod' ][ 'start' ] as $i => $dt )
                    {
                        if( !empty( $dt ) && !empty( $data[ 'lperiod' ][ 'end' ][ $i ] ) )
                        {
                            $result = parent::insert( 'lumonata_rate_periods', array(
                                'lrate_id'      => $data[ 'lrate_id' ],
                                'lstart_period' => $dt,
                                'lend_period'   => $data[ 'lperiod' ][ 'end' ][ $i ]
                            ));

                            if( is_array( $result ) )
                            {
                                $commit = 0;

                                break;
                            }
                        }
                    }
                }

                if( isset( $this->post[ 'period' ] ) )
                {
                    foreach( $this->post[ 'period' ]  as $period_id => $dt )
                    {
                        if( !empty( $dt[ 'start' ] ) && !empty( $dt[ 'end' ] ) )
                        {
                            $result = parent::update( 'lumonata_rate_periods', array(
                                'lstart_period' => $dt[ 'start' ],
                                'lend_period'   => $dt[ 'end' ]
                            ), array( 'lrate_period_id' => $period_id ));

                            if( is_array( $result ) )
                            {
                                $commit = 0;

                                break;
                            }
                        }
                    }
                }
            }
                
            if( $commit == 0 )
            {
                parent::rollback();

                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to update existing data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

                exit;
            }
            else
            {
                parent::commit();

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully updated existing data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

            exit;
        }
    }

    function do_delete( $mod )
    {
        $s = 'DELETE FROM lumonata_rates WHERE lrate_id = %d';
        $q = parent::prepare_query( $s, $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function do_change_status()
    {
        $s = 'UPDATE lumonata_rates AS a SET a.lstatus = %d WHERE a.lrate_id = %s';
        $q = parent::prepare_query( $s, $this->post[ 'status' ], $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function language()
    {
        $this->lang = $this->global->getSettingValue( 'llanguage' );
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif ); 
        $this->sess    = $this->global->getCurrentSession();
        $this->valid   = true;

        if( isset( $_GET[ 'id' ] ) )
        {
            $s = 'SELECT * FROM lumonata_rates AS a WHERE a.lrate_id = %d';
            $q = parent::prepare_query( $s, $_GET[ 'id' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $d = parent::fetch_array( $r );

                $fields = array(
                    'lcreated_by'   => $d[ 'lcreated_by' ],
                    'lusername'     => $d[ 'lusername' ],
                    'llang_id'      => $d[ 'llang_id' ],
                    'lcreated_date' => $d[ 'lcreated_date' ],
                    'ldlu'          => $d[ 'ldlu' ],
                    'lrate_id'      => $d[ 'lrate_id' ],
                    'lrate_name'    => $d[ 'lrate_name' ],
                    'lpost_id'      => $d[ 'lpost_id' ],
                    'lrate_price'   => $d[ 'lrate_price' ],
                    'lorder_id'     => $d[ 'lorder_id' ],
                    'lstatus'       => $d[ 'lstatus' ]
                );
            }
            else
            {
                $this->valid = false;
            }
        }
        else
        {
            $fields = array(
                'lcreated_by'   => $this->sess[ 'username' ],
                'lusername'     => $this->sess[ 'username' ],
                'llang_id'      => $this->lang,
                'lcreated_date' => time(),
                'ldlu'          => time(),
                'lrate_id'      => '',
                'lrate_name'    => '',
                'lpost_id'      => '',
                'lrate_price'   => 0,
                'lorder_id'     => 0,
                'lstatus'       => 1
            );
        }

        if( empty( $_POST ) )
        {
            if( empty( $this->notif ) === false && isset( $this->notif[ 'post' ] ) )
            {
                $this->fields = array_merge( $fields, $this->notif[ 'post' ] );
            }
            else
            {
                if( isset( $fields ) )
                {
                    $this->fields = $fields;   
                }
            }

            $this->post = array();
        }
        else
        {
            if( isset( $_POST[ 'fields' ] ) )
            {
                $this->fields = array_merge( $fields, $_POST[ 'fields' ] );
            }
            else
            {
                $this->fields = $fields;
            }

            $this->post = array_diff_key( $_POST, array_flip( array( 'fields' ) ) );
        }
    }

    function get_accommodation_rate()
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            $lrate_cls = 'text text-rate text-small';
            $lrate_id  = $this->post[ 'rid' ];
            $lpost_id  = $this->post[ 'id' ];
        }
        else
        {
            $lrate_cls = 'text text-number text-small';
            $lrate_id  = $this->fields[ 'lrate_id' ];
            $lpost_id  = $this->fields[ 'lpost_id' ];
        }

        $data = $this->global->getPosts( array( 'lpost_id' => $lpost_id ) );

        if( !empty( $data ) )
        {
            $sign  = $this->global->get_currency_sign( $lpost_id );
            $price = $this->global->getFields( 'lumonata_rates', 'lrate_price', array( 'lpost_id' => $lpost_id, 'lrate_id' => $lrate_id ) );

            if( $price == '' || ( is_array( $price ) && empty( $price ) ) )
            {
                $price = $this->global->get_start_price( $data );
            }

            $list = '
            <div class="list list-field list-field-period">
                <label class="list-field-title">Period</label>
                <div class="list-field-period-wrap">';

                    if( isset( $this->post[ 'period' ] ) )
                    {
                        foreach( $this->post[ 'period' ] as $period_id => $p )
                        {
                            $list .= '
                            <div class="period-item">
                                <input class="period-picker period-start text text-small" name="period[' . $period_id . '][start]" value="' . $p[ 'start' ] . '" />
                                <span> - </span>
                                <input class="period-picker period-end text text-small" name="period[' . $period_id . '][end]" value="' . $p[ 'end' ] . '" />
                                <a class="actiond actiond2" data-id="' . $period_id . '"></a>
                            </div>';
                        }
                    }
                    else
                    {
                        if( !empty( $lrate_id ) )
                        {
                            $rp = $this->global->getFields( 'lumonata_rate_periods', null, array( 'lrate_id' => $lrate_id ), false );
                            $np = parent::num_rows( $rp );

                            if( $np > 0 )
                            {
                                while( $dp = parent::fetch_array( $rp ) )
                                {
                                    $list .= '
                                    <div class="period-item">
                                        <input class="period-picker period-start text text-small" name="period[' . $dp[ 'lrate_period_id' ] . '][start]" value="' . $dp[ 'lstart_period' ] . '" />
                                        <span> - </span>
                                        <input class="period-picker period-end text text-small" name="period[' . $dp[ 'lrate_period_id' ] . '][end]" value="' . $dp[ 'lend_period' ] . '" />
                                        <a class="actiond actiond2" data-id="' . $dp[ 'lrate_period_id' ] . '"></a>
                                    </div>';
                                }
                            }
                        }
                    }

                    if( isset( $this->fields[ 'lperiod' ][ 'start' ] ) )
                    {
                        foreach( $this->fields[ 'lperiod' ][ 'start' ] as $i => $period )
                        {
                            $list .= '
                            <div class="period-item">
                                <input class="period-picker period-start text text-small" name="fields[lperiod][start][]" value="' . $period . '" />
                                <span> - </span>
                                <input class="period-picker period-end text text-small" name="fields[lperiod][end][]" value="' . $this->fields[ 'lperiod' ][ 'end' ][ $i ] . '" />
                                <a class="actiond actiond2"></a>
                            </div>';
                        }
                    }

                    $list .= '
                </div>
                <input id="add_period" type="button" value="Add Period" class="button shadow fwbold mt0" name="add_period" style="font-size: 15px; padding: 9px 20px; font-weight: 400; margin:0; float:left;">
            </div>
            <div class="list list-field list-field-rate">
                <label class="list-field-title">Rates</label>
                <input value="' . $price . '" name="fields[lrate_price]" class="' . $lrate_cls . '" autocomplete="off" data-a-sign="' . $sign . '" data-a-sep="." data-a-dec="," data-m-dec="0">
            </div>';

            if( isset( $data[ 'room_type' ] ) && !empty( $data[ 'room_type' ] ) )
            {
                $list .= '
                <div class="list">
                    <div class="clear"></div>
                    <hr class="style4">
                    <table class="display dataTable no-footer">
                        <thead>
                            <tr>
                                <th style="vertical-align:middle;">Room Name</th>
                                <th style="vertical-align:middle;">Rates</th>
                            </tr>
                        </thead>
                        <tbody>';

                            foreach( $data[ 'room_type' ] as $dt )
                            {
                                if( $dt[ 'lstatus' ] != 2 )
                                {  
                                    $dta = $this->global->getFields( 'lumonata_rate_details', array( 'lterm_id', 'lrate_price' ), array( 'lterm_id' => $dt[ 'lterm_id' ], 'lrate_id' => $lrate_id ) );

                                    if( isset( $dta[ 'lrate_price' ] ) && !empty( $dta[ 'lrate_price' ] ) )
                                    {
                                        $rprice = $dta[ 'lrate_price' ];
                                    }
                                    else
                                    {
                                        $rprice = $dt[ 'room_price' ];
                                    }

                                    $list .= '
                                    <tr>
                                        <td style="vertical-align:middle;">' . $dt[ 'lname' ] . '</td>
                                        <td style="vertical-align:middle;">
                                            <input value="' . $rprice . '" name="fields[lroom_rate][' . $dt[ 'lterm_id' ] . '][price]" class="' . $lrate_cls . '" style="margin:0;" autocomplete="off" data-a-sign="' . $sign . '" data-a-sep="." data-a-dec="," data-m-dec="0">
                                        </td>
                                    </tr>';
                                }
                            }
                        
                            $list .= '
                        </tbody>
                    </table>
                </div>';
            }

            if( isset( $this->post[ 'pKEY' ] ) )
            {
                return json_encode( array( 'result' => 'success', 'data' => $list ) );
            }
            else
            {
                return $list;
            }
        }
        else
        {
            if( isset( $this->post[ 'pKEY' ] ) )
            {
                return json_encode( array( 'result' => 'failed' ) );
            }
            else
            {
                return '';
            }
        }
    }

    function add_rate_period()
    {
        return '        
        <div class="period-item">
            <input class="period-picker period-start text text-small" name="fields[lperiod][start][]" />
            <span> - </span>
            <input class="period-picker period-end text text-small" name="fields[lperiod][end][]" />
            <a class="actiond actiond2"></a>
        </div>';
    }

    function  delete_rate_period()
    {
        if( isset( $this->post[ 'id' ] ) )
        {            
            $s = 'DELETE FROM lumonata_rate_periods WHERE lrate_period_id = %d';
            $q = parent::prepare_query( $s, $this->post[ 'id' ] );
            $r = parent::query( $q );

            if( is_array( $r ) )
            {
                return json_encode( array( 'result' => 'failed', 'message' => 'Failed to delete this period' ) );
            }
            else
            {
                return json_encode( array( 'result' => 'success' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function get_accommodation_option( $laccommodation = NULL )
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = %s AND ( a.lref_id IS NULL OR a.lref_id = 0 ) AND a.lstatus = %d';
        $q = parent::prepare_query( $s, 'accommodation', 1 );

        return $this->global->set_option( $q, 'lpost_id', 'ltitle', $laccommodation, true );
    }

    function get_accommodation_room_type_option( $lroom_type = NULL )
    {
        $s = 'SELECT a.lterm_id, a.lname FROM lumonata_post_terms AS a WHERE a.lrule = %s AND a.lgroup = %s';
        $q = parent::prepare_query( $s, 'room_type', 'accommodation' );

        return $this->global->set_option( $q, 'lterm_id', 'lname', $lroom_type, false );
    }

    function get_status_option( $lstatus = NULL )
    {
        return $this->global->set_static_option( array( 'Off', 'On' ), $lstatus, false );
    }

    function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'load' )
            {
                echo $this->load( $mod, $usertype );
            }

            if( $this->post[ 'pKEY' ] == 'do_change_status' )
            {
                echo $this->do_change_status();
            }

            if( $this->post[ 'pKEY' ] == 'do_delete' )
            {
                echo $this->do_delete( $mod );
            }

            if( $this->post[ 'pKEY' ] == 'get_accommodation_rate' )
            {
                echo $this->get_accommodation_rate();
            }

            if( $this->post[ 'pKEY' ] == 'add_rate_period' )
            {
                echo $this->add_rate_period();
            }

            if( $this->post[ 'pKEY' ] == 'delete_rate_period' )
            {
                echo $this->delete_rate_period();
            }
        }

        exit;
    }
}

?> 