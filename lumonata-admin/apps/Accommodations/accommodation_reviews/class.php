<?php

class accommodation_reviews extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->upload   = new upload();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }

    function view( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $this->global->getPViewBoth( $mod, $usertype ) )
        {
            $this->template->set_var( 'mod', $mod );
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'limit', $this->global->getSettingValue( 'list_admin' ) );

            $this->template->set_var( 'lperiod', $this->get_period_option() );
            $this->template->set_var( 'laccommodation', $this->get_accommodation_option() );
            
            $this->template->set_var( 'add_url', $this->global->getAddUrl( $mod, $usertype ) );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Accommodations/accommodation_reviews/view.js' ) );

            $this->template->Parse( 'vC', 'viewContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function insert( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->global->getPInsertBoth( $mod, $usertype ) )
        {
            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->create( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'lname', $lname );
            $this->template->set_var( 'lname_css', '' );

            $this->template->set_var( 'lreview_id', $lreview_id );
            $this->template->set_var( 'lreview_note', $lreview_note );
            $this->template->set_var( 'lreview_title', $lreview_title );
            $this->template->set_var( 'lcreated_date', date( 'd F Y', $lcreated_date ) );
            $this->template->set_var( 'lpost_id', $this->get_accommodation_option( $lpost_id ) );

            $this->template->set_var( 'lstaff_score', $this->set_score_option( 'lstaff_score', $lstaff_score ) );
            $this->template->set_var( 'loverall_score', $this->set_score_option( 'loverall_score', $loverall_score ) );
            $this->template->set_var( 'laccuracy_score', $this->set_score_option( 'laccuracy_score', $laccuracy_score ) );
            $this->template->set_var( 'llocation_score', $this->set_score_option( 'llocation_score', $llocation_score ) );
            $this->template->set_var( 'lactivities_score', $this->set_score_option( 'lactivities_score', $lactivities_score ) );
            $this->template->set_var( 'lcleanliness_score', $this->set_score_option( 'lcleanliness_score', $lcleanliness_score ) );
            
            $this->template->set_var( 'message', $this->message );

            $this->template->set_var( 'site_url', SITE_URL );
            $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
            $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/all.min.css' ) );

            $this->template->Parse( 'fC', 'formContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function edit( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->valid )
        {
            if( $this->global->getPEditBoth( $mod, $usertype ) )
            {
                //-- EXECUTE update action
                if( isset( $this->post[ 'save' ] ) )
                {
                    $this->change( $mod );
                }

                //-- EXTRACT fields
                extract( $this->fields );

                $this->template->set_var( 'lname', $lname );
                $this->template->set_var( 'lname_css', empty( $luser_id ) ? '' : 'disabled' );

                $this->template->set_var( 'lreview_id', $lreview_id );
                $this->template->set_var( 'lreview_note', $lreview_note );
                $this->template->set_var( 'lreview_title', $lreview_title );
                $this->template->set_var( 'lcreated_date', date( 'd F Y', $lcreated_date ) );
                $this->template->set_var( 'lpost_id', $this->get_accommodation_option( $lpost_id ) );

                $this->template->set_var( 'lstaff_score', $this->set_score_option( 'lstaff_score', $lstaff_score ) );
                $this->template->set_var( 'loverall_score', $this->set_score_option( 'loverall_score', $loverall_score ) );
                $this->template->set_var( 'laccuracy_score', $this->set_score_option( 'laccuracy_score', $laccuracy_score ) );
                $this->template->set_var( 'llocation_score', $this->set_score_option( 'llocation_score', $llocation_score ) );
                $this->template->set_var( 'lactivities_score', $this->set_score_option( 'lactivities_score', $lactivities_score ) );
                $this->template->set_var( 'lcleanliness_score', $this->set_score_option( 'lcleanliness_score', $lcleanliness_score ) );
                
                $this->template->set_var( 'message', $this->message );

                $this->template->set_var( 'site_url', SITE_URL );
                $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
                $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
                $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/all.min.css' ) );

                $this->template->Parse( 'fC', 'formContent', true );
            }
            else
            {
                $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
            }
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NOT_FOUND ) );
        }
        
        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function load( $mod, $usertype )
    {
        $cols  = array(
            1 => 'a.lname',
            2 => 'laccommodation',
            3 => 'a.lreview_title',
            4 => 'a.loverall_score',
            5 => 'a.lreview_id'
        );

        //-- Query Limit
        $limit = parent::prepare_query( ' LIMIT ' . $this->post[ 'start' ] . ', ' . $this->post[ 'length' ] );
    
        //-- Query Order By
        if( empty( $this->post[ 'order' ] ) )
        {
            $order = parent::prepare_query( ' ORDER BY a.lcreated_date DESC' );
        }
        else
        {
            $order = array();

            foreach( $this->post[ 'order' ] as $i => $od )
            {
                if( isset( $cols[ $od[ 'column' ] ] ) )
                {
                    $order[] = $cols[ $od[ 'column' ] ] . ' ' . $od[ 'dir' ];
                }
            }

            if( empty( $order ) )
            {
                $order = parent::prepare_query( ' ORDER BY a.lcreated_date DESC' );
            }
            else
            {
                $order = parent::prepare_query( ' ORDER BY ' . implode( ', ', $order ) );
            }
        }

        //-- Query Additional Where
        $w = array();

        if( $this->post[ 'laccommodation' ] != '' )
        {
            $w[] = parent::prepare_query( 'lpost_id = %d', $this->post[ 'laccommodation' ] );
        }

        if( $this->post[ 'lperiod' ] != '' )
        {
            list( $month, $year ) = explode( ', ', $this->post[ 'lperiod' ] );

            $w[] = parent::prepare_query( 'MONTHNAME( FROM_UNIXTIME( a.lcreated_date ) ) = %s AND YEAR( FROM_UNIXTIME( a.lcreated_date ) ) = %s', $month, $year );
        }

        if( $this->post[ 'lsearch' ] != '' )
        {
            $s = array();

            foreach( $cols as $col )
            {
                $s[] = parent::prepare_query( $col . ' LIKE %s', '%' . $this->post[ 'lsearch' ] . '%' );
            }

            $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
        }

        if( empty( $w ) === false )
        {
            $where = ' HAVING ' . implode( ' AND ', $w );
        }
        else
        {
            $where = '';
        }

        //-- Main Query
        $qm = 'SELECT
                a.lname,
                a.lpost_id,
                a.lreview_id,
                a.lreview_title,
                a.loverall_score,
                a.lcreated_date,
                ( 
                    SELECT a2.ltitle 
                    FROM lumonata_post AS a2 
                    WHERE a2.lpost_id = a.lpost_id
                ) AS laccommodation
               FROM lumonata_reviews AS a' . $where . $order;
        $rm = parent::query( $qm );
        $nm = parent::num_rows( $rm );

        $qf = $qm . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        $data = array();

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
                $data[] = array(
                    'leditlink'      => $this->global->getEditUrl( $mod, $usertype, $df[ 'lreview_id' ] ),
                    'lreview_title'  => $df[ 'lreview_title' ],
                    'lname'          => $df[ 'lname' ],
                    'laccommodation' => $df[ 'laccommodation' ],
                    'loverall_score' => $df[ 'loverall_score' ],
                    'lreview_id'     => $df[ 'lreview_id' ]
                );
            }
        }

        return json_encode( array(
            'draw'            => intval( $this->post[ 'draw' ] ),
            'recordsFiltered' => intval( $nm ),
            'recordsTotal'    => intval( $nf ),
            'data'            => $data
        ));
    }

    function validate( $data = array() )
    {
        $error = array();

        foreach( $data as $field => $dt )
        {
            if( $field == 'lreview_note' && $dt == '' )
            {
                array_push( $error, 'Review must have value' );
            }
            elseif( $field == 'lpost_id' && $dt == '' )
            {
                array_push( $error, 'Accommodation must have value' );
            }
        }

        return $error;
    }

    function create( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- INSERT lumonata_reviews
            $param  = array_diff_key( $data, array_flip( array( 'lreview_id' ) ) );

            $result = parent::insert( 'lumonata_reviews', array_filter( $param ) );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to add new review', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=new' );

                exit;
            }
            else
            {
                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully write new review' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=new' );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=new' );

            exit;
        }
    }

    function change( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- UPDATE lumonata_reviews
            $param  = array_diff_key( $data, array_flip( array( 'lreview_id' ) ) );
            $where  = array( 'lreview_id' => $data[ 'lreview_id' ] );

            $result = parent::update( 'lumonata_reviews', array_filter( $param ), $where );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Your review failed to update', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $data[ 'lreview_id' ] );

                exit;
            }
            else
            {
                $this->flash->add( array( 'type' => 'success', 'message' => 'Your review has been updated' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $data[ 'lreview_id' ] );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $data[ 'lreview_id' ] );

            exit;
        }
    }

    function do_delete( $mod )
    {
        $s = 'DELETE FROM lumonata_reviews WHERE lreview_id = %s';
        $q = parent::prepare_query( $s, $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function set_score_option( $name, $score = 0 )
    {
        $random = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 ); 
        $label  = array(
            'laccuracy_score'    => 'Accuracy',
            'llocation_score'    => 'Location',
            'lactivities_score'  => 'Activities',
            'lcleanliness_score' => 'Cleanliness',
            'lstaff_score'       => 'Staff',
            'loverall_score'     => 'Overall'
        );

        $content = '
        <div class="list list-field">
            <label class="list-field-title">' . $label[ $name ] . '</label>
            <div class="rating">
                <input type="radio" id="5-' . $random . '" name="fields[' . $name . ']" value="5" ' . ( $score == 5 ? 'checked' : '' ) . '/><label class="full" for="5-' . $random . '" title="Excellent - 5 stars"></label>
                <input type="radio" id="4-' . $random . '" name="fields[' . $name . ']" value="4" ' . ( $score == 4 ? 'checked' : '' ) . '/><label class="full" for="4-' . $random . '" title="Pretty good - 4 stars"></label>
                <input type="radio" id="3-' . $random . '" name="fields[' . $name . ']" value="3" ' . ( $score == 3 ? 'checked' : '' ) . '/><label class="full" for="3-' . $random . '" title="Good - 3 stars"></label>
                <input type="radio" id="2-' . $random . '" name="fields[' . $name . ']" value="2" ' . ( $score == 4 ? 'checked' : '' ) . '/><label class="full" for="2-' . $random . '" title="Kinda bad - 2 stars"></label>
                <input type="radio" id="1-' . $random . '" name="fields[' . $name . ']" value="1" ' . ( $score == 1 ? 'checked' : '' ) . '/><label class="full" for="1-' . $random . '" title="Bad - 1 stars"></label>
            </div>
        </div>';

        return $content;
    }

    function language()
    {
        $this->lang = $this->global->getSettingValue( 'llanguage' );
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif ); 
        $this->sess    = $this->global->getCurrentSession();
        $this->valid   = true;

        if( isset( $_GET[ 'id' ] ) )
        {
            $s = 'SELECT * FROM lumonata_reviews AS a WHERE a.lreview_id = %d';
            $q = parent::prepare_query( $s, $_GET[ 'id' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $d = parent::fetch_array( $r );

                $fields = array(
                    'luser_id'           => $d[ 'luser_id' ],
                    'lcreated_date'      => $d[ 'lcreated_date' ],
                    'lreview_id'         => $d[ 'lreview_id' ],
                    'lname'              => $d[ 'lname' ],
                    'lpost_id'           => $d[ 'lpost_id' ],
                    'lbooking_id'        => $d[ 'lbooking_id' ],
                    'lreview_title'      => $d[ 'lreview_title' ],
                    'lreview_note'       => $d[ 'lreview_note' ],
                    'laccuracy_score'    => $d[ 'laccuracy_score' ],
                    'llocation_score'    => $d[ 'llocation_score' ],
                    'lactivities_score'  => $d[ 'lactivities_score' ],
                    'lcleanliness_score' => $d[ 'lcleanliness_score' ],
                    'lstaff_score'       => $d[ 'lstaff_score' ],
                    'loverall_score'     => $d[ 'loverall_score' ],
                    'lstatus'            => $d[ 'lstatus' ],
                    'ldlu'               => time(),
                );
            }
            else
            {
                $this->valid = false;
            }
        }
        else
        {
            $fields = array(
                'lcreated_date'      => time(),
                'ldlu'               => time(),
                'lreview_id'         => '',
                'lname'              => '',
                'luser_id'           => '',
                'lbooking_id'        => '',
                'lpost_id'           => '',
                'lreview_title'      => '',
                'lreview_note'       => '', 
                'laccuracy_score'    => 0,
                'llocation_score'    => 0,
                'lactivities_score'  => 0,
                'lcleanliness_score' => 0,
                'lstaff_score'       => 0,
                'loverall_score'     => 0,
                'lstatus'            => 1
            );
        }

        if( empty( $_POST ) )
        {
            if( empty( $this->notif ) === false && isset( $this->notif[ 'post' ] ) )
            {
                $this->fields = array_merge( $fields, $this->notif[ 'post' ] );
            }
            else
            {
                if( isset( $fields ) )
                {
                    $this->fields = $fields;   
                }
            }

            $this->post = array();
        }
        else
        {
            if( isset( $_POST[ 'fields' ] ) )
            {
                $this->fields = array_merge( $fields, $_POST[ 'fields' ] );
            }
            else
            {
                $this->fields = $fields;
            }

            $this->post = array_diff_key( $_POST, array_flip( array( 'fields' ) ) );
        }
    }

    function get_accommodation_option( $lacco_id = NULL )
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = %s AND ( a.lref_id IS NULL OR a.lref_id = 0 ) AND a.lstatus = %d';
        $q = parent::prepare_query( $s, 'accommodation', 1 );

        return $this->global->set_option( $q, 'lpost_id', 'ltitle', $lacco_id, false );
    }

    function get_period_option( $date = NULL )
    {
        $q = 'SELECT DATE_FORMAT( FROM_UNIXTIME( a.lcreated_date ), "%M, %Y" ) AS period FROM lumonata_reviews AS a GROUP BY YEAR( FROM_UNIXTIME( a.lcreated_date ) ) DESC, MONTH( FROM_UNIXTIME( a.lcreated_date ) ) ASC';

        return $this->global->set_option( $q, 'period', 'period', $date, false );
    }

    function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'load' )
            {
                echo $this->load( $mod, $usertype );
            }

            if( $this->post[ 'pKEY' ] == 'do_delete' )
            {
                echo $this->do_delete( $mod );
            }
        }

        exit;
    }
}

?> 