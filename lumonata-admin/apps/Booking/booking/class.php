<?php

class booking extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->upload   = new upload();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }

    function view( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $this->global->getPViewBothP( $mod, $usertype ) )
        {
            extract( $this->fields );

            $this->template->set_var( 'mod', $mod );
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'lstatus', $this->get_status_option() );
            $this->template->set_var( 'lpost_id', $this->get_accommodation_option() );
            $this->template->set_var( 'limit', $this->global->getSettingValue( 'list_admin' ) );

            $this->template->set_var( 'site_url', SITE_URL );
            $this->template->set_var( 'apps_url', ADMIN_APPS_URL . '/Booking/booking' );
            
            $this->template->set_var( 'add_url', $this->global->getAddUrl( $mod, $usertype ) );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Booking/booking/view.js' ) );

            $this->template->Parse( 'vC', 'viewContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function insert( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->global->getPInsertBoth( $mod, $usertype ) )
        {
            extract( $this->fields );

            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->create( $mod );
            }

            $this->template->set_var( 'lnote', $lnote );
            $this->template->set_var( 'lfname', $lfname );
            $this->template->set_var( 'llname', $llname );
            $this->template->set_var( 'lemail', $lemail );
            $this->template->set_var( 'lphone', $lphone );
            $this->template->set_var( 'lnights', $lnights );
            $this->template->set_var( 'lbooking_id', $lbooking_id );

            $this->template->set_var( 'message', $this->message );

            $this->template->set_var( 'htitle', 'Add New Booking' );
            $this->template->set_var( 'hdesc', 'Create a new booking' );

            $this->template->set_var( 'ltitle', $this->get_title_option( $ltitle ) );
            $this->template->set_var( 'lguests', $this->get_guest_list( $lguests ) );
            $this->template->set_var( 'lpost_id', $this->get_accommodation_option() );

            $this->template->set_var( 'gtype', $this->get_type_option() );
            $this->template->set_var( 'gtitle', $this->get_title_option() );

            $this->template->set_var( 'lcheck_in', date( 'd F Y', $lcheck_in ) );
            $this->template->set_var( 'lcheck_out', date( 'd F Y', $lcheck_out ) );
            $this->template->set_var( 'lcreated_date', date( 'd F Y', $lcreated_date ) );

            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Booking/booking/form.js' ) );

            $this->template->Parse( 'fC', 'formContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function edit( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->valid )
        {
            if( $this->global->getPEditBoth( $mod, $usertype ) )
            {
                //-- EXECUTE update action
                if( isset( $this->post[ 'save' ] ) )
                {
                    $this->change( $mod );
                }

                //-- EXTRACT fields
                extract( $this->fields );

                $this->template->set_var( 'lnote', $lnote );
                $this->template->set_var( 'lfname', $lfname );
                $this->template->set_var( 'llname', $llname );
                $this->template->set_var( 'lemail', $lemail );
                $this->template->set_var( 'lphone', $lphone );
                $this->template->set_var( 'lnights', $lnights );
                $this->template->set_var( 'lbooking_id', $lbooking_id );

                $this->template->set_var( 'message', $this->message );

                $this->template->set_var( 'htitle', sprintf( 'Update Booking #%s', $lbooking_code ) );
                $this->template->set_var( 'hdesc', 'Edit existing booking' );

                $this->template->set_var( 'ltitle', $this->get_title_option( $ltitle ) );
                $this->template->set_var( 'lguests', $this->get_guest_list( $lguests ) );
                $this->template->set_var( 'lstatus', $this->get_status_label( $lstatus ) );
                $this->template->set_var( 'lpost_id', $this->get_accommodation_option( $lpost_id ) );
                $this->template->set_var( 'ldetails', $this->get_accommodation_detail( $lpost_id, false ) );

                $this->template->set_var( 'lcheck_in', date( 'd F Y', $lcheck_in ) );
                $this->template->set_var( 'lcheck_out', date( 'd F Y', $lcheck_out ) );
                $this->template->set_var( 'lcreated_date', date( 'd F Y', $lcreated_date ) );

                $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js' ) );

                $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Booking/booking/form.js' ) );

                $this->template->Parse( 'fC', 'formContent', true );
            }
            else
            {
                $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
            }
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NOT_FOUND ) );
        }
        
        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function popup()
    {
        $data = $this->global->getBooking( $this->post[ 'bid' ] );

        if( empty( $data ) === false )
        {
            $data[ 'lposts_data' ] = $this->global->getPosts( array( 'lpost_id' => $data[ 'lpost_id' ] ) );

            $this->template->set_file( 'popup', 'popup.html' );

            $this->template->set_block( 'popup', 'entireViewBlock', 'etvB' );

            $this->template->set_block( 'popup', 'tripsViewLoopBlock', 'trvlB' );
            $this->template->set_block( 'popup', 'tripsViewBlock', 'trvB' );

            $this->template->set_block( 'popup', 'roomsViewLoopBlock', 'rcvlB' );
            $this->template->set_block( 'popup', 'roomsViewBlock', 'rcvB' );

            $this->template->set_block( 'popup', 'addonsViewLoopBlock', 'advlB' );
            $this->template->set_block( 'popup', 'addonsViewBlock', 'advB' );

            $this->template->set_block( 'popup', 'guestViewLoopBlock', 'gsvlB' );
            $this->template->set_block( 'popup', 'guestViewBlock', 'gsvB' );

            $this->template->set_block( 'popup', 'popupViewBlock', 'pvB' );

            $this->template->set_var( 'lfullname', sprintf( '%s %s', $data[ 'ltitle' ], $data[ 'lfullname' ] ) );
            $this->template->set_var( 'lstatus_option', $this->get_status_option( $data[ 'lstatus' ] ) );
            $this->template->set_var( 'lcreated_date', date( 'd F Y', $data[ 'lcreated_date' ] ) );
            $this->template->set_var( 'lstatus', $this->get_status_label( $data[ 'lstatus' ] ) );
            $this->template->set_var( 'ltitle', $this->get_title_option( $data[ 'ltitle' ] ) );
            $this->template->set_var( 'lcheckout', date( 'd F Y', $data[ 'lcheck_out' ] ) );
            $this->template->set_var( 'lcheckin', date( 'd F Y', $data[ 'lcheck_in' ] ) );
            $this->template->set_var( 'lbooking_code', $data[ 'lbooking_code' ] );
            $this->template->set_var( 'lbooking_id',$data[ 'lbooking_id' ] );
            $this->template->set_var( 'lnights', $data[ 'lnights' ] );
            $this->template->set_var( 'lfname', $data[ 'lfname' ] );
            $this->template->set_var( 'llname', $data[ 'llname' ] );
            $this->template->set_var( 'lemail', $data[ 'lemail' ] );
            $this->template->set_var( 'lphone', $data[ 'lphone' ] );
            $this->template->set_var( 'lnote', $data[ 'lnote' ] );

            if( $data[ 'lposts_data' ][ 'page_template' ] == '3' && $data[ 'lprice' ] > 0 )
            {
                $this->template->set_var( 'eprice', $data[ 'lprice' ] );
                $this->template->set_var( 'efprice', $this->global->get_format_price( $data[ 'lprice' ], $data[ 'lpost_id' ] ) );

                $this->template->parse( 'etvB', 'entireViewBlock', true );
            }

            if( empty( $data[ 'ltrips' ] ) === false )
            {
                $no = 1;

                foreach( $data[ 'ltrips' ] as $rm )
                {
                    $guest = $data[ 'ladult' ] + $data[ 'lchild' ] + $data[ 'linfant' ];
                    $total = $rm[ 'price' ] * $guest * $data[ 'lnights' ];

                    $detail = array();

                    if( isset( $rm[ 'duration' ] ) && !empty( $rm[ 'duration' ] ) )
                    {
                        $detail[] = sprintf( 'Duration : %s', $rm[ 'duration' ] );
                    }

                    if( isset( $rm[ 'departure' ] ) && !empty( $rm[ 'departure' ] ) )
                    {
                        $detail[] = sprintf( 'Departure : %s', $rm[ 'departure' ] );
                    }

                    if( isset( $rm[ 'itinerary' ] ) && !empty( $rm[ 'itinerary' ] ) )
                    {
                        $detail[] = sprintf( 'Itinerary : %s', $rm[ 'itinerary' ] );
                    }

                    if( empty( $detail ) )
                    {
                        $detail = '-';
                    }
                    else
                    {
                        $detail = implode( '<br />', $detail );
                    }

                    $this->template->set_var( 'no', $no );
                    $this->template->set_var( 'total', $total );
                    $this->template->set_var( 'guest', $guest );
                    $this->template->set_var( 'detail', $detail );
                    $this->template->set_var( 'sid', $rm[ 'id' ] );
                    $this->template->set_var( 'price', $rm[ 'price' ] );
                    $this->template->set_var( 'night', $data[ 'lnights' ] );
                    $this->template->set_var( 'duration', $rm[ 'duration' ] );
                    $this->template->set_var( 'departure', $rm[ 'departure' ] );
                    $this->template->set_var( 'itinerary', $rm[ 'itinerary' ] );

                    $this->template->set_var( 'ftotal', $this->global->get_format_price( $total, $data[ 'lpost_id' ] ) );
                    $this->template->set_var( 'fprice', $this->global->get_format_price( $rm[ 'price' ], $data[ 'lpost_id' ] ) );

                    $this->template->parse( 'trvlB', 'tripsViewLoopBlock', true );

                    $no++;
                }

                $this->template->set_var( 'trip_total', $this->global->get_format_price( $data[ 'lroom_total' ], $data[ 'lpost_id' ] ) );

                $this->template->parse( 'trvB', 'tripsViewBlock', false );
            }

            if( empty( $data[ 'lrooms' ] ) === false )
            {
                $no = 1;

                foreach( $data[ 'lrooms' ] as $rm )
                {
                    $this->template->set_var( 'no', $no );
                    $this->template->set_var( 'qty', $rm[ 'num' ] );
                    $this->template->set_var( 'name', $rm[ 'name' ] );
                    $this->template->set_var( 'price', $rm[ 'price' ] );
                    $this->template->set_var( 'subtotal', $rm[ 'total' ] );

                    $this->template->set_var( 'fprice', $this->global->get_format_price( $rm[ 'price' ], $data[ 'lpost_id' ] ) );
                    $this->template->set_var( 'fsubtotal', $this->global->get_format_price( $rm[ 'total' ], $data[ 'lpost_id' ] ) );

                    $this->template->parse( 'rcvlB', 'roomsViewLoopBlock', true );

                    $no++;
                }

                $this->template->set_var( 'room_total', $data[ 'lroom_total' ] );
                $this->template->set_var( 'froom_total', $this->global->get_format_price( $data[ 'lroom_total' ], $data[ 'lpost_id' ] ) );

                $this->template->parse( 'rcvB', 'roomsViewBlock', false );
            }

            if( empty( $data[ 'laddons' ] ) === false )
            {
                $no = 1;

                foreach( $data[ 'laddons' ] as $rm )
                {
                    $this->template->set_var( 'no', $no );
                    $this->template->set_var( 'qty', $rm[ 'num' ] );
                    $this->template->set_var( 'name', $rm[ 'name' ] );
                    $this->template->set_var( 'price', $rm[ 'price' ] );
                    $this->template->set_var( 'subtotal', $rm[ 'total' ] );

                    $this->template->set_var( 'fprice', $this->global->get_format_price( $rm[ 'price' ], $data[ 'lpost_id' ] ) );
                    $this->template->set_var( 'fsubtotal', $this->global->get_format_price( $rm[ 'total' ], $data[ 'lpost_id' ] ) );

                    $this->template->parse( 'advlB', 'addonsViewLoopBlock', true );

                    $no++;
                }

                $this->template->set_var( 'addons_total', $data[ 'laddons_total' ] );
                $this->template->set_var( 'faddons_total', $this->global->get_format_price( $data[ 'laddons_total' ], $data[ 'lpost_id' ] ) );

                $this->template->parse( 'advB', 'addonsViewBlock', false );
            }
            
            if( empty( $data[ 'lguests' ] ) === false )
            {
                $no  = 1;
                $num = 0;

                foreach( $data[ 'lguests' ] as $type => $guest )
                {
                    $num += count( $guest );

                    foreach( $guest as $idx => $gs )
                    {
                        $this->template->set_var( 'firstname', $gs[ 'firstname' ] );
                        $this->template->set_var( 'lastname', $gs[ 'lastname' ] );
                        $this->template->set_var( 'gtype', ucfirst( $type ) );
                        $this->template->set_var( 'idx', $idx );
                        $this->template->set_var( 'no', $no );

                        if( $type == 'adult' )
                        {
                            $this->template->set_var( 'name', sprintf( '%s %s %s', $gs[ 'title' ], $gs[ 'firstname' ], $gs[ 'lastname' ] ) );
                        }
                        else
                        {
                            $this->template->set_var( 'name', sprintf( '%s %s', $gs[ 'firstname' ], $gs[ 'lastname' ] ) );
                        }

                        $this->template->parse( 'gsvlB', 'guestViewLoopBlock', true );

                        $no++;
                    }
                }

                $this->template->set_var( 'guest_num', $num );

                $this->template->parse( 'gsvB', 'guestViewBlock', false );
            }

            $this->template->set_var( 'acc_name', $data[ 'lposts_data' ][ 'ltitle' ] );

            $this->template->set_var( 'ltotal', $data[ 'ltotal' ] );
            $this->template->set_var( 'ldeposit', $data[ 'ldeposit' ] );
            $this->template->set_var( 'lbalance', $data[ 'lbalance' ] );

            $this->template->set_var( 'ftotal', $this->global->get_format_price( $data[ 'ltotal' ], $data[ 'lpost_id' ] ) );
            $this->template->set_var( 'fdeposit', $this->global->get_format_price( $data[ 'ldeposit' ], $data[ 'lpost_id' ] ) );
            $this->template->set_var( 'fbalance', $this->global->get_format_price( $data[ 'lbalance' ], $data[ 'lpost_id' ] ) );
            
            $this->template->parse( 'pvB', 'popupViewBlock', false );

            return $this->template->finish( $this->template->parse( 'Output', 'popup', false ) );
        }
    }

    function load( $mod, $usertype )
    {
        $cols  = array( 
            0 => 'a.lbooking_code', 
            1 => 'a.lcreated_date',
            2 => 'a.lfullname',
            3 => 'laccommodation',
        );

        //-- Query Limit
        $limit = parent::prepare_query( ' LIMIT ' . $this->post[ 'start' ] . ', ' . $this->post[ 'length' ] );
    
        //-- Query Order By
        if( empty( $this->post[ 'order' ] ) )
        {
            $order = parent::prepare_query( ' ORDER BY a.lcreated_date DESC' );
        }
        else
        {
            $order = array();

            foreach( $this->post[ 'order' ] as $i => $od )
            {
                if( isset( $cols[ $od[ 'column' ] ] ) )
                {
                    $order[] = $cols[ $od[ 'column' ] ] . ' ' . $od[ 'dir' ];
                }
            }

            if( empty( $order ) )
            {
                $order = parent::prepare_query( ' ORDER BY a.lcreated_date DESC' );
            }
            else
            {
                $order = parent::prepare_query( ' ORDER BY ' . implode( ', ', $order ) );
            }
        }

        //-- Query Additional Where
        $w = array();

        if( $this->post[ 'lpost_id' ] != '' )
        {
            $w[] = parent::prepare_query( 'a.lpost_id = %d', $this->post[ 'lpost_id' ] );
        }

        if( $this->post[ 'lstatus' ] != '' )
        {
            $w[] = parent::prepare_query( 'a.lstatus = %d', $this->post[ 'lstatus' ] );
        }

        if( $this->post[ 'lsearch' ] != '' )
        {
            $s = array();

            foreach( $cols as $col )
            {
                $s[] = parent::prepare_query( $col . ' LIKE %s', '%' . $this->post[ 'lsearch' ] . '%' );
            }

            $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
        }

        if( empty( $w ) === false )
        {
            $where = ' HAVING ' . implode( ' AND ', $w );
        }
        else
        {
            $where = '';
        }

        //-- Main Query
        $qm = 'SELECT 
                a.*, (
                    SELECT a2.ltitle FROM lumonata_post AS a2 WHERE a2.lpost_id = a.lpost_id
                ) AS laccommodation
               FROM lumonata_booking AS a' . $where . $order;
        $rm = parent::query( $qm );
        $nm = parent::num_rows( $rm );

        $qf = $qm . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        $data = array();

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
                $data[] = array(
                    'leditlink'      => $this->global->getEditUrl( $mod, $usertype, $df[ 'lbooking_id' ] ),
                    'lpaymentlink'   => $this->global->getPaymentUrl( $df[ 'lbooking_id' ] ),
                    'lcreated_date'  => date( 'd F Y', $df[ 'lcreated_date' ] ),
                    'laccommodation' => $df[ 'laccommodation' ],
                    'lbooking_code'  => $df[ 'lbooking_code' ],
                    'lbooking_id'    => $df[ 'lbooking_id' ],
                    'lfullname'      => $df[ 'lfullname' ],
                    'lstatus'        => $df[ 'lstatus' ]
                );
            }
        }

        return json_encode( array(
            'draw'            => intval( $this->post[ 'draw' ] ),
            'recordsFiltered' => intval( $nm ),
            'recordsTotal'    => intval( $nf ),
            'data'            => $data
        ));
    }

    function validate( $data = array() )
    {
        $error = array();

        foreach( $data as $field => $dt )
        {
            if( $field == 'ltitle' && $dt == '' )
            {
                array_push( $error, 'Title must have value' );
            }

            if( $field == 'lfname' && $dt == '' )
            {
                array_push( $error, 'First name must have value' );
            }

            if( $field == 'llname' && $dt == '' )
            {
                array_push( $error, 'Last name must have value' );
            }

            if( $field == 'lemail' && $dt == '' )
            {
                array_push( $error, 'Email must have value' );
            }

            if( $field == 'lphone' && $dt == '' )
            {
                array_push( $error, 'Phone must have value' );
            }

            if( $field == 'lcheck_in' && $dt == '' )
            {
                array_push( $error, 'Check-in date must have value' );
            }

            if( $field == 'lcheck_out' && $dt == '' )
            {
                array_push( $error, 'Check-out date must have value' );
            }

            if( $field == 'lrooms' && empty( $dt ) && !isset( $data[ 'lprice' ] ) )
            {
                array_push( $error, 'You not selected any room yet' );
            }

            if( $field == 'lpackage_id' && empty( $dt ) && !isset( $data[ 'lprice' ] ) )
            {
                array_push( $error, 'You not selected any package yet' );
            }

            if( $field == 'lschedule_id' && empty( $dt ) && !isset( $data[ 'lprice' ] ) )
            {
                array_push( $error, 'You not selected any schedule yet' );
            }
        }

        return $error;
    }

    function create( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            if( isset( $data[ 'lrooms' ] ) && !empty( $data[ 'lrooms' ] ) )
            {
                foreach( $data[ 'lrooms' ] as $idx => $rm )
                {
                    if( !isset( $rm[ 'id' ] ) )
                    {
                        unset( $data[ 'lrooms' ][ $idx ] );
                    }
                }

                $data[ 'lrooms' ] = json_encode( $data[ 'lrooms' ] );
            }

            if( isset( $data[ 'laddons' ] ) && !empty( $data[ 'laddons' ] ) )
            {
                foreach( $data[ 'laddons' ] as $idx => $ad )
                {
                    if( !isset( $ad[ 'id' ] ) )
                    {
                        unset( $data[ 'laddons' ][ $idx ] );
                    }
                }

                $data[ 'laddons' ] = json_encode( $data[ 'laddons' ] );
            }

            if( isset( $data[ 'lguests' ] ) && !empty( $data[ 'lguests' ] ) )
            {
                $data[ 'lguests' ] = json_encode( $data[ 'lguests' ] );
            }

            $data = array_merge( $data, array( 
                'lbooking_code' => $this->global->uniqueCode( 'NSF', 'lumonata_booking', 'lbooking_code' ),
                'lfullname'     => sprintf( '%s %s', $data[ 'lfname' ], $data[ 'llname' ] ),
                'lcreated_date' => strtotime( $data[ 'lcreated_date' ] ),
                'lcheck_out'    => strtotime( $data[ 'lcheck_out' ] ),
                'lcheck_in'     => strtotime( $data[ 'lcheck_in' ] ),
                'lcurrency_id'  => $data[ 'lcurrency_id' ],
            ));

            $result = parent::insert( 'lumonata_booking', array_filter( $data ) );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to add new booking', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=new' );

                exit;
            }
            else
            {
                $bid = parent::insert_id();

                $this->global->sendBookingRequestEmail( $bid );

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully add new booking' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=new' );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=new' );

            exit;
        }
    }

    function change( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            if( isset( $data[ 'lrooms' ] ) && !empty( $data[ 'lrooms' ] ) )
            {
                foreach( $data[ 'lrooms' ] as $idx => $rm )
                {
                    if( !isset( $rm[ 'id' ] ) )
                    {
                        unset( $data[ 'lrooms' ][ $idx ] );
                    }
                }

                $data[ 'lrooms' ] = json_encode( $data[ 'lrooms' ] );
            }

            if( isset( $data[ 'laddons' ] ) && !empty( $data[ 'laddons' ] ) )
            {
                foreach( $data[ 'laddons' ] as $idx => $ad )
                {
                    if( !isset( $ad[ 'id' ] ) )
                    {
                        unset( $data[ 'laddons' ][ $idx ] );
                    }
                }

                $data[ 'laddons' ] = json_encode( $data[ 'laddons' ] );
            }

            if( isset( $data[ 'lguests' ] ) && !empty( $data[ 'lguests' ] ) )
            {
                $data[ 'lguests' ] = json_encode( $data[ 'lguests' ] );
            }

            $data = array_merge( $data, array( 
                'lfullname'       => sprintf( '%s %s', $data[ 'lfname' ], $data[ 'llname' ] ),
                'lcreated_date'   => strtotime( $data[ 'lcreated_date' ] ),
                'lcheck_out'      => strtotime( $data[ 'lcheck_out' ] ),
                'lcheck_in'       => strtotime( $data[ 'lcheck_in' ] ),
                'lcurrency_id'    => $data[ 'lcurrency_id' ],
                'llast_edited_by' => $data[ 'lusername' ],
                'llast_edited'    => time()
            ));

            //-- UPDATE lumonata_booking
            $param  = array_filter( $data );
            $where  = array( 'lbooking_id' => $data[ 'lbooking_id' ] );
            $result = parent::update( 'lumonata_booking', $param, $where );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to update booking data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $data[ 'lbooking_id' ] );

                exit;
            }
            else
            {
                $bid = parent::insert_id();

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully update booking data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $data[ 'lbooking_id' ] );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $data[ 'lbooking_id' ] );

            exit;
        }
    }

    function get_status_label( $lstatus = '' )
    {
        $label = array( 
            'Check Availability', 
            'Confirmed',
            'Refused',
            'Request Alternative Dates',
        );

        if( isset( $label[ $lstatus ] ) )
        {
            return $label[ $lstatus ];
        }
        else
        {
            return $label;
        }
    }

    function get_status_option( $lstatus = NULL )
    {
        return $this->global->set_static_option( $this->get_status_label(), $lstatus, false );
    }

    function get_type_option( $type = '' )
    {
        return $this->global->set_static_option( array( 
            'adult'  => 'Adult',
            'child'  => 'Child',
            'infant' => 'Infant'
        ), $type, false );
    }

    function get_title_option( $ltitle = '' )
    {
        return $this->global->set_static_option( array( 
            'Mr.'   => 'Mr.',
            'Mrs.'  => 'Mrs.',
            'Miss.' => 'Miss.' 
        ), $ltitle, false );
    }

    function get_room_type_option( $room = '', $posts = array() )
    {
        if( isset( $posts[ 'room_type' ] ) && !empty( $posts[ 'room_type' ] ) )
        {
            $opt = array();

            foreach( $posts[ 'room_type' ] as $d )
            {
                $opt[ $d[ 'lterm_id' ] ] = $d[ 'lname' ];
            }

            return $this->global->set_static_option( $opt, $room, false );
        }
    }

    function get_addons_option( $addons = '' )
    {
        $s = 'SELECT a.lterm_id, a.lname FROM lumonata_post_terms AS a WHERE a.lrule = %s AND a.lgroup = %s AND a.lstatus = %s';
        $q = parent::prepare_query( $s, 'accommodation_add_ons', 'accommodation', 1 );

        return $this->global->set_option( $q, 'lterm_id', 'lname', strval( $addons ), true, null, null );
    }

    function get_accommodation_option( $lacco_id = NULL )
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = %s AND ( a.lref_id IS NULL OR a.lref_id = 0 ) AND a.lstatus = %d';
        $q = parent::prepare_query( $s, 'accommodation', 1 );

        return $this->global->set_option( $q, 'lpost_id', 'ltitle', $lacco_id, false );
    }

    function check_availability( $room_id )
    {
        $valid = true;

        if( isset( $this->post[ 'lpost_id' ] ) && isset( $this->post[ 'lcheckin' ] ) && isset( $this->post[ 'lcheckout' ] ) )
        {
            $s = 'SELECT * FROM lumonata_calendar AS a WHERE a.lpost_id = %d AND a.lterm_id = %d AND a.ldate BETWEEN %s AND %s';
            $q = parent::prepare_query( $s, $this->post[ 'lpost_id' ], $room_id, $this->post[ 'lcheckin' ], $this->post[ 'lcheckout' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                while( $d = parent::fetch_array( $r ) )
                {
                    if( $d[ 'lstatus' ] == 2 )
                    {
                        $valid = false;
                    }
                }
            }
        }

        return $valid;
    }

    function get_accommodation_detail( $lpost_id = '', $is_ajax = true )
    {
        if( $is_ajax === true )
        {
            if( $this->post[ 'lcheckin' ] == '' || $this->post[ 'lcheckout' ] == '' )
            {
                return json_encode( array( 'result' => 'failed', 'message' => 'Please check again your check in/out date' ) );
            }
            else
            {
                $data = $this->global->getPosts( array( 'lpost_id' => $this->post[ 'lpost_id' ] ) );
                $resv = $this->global->getBooking( $this->post[ 'lbooking_id' ] );

                if( isset( $resv[ 'lpost_id' ] ) && $resv[ 'lpost_id' ] == $this->post[ 'lpost_id' ]  )
                {
                    $content = $this->get_booking_accommodation_detail( $this->post[ 'lpost_id' ], $data, $resv );
                }
                else
                {
                    $content = $this->get_new_accommodation_detail( $this->post[ 'lpost_id' ], $data );
                }

                if( empty( $content ) === false )
                {
                    return json_encode( array( 'result' => 'success', 'content' => $content ) );
                }
                else
                {
                    return json_encode( array( 'result' => 'failed', 'message' => 'Failed to load detail property' ) );
                }
            }
        }
        else
        {
            $data = $this->global->getPosts( array( 'lpost_id' => $lpost_id ) );
            $resv = $this->global->getBooking( $_GET[ 'id' ] );

            return $this->get_booking_accommodation_detail( $lpost_id, $data, $resv );
        }
    }

    function get_booking_accommodation_detail( $lpost_id, $data, $resv )
    {
        //-- GET property currency
        $currency = $this->global->get_currency_sign( $resv[ 'lcurrency_id' ], false );
        $content  = '';

        if( empty( $resv[ 'lpackage_id' ] ) === false )
        {
            //-- GET packages
            $packages = $this->global->getPackages( array( 'lpost_id' => $lpost_id, 'lstatus'  => 1 ), false );

            if( empty( $packages ) === false )
            {
                $options = '';

                foreach( $packages as $d )
                {
                    if( $d[ 'lpackage_id' ] == $resv[ 'lpackage_id' ] )
                    {
                        $options .= '<option value="' . $d[ 'lpackage_id' ] . '" data-price="' . $d[ 'lprice' ] . '" selected="selected">' . $d[ 'lname' ] . '</option>';
                    }
                    else
                    {
                        $options .= '<option value="' . $d[ 'lpackage_id' ] . '" data-price="' . $d[ 'lprice' ] . '">' . $d[ 'lname' ] . '</option>';
                    }
                }

                $content .= '
                <tr class="back-soft-gray">
                    <td class="text-left" colspan="3">Package Name</td>
                    <td class="text-right" width="125">Price</td>
                </tr>
                <tr class="packages-items">
                    <td class="text-left"  colspan="3">
                        <select class="popup-field-select field-package-id" name="fields[lpackage_id]">
                            ' . $options . '
                        </select>
                    </td>
                    <td class="text-right" width="125">
                        <input class="popup-field-text text-number text-right field-package-price" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . '" type="text" name="fields[lprice]" value="' . $resv[ 'lprice' ] . '" autocomplete="off" readonly />
                    </td>
                </tr>';
            }
        }
        else if( empty( $resv[ 'lschedule_id' ] ) === false )
        {
            //-- GET schedules
            $schedules = $this->global->getSchedule( array( 'lpost_id' => $lpost_id, 'lstatus'  => 1 ), false );

            if( empty( $schedules ) === false )
            {
                $options = '';

                foreach( $schedules as $d )
                {
                    $datelist = sprintf( 'Trip %s ( %s - %s )', $d[ 'litinerary' ], date( 'd/m/Y', strtotime( $d[ 'lcheck_in' ] ) ), date( 'd/m/Y', strtotime( $d[ 'lcheck_out' ] ) ) );

                    if( $d[ 'lschedule_id' ] == $resv[ 'lschedule_id' ] )
                    {
                        $options .= '<option value="' . $d[ 'lschedule_id' ] . '" data-price="' . $d[ 'lprice' ] . '" selected="selected">' . $datelist . '</option>';
                    }
                    else
                    {
                        $options .= '<option value="' . $d[ 'lschedule_id' ] . '" data-price="' . $d[ 'lprice' ] . '">' . $datelist . '</option>';
                    }
                }

                $content .= '
                <tr class="back-soft-gray">
                    <td class="text-left" colspan="3">Schedules</td>
                    <td class="text-right" width="125">Price</td>
                </tr>
                <tr class="schedules-items">
                    <td class="text-left" colspan="3">
                        <select class="popup-field-select field-schedule-id" name="fields[lschedule_id]">
                            <option value="" data-price="0">Select Schedule</option>
                            ' . $options . '
                        </select>
                    </td>
                    <td class="text-right" width="125">
                        <input class="popup-field-text text-number text-right field-schedule-price" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . '" type="text" name="fields[lprice]" value="' . $resv[ 'lprice' ] . '" autocomplete="off" readonly />
                    </td>
                </tr>';
            }
        }
        else
        {
            if( isset( $data[ 'room_type' ] ) && !empty( $data[ 'room_type' ] ) )
            {
                $roomcont = '';

                $no = 1;

                foreach( $data[ 'room_type' ] as $d )
                {
                    if( isset( $resv[ 'lrooms' ][ $d[ 'lterm_id' ] ] ) )
                    {                        
                        $checked = 'checked';
                        $disable = '';
                    }
                    else
                    {
                        $checked = '';
                        $disable = 'disabled';

                        //-- CHECK availability if policy = 0 
                        //-- and room not in existing booking
                        if( isset( $data[ 'policy' ] ) && $data[ 'policy' ] == '0' )
                        {
                            if( $this->check_availability( $d[ 'lterm_id' ] ) === false )
                            {
                                continue;
                            }
                        }
                    }

                    $night    = empty( $resv[ 'lnights' ] ) ? 0 : $resv[ 'lnights' ];
                    $opt      = $this->get_room_type_option( $d[ 'lterm_id' ], $data );
                    $price    = $this->global->roomPrice( $data, $d );
                    $subtotal = $night * $price;

                    $roomcont .= '
                    <tr class="room-items">
                        <td class="text-left field-room-id-' . $no . '" style="vertical-align:middle;">
                            <label for="room-' . $d[ 'lterm_id' ] . '">
                                <input id="room-' . $d[ 'lterm_id' ] . '" type="checkbox" name="fields[lrooms][' . $d[ 'lterm_id' ] . '][id]" value="' . $d[ 'lterm_id' ] . '" autocomplete="off" ' . $checked . ' />
                                <span>' . $d[ 'lname' ] . '</span>
                            </label>
                        </td>
                        <td class="text-right" width="100">
                            <input class="popup-field-text field-room-name-' . $no . '" type="hidden" name="fields[lrooms][' . $d[ 'lterm_id' ] . '][name]" value="' . $d[ 'lname' ] . '" disabled />
                            <input class="popup-field-text text-number text-right field-room-qty-' . $no . '" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="" type="text" name="fields[lrooms][' . $d[ 'lterm_id' ] . '][num]" value="1" autocomplete="off" ' . $disable . ' />
                        </td>
                        <td class="text-right" width="125">
                            <input class="popup-field-text text-number text-right field-room-price-' . $no . '" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' " type="text" name="fields[lrooms][' . $d[ 'lterm_id' ] . '][price]" value="' . $price . '" autocomplete="off" ' . $disable . ' readonly />
                        </td>
                        <td class="text-right" width="125">
                            <input class="popup-field-text text-number text-right field-room-subtotal-' . $no . '" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' " type="text" name="fields[lrooms][' . $d[ 'lterm_id' ] . '][total]" value="' . $subtotal . '" autocomplete="off" ' . $disable . ' readonly />
                        </td>
                    </tr>';

                    $no++;  
                }

                if( $no > 1 )
                {
                    $content .= '
                    <tr class="back-soft-gray">
                        <td class="text-left">Room Name</td>
                        <td class="text-right" width="100">Qty</td>
                        <td class="text-right" width="125">Price</td>
                        <td class="text-right" width="125">Sub Total</td>
                    </tr>
                    ' . $roomcont . '
                    <tr class="back-soft-gray">
                        <td class="text-right middle" colspan="3">Total</td>
                        <td class="text-right">
                            <input class="popup-field-text text-number text-right field-total field-room-total" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' " type="text" name="fields[lroom_total]"  value="0" autocomplete="off" readonly />
                        </td>
                    </tr>';
                }
            }
        }

        if( empty( $content ) === false )
        {
            if( isset( $data[ 'accommodation_add_ons' ] ) && !empty( $data[ 'accommodation_add_ons' ] ) )
            {
                $adds = '';
                $no   = 1;

                foreach( $data[ 'accommodation_add_ons' ] as $d )
                {
                    //-- CONTINUE if status addons is temporary
                    if( $d[ 'lstatus' ] == 2 )
                    {
                        continue;
                    }

                    //-- SET variable
                    $num   = isset( $resv[ 'laddons' ][ $d[ 'lterm_id' ] ][ 'num' ] ) ? $resv[ 'laddons' ][ $d[ 'lterm_id' ] ][ 'num' ] : 1;
                    $name  = isset( $resv[ 'laddons' ][ $d[ 'lterm_id' ] ][ 'name' ] ) ? $resv[ 'laddons' ][ $d[ 'lterm_id' ] ][ 'name' ] : $d[ 'lname' ];
                    $price = isset( $resv[ 'laddons' ][ $d[ 'lterm_id' ] ][ 'price' ] ) ? $resv[ 'laddons' ][ $d[ 'lterm_id' ] ][ 'price' ] : $d[ 'addons_price' ];
                    $total = isset( $resv[ 'laddons' ][ $d[ 'lterm_id' ] ][ 'total' ] ) ? $resv[ 'laddons' ][ $d[ 'lterm_id' ] ][ 'total' ] : $d[ 'addons_price' ];

                    if( isset( $resv[ 'laddons' ][ $d[ 'lterm_id' ] ] ) )
                    {
                        $checked = 'checked';
                        $disable = '';
                    }
                    else
                    {
                        $checked = '';
                        $disable = 'disabled';
                    }

                    $adds .= '
                    <tr class="addons-items">
                        <td class="text-left field-addons-id-' . $no . '" style="vertical-align:middle;">
                            <label for="addons-' . $d[ 'lterm_id' ] . '">
                                <input id="addons-' . $d[ 'lterm_id' ] . '" type="checkbox" name="fields[laddons][' . $d[ 'lterm_id' ] . '][id]" value="' . $d[ 'lterm_id' ] . '" autocomplete="off" ' . $checked . ' />
                                <span>' . $d[ 'lname' ] . '</span>
                            </label>
                        </td>
                        <td class="text-right" width="100">
                            <input class="popup-field-text field-addons-name-' . $no . '" type="hidden" name="fields[laddons][' . $d[ 'lterm_id' ] . '][name]" value="' . $name . '" ' . $disable . ' />
                            <input class="popup-field-text text-number text-right field-addons-qty-' . $no . '" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="" type="text" name="fields[laddons][' . $d[ 'lterm_id' ] . '][num]" value="' . $num . '" autocomplete="off" ' . $disable . ' />
                        </td>
                        <td class="text-right" width="125">
                            <input class="popup-field-text text-number text-right field-addons-price-' . $no . '" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' " type="text" name="fields[laddons][' . $d[ 'lterm_id' ] . '][price]" value="' . $price . '" autocomplete="off" ' . $disable . ' readonly />
                        </td>
                        <td class="text-right" width="125">
                            <input class="popup-field-text text-number text-right field-addons-subtotal-' . $no . '" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' " type="text" name="fields[laddons][' . $d[ 'lterm_id' ] . '][total]" value="' . $total . '" autocomplete="off" ' . $disable . ' readonly />
                        </td>
                    </tr>';

                    $no++;
                }

                if( $adds != '' )
                {
                    $content .= '
                    <tr class="back-soft-blue">
                        <td class="text-left" colspan="4">Add-Ons</td>
                    </tr>
                    <tr class="back-soft-gray">
                        <td class="text-left">Add-ons Name</td>
                        <td class="text-right" width="100">Qty</td>
                        <td class="text-right" width="125">Price</td>
                        <td class="text-right" width="125">Sub Total</td>
                    </tr>
                    ' . $adds . '
                    <tr class="back-soft-gray">
                        <td class="text-right middle" colspan="3">Total</td>
                        <td class="text-right">
                            <input class="popup-field-text text-number text-right field-total field-addons-total" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' "" type="text" name="fields[laddons_total]" value="0" autocomplete="off" readonly />
                        </td>
                    </tr>';
                }
            }

            $content .= '
            <tr class="back-soft-blue">
                <td class="text-right middle" colspan="3">Grand Total</td>
                <td class="text-right">
                    <input type="hidden" name="fields[lagent]" value="' . $resv[ 'lagent' ] . '" />
                    <input type="hidden" name="fields[lcurrency_id]" value="' . $resv[ 'lcurrency_id' ] . '" />

                    <input class="popup-field-text text-number text-right field-subtotal" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' " type="hidden" name="fields[lsubtotal]" value="' . $resv[ 'lsubtotal' ] . '" autocomplete="off" readonly />
                    <input class="popup-field-text text-number text-right field-grand-total" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' " type="text" name="fields[ltotal]" value="' . $resv[ 'ltotal' ] . '" autocomplete="off" readonly />
                </td>
            </tr>
            <tr class="back-soft-blue">
                <td class="text-right middle" colspan="3">Deposit</td>
                <td class="text-right">
                    <input class="popup-field-text text-number text-right field-deposit" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' " type="text" name="fields[ldeposit]" value="' . $resv[ 'ldeposit' ] . '" autocomplete="off" />
                </td>
            </tr>
            <tr class="back-soft-blue">
                <td class="text-right middle" colspan="3">Balance</td>
                <td class="text-right">
                    <input class="popup-field-text text-number text-right field-balance" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' " type="text" name="fields[lbalance]" value="' . $resv[ 'lbalance' ] . '" autocomplete="off" readonly />
                </td>
            </tr>';
        }

        return $content;
    }

    function get_new_accommodation_detail( $lpost_id, $data )
    {        
        //-- GET property currency
        $currency = $this->global->get_currency_sign( $lpost_id );
        $content  = '';

        //-- CHECK accommodation type of price
        //-- 0 = Per night
        //-- 1 = Per package
        if( $data[ 'price_type' ] == '1' )
        {
            //-- CHECK used template
            if( $data[ 'page_template' ] == '1' )
            {
                //-- GET schedules
                $schedules = $this->global->getSchedule( array( 'lpost_id' => $lpost_id, 'lstatus'  => 1 ), false );

                if( empty( $schedules ) === false )
                {
                    $options = '';

                    foreach( $schedules as $d )
                    {
                        $datelist = sprintf( 'Trip %s ( %s - %s )', $d[ 'litinerary' ], date( 'd/m/Y', strtotime( $d[ 'lcheck_in' ] ) ), date( 'd/m/Y', strtotime( $d[ 'lcheck_out' ] ) ) );

                        $options .= '<option value="' . $d[ 'lschedule_id' ] . '" data-price="' . $d[ 'lprice' ] . '">' . $datelist . '</option>';
                    }

                    $content .= '
                    <tr class="back-soft-gray">
                        <td class="text-left" colspan="3">Schedules</td>
                        <td class="text-right" width="125">Price</td>
                    </tr>
                    <tr class="schedules-items">
                        <td class="text-left" colspan="3">
                            <select class="popup-field-select field-schedule-id" name="fields[lschedule_id]">
                                <option value="" data-price="0">Select Schedule</option>
                                ' . $options . '
                            </select>
                        </td>
                        <td class="text-right" width="125">
                            <input class="popup-field-text text-number text-right field-schedule-price" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . '" type="text" name="fields[lprice]" value="0" autocomplete="off" readonly />
                        </td>
                    </tr>';
                }
            }
            else
            {
                //-- GET packages
                $packages = $this->global->getPackages( array( 'lpost_id' => $lpost_id, 'lstatus'  => 1 ), false );

                if( empty( $packages ) === false )
                {
                    $options = '';

                    foreach( $packages as $d )
                    {
                        $options .= '<option value="' . $d[ 'lpackage_id' ] . '" data-price="' . $d[ 'lprice' ] . '">' . $d[ 'lname' ] . '</option>';
                    }

                    $content .= '
                    <tr class="back-soft-gray">
                        <td class="text-left" colspan="3">Package Name</td>
                        <td class="text-right" width="125">Price</td>
                    </tr>
                    <tr class="packages-items">
                        <td class="text-left"  colspan="3">
                            <select class="popup-field-select field-package-id" name="fields[lpackage_id]">
                                ' . $options . '
                            </select>
                        </td>
                        <td class="text-right" width="125">
                            <input class="popup-field-text text-number text-right field-package-price" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . '" type="text" name="fields[lprice]" value="' . $resv[ 'lprice' ] . '" autocomplete="off" readonly />
                        </td>
                    </tr>';
                }
            }
        }
        else
        {
            //-- SET Rooms Content
            if( isset( $data[ 'room_type' ] ) && !empty( $data[ 'room_type' ] ) )
            {
                $roomcont = '';

                $no = 1;

                foreach( $data[ 'room_type' ] as $d )
                {
                    //-- CONTINUE if status room is temporary
                    if( $d[ 'lstatus' ] == 2 )
                    {
                        continue;
                    }

                    //-- CHECK availability if policy = 0
                    if( isset( $data[ 'policy' ] ) && $data[ 'policy' ] == '0' )
                    {
                        if( $this->check_availability( $d[ 'lterm_id' ] ) === false )
                        {
                            continue;
                        }
                    }

                    $night    = empty( $this->post[ 'lnights' ] ) ? 0 : $this->post[ 'lnights' ];
                    $opt      = $this->get_room_type_option( $d[ 'lterm_id' ], $data );
                    $price    = $this->global->roomPrice( $data, $d );
                    $subtotal = $night * $price;

                    $roomcont .= '
                    <tr class="room-items">
                        <td class="text-left field-room-id-' . $no . '" style="vertical-align:middle;">
                            <label for="room-' . $d[ 'lterm_id' ] . '">
                                <input id="room-' . $d[ 'lterm_id' ] . '" type="checkbox" name="fields[lrooms][' . $d[ 'lterm_id' ] . '][id]" value="' . $d[ 'lterm_id' ] . '" autocomplete="off" />
                                <span>' . $d[ 'lname' ] . '</span>
                            </label>
                        </td>
                        <td class="text-right" width="100">
                            <input class="popup-field-text field-room-name-' . $no . '" type="hidden" name="fields[lrooms][' . $d[ 'lterm_id' ] . '][name]" value="' . $d[ 'lname' ] . '" disabled />
                            <input class="popup-field-text text-number text-right field-room-qty-' . $no . '" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="" type="text" name="fields[lrooms][' . $d[ 'lterm_id' ] . '][num]" value="1" autocomplete="off" disabled />
                        </td>
                        <td class="text-right" width="125">
                            <input class="popup-field-text text-number text-right field-room-price-' . $no . '" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' " type="text" name="fields[lrooms][' . $d[ 'lterm_id' ] . '][price]" value="' . $price . '" autocomplete="off" disabled readonly />
                        </td>
                        <td class="text-right" width="125">
                            <input class="popup-field-text text-number text-right field-room-subtotal-' . $no . '" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' " type="text" name="fields[lrooms][' . $d[ 'lterm_id' ] . '][total]" value="' . $subtotal . '" autocomplete="off" disabled readonly />
                        </td>
                    </tr>';

                    $no++; 
                } 

                if( $no > 1 )
                {
                    $content .= '
                    <tr class="back-soft-gray">
                        <td class="text-left">Room Name</td>
                        <td class="text-right" width="100">Qty</td>
                        <td class="text-right" width="125">Price</td>
                        <td class="text-right" width="125">Sub Total</td>
                    </tr>
                    ' . $roomcont . '
                    <tr class="back-soft-gray">
                        <td class="text-right middle" colspan="3">Total</td>
                        <td class="text-right">
                            <input class="popup-field-text text-number text-right field-total field-room-total" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' " type="text" name="fields[lroom_total]"  value="0" autocomplete="off" readonly />
                        </td>
                    </tr>';
                }
            }
        }

        if( empty( $content ) === false )
        {
            if( isset( $data[ 'accommodation_add_ons' ] ) && !empty( $data[ 'accommodation_add_ons' ] ) )
            {
                $adds = '';
                $no   = 1;

                foreach( $data[ 'accommodation_add_ons' ] as $d )
                {
                    //-- CONTINUE if status addons is temporary
                    if( $d[ 'lstatus' ] == 2 )
                    {
                        continue;
                    }

                    $adds .= '
                    <tr class="addons-items">
                        <td class="text-left field-addons-id-' . $no . '" style="vertical-align:middle;">
                            <label for="addons-' . $d[ 'lterm_id' ] . '">
                                <input id="addons-' . $d[ 'lterm_id' ] . '" type="checkbox" name="fields[laddons][' . $d[ 'lterm_id' ] . '][id]" value="' . $d[ 'lterm_id' ] . '" autocomplete="off" />
                                <span>' . $d[ 'lname' ] . '</span>
                            </label>
                        </td>
                        <td class="text-right" width="100">
                            <input class="popup-field-text field-addons-name-' . $no . '" type="hidden" name="fields[laddons][' . $d[ 'lterm_id' ] . '][name]" value="' . $d[ 'lname' ] . '" />
                            <input class="popup-field-text text-number text-right field-addons-qty-' . $no . '" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="" type="text" name="fields[laddons][' . $d[ 'lterm_id' ] . '][num]" value="1" autocomplete="off" />
                        </td>
                        <td class="text-right" width="125">
                            <input class="popup-field-text text-number text-right field-addons-price-' . $no . '" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' " type="text" name="fields[laddons][' . $d[ 'lterm_id' ] . '][price]" value="' . $d[ 'addons_price' ] . '" autocomplete="off" readonly />
                        </td>
                        <td class="text-right" width="125">
                            <input class="popup-field-text text-number text-right field-addons-subtotal-' . $no . '" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' " type="text" name="fields[laddons][' . $d[ 'lterm_id' ] . '][total]" value="' . $d[ 'addons_price' ] . '" autocomplete="off" readonly />
                        </td>
                    </tr>';

                    $no++;
                }

                if( $adds != '' )
                {
                    $content .= '
                    <tr class="back-soft-blue">
                        <td class="text-left" colspan="4">Add-Ons</td>
                    </tr>
                    <tr class="back-soft-gray">
                        <td class="text-left">Add-ons Name</td>
                        <td class="text-right" width="100">Qty</td>
                        <td class="text-right" width="125">Price</td>
                        <td class="text-right" width="125">Sub Total</td>
                    </tr>
                    ' . $adds . '
                    <tr class="back-soft-gray">
                        <td class="text-right middle" colspan="3">Total</td>
                        <td class="text-right">
                            <input class="popup-field-text text-number text-right field-total field-addons-total" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' "" type="text" name="fields[laddons_total]" value="0" autocomplete="off" readonly />
                        </td>
                    </tr>';
                }
            }

            $content .= '
            <tr class="back-soft-blue">
                <td class="text-right middle" colspan="3">Grand Total</td>
                <td class="text-right">
                    <input type="hidden" name="fields[lagent]" value="' . $data[ 'lcreated_by' ] . '" />
                    <input type="hidden" name="fields[lcurrency_id]" value="' . $data[ 'currency' ] . '" />

                    <input class="popup-field-text text-number text-right field-subtotal" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' " type="hidden" name="fields[lsubtotal]" value="' . $resv[ 'lsubtotal' ] . '" autocomplete="off" readonly />
                    <input class="popup-field-text text-number text-right field-grand-total" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' " type="text" name="fields[ltotal]" value="' . $resv[ 'ltotal' ] . '" autocomplete="off" readonly />
                </td>
            </tr>
            <tr class="back-soft-blue">
                <td class="text-right middle" colspan="3">Deposit</td>
                <td class="text-right">
                    <input class="popup-field-text text-number text-right field-deposit" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' " type="text" name="fields[ldeposit]" value="' . $resv[ 'ldeposit' ] . '" autocomplete="off" />
                </td>
            </tr>
            <tr class="back-soft-blue">
                <td class="text-right middle" colspan="3">Balance</td>
                <td class="text-right">
                    <input class="popup-field-text text-number text-right field-balance" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="' . $currency . ' " type="text" name="fields[lbalance]" value="' . $resv[ 'lbalance' ] . '" autocomplete="off" readonly />
                </td>
            </tr>';
        }

        return $content;
    }

    function get_guest_list( $list = array() )
    {
        $content = '';

        if( empty( $list ) )
        {
            $index = uniqid();

            $content .= '
            <table class="table-pass-detail table-adult-pass">
                <thead>
                    <tr class="back-soft-gray">
                        <th class="text-left" colspan="4">Adult</th>
                    </tr>
                    <tr class="back-soft-blue">
                        <th class="text-left" width="120">Title</th>
                        <th class="text-left">First Name</th>
                        <th class="text-left">Last Name</th>
                        <th class="text-center" width="100">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="guest-item">
                        <td class="text-left">
                            <select class="guest-select" name="fields[lguests][adult][' . $index . '][title]" autocomplete="off">
                                ' . $this->get_title_option() . '
                            </select>
                        </td>
                        <td class="text-left relative-content">
                            <input class="popup-field-text" type="text" name="fields[lguests][adult][' . $index . '][firstname]" value="" autocomplete="off" required />
                        </td>
                        <td class="text-left relative-content">
                            <input class="popup-field-text" type="text" name="fields[lguests][adult][' . $index . '][lastname]" value="" autocomplete="off" required />
                        </td>
                        <td class="text-center">
                            <a class="del-pass" data-type="adult">Delete</a>
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr class="back-soft-gray">
                        <td class="text-right" colspan="3">
                            <div class="bblock">
                                <span>Total adult</span>
                                <input class="popup-field-text text-number text-right field-total-adult" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="" type="text" name="fields[ladult]" value="1" autocomplete="off" readonly />
                            </div>                                     
                        </td>
                        <td class="text-right">
                            <a class="add-new connect" data-type="adult" href="javascript:;">Add New</a>
                        </td>
                    </tr>
                </tfoot>
            </table>                  
            <table class="table-pass-detail table-child-pass">
                <thead>
                    <tr class="back-soft-gray">
                        <th class="text-left" colspan="3">Child</th>
                    </tr>
                    <tr class="back-soft-blue">
                        <th class="text-left">First Name</th>
                        <th class="text-left">Last Name</th>
                        <th class="text-center" width="80">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="guest-empty">
                        <td class="text-center" colspan="3">No child guest</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr class="back-soft-gray">
                        <td class="text-right" colspan="2">
                            <div class="bblock">
                                <span>Total child</span>
                                <input class="popup-field-text text-number text-right field-total-child" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="" type="text" name="fields[lchild]" value="0" autocomplete="off" readonly />
                            </div>                                     
                        </td>
                        <td class="text-right">
                            <a class="add-new connect" data-type="child" href="javascript:;">Add New</a>
                        </td>
                    </tr>
                </tfoot>
            </table>                 
            <table class="table-pass-detail table-infant-pass">
                <thead>
                    <tr class="back-soft-gray">
                        <th class="text-left" colspan="3">Infant</th>
                    </tr>
                    <tr class="back-soft-blue">
                        <th class="text-left">First Name</th>
                        <th class="text-left">Last Name</th>
                        <th class="text-center" width="80">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="guest-empty">
                        <td class="text-center" colspan="3">No infant guest</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr class="back-soft-gray">
                        <td class="text-right" colspan="2">
                            <div class="bblock">
                                <span>Total infant</span>
                                <input class="popup-field-text text-number text-right field-total-infant" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="" type="text" name="fields[linfant]" value="0" autocomplete="off" readonly />
                            </div>                                     
                        </td>
                        <td class="text-right">
                            <a class="add-new connect" data-type="infant" href="javascript:;">Add New</a>
                        </td>
                    </tr>
                </tfoot>
            </table>';
        }
        else
        {
            foreach( $list as $type => $obj )
            {
                $content .= '
                <table class="table-pass-detail table-' . $type . '-pass">
                    <thead>
                        <tr class="back-soft-gray">
                            <th class="text-left" colspan="' . ( $type == 'adult' ? '4' : '3' ) . '">' . ucfirst( $type ) . '</th>
                        </tr>
                        <tr class="back-soft-blue">';

                            if( $type == 'adult' )
                            {
                                $content .= '<th class="text-left" width="120">Title</th>';
                            }

                            $content .= '
                            <th class="text-left">First Name</th>
                            <th class="text-left">Last Name</th>
                            <th class="text-center" width="100">Action</th>
                        </tr>
                    </thead>
                    <tbody>';

                        $num = 0;

                        if( $type != 'adult' )
                        {
                            $content .= '
                            <tr class="guest-empty" style="display:none;">
                                <td class="text-center" colspan="3">No ' . $type . ' guest</td>
                            </tr>';
                        }

                        foreach( $obj as $index => $d )
                        {
                            $content .= '
                            <tr class="guest-item">';

                                if( $type == 'adult' )
                                {
                                    $content .= '
                                    <td class="text-left">
                                        <select class="guest-select" name="fields[lguests][' . $type . '][' . $index . '][title]" autocomplete="off">
                                            ' . $this->get_title_option( $d[ 'title' ] ) . '
                                        </select>
                                    </td>';
                                }

                                $content .= '
                                <td class="text-left relative-content">
                                    <input class="popup-field-text" type="text" name="fields[lguests][' . $type . '][' . $index . '][firstname]" value="' . $d[ 'firstname' ] . '" autocomplete="off" required />
                                </td>
                                <td class="text-left relative-content">
                                    <input class="popup-field-text" type="text" name="fields[lguests][' . $type . '][' . $index . '][lastname]" value="' . $d[ 'lastname' ] . '" autocomplete="off" required />
                                </td>
                                <td class="text-center">
                                    <a class="del-pass" data-type="' . $type . '">Delete</a>
                                </td>
                            </tr>';

                            $num++;
                        }

                        $content .= '
                    </tbody>
                    <tfoot>
                        <tr class="back-soft-gray">
                            <td class="text-right" colspan="' . ( $type == 'adult' ? '3' : '2' ) . '">
                                <div class="bblock">
                                    <span>Total ' . $type . '</span>
                                    <input class="popup-field-text text-number text-right field-total-' . $type . '" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="" type="text" name="fields[l' . $type . ']" value="' . $num . '" autocomplete="off" readonly />
                                </div>                                     
                            </td>
                            <td class="text-right">
                                <a class="add-new connect" data-type="' . $type . '" href="javascript:;">Add New</a>
                            </td>
                        </tr>
                    </tfoot>
                </table>';
            }

            if( !isset( $list[ 'child' ] ) ) 
            {
                $content .= '
                <table class="table-pass-detail table-child-pass">
                    <thead>
                        <tr class="back-soft-gray">
                            <th class="text-left" colspan="3">Child</th>
                        </tr>
                        <tr class="back-soft-blue">
                            <th class="text-left">First Name</th>
                            <th class="text-left">Last Name</th>
                            <th class="text-center" width="80">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="guest-empty">
                            <td class="text-center" colspan="3">No child guest</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr class="back-soft-gray">
                            <td class="text-right" colspan="2">
                                <div class="bblock">
                                    <span>Total child</span>
                                    <input class="popup-field-text text-number text-right field-total-child" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="" type="text" name="fields[lchild]" value="0" autocomplete="off" readonly />
                                </div>                                     
                            </td>
                            <td class="text-right">
                                <a class="add-new connect" data-type="child" href="javascript:;">Add New</a>
                            </td>
                        </tr>
                    </tfoot>
                </table>';
            }          

            if( !isset( $list[ 'infant' ] ) ) 
            {
                $content .= '       
                <table class="table-pass-detail table-infant-pass">
                    <thead>
                        <tr class="back-soft-gray">
                            <th class="text-left" colspan="3">Infant</th>
                        </tr>
                        <tr class="back-soft-blue">
                            <th class="text-left">First Name</th>
                            <th class="text-left">Last Name</th>
                            <th class="text-center" width="80">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="guest-empty">
                            <td class="text-center" colspan="3">No infant guest</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr class="back-soft-gray">
                            <td class="text-right" colspan="2">
                                <div class="bblock">
                                    <span>Total infant</span>
                                    <input class="popup-field-text text-number text-right field-total-infant" data-a-sep="." data-a-dec="," data-m-dec="0" data-a-sign="" type="text" name="fields[linfant]" value="0" autocomplete="off" readonly />
                                </div>                                     
                            </td>
                            <td class="text-right">
                                <a class="add-new connect" data-type="infant" href="javascript:;">Add New</a>
                            </td>
                        </tr>
                    </tfoot>
                </table>';
            }
        }

        return $content;
    }

    function add_new_guest()
    {   
        $type  = $this->post[ 'type' ];
        $index = uniqid();

        $content = '
        <tr class="guest-item">';

            if( $type == 'adult' )
            {
                $content .= '
                <td class="text-left">
                    <select class="guest-select" name="fields[lguests][' . $type . '][' . $index . '][title]" autocomplete="off">
                        ' . $this->get_title_option() . '
                    </select>
                </td>';
            }

            $content .= '
            <td class="text-left relative-content">
                <input class="popup-field-text" type="text" name="fields[lguests][' . $type . '][' . $index . '][firstname]" value="" autocomplete="off" required />
            </td>
            <td class="text-left relative-content">
                <input class="popup-field-text" type="text" name="fields[lguests][' . $type . '][' . $index . '][lastname]" value="" autocomplete="off" required />
            </td>
            <td class="text-center">
                <a class="del-pass" data-type="' . $type . '">Delete</a>
            </td>
        </tr>';

        return $content;
    }

    function language()
    {
        $this->lang = $this->global->getSettingValue( 'llanguage' );
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif ); 
        $this->sess    = $this->global->getCurrentSession();
        $this->valid   = true;

        if( isset( $_GET[ 'id' ] ) )
        {
            $s = 'SELECT * FROM lumonata_booking AS a WHERE a.lbooking_id = %s';
            $q = parent::prepare_query( $s, $_GET[ 'id' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $fields = parent::fetch_assoc( $r );

                if( !empty( $fields[ 'lguests' ] ) )
                {
                    $fields[ 'lguests' ] = json_decode( $fields[ 'lguests' ], true );
                }

                if( !empty( $fields[ 'lrooms' ] ) )
                {
                    $fields[ 'lrooms' ] = json_decode( $fields[ 'lrooms' ], true );
                }

                if( !empty( $fields[ 'laddons' ] ) )
                {
                    $fields[ 'laddons' ] = json_decode( $fields[ 'laddons' ], true );
                }
            }
            else
            {
                $this->valid = false;
            }
        }
        else
        {
            $fields = array(
                'lcreated_by'   => $this->sess[ 'username' ],
                'lusername'     => $this->sess[ 'username' ],
                'lcreated_date' => time(),
                'lcheck_in'     => time(),
                'lcheck_out'    => time(),
                'ldlu'          => time(),
                'lguests'       => array(),
                'lrooms'        => array(),
                'laddons'       => array(),
                'lpackage_id'   => '',
                'lschedule_id'  => '',
                'lcurrency_id'  => '',
                'lpost_id'      => '',
                'ltitle'        => '',
                'lfname'        => '',
                'llname'        => '',
                'lphone'        => '',
                'lemail'        => '',
                'lnote'         => '',
                'lnights'       => 1,
                'ladult'        => 1,
                'lchild'        => 0,
                'linfant'       => 0,
                'lprice'        => 0
            );
        }

        if( empty( $_POST ) )
        {
            if( empty( $this->notif ) === false && isset( $this->notif[ 'post' ] ) )
            {
                $this->fields = array_merge( $fields, $this->notif[ 'post' ] );
            }
            else
            {
                if( isset( $fields ) )
                {
                    $this->fields = $fields;   
                }
            }

            $this->post = array();
        }
        else
        {
            if( isset( $_POST[ 'fields' ] ) )
            {
                $this->fields = array_merge( $fields, $_POST[ 'fields' ] );
            }
            else
            {
                $this->fields = $fields;
            }

            $this->post = array_diff_key( $_POST, array_flip( array( 'fields' ) ) );
        }
    }

    function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'load' )
            {
                echo $this->load( $mod, $usertype );
            }

            if( $this->post[ 'pKEY' ] == 'popup' )
            {
                echo $this->popup();
            }

            if( $this->post[ 'pKEY' ] == 'get-property-detail' )
            {
                echo $this->get_accommodation_detail();
            }
            else if( $this->post[ 'pKEY' ] == 'add-new-guest' )
            {
                echo $this->add_new_guest();
            }
        }

        exit;
    }
}