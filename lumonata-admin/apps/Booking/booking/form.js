function init_booking()
{
    var form = jQuery('[name=new_booking]');
    var csel = form.find('.popup-field-select');
    var psel = form.find('.text-number');
    
    init_number( psel );
    init_datepicker();
    calculate( form );
    csel.chosen();

    form.find('.text-number').on('blur', function(){
        calculate( form );
    });

    form.find('[name="fields[lcheck_in]"]').datepicker( 'option', 'onSelect', function( date ){
        var start = form.find('[name="fields[lcheck_in]"]').datepicker('getDate');
        var end   = form.find('[name="fields[lcheck_out]"]').datepicker('getDate');

        if( end == null )
        {
            form.find('[name="fields[lcheck_out]"]').datepicker('setDate', start);

            end = start;
        }

        var night = ( end - start ) / ( 1000 * 60 * 60 * 24 );

        if( night <= 0 )
        {
            night = 1;
        }
        
        form.find('[name="fields[lcheck_out]"]').datepicker( 'option', 'minDate', date );

        AutoNumeric.set( '.field-night', Math.round( night ) );

        calculate( form );
    });

    form.find('[name="fields[lcheck_out]"]').datepicker( 'option', 'onSelect', function( date ){
        var start = form.find('[name="fields[lcheck_in]"]').datepicker('getDate');
        var end   = form.find('[name="fields[lcheck_out]"]').datepicker('getDate');

        if( start == null )
        {
            form.find('[name="fields[lcheck_in]"]').datepicker('setDate', end);

            start = end;
        }

        var night = ( end - start ) / ( 1000 * 60 * 60 * 24 );

        if( night <= 0 )
        {
            night = 1;
        }

        AutoNumeric.set( '.field-night', Math.round( night ) );

        calculate( form );
    });

    form.find('[name="fields[lpackage_id]"]').on('change', function(){
        var price = jQuery(this).find('option:selected').data('price');

        AutoNumeric.set( '.field-package-price', price );

        calculate( form );
    });

    form.find('[name="fields[lschedule_id]"]').on('change', function(){
        var price = jQuery(this).find('option:selected').data('price');

        AutoNumeric.set( '.field-schedule-price', price );

        calculate( form );
    });

    form.find('[name="fields[lpost_id]"]').on('change', function(){
        var checkout = form.find('[name="fields[lcheck_out]"]').datepicker('getDate');
        var checkin  = form.find('[name="fields[lcheck_in]"]').datepicker('getDate');
        var bookid   = form.find('[name="fields[lbooking_id]"]').val();
        var nights   = form.find('[name="fields[lnights]"]').val();

    	var url = form.data('ajax-url');
    	var prm = new Object;
    		prm.pKEY        = 'get-property-detail';
    		prm.lpost_id    = jQuery(this).val();
            prm.lcheckout   = checkout;
            prm.lcheckin    = checkin;
            prm.lnights     = nights;
            prm.lbooking_id = bookid;

        if( prm.lpost_id == '' )
        {
            jQuery('.table-booking-detail tbody').html('');
        }
        else
        {
            jQuery.ajax({
                url: url,
                data: prm,
                method: 'POST',
                dataType : 'json',
                beforeSend: function( xhr ){
                    form.find('.content-body-1042').addClass('processing');
                },
                success: function( e ){
                    if( e.result == 'success' )
                    {
                        jQuery('.table-booking-detail tbody').html( e.content );

                        init_accommodation_table_act( form );
                    }
                    else
                    {
                        form.find('[name="fields[lpost_id]"]').val('').trigger('chosen:updated');

                        jQuery('.table-booking-detail tbody').html('');

                        show_popup( e.message );
                    }
                },
                error: function( e ){
                    show_popup( 'Failed to get property detail' );
                },
                complete: function( e ){
                    form.find('.content-body-1042').removeClass('processing');

                    calculate( form );
                }
            });
        }
    });

    form.find('.guest-select').chosen();

    form.find('.del-pass').on('click', function(){
        var sel = jQuery(this).parents('.guest-item');
        var typ = jQuery(this).data('type');
        
        if( form.find('.table-' + typ +'-pass tbody .guest-item').length == 1 )
        {
            if( typ == 'adult' )
            {
                show_popup( 'Can\'t remove this item! there must be at least one item in the list' );
            }
            else
            {
               form.find('.table-' + typ +'-pass tbody .guest-empty').show();
            }
        }

        sel.remove();

        AutoNumeric.set( '.field-total-' + typ, form.find('.table-' + typ +'-pass tbody .guest-item').length );
        
        calculate( form );
    });

    form.find('.table-booking-detail tbody .room-items [type=checkbox]').on('change', function(){
        if( this.checked )
        {
            jQuery(this).parents('.room-items').find('.popup-field-text').prop('disabled', false);
        }
        else
        {
            jQuery(this).parents('.room-items').find('.popup-field-text').prop('disabled', true);
        }

        calculate( form );
    });
    
    form.find('.table-booking-detail tbody .addons-items [type=checkbox]').on('change', function(){
        if( this.checked )
        {
            jQuery(this).parents('.addons-items').find('.popup-field-text').prop('disabled', false);
        }
        else
        {
            jQuery(this).parents('.addons-items').find('.popup-field-text').prop('disabled', true);
        }

        calculate( form );
    });

    form.validate({
        ignore: [],
        submitHandler: function( frm ){
            frm.submit();
        }
    });

    form.find('.add-new').on('click', function(){
        var url = form.data('ajax-url');
        var prm = new Object;
            prm.pKEY = 'add-new-guest';
            prm.type = jQuery(this).data('type');

        jQuery.ajax({
            url: url,
            data: prm,
            method: 'POST',
            dataType : 'html',
            beforeSend: function( xhr ){
                form.find('.content-body-1042').addClass('processing');
            },
            success: function( e ){
                jQuery('.table-' + prm.type +'-pass tbody .guest-empty').hide();

                jQuery( e ).appendTo('.table-' + prm.type +'-pass tbody');

                init_guest_table_act( form, prm.type );
            },
            error: function( e ){
                show_popup( 'Failed to add new ' + prm.type + ' guest' );
            },
            complete: function( e ){
                form.find('.content-body-1042').removeClass('processing');

                calculate( form );
            }
        });
    });
}

function init_guest_table_act( form, type )
{        
    AutoNumeric.set( '.field-total-' + type, form.find('.table-' + type +'-pass tbody .guest-item').length );

    form.find('.table-' + type +'-pass tbody .guest-item:last-child select').chosen();

    form.find('.table-' + type +'-pass tbody .guest-item:last-child .del-pass').on('click', function(){
        var sel = jQuery(this).parents('.guest-item');
        var typ = jQuery(this).data('type');

        if( form.find('.table-' + typ +'-pass tbody .guest-item').length == 1 )
        {
            if( typ == 'adult' )
            {
                show_popup( 'Can\'t remove this item! there must be at least one item in the list' );
            }
            else
            {
               form.find('.table-' + typ +'-pass tbody .guest-empty').show();
            }
        }

        sel.remove();

        AutoNumeric.set( '.field-total-' + typ, form.find('.table-' + typ +'-pass tbody .guest-item').length );
        
        calculate( form );
    });
}

function init_accommodation_table_act( form )
{
    if( form.find('.table-booking-detail tbody .text-number').length > 0 )
    {
        init_number( form.find('.table-booking-detail tbody .text-number') );
    }

    if( form.find('.table-booking-detail tbody .popup-field-select').length > 0 )
    {
        form.find('.table-booking-detail tbody .popup-field-select').chosen();
    }

    form.find('.table-booking-detail tbody .text-number').on('blur', function(){
        calculate( form );
    });

    form.find('.table-booking-detail tbody .room-items [type=checkbox]').unbind('change');
    form.find('.table-booking-detail tbody .room-items [type=checkbox]').on('change', function(){
        if( this.checked )
        {
            jQuery(this).parents('.room-items').find('.popup-field-text').prop('disabled', false);
        }
        else
        {
            jQuery(this).parents('.room-items').find('.popup-field-text').prop('disabled', true);
        }

        calculate( form );
    });

    form.find('.table-booking-detail tbody .addons-items [type=checkbox]').unbind('change');
    form.find('.table-booking-detail tbody .addons-items [type=checkbox]').on('change', function(){
        if( this.checked )
        {
            jQuery(this).parents('.addons-items').find('.popup-field-text').prop('disabled', false);
        }
        else
        {
            jQuery(this).parents('.addons-items').find('.popup-field-text').prop('disabled', true);
        }

        calculate( form );
    });

    form.find('.table-booking-detail tbody [name="fields[lpackage_id]"]').unbind('change');
    form.find('.table-booking-detail tbody [name="fields[lpackage_id]"]').on('change', function(){
        var price = jQuery(this).find('option:selected').data('price');

        AutoNumeric.set( '.field-package-price', price );

        calculate( form );
    });

    form.find('.table-booking-detail tbody [name="fields[lschedule_id]"]').unbind('change');
    form.find('.table-booking-detail tbody [name="fields[lschedule_id]"]').on('change', function(){
        var price = jQuery(this).find('option:selected').data('price');

        AutoNumeric.set( '.field-schedule-price', price );

        calculate( form );
    });
}

function calculate( form )
{
    var deposit = 0;
    var ttotal  = 0;
    var rtotal  = 0;
    var atotal  = 0;
    var etotal  = 0;
    var ptotal  = 0;
    var stotal  = 0;
    var night   = 0;
    var adult   = 0;
    var child   = 0;
    var infant  = 0;

    if( form.find('.field-night').length > 0 )
    {
        night = AutoNumeric.getNumber( '.field-night' ) || 0;
    }

    if( form.find('.field-total-adult').length > 0 )
    {
        adult = AutoNumeric.getNumber( '.field-total-adult' ) || 0;
    }

    if( form.find('.field-total-child').length > 0 )
    {
        child = AutoNumeric.getNumber( '.field-total-child' ) || 0;
    }

    if( form.find('.field-total-infant').length > 0 )
    {
        infant = AutoNumeric.getNumber( '.field-total-infant' ) || 0;
    }

    if( form.find('.field-deposit').length > 0 )
    {
        deposit = AutoNumeric.getNumber( '.field-deposit' ) || 0;
    }

    var guest = adult + child + infant;

    if( form.find('.field-entire-price').length > 0 )
    {
        var price    = AutoNumeric.getNumber( '.field-entire-price' ) || 0;
        var subtotal = price * guest * night;

        etotal = subtotal;
    }

    if( form.find('.field-package-price').length > 0 )
    {
        var price    = AutoNumeric.getNumber( '.field-package-price' ) || 0;
        var subtotal = price;

        ptotal = subtotal;
    }

    if( form.find('.field-schedule-price').length > 0 )
    {
        var price    = AutoNumeric.getNumber( '.field-schedule-price' ) || 0;
        var subtotal = price;

        stotal = subtotal;
    }

    if( form.find('.trip-items').length > 0 )
    {
        form.find('.trip-items').each( function( i, e ){
            var idx      = i + 1;
            var price    = AutoNumeric.getNumber( '.field-trip-price-' + idx ) || 0;
            var subtotal = price * guest * night;

            AutoNumeric.set( '.field-trip-total-' + idx, subtotal );

            ttotal += subtotal;
        });
    }

    if( form.find('.room-items').length > 0 )
    {
        form.find('.room-items').each( function( i, e ){
            var idx      = i + 1;
            var price    = AutoNumeric.getNumber( '.field-room-price-' + idx ) || 0;
            var qty      = AutoNumeric.getNumber( '.field-room-qty-' + idx ) || 0;
            var subtotal = price * guest * night * qty;

            AutoNumeric.set( '.field-room-subtotal-' + idx, subtotal );

            if( jQuery('.field-room-price-' + idx).prop('disabled') === false )
            {
                rtotal += subtotal;
            }
        });

        AutoNumeric.set( '.field-room-total', rtotal );
    }

    if( form.find('.addons-items').length > 0 )
    {
        form.find('.addons-items').each( function( i, e ){
            var idx      = i + 1;
            var price    = AutoNumeric.getNumber( '.field-addons-price-' + idx ) || 0;
            var qty      = AutoNumeric.getNumber( '.field-addons-qty-' + idx ) || 0;
            var subtotal = price * qty;

            AutoNumeric.set( '.field-addons-subtotal-' + idx, subtotal );

            if( jQuery('.field-addons-price-' + idx).prop('disabled') === false )
            {
                atotal += subtotal;
            }
        });

        AutoNumeric.set( '.field-addons-total', atotal );
    }

    var gtotal  = ttotal + rtotal + atotal + etotal + ptotal + stotal;
    var balance = gtotal - deposit;

    if( form.find('.field-grand-total').length > 0 )
    {
        AutoNumeric.set( '.field-grand-total', gtotal );
    }

    if( form.find('.field-subtotal').length > 0 )
    {
        AutoNumeric.set( '.field-subtotal', gtotal );
    }

    if( form.find('.field-deposit').length > 0 )
    {
        AutoNumeric.set( '.field-deposit', deposit );
    }

    if( form.find('.field-balance').length > 0 )
    {
        AutoNumeric.set( '.field-balance', balance );
    }
}

jQuery(document).ready(function(){
	init_booking();
});