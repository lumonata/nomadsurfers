function init_table()
{
	var limit = parseInt( jQuery('#limit').val() ) || 1;
    var table = jQuery('#table').DataTable({
        lengthChange: false,
        deferRender: true,
        pageLength: limit,
        processing: true,
        serverSide: true,
        info: false,
        order: [],
        ajax: {
            url: jQuery('#ajax_url').val(),
            type: 'POST',
            data: function( d ){
                d.lpost_id = jQuery('[name=lpost_id]').val();
                d.lstatus  = jQuery('[name=lstatus]').val();
                d.lsearch  = jQuery('[name=lsearch]').val();
                d.pKEY     = 'load';
            }
        },
        columns: [
            {
                data: 'lbooking_code', render: function ( data, type, row ){
                    var html = '<a href="javascript:;" class="actionv" data-id="' + row.lbooking_id + '"><b>' + data + '</b></a>';

                    return html;
                }
            },
            {
                data: 'lcreated_date', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'lfullname', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'laccommodation', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'lstatus', render: function ( data, type, row ) {
                    if( data == 0 )
                    {
                        data = 'Check Availability';
                    }
                    else if( data == 1 )
                    {
                        data = 'Confirmed';
                    }
                    else if( data == 2 )
                    {
                        data = 'Refused';
                    }
                    else if( data == 3 )
                    {
                        data = 'Request Alternative Dates';
                    }

                    return data;
                }
            },
            {
                data: 'lbooking_id', render: function ( data, type, row ) {
                    var html =                     
                    '<a class="actionp actione2" href="' + row.lpaymentlink + '" title="Paid" target="_blank"></a>'+
                    '<a class="actione actione2" href="' + row.leditlink + '" title="Edit"></a>'+
                    '<a class="actiond actiond2 hidden" data-id="' + row.lbooking_id + '" title="Delete"></a>';

                    return html;
                },
                className: 'text-center'
            }
        ],
        dom: 'tp'
    });

    table.on( 'draw.dt', function () {
        jQuery(this).closest('.dataTables_wrapper').find('.dataTables_paginate').toggle( table.page.info().pages > 1 );
        init_action( table );
    });

    jQuery('[name=filter]').on('click', function(){
        table.ajax.reload();
    });
}

function init_action( table )
{
    jQuery('.actionv').unbind();
    jQuery('.actionv').on('click', function(){
        var src = jQuery('#ajax_url').val();
        var bid = jQuery(this).data('id');

        jQuery.fancybox.open({
            type: 'ajax',
            src: src,
            opts: {
                baseClass: 'popup-booking',
                closeExisting: true,
                ajax: { 
                    settings: { 
                        type: 'POST',
                        data: {
                            pKEY: 'popup',
                            bid: bid
                        }
                    }
                }
            }
        });
    });

    jQuery('.actiond').unbind();
    jQuery('.actiond').on('click', function(){
        var id = jQuery(this).attr('data-id');

        jQuery('<div></div>').dialog({
            dialogClass: 'ui-dialog-no-titlebar',
            draggable: false,
            resizable: false,
            modal: true,
            open: function(){
                jQuery(this).html( 'Do you really want to DELETE this data? Delete is permanent and cannot be restored.' );
            },
            buttons: [{
                text: 'Yes',
                click: function(){
                    var pop = jQuery( this ).dialog( 'close' );
                    var url = jQuery('#ajax_url').val();
                    var prm = new Object;
                        prm.pKEY = 'do_delete';
                        prm.id   = id;

                    jQuery.ajax({
                        url: url,
                        data: prm,
                        type: 'POST',
                        dataType : 'json',
                        success: function( e ){
                            pop.dialog('close');

                            if( e.result == 'success' )
                            {                        
                                table.ajax.reload( function(){
                                    show_popup( 'Successfully deleted data' );
                                    init_action( table );
                                }, false);
                            }
                            else
                            {
                                show_popup( 'Failed to delete data!' );
                            }
                        },
                        error: function( e ){
                            pop.dialog('close');
                                          
                            show_popup( 'Failed to delete data!' );
                        }
                    });
                }
            },{
                text: 'No',
                click: function(){
                    jQuery( this ).dialog( 'close' );
                }
            }]
        });
    });
}

function calculate( form )
{
    var deposit = 0;
    var ttotal  = 0;
    var rtotal  = 0;
    var atotal  = 0;
    var etotal  = 0;
    var night   = 0;
    var guest   = 0;

    if( form.find('.field-night').length > 0 )
    {
        night = AutoNumeric.getNumber( '.field-night' ) || 0;
    }

    if( form.find('.field-guest').length > 0 )
    {
        guest = AutoNumeric.getNumber( '.field-guest' ) || 0;
    }

    if( form.find('.field-deposit').length > 0 )
    {
        deposit = AutoNumeric.getNumber( '.field-deposit' ) || 0;
    }

    if( form.find('.field-entire-price').length > 0 )
    {
        var price    = AutoNumeric.getNumber( '.field-entire-price' ) || 0;
        var subtotal = price * guest * night;

        etotal = subtotal;
    }

    if( form.find('.trip-items').length > 0 )
    {
        form.find('.trip-items').each( function( i, e ){
            var idx      = i + 1;
            var price    = AutoNumeric.getNumber( '.field-trip-price-' + idx ) || 0;
            var subtotal = price * guest * night;

            AutoNumeric.set( '.field-trip-total-' + idx, subtotal );

            ttotal += subtotal;
        });
    }

    if( form.find('.room-items').length > 0 )
    {
        form.find('.room-items').each( function( i, e ){
            var idx      = i + 1;
            var price    = AutoNumeric.getNumber( '.field-room-price-' + idx ) || 0;
            var qty      = AutoNumeric.getNumber( '.field-room-qty-' + idx ) || 0;
            var subtotal = price * guest * night * qty;

            AutoNumeric.set( '.field-room-subtotal-' + idx, subtotal );

            rtotal += subtotal;
        });

        AutoNumeric.set( '.field-room-total', rtotal );
    }

    if( form.find('.addons-items').length > 0 )
    {
        form.find('.addons-items').each( function( i, e ){
            var idx      = i + 1;
            var price    = AutoNumeric.getNumber( '.field-addons-price-' + idx ) || 0;
            var qty      = AutoNumeric.getNumber( '.field-addons-qty-' + idx ) || 0;
            var subtotal = price * qty;

            AutoNumeric.set( '.field-addons-subtotal-' + idx, subtotal );

            atotal += subtotal;
        });

        AutoNumeric.set( '.field-addons-total', atotal );
    }

    var gtotal  = ttotal + rtotal + atotal;
    var balance = gtotal - deposit;

    if( form.find('.field-grand-total').length > 0 )
    {
        AutoNumeric.set( '.field-grand-total', gtotal );
    }

    if( form.find('.field-subtotal').length > 0 )
    {
        AutoNumeric.set( '.field-subtotal', gtotal );
    }

    if( form.find('.field-deposit').length > 0 )
    {
        AutoNumeric.set( '.field-deposit', deposit );
    }

    if( form.find('.field-balance').length > 0 )
    {
        AutoNumeric.set( '.field-balance', balance );
    }
}

jQuery(document).ready(function(){
    init_table();
});