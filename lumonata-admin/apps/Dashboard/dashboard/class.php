<?php

class dashboard extends db
{
    private $template;
    private $actions;
    private $global;
    private $flash;

    private $accommodation;
    private $message;
    private $notif;
    private $post;
    private $sess;
    private $lang;

    function __construct( $template_dir = '.', $actions )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->actions  = $actions;

        $this->language();
        $this->import();
        $this->param();
    }

    function view( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $this->global->getPViewBothP( $mod, $usertype ) )
        {
            $bmod = $this->global->getValueField( 'lumonata_module', 'lmodule_id', 'lapps', 'booking' );
            $amod = $this->global->getValueField( 'lumonata_module', 'lmodule_id', 'lapps', 'accommodation' );

            $this->template->set_var( 'message', $this->message );

            $this->template->set_var( 'site_url', SITE_URL );
            $this->template->set_var( 'apps_url', ADMIN_APPS_URL . '/Dashboard/dashboard' );

            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );
            $this->template->set_var( 'add_acco_url', $this->global->getAddUrl( $amod, $usertype ) );
            $this->template->set_var( 'view_acco_url', $this->global->getViewUrl( $amod, $usertype ) );
            $this->template->set_var( 'view_booking_url', $this->global->getViewUrl( $bmod, $usertype ) );
            $this->template->set_var( 'ajax_booking_url', $this->global->getAjaxUrl( $bmod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Dashboard/dashboard/view.js' ) );

            $this->template->Parse( 'vC', 'viewContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function load_latest_accommodation()
    {
        //-- Get Module
        $mod = $this->global->getValueField( 'lumonata_module', 'lmodule_id', 'lapps', 'accommodation' );

        //-- Query Limit
        $limit = parent::prepare_query( ' LIMIT ' . $this->post[ 'start' ] . ', ' . $this->post[ 'length' ] );
    
        //-- Query Order By
        $order = parent::prepare_query( ' ORDER BY a.lcreated_date DESC' );

        //-- Query Filter
        if( $this->sess[ 'usertype' ] == 2 )
        {
            $where = parent::prepare_query( ' WHERE a.ltype = %s AND ( a.lref_id IS NULL OR a.lref_id = 0 ) AND ( SELECT a2.lusertype_id FROM lumonata_user AS a2 WHERE a2.lusername = a.lusername ) = %d', 'accommodation', 2 );
        }
        else
        {
            $where = parent::prepare_query( ' WHERE a.ltype = %s AND ( a.lref_id IS NULL OR a.lref_id = 0 )', 'accommodation' );
        }

        //-- Main Query
        $qf = 'SELECT 
                a.ltitle,
                a.lpost_id,
                ( SELECT GROUP_CONCAT( a2.lterm_id ) FROM lumonata_post_relationship AS a2 LEFT JOIN lumonata_post_terms AS b2 ON a2.lterm_id = b2.lterm_id WHERE a2.lpost_id = a.lpost_id AND b2.lrule = "accommodation_type" ) AS laccommodation_type,
                ( SELECT GROUP_CONCAT( a2.lterm_id ) FROM lumonata_post_relationship AS a2 LEFT JOIN lumonata_post_terms AS b2 ON a2.lterm_id = b2.lterm_id WHERE a2.lpost_id = a.lpost_id AND b2.lrule = "surf_trip" ) AS lsurf_trip
               FROM lumonata_post AS a' . $where . $order . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        $data = array();

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
                $si = 'SELECT 
                        a.lattach 
                       FROM lumonata_attachment AS a 
                       WHERE a.lattach_id = (
                            SELECT a2.ladditional_value 
                            FROM lumonata_additional_field AS a2
                            WHERE a2.lapp_id = %d 
                            AND a2.ladditional_key = %s
                            AND a.lmodule_id = %d
                       )';
                $qi = parent::prepare_query( $si, $df[ 'lpost_id' ], 'accommodation_image', $mod );
                $ri = parent::query( $qi ); 
                $di = parent::fetch_array( $ri );

                if( empty( $di[ 'lattach' ] ) || ( !empty( $di[ 'lattach' ] ) && !file_exists( IMAGE_DIR . '/Uploads/' . $di[ 'lattach' ] ) ) )
                {
                    $img = 'default.png';
                }
                else
                {
                    $img = $di[ 'lattach' ];
                }

                $data[] = array(
                    'limage'        => HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=90&h=67&src=' . HT_SERVER . SITE_URL . '/images/Uploads/' . $img,
                    'lacco_type'    => $this->accommodation->accommodation_type( $df[ 'laccommodation_type' ] ),
                    'lsurf_trip'    => $this->accommodation->accommodation_surf_trip( $df[ 'lsurf_trip' ] ),
                    'ltitle'        => ucwords( strtolower( $df[ 'ltitle' ] ) ),
                    'lpost_id'      => $df[ 'lpost_id' ]
                );
            }
        }

        return json_encode( array(
            'draw'            => intval( $this->post[ 'draw' ] ),
            'recordsFiltered' => intval( $nf ),
            'recordsTotal'    => intval( $nf ),
            'data'            => $data
        ));
    }

    function load_latest_booking()
    {
        //-- Query Limit
        $limit = parent::prepare_query( ' LIMIT ' . $this->post[ 'start' ] . ', ' . $this->post[ 'length' ] );
        
        //-- Query Order By
        $order = parent::prepare_query( ' ORDER BY a.lcreated_date DESC' );

        //-- Query Filter
        $where = '';

        if( $this->sess[ 'usertype' ] == 2 )
        {
            $where = parent::prepare_query( ' WHERE ( SELECT a2.lusertype_id FROM lumonata_user AS a2 WHERE a2.lusername = a.lusername ) = %d', 2 );
        }

        //-- Main Query
        $qf = 'SELECT 
                a.*, (
                    SELECT a2.ltitle FROM lumonata_post AS a2 WHERE a2.lpost_id = a.lpost_id
                ) AS laccommodation
               FROM lumonata_booking AS a' . $where . $order . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        $data = array();

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
                $data[] = array(
                    'ltotal'         => $this->global->get_format_price( $df[ 'ltotal' ] ),
                    'lcreated_date'  => date( 'd F Y', $df[ 'lcreated_date' ] ),
                    'laccommodation' => $df[ 'laccommodation' ],
                    'lbooking_code'  => $df[ 'lbooking_code' ],
                    'lbooking_id'    => $df[ 'lbooking_id' ],
                    'lfullname'      => $df[ 'lfullname' ]
                );
            }
        }

        return json_encode( array(
            'draw'            => intval( $this->post[ 'draw' ] ),
            'recordsFiltered' => intval( $nf ),
            'recordsTotal'    => intval( $nf ),
            'data'            => $data
        ));
    }

    function language()
    {
        $this->lang = $this->global->getSettingValue( 'llanguage' );
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();
        $this->post    = $_POST;
    }

    function import()
    {
        require_once( ADMIN_APPS_DIR . '/Accommodations/accommodation/class.php' );

        $this->accommodation = new accommodation( ADMIN_APPS_DIR . '/Accommodations/accommodation/', new stdClass() );
    }

    function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'load_latest_accommodation' )
            {
                echo $this->load_latest_accommodation();
            }
            
            if( $this->post[ 'pKEY' ] == 'load_latest_booking' )
            {
                echo $this->load_latest_booking();
            }
        }

        exit;
    }
}

?> 