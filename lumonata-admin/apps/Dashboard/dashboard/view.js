function init_table()
{
    var table = jQuery('#table-latest-accommodation').DataTable({
        lengthChange: false,
        deferRender: true,
        processing: true,
        serverSide: true,
        pageLength: 5,
        info: false,
        order: [],
        ajax: {
            url: jQuery('#ajax_url').val(),
            type: 'POST',
            data: function( d ){
                d.pKEY = 'load_latest_accommodation';
            }
        },
        columns: [
            {
                data: 'limage', render: function ( data, type, row ){
                    var html = '<img src="'+ data +'" class="avatar-dsh-property" alt="" />';

                    return html;
                }
            },
            {
                data: 'ltitle', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'lacco_type', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'lsurf_trip', render: function ( data, type, row ){
                    return data;
                }
            }
        ],
        dom: 't'
    });

    var gtable = jQuery('#table-latest-booking').DataTable({
        lengthChange: false,
        deferRender: true,
        processing: true,
        serverSide: true,
        pageLength: 10,
        info: false,
        order: [],
        ajax: {
            url: jQuery('#ajax_url').val(),
            type: 'POST',
            data: function( d ){
                d.pKEY = 'load_latest_booking';
            }
        },
        columns: [
            {
                data: 'lbooking_code', render: function ( data, type, row ){
                    var html = '<a href="javascript:;" class="actionv" data-id="' + row.lbooking_id + '"><b>' + data + '</b></a>';

                    return html;
                }
            },
            {
                data: 'lfullname', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'laccommodation', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'lcreated_date', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'ltotal', render: function ( data, type, row ) {
                    return data;
                },
                className: 'text-right'
            }
        ],
        dom: 't'
    });

    gtable.on( 'draw.dt', function () {
        init_booking_action( gtable );
    });
}

function init_booking_action()
{
    jQuery('.actionv').unbind();
    jQuery('.actionv').on('click', function(){
        var src = jQuery('#ajax_booking_url').val();
        var bid = jQuery(this).data('id');

        jQuery.fancybox.open({
            type: 'ajax',
            src: src,
            opts: {
                baseClass: 'popup-booking',
                closeExisting: true,
                ajax: { 
                    settings: { 
                        type: 'POST',
                        data: {
                            pKEY: 'popup',
                            bid: bid
                        }
                    }
                }
            }
        });
    });
}

jQuery(document).ready(function(){
    init_table();
});