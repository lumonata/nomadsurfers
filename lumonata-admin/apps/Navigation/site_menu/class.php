<?php

class site_menu extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }
    
    function view( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );
        
        if( $this->global->getPViewBothP( $mod, $usertype ) )
        {
            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->create( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'mod', $mod );
            $this->template->set_var( 'cancel_css', 'hidden' );
            $this->template->set_var( 'action_label', 'CREATE' );
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'limit', $this->global->getSettingValue( 'list_admin' ) );


            $this->template->set_var( 'lmenu', $lmenu );
            $this->template->set_var( 'llang', $llang_id );
            $this->template->set_var( 'lmenu_id', $lmenu_id );
            $this->template->set_var( 'ltrigger', $ltrigger );
            $this->template->set_var( 'lorder_id', $lorder_id );
            $this->template->set_var( 'lcustom_url', $lcustom_url );

            $this->template->set_var( 'llang_id', $this->get_language_option() );
            $this->template->set_var( 'ltarget', $this->get_menu_target_option( $ltarget ) );
            $this->template->set_var( 'llink_type', $this->get_menu_type_option( $llink_type ) );
            $this->template->set_var( 'lgroup_id', $this->get_menu_group_option( $lgroup_id, $llang_id ) );
            $this->template->set_var( 'lparent_id', $this->get_menu_parent_option( $lparent_id, $llang_id ) );
            $this->template->set_var( 'lmodule_id', $this->get_menu_module_option( $lmodule_id, $llink_type ) );
            $this->template->set_var( 'lapp_id', $this->get_menu_application_option( $lapp_id, $lmodule_id, $llink_type, $llang_id ) );
            $this->template->set_var( 'switcher_lang', $this->switcher_language( $mod, $llang_id, $this->ref ) );

            if( $llink_type == '0' )
            {
                $this->template->set_var( 'attr_custom_url', '' );
            }
            else
            {
                $this->template->set_var( 'attr_custom_url', 'disabled' );
            }

            if( in_array( $llink_type, array( '2', '3', '4', '5', '6' ) ) )
            {
                $this->template->set_var( 'attr_module', '' );
            }
            else
            {
                $this->template->set_var( 'attr_module', 'disabled' );
            }

            if( in_array( $llink_type, array( '2', '4', '6' ) ) )
            {
                $this->template->set_var( 'attr_app', '' );
            }
            else
            {
                $this->template->set_var( 'attr_app', 'disabled' );
            }

            if( $llink_type == '7' )
            {
                $this->template->set_var( 'attr_trigger', '' );
            }
            else
            {
                $this->template->set_var( 'attr_trigger', 'disabled' );
            }

            $this->template->set_var( 'site_url', SITE_URL );
            $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Navigation/site_menu/view.js' ) );
            
            $this->template->Parse( 'vC', 'viewContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }
    
    function insert( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );
        
        if( $this->global->getPViewBothP( $mod, $usertype ) )
        {
            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->create( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'mod', $mod );
            $this->template->set_var( 'cancel_css', 'hidden' );
            $this->template->set_var( 'action_label', 'CREATE' );
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'limit', $this->global->getSettingValue( 'list_admin' ) );

            $this->template->set_var( 'lmenu', $lmenu );
            $this->template->set_var( 'llang', $llang_id );
            $this->template->set_var( 'lmenu_id', $lmenu_id );
            $this->template->set_var( 'ltrigger', $ltrigger );
            $this->template->set_var( 'lorder_id', $lorder_id );
            $this->template->set_var( 'lcustom_url', $lcustom_url );

            $this->template->set_var( 'llang_id', $this->get_language_option() );
            $this->template->set_var( 'ltarget', $this->get_menu_target_option( $ltarget ) );
            $this->template->set_var( 'llink_type', $this->get_menu_type_option( $llink_type ) );
            $this->template->set_var( 'lgroup_id', $this->get_menu_group_option( $lgroup_id, $llang_id ) );
            $this->template->set_var( 'lparent_id', $this->get_menu_parent_option( $lparent_id, $llang_id ) );
            $this->template->set_var( 'lmodule_id', $this->get_menu_module_option( $lmodule_id, $llink_type ) );
            $this->template->set_var( 'lapp_id', $this->get_menu_application_option( $lapp_id, $lmodule_id, $llink_type, $llang_id ) );
            $this->template->set_var( 'switcher_lang', $this->switcher_language( $mod, $llang_id, $this->ref ) );

            if( $llink_type == '0' )
            {
                $this->template->set_var( 'attr_custom_url', '' );
            }
            else
            {
                $this->template->set_var( 'attr_custom_url', 'disabled' );
            }

            if( in_array( $llink_type, array( '2', '3', '4', '5', '6' ) ) )
            {
                $this->template->set_var( 'attr_module', '' );
            }
            else
            {
                $this->template->set_var( 'attr_module', 'disabled' );
            }

            if( in_array( $llink_type, array( '2', '4', '6' ) ) )
            {
                $this->template->set_var( 'attr_app', '' );
            }
            else
            {
                $this->template->set_var( 'attr_app', 'disabled' );
            }

            if( $llink_type == '7' )
            {
                $this->template->set_var( 'attr_trigger', '' );
            }
            else
            {
                $this->template->set_var( 'attr_trigger', 'disabled' );
            }

            $this->template->set_var( 'site_url', SITE_URL );
            $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Navigation/site_menu/view.js' ) );
            
            $this->template->Parse( 'vC', 'viewContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function edit( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $this->valid )
        {
            if( $this->global->getPEditBoth( $mod, $usertype ) )
            {
                //-- EXECUTE update action
                if( isset( $this->post[ 'save' ] ) )
                {
                    $this->change( $mod );
                }

                //-- EXTRACT fields
                extract( $this->fields );

                $this->template->set_var( 'mod', $mod );
                $this->template->set_var( 'action_label', 'CHANGE' );
                $this->template->set_var( 'message', $this->message );
                $this->template->set_var( 'limit', $this->global->getSettingValue( 'list_admin' ) );

                $this->template->set_var( 'lmenu', $lmenu );
                $this->template->set_var( 'llang', $llang_id );
                $this->template->set_var( 'lmenu_id', $lmenu_id );
                $this->template->set_var( 'ltrigger', $ltrigger );
                $this->template->set_var( 'lorder_id', $lorder_id );
                $this->template->set_var( 'lcustom_url', $lcustom_url );

                $this->template->set_var( 'llang_id', $this->get_language_option() );
                $this->template->set_var( 'ltarget', $this->get_menu_target_option( $ltarget ) );
                $this->template->set_var( 'llink_type', $this->get_menu_type_option( $llink_type ) );
                $this->template->set_var( 'lgroup_id', $this->get_menu_group_option( $lgroup_id, $llang_id ) );
                $this->template->set_var( 'lparent_id', $this->get_menu_parent_option( $lparent_id, $llang_id ) );
                $this->template->set_var( 'lmodule_id', $this->get_menu_module_option( $lmodule_id, $llink_type ) );
                $this->template->set_var( 'lapp_id', $this->get_menu_application_option( $lapp_id, $lmodule_id, $llink_type, $llang_id ) );
                $this->template->set_var( 'switcher_lang', $this->switcher_language( $mod, $llang_id, $this->ref ) );

                if( $llink_type == '0' )
                {
                    $this->template->set_var( 'attr_custom_url', '' );
                }
                else
                {
                    $this->template->set_var( 'attr_custom_url', 'disabled' );
                }

                if( in_array( $llink_type, array( '2', '3', '4', '5', '6' ) ) )
                {
                    $this->template->set_var( 'attr_module', '' );
                }
                else
                {
                    $this->template->set_var( 'attr_module', 'disabled' );
                }

                if( in_array( $llink_type, array( '2', '4', '6' ) ) )
                {
                    $this->template->set_var( 'attr_app', '' );
                }
                else
                {
                    $this->template->set_var( 'attr_app', 'disabled' );
                }

                if( $llink_type == '7' )
                {
                    $this->template->set_var( 'attr_trigger', '' );
                }
                else
                {
                    $this->template->set_var( 'attr_trigger', 'disabled' );
                }

                $this->template->set_var( 'site_url', SITE_URL );
                $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
                $this->template->set_var( 'cancel_url', HT_SERVER . ADMIN_URL . '/home.php?mod=' . $mod . '&prc=view' );
                $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

            	$this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Navigation/site_menu/view.js' ) );
                
                $this->template->Parse( 'vC', 'viewContent', true );
            }
            else
            {
                $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
            }
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NOT_FOUND ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function load( $mod, $usertype )
    {
        $cols  = array(
            1 => 'a.lmenu',
            2 => 'a.lparent_id',
            3 => 'a.lgroup_id',
            4 => 'b.llink_type'
        );

        //-- Query Limit
        $limit = parent::prepare_query( ' LIMIT ' . $this->post[ 'start' ] . ', ' . $this->post[ 'length' ] );
    
        //-- Query Order By
        if( empty( $this->post[ 'order' ] ) )
        {
            $order = parent::prepare_query( ' ORDER BY a.llang_id, a.lgroup_id, a.lparent_id, a.lorder_id ASC' );
        }
        else
        {
            $order = array();

            foreach( $this->post[ 'order' ] as $i => $od )
            {
                if( isset( $cols[ $od[ 'column' ] ] ) )
                {
                    $order[] = $cols[ $od[ 'column' ] ] . ' ' . $od[ 'dir' ];
                }
            }

            if( empty( $order ) )
            {
                $order = parent::prepare_query( ' ORDER BY a.llang_id, a.lgroup_id,, a.lparent_id, a.lorder_id ASC' );
            }
            else
            {
                $order = parent::prepare_query( ' ORDER BY ' . implode( ', ', $order ) );
            }
        }

        //-- Query Additional Where
        $w = array();

        if( $this->post[ 'llang_id' ] != '' )
        {
            $w[] = parent::prepare_query( 'a.llang_id = %d', $this->post[ 'llang_id' ] );
        }

        if( $this->post[ 'lsearch' ] != '' )
        {
            $s = array();

            foreach( $cols as $col )
            {
                $s[] = parent::prepare_query( $col . ' LIKE %s', '%' . $this->post[ 'lsearch' ] . '%' );
            }

            $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
        }

        if( empty( $w ) === false )
        {
            $where = ' HAVING ' . implode( ' AND ', $w );
        }
        else
        {
            $where = '';
        }

        //-- Main Query
        $qm = 'SELECT 
                a.lmenu,
                a.lmenu_id,
                a.llang_id,
                a.lgroup_id,
                a.lparent_id,
                a.llink_type
               FROM lumonata_menu AS a' . $where . $order;
        $rm = parent::query( $qm );
        $nm = parent::num_rows( $rm );

        $qf = $qm . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        $data = array();

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
                $data[] = array(
                    'lgroupname' => $this->global->getFields( 'lumonata_menu_group', 'ltitle', array( 'lgroup_id' => $df[ 'lgroup_id' ] ) ),
                    'leditlink'  => $this->global->getEditUrl( $mod, $usertype, $df[ 'lmenu_id' ] ),
                    'lflag'      => $this->global->get_language_flag( $df[ 'llang_id' ] ),
                    'llink_type' => $this->get_menu_type_label( $df[ 'llink_type' ] ),
                    'lparent'    => $this->get_menu_parent( $df[ 'lparent_id' ] ),
                    'lmenu_id'   => $df[ 'lmenu_id' ],
                    'lmenu'      => $df[ 'lmenu' ]
                );
            }
        }

        return json_encode( array(
            'draw'            => intval( $this->post[ 'draw' ] ),
            'recordsFiltered' => intval( $nm ),
            'recordsTotal'    => intval( $nf ),
            'data'            => $data
        ));
    }

    function validate( $data = array(), $update = false )
    {
        $error = array();

        foreach( $data as $field => $dt )
        {
            if( $field == 'lgroup_id' && empty( $dt ) )
            {
                array_push( $error, 'Please input menu group' );
            }

            if( $field == 'lmenu' && empty( $dt ) )
            {
                array_push( $error, 'Please input menu title' );
            }

            if( $field == 'lmodule_id' && in_array( $data[ 'llink_type' ], array( '2', '3', '4', '5', '6' ) ) && $dt == '' )
            {
                array_push( $error, 'Please select module' );
            }

            if( $field == 'lapp_id' && in_array( $data[ 'llink_type' ], array( '2', '4', '6' ) ) && $dt == '' )
            {
                array_push( $error, 'Please select application' );
            }

            if( $field == 'lcustom_url' && $data[ 'llink_type' ] == '0' && $dt == '' )
            {
                array_push( $error, 'Please input URL link' );
            }
        }

        return $error;
    }

    function create( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- INSERT lumonata_menu
            $param  = array_diff_key( $data, array_flip( array( 'lmenu_id' ) ) );
            $result = parent::insert( 'lumonata_menu', array_filter( $param ) );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to add new menu', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

                exit;
            }
            else
            {
                $lmenu_id = parent::insert_id();

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully add new menu' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $lmenu_id );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

            exit;
        }
    }

    function change( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- UPDATE lumonata_menu
            $param  = array_diff_key( $data, array_flip( array( 'lmenu_id' ) ) );
            $where  = array( 'lmenu_id' => $data[ 'lmenu_id' ] );

            $result = parent::update( 'lumonata_menu', $param, $where );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to update existing menu', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

                exit;
            }
            else
            {
                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully updated existing menu' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

            exit;
        }
    }

    function do_delete()
    {
        parent::begin();

        $commit = 1;

        $s = 'DELETE FROM lumonata_menu WHERE lmenu_id = %d';
        $q = parent::prepare_query( $s, $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            $commit = 0;
        }
                
        if( $commit == 0 )
        {
            parent::rollback();

            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            parent::commit();

            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function do_change_menu_type()
    {
        if( $this->post[ 'type' ] == '0' )
        {
            return '
            <div class="list">
                <label>URL</label>
                <input name="fields[lcustom_url]" type="text" class="text custom-url" required/>
            </div>
            <div class="list">
                <label>Modules</label>
                <div class="chzn-selecto">
                    <select name="fields[lmodule_id]" class="chzn-select list-modules" style="width: 100%;" disabled>
                        <option value="">Select Modules</option>
                    </select>
                </div>
            </div>
            <div class="list">
                <label>Applications</label>
                <div class="chzn-selecto">
                    <select name="fields[lapp_id]" class="chzn-select list-application" style="width: 100%;" disabled>
                        <option value="">Select Application</option>
                    </select>
                </div>
            </div>';
        }
        elseif( $this->post[ 'type' ] == '1' )
        {
            return '
            <div class="list">
                <label>URL</label>
                <input name="fields[lcustom_url]" type="text" class="text custom-url" disabled />
            </div>
            <div class="list">
                <label>Modules</label>
                <div class="chzn-selecto">
                    <select name="fields[lmodule_id]" class="chzn-select list-modules" style="width: 100%;" disabled>
                        <option value="">Select Modules</option>
                    </select>
                </div>
            </div>
            <div class="list">
                <label>Applications</label>
                <div class="chzn-selecto">
                    <select name="fields[lapp_id]" class="chzn-select list-application" style="width: 100%;" disabled>
                        <option value="">Select Application</option>
                    </select>
                </div>
            </div>';
        }
        elseif( $this->post[ 'type' ] == '2' )
        {
            return '
            <div class="list">
                <label>URL</label>
                <input name="fields[lcustom_url]" type="text" class="text custom-url" disabled />
            </div>
            <div class="list">
                <label>Modules</label>
                <div class="chzn-selecto">
                    <select name="fields[lmodule_id]" class="chzn-select list-modules" style="width: 100%;" required>
                        <option value="">Select Modules</option>
                        ' . $this->get_menu_module_option( $this->post[ 'modid' ], $this->post[ 'type' ] ) . '
                    </select>
                </div>
            </div>
            <div class="list">
                <label>Applications</label>
                <div class="chzn-selecto">
                    <select name="fields[lapp_id]" class="chzn-select list-application" style="width: 100%;" required>
                        <option value="">Select Application</option>
                        ' . $this->get_menu_application_option( null, $this->post[ 'modid' ], $this->post[ 'type' ], $this->post[ 'lang' ] ) . '
                    </select>
                </div>
            </div>';
        }
        elseif( in_array( $this->post[ 'type' ], array( '3', '5' ) ) )
        {
            return '
            <div class="list">
                <label>URL</label>
                <input name="fields[lcustom_url]" type="text" class="text custom-url" disabled />
            </div>
            <div class="list">
                <label>Modules</label>
                <div class="chzn-selecto">
                    <select name="fields[lmodule_id]" class="chzn-select list-modules" style="width: 100%;" required>
                        <option value="">Select Modules</option>
                        ' . $this->get_menu_module_option( $this->post[ 'modid' ], $this->post[ 'type' ] ) . '
                    </select>
                </div>
            </div>
            <div class="list">
                <label>Applications</label>
                <div class="chzn-selecto">
                    <select name="fields[lapp_id]" class="chzn-select list-application" style="width: 100%;" disabled>
                        <option value="">Select Application</option>
                    </select>
                </div>
            </div>';
        }
        elseif( in_array( $this->post[ 'type' ], array( '4', '6' ) ) )
        {
            return '
            <div class="list">
                <label>URL</label>
                <input name="fields[lcustom_url]" type="text" class="text custom-url" disabled />
            </div>
            <div class="list">
                <label>Modules</label>
                <div class="chzn-selecto">
                    <select name="fields[lmodule_id]" class="chzn-select list-modules" style="width: 100%;" required>
                        <option value="">Select Modules</option>
                        ' . $this->get_menu_module_option( $this->post[ 'modid' ], $this->post[ 'type' ] ) . '
                    </select>
                </div>
            </div>
            <div class="list">
                <label>Applications</label>
                <div class="chzn-selecto">
                    <select name="fields[lapp_id]" class="chzn-select list-application" style="width: 100%;" required>
                        <option value="">Select Application</option>
                        ' . $this->get_menu_application_option( null, $this->post[ 'modid' ], $this->post[ 'type' ], $this->post[ 'lang' ] ) . '
                    </select>
                </div>
            </div>';
        }
        elseif( $this->post[ 'type' ] == '7' )
        {
            return '
            <div class="list">
                <label>URL</label>
                <input name="fields[lcustom_url]" type="text" class="text custom-url" />
            </div>
            <div class="list">
                <label>Trigger</label>
                <input name="fields[ltrigger]" type="text" class="text ltrigger-id" />
            </div>';
        }
    }

    function switcher_language( $mod, $llang_id, $lref_id )
    {
        $s = 'SELECT a.llang_id FROM lumonata_menu AS a WHERE a.lref_id IS NOT NULL AND a.lref_id = %d ORDER BY a.lorder_id';
        $q = parent::prepare_query( $s, $lref_id );
        $r = parent::query( $q );

        $translation = array();

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_assoc( $r ) )
            {
                array_push( $translation, $d[ 'llang_id' ] );
            }
        }

        if( empty( $lref_id ) )
        {
            return $this->global->get_switcher_language( $mod, 'lumonata_menu', $llang_id, $lref_id, $translation, false, false, true );
        }
        else
        {
            return $this->global->get_switcher_language( $mod, 'lumonata_menu', $llang_id, $lref_id, $translation, false, true, true );
        }
    }

    function language()
    {
        if( isset( $_GET[ 'lang' ] ) && $_GET[ 'lang' ] != '' )
        {
            $this->lang = $_GET[ 'lang' ];
        }
        else
        {
            $this->lang = $this->global->getSettingValue( 'llanguage' );
        }
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();
        $this->ref     = null;
        $this->valid   = true;

        if( isset( $_GET[ 'id' ] ) )
        {
            $s = 'SELECT * FROM lumonata_menu AS a WHERE a.lmenu_id = %d';
            $q = parent::prepare_query( $s, $_GET[ 'id' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $d = parent::fetch_array( $r );

                $fields = array(
                    'lcreated_by'   => $d[ 'lcreated_by' ],
                    'lusername'     => $d[ 'lusername' ],
                    'llang_id'      => $d[ 'llang_id' ],
                    'lref_id'       => $d[ 'lref_id' ],
                    'lmenu_id'      => $d[ 'lmenu_id' ],
                    'lcreated_date' => $d[ 'lcreated_date' ],
                    'ldlu'          => $d[ 'ldlu' ],
                    'lmenu'         => $d[ 'lmenu' ],
                    'llink_type'    => $d[ 'llink_type' ],
                    'lmodule_id'    => $d[ 'lmodule_id' ],
                    'lapp_id'       => $d[ 'lapp_id' ],
                    'lcustom_url'   => $d[ 'lcustom_url' ],
                    'ltrigger'      => $d[ 'ltrigger' ],
                    'ltarget'       => $d[ 'ltarget' ],
                    'lgroup_id'     => $d[ 'lgroup_id' ],
                    'lparent_id'    => $d[ 'lparent_id' ],
                    'lorder_id'     => $d[ 'lorder_id' ]
                );

                if( is_null( $d[ 'lref_id' ] ) )
                {
                    $this->ref = $d[ 'lmenu_id' ];
                }
                else
                {
                    $this->ref = $d[ 'lref_id' ];
                }
            }
            else
            {
                $this->valid = false;
            }
        }
        else
        {
            if( isset( $_GET[ 'ref' ] ) )
            {
                $this->ref = $_GET[ 'ref' ];
            }

            $fields = array(
                'lcreated_by'   => $this->sess[ 'username' ],
                'lusername'     => $this->sess[ 'username' ],
                'llang_id'      => $this->lang,
                'lref_id'       => $this->ref,
                'lmenu_id'      => time(),
                'lcreated_date' => time(),
                'ldlu'          => time(),
                'ltarget'       => '_self',
                'lgroup_id'     => '',
                'llink_type'    => '0',
                'lmenu'         => '',
                'lmodule_id'    => '',
                'lapp_id'       => '',
                'lcustom_url'   => '',
                'ltrigger'      => '',
                'lparent_id'    => 0,
                'lorder_id'     => 0
            );

            if( isset( $_GET[ 'ref' ] ) )
            {
                $s = 'SELECT * FROM lumonata_menu AS a WHERE a.lmenu_id = %d';
                $q = parent::prepare_query( $s, $_GET[ 'ref' ] );
                $r = parent::query( $q );

                if( parent::num_rows( $r ) > 0 )
                {
                    $d = parent::fetch_array( $r );

                    $fields = array_merge( $fields, array( 'lmenu' => $d[ 'lmenu' ] ) );
                }
            }
        }

        if( empty( $_POST ) )
        {
            if( empty( $this->notif ) === false && isset( $this->notif[ 'post' ] ) )
            {
                $this->fields = array_merge( $fields, $this->notif[ 'post' ] );
            }
            else
            {
                if( isset( $fields ) )
                {
                    $this->fields = $fields;   
                }
            }

            $this->post = array();
        }
        else
        {
            if( isset( $_POST[ 'fields' ] ) )
            {
                $this->fields = array_merge( $fields, $_POST[ 'fields' ] );
            }
            else
            {
                $this->fields = $fields;
            }

            $this->post = array_diff_key( $_POST, array_flip( array( 'fields' ) ) );
        }
    }

    function get_language_option( $llang_id = NULL )
    {
        return $this->global->set_language_option( $llang_id, false );
    }

    function get_menu_group_option( $lgroup_id = NULL, $llang_id = '' )
    {
        $q = parent::prepare_query( 'SELECT * FROM lumonata_menu_group AS a WHERE a.llang_id = %d ORDER BY a.lorder_id', $llang_id );

        return $this->global->set_option( $q, 'lgroup_id', 'ltitle', $lgroup_id, false );
    }

    function get_menu_type_option( $llink_type = NULL )
    {
        return $this->global->set_static_option( array( 
            '0' => 'Custom URL',
            '1' => 'Homepage',
            '2' => 'Pages',
            '3' => 'Post Archive',
            '4' => 'Post Detail',
            '5' => 'Term Archive',
            '6' => 'Term Detail',
            '7' => 'Popup',
        ), $llink_type, false );
    }

    function get_menu_target_option( $ltarget = NULL )
    {
        return $this->global->set_static_option( array( 
            '_self'   => 'Same frame as it was clicked',
            '_blank'  => 'New window or tab',
            '_parent' => 'The parent frame',
            '_top'    => 'The full body of the window'
        ), $ltarget, false );
    }

    function get_menu_parent_option( $lparent_id = 0, $llang_id = '' )
    {
        $q = parent::prepare_query( 'SELECT a.lmenu_id, a.lmenu FROM lumonata_menu AS a WHERE a.llang_id = %d ORDER BY a.lorder_id', $llang_id );

        return $this->global->set_option( $q, 'lmenu_id', 'lmenu', $lparent_id, false );
    }

    function get_menu_module_option( $lmodule_id = NULL, $llink_type = '0' )
    {
        if( $llink_type == '3' || $llink_type == '4' )
        {
            $q = parent::prepare_query( 'SELECT a.lmodule_id, a.ltitle FROM lumonata_module AS a WHERE a.larchive = %s ORDER BY a.lmorder_id', 1 );
        }
        elseif( $llink_type == '5' || $llink_type == '6' )
        {
            $q = parent::prepare_query( 'SELECT a.lmodule_id, a.ltitle FROM lumonata_module AS a WHERE a.larchive = %s ORDER BY a.lmorder_id', 2 );
        }
        elseif( $llink_type == '2' )
        {
            $q = parent::prepare_query( 'SELECT a.lmodule_id, a.ltitle FROM lumonata_module AS a WHERE a.lapps = %s ORDER BY a.lmorder_id', 'pages' );
        }

        if( isset( $q ) && $q != '' )
        {
            return $this->global->set_option( $q, 'lmodule_id', 'ltitle', $lmodule_id, false );
        }
    }

    function get_menu_application_option( $lapp_id = NULL, $lmodule_id = NULL, $llink_type = '0', $llang_id = '' )
    {
        $apps = $this->global->getApps( $lmodule_id );

        if( !empty( $apps ) )
        {
            if( $llink_type == '2' || $llink_type == '4' )
            {
                $q = parent::prepare_query( 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = %s AND a.llang_id = %d ORDER BY a.lorder_id', $apps, $llang_id );

                return $this->global->set_option( $q, 'lpost_id', 'ltitle', $lapp_id, false );
            }
            elseif( $llink_type == '6' )
            {
                if( $apps == 'accommodation_surf_trips' )
                {
                    $apps = 'surf_trip';
                }

                $q = parent::prepare_query( 'SELECT a.lterm_id, a.lname FROM lumonata_post_terms AS a WHERE a.lrule = %s AND a.llang_id = %d ORDER BY a.lorder_id', $apps, $llang_id );

                return $this->global->set_option( $q, 'lterm_id', 'lname', $lapp_id, false );
            }
        }
    }

    function get_menu_type_label( $llink_type )
    {
        $stack = array( 
            '0' => 'Custom URL',
            '1' => 'Homepage',
            '2' => 'Pages',
            '3' => 'Post Archive',
            '4' => 'Post Detail',
            '5' => 'Term Archive',
            '6' => 'Term Detail',
            '7' => 'Popup',
        );

        if( isset( $stack[ $llink_type ] ) )
        {
            return $stack[ $llink_type ];
        }
    }

    function get_menu_parent( $lparent_id = 0 )
    {
        if( $lparent_id == 0 )
        {
            return 'Root';
        }
        else
        {
            return $this->global->set_recursive_label( $lparent_id, 'lumonata_menu', 'lmenu', 'lmenu_id', 'lparent_id' );
        }
    }

    function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'load' )
            {
                echo $this->load( $mod, $usertype );
            }

            if( $this->post[ 'pKEY' ] == 'do_delete' )
            {
                echo $this->do_delete();
            }

            if( $this->post[ 'pKEY' ] == 'do_change_menu_type' )
            {
                echo $this->do_change_menu_type();
            }
        }

        exit;
    }
}

?> 