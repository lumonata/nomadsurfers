function init_table()
{
	var limit = parseInt( jQuery('#limit').val() ) || 1;
    var table = jQuery('#table').DataTable({
        lengthChange: false,
        deferRender: true,
        pageLength: limit,
        processing: true,
        serverSide: true,
        info: false,
        order: [],
        ajax: {
            url: jQuery('#ajax_url').val(),
            type: 'POST',
            data: function( d ){
                d.llang_id = jQuery('[name=llang_id]').val();
                d.lsearch  = jQuery('[name=lsearch]').val();
                d.pKEY     = 'load';
            }
        },
        columns: [
            {
                data: 'lmenu', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'lgroupname', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'lparent', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'llink_type', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'lflag', render: function ( data, type, row ){
                    return data;
                },
                className: 'text-center'
            },
            {
                data: 'lmenu_id', render: function ( data, type, row ) {
                    var html = 
                    '<a class="actione actione2" href="' + row.leditlink + '" title="Edit"></a>'+
                    '<a class="actiond actiond2" data-id="' + row.lmenu_id + '" title="Delete"></a>';

                    return html;
                },
                className: 'text-center'
            }    
        ],
        dom: 'tp'
    });

    table.on( 'draw.dt', function () {
        jQuery(this).closest('.dataTables_wrapper').find('.dataTables_paginate').toggle( table.page.info().pages > 1 );
        init_action( table );
    });

    jQuery('[name=filter]').on('click', function(){
        table.ajax.reload();
    });

    jQuery('.link-type').on('change', function(){
        init_append_data();
    });

    jQuery('.list-modules').on('change', function(){
        init_append_data();
    });
}

function init_append_data()
{
    var url = jQuery('#ajax_url').val();
    var prm = new Object;
        prm.lang  = jQuery('.current-lang').val();
        prm.modid = jQuery('.list-modules').val();
        prm.type  = jQuery('.link-type').val();
        prm.pKEY  = 'do_change_menu_type';

    jQuery.ajax({
        url: url,
        data: prm,
        type: 'POST',
        dataType : 'html',
        success: function(e){
            if( e != '')
            {
                jQuery('.append-ajax').html( e );
                jQuery('.append-ajax .chzn-select').chosen({ search_contains: true });
                jQuery('.append-ajax .list-modules').on('change', function(){
                    init_append_data();
                });
            }
        },
        error: function(e){
            show_popup( 'Something wrong on the request' );
        }
    });
}

function init_action( table )
{
    jQuery('.actiond').unbind();
    jQuery('.actiond').on('click', function(){
        var id = jQuery(this).attr('data-id');

        jQuery('<div></div>').dialog({
            dialogClass: 'ui-dialog-no-titlebar',
            draggable: false,
            resizable: false,
            modal: true,
            open: function(){
                jQuery(this).html( 'Do you really want to DELETE this data?' );
            },
            buttons: [{
                text: 'Yes',
                click: function(){
                    var pop = jQuery( this ).dialog( 'close' );
                    var url = jQuery('#ajax_url').val();
                    var prm = new Object;
                        prm.pKEY = 'do_delete';
                        prm.id   = id;

                    jQuery.ajax({
                        url: url,
                        data: prm,
                        type: 'POST',
                        dataType : 'json',
                        success: function( e ){
                            pop.dialog('close');

                            if( e.result == 'success' )
                            {                        
                                table.ajax.reload( function(){
                                    show_popup( 'Successfully deleted data' );
                                    init_action( table );
                                }, false);
                            }
                            else
                            {
                                show_popup( 'Failed to delete data!' );
                            }
                        },
                        error: function( e ){
                            pop.dialog('close');
                                          
                            show_popup( 'Failed to delete data!' );
                        }
                    });
                }
            },{
                text: 'No',
                click: function(){
                    jQuery( this ).dialog( 'close' );
                }
            }]
        });
    });
}

jQuery(document).ready(function(){
    init_translation();
    init_table();
});