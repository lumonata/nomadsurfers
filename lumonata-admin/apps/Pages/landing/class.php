<?php

class landing extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->upload   = new upload();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }

    function view( $mod, $usertype )
    {
        $this->custom_field( $mod );

        if( $this->exist === false )
        {
            return $this->insert( $mod, $usertype );
        }
        else
        {
            return $this->edit( $mod, $usertype );
        }
    }

    function insert( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->global->getPInsertBoth( $mod, $usertype ) )
        {
            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->create( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'lmodule', $mod );
            $this->template->set_var( 'ltype', $ltype );
            $this->template->set_var( 'ltitle', $ltitle );
            $this->template->set_var( 'lsef_url', $lsef_url );
            $this->template->set_var( 'lpost_id', $lpost_id );
            $this->template->set_var( 'llang_id', $llang_id );
            $this->template->set_var( 'lcreated_by', $lcreated_by );
            $this->template->set_var( 'ldescription', $ldescription );
            $this->template->set_var( 'switcher_lang', $this->switcher_language( $mod, $llang_id, $this->ref ) );

            $this->template->set_var( 'lmeta_title', $lmeta_title );
            $this->template->set_var( 'lmeta_desc', $lmeta_desc );
            $this->template->set_var( 'lmeta_key', $lmeta_key );
            
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'ladditional', $this->actions->attemp_actions( 'landing_additional_field' ) );

            $this->template->set_var( 'site_url', SITE_URL );
            $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
            $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/blueimp-file-upload@10.32.0/js/jquery.fileupload.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/tinymce@5.10.2/tinymce.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( '//' . ADMIN_THEME_URL . '/assets/dropbox.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Pages/landing/form.js' ) );

            $this->template->Parse( 'fC', 'formContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function edit( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->global->getPEditBoth( $mod, $usertype ) )
        {
            //-- EXECUTE update action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->change( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'lmodule', $mod );
            $this->template->set_var( 'ltype', $ltype );
            $this->template->set_var( 'ltitle', $ltitle );
            $this->template->set_var( 'lsef_url', $lsef_url );
            $this->template->set_var( 'lpost_id', $lpost_id );
            $this->template->set_var( 'llang_id', $llang_id );
            $this->template->set_var( 'lcreated_by', $lcreated_by );
            $this->template->set_var( 'ldescription', $ldescription );
            $this->template->set_var( 'switcher_lang', $this->switcher_language( $mod, $llang_id, $this->ref ) );

            $this->template->set_var( 'lmeta_title', $lmeta_title );
            $this->template->set_var( 'lmeta_desc', $lmeta_desc );
            $this->template->set_var( 'lmeta_key', $lmeta_key );

            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'ladditional', $this->actions->attemp_actions( 'landing_additional_field' ) );

            $this->template->set_var( 'site_url', SITE_URL );
            $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
            $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/blueimp-file-upload@10.32.0/js/jquery.fileupload.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/tinymce@5.10.2/tinymce.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( '//' . ADMIN_THEME_URL . '/assets/dropbox.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Pages/landing/form.js' ) );

            $this->template->Parse( 'fC', 'formContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }
        
        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function validate( $data = array() )
    {
        $error = array();

        return $error;
    }

    function create( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- MERGE lsef_url INTO data array
            if( empty( $data[ 'lsef_url' ] ) )
            {
                $data = array_merge( $data, array( 'lsef_url' => $this->sef_url( $data[ 'ltitle' ] ) ) );
            }
            else
            {
                $data = array_merge( $data, array( 'lsef_url' => $this->sef_url( $data[ 'lsef_url' ] ) ) );
            }

            //-- INSERT lumonata_post
            $query  = http_build_query( array( 'mod' => $mod, 'prc' => 'view', 'lang' => $data[ 'llang_id' ] ) );
            $param  = array_diff_key( $data, array_flip( array( 'lpost_id', 'ladditional' ) ) );
            $result = parent::insert( 'lumonata_post', array_filter( $param ) );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to add new data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . $query );

                exit;
            }
            else
            {
                $lapp_id = parent::insert_id();

                //-- SYNC lumonata_attachment
                $param = array( 'lapp_id' => $lapp_id );
                $where = array( 'lapp_id' => $data[ 'lpost_id' ], 'lmodule_id' => $mod );

                parent::update( 'lumonata_attachment', $param, $where );

                //-- SYNC lumonata_additional_field
                if( isset( $data[ 'ladditional' ] ) )
                {
                    foreach( $data[ 'ladditional' ] as $key => $value )
                    {
                        if( is_array( $value ) )
                        {
                            $value = json_encode( $value );
                        }

                        parent::insert( 'lumonata_additional_field', array(
                            'ladditional_value' => $value,
                            'ladditional_key' => $key,
                            'lapp_id' => $lapp_id,
                            'lmodule_id' => $mod,
                        ));
                    }
                }

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully add new data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . $query );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . $query );

            exit;
        }
    }

    function change( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- MERGE lsef_url INTO data array
            if( empty( $data[ 'lsef_url' ] ) )
            {
                $data = array_merge( $data, array( 'lsef_url' => $this->sef_url( $data[ 'ltitle' ] ) ) );
            }

            //-- UPDATE lumonata_post
            $query  = http_build_query( array( 'mod' => $mod, 'prc' => 'view', 'lang' => $data[ 'llang_id' ] ) );
            $param  = array_diff_key( $data, array_flip( array( 'lpost_id', 'ladditional' ) ) );
            $where  = array( 'lpost_id' => $data[ 'lpost_id' ] );

            $result = parent::update( 'lumonata_post', array_filter( $param ), $where );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to update existing data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . $query );

                exit;
            }
            else
            {
                //-- SYNC lumonata_additional_field
                if( isset( $data[ 'ladditional' ] ) )
                {
                    foreach( $data[ 'ladditional' ] as $key => $value )
                    {
                        if( is_array( $value ) )
                        {
                            $value = json_encode( $value );
                        }

                        //-- Check Additonal Field Exist or Not
                        $aid = $this->global->getValueField3( 'lumonata_additional_field', 'ladditional_id', 'lapp_id', $data[ 'lpost_id' ], 'ladditional_key', $key, 'lmodule_id', $mod );

                        if( empty( $aid ) )
                        {
                            parent::insert( 'lumonata_additional_field', array(
                                'lapp_id' => $data[ 'lpost_id' ],
                                'ladditional_value' => $value,
                                'ladditional_key' => $key,
                                'lmodule_id' => $mod,
                            ));
                        }
                        else
                        {
                            $param = array( 'ladditional_value' => $value );
                            $where = array( 'ladditional_id' => $aid );

                            parent::update( 'lumonata_additional_field', $param, $where );
                        }
                    }
                }

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully updated existing data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . $query );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . $query );

            exit;
        }
    }

    function upload_file( $mod )
    {
        if( isset( $_FILES[ 'attachment' ] ) && $_FILES[ 'attachment' ][ 'error' ] == 0 )
        {
            global $db;

            extract( $this->post );

            $g = new globalAdmin();
            $u = new upload();

            $file_name = $_FILES[ 'attachment' ][ 'name' ];
            $file_size = $_FILES[ 'attachment' ][ 'size' ];
            $file_type = $_FILES[ 'attachment' ][ 'type' ];
            $file_tmp  = $_FILES[ 'attachment' ][ 'tmp_name' ];

            $sef_img  = $this->upload->file_name_filter( $file_name ) . '-' . time();
            $file     = $this->upload->rename_file( $file_name, $sef_img );

            $this->upload->upload_constructor( IMAGE_DIR . '/Uploads/' );

            if( $this->post[ 'file_type' ] == 'images' )
            {
                if( in_array( $file_type, array( 'image/jpg', 'image/jpeg', 'image/pjpeg', 'image/gif', 'image/png' ) ) )
                {
                    $this->upload->upload_resize( $file_name, $sef_img, $file_tmp, $file_type, 940, 940, 1 );
                }
                else
                {
                    return json_encode( array( 'result' => 'not-supported' ) );
                }
            }
            elseif( $this->post[ 'file_type' ] == 'word' && !in_array( $file_type, array( 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ) ) )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }
            elseif( $this->post[ 'file_type' ] == 'pdf' && $file_type != 'application/pdf' )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }
            elseif( $this->post[ 'file_type' ] == 'svg' && $file_type != 'image/svg+xml' )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }

            if( $this->upload->upload_file( $file_name, $sef_img, $file_tmp, 0 ) )
            {
                $order_id = $this->global->setCode( 'lumonata_attachment', 'lorder_id' );

                $r = $db->insert( 'lumonata_attachment', array(
                    'lcreated_by'   => $this->sess[ 'username' ],
                    'lusername'     => $this->sess[ 'username' ],
                    'llang_id'      => $this->lang,
                    'lmimetype'     => $file_type,
                    'lorder_id'     => $order_id,
                    'lsef_url'      => $sef_img,
                    'lapp_id'       => $app_id,
                    'ltitle'        => $title,
                    'lattach'       => $file,
                    'lmodule_id'    => $mod,
                    'lcreated_date' => time(),
                    'ldlu'          => time(),
                    'lstatus'       => 2
                ));

                if( is_array( $r ) )
                {
                    $this->upload->delete_file_thumb( $file );

                    return json_encode( array( 'result' => 'failed' ) );
                }
                else
                {
                    $id = $db->insert_id();

                    if( $file_type == 'image/jpg' || $file_type == 'image/jpeg' || $file_type == 'image/pjpeg' || $file_type == 'image/gif' || $file_type == 'image/png' )
                    {
                        //-- If file type is an image show the thumbnail
                        $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=180&h=180&src=' . HT_SERVER . IMAGE_URL . '/Uploads/' . $file;
                    }
                    else
                    {
                        if( in_array( $file_type, array( 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ) ) )
                        {
                            //-- Word file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-doc.png';
                        }
                        elseif( $file_type == 'application/pdf' )
                        {
                            //-- PDF file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-pdf.png';
                        }
                        elseif( $file_type == 'image/svg+xml' )
                        {
                            //-- SVG file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-svg.png';
                        }
                        else
                        {
                            //-- Other file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-file.png';
                        }
                    }

                    return json_encode( array( 'result' => 'success', 'src' => $src, 'id' => $id ) );
                }
            }
            else
            {
                $this->upload->delete_thumb( $file );

                return json_encode( array( 'result' => 'failed' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'error' ) );
        }
    }

    function delete_file()
    {
        if( isset( $this->post[ 'lattach_id' ] ) && !empty( $this->post[ 'lattach_id' ] ) )
        {
            $image  = $this->global->getValueField( 'lumonata_attachment', 'lattach', 'lattach_id', $this->post[ 'lattach_id' ] );

            $result = parent::delete( 'lumonata_attachment', array( 'lattach_id' => $this->post[ 'lattach_id' ] ) );

            if( is_array( $result ) )
            {
                return json_encode( array( 'result' => 'error' ) );
            }
            else
            {
                if( empty( $image ) === false )
                {
                    $this->upload->upload_constructor( IMAGE_DIR . '/Uploads/' );
                    $this->upload->delete_file_thumb( $image );
                }

                return json_encode( array( 'result' => 'success' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
    }

    function order_file()
    {
        if( isset( $this->post[ 'image' ] ) && !empty( $this->post[ 'image' ] ) )
        {
            $error = 0;

            foreach( $this->post[ 'image' ] as $order_id => $attach_id )
            {
                $param  = array( 'lorder_id' => $order_id );
                $where  = array( 'lattach_id' => $attach_id );

                $result = parent::update( 'lumonata_attachment', $param, $where );

                if( is_array( $result ) )
                {
                    $error++;
                }
            }

            if( empty( $error ) == false )
            {
                return json_encode( array( 'result' => 'error', 'message' => 'Failed to reorder some image' ) );
            }
            else
            {
                return json_encode( array( 'result' => 'success' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'error', 'message' => 'No images was found to reorder' ) );
        }
    }

    function add_group()
    {
        if( isset( $this->post[ 'items' ] ) && !empty( $this->post[ 'items' ] ) )
        {
            $items  = json_decode( base64_decode( $this->post[ 'items' ] ), true );
            $object = json_decode( base64_decode( $this->post[ 'object' ] ), true );

            return $this->global->add_repeater_items( $items, $object );
        }
    }

    function sef_url( $string = '' )
    {
        $num = $this->global->getNumRows( 'lumonata_post', 'ltitle', $string );

        if( $num > 0 )
        {
            for( $i = 2; $i <= $num + 1; $i++ )
            {
                $sef = $this->global->sef_url( $string ) . '-' . $i;
                
                if( $this->global->getNumRows( 'lumonata_post', 'lsef_url', $sef ) < 1 )
                {
                    $sef = $sef;

                    break;
                }
            }
        }
        else
        {
            $sef = $this->global->sef_url( $string );
        }
        
        return $sef;
    }

    function switcher_language( $mod, $llang_id, $lref_id )
    {
        $s = 'SELECT a.llang_id FROM lumonata_post AS a WHERE a.lref_id IS NOT NULL AND a.lref_id = %d ORDER BY a.lorder_id';
        $q = parent::prepare_query( $s, $lref_id );
        $r = parent::query( $q );

        $translation = array();

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_assoc( $r ) )
            {
                array_push( $translation, $d[ 'llang_id' ] );
            }
        }

        if( empty( $lref_id ) )
        {
            return $this->global->get_switcher_language( $mod, 'lumonata_post', $llang_id, $lref_id, $translation, true );
        }
        else
        {
            return $this->global->get_switcher_language( $mod, 'lumonata_post', $llang_id, $lref_id, $translation, true, true );
        }
    }

    function custom_field( $mod )
    {
        $param = array(
            array( 
                'group' => 'Section One',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'     => 'section_1_repeater',
                        'name'   => 'section_1_repeater',
                        'type'   => 'repeater',
                        'option' => array(
                            'title'       => 'Slide',
                            'add_text'    => 'Add Slide',
                            'remove_text' => 'Delete Slide',
                        ),
                        'items'  => array(
                            array(
                                'id'     => 'slide_image',
                                'name'   => 'slide_image',
                                'label'  => 'Image',
                                'type'   => 'image',
                                'desc'   => 'Recommended size 1920 * 768',
                                'btntxt' => 'Choose Image',
                                'attributes' => array(
                                    'autocomplete'  => 'off'
                                )
                            ),                           
                            array(
                                'id'     => 'slide_video',
                                'name'   => 'slide_video',
                                'label'  => 'Video',
                                'type'   => 'file',
                                'btntxt' => 'Choose Video',
                                'attributes' => array(
                                    'autocomplete'  => 'off',
                                    'data-filetype' => 'video'
                                )
                            ),
                            array(
                                'id'    => 'big_title',
                                'name'  => 'big_title',
                                'label' => 'Big Title',
                                'desc'  => 'Big text for slide image',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'middle_title',
                                'name'  => 'middle_title',
                                'label' => 'Middle Title',
                                'desc'  => 'Middle text for slide image',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'bottom_title',
                                'name'  => 'bottom_title',
                                'label' => 'Bottom Title',
                                'desc'  => 'Bottom text for slide image',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'subtitle',
                                'name'  => 'subtitle',
                                'label' => 'Subtitle',
                                'desc'  => 'Sub heading text for slide image',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            )
                        )
                    )
                )
            ),
            array( 
                'group' => 'Section Two',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_2_big_title',
                        'name'  => 'section_2_big_title',
                        'label' => 'Section Big Title',
                        'desc'  => 'Big text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_2_heading',
                        'name'  => 'section_2_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'     => 'section_2_repeater',
                        'name'   => 'section_2_repeater',
                        'type'   => 'repeater',
                        'option' => array(
                            'title'       => 'Column',
                            'add_text'    => 'Add Column',
                            'remove_text' => 'Delete Column',
                        ),
                        'items'  => array(
                            array(
                                'id'    => 'title',
                                'name'  => 'title',
                                'label' => 'Title',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'description',
                                'name'  => 'description',
                                'label' => 'Description',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            )
                        )
                    )
                )
            ),
            array( 
                'group' => 'Section Three',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_3_heading',
                        'name'  => 'section_3_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_3_short_description',
                        'name'  => 'section_3_short_description',
                        'label' => 'Short Description',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'rows' => 7
                        )
                    )
                )
            ),
            array( 
                'group' => 'Section Four',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_4_heading',
                        'name'  => 'section_4_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_4_short_description',
                        'name'  => 'section_4_short_description',
                        'label' => 'Short Description',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'rows' => 7
                        )
                    )
                )
            ),
            array( 
                'group' => 'Section Five',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_5_big_title',
                        'name'  => 'section_5_big_title',
                        'label' => 'Big Title',
                        'desc'  => 'Big text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_5_middle_title',
                        'name'  => 'section_5_middle_title',
                        'label' => 'Middle Title',
                        'desc'  => 'Middle text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_5_bottom_title',
                        'name'  => 'section_5_bottom_title',
                        'label' => 'Bottom Title',
                        'desc'  => 'Bottom text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                          
                    array(
                        'id'     => 'section_5_bg_image',
                        'name'   => 'section_5_bg_image',
                        'label'  => 'Background Image',
                        'type'   => 'image',
                        'btntxt' => 'Choose Image',
                        'desc'   => 'Recommended size 1920 * 768',
                        'attributes' => array(
                            'autocomplete'  => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_5_link',
                        'name'  => 'section_5_link',
                        'label' => 'Link',
                        'desc'  => 'ex: http://somerandomlink.com',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                )
            ),
            array( 
                'group' => 'Section Six',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_6_heading',
                        'name'  => 'section_6_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_6_num_of_slide',
                        'name'  => 'section_6_num_of_slide',
                        'label' => 'Number of Slide',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off',

                        )
                    ),
                )
            ),
            array( 
                'group' => 'Section Seven',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_7_heading',
                        'name'  => 'section_7_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_7_num_of_slide',
                        'name'  => 'section_7_num_of_slide',
                        'label' => 'Number of Slide',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off',

                        )
                    ),
                )
            ),
            array( 
                'group' => 'Section Eight',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_8_heading',
                        'name'  => 'section_8_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'     => 'section_8_repeater',
                        'name'   => 'section_8_repeater',
                        'type'   => 'repeater',
                        'option' => array(
                            'title'       => 'Review',
                            'add_text'    => 'Add Review',
                            'remove_text' => 'Delete Review',
                        ),
                        'items'  => array(
                            array(
                                'id'    => 'name',
                                'name'  => 'name',
                                'label' => 'Name',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'desc',
                                'name'  => 'desc',
                                'label' => 'Description',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'star',
                                'name'  => 'star',
                                'label' => 'Star',
                                'type'  => 'text',
                                'desc'  => 'Please input value from 1 - 5',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'review',
                                'name'  => 'review',
                                'label' => 'Review',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            ),             
                            array(
                                'id'     => 'avatar',
                                'name'   => 'avatar',
                                'label'  => 'Photos',
                                'type'   => 'image',
                                'btntxt' => 'Choose Photo',
                                'desc'   => 'Recommended size 300 * 300',
                                'attributes' => array(
                                    'autocomplete'  => 'off'
                                )
                            ),
                        )
                    )
                )
            ),
            array( 
                'group' => 'Section Nine',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_9_heading',
                        'name'  => 'section_9_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_9_description',
                        'name'  => 'section_9_description',
                        'label' => 'Description',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off',
                        )
                    )
                )
            ),
            array( 
                'group' => 'Section Eleven',
                'types' => 1, 
                'field' => array(                    
                    array(
                        'id'    => 'section_11_heading',
                        'name'  => 'section_11_heading',
                        'label' => 'Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),     
                    array(
                        'id'    => 'section_11_subtitle',
                        'name'  => 'section_11_subtitle',
                        'label' => 'Subtitle',
                        'desc'  => 'Sub heading text for this section',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),     
                    array(
                        'id'    => 'section_11_brief_text',
                        'name'  => 'section_11_brief_text',
                        'label' => 'Brief Text',
                        'desc'  => 'Brief text for this section',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'autocomplete' => 'off',
                        )
                    ),
                    array(
                        'id'    => 'section_11_btn_text',
                        'name'  => 'section_11_btn_text',
                        'label' => 'Button Text',
                        'desc'  => 'Text on button',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off',
                        )
                    ),    
                    array(
                        'id'    => 'section_11_btn_link',
                        'name'  => 'section_11_btn_link',
                        'label' => 'Button Link',
                        'desc'  => 'ex: http://example.com',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off',
                        )
                    ),   
                    array(
                        'id'    => 'section_11_content',
                        'name'  => 'section_11_content',
                        'label' => 'Content',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'class' => 'tinymce'
                        )
                    ),
                    array(
                        'id'     => 'section_11_bg_image',
                        'name'   => 'section_11_bg_image',
                        'label'  => 'Background Image',
                        'type'   => 'image',
                        'btntxt' => 'Choose Image',
                        'desc'   => 'Recommended size 1920 * 768',
                        'attributes' => array(
                            'autocomplete'  => 'off'
                        )
                    )
                )
            )
        );

        $id = $this->fields[ 'lpost_id' ];
        $lg = $this->fields[ 'llang_id' ];

        $this->actions->add_actions( 'landing_additional_field', $this->global->init_custom_field( $id, $mod, $param, $lg ) );
    }

    function language()
    {
        if( isset( $_GET[ 'lang' ] ) && $_GET[ 'lang' ] != '' )
        {
            $this->lang = $_GET[ 'lang' ];
        }
        else
        {
            $this->lang = $this->global->getSettingValue( 'llanguage' );
        }
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();
        $this->ref     = $this->global->getFields( 'lumonata_post', 'lpost_id', array( 'ltype' => 'landing', 'lref_id' => null ) );

        $s = 'SELECT * FROM lumonata_post AS a WHERE a.ltype = %s AND a.llang_id = %d LIMIT 1';
        $q = parent::prepare_query( $s, 'landing', $this->lang );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $d = parent::fetch_array( $r );

            $fields = array(
                'lcreated_by'   => $d[ 'lcreated_by' ],
                'lusername'     => $d[ 'lusername' ],
                'llang_id'      => $d[ 'llang_id' ],
                'lref_id'       => $d[ 'lref_id' ],
                'lpost_id'      => $d[ 'lpost_id' ],
                'lcreated_date' => $d[ 'lcreated_date' ],
                'ldlu'          => $d[ 'ldlu' ],
                'ltitle'        => $d[ 'ltitle' ],
                'ltype'         => $d[ 'ltype' ],
                'ldescription'  => $d[ 'ldescription' ],
                'lmeta_title'   => $d[ 'lmeta_title' ],
                'lmeta_key'     => $d[ 'lmeta_key' ],
                'lmeta_desc'    => $d[ 'lmeta_desc' ],
                'lsef_url'      => $d[ 'lsef_url' ],
                'lorder_id'     => $d[ 'lorder_id' ],
                'lparent_id'    => $d[ 'lparent_id' ],
                'lstatus'       => $d[ 'lstatus' ]
            );

            $this->exist = true;
        }
        else
        {
            $fields = array(
                'lcreated_by'   => $this->sess[ 'username' ],
                'lusername'     => $this->sess[ 'username' ],
                'llang_id'      => $this->lang,
                'lref_id'       => $this->ref,
                'lpost_id'      => time(),
                'lcreated_date' => time(),
                'ldlu'          => time(),
                'ltitle'        => 'Homepage',
                'ltype'         => 'landing',
                'ldescription'  => '',
                'lmeta_title'   => '',
                'lmeta_key'     => '',
                'lmeta_desc'    => '',
                'lsef_url'      => '',
                'lorder_id'     => 0,
                'lparent_id'    => 0,
                'lstatus'       => 1
            );

            $this->exist = false;
        }

        if( empty( $_POST ) )
        {
            if( empty( $this->notif ) === false && isset( $this->notif[ 'post' ] ) )
            {
                $this->fields = array_merge( $fields, $this->notif[ 'post' ] );
            }
            else
            {
                if( isset( $fields ) )
                {
                    $this->fields = $fields;   
                }
            }

            $this->post = array();
        }
        else
        {
            if( isset( $_POST[ 'fields' ] ) )
            {
                $this->fields = array_merge( $fields, $_POST[ 'fields' ] );
            }
            else
            {
                $this->fields = $fields;
            }

            $this->post = array_diff_key( $_POST, array_flip( array( 'fields' ) ) );
        }
    }

    function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'order_file' )
            {
                echo $this->order_file();
            }

            if( $this->post[ 'pKEY' ] == 'delete_file' )
            {
                echo $this->delete_file();
            }

            if( $this->post[ 'pKEY' ] == 'upload_file' )
            {
                echo $this->upload_file( $mod );
            }

            if( $this->post[ 'pKEY' ] == 'add_group' )
            {
                echo $this->add_group();
            }
        }

        exit;
    }
}

?> 