<?php

class pages extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->upload   = new upload();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }

    function view( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $this->global->getPViewBothP( $mod, $usertype ) )
        {
            $this->template->set_var( 'mod', $mod );
            $this->template->set_var( 'lang', $this->lang );
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'limit', $this->global->getSettingValue( 'list_admin' ) );

            $this->template->set_var( 'lstatus', $this->get_status_option() );
            $this->template->set_var( 'lparent_id', $this->get_parent_option() );
            $this->template->set_var( 'lcreated_date', $this->get_date_option() );
            $this->template->set_var( 'llang_id', $this->get_language_option( $mod ) );
            
            $this->template->set_var( 'add_url', $this->global->getAddUrl( $mod, $usertype ) );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Pages/pages/view.js' ) );

            $this->template->Parse( 'vC', 'viewContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function insert( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->global->getPInsertBoth( $mod, $usertype ) )
        {
            //-- INIT custom field
            $this->custom_field( $mod );

            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->create( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'lmodule', $mod );
            $this->template->set_var( 'ltype', $ltype );
            $this->template->set_var( 'ltitle', $ltitle );
            $this->template->set_var( 'lsef_url', $lsef_url );
            $this->template->set_var( 'lpost_id', $lpost_id );
            $this->template->set_var( 'llang_id', $llang_id );
            $this->template->set_var( 'lorder_id', $lorder_id );
            $this->template->set_var( 'lcreated_by', $lcreated_by );
            $this->template->set_var( 'ldescription', $ldescription );
            $this->template->set_var( 'switcher_lang', $this->switcher_language( $mod, $llang_id, $this->ref ) );

            $this->template->set_var( 'lmeta_title', $lmeta_title );
            $this->template->set_var( 'lmeta_desc', $lmeta_desc );
            $this->template->set_var( 'lmeta_key', $lmeta_key );
            
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'lparent_id', $this->get_parent_option( $lparent_id ) );
            
            $this->template->set_var( 'lcustom', $this->actions->attemp_actions( 'pages_custom_field' ) );
            $this->template->set_var( 'ladditional', $this->actions->attemp_actions( 'pages_additional_field' ) );

            $this->template->set_var( 'site_url', SITE_URL );
            $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
            $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );

            $this->template->set_var( 'add_url', $this->global->getAddUrl( $mod, $usertype ) );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/blueimp-file-upload@10.32.0/js/jquery.fileupload.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/tinymce@5.10.2/tinymce.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( '//' . ADMIN_THEME_URL . '/assets/dropbox.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Pages/pages/form.js' ) );

            $this->template->Parse( 'fC', 'formContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function edit( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->valid )
        {
            if( $this->global->getPEditBoth( $mod, $usertype ) )
            {
                //-- INIT custom field
                $this->custom_field( $mod );

                //-- EXECUTE update action
                if( isset( $this->post[ 'save' ] ) )
                {
                    $this->change( $mod );
                }

                //-- EXTRACT fields
                extract( $this->fields );

                $this->template->set_var( 'lmodule', $mod );
                $this->template->set_var( 'ltype', $ltype );
                $this->template->set_var( 'ltitle', $ltitle );
                $this->template->set_var( 'lsef_url', $lsef_url );
                $this->template->set_var( 'lpost_id', $lpost_id );
                $this->template->set_var( 'llang_id', $llang_id );
                $this->template->set_var( 'lorder_id', $lorder_id );
                $this->template->set_var( 'lcreated_by', $lcreated_by );
                $this->template->set_var( 'ldescription', $ldescription );
                $this->template->set_var( 'switcher_lang', $this->switcher_language( $mod, $llang_id, $this->ref ) );

                $this->template->set_var( 'lmeta_title', $lmeta_title );
                $this->template->set_var( 'lmeta_desc', $lmeta_desc );
                $this->template->set_var( 'lmeta_key', $lmeta_key );

                $this->template->set_var( 'message', $this->message );
                $this->template->set_var( 'lparent_id', $this->get_parent_option( $lparent_id ) );
            
                $this->template->set_var( 'lcustom', $this->actions->attemp_actions( 'pages_custom_field' ) );
                $this->template->set_var( 'ladditional', $this->actions->attemp_actions( 'pages_additional_field' ) );

                $this->template->set_var( 'site_url', SITE_URL );
                $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
                $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );

                $this->template->set_var( 'add_url', $this->global->getAddUrl( $mod, $usertype ) );
                $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/blueimp-file-upload@10.32.0/js/jquery.fileupload.min.js' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/tinymce@5.10.2/tinymce.min.js' ) );

                $this->actions->add_actions( 'css', $this->global->get_css( '//' . ADMIN_THEME_URL . '/assets/dropbox.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Pages/pages/form.js' ) );

                $this->template->Parse( 'fC', 'formContent', true );
            }
            else
            {
                $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
            }
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NOT_FOUND ) );
        }
        
        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function load( $mod, $usertype )
    {
        $cols  = array(
            1 => 'a.ltitle', 
            2 => 'a.ltype',
            3 => 'a.lstatus'
        );

        //-- Query Limit
        $limit = parent::prepare_query( ' LIMIT ' . $this->post[ 'start' ] . ', ' . $this->post[ 'length' ] );
    
        //-- Query Order By
        if( empty( $this->post[ 'order' ] ) )
        {
            $order = parent::prepare_query( ' ORDER BY a.llang_id, a.lorder_id ASC' );
        }
        else
        {
            $order = array();

            foreach( $this->post[ 'order' ] as $i => $od )
            {
                if( isset( $cols[ $od[ 'column' ] ] ) )
                {
                    $order[] = $cols[ $od[ 'column' ] ] . ' ' . $od[ 'dir' ];
                }
            }

            if( empty( $order ) )
            {
                $order = parent::prepare_query( ' ORDER BY a.llang_id, a.lorder_id ASC' );
            }
            else
            {
                $order = parent::prepare_query( ' ORDER BY ' . implode( ', ', $order ) );
            }
        }

        //-- Query Additional Where
        $w = array();

        $w[] = parent::prepare_query( 'a.ltype = %s', 'pages' );

        if( $this->post[ 'lcreated_date' ] != '' )
        {
            list( $month, $year ) = explode( ', ', $this->post[ 'lcreated_date' ] );

            $w[] = parent::prepare_query( 'MONTHNAME( FROM_UNIXTIME( a.lcreated_date ) ) = %s AND YEAR( FROM_UNIXTIME( a.lcreated_date ) ) = %s', $month, $year );
        }

        if( $this->post[ 'lparent_id' ] != '' )
        {
            $w[] = parent::prepare_query( 'a.lparent_id = %d', $this->post[ 'lparent_id' ] );
        }

        if( $this->post[ 'lstatus' ] != '' )
        {
            $w[] = parent::prepare_query( 'a.lstatus = %d', $this->post[ 'lstatus' ] );
        }

        if( $this->post[ 'llang_id' ] != '' )
        {
            $w[] = parent::prepare_query( 'a.llang_id = %d', $this->post[ 'llang_id' ] );
        }

        if( $this->post[ 'lsearch' ] != '' )
        {
            $s = array();

            foreach( $cols as $col )
            {
                $s[] = parent::prepare_query( $col . ' LIKE %s', '%' . $this->post[ 'lsearch' ] . '%' );
            }

            $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
        }

        if( empty( $w ) === false )
        {
            $where = ' WHERE ' . implode( ' AND ', $w );
        }
        else
        {
            $where = '';
        }

        //-- Main Query
        $qm = 'SELECT 
                a.ltitle,
                a.lstatus,
                a.lpost_id,
                a.llang_id,
                a.lcreated_date
               FROM lumonata_post AS a'. $where . $order;
        $rm = parent::query( $qm );
        $nm = parent::num_rows( $rm );

        $qf = $qm . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        $data = array();

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
                $data[] = array(
                    'leditlink'     => $this->global->getEditUrl( $mod, $usertype, $df[ 'lpost_id' ] ),
                    'lflag'         => $this->global->get_language_flag( $df[ 'llang_id' ] ),
                    'ltitle'        => ucwords( strtolower( $df[ 'ltitle' ] ) ),
                    'lcreated_date' => date( 'd F, Y', $df[ 'lcreated_date' ] ),
                    'lpost_id'      => $df[ 'lpost_id' ],
                    'lstatus'       => $df[ 'lstatus' ]
                );
            }
        }

        return json_encode( array(
            'draw'            => intval( $this->post[ 'draw' ] ),
            'recordsFiltered' => intval( $nm ),
            'recordsTotal'    => intval( $nf ),
            'data'            => $data
        ));
    }

    function validate( $data = array() )
    {
        $error = array();

        foreach( $data as $field => $dt )
        {
            if( $field == 'ltitle' && $dt == '' )
            {
                array_push( $error, 'Title must have value' );
            }
        }

        return $error;
    }

    function create( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- MERGE lsef_url INTO data array
            if( empty( $data[ 'lsef_url' ] ) )
            {
                $data = array_merge( $data, array( 'lsef_url' => $this->sef_url( $data[ 'ltitle' ] ) ) );
            }
            else
            {
                $data = array_merge( $data, array( 'lsef_url' => $this->sef_url( $data[ 'lsef_url' ] ) ) );
            }

            //-- INSERT lumonata_post
            $param  = array_diff_key( $data, array_flip( array( 'lpost_id', 'ladditional' ) ) );
            $result = parent::insert( 'lumonata_post', array_filter( $param ) );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to add new data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

                exit;
            }
            else
            {
                $lapp_id = parent::insert_id();

                //-- SYNC lumonata_attachment
                $param = array( 'lapp_id' => $lapp_id );
                $where = array( 'lapp_id' => $data[ 'lpost_id' ], 'lmodule_id' => $mod );

                parent::update( 'lumonata_attachment', $param, $where );

                //-- SYNC lumonata_additional_field
                if( isset( $data[ 'ladditional' ] ) )
                {
                    foreach( $data[ 'ladditional' ] as $key => $value )
                    {
                        if( is_array( $value ) )
                        {
                            $value = json_encode( $value );
                        }

                        parent::insert( 'lumonata_additional_field', array(
                            'ladditional_value' => $value,
                            'ladditional_key' => $key,
                            'lapp_id' => $lapp_id,
                            'lmodule_id' => $mod,
                        ));
                    }
                }

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully add new data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $lapp_id );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

            exit;
        }
    }

    function change( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- MERGE lsef_url INTO data array
            if( empty( $data[ 'lsef_url' ] ) )
            {
                $data = array_merge( $data, array( 'lsef_url' => $this->sef_url( $data[ 'ltitle' ] ) ) );
            }

            //-- UPDATE lumonata_post
            $param  = array_diff_key( $data, array_flip( array( 'lpost_id', 'ladditional' ) ) );
            $where  = array( 'lpost_id' => $data[ 'lpost_id' ] );

            $result = parent::update( 'lumonata_post', array_filter( $param ), $where );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to update existing data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

                exit;
            }
            else
            {
                //-- SYNC lumonata_additional_field
                if( isset( $data[ 'ladditional' ] ) )
                {
                    foreach( $data[ 'ladditional' ] as $key => $value )
                    {
                        if( is_array( $value ) )
                        {
                            $value = json_encode( $value );
                        }

                        //-- Check Additonal Field Exist or Not
                        $aid = $this->global->getValueField3( 'lumonata_additional_field', 'ladditional_id', 'lapp_id', $data[ 'lpost_id' ], 'ladditional_key', $key, 'lmodule_id', $mod );

                        if( empty( $aid ) )
                        {
                            parent::insert( 'lumonata_additional_field', array(
                                'lapp_id' => $data[ 'lpost_id' ],
                                'ladditional_value' => $value,
                                'ladditional_key' => $key,
                                'lmodule_id' => $mod,
                            ));
                        }
                        else
                        {
                            $param = array( 'ladditional_value' => $value );
                            $where = array( 'ladditional_id' => $aid );

                            parent::update( 'lumonata_additional_field', $param, $where );
                        }
                    }
                }

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully updated existing data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?' . http_build_query( $_GET ) );

            exit;
        }
    }

    function do_delete( $mod )
    {
        parent::begin();

        $commit = 1;

        $s = 'DELETE FROM lumonata_post WHERE lpost_id = %s';
        $q = parent::prepare_query( $s, $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            $commit = 0;
        }
        else
        {
            //-- DELETE attachment
            $s = 'SELECT * FROM lumonata_attachment AS a WHERE a.lapp_id = %d AND a.lmodule_id = %d';
            $q = parent::prepare_query( $s, $this->post[ 'id' ], $mod );
            $r = parent::query( $q );

            while( $d = parent::fetch_array( $r ) )
            {
                $r2 = parent::delete( 'lumonata_attachment', array( 'lattach_id' => $d[ 'lattach_id' ] ) );

                if( is_array( $r2 ) )
                {
                    $commit = 0;
                }
                else
                {
                    if( empty( $d[ 'lattach' ] ) === false )
                    {
                        $this->upload->upload_constructor( IMAGE_DIR . '/Uploads/' );
                        $this->upload->delete_file_thumb( $d[ 'lattach' ] );
                    }
                }
            }

            //-- DELETE additional field
            $r3 = parent::delete( 'lumonata_additional_field', array( 'lapp_id' => $this->post[ 'id' ], 'lmodule_id' => $mod ) );

            if( is_array( $r3 ) )
            {
                $commit = 0;
            }
        }
                
        if( $commit == 0 )
        {
            parent::rollback();

            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            parent::commit();

            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function do_change_status()
    {
        $s = 'UPDATE lumonata_post AS a SET a.lstatus = %d WHERE a.lpost_id = %s';
        $q = parent::prepare_query( $s, $this->post[ 'status' ], $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function upload_file( $mod )
    {
        if( isset( $_FILES[ 'attachment' ] ) && $_FILES[ 'attachment' ][ 'error' ] == 0 )
        {
            global $db;

            extract( $this->post );

            $g = new globalAdmin();
            $u = new upload();

            $file_name = $_FILES[ 'attachment' ][ 'name' ];
            $file_size = $_FILES[ 'attachment' ][ 'size' ];
            $file_type = $_FILES[ 'attachment' ][ 'type' ];
            $file_tmp  = $_FILES[ 'attachment' ][ 'tmp_name' ];

            $sef_img  = $this->upload->file_name_filter( $file_name ) . '-' . time();
            $file     = $this->upload->rename_file( $file_name, $sef_img );

            $this->upload->upload_constructor( IMAGE_DIR . '/Uploads/' );

            if( $this->post[ 'file_type' ] == 'images' )
            {
                if( in_array( $file_type, array( 'image/jpg', 'image/jpeg', 'image/pjpeg', 'image/gif', 'image/png' ) ) )
                {
                    $this->upload->upload_resize( $file_name, $sef_img, $file_tmp, $file_type, 940, 940, 1 );
                }
                else
                {
                    return json_encode( array( 'result' => 'not-supported' ) );
                }
            }
            elseif( $this->post[ 'file_type' ] == 'word' && !in_array( $file_type, array( 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ) ) )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }
            elseif( $this->post[ 'file_type' ] == 'pdf' && $file_type != 'application/pdf' )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }
            elseif( $this->post[ 'file_type' ] == 'svg' && $file_type != 'image/svg+xml' )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }

            if( $this->upload->upload_file( $file_name, $sef_img, $file_tmp, 0 ) )
            {
                $order_id = $this->global->setCode( 'lumonata_attachment', 'lorder_id' );

                $r = $db->insert( 'lumonata_attachment', array(
                    'lcreated_by'   => $this->sess[ 'username' ],
                    'lusername'     => $this->sess[ 'username' ],
                    'llang_id'      => $this->lang,
                    'lmimetype'     => $file_type,
                    'lorder_id'     => $order_id,
                    'lsef_url'      => $sef_img,
                    'lapp_id'       => $app_id,
                    'ltitle'        => $title,
                    'lattach'       => $file,
                    'lmodule_id'    => $mod,
                    'lcreated_date' => time(),
                    'ldlu'          => time(),
                    'lstatus'       => 2
                ));

                if( is_array( $r ) )
                {
                    $this->upload->delete_file_thumb( $file );

                    return json_encode( array( 'result' => 'failed' ) );
                }
                else
                {
                    $id = $db->insert_id();

                    if( $file_type == 'image/jpg' || $file_type == 'image/jpeg' || $file_type == 'image/pjpeg' || $file_type == 'image/gif' || $file_type == 'image/png' )
                    {
                        //-- If file type is an image show the thumbnail
                        $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=180&h=180&src=' . HT_SERVER . IMAGE_URL . '/Uploads/' . $file;
                    }
                    else
                    {
                        if( in_array( $file_type, array( 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ) ) )
                        {
                            //-- Word file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-doc.png';
                        }
                        elseif( $file_type == 'application/pdf' )
                        {
                            //-- PDF file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-pdf.png';
                        }
                        elseif( $file_type == 'image/svg+xml' )
                        {
                            //-- SVG file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-svg.png';
                        }
                        else
                        {
                            //-- Other file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-file.png';
                        }
                    }

                    return json_encode( array( 'result' => 'success', 'src' => $src, 'id' => $id ) );
                }
            }
            else
            {
                $this->upload->delete_thumb( $file );

                return json_encode( array( 'result' => 'failed' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'error' ) );
        }
    }

    function delete_file()
    {
        if( isset( $this->post[ 'lattach_id' ] ) && !empty( $this->post[ 'lattach_id' ] ) )
        {
            $image  = $this->global->getValueField( 'lumonata_attachment', 'lattach', 'lattach_id', $this->post[ 'lattach_id' ] );

            $result = parent::delete( 'lumonata_attachment', array( 'lattach_id' => $this->post[ 'lattach_id' ] ) );

            if( is_array( $result ) )
            {
                return json_encode( array( 'result' => 'error' ) );
            }
            else
            {
                if( empty( $image ) === false )
                {
                    $this->upload->upload_constructor( IMAGE_DIR . '/Uploads/' );
                    $this->upload->delete_file_thumb( $image );
                }

                return json_encode( array( 'result' => 'success' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
    }

    function order_file()
    {
        if( isset( $this->post[ 'image' ] ) && !empty( $this->post[ 'image' ] ) )
        {
            $error = 0;

            foreach( $this->post[ 'image' ] as $order_id => $attach_id )
            {
                $param  = array( 'lorder_id' => $order_id );
                $where  = array( 'lattach_id' => $attach_id );

                $result = parent::update( 'lumonata_attachment', $param, $where );

                if( is_array( $result ) )
                {
                    $error++;
                }
            }

            if( empty( $error ) == false )
            {
                return json_encode( array( 'result' => 'error', 'message' => 'Failed to reorder some image' ) );
            }
            else
            {
                return json_encode( array( 'result' => 'success' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'error', 'message' => 'No images was found to reorder' ) );
        }
    }

    function add_group()
    {
        if( isset( $this->post[ 'items' ] ) && !empty( $this->post[ 'items' ] ) )
        {
            $items  = json_decode( base64_decode( $this->post[ 'items' ] ), true );
            $object = json_decode( base64_decode( $this->post[ 'object' ] ), true );

            return $this->global->add_repeater_items( $items, $object );
        }
    }

    function sef_url( $string = '' )
    {
        $num = $this->global->getNumRows( 'lumonata_post', 'ltitle', $string );

        if( $num > 0 )
        {
            for( $i = 2; $i <= $num + 1; $i++ )
            {
                $sef = $this->global->sef_url( $string ) . '-' . $i;
                
                if( $this->global->getNumRows( 'lumonata_post', 'lsef_url', $sef ) < 1 )
                {
                    $sef = $sef;

                    break;
                }
            }
        }
        else
        {
            $sef = $this->global->sef_url( $string );
        }
        
        return $sef;
    }

    function switcher_language( $mod, $llang_id, $lref_id )
    {
        $s = 'SELECT a.llang_id FROM lumonata_post AS a WHERE a.lref_id IS NOT NULL AND a.lref_id = %d ORDER BY a.lorder_id';
        $q = parent::prepare_query( $s, $lref_id );
        $r = parent::query( $q );

        $translation = array();

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_assoc( $r ) )
            {
                array_push( $translation, $d[ 'llang_id' ] );
            }
        }

        if( empty( $lref_id ) )
        {
            return $this->global->get_switcher_language( $mod, 'lumonata_post', $llang_id, $lref_id, $translation, false );
        }
        else
        {
            return $this->global->get_switcher_language( $mod, 'lumonata_post', $llang_id, $lref_id, $translation, false, true );
        }
    }

    function custom_field( $mod )
    {
        //-- Custom Field
        $cparam = array(
            'types' => 1,
            'field' => array(
                array(
                    'id'      => 'page_template',
                    'name'    => 'page_template',
                    'label'   => 'Page Template',
                    'type'    => 'select',
                    'options' => array(
                        0 => 'Default',
                        1 => 'About Us',
                        2 => 'Term & Condition',
                        3 => 'Become Nomad Member',
                        4 => 'Nomad Loyalty Program',
                        5 => 'Become a Digital Nomad',
                        6 => 'Nomad Franchises',
                        7 => 'Contact Us',
                        8 => 'FAQ',
                        9 => 'Register',
                    ),
                    'attributes' => array(
                        'class' => 'chzn-select',
                        'autocomplete' => 'off',
                        'data-placeholder' => 'Choose page template'
                    )
                ),
                array(
                    'id'     => 'header_image',
                    'name'   => 'header_image',
                    'label'  => 'Header Image',
                    'type'   => 'image',
                    'desc'   => 'Recommended size 300 * 300',
                    'btntxt' => 'Choose Image',
                    'attributes' => array(
                        'autocomplete'  => 'off'
                    )
                ),
                array(
                    'id'    => 'page_brief_text',
                    'name'  => 'page_brief_text',
                    'label' => 'Brief Text',
                    'type'  => 'textarea',
                    'attributes' => array(
                        'autocomplete' => 'off'
                    )
                )
            )
        );

        //-- About Us Additional Field
        $aparam = array(
            array( 
                'group' => 'Section One',
                'types' => 1,
                'field' => array(
                    array(
                        'id'      => 'section_1_1_repeater',
                        'name'    => 'section_1_1_repeater',
                        'desc'    => 'Show image and textbox content on your page',
                        'type'    => 'repeater',
                        'option' => array(
                            'title'       => 'Item',
                            'add_text'    => 'Add Item',
                            'remove_text' => 'Delete Item',
                        ),
                        'items'  => array(                            
                            array(
                                'id'    => 'box_title',
                                'name'  => 'box_title',
                                'label' => 'Title',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'box_content',
                                'name'  => 'box_content',
                                'label' => 'Content',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'class' => 'textarea tinymce',
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            ),             
                            array(
                                'id'     => 'box_image',
                                'name'   => 'box_image',
                                'label'  => 'Image',
                                'type'   => 'image',
                                'btntxt' => 'Choose Image',
                                'desc'   => 'Recommended size 600 * 400',
                                'attributes' => array(
                                    'autocomplete'  => 'off'
                                )
                            ),
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 1 
                )
            ),
            array( 
                'group' => 'Section Two',
                'types' => 1,
                'field' => array(
                    array(
                        'id'    => 'section_2_1_heading',
                        'name'  => 'section_2_1_heading',
                        'label' => 'Heading Title',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'      => 'section_2_1_repeater',
                        'name'    => 'section_2_1_repeater',
                        'desc'    => 'Show title and textbox content on your page',
                        'type'    => 'repeater',
                        'option' => array(
                            'title'       => 'Item',
                            'add_text'    => 'Add Item',
                            'remove_text' => 'Delete Item',
                        ),
                        'items'  => array(                            
                            array(
                                'id'    => 'box_title',
                                'name'  => 'box_title',
                                'label' => 'Title',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'box_content',
                                'name'  => 'box_content',
                                'label' => 'Content',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'class' => 'textarea',
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            ),
                        )
                    ),
                    array(
                        'id'    => 'section_2_1_short_desc',
                        'name'  => 'section_2_1_short_desc',
                        'label' => 'Short Description',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 1 
                )
            ),
            array( 
                'group' => 'Section Three',
                'types' => 1,
                'field' => array(
                    array(
                        'id'    => 'section_3_1_heading',
                        'name'  => 'section_3_1_heading',
                        'label' => 'Heading Title',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'     => 'section_3_1_gallery',
                        'name'   => 'section_3_1_gallery',
                        'label'  => 'Slide Gallery',
                        'type'   => 'gallery',
                        'desc'   => 'Recommended size height = 300px'
                    ),
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 1 
                )
            ),
            array( 
                'group' => 'Section Four',
                'types' => 1,
                'field' => array(
                    array(
                        'id'    => 'section_4_1_heading',
                        'name'  => 'section_4_1_heading',
                        'label' => 'Heading Title',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_4_1_content',
                        'name'  => 'section_4_1_content',
                        'label' => 'Content',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    ),
                    array(
                        'id'    => 'section_4_1_btn_text',
                        'name'  => 'section_4_1_btn_text',
                        'label' => 'Text Button',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_4_1_btn_link',
                        'name'  => 'section_4_1_btn_link',
                        'label' => 'Link Button',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 1 
                )
            )
        );

        //-- T & C Additional Field
        $bparam = array(
            array( 
                'group' => 'Tab Content',
                'types' => 1,
                'field' => array(
                    array(
                        'id'      => 'section_1_2_repeater',
                        'name'    => 'section_1_2_repeater',
                        'type'    => 'repeater',
                        'option' => array(
                            'title'       => 'Tab',
                            'add_text'    => 'Add Tab',
                            'remove_text' => 'Delete Tab',
                        ),
                        'items'  => array(
                            array(
                                'id'    => 'tab_title',
                                'name'  => 'tab_title',
                                'label' => 'Title',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'tab_content',
                                'name'  => 'tab_content',
                                'label' => 'Content',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'class' => 'textarea tinymce',
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            ),
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 2 
                )
            )
        );

        //-- Become Nomad Member Additional Field
        $dparam = array(
            array( 
                'group' => 'Section One',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_1_3_big_title',
                        'name'  => 'section_1_3_big_title',
                        'label' => 'Section Big Title',
                        'desc'  => 'Big text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_1_3_heading',
                        'name'  => 'section_1_3_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'     => 'section_1_3_repeater',
                        'name'   => 'section_1_3_repeater',
                        'type'   => 'repeater',
                        'option' => array(
                            'title'       => 'Column',
                            'add_text'    => 'Add Column',
                            'remove_text' => 'Delete Column',
                        ),
                        'items'  => array(
                            array(
                                'id'    => 'title',
                                'name'  => 'title',
                                'label' => 'Title',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'description',
                                'name'  => 'description',
                                'label' => 'Description',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            )
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 3 
                )
            ),           
            array( 
                'group' => 'Section Two',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_2_3_heading',
                        'name'  => 'section_2_3_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'     => 'section_2_3_repeater',
                        'name'   => 'section_2_3_repeater',
                        'type'   => 'repeater',
                        'option' => array(
                            'title'       => 'Item',
                            'add_text'    => 'Add Item',
                            'remove_text' => 'Delete Item',
                        ),
                        'items'  => array(
                            array(
                                'id'    => 'title',
                                'name'  => 'title',
                                'label' => 'Title',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'subtitle',
                                'name'  => 'subtitle',
                                'label' => 'Subtitle',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'description',
                                'name'  => 'description',
                                'label' => 'Description',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            ),
                            array(
                                'id'     => 'image',
                                'name'   => 'image',
                                'label'  => 'Image',
                                'type'   => 'image',
                                'desc'   => 'Recommended size 200 * 200',
                                'btntxt' => 'Choose Image',
                                'attributes' => array(
                                    'autocomplete'  => 'off'
                                )
                            )
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 3 
                )
            ),
            array( 
                'group' => 'Section Three',
                'types' => 1,
                'field' => array(
                    array(
                        'id'    => 'section_3_3_heading',
                        'name'  => 'section_3_3_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_3_3_content',
                        'name'  => 'section_3_3_content',
                        'label' => 'Content',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 3 
                )
            ),
            array( 
                'group' => 'Section Four',
                'types' => 1,
                'field' => array(
                    array(
                        'id'    => 'section_4_3_heading',
                        'name'  => 'section_4_3_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_4_3_content',
                        'name'  => 'section_4_3_content',
                        'label' => 'Content',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    ),
                    array(
                        'id'    => 'section_4_3_btn_text',
                        'name'  => 'section_4_3_btn_text',
                        'label' => 'Text Button',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_4_3_btn_link',
                        'name'  => 'section_4_3_btn_link',
                        'label' => 'Link Button',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 3 
                )
            )
        );

        //-- Nomad Loyalty Program Additional Field
        $eparam = array(            
            array( 
                'group' => 'Section One',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'     => 'section_1_4_repeater',
                        'name'   => 'section_1_4_repeater',
                        'type'   => 'repeater',
                        'option' => array(
                            'title'       => 'Column',
                            'add_text'    => 'Add Column',
                            'remove_text' => 'Delete Column',
                        ),
                        'items'  => array(
                            array(
                                'id'    => 'float_title',
                                'name'  => 'float_title',
                                'label' => 'Float Title',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'title',
                                'name'  => 'title',
                                'label' => 'Title',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'description',
                                'name'  => 'description',
                                'label' => 'Description',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            ),
                            array(
                                'id'     => 'image',
                                'name'   => 'image',
                                'label'  => 'Image',
                                'type'   => 'image',
                                'desc'   => 'Recommended size 400 * 250',
                                'btntxt' => 'Choose Image',
                                'attributes' => array(
                                    'autocomplete'  => 'off'
                                )
                            )
                        )
                    ),
                    array(
                        'id'    => 'section_1_4_more_title',
                        'name'  => 'section_1_4_more_title',
                        'label' => 'More Title',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_1_4_more_desc',
                        'name'  => 'section_1_4_more_desc',
                        'label' => 'More Short Description',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    ),
                    array(
                        'id'    => 'section_1_4_more_btn_text',
                        'name'  => 'section_1_4_more_btn_text',
                        'label' => 'More Button Text',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_1_4_more_btn_link',
                        'name'  => 'section_1_4_more_btn_link',
                        'label' => 'More Button Link',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 4 
                )
            ),           
            array( 
                'group' => 'Section Two',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_2_4_heading',
                        'name'  => 'section_2_4_heading',
                        'label' => 'Section Title',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_2_4_content',
                        'name'  => 'section_2_4_content',
                        'label' => 'Content',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    ),
                    array(
                        'id'    => 'section_2_4_email',
                        'name'  => 'section_2_4_email',
                        'label' => 'Email To',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'type' => 'email'
                        )
                    ),
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 4 
                )
            )
        );

        //-- Become a Digital Nomad Additional Field
        $fparam = array(
            array( 
                'group' => 'Section One',
                'types' => 1,
                'field' => array(
                    array(
                        'id'    => 'section_1_5_heading',
                        'name'  => 'section_1_5_heading',
                        'label' => 'Section Title',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'      => 'section_1_5_repeater',
                        'name'    => 'section_1_5_repeater',
                        'desc'    => 'Show image and textbox content on your page',
                        'type'    => 'repeater',
                        'option' => array(
                            'title'       => 'Item',
                            'add_text'    => 'Add Item',
                            'remove_text' => 'Delete Item',
                        ),
                        'items'  => array(
                            array(
                                'id'    => 'box_content',
                                'name'  => 'box_content',
                                'label' => 'Content',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'class' => 'textarea tinymce',
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            ),             
                            array(
                                'id'     => 'box_image',
                                'name'   => 'box_image',
                                'label'  => 'Image',
                                'type'   => 'image',
                                'btntxt' => 'Choose Image',
                                'desc'   => 'Recommended size 600 * 400',
                                'attributes' => array(
                                    'autocomplete'  => 'off'
                                )
                            ),
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 5 
                )
            ),
            array( 
                'group' => 'Section Two',
                'types' => 1,
                'field' => array(
                    array(
                        'id'    => 'section_2_5_heading',
                        'name'  => 'section_2_5_heading',
                        'label' => 'Section Title',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_2_5_content',
                        'name'  => 'section_2_5_content',
                        'label' => 'Content',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    ),
                    array(
                        'id'    => 'section_2_5_email_to',
                        'name'  => 'section_2_5_email_to',
                        'label' => 'Email To',
                        'desc'  => 'email address that will receive form data',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 5 
                )
            ),
        );

        //-- Franchises
        $gparam = array(
            array( 
                'group' => 'Section One',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_1_6_big_title',
                        'name'  => 'section_1_6_big_title',
                        'label' => 'Section Big Title',
                        'desc'  => 'Big text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_1_6_heading',
                        'name'  => 'section_1_6_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'     => 'section_1_6_repeater',
                        'name'   => 'section_1_6_repeater',
                        'type'   => 'repeater',
                        'option' => array(
                            'title'       => 'Column',
                            'add_text'    => 'Add Column',
                            'remove_text' => 'Delete Column',
                        ),
                        'items'  => array(
                            array(
                                'id'    => 'title',
                                'name'  => 'title',
                                'label' => 'Title',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'description',
                                'name'  => 'description',
                                'label' => 'Description',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            )
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 6 
                )
            ),           
            array( 
                'group' => 'Section Two',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_2_6_heading',
                        'name'  => 'section_2_6_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_2_6_content',
                        'name'  => 'section_2_6_content',
                        'label' => 'Content',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 6 
                )
            ),
            array( 
                'group' => 'Section Three',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_3_6_heading',
                        'name'  => 'section_3_6_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_3_6_content',
                        'name'  => 'section_3_6_content',
                        'label' => 'Content',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 6 
                )
            ),
            array( 
                'group' => 'Section Four',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_4_6_heading',
                        'name'  => 'section_4_6_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_4_6_content',
                        'name'  => 'section_4_6_content',
                        'label' => 'Content',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 6 
                )
            ),
            array( 
                'group' => 'Section Five',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_5_6_content',
                        'name'  => 'section_5_6_content',
                        'label' => 'Content',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 6 
                )
            ),
            array( 
                'group' => 'Section Six',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_6_6_heading',
                        'name'  => 'section_6_6_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_6_6_content',
                        'name'  => 'section_6_6_content',
                        'label' => 'Content',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 6 
                )
            ),
            array( 
                'group' => 'Section Seven',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_7_6_heading',
                        'name'  => 'section_7_6_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'     => 'section_7_6_repeater',
                        'name'   => 'section_7_6_repeater',
                        'type'   => 'repeater',
                        'option' => array(
                            'title'       => 'Slide',
                            'add_text'    => 'Add Slide',
                            'remove_text' => 'Delete Slide',
                        ),
                        'items'  => array(
                            array(
                                'id'    => 'title',
                                'name'  => 'title',
                                'label' => 'Title',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'description',
                                'name'  => 'description',
                                'label' => 'Description',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            ),
                            array(
                                'id'    => 'quotes',
                                'name'  => 'quotes',
                                'label' => 'Quotes',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            ),
                            array(
                                'id'     => 'avatar',
                                'name'   => 'avatar',
                                'label'  => 'Avatar',
                                'type'   => 'image',
                                'desc'   => 'Recommended size 150 * 150',
                                'btntxt' => 'Choose Image',
                                'attributes' => array(
                                    'autocomplete'  => 'off'
                                )
                            )
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 6 
                )
            ),
            array( 
                'group' => 'Section Eight',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_8_6_heading',
                        'name'  => 'section_8_6_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_8_6_content',
                        'name'  => 'section_8_6_content',
                        'label' => 'Content',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    ),
                    array(
                        'id'    => 'section_8_6_email_to',
                        'name'  => 'section_8_6_email_to',
                        'label' => 'Email To',
                        'desc'  => 'email address that will receive form data',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 6 
                )
            ),
        );

        //-- Contact Us
        $hparam = array(
            array( 
                'group' => 'Section One',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_1_7_heading',
                        'name'  => 'section_1_7_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_1_7_content',
                        'name'  => 'section_1_7_content',
                        'label' => 'Content',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    ),
                    array(
                        'id'    => 'section_1_7_btn_text',
                        'name'  => 'section_1_7_btn_text',
                        'label' => 'Button Text',
                        'desc'  => 'text for action form',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_1_7_email_to',
                        'name'  => 'section_1_7_email_to',
                        'label' => 'Email To',
                        'desc'  => 'email address that will receive form data',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 7 
                )
            ),
            array( 
                'group' => 'Section Two',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_2_7_big_title',
                        'name'  => 'section_2_7_big_title',
                        'label' => 'Section Big Title',
                        'desc'  => 'Big text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_2_7_heading',
                        'name'  => 'section_2_7_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'     => 'section_2_7_repeater',
                        'name'   => 'section_2_7_repeater',
                        'type'   => 'repeater',
                        'option' => array(
                            'title'       => 'Column',
                            'add_text'    => 'Add Column',
                            'remove_text' => 'Delete Column',
                        ),
                        'items'  => array(
                            array(
                                'id'    => 'title',
                                'name'  => 'title',
                                'label' => 'Title',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'description',
                                'name'  => 'description',
                                'label' => 'Description',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            )
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 7 
                )
            ),
            array( 
                'group' => 'Section Three',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_3_7_heading',
                        'name'  => 'section_3_7_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_3_7_content',
                        'name'  => 'section_3_7_content',
                        'label' => 'Content',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'     => 'section_3_7_image',
                        'name'   => 'section_3_7_image',
                        'label'  => 'Image',
                        'type'   => 'image',
                        'desc'   => 'Recommended size 600 * 400',
                        'btntxt' => 'Choose Image',
                        'attributes' => array(
                            'autocomplete'  => 'off'
                        )
                    ),
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 7 
                )
            )
        );

        //-- FAQ Additional Field
        $iparam = array(
            array( 
                'group' => 'Tab Content',
                'types' => 1,
                'field' => array(
                    array(
                        'id'      => 'section_1_8_repeater',
                        'name'    => 'section_1_8_repeater',
                        'type'    => 'repeater',
                        'option' => array(
                            'title'       => 'Tab',
                            'add_text'    => 'Add Tab',
                            'remove_text' => 'Delete Tab',
                        ),
                        'items'  => array(
                            array(
                                'id'    => 'tab_title',
                                'name'  => 'tab_title',
                                'label' => 'Title',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'tab_content',
                                'name'  => 'tab_content',
                                'label' => 'Content',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'class' => 'textarea tinymce',
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            ),
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 8 
                )
            )
        );

        //-- Register
        $jparam = array(
            array( 
                'group' => 'Section One',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_1_9_heading',
                        'name'  => 'section_1_9_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_1_9_content',
                        'name'  => 'section_1_9_content',
                        'label' => 'Content',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    ),
                    array(
                        'id'    => 'section_1_9_btn_text',
                        'name'  => 'section_1_9_btn_text',
                        'label' => 'Button Text',
                        'desc'  => 'text for action form',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_1_9_email_to',
                        'name'  => 'section_1_9_email_to',
                        'label' => 'Email To',
                        'desc'  => 'email address that will receive form data',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 9 
                )
            ),
            array( 
                'group' => 'Section Two',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'section_2_9_big_title',
                        'name'  => 'section_2_9_big_title',
                        'label' => 'Section Big Title',
                        'desc'  => 'Big text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'section_2_9_heading',
                        'name'  => 'section_2_9_heading',
                        'label' => 'Section Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'     => 'section_2_9_repeater',
                        'name'   => 'section_2_9_repeater',
                        'type'   => 'repeater',
                        'option' => array(
                            'title'       => 'Column',
                            'add_text'    => 'Add Column',
                            'remove_text' => 'Delete Column',
                        ),
                        'items'  => array(
                            array(
                                'id'    => 'title',
                                'name'  => 'title',
                                'label' => 'Title',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'description',
                                'name'  => 'description',
                                'label' => 'Description',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            )
                        )
                    )
                ),
                'show_on' => array( 
                    'key'   => 'page_template', 
                    'value' => 9 
                )
            )
        );

        $id = $this->fields[ 'lpost_id' ];
        $lg = $this->fields[ 'llang_id' ];

        $this->actions->add_actions( 'pages_custom_field', $this->global->init_custom_field( $id, $mod, $cparam, $lg ) );
        $this->actions->add_actions( 'pages_additional_field', $this->global->init_custom_field( $id, $mod, $aparam, $lg ) );
        $this->actions->add_actions( 'pages_additional_field', $this->global->init_custom_field( $id, $mod, $bparam, $lg ) );
        $this->actions->add_actions( 'pages_additional_field', $this->global->init_custom_field( $id, $mod, $dparam, $lg ) );
        $this->actions->add_actions( 'pages_additional_field', $this->global->init_custom_field( $id, $mod, $eparam, $lg ) );
        $this->actions->add_actions( 'pages_additional_field', $this->global->init_custom_field( $id, $mod, $fparam, $lg ) );
        $this->actions->add_actions( 'pages_additional_field', $this->global->init_custom_field( $id, $mod, $gparam, $lg ) );
        $this->actions->add_actions( 'pages_additional_field', $this->global->init_custom_field( $id, $mod, $hparam, $lg ) );
        $this->actions->add_actions( 'pages_additional_field', $this->global->init_custom_field( $id, $mod, $iparam, $lg ) );
        $this->actions->add_actions( 'pages_additional_field', $this->global->init_custom_field( $id, $mod, $jparam, $lg ) );
    }

    function language()
    {
        if( isset( $_GET[ 'lang' ] ) && $_GET[ 'lang' ] != '' )
        {
            $this->lang = $_GET[ 'lang' ];
        }
        else
        {
            $this->lang = $this->global->getSettingValue( 'llanguage' );
        }
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();
        $this->ref     = null;
        $this->valid   = true;

        if( isset( $_GET[ 'id' ] ) )
        {
            $s = 'SELECT * FROM lumonata_post AS a WHERE a.lpost_id = %d';
            $q = parent::prepare_query( $s, $_GET[ 'id' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $d = parent::fetch_array( $r );

                $fields = array(
                    'lcreated_by'   => $d[ 'lcreated_by' ],
                    'lusername'     => $d[ 'lusername' ],
                    'llang_id'      => $d[ 'llang_id' ],
                    'lref_id'       => $d[ 'lref_id' ],
                    'lpost_id'      => $d[ 'lpost_id' ],
                    'lcreated_date' => $d[ 'lcreated_date' ],
                    'ldlu'          => $d[ 'ldlu' ],
                    'ltype'         => $d[ 'ltype' ],
                    'ltitle'        => $d[ 'ltitle' ],
                    'ldescription'  => $d[ 'ldescription' ],
                    'lmeta_title'   => $d[ 'lmeta_title' ],
                    'lmeta_key'     => $d[ 'lmeta_key' ],
                    'lmeta_desc'    => $d[ 'lmeta_desc' ],
                    'lsef_url'      => $d[ 'lsef_url' ],
                    'lorder_id'     => $d[ 'lorder_id' ],
                    'lparent_id'    => $d[ 'lparent_id' ],
                    'lstatus'       => $d[ 'lstatus' ]
                );

                if( is_null( $d[ 'lref_id' ] ) )
                {
                    $this->ref = $d[ 'lpost_id' ];
                }
                else
                {
                    $this->ref = $d[ 'lref_id' ];
                }
            }
            else
            {
                $this->valid = false;
            }
        }
        else
        {
            if( isset( $_GET[ 'ref' ] ) )
            {
                $this->ref = $_GET[ 'ref' ];
            }

            $fields = array(
                'lcreated_by'   => $this->sess[ 'username' ],
                'lusername'     => $this->sess[ 'username' ],
                'llang_id'      => $this->lang,
                'lref_id'       => $this->ref,
                'lpost_id'      => time(),
                'lcreated_date' => time(),
                'ldlu'          => time(),
                'ltype'         => 'pages',
                'ltitle'        => '',
                'ldescription'  => '',
                'lmeta_title'   => '',
                'lmeta_key'     => '',
                'lmeta_desc'    => '',
                'lsef_url'      => '',
                'lorder_id'     => 0,
                'lparent_id'    => 0,
                'lstatus'       => 1
            );

            if( isset( $_GET[ 'ref' ] ) )
            {
                $s = 'SELECT * FROM lumonata_post AS a WHERE a.lpost_id = %d';
                $q = parent::prepare_query( $s, $_GET[ 'ref' ] );
                $r = parent::query( $q );

                if( parent::num_rows( $r ) > 0 )
                {
                    $d = parent::fetch_assoc( $r );

                    $params = array_diff_key( $d, array_flip( array( 'lref_id', 'llang_id', 'lsef_url' ) ) );
                    $fields = array_merge( $fields, $params );
                }
            }
        }

        if( empty( $_POST ) )
        {
            if( empty( $this->notif ) === false && isset( $this->notif[ 'post' ] ) )
            {
                $this->fields = array_merge( $fields, $this->notif[ 'post' ] );
            }
            else
            {
                if( isset( $fields ) )
                {
                    $this->fields = $fields;   
                }
            }

            $this->post = array();
        }
        else
        {
            if( isset( $_POST[ 'fields' ] ) )
            {
                $this->fields = array_merge( $fields, $_POST[ 'fields' ] );
            }
            else
            {
                $this->fields = $fields;
            }

            $this->post = array_diff_key( $_POST, array_flip( array( 'fields' ) ) );
        }
    }

    function get_date_option( $date = NULL )
    {
        $q = 'SELECT DATE_FORMAT( FROM_UNIXTIME( a.lcreated_date ), "%M, %Y" ) AS postdate FROM lumonata_post AS a GROUP BY YEAR( FROM_UNIXTIME( a.lcreated_date ) ) DESC, MONTH( FROM_UNIXTIME( a.lcreated_date ) ) ASC';

        return $this->global->set_option( $q, 'postdate', 'postdate', $date, false );
    }

    function get_status_option( $lstatus = NULL )
    {
        return $this->global->set_static_option( array( 'Off', 'On' ), $lstatus, false );
    }

    function get_parent_option( $selected_id = NULL )
    {
        $q = 'SELECT a.lpost_id, a.ltitle, a.lparent_id FROM lumonata_post AS a WHERE ltype = "pages"';

        return $this->global->set_recursive_option( $selected_id, $q, 'ltitle', 'lpost_id', 'lparent_id' );
    }

    function get_language_option( $mod )
    {
        return $this->global->set_filter_language_option( $mod, $this->lang );
    }

    function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'load' )
            {
                echo $this->load( $mod, $usertype );
            }

            if( $this->post[ 'pKEY' ] == 'do_change_status' )
            {
                echo $this->do_change_status();
            }

            if( $this->post[ 'pKEY' ] == 'do_delete' )
            {
                echo $this->do_delete( $mod );
            }

            if( $this->post[ 'pKEY' ] == 'order_file' )
            {
                echo $this->order_file();
            }

            if( $this->post[ 'pKEY' ] == 'delete_file' )
            {
                echo $this->delete_file();
            }

            if( $this->post[ 'pKEY' ] == 'upload_file' )
            {
                echo $this->upload_file( $mod );
            }

            if( $this->post[ 'pKEY' ] == 'add_group' )
            {
                echo $this->add_group();
            }
        }

        exit;
    }
}

?> 