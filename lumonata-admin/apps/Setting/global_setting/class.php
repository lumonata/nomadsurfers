<?php

class global_setting extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->upload   = new upload();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }

    function view( $mod, $usertype )
    {
        $this->custom_field( $mod );

        if( $this->exist === false )
        {
            return $this->insert( $mod, $usertype );
        }
        else
        {
            return $this->edit( $mod, $usertype );
        }
    }

    function insert( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->global->getPInsertBoth( $mod, $usertype ) )
        {
            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->create( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'lmodule', $mod );
            $this->template->set_var( 'ltype', $ltype );
            $this->template->set_var( 'ltitle', $ltitle );
            $this->template->set_var( 'lsef_url', $lsef_url );
            $this->template->set_var( 'lpost_id', $lpost_id );
            $this->template->set_var( 'llang_id', $llang_id );
            $this->template->set_var( 'lcreated_by', $lcreated_by );
            $this->template->set_var( 'ldescription', $ldescription );

            $this->template->set_var( 'lmeta_title', $lmeta_title );
            $this->template->set_var( 'lmeta_desc', $lmeta_desc );
            $this->template->set_var( 'lmeta_key', $lmeta_key );
            
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'lcustom', $this->actions->attemp_actions( 'global_setting_custom_field' ) );
            $this->template->set_var( 'ladditional', $this->actions->attemp_actions( 'global_setting_additional_field' ) );

            $this->template->set_var( 'site_url', SITE_URL );
            $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
            $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/blueimp-file-upload@10.32.0/js/jquery.fileupload.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/tinymce@5.10.2/tinymce.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( '//' . ADMIN_THEME_URL . '/assets/dropbox.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Setting/global_setting/form.js' ) );

            $this->template->Parse( 'fC', 'formContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function edit( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->global->getPEditBoth( $mod, $usertype ) )
        {
            //-- EXECUTE update action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->change( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'lmodule', $mod );
            $this->template->set_var( 'ltype', $ltype );
            $this->template->set_var( 'ltitle', $ltitle );
            $this->template->set_var( 'lsef_url', $lsef_url );
            $this->template->set_var( 'lpost_id', $lpost_id );
            $this->template->set_var( 'llang_id', $llang_id );
            $this->template->set_var( 'lcreated_by', $lcreated_by );
            $this->template->set_var( 'ldescription', $ldescription );

            $this->template->set_var( 'lmeta_title', $lmeta_title );
            $this->template->set_var( 'lmeta_desc', $lmeta_desc );
            $this->template->set_var( 'lmeta_key', $lmeta_key );

            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'lcustom', $this->actions->attemp_actions( 'global_setting_custom_field' ) );
            $this->template->set_var( 'ladditional', $this->actions->attemp_actions( 'global_setting_additional_field' ) );

            $this->template->set_var( 'site_url', SITE_URL );
            $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
            $this->template->set_var( 'include_url', ADMIN_INCLUDE_URL );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/blueimp-file-upload@10.32.0/js/jquery.fileupload.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/tinymce@5.10.2/tinymce.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( '//' . ADMIN_THEME_URL . '/assets/dropbox.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Setting/global_setting/form.js' ) );

            $this->template->Parse( 'fC', 'formContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }
        
        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function validate( $data = array() )
    {
        $error = array();

        return $error;
    }

    function create( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- MERGE lsef_url INTO data array
            if( empty( $data[ 'lsef_url' ] ) )
            {
                $data = array_merge( $data, array( 'lsef_url' => $this->sef_url( $data[ 'ltitle' ] ) ) );
            }
            else
            {
                $data = array_merge( $data, array( 'lsef_url' => $this->sef_url( $data[ 'lsef_url' ] ) ) );
            }

            //-- INSERT lumonata_post
            $param  = array_diff_key( $data, array_flip( array( 'lpost_id', 'ladditional' ) ) );
            $result = parent::insert( 'lumonata_post', $param );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to add new data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=new' );

                exit;
            }
            else
            {
                $lapp_id = parent::insert_id();

                //-- SYNC lumonata_attachment
                $param = array( 'lapp_id' => $lapp_id );
                $where = array( 'lapp_id' => $data[ 'lpost_id' ], 'lmodule_id' => $mod );

                parent::update( 'lumonata_attachment', $param, $where );

                //-- SYNC lumonata_additional_field
                if( isset( $data[ 'ladditional' ] ) )
                {
                    foreach( $data[ 'ladditional' ] as $key => $value )
                    {
                        if( is_array( $value ) )
                        {
                            $value = json_encode( $value );
                        }

                        parent::insert( 'lumonata_additional_field', array(
                            'ladditional_value' => $value,
                            'ladditional_key' => $key,
                            'lapp_id' => $lapp_id,
                            'lmodule_id' => $mod,
                        ));
                    }
                }

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully add new data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=view' );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=view' );

            exit;
        }
    }

    function change( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- MERGE lsef_url INTO data array
            if( empty( $data[ 'lsef_url' ] ) )
            {
                $data = array_merge( $data, array( 'lsef_url' => $this->sef_url( $data[ 'ltitle' ] ) ) );
            }

            //-- UPDATE lumonata_post
            $param  = array_diff_key( $data, array_flip( array( 'lpost_id', 'ladditional' ) ) );
            $where  = array( 'lpost_id' => $data[ 'lpost_id' ] );

            $result = parent::update( 'lumonata_post', $param, $where );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to update existing data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=view' );

                exit;
            }
            else
            {
                //-- SYNC lumonata_additional_field
                if( isset( $data[ 'ladditional' ] ) )
                {
                    foreach( $data[ 'ladditional' ] as $key => $value )
                    {
                        if( is_array( $value ) )
                        {
                            $value = json_encode( $value );
                        }

                        //-- Check Additonal Field Exist or Not
                        $aid = $this->global->getFields( 'lumonata_additional_field', 'ladditional_id', array( 'lapp_id' => $data[ 'lpost_id' ], 'ladditional_key' => $key, 'lmodule_id' => $mod ) );

                        if( empty( $aid ) )
                        {
                            parent::insert( 'lumonata_additional_field', array(
                                'lapp_id' => $data[ 'lpost_id' ],
                                'ladditional_value' => $value,
                                'ladditional_key' => $key,
                                'lmodule_id' => $mod,
                            ));
                        }
                        else
                        {
                            $param = array( 'ladditional_value' => $value );
                            $where = array( 'ladditional_id' => $aid );

                            parent::update( 'lumonata_additional_field', $param, $where );
                        }
                    }
                }

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully updated existing data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=view' );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=view' );

            exit;
        }
    }

    function upload_file( $mod )
    {
        if( isset( $_FILES[ 'attachment' ] ) && $_FILES[ 'attachment' ][ 'error' ] == 0 )
        {
            global $db;

            extract( $this->post );

            $g = new globalAdmin();
            $u = new upload();

            $file_name = $_FILES[ 'attachment' ][ 'name' ];
            $file_size = $_FILES[ 'attachment' ][ 'size' ];
            $file_type = $_FILES[ 'attachment' ][ 'type' ];
            $file_tmp  = $_FILES[ 'attachment' ][ 'tmp_name' ];

            $sef_img  = $this->upload->file_name_filter( $file_name ) . '-' . time();
            $file     = $this->upload->rename_file( $file_name, $sef_img );

            $this->upload->upload_constructor( IMAGE_DIR . '/Uploads/' );

            if( $this->post[ 'file_type' ] == 'images' )
            {
                if( in_array( $file_type, array( 'image/jpg', 'image/jpeg', 'image/pjpeg', 'image/gif', 'image/png' ) ) )
                {
                    $this->upload->upload_resize( $file_name, $sef_img, $file_tmp, $file_type, 940, 940, 1 );
                }
                else
                {
                    return json_encode( array( 'result' => 'not-supported' ) );
                }
            }
            elseif( $this->post[ 'file_type' ] == 'word' && !in_array( $file_type, array( 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ) ) )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }
            elseif( $this->post[ 'file_type' ] == 'pdf' && $file_type != 'application/pdf' )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }
            elseif( $this->post[ 'file_type' ] == 'svg' && $file_type != 'image/svg+xml' )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }

            if( $this->upload->upload_file( $file_name, $sef_img, $file_tmp, 0 ) )
            {
                $order_id = $this->global->setCode( 'lumonata_attachment', 'lorder_id' );

                $r = $db->insert( 'lumonata_attachment', array(
                    'lcreated_by'   => $this->sess[ 'username' ],
                    'lusername'     => $this->sess[ 'username' ],
                    'llang_id'      => $this->lang,
                    'lmimetype'     => $file_type,
                    'lorder_id'     => $order_id,
                    'lsef_url'      => $sef_img,
                    'lapp_id'       => $app_id,
                    'ltitle'        => $title,
                    'lattach'       => $file,
                    'lmodule_id'    => $mod,
                    'lcreated_date' => time(),
                    'ldlu'          => time(),
                    'lstatus'       => 2
                ));

                if( is_array( $r ) )
                {
                    $this->upload->delete_file_thumb( $file );

                    return json_encode( array( 'result' => 'failed' ) );
                }
                else
                {
                    $id = $db->insert_id();

                    if( $file_type == 'image/jpg' || $file_type == 'image/jpeg' || $file_type == 'image/pjpeg' || $file_type == 'image/gif' || $file_type == 'image/png' )
                    {
                        //-- If file type is an image show the thumbnail
                        $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=180&h=180&src=' . HT_SERVER . IMAGE_URL . '/Uploads/' . $file;
                    }
                    else
                    {
                        if( in_array( $file_type, array( 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ) ) )
                        {
                            //-- Word file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-doc.png';
                        }
                        elseif( $file_type == 'application/pdf' )
                        {
                            //-- PDF file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-pdf.png';
                        }
                        elseif( $file_type == 'image/svg+xml' )
                        {
                            //-- SVG file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-svg.png';
                        }
                        else
                        {
                            //-- Other file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-file.png';
                        }
                    }

                    return json_encode( array( 'result' => 'success', 'src' => $src, 'id' => $id ) );
                }
            }
            else
            {
                $this->upload->delete_thumb( $file );

                return json_encode( array( 'result' => 'failed' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'error' ) );
        }
    }

    function delete_file()
    {
        if( isset( $this->post[ 'lattach_id' ] ) && !empty( $this->post[ 'lattach_id' ] ) )
        {
            $image  = $this->global->getFields( 'lumonata_attachment', 'lattach', array( 'lattach_id' => $this->post[ 'lattach_id' ] ) );
            $result = parent::delete( 'lumonata_attachment', array( 'lattach_id' => $this->post[ 'lattach_id' ] ) );

            if( is_array( $result ) )
            {
                return json_encode( array( 'result' => 'error' ) );
            }
            else
            {
                if( empty( $image ) === false )
                {
                    $this->upload->upload_constructor( IMAGE_DIR . '/Uploads/' );
                    $this->upload->delete_file_thumb( $image );
                }

                return json_encode( array( 'result' => 'success' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
    }

    function order_file()
    {
        if( isset( $this->post[ 'image' ] ) && !empty( $this->post[ 'image' ] ) )
        {
            $error = 0;

            foreach( $this->post[ 'image' ] as $order_id => $attach_id )
            {
                $param  = array( 'lorder_id' => $order_id );
                $where  = array( 'lattach_id' => $attach_id );

                $result = parent::update( 'lumonata_attachment', $param, $where );

                if( is_array( $result ) )
                {
                    $error++;
                }
            }

            if( empty( $error ) == false )
            {
                return json_encode( array( 'result' => 'error', 'message' => 'Failed to reorder some image' ) );
            }
            else
            {
                return json_encode( array( 'result' => 'success' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'error', 'message' => 'No images was found to reorder' ) );
        }
    }

    function add_group()
    {
        if( isset( $this->post[ 'items' ] ) && !empty( $this->post[ 'items' ] ) )
        {
            $items  = json_decode( base64_decode( $this->post[ 'items' ] ), true );
            $object = json_decode( base64_decode( $this->post[ 'object' ] ), true );

            return $this->global->add_repeater_items( $items, $object );
        }
    }

    function sef_url( $string = '' )
    {
        $num = $this->global->getNumRows( 'lumonata_post', 'ltitle', $string );

        if( $num > 0 )
        {
            for( $i = 2; $i <= $num + 1; $i++ )
            {
                $sef = $this->global->sef_url( $string ) . '-' . $i;
                
                if( $this->global->getNumRows( 'lumonata_post', 'lsef_url', $sef ) < 1 )
                {
                    $sef = $sef;

                    break;
                }
            }
        }
        else
        {
            $sef = $this->global->sef_url( $string );
        }
        
        return $sef;
    }

    function custom_field( $mod )
    {
        $id = $this->fields[ 'lpost_id' ];

        //-- Custom Field
        $cparam = array(            
            'types' => 1,
            'field' => array(
                array(
                    'id'    => 'site_url',
                    'name'  => 'site_url',
                    'label' => 'Site URL',
                    'desc'  => 'your website domain whithout http:// or https://',
                    'type'  => 'text',
                    'attributes' => array(
                        'autocomplete' => 'off'
                    )
                ),
                array(
                    'id'    => 'list_front',
                    'name'  => 'list_front',
                    'label' => 'Frontend Limit View',
                    'desc'  => 'number of data that will show on frontend',
                    'type'  => 'text',
                    'attributes' => array(
                        'class'        => 'text text-number',
                        'autocomplete' => 'off',
                        'data-a-sep'   => '.',
                        'data-a-dec'   => ',',
                        'data-m-dec'   => '0'
                    )
                ),
                array(
                    'id'    => 'list_admin',
                    'name'  => 'list_admin',
                    'label' => 'Admin Limit View',
                    'desc'  => 'number of data that will show on admin panel',
                    'type'  => 'text',
                    'attributes' => array(
                        'class'        => 'text text-number',
                        'autocomplete' => 'off',
                        'data-a-sep'   => '.',
                        'data-a-dec'   => ',',
                        'data-m-dec'   => '0'
                    )
                ),
                array(
                    'id'    => 'google_map_api_key',
                    'name'  => 'google_map_api_key',
                    'label' => 'Google Map API Key',
                    'type'  => 'text',
                    'attributes' => array(
                        'autocomplete' => 'off'
                    )
                ),
                array(
                    'id'    => 'admin_credential',
                    'name'  => 'admin_credential',
                    'label' => 'Admin Credential',
                    'type'  => 'text',
                    'attributes' => array(
                        'autocomplete' => 'off'
                    )
                ),
                array(
                    'id'    => 'deposit_value',
                    'name'  => 'deposit_value',
                    'label' => 'Deposit Payment (%)',
                    'type'  => 'text',
                    'attributes' => array(
                        'class'        => 'text text-number',
                        'autocomplete' => 'off',
                        'data-a-sep'   => '.',
                        'data-a-dec'   => ',',
                        'data-m-dec'   => '0'
                    )
                ),
                array(
                    'id'      => 'llanguage',
                    'name'    => 'llanguage',
                    'label'   => 'Language',
                    'type'    => 'select',
                    'options' => $this->global->get_language_option(),
                    'attributes' => array(
                        'class' => 'chzn-select',
                        'autocomplete' => 'off',
                        'data-placeholder' => 'Choose Default Language'
                    )
                ),
                array(
                    'id'      => 'lcurrency',
                    'name'    => 'lcurrency',
                    'label'   => 'Currency',
                    'type'    => 'select',
                    'options' => $this->global->get_currency_option(),
                    'attributes' => array(
                        'class' => 'chzn-select',
                        'autocomplete' => 'off',
                        'data-placeholder' => 'Choose Default Currency'
                    )
                ),
                array(
                    'id'      => 'lcurrency_display',
                    'name'    => 'lcurrency_display',
                    'label'   => 'Display Currency In',
                    'type'    => 'select',
                    'options' => array( 
                        'Show Currency Symbol', 
                        'Show 3 Letters Code' 
                    ),
                    'attributes' => array(
                        'class' => 'chzn-select',
                        'autocomplete' => 'off',
                        'data-placeholder' => 'Choose Default Currency'
                    )
                ),
                array(
                    'id'      => 'default_payment',
                    'name'    => 'default_payment',
                    'label'   => 'Default Payment',
                    'type'    => 'select',
                    'options' => array( 
                        'paypal' => 'Paypal', 
                        'stripe' => 'Stripe' 
                    ),
                    'attributes' => array(
                        'class' => 'chzn-select',
                        'autocomplete' => 'off',
                        'data-placeholder' => 'Choose default payment'
                    )
                ),
            )
        );

        //-- Additional Field
        $aparam = array(           
            array(
                'group' => 'Detail Company',
                'types' => 1,
                'field' => array(
                    array(
                        'id'    => 'name',
                        'name'  => 'name',
                        'label' => 'Name',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                
                    array(
                        'id'    => 'address',
                        'name'  => 'address',
                        'label' => 'Address',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'email',
                        'name'  => 'email',
                        'label' => 'Email Address',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'type' => 'email'
                        )
                    ),
                    array(
                        'id'    => 'email_booking',
                        'name'  => 'email_booking',
                        'label' => 'Booking Email Address',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'type' => 'email'
                        )
                    ),
                    array(
                        'id'    => 'phone',
                        'name'  => 'phone',
                        'label' => 'Phone Number',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'fax',
                        'name'  => 'fax',
                        'label' => 'Fax Number',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                )
            ),
            array(
                'group' => 'Contact Us',
                'types' => 1,
                'field' => array(
                    array(
                        'id'    => 'sidebar_contact_us_text',
                        'name'  => 'sidebar_contact_us_text',
                        'label' => 'Sidebar Text',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off',
                        )
                    ),
                    array(
                        'id'    => 'sidebar_contact_us_subtext',
                        'name'  => 'sidebar_contact_us_subtext',
                        'label' => 'Sidebar Subtext',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'autocomplete' => 'off',
                        )
                    ),
                    array(
                        'id'    => 'contact_page_url',
                        'name'  => 'contact_page_url',
                        'label' => 'Contact Page URL',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                )
            ),
            array(
                'group' => 'SMTP Setting',
                'types' => 1,
                'field' => array(
                    array(
                        'id'    => 'smtp_server',
                        'name'  => 'smtp_server',
                        'label' => 'Server',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'smtp_port',
                        'name'  => 'smtp_port',
                        'label' => 'Port',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'smtp_email',
                        'name'  => 'smtp_email',
                        'label' => 'Email',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'type' => 'email'
                        )
                    ),
                    array(
                        'id'    => 'smtp_password',
                        'name'  => 'smtp_password',
                        'label' => 'Password',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'type' => 'password'
                        )
                    )
                )
            ),
            array(
                'group' => 'Gmail Xoauth',
                'types' => 1,
                'field' => array(
                    array(
                        'id'    => 'gmail_xoauth_from_email',
                        'name'  => 'gmail_xoauth_from_email',
                        'label' => 'Email Address',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'gmail_xoauth_client_id',
                        'name'  => 'gmail_xoauth_client_id',
                        'label' => 'Client ID',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'gmail_xoauth_client_secret',
                        'name'  => 'gmail_xoauth_client_secret',
                        'label' => 'Client Secret',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'gmail_xoauth_redirect_uri',
                        'name'  => 'gmail_xoauth_redirect_uri',
                        'label' => 'Redirect URI',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'gmail_xoauth_refresh_token',
                        'name'  => 'gmail_xoauth_refresh_token',
                        'label' => 'Refresh Token',
                        'type'  => 'gmail-xoauth',
                        'options' => array(
                            'client_id'     => 'gmail_xoauth_client_id',
                            'client_secret' => 'gmail_xoauth_client_secret',
                            'redirect_uri'  => 'gmail_xoauth_redirect_uri',
                        ),
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                )
            ),
            array(
                'group' => 'Mailchimp',
                'types' => 1,
                'field' => array(                    
                    array(
                        'id'    => 'mailchimp_api_key',
                        'name'  => 'mailchimp_api_key',
                        'label' => 'API Key',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'mailchimp_list_id',
                        'name'  => 'mailchimp_list_id',
                        'label' => 'List ID',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'mailchimp_server',
                        'name'  => 'mailchimp_server',
                        'label' => 'Server Prefix',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                )
            ),
            array(
                'group' => 'Google Client',
                'types' => 1,
                'field' => array(                    
                    array(
                        'id'    => 'google_client_id',
                        'name'  => 'google_client_id',
                        'label' => 'Client ID',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'google_client_secret',
                        'name'  => 'google_client_secret',
                        'label' => 'Client Secret',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'google_redirect_uri',
                        'name'  => 'google_redirect_uri',
                        'label' => 'Redirect URI Login',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                )
            ),
            array(
                'group' => 'Instagram Feeds',
                'types' => 1,
                'field' => array(                    
                    array(
                        'id'    => 'ig_app_id',
                        'name'  => 'ig_app_id',
                        'label' => 'App ID',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'ig_app_secret',
                        'name'  => 'ig_app_secret',
                        'label' => 'App Secret',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'ig_redirect_uri',
                        'name'  => 'ig_redirect_uri',
                        'label' => 'Valid OAuth Redirect URIs',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'ig_token',
                        'name'  => 'ig_token',
                        'label' => 'Refresh Token',
                        'type'  => 'instagram-xoauth',
                        'options' => array(
                            'client_id'     => 'ig_app_id',
                            'client_secret' => 'ig_app_secret',
                            'redirect_uri'  => 'ig_redirect_uri',
                        ),
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                )
            ),
            array(
                'group' => 'Paypal',
                'types' => 1,
                'field' => array(
                    array(
                        'id'      => 'paypal_environment',
                        'name'    => 'paypal_environment',
                        'label'   => 'Environment',
                        'type'    => 'select',
                        'options' => array(
                            'sandbox'    => 'Sandbox', 
                            'production' => 'Production' 
                        ),
                        'attributes' => array(
                            'class' => 'chzn-select',
                            'autocomplete' => 'off',
                            'data-placeholder' => 'Choose Paypal Environment'
                        )
                    ),
                    array(
                        'id'    => 'paypal_sandbox_app_id',
                        'name'  => 'paypal_sandbox_app_id',
                        'label' => 'Sandbox App ID',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'paypal_sandbox_app_secret',
                        'name'  => 'paypal_sandbox_app_secret',
                        'label' => 'Sandbox App Secret',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'paypal_production_app_id',
                        'name'  => 'paypal_production_app_id',
                        'label' => 'Production App ID',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'paypal_production_app_secret',
                        'name'  => 'paypal_production_app_secret',
                        'label' => 'Production App Secret',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                )
            ),
            array(
                'group' => 'Stripe',
                'types' => 1,
                'field' => array(
                    array(
                        'id'      => 'stripe_environment',
                        'name'    => 'stripe_environment',
                        'label'   => 'Environment',
                        'type'    => 'select',
                        'options' => array( 
                            'sandbox'    => 'Sandbox', 
                            'production' => 'Production' 
                        ),
                        'attributes' => array(
                            'class' => 'chzn-select',
                            'autocomplete' => 'off',
                            'data-placeholder' => 'Choose Stripe Environment'
                        )
                    ),
                    array(
                        'id'    => 'stripe_sandbox_public_key',
                        'name'  => 'stripe_sandbox_public_key',
                        'label' => 'Sandbox Public Key',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'stripe_sandbox_secret_key',
                        'name'  => 'stripe_sandbox_secret_key',
                        'label' => 'Sandbox Secret Secret',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'stripe_production_public_key',
                        'name'  => 'stripe_production_public_key',
                        'label' => 'Production Public Key',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'stripe_production_secret_key',
                        'name'  => 'stripe_production_secret_key',
                        'label' => 'Production Secret Secret',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                )
            ),
            array(
                'group' => 'Social Networks',
                'types' => 1,
                'field' => array(                    
                    array(
                        'id'    => 'fb_link',
                        'name'  => 'fb_link',
                        'label' => 'Facebook',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'ig_link',
                        'name'  => 'ig_link',
                        'label' => 'Instagram',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'yt_link',
                        'name'  => 'yt_link',
                        'label' => 'Youtube',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                )
            ),
            array(
                'group' => 'Phone Number',
                'types' => 1,
                'field' => array(                    
                    array(
                        'id'    => 'phone_global',
                        'name'  => 'phone_global',
                        'label' => 'Global',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'phone_asia',
                        'name'  => 'phone_asia',
                        'label' => 'Asia',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'phone_europe',
                        'name'  => 'phone_europe',
                        'label' => 'Europe',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'phone_international',
                        'name'  => 'phone_international',
                        'label' => 'International',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                )
            ),
            array(
                'group' => 'Whatsapp Number',
                'types' => 1,
                'field' => array(                    
                    array(
                        'id'    => 'wa_global',
                        'name'  => 'wa_global',
                        'label' => 'Global',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'wa_asia',
                        'name'  => 'wa_asia',
                        'label' => 'Asia',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'wa_europe',
                        'name'  => 'wa_europe',
                        'label' => 'Europe',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'wa_international',
                        'name'  => 'wa_international',
                        'label' => 'International',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                )
            ),
            array(
                'group' => 'Skype Number',
                'types' => 1,
                'field' => array(                    
                    array(
                        'id'    => 'skype_america',
                        'name'  => 'skype_america',
                        'label' => 'America',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'skype_europe',
                        'name'  => 'skype_europe',
                        'label' => 'Europe',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'skype_asia',
                        'name'  => 'skype_asia',
                        'label' => 'Asia',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                )
            ),
            array( 
                'group' => 'Prices Indicator',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'    => 'indicator_heading',
                        'name'  => 'indicator_heading',
                        'label' => 'Title',
                        'desc'  => 'Heading text for this section',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'     => 'indicator_repeater',
                        'name'   => 'indicator_repeater',
                        'type'   => 'repeater',
                        'option' => array(
                            'title'       => 'Indicator',
                            'add_text'    => 'Add',
                            'remove_text' => 'Delete',
                        ),
                        'items'  => array(
                            array(
                                'id'     => 'indicator_img',
                                'name'   => 'indicator_img',
                                'label'  => 'Icon',
                                'type'   => 'file',
                                'attributes' => array(
                                    'autocomplete' => 'off',
                                    'data-filetype' => 'svg'
                                )
                            ),                  
                            array(
                                'id'    => 'indicator_title',
                                'name'  => 'indicator_title',
                                'label' => 'Title',
                                'type'  => 'text',
                                'attributes' => array(
                                    'autocomplete' => 'off'
                                )
                            ),
                            array(
                                'id'    => 'indicator_description',
                                'name'  => 'indicator_description',
                                'label' => 'Description',
                                'type'  => 'textarea',
                                'attributes' => array(
                                    'autocomplete' => 'off',
                                    'rows' => 5
                                )
                            )
                        )
                    )
                )
            ),
            array( 
                'group' => 'Terms & Cancellation Policy',
                'types' => 1,
                'field' => array(                    
                    array(
                        'id'    => 'tc_payment',
                        'name'  => 'tc_payment',
                        'label' => 'Payment',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'tc_cancellation',
                        'name'  => 'tc_cancellation',
                        'label' => 'Cancellation Policy',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'tc_transfer',
                        'name'  => 'tc_transfer',
                        'label' => 'Transfer',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off'
                        )
                    )
                )
            ),
            array( 
                'group' => 'Destination Archive',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'     => 'destination_archive_header_image',
                        'name'   => 'destination_archive_header_image',
                        'label'  => 'Header Image',
                        'type'   => 'image',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                  
                    array(
                        'id'    => 'destination_archive_header_title',
                        'name'  => 'destination_archive_header_title',
                        'label' => 'Header Title',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                 
                    array(
                        'id'    => 'destination_archive_title',
                        'name'  => 'destination_archive_title',
                        'label' => 'Title',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'destination_archive_brief_text',
                        'name'  => 'destination_archive_brief_text',
                        'label' => 'Brief Text',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    )
                )
            ),
            array( 
                'group' => 'Accommodation Type Archive',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'     => 'acco_type_archive_header_image',
                        'name'   => 'acco_type_archive_header_image',
                        'label'  => 'Header Image',
                        'type'   => 'image',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                  
                    array(
                        'id'    => 'acco_type_archive_header_title',
                        'name'  => 'acco_type_archive_header_title',
                        'label' => 'Header Title',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                 
                    array(
                        'id'    => 'acco_type_archive_title',
                        'name'  => 'acco_type_archive_title',
                        'label' => 'Title',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'acco_type_archive_brief_text',
                        'name'  => 'acco_type_archive_brief_text',
                        'label' => 'Brief Text',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    )
                )
            ),
            array( 
                'group' => 'Blog Archive',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'     => 'blog_archive_header_image',
                        'name'   => 'blog_archive_header_image',
                        'label'  => 'Header Image',
                        'type'   => 'image',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                  
                    array(
                        'id'    => 'blog_archive_title',
                        'name'  => 'blog_archive_title',
                        'label' => 'Title',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'blog_archive_brief_text',
                        'name'  => 'blog_archive_brief_text',
                        'label' => 'Brief Text',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'autocomplete' => 'off',
                            'rows' => 5
                        )
                    )
                )
            ),
            array( 
                'group' => 'Search',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'     => 'search_header_image',
                        'name'   => 'search_header_image',
                        'label'  => 'Header Image',
                        'type'   => 'image',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                  
                    array(
                        'id'    => 'search_title',
                        'name'  => 'search_title',
                        'label' => 'Title',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'search_result_title',
                        'name'  => 'search_result_title',
                        'label' => 'Result Title',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    )
                )
            ),
            array( 
                'group' => 'Account Activation',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'     => 'activation_header_image',
                        'name'   => 'activation_header_image',
                        'label'  => 'Header Image',
                        'type'   => 'image',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                  
                    array(
                        'id'    => 'activation_success_title',
                        'name'  => 'activation_success_title',
                        'label' => 'Activation Success Title',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'activation_success_content',
                        'name'  => 'activation_success_content',
                        'label' => 'Activation Success Content',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off',
                        )
                    ),                 
                    array(
                        'id'    => 'activation_failed_title',
                        'name'  => 'activation_failed_title',
                        'label' => 'Activation Failed Title',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => 'activation_failed_content',
                        'name'  => 'activation_failed_content',
                        'label' => 'Activation Failed Content',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off',
                        )
                    ),
                )
            ),
            array( 
                'group' => '404 Not Found',
                'types' => 1, 
                'field' => array(
                    array(
                        'id'     => '404_header_image',
                        'name'   => '404_header_image',
                        'label'  => 'Header Image',
                        'type'   => 'image',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),                  
                    array(
                        'id'    => '404_title',
                        'name'  => '404_title',
                        'label' => 'Title',
                        'type'  => 'text',
                        'attributes' => array(
                            'autocomplete' => 'off'
                        )
                    ),
                    array(
                        'id'    => '404_content',
                        'name'  => '404_content',
                        'label' => 'Content',
                        'type'  => 'textarea',
                        'attributes' => array(
                            'class' => 'textarea tinymce',
                            'autocomplete' => 'off',
                        )
                    ),
                )
            )
        );

        $this->actions->add_actions( 'global_setting_custom_field', $this->global->init_custom_field( $id, $mod, $cparam ) );
        $this->actions->add_actions( 'global_setting_additional_field', $this->global->init_custom_field( $id, $mod, $aparam ) );
    }

    function language()
    {
        $this->lang = $this->global->getSettingValue( 'llanguage' );
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();

        $s = 'SELECT * FROM lumonata_post AS a WHERE a.ltype = %s LIMIT 1';
        $q = parent::prepare_query( $s, 'global_setting' );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $d = parent::fetch_array( $r );

            $fields = array(
                'lcreated_by'   => $d[ 'lcreated_by' ],
                'lusername'     => $d[ 'lusername' ],
                'llang_id'      => $d[ 'llang_id' ],
                'lpost_id'      => $d[ 'lpost_id' ],
                'lcreated_date' => $d[ 'lcreated_date' ],
                'ldlu'          => $d[ 'ldlu' ],
                'ltitle'        => $d[ 'ltitle' ],
                'ltype'         => $d[ 'ltype' ],
                'ldescription'  => $d[ 'ldescription' ],
                'lmeta_title'   => $d[ 'lmeta_title' ],
                'lmeta_key'     => $d[ 'lmeta_key' ],
                'lmeta_desc'    => $d[ 'lmeta_desc' ],
                'lsef_url'      => $d[ 'lsef_url' ],
                'lorder_id'     => $d[ 'lorder_id' ],
                'lparent_id'    => $d[ 'lparent_id' ],
                'lstatus'       => $d[ 'lstatus' ]
            );

            $this->exist = true;
        }
        else
        {
            $fields = array(
                'lcreated_by'   => $this->sess[ 'username' ],
                'lusername'     => $this->sess[ 'username' ],
                'llang_id'      => $this->lang,
                'lpost_id'      => time(),
                'lcreated_date' => time(),
                'ldlu'          => time(),
                'ltitle'        => 'Global Setting',
                'ltype'         => 'global_setting',
                'ldescription'  => '',
                'lmeta_title'   => '',
                'lmeta_key'     => '',
                'lmeta_desc'    => '',
                'lsef_url'      => '',
                'lorder_id'     => 0,
                'lparent_id'    => 0,
                'lstatus'       => 1
            );

            $this->exist = false;
        }

        if( empty( $_POST ) )
        {
            if( empty( $this->notif ) === false && isset( $this->notif[ 'post' ] ) )
            {
                $this->fields = array_merge( $fields, $this->notif[ 'post' ] );
            }
            else
            {
                if( isset( $fields ) )
                {
                    $this->fields = $fields;   
                }
            }

            $this->post = array();
        }
        else
        {
            if( isset( $_POST[ 'fields' ] ) )
            {
                $this->fields = array_merge( $fields, $_POST[ 'fields' ] );
            }
            else
            {
                $this->fields = $fields;
            }

            $this->post = array_diff_key( $_POST, array_flip( array( 'fields' ) ) );
        }
    }

    function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'order_file' )
            {
                echo $this->order_file();
            }

            if( $this->post[ 'pKEY' ] == 'delete_file' )
            {
                echo $this->delete_file();
            }

            if( $this->post[ 'pKEY' ] == 'upload_file' )
            {
                echo $this->upload_file( $mod );
            }

            if( $this->post[ 'pKEY' ] == 'add_group' )
            {
                echo $this->add_group();
            }
        }

        exit;
    }
}

?> 