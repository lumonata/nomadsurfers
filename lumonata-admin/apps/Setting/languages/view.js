function init_table()
{
	var limit = parseInt( jQuery('#limit').val() ) || 1;
    var table = jQuery('#table').DataTable({
        lengthChange: false,
        deferRender: true,
        pageLength: limit,
        processing: true,
        serverSide: true,
        info: false,
        order: [],
        ajax: {
            url: jQuery('#ajax_url').val(),
            type: 'POST',
            data: function( d ){
                d.lsearch = jQuery('[name=lsearch]').val();
                d.pKEY    = 'load';
            }
        },
        columns: [
            {
                data: 'llanguage', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'llang_code', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'lflag', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'lstatus', render: function ( data, type, row ) {
                    var html = 
                    '<div class="tbcheck">'+
                        '<input id="check-' + row.llang_id + '" value="' + row.llang_id + '" type="checkbox"' + ( data == 1 ? ' checked="checked"' : '' ) + ' autocomplete="off">'+
                        '<label for="#check-' + row.llang_id + '"></label>'+
                    '</div>';

                    return html;
                },
                className: 'text-center'
            },
            {
                data: 'llang_id', render: function ( data, type, row ) {
                    var html = 
                    '<a class="actione actione2" href="' + row.leditlink + '" title="Edit"></a>'+
                    '<a class="actiond actiond2" data-id="' + row.llang_id + '" title="Delete"></a>';

                    return html;
                },
                className: 'text-center'
            }    
        ],
        dom: 't'
    });

    table.on( 'draw.dt', function () {
        jQuery(this).closest('.dataTables_wrapper').find('.dataTables_paginate').toggle( table.page.info().pages > 1 );
        init_action( table );
    });

    jQuery('[name=filter]').on('click', function(){
        table.ajax.reload();
    });
}

function init_action( table )
{
    jQuery('.tbcheck input').unbind();
    jQuery('.tbcheck input').on('change', function(){
        var url = jQuery('#ajax_url').val();
        var sel = jQuery(this).parent();
        var prm = new Object;
            prm.status = this.checked ? 1 : 0;
            prm.id     = jQuery(this).val();
            prm.pKEY   = 'do_change_status';

        jQuery.ajax({
            url: url,
            data: prm,
            type: 'POST',
            dataType : 'json',
            success: function(e){
                if( e.result == 'success' )
                {
                    if( prm.status == 1 )
                    {
                        sel.find('.tcheck').removeClass('tcheckoff').addClass('tcheckon');
                        sel.find('input').prop('checked', true);
                    }
                    else
                    {
                        sel.find('.tcheck').removeClass('tcheckon').addClass('tcheckoff');
                        sel.find('input').prop('checked', false);
                    }
                }
                else
                {
                    show_popup( 'Failed to change data status' );

                    if( prm.status == 1 )
                    {
                        sel.find('input').prop('checked', false);
                    }
                    else
                    {
                        sel.find('input').prop('checked', true);
                    }
                }
            },
            error: function(e){
                show_popup( 'Something wrong on the request' );

                if( prm.status == 1 )
                {
                    sel.find('input').prop('checked', false);
                }
                else
                {
                    sel.find('input').prop('checked', true);
                }
            }
        });
    });

    jQuery('.actiond').unbind();
    jQuery('.actiond').on('click', function(){
        var id = jQuery(this).attr('data-id');

        jQuery('<div></div>').dialog({
            dialogClass: 'ui-dialog-no-titlebar',
            draggable: false,
            resizable: false,
            modal: true,
            open: function(){
                jQuery(this).html( 'Do you really want to DELETE this data?' );
            },
            buttons: [{
                text: 'Yes',
                click: function(){
                    var pop = jQuery( this ).dialog( 'close' );
                    var url = jQuery('#ajax_url').val();
                    var prm = new Object;
                        prm.pKEY = 'do_delete';
                        prm.id   = id;

                    jQuery.ajax({
                        url: url,
                        data: prm,
                        type: 'POST',
                        dataType : 'json',
                        success: function( e ){
                            pop.dialog('close');

                            if( e.result == 'success' )
                            {                        
                                table.ajax.reload( function(){
                                    show_popup( 'Successfully deleted data' );
                                    init_action( table );
                                }, false);
                            }
                            else
                            {
                                show_popup( 'Failed to delete data!' );
                            }
                        },
                        error: function( e ){
                            pop.dialog('close');
                                          
                            show_popup( 'Failed to delete data!' );
                        }
                    });
                }
            },{
                text: 'No',
                click: function(){
                    jQuery( this ).dialog( 'close' );
                }
            }]
        });
    });
}

function init_language_action()
{
    jQuery('[name=langopt]').on('change', function(){
        var dt = jQuery(this).find('option:selected').data();
        var va = jQuery(this).val();

        jQuery('.field-lcountry-code').val( dt.countryCode ).trigger('chosen:updated');
        jQuery('.field-llanguage').val( dt.name );
        jQuery('.field-llang-code').val( va );
    });
}

jQuery(document).ready(function(){
    init_language_action();
    init_table();
});