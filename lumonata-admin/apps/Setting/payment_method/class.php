<?php 
class payment_method extends db{
	function __construct($TEMPLATE_DIR=""){

        parent::__construct();
		$this->template_dir = $TEMPLATE_DIR;	
		$this->mod_id = $this->get_id_module();
	}

	function view($mod,$usertype,$src="",$arr=""){
		//check action
		if(isset($_POST['act']))$this->action_payment_method();
		
		$OUT_TEMPLATE="view.html";
		$t=new Template($this->template_dir);
		$t->set_file('view', $OUT_TEMPLATE);
		
		//set block	
		$t->set_block('view', 'viewContent', 'vC');
		$t->set_block('view', 'viewBlock', 'vB');
		
		$t->set_var('apps_dir',ADMIN_APPS_URL);
		$t->set_var('app_name',get_class($this));	
		$t->set_var('template_dir',ADMIN_THEME_URL);
		$this->set_content_payment_methode($t);
		
		$t->Parse('vC', 'viewContent', true);		
		return $t->Parse('vB', 'viewBlock', false); 
	}
	
	function set_content_payment_methode($t){
		$globalAdmin = new globalAdmin();
		$checked = 'checked="checked"';
		$paypal_method = $globalAdmin->getValueField2('lumonata_additional_fields','lvalue','lkey','paypal_method','lapp_name','payment_method');
		if(!empty($paypal_method) && $paypal_method=='true') $t->set_var('paypal_method',$checked);
		
		$verisign_method = $globalAdmin->getValueField2('lumonata_additional_fields','lvalue','lkey','verisign_method','lapp_name','payment_method');
		if(!empty($verisign_method) && $verisign_method=='true') $t->set_var('verisign_method',$checked);
		
		$doku_method = $globalAdmin->getValueField2('lumonata_additional_fields','lvalue','lkey','doku_method','lapp_name','payment_method');
		if(!empty($doku_method) && $doku_method=='true') $t->set_var('doku_method',$checked);
	}
	
	function action_payment_method(){
		$globalAdmin = new globalAdmin();
		$paypal_method = (!isset($_POST['paypal_method']) ? 'false':'true');
		$verisign_method = (!isset($_POST['verisign_method']) ? 'false':'true');
		$doku_method = (!isset($_POST['doku_method']) ? 'false':'true');
		
		$dt_paypal_method = $globalAdmin->getValueField2('lumonata_additional_fields','lapp_id','lkey','paypal_method','lapp_name','payment_method');
		if(empty($dt_paypal_method)) $this->save_additional_service($this->mod_id,'paypal_method',$paypal_method,'payment_method');
		else $this->update_additional_service('paypal_method',$paypal_method,'payment_method');
		
		$dt_verisign_method = $globalAdmin->getValueField2('lumonata_additional_fields','lapp_id','lkey','verisign_method','lapp_name','payment_method');
		if(empty($dt_verisign_method)) $this->save_additional_service($this->mod_id,'verisign_method',$verisign_method,'payment_method');
		else $this->update_additional_service('verisign_method',$verisign_method,'payment_method');
		
		$dt_doku_method = $globalAdmin->getValueField2('lumonata_additional_fields','lapp_id','lkey','doku_method','lapp_name','payment_method');
		if(empty($dt_doku_method)) $this->save_additional_service($this->mod_id,'doku_method',$doku_method,'payment_method');
		else $this->update_additional_service('doku_method',$doku_method,'payment_method');
	}
	
	function save_additional_service($app_id,$key,$value,$app_name){
		global $db;
		$q = $db->prepare_query("insert into lumonata_additional_fields (lapp_id,lkey,lvalue,lapp_name) values (%d,%s,%s,%s)",$app_id,$key,$value,$app_name);	
		$r = $db->query($q);
		if($r)return true;
		else return false;
	}
	
	function update_additional_service($key,$value,$app_name){
		global $db;
		$q = $db->prepare_query("update lumonata_additional_fields set lvalue=%s where lkey=%s and lapp_name=%s",$value,$key,$app_name);
		$r = $db->query($q);
		if($r)return true;
		else return false;
	}
	
	function get_id_module(){
		global $db;
		$globalAdmin = new globalAdmin();
		$mod_id = $globalAdmin->getValueField('lumonata_module','lmodule_id','lapps','payment_method');
		return $mod_id;
	}
}
?>