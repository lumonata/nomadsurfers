<?php

class translations extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->actions  = $actions;

        $this->param();
    }

    function view( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $this->global->getPViewBoth( $mod, $usertype ) )
        {
            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->create( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'mod', $mod );
            $this->template->set_var( 'cancel_css', 'hidden' );
            $this->template->set_var( 'action_label', 'CREATE' );
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'limit', $this->global->getSettingValue( 'list_admin' ) );

            $this->template->set_var( 'ltranslate_id', $ltranslate_id );
            $this->template->set_var( 'ltranslate_code', $ltranslate_code );
            $this->template->set_var( 'ltranslate_content', $this->get_translate_field( $ltranslate_content ) );

            $this->template->set_var( 'add_url', $this->global->getAddUrl( $mod, $usertype ) );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Setting/translations/view.js' ) );

            $this->template->Parse( 'vC', 'viewContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function edit( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $this->valid )
        {
            if( $this->global->getPEditBoth( $mod, $usertype ) )
            {
                //-- EXECUTE update action
                if( isset( $this->post[ 'save' ] ) )
                {
                    $this->change( $mod );
                }

                //-- EXTRACT fields
                extract( $this->fields );

                $this->template->set_var( 'mod', $mod );
                $this->template->set_var( 'action_label', 'CHANGE' );
                $this->template->set_var( 'message', $this->message );
                $this->template->set_var( 'limit', $this->global->getSettingValue( 'list_admin' ) );

                $this->template->set_var( 'ltranslate_id', $ltranslate_id );
                $this->template->set_var( 'ltranslate_code', $ltranslate_code );
                $this->template->set_var( 'ltranslate_code_attr', 'disabled' );
                $this->template->set_var( 'ltranslate_content', $this->get_translate_field( $ltranslate_content ) );

                $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );
                $this->template->set_var( 'cancel_url', $this->global->getViewUrl( $mod, $usertype ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

                $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Setting/translations/view.js' ) );

                $this->template->Parse( 'vC', 'viewContent', true );
            }
            else
            {
                $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
            }
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NOT_FOUND ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function load( $mod, $usertype )
    {
        $cols  = array(
            1 => 'a.ltranslate_code',
            2 => 'a.lcreated_date'
        );

        //-- Query Limit
        $limit = parent::prepare_query( ' LIMIT ' . $this->post[ 'start' ] . ', ' . $this->post[ 'length' ] );
    
        //-- Query Order By
        if( empty( $this->post[ 'order' ] ) )
        {
            $order = parent::prepare_query( ' ORDER BY a.lcreated_date DESC' );
        }
        else
        {
            $order = array();

            foreach( $this->post[ 'order' ] as $i => $od )
            {
                if( isset( $cols[ $od[ 'column' ] ] ) )
                {
                    $order[] = $cols[ $od[ 'column' ] ] . ' ' . $od[ 'dir' ];
                }
            }

            if( empty( $order ) )
            {
                $order = parent::prepare_query( ' ORDER BY a.lcreated_date DESC' );
            }
            else
            {
                $order = parent::prepare_query( ' ORDER BY ' . implode( ', ', $order ) );
            }
        }

        //-- Query Additional Where
        $w = array();

        if( $this->post[ 'lsearch' ] != '' )
        {
            $s = array();

            foreach( $cols as $col )
            {
                $s[] = parent::prepare_query( $col . ' LIKE %s', '%' . $this->post[ 'lsearch' ] . '%' );
            }

            $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
        }

        if( empty( $w ) === false )
        {
            $where = ' HAVING ' . implode( ' AND ', $w );
        }
        else
        {
            $where = '';
        }

        //-- Main Query
        $qm = 'SELECT 
                a.ltranslate_id,
                a.lcreated_date,
                a.ltranslate_code,
                a.ltranslate_content
              FROM lumonata_translations AS a' . $where . $order;
        $rm = parent::query( $qm );
        $nm = parent::num_rows( $rm );

        $qf = $qm . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        $data = array();

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
                $c = json_decode( $df[ 'ltranslate_content' ], true );

                if( isset( $c[ 'en' ] ) )
                {
                    $ltranslate_text = $c[ 'en' ];
                }
                else
                {
                    $ltranslate_text = '-';
                }

                $data[] = array(
                    'leditlink'       => $this->global->getEditUrl( $mod, $usertype, $df[ 'ltranslate_id' ] ),
                    'lcreated_date'   => date( 'd F Y', $df[ 'lcreated_date' ] ),
                    'ltranslate_code' => $df[ 'ltranslate_code' ],
                    'ltranslate_id'   => $df[ 'ltranslate_id' ],
                    'ltranslate_text' => $ltranslate_text
                );
            }
        }

        return json_encode( array(
            'draw'            => intval( $this->post[ 'draw' ] ),
            'recordsFiltered' => intval( $nm ),
            'recordsTotal'    => intval( $nf ),
            'data'            => $data
        ));
    }

    function validate( $data = array() )
    {
        $error = array();

        foreach( $data as $field => $dt )
        {
            if( $field == 'ltranslate_code' && $dt == '' )
            {
                array_push( $error, 'String code must have value' );
            }
        }

        return $error;
    }

    function create( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- INSERT lumonata_translations
            $param  = array_diff_key( array_merge( $data, array( 
                'ltranslate_code' => $this->sef_url( $data[ 'ltranslate_code' ] ),
                'ltranslate_content' => json_encode( $data[ 'ltranslate_content' ] )
            )), array_flip( array( 'ltranslate_id' ) ) );

            $result = parent::insert( 'lumonata_translations', $param );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to add new data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=view' );

                exit;
            }
            else
            {
                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully add new data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=view' );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=view' );

            exit;
        }
    }

    function change( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- UPDATE lumonata_translations
            $param  = array_diff_key( array_merge( $data, array(
                'ltranslate_content' => json_encode( $data[ 'ltranslate_content' ] )
            )), array_flip( array( 'ltranslate_id' ) ) );

            $where  = array( 'ltranslate_id' => $data[ 'ltranslate_id' ] );
            $result = parent::update( 'lumonata_translations', $param, $where );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to update existing data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $data[ 'ltranslate_id' ] );

                exit;
            }
            else
            {
                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully updated existing data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $data[ 'ltranslate_id' ] );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $data[ 'ltranslate_id' ] );

            exit;
        }
    }

    function do_delete( $mod )
    {
        $s = 'DELETE FROM lumonata_translations WHERE ltranslate_id = %d';
        $q = parent::prepare_query( $s, $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();
        $this->valid   = true;

        if( isset( $_GET[ 'id' ] ) )
        {
            $s = 'SELECT * FROM lumonata_translations AS a WHERE a.ltranslate_id = %d';
            $q = parent::prepare_query( $s, $_GET[ 'id' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $d = parent::fetch_array( $r );

                if( $d[ 'ltranslate_content' ] != '' )
                {
                    $d[ 'ltranslate_content' ] = json_decode( $d[ 'ltranslate_content' ], true );
                }

                $fields = array(
                    'lcreated_by'        => $d[ 'lcreated_by' ],
                    'lusername'          => $d[ 'lusername' ],
                    'ltranslate_id'      => $d[ 'ltranslate_id' ],
                    'lcreated_date'      => $d[ 'lcreated_date' ],
                    'ldlu'               => $d[ 'ldlu' ],
                    'ltranslate_code'    => $d[ 'ltranslate_code' ],
                    'ltranslate_content' => $d[ 'ltranslate_content' ]
                );
            }
            else
            {
                $this->valid = false;
            }
        }
        else
        {
            $fields = array(
                'lcreated_by'        => $this->sess[ 'username' ],
                'lusername'          => $this->sess[ 'username' ],
                'ltranslate_id'      => time(),
                'lcreated_date'      => time(),
                'ldlu'               => time(),
                'ltranslate_code'    => '',
                'ltranslate_content' => ''
            );
        }

        if( empty( $_POST ) )
        {
            if( empty( $this->notif ) === false && isset( $this->notif[ 'post' ] ) )
            {
                $this->fields = array_merge( $fields, $this->notif[ 'post' ] );
            }
            else
            {
                if( isset( $fields ) )
                {
                    $this->fields = $fields;   
                }
            }

            $this->post = array();
        }
        else
        {
            if( isset( $_POST[ 'fields' ] ) )
            {
                $this->fields = array_merge( $fields, $_POST[ 'fields' ] );
            }
            else
            {
                $this->fields = $fields;
            }

            $this->post = array_diff_key( $_POST, array_flip( array( 'fields' ) ) );
        }
    }

    function sef_url( $string = '' )
    {
        $s = 'SELECT * FROM lumonata_translations AS a WHERE a.ltranslate_code LIKE %s';
        $q = parent::prepare_query( $s, '%' . $string . '%' );
        $r = parent::query( $q );
        $n = parent::num_rows( $r );

        if( $n > 0 )
        {
            for( $i = 2; $i <= $n + 1; $i++ )
            {
                $sef = $this->global->sef_url( $string ) . '-' . $i;
                
                if( $this->global->getNumRows( 'lumonata_translations', 'ltranslate_code', $sef ) < 1 )
                {
                    $sef = $sef;

                    break;
                }
            }
        }
        else
        {
            $sef = $this->global->sef_url( $string );
        }
        
        return $sef;
    }

    function get_translate_field( $content )
    {
        require_once( INCLUDE_DIR . '/vendor/autoload.php' );

        $s = 'SELECT * FROM lumonata_language AS a WHERE a.lstatus = 1 ORDER BY a.lorder_id';
        $q = parent::prepare_query( $s );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $list = '';

            while( $d = parent::fetch_assoc( $r ) )
            {
                try
                {
                    $c = country( $d[ 'lcountry_code' ] );

                    $list .= '
                    <div class="field-wrapp">
                        <span>' . $c->getEmoji() . '&nbsp;&nbsp;' . strtoupper( $d[ 'llang_code' ] ) . '</span>
                        <textarea class="textarea" name="fields[ltranslate_content][' . $d[ 'llang_code' ] . ']" row="3">' . ( isset( $content[ $d[ 'llang_code' ] ] ) ? $content[ $d[ 'llang_code' ] ] : '' ) . '</textarea>
                    </div>';
                }
                catch( Exception $e )
                {
                    //-- do nothing
                }
            }

            return $list;
        }
    }

    function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'load' )
            {
                echo $this->load( $mod, $usertype );
            }

            if( $this->post[ 'pKEY' ] == 'do_delete' )
            {
                echo $this->do_delete( $mod );
            }
        }

        exit;
    }
}

?> 