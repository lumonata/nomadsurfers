function init_table()
{
	var limit = parseInt( jQuery('#limit').val() ) || 1;
    var table = jQuery('#table').DataTable({
        lengthChange: false,
        deferRender: true,
        pageLength: limit,
        processing: true,
        serverSide: true,
        info: false,
        order: [],
        ajax: {
            url: jQuery('#ajax_url').val(),
            type: 'POST',
            data: function( d ){
                d.lsearch = jQuery('[name=lsearch]').val();
                d.pKEY    = 'load';
            }
        },
        columns: [
            {
                data: 'ltranslate_code', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'ltranslate_text', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'lcreated_date', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'ltranslate_id', render: function ( data, type, row ) {
                    var html = 
                    '<a class="actione actione2" href="' + row.leditlink + '" title="Edit"></a>'+
                    '<a class="actiond actiond2" data-id="' + row.ltranslate_id + '" title="Delete"></a>';

                    return html;
                },
                className: 'text-center'
            }    
        ],
        dom: 'tp'
    });

    table.on( 'draw.dt', function () {
        jQuery(this).closest('.dataTables_wrapper').find('.dataTables_paginate').toggle( table.page.info().pages > 1 );
        init_action( table );
    });

    jQuery('[name=filter]').on('click', function(){
        table.ajax.reload();
    });
}

function init_action( table )
{
    jQuery('.actiond').unbind();
    jQuery('.actiond').on('click', function(){
        var id = jQuery(this).attr('data-id');

        jQuery('<div></div>').dialog({
            dialogClass: 'ui-dialog-no-titlebar',
            draggable: false,
            resizable: false,
            modal: true,
            open: function(){
                jQuery(this).html( 'Do you really want to DELETE this data?' );
            },
            buttons: [{
                text: 'Yes',
                click: function(){
                    var pop = jQuery( this ).dialog( 'close' );
                    var url = jQuery('#ajax_url').val();
                    var prm = new Object;
                        prm.pKEY = 'do_delete';
                        prm.id   = id;

                    jQuery.ajax({
                        url: url,
                        data: prm,
                        type: 'POST',
                        dataType : 'json',
                        success: function( e ){
                            pop.dialog('close');

                            if( e.result == 'success' )
                            {                        
                                table.ajax.reload( function(){
                                    show_popup( 'Successfully deleted data' );
                                    init_action( table );
                                }, false);
                            }
                            else
                            {
                                show_popup( 'Failed to delete data!' );
                            }
                        },
                        error: function( e ){
                            pop.dialog('close');
                                          
                            show_popup( 'Failed to delete data!' );
                        }
                    });
                }
            },{
                text: 'No',
                click: function(){
                    jQuery( this ).dialog( 'close' );
                }
            }]
        });
    });
}

jQuery(document).ready(function(){
    init_table();
});