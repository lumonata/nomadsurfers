<?php

class admin extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->upload   = new upload();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }
    
    function view( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );
        
        if( $this->global->getPViewBothP( $mod, $usertype ) )
        {
            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->create( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'mod', $mod );
            $this->template->set_var( 'cancel_css', 'hidden' );
            $this->template->set_var( 'action_label', 'CREATE' );
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'limit', $this->global->getSettingValue( 'list_admin' ) );

        	$this->template->set_var( 'lemail', $lemail );
        	$this->template->set_var( 'lskype', $lskype );
        	$this->template->set_var( 'lphone', $lphone );
            $this->template->set_var( 'luser_id', $luser_id );
        	$this->template->set_var( 'llastname', $llastname );
        	$this->template->set_var( 'lusername', $lusername );
        	$this->template->set_var( 'lfirstname', $lfirstname );
        	$this->template->set_var( 'lusername_style', '' );

            $this->template->set_var( 'limage', $this->get_avatar_image( $limage ) );
            $this->template->set_var( 'lmodule', $this->get_user_module_option( $lmodule_id ) );
            $this->template->set_var( 'lcountry_code', $this->get_country_option( $lcountry_code ) );
            $this->template->set_var( 'lusertype_id', $this->get_user_type_option( $lusertype_id ) );

            $this->template->set_var( 'site_url', SITE_URL );
            $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Users/admin/view.js' ) );
            
            $this->template->Parse( 'vC', 'viewContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function edit( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $this->valid )
        {
            if( $this->global->getPEditBoth( $mod, $usertype ) )
            {
                //-- EXECUTE update action
                if( isset( $this->post[ 'save' ] ) )
                {
                    $this->change( $mod );
                }

                //-- EXTRACT fields
                extract( $this->fields );

                $this->template->set_var( 'mod', $mod );
                $this->template->set_var( 'action_label', 'CHANGE' );
                $this->template->set_var( 'message', $this->message );
                $this->template->set_var( 'limit', $this->global->getSettingValue( 'list_admin' ) );

            	$this->template->set_var( 'lemail', $lemail );
            	$this->template->set_var( 'lskype', $lskype );
            	$this->template->set_var( 'lphone', $lphone );
                $this->template->set_var( 'luser_id', $luser_id );
            	$this->template->set_var( 'llastname', $llastname );
            	$this->template->set_var( 'lusername', $lusername );
            	$this->template->set_var( 'lfirstname', $lfirstname );
        		$this->template->set_var( 'lmodule_style', 'disabled' );
            	$this->template->set_var( 'lusername_style', 'readonly' );

                $this->template->set_var( 'limage', $this->get_avatar_image( $limage ) );
	            $this->template->set_var( 'lmodule', $this->get_user_module_option( $lmodule_id ) );
                $this->template->set_var( 'lcountry_code', $this->get_country_option( $lcountry_code ) );
	            $this->template->set_var( 'lusertype_id', $this->get_user_type_option( $lusertype_id ) );

                $this->template->set_var( 'site_url', SITE_URL );
                $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
                $this->template->set_var( 'cancel_url', HT_SERVER . ADMIN_URL . '/home.php?mod=' . $mod . '&prc=view' );
                $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

                $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Users/admin/view.js' ) );
                
                $this->template->Parse( 'vC', 'viewContent', true );
            }
            else
            {
                $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
            }
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NOT_FOUND ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function load( $mod, $usertype )
    {
        $cols  = array(
            1 => 'a.lusername',
            2 => 'b.lname',
            4 => 'a.lcountry_code',
            5 => 'a.lstatus'
        );

        //-- Query Limit
        $limit = parent::prepare_query( ' LIMIT ' . $this->post[ 'start' ] . ', ' . $this->post[ 'length' ] );
    
        //-- Query Order By
        if( empty( $this->post[ 'order' ] ) )
        {
            $order = parent::prepare_query( ' ORDER BY a.lorder_id ASC' );
        }
        else
        {
            $order = array();

            foreach( $this->post[ 'order' ] as $i => $od )
            {
                if( isset( $cols[ $od[ 'column' ] ] ) )
                {
                    $order[] = $cols[ $od[ 'column' ] ] . ' ' . $od[ 'dir' ];
                }
            }

            if( empty( $order ) )
            {
                $order = parent::prepare_query( ' ORDER BY a.lorder_id ASC' );
            }
            else
            {
                $order = parent::prepare_query( ' ORDER BY ' . implode( ', ', $order ) );
            }
        }

        //-- Query Additional Where
        $w = array();

        if( $this->post[ 'search' ][ 'value' ] != '' )
        {
            $s = array();

            foreach( $cols as $col )
            {
                $s[] = parent::prepare_query( $col . ' LIKE %s', '%' . $this->post[ 'search' ][ 'value' ] . '%' );
            }

            $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
        }

        if( empty( $w ) === false )
        {
            $where = ' AND ' . implode( ' AND ', $w );
        }
        else
        {
            $where = '';
        }

        //-- Main Query
        $qm = 'SELECT 
                a.lname,
                a.limage,
                a.lstatus,
                a.luser_id,
                a.lusername,
                a.lcountry_code,
                b.lname AS lusertype
               FROM lumonata_user AS a
               LEFT JOIN lumonata_usertype AS b ON a.lusertype_id = b.lusertype_id
               WHERE a.lusertype_id NOT IN( 1, 2 )' . $where . $order;
        $rm = parent::query( $qm );
        $nm = parent::num_rows( $rm );

        $qf = $qm . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        $data = array();

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
                if( empty( $df[ 'limage' ] ) || ( !empty( $df[ 'limage' ] ) && !file_exists( IMAGE_DIR . '/Users/' . $df[ 'limage' ] ) ) )
                {
                    $img = 'default.png';
                }
                else
                {
                    $img = $df[ 'limage' ];
                }

                $data[] = array(
                    'limage'    => HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=60&h=60&src=' . HT_SERVER . SITE_URL . '/images/Users/' . $img,
                    'leditlink' => $this->global->getEditUrl( $mod, $usertype, $df[ 'luser_id' ] ),
                    'lflag'     => $this->global->get_country_flag( $df[ 'lcountry_code' ] ),
                    'lcountry'  => $this->global->get_country_name( $df[ 'lcountry_code' ] ),
                    'lusertype' => $df[ 'lusertype' ],
                    'lusername' => $df[ 'lusername' ],
                    'luser_id'  => $df[ 'luser_id' ],
                    'lstatus'   => $df[ 'lstatus' ],
                    'lname'     => $df[ 'lname' ]
                );
            }
        }

        return json_encode( array(
            'draw'            => intval( $this->post[ 'draw' ] ),
            'recordsFiltered' => intval( $nm ),
            'recordsTotal'    => intval( $nf ),
            'data'            => $data
        ));
    }

    function validate( $data = array(), $update = false )
    {
        $error = array();

        foreach( $data as $field => $dt )
        {
            if( $field == 'lusername' && $dt == '' )
            {
                array_push( $error, 'Username must have value' );
            }

            if( $field == 'lemail' && $dt == '' )
            {
                array_push( $error, 'Email address must have value' );
            }

            if( $field == 'lusertype_id' && $dt == '' )
            {
                array_push( $error, 'User level must have value' );
            }

            if( $field == 'lusertype_id' && $dt == '0' && $data[ 'lpermission_name' ] == '' )
            {
                array_push( $error, 'New level name must have value' );
            }

            if( $field == 'lusertype_id' && $dt == '0' && empty( $data[ 'lmodule_id' ] ) )
            {
                array_push( $error, 'Select some module for new lever user' );
            }

            if( $field == 'lpassword' && $dt == '' && $update === false )
            {
                array_push( $error, 'Password must have value' );
            }

            if( $field == 'lpassword' && $dt != '' )
            {
                if( $dt != $data[ 'lcpassword' ] )
                {
                    array_push( $error, 'Confirm your password with same value' );
                }
            }
        }

        return $error;
    }

    function set_param( $data, $update = false )
    {
        //-- Encrypt password
        $data[ 'lpassword' ] = md5( $data[ 'lpassword' ] );

        //-- Remove confirm password
        unset( $data[ 'lcpassword' ] );

        //-- Remove permission field
        unset( $data[ 'lmodule_id' ] );
        unset( $data[ 'lpermission_name' ] );

        //-- Add image if exist
        if( isset( $_FILES[ 'limage' ] ) && $_FILES[ 'limage' ][ 'error' ] == 0 )
        {
            $image_name = $_FILES[ 'limage' ][ 'name' ];
            $image_size = $_FILES[ 'limage' ][ 'size' ];
            $image_type = $_FILES[ 'limage' ][ 'type' ];
            $image_tmp  = $_FILES[ 'limage' ][ 'tmp_name' ];

            $sef_img = $this->upload->file_name_filter( $image_name ) . '-' . time();
            $image   = $this->upload->rename_file( $image_name, $sef_img );

            if( $image_type == 'image/jpg' || $image_type == 'image/jpeg' || $image_type == 'image/pjpeg' || $image_type == 'image/gif' || $image_type == 'image/png' )
            {
                $this->upload->upload_constructor( IMAGE_DIR . '/Users/' );

                if( $this->upload->upload_resize( $image_name, $sef_img, $image_tmp, $image_type, 90, 90 ) )
                {
                    if( empty( $data[ 'limage' ] ) === false )
                    {
                        $this->upload->delete_file( $data[ 'limage' ] );
                    }

                    if( $this->upload->upload_file( $image_name, $sef_img, $image_tmp, 0 ) )
                    {
                        $data[ 'limage' ] = $image;
                    }
                    else
                    {
                        $this->upload->delete_thumb( $image_name );

                        $data[ 'limage' ] = '';
                    }
                }
            }
        }

        //-- Remove id from stack
        if( $update === false )
        {
            unset( $data[ 'luser_id' ] );
        }

        //-- Remove empty value, except 0 & "0"
        $data = array_filter( $data, function( $var ){ 
            return $var !== NULL && $var !== FALSE && $var !== ''; 
        });

        return $data;
    }

    function create( $mod )
    {
        $error = $this->validate( $this->fields );
        $data  = $this->fields;

        if( empty( $error ) )
        {
            parent::begin();

            $commit = 1;

            //-- INSERT lumonata_user_type
            if( $data[ 'lusertype_id' ] == '0' )
            {
                $result = parent::insert( 'lumonata_usertype', array(
                    'lname'         => $data[ 'lpermission_name' ],
                    'lcreated_date' => $data[ 'lcreated_date' ],
                    'lcreated_by'   => $data[ 'lcreated_by' ],
                    'lusername'     => $data[ 'lusername' ],
                    'ldlu'          => $data[ 'ldlu' ]
                ));

                if( is_array( $result ) )
                {
                    $commit = 0;
                }
                else
                {
                    $data[ 'lusertype_id' ] = parent::insert_id();
                }
            }
            
            if( empty( $data[ 'lmodule_id' ] ) === false )
            {
                $s = 'SELECT a.lmodule_id FROM lumonata_user_permissions AS a WHERE a.lusertype_id = %d';
                $q = parent::prepare_query( $s, $data[ 'lusertype_id' ] );
                $r = parent::query( $q );

                $module = array();

                if( parent::num_rows( $r ) > 0 )
                {
                    while( $d = parent::fetch_array( $r ) )
                    {
                        $module[] = $d[ 'lmodule_id' ];
                    }
                }

                $delete = array_diff( $module, $data[ 'lmodule_id' ] );
                $insert = array_diff( $data[ 'lmodule_id' ], $module );

                //-- DELETE unused permission
                if( empty( $delete ) === false )
                {
                    foreach( $delete as $dt )
                    {
                        $result = parent::delete( 'lumonata_user_permissions', array( 'lmodule_id' => $dt, 'lusertype_id' => $data[ 'lusertype_id' ] ) );

                        if( is_array( $result ) )
                        {
                            $commit = 0;

                            break;
                        }
                    }
                }

                //-- INSERT new permission
                if( empty( $insert ) === false )
                {
                    foreach( $insert as $dt )
                    {
                        $result = parent::insert( 'lumonata_user_permissions', array( 
                            'lcreated_by'   => $this->sess[ 'username' ],
                            'lusername'     => $this->sess[ 'username' ],
                            'lusertype_id'  => $data[ 'lusertype_id' ],
                            'lcreated_date' => time(),
                            'ldlu'          => time(),
                            'lmodule_id'    => $dt, 
                        ));

                        if( is_array( $result ) )
                        {
                            $commit = 0;

                            break;
                        }
                    }
                }
            }

            if( $commit == 1 )
            {
                //-- INSERT lumonata_user
                $result = parent::insert( 'lumonata_user', $this->set_param( $data ) );

                if( is_array( $result ) )
                {
                    $commit = 0;
                }
            }

            if( $commit == 0 )
            {
                parent::rollback();

                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to add new data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=view' );

                exit;
            }
            else
            {
                parent::commit();

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully add new data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=view' );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=view' );

            exit;
        }
    }

    function change( $mod )
    {
        $error = $this->validate( $this->fields, true );
        $data  = $this->fields;

        if( empty( $error ) )
        {
            parent::begin();

            $commit = 1;

            //-- INSERT lumonata_user_type
            if( $data[ 'lusertype_id' ] == '0' )
            {
                $result = parent::insert( 'lumonata_usertype', array(
                    'lname'         => $data[ 'lpermission_name' ],
                    'lcreated_date' => $data[ 'lcreated_date' ],
                    'lcreated_by'   => $data[ 'lcreated_by' ],
                    'lusername'     => $data[ 'lusername' ],
                    'ldlu'          => $data[ 'ldlu' ]
                ));

                if( is_array( $result ) )
                {
                    $commit = 0;
                }
                else
                {
                    $data[ 'lusertype_id' ] = parent::insert_id();
                }
            }
            
            if( empty( $data[ 'lmodule_id' ] ) === false )
            {
                $s = 'SELECT a.lmodule_id FROM lumonata_user_permissions AS a WHERE a.lusertype_id = %d';
                $q = parent::prepare_query( $s, $data[ 'lusertype_id' ] );
                $r = parent::query( $q );

                $module = array();

                if( parent::num_rows( $r ) > 0 )
                {
                    while( $d = parent::fetch_array( $r ) )
                    {
                        $module[] = $d[ 'lmodule_id' ];
                    }
                }

                $delete = array_diff( $module, $data[ 'lmodule_id' ] );
                $insert = array_diff( $data[ 'lmodule_id' ], $module );

                //-- DELETE unused permission
                if( empty( $delete ) === false )
                {
                    foreach( $delete as $dt )
                    {
                        $result = parent::delete( 'lumonata_user_permissions', array( 'lmodule_id' => $dt, 'lusertype_id' => $data[ 'lusertype_id' ] ) );

                        if( is_array( $result ) )
                        {
                            $commit = 0;

                            break;
                        }
                    }
                }

                //-- INSERT new permission
                if( empty( $insert ) === false )
                {
                    foreach( $insert as $dt )
                    {
                        $result = parent::insert( 'lumonata_user_permissions', array( 
                            'lcreated_by'   => $this->sess[ 'username' ],
                            'lusername'     => $this->sess[ 'username' ],
                            'lusertype_id'  => $data[ 'lusertype_id' ],
                            'lcreated_date' => time(),
                            'ldlu'          => time(),
                            'lmodule_id'    => $dt, 
                        ));

                        if( is_array( $result ) )
                        {
                            $commit = 0;

                            break;
                        }
                    }
                }
            }

            if( $commit == 1 )
            {
                //-- UPDATE lumonata_user
                $result = parent::update( 'lumonata_user', $this->set_param( $data, true ), array( 'luser_id' => $data[ 'luser_id' ] ) );

                if( is_array( $result ) )
                {
                    $commit = 0;
                }
            }

            if( $commit == 0 )
            {
                parent::rollback();

                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to update existing data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $data[ 'luser_id' ] );

                exit;
            }
            else
            {
                parent::commit();

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully updated existing data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $data[ 'luser_id' ] );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $data[ 'luser_id' ] );

            exit;
        }
    }

    function do_delete()
    {
        parent::begin();

        $commit = 1;

        $avatar = $this->global->getValueField( 'lumonata_user', 'limage', 'luser_id', $this->post[ 'id' ] );

        $s = 'DELETE FROM lumonata_user WHERE luser_id = %s';
        $q = parent::prepare_query( $s, $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            $commit = 0;
        }
        else
        {
            //-- Delete avatar image if exist
            if( empty( $avatar ) === false )
            {
                $this->upload->upload_constructor( IMAGE_DIR . '/Users/' );
                $this->upload->delete_file( $avatar );
            }
        }
                
        if( $commit == 0 )
        {
            parent::rollback();

            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            parent::commit();

            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function do_change_status()
    {
        $s = 'UPDATE lumonata_user AS a SET a.lstatus = %d WHERE a.luser_id = %s';
        $q = parent::prepare_query( $s, $this->post[ 'status' ], $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function language()
    {
        $this->lang = $this->global->getSettingValue( 'llanguage' );
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();
        $this->valid   = true;

        if( isset( $_GET[ 'id' ] ) )
        {
            $s = 'SELECT *, ( SELECT GROUP_CONCAT( a2.lmodule_id ) FROM lumonata_user_permissions AS a2 WHERE a2.lusertype_id = a.lusertype_id ) AS lmodule_id FROM lumonata_user AS a WHERE a.luser_id = %d';
            $q = parent::prepare_query( $s, $_GET[ 'id' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $d = parent::fetch_array( $r );

                if( empty( $d[ 'lmodule_id' ] ) === false )
                {
                    $d[ 'lmodule_id' ] = explode( ',', $d[ 'lmodule_id' ] );
                }

                $fields = array(
                    'lcreated_by'   => $d[ 'lcreated_by' ],
                    'lmodule_id'    => $d[ 'lmodule_id' ],
                    'lcreated_date' => $d[ 'lcreated_date' ],
                    'ldlu'          => $d[ 'ldlu' ],
                    'luser_id'      => $d[ 'luser_id' ],
                    'limage'        => $d[ 'limage' ],
                    'lusername'     => $d[ 'lusername' ],
                    'lusertype_id'  => $d[ 'lusertype_id' ],
                    'lname'         => $d[ 'lname' ],
                    'lfirstname'    => $d[ 'lfirstname' ],
                    'llastname'     => $d[ 'llastname' ],
                    'lemail'        => $d[ 'lemail' ],
                    'lphone'        => $d[ 'lphone' ],
                    'lskype'        => $d[ 'lskype' ],
                    'lcountry_code' => $d[ 'lcountry_code' ],
                    'lorder_id'     => $d[ 'lorder_id' ],
                    'lstatus'       => $d[ 'lstatus' ],
                );
            }
            else
            {
                $this->valid = false;
            }
        }
        else
        {
            $fields = array(
                'lcreated_by'   => $this->sess[ 'username' ],
                'lmodule_id'    => array(),
                'lcreated_date' => time(),
                'ldlu'          => time(),
                'luser_id'      => time(),
                'limage'        => '',
                'lusername'     => '',
                'lusertype_id'  => '',
                'lname'         => '',
                'lfirstname'    => '',
                'llastname'     => '',
                'lemail'        => '',
                'lphone'        => '',
                'lskype'        => '',
                'lcountry_code' => '',
                'lorder_id'     => 0,
                'lstatus'       => 1,
            );
        }

        if( empty( $_POST ) )
        {
            if( empty( $this->notif ) === false && isset( $this->notif[ 'post' ] ) )
            {
                $this->fields = array_merge( $fields, $this->notif[ 'post' ] );
            }
            else
            {
                if( isset( $fields ) )
                {
                    $this->fields = $fields;   
                }
            }

            $this->post = $_POST;
        }
        else
        {
            if( isset( $_POST[ 'fields' ] ) )
            {
                $this->fields = array_merge( $fields, $_POST[ 'fields' ] );
            }
            else
            {
                $this->fields = $fields;
            }

            $this->post = array_diff_key( $_POST, array_flip( array( 'fields' ) ) );
        }
    }

    function get_avatar_image( $image = '' )
    {
        $avatar = '<input value="' . $image . '" name="fields[limage]" type="hidden" autocomplete="off" />';

        if( empty( $image ) || ( !empty( $image ) && !file_exists( IMAGE_DIR . '/Users/' . $image ) ) )
        {
            $avatar .= '<img id="fileoutput" src="' . HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=90&h=90&src=' . HT_SERVER . SITE_URL . '/images/Users/default.png" alt="" />';
        }
        else
        {
            $avatar .= '<img id="fileoutput" src="' . HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=90&h=90&src=' . HT_SERVER . SITE_URL . '/images/Users/' . $image . '" alt="" />';
        }

        return $avatar;
    }

    function get_module_permission()
    {
        $s = 'SELECT a.lmodule_id FROM lumonata_user_permissions AS a WHERE a.lusertype_id = %d';
        $q = parent::prepare_query( $s, $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $data = array();

            while( $d = parent::fetch_array( $r ) )
            {
                $data[] = $d[ 'lmodule_id' ];
            }

            return json_encode( array( 'result' => 'success', 'data' => $data ) );
        }
        else
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
    }

    function get_user_module_option( $module_id = array() )
    {
        $q = 'SELECT a.lmodule_id, a.lname, a.lparent_id FROM  lumonata_module AS a WHERE a.lview = 1 GROUP BY a.lmodule_id ORDER BY a.lorder_id DESC';

        return $this->global->set_recursive_option( $module_id, $q, 'lname', 'lmodule_id', 'lparent_id' );
    }

    function get_user_type_option( $usertype_id = '', $where = array() )
    {
        $q = 'SELECT a.lusertype_id, a.lname FROM lumonata_usertype AS a WHERE a.lusertype_id NOT IN( 1, 2 )';
        
        return $this->global->set_option( $q, 'lusertype_id', 'lname', $usertype_id, false );
    }

    function get_country_option( $lcountry_code = '' )
    {
        return $this->global->set_flag_option( $lcountry_code, false );
    }

    function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'load' )
            {
                echo $this->load( $mod, $usertype );
            }

            if( $this->post[ 'pKEY' ] == 'do_change_status' )
            {
                echo $this->do_change_status();
            }

            if( $this->post[ 'pKEY' ] == 'do_delete' )
            {
                echo $this->do_delete();
            }

            if( $this->post[ 'pKEY' ] == 'get_module_permission' )
            {
                echo $this->get_module_permission();
            }
        }

        exit;
    }
}

?> 