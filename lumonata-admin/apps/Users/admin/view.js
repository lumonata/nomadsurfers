function init_table()
{
	var limit = parseInt( jQuery('#limit').val() ) || 1;
    var table = jQuery('#table').DataTable({
        lengthChange: false,
        deferRender: true,
        pageLength: limit,
        processing: true,
        serverSide: true,
        info: false,
        order: [],
        ajax: {
            url: jQuery('#ajax_url').val(),
            type: 'POST',
            data: function( d ){
                d.pKEY = 'load';
            }
        },
        columns: [
            {
                data: 'limage', render: function ( data, type, row ){
                    var html = '<img src="'+ data +'" class="avatar-dsh-property" alt="" />';

                    return html;
                },
                className: 'text-center'
            },
            {
                data: 'lusername', render: function ( data, type, row ){
                    return data
                }
            },
            {
                data: 'lname', render: function ( data, type, row ){
                    return data
                }
            },
            {
                data: 'lusertype', render: function ( data, type, row ){
                    var html = '<span class="utype">' + data + '</span>';

                    return html;
                },
                className: 'text-center'
            },
            {
                data: 'lcountry', render: function ( data, type, row ){
                    if( row.lflag == '' )
                    {
                        return data;
                    }
                    else
                    {
                        var html = row.lflag + '<br />' + data;

                        return html;
                    }
                },
                className: 'text-center'
            },
            {
                data: 'lstatus', render: function ( data, type, row ) {
                    var html = 
                    '<div class="tbcheck">'+
                        '<input id="check-' + row.luser_id + '" value="' + row.luser_id + '" type="checkbox"' + ( data == 1 ? ' checked="checked"' : '' ) + ' autocomplete="off">'+
                        '<label for="#check-' + row.luser_id + '"></label>'+
                    '</div>';

                    return html;
                },
                className: 'text-center'
            },
            {
                data: 'luser_id', render: function ( data, type, row ) {
                    var html = 
                    '<a class="actione actione2" href="' + row.leditlink + '"  title="Edit"></a>'+
                    '<a class="actiond actiond2" data-id="' + row.luser_id + '" title="Delete"></a>';

                    return html;
                },
                className: 'text-center'
            }           
        ],
        dom: 't'
    });

    table.on( 'draw.dt', function () {
        jQuery(this).closest('.dataTables_wrapper').find('.dataTables_paginate').toggle( table.page.info().pages > 1 );
        init_action( table );
    });

    jQuery('[name=search]').on('keyup change', function(){
        table.search( this.value ).draw() ;
    });
}

function init_action( table )
{
    jQuery('.tbcheck input').unbind();
    jQuery('.tbcheck input').on('change', function(){
        var url = jQuery('#ajax_url').val();
        var sel = jQuery(this).parent();
        var prm = new Object;
            prm.status = this.checked ? 1 : 0;
            prm.id     = jQuery(this).val();
            prm.pKEY   = 'do_change_status';

        jQuery.ajax({
            url: url,
            data: prm,
            type: 'POST',
            dataType : 'json',
            success: function(e){
                if( e.result == 'success' )
                {
                    if( prm.status == 1 )
                    {
                        sel.find('.tcheck').removeClass('tcheckoff').addClass('tcheckon');
                        sel.find('input').prop('checked', true);
                    }
                    else
                    {
                        sel.find('.tcheck').removeClass('tcheckon').addClass('tcheckoff');
                        sel.find('input').prop('checked', false);
                    }
                }
                else
                {
                    show_popup( 'Failed to change data status' );

                    if( prm.status == 1 )
                    {
                        sel.find('input').prop('checked', false);
                    }
                    else
                    {
                        sel.find('input').prop('checked', true);
                    }
                }
            },
            error: function(e){
                show_popup( 'Something wrong on the request' );

                if( prm.status == 1 )
                {
                    sel.find('input').prop('checked', false);
                }
                else
                {
                    sel.find('input').prop('checked', true);
                }
            }
        });
    });

    jQuery('.actiond').unbind();
    jQuery('.actiond').on('click', function(){
        var id = jQuery(this).attr('data-id');

        jQuery('<div></div>').dialog({
            dialogClass: 'ui-dialog-no-titlebar',
            draggable: false,
            resizable: false,
            modal: true,
            open: function(){
                jQuery(this).html( 'Do you really want to DELETE this data?' );
            },
            buttons: [{
                text: 'Yes',
                click: function(){
                    var pop = jQuery( this ).dialog( 'close' );
                    var url = jQuery('#ajax_url').val();
                    var prm = new Object;
                        prm.pKEY = 'do_delete';
                        prm.id   = id;

                    jQuery.ajax({
                        url: url,
                        data: prm,
                        type: 'POST',
                        dataType : 'json',
                        success: function( e ){
                            pop.dialog('close');

                            if( e.result == 'success' )
                            {                        
                                table.ajax.reload( function(){
                                    show_popup( 'Successfully deleted data' );
                                    init_load_more( table );
                                    init_action( table );
                                }, false);
                            }
                            else
                            {
                                show_popup( 'Failed to delete data!' );
                            }
                        },
                        error: function( e ){
                            pop.dialog('close');
                                          
                            show_popup( 'Failed to delete data!' );
                        }
                    });
                }
            },{
                text: 'No',
                click: function(){
                    jQuery( this ).dialog( 'close' );
                }
            }]
        });
    });
}

function init_select()
{
    jQuery('.usertype').on('change', function(){
        var id = jQuery(this).val();

        if( id == '' )
        {
            jQuery('.list-usertype input').prop('disabled', true);
            jQuery('.moduleid').val('').prop('disabled', true).trigger('chosen:updated');
        }
        else if( id == '0' )
        {
            jQuery('.list-usertype input').prop('disabled', false);
            jQuery('.moduleid').val('').prop('disabled', false).trigger('chosen:updated');
        }
        else
        {
            jQuery('.list-usertype input').prop('disabled', true);
            jQuery('.moduleid').val('').prop('disabled', false).trigger('chosen:updated');

            var url = jQuery('#ajax_url').val();
            var prm = new Object;
                prm.pKEY = 'get_module_permission';
                prm.id   = id;

            jQuery.ajax({
                url: url,
                data: prm,
                type: 'POST',
                dataType : 'json',
                success: function( e ){
                    if( e.result == 'success' )
                    {
                        jQuery.each( e.data, function( i, mid ){
                            jQuery('.moduleid option[value="' + mid + '"]').prop('selected', true);
                        });

                        jQuery('.moduleid').trigger('chosen:updated');
                    }
                    else
                    {
                        show_popup( 'Failed to load module list!' );
                    }
                },
                error: function( e ){
                    show_popup( 'Failed to load module list!' );
                }
            });
        }
    });
}

function init_avatar()
{
    jQuery('#fileupload').on('change', function( e ){
        var reader = new FileReader();

        reader.onload = function(){
            document.getElementById('fileoutput').src = reader.result;
        };

        reader.readAsDataURL( e.target.files[0] );
    })
}

jQuery(document).ready(function(){
    init_avatar();
    init_select();
    init_table();
});