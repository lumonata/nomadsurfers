<?php

class agent extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }
    
    function view( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );
        
        if( $this->global->getPViewBothP( $mod, $usertype ) )
        {
            $this->template->set_var( 'mod', $mod );
            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'limit', $this->global->getSettingValue( 'list_admin' ) );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Users/agent/view.js' ) );
            
            $this->template->Parse( 'vC', 'viewContent', true );
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function edit( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->valid )
        {
            if( $this->global->getPEditBoth( $mod, $usertype ) )
            {
                //-- EXECUTE update action
                if( isset( $this->post[ 'save' ] ) )
                {
                    $this->change( $mod );
                }

                //-- EXTRACT fields
                extract( $this->fields );

                $this->template->set_var( 'lfax', $lfax );
                $this->template->set_var( 'lcity', $lcity );
                $this->template->set_var( 'lname', $lname );
                $this->template->set_var( 'labout', $labout );
                $this->template->set_var( 'lemail', $lemail );
                $this->template->set_var( 'lphone', $lphone );
                $this->template->set_var( 'luser_id', $luser_id );
                $this->template->set_var( 'laddress', $laddress );
                $this->template->set_var( 'lwebsite', $lwebsite );
                $this->template->set_var( 'lcompany', $lcompany );
                $this->template->set_var( 'lusername', $lusername );
                $this->template->set_var( 'llastname', $llastname );
                $this->template->set_var( 'lfacebook', $lfacebook );
                $this->template->set_var( 'lorder_id', $lorder_id );
                $this->template->set_var( 'linstagram', $linstagram );
                $this->template->set_var( 'lfirstname', $lfirstname );
                $this->template->set_var( 'ljob_title', $ljob_title );
                $this->template->set_var( 'lagent_type', $lagent_type );
                $this->template->set_var( 'lpostal_code', $lpostal_code );
                $this->template->set_var( 'lstatus', $this->get_agent_status_option( $lstatus ) );
                $this->template->set_var( 'lgender', $this->get_agent_gender_option( $lgender ) );


                $this->template->set_var( 'mod', $mod );
                $this->template->set_var( 'message', $this->message );

                $this->template->set_var( 'site_url', SITE_URL );
                $this->template->set_var( 'apps_url', ADMIN_APPS_URL );
                $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.min.css' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chosenjs@1.4.3/chosen.jquery.min.js' ) );

                $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . ADMIN_APPS_URL . '/Users/agent/form.js' ) );
                
                $this->template->Parse( 'fC', 'formContent', true );
            }
            else
            {
                $this->template->set_var( 'error', $this->global->setError( NO_AUTHO ) );
            }
        }
        else
        {
            $this->template->set_var( 'error', $this->global->setError( NOT_FOUND ) );
        }
        
        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function load( $mod, $usertype )
    {
        $cols  = array(
            0 => 'a.lname', 
            1 => 'a.lemail',
            2 => 'a.lphone',
            3 => 'a.lstatus'
        );

        //-- Query Limit
        $limit = parent::prepare_query( ' LIMIT ' . $this->post[ 'start' ] . ', ' . $this->post[ 'length' ] );
    
        //-- Query Order By
        if( empty( $this->post[ 'order' ] ) )
        {
            $order = parent::prepare_query( ' ORDER BY a.lorder_id ASC' );
        }
        else
        {
            $order = array();

            foreach( $this->post[ 'order' ] as $i => $od )
            {
                if( isset( $cols[ $od[ 'column' ] ] ) )
                {
                    $order[] = $cols[ $od[ 'column' ] ] . ' ' . $od[ 'dir' ];
                }
            }

            if( empty( $order ) )
            {
                $order = parent::prepare_query( ' ORDER BY a.lorder_id ASC' );
            }
            else
            {
                $order = parent::prepare_query( ' ORDER BY ' . implode( ', ', $order ) );
            }
        }

        //-- Query Additional Where
        $w = array();

        if( $this->post[ 'search' ][ 'value' ] != '' )
        {
            $s = array();

            foreach( $cols as $col )
            {
                $s[] = parent::prepare_query( $col . ' LIKE %s', '%' . $this->post[ 'search' ][ 'value' ] . '%' );
            }

            $w[] = sprintf( '(%s)', implode( ' OR ', $s ) );
        }

        if( empty( $w ) === false )
        {
            $where = ' HAVING' . implode( ' AND ', $w );
        }
        else
        {
            $where = '';
        }

        //-- Main Query
        $qm = 'SELECT 
                a.lname,
                a.lphone,
                a.lemail,
                a.lstatus,
                a.luser_id,
                a.llastname,
                a.lfirstname
               FROM lumonata_user AS a WHERE a.lusertype_id = 2' . $where . $order;
        $rm = parent::query( $qm );
        $nm = parent::num_rows( $rm );

        $qf = $qm . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        $data = array();

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
                if( empty( $df[ 'lname' ] ) )
                {
                    if( !empty( $df[ 'lfirstname' ] ) && $df[ 'llastname' ] )
                    {
                        $df[ 'lname' ] = sprintf( '%s %s', $df[ 'lfirstname' ], $df[ 'llastname' ] );
                    }
                    elseif( !empty( $df[ 'lfirstname' ] ) )
                    {
                        $df[ 'lname' ] = $df[ 'lfirstname' ];
                    }
                    elseif( !empty( $df[ 'llastname' ] ) )
                    {
                        $df[ 'lname' ] = $df[ 'llastname' ];
                    }
                }

                $data[] = array(
                    'leditlink' => $this->global->getEditUrl( $mod, $usertype, $df[ 'luser_id' ] ),
                    'lname'     => ucwords( strtolower( $df[ 'lname' ] ) ),
                    'luser_id'  => $df[ 'luser_id' ],
                    'lstatus'   => $df[ 'lstatus' ],
                    'lemail'    => $df[ 'lemail' ],
                    'lphone'    => $df[ 'lphone' ]
                );
            }
        }

        return json_encode( array(
            'draw'            => intval( $this->post[ 'draw' ] ),
            'recordsFiltered' => intval( $nm ),
            'recordsTotal'    => intval( $nf ),
            'data'            => $data
        ));
    }

    function validate( $data = array() )
    {
        $error = array();

        foreach( $data as $field => $dt )
        {
            if( $field == 'lfirstname' && $dt == '' )
            {
                array_push( $error, 'First name must have value' );
            }

            if( $field == 'lusername' && $dt == '' )
            {
                array_push( $error, 'Username must have value' );
            }

            if( $field == 'lemail' && $dt == '' )
            {
                array_push( $error, 'Email address must have value' );
            }

            if( $field == 'lpassword' && $dt != '' )
            {
                if( $dt != $data[ 'lcpassword' ] )
                {
                    array_push( $error, 'Confirm your password with same value' );
                }
            }
        }

        return $error;
    }

    function set_param( $data, $update = false )
    {
        //-- Encrypt password
        $data[ 'lpassword' ] = md5( $data[ 'lpassword' ] );

        //-- Remove confirm password
        unset( $data[ 'lcpassword' ] );

        //-- Remove id from stack
        if( $update === false )
        {
            unset( $data[ 'luser_id' ] );
        }

        //-- Remove empty value, except 0 & "0"
        $data = array_filter( $data, function( $var ){ 
            return $var !== NULL && $var !== FALSE && $var !== ''; 
        });

        return $data;
    }

    function change( $mod )
    {
        $error = $this->validate( $this->fields );
        $data  = $this->fields;

        if( empty( $error ) )
        {
            //-- UPDATE lumonata_user
            $result = parent::update( 'lumonata_user', $this->set_param( $data, true ), array( 'luser_id' => $data[ 'luser_id' ] ) );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to update existing data', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $data[ 'luser_id' ] );

                exit;
            }
            else
            {
                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully updated existing data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $data[ 'luser_id' ] );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/lumonata-admin/home.php?mod=' . $mod . '&prc=edit&id=' . $data[ 'luser_id' ] );

            exit;
        }
    }

    function do_delete()
    {
        $s = 'DELETE FROM lumonata_user WHERE luser_id = %s';
        $q = parent::prepare_query( $s, $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function do_change_status()
    {
        $s = 'UPDATE lumonata_user AS a SET a.lstatus = %d WHERE a.luser_id = %s';
        $q = parent::prepare_query( $s, $this->post[ 'status' ], $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            return json_encode( array( 'result' => 'success' ) );
        }
    }

    function get_agent_status_option( $lstatus = 0 )
    {
        return $this->global->set_static_option( array( 'Not Active', 'Active', 'Waiting Approval' ), $lstatus, false );
    }

    function get_agent_gender_option( $lgender = 0 )
    {
        return $this->global->set_static_option( array( 'Male', 'Female', 'Other' ), $lgender, false );
    }

    function language()
    {
        $this->lang = $this->global->getSettingValue( 'llanguage' );
    }

    function param()
    {
        $this->notif    = $this->flash->render();
        $this->message  = $this->flash->message( $this->notif ); 
        $this->sess     = $this->global->getCurrentSession();
        $this->valid    = true;

        if( isset( $_GET[ 'id' ] ) )
        {
            $s = 'SELECT * FROM lumonata_user AS a WHERE a.luser_id = %s';
            $q = parent::prepare_query( $s, $_GET[ 'id' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $d = parent::fetch_array( $r );

                $fields = array(
                    'lcreated_by'   => $d[ 'lcreated_by' ],
                    'lcreated_date' => $d[ 'lcreated_date' ],
                    'ldlu'          => $d[ 'ldlu' ],
                    'luser_id'      => $d[ 'luser_id' ],
                    'lusername'     => $d[ 'lusername' ],
                    'lfirstname'    => $d[ 'lfirstname' ],
                    'llastname'     => $d[ 'llastname' ],
                    'lemail'        => $d[ 'lemail' ],
                    'laddress'      => $d[ 'laddress' ],
                    'lwebsite'      => $d[ 'lwebsite' ],
                    'lcompany'      => $d[ 'lcompany' ],
                    'ljob_title'    => $d[ 'ljob_title' ],
                    'lphone'        => $d[ 'lphone' ],
                    'lfax'          => $d[ 'lfax' ],
                    'lpostal_code'  => $d[ 'lpostal_code' ],
                    'lcity'         => $d[ 'lcity' ],
                    'lname'         => $d[ 'lname' ],
                    'lcat_id'       => $d[ 'lcat_id' ],
                    'labout'        => $d[ 'labout' ],
                    'lfacebook'     => $d[ 'lfacebook' ],
                    'linstagram'    => $d[ 'linstagram' ],
                    'lagent_type'   => $d[ 'lagent_type' ],
                    'lcountry_id'   => $d[ 'lcountry_id' ],
                    'lgender'       => $d[ 'lgender' ],
                    'lorder_id'     => $d[ 'lorder_id' ],
                    'lstatus'       => $d[ 'lstatus' ]
                );
            }
            else
            {
                $this->valid = false;
            }
        }
        else
        {
            $fields = array(
                'lcreated_by'   => $this->sess[ 'username' ],
                'lcreated_date' => time(),
                'ldlu'          => time(),
                'luser_id'      => time(),
                'lusername'     => '',
                'lfirstname'    => '',
                'llastname'     => '',
                'lemail'        => '',
                'laddress'      => '',
                'lwebsite'      => '',
                'lcompany'      => '',
                'ljob_title'    => '',
                'lphone'        => '',
                'lfax'          => '',
                'lpostal_code'  => '',
                'lcity'         => '',
                'lname'         => '',
                'lcat_id'       => '',
                'labout'        => '',
                'lfacebook'     => '',
                'linstagram'    => '',
                'lagent_type'   => 0,
                'lcountry_id'   => 0,
                'lgender'       => 0,
                'lorder_id'     => 0,
                'lstatus'       => 1,
            );
        }

        if( empty( $_POST ) )
        {
            if( empty( $this->notif ) === false && isset( $this->notif[ 'post' ] ) )
            {
                $this->fields = array_merge( $fields, $this->notif[ 'post' ] );
            }
            else
            {
                if( isset( $fields ) )
                {
                    $this->fields = $fields;   
                }
            }

            $this->post = $_POST;
        }
        else
        {
            if( isset( $_POST[ 'fields' ] ) )
            {
                $this->fields = array_merge( $fields, $_POST[ 'fields' ] );
            }
            else
            {
                $this->fields = $fields;
            }

            $this->post = array_diff_key( $_POST, array_flip( array( 'fields' ) ) );
        }
    }

    function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'load' )
            {
                echo $this->load( $mod, $usertype );
            }

            if( $this->post[ 'pKEY' ] == 'do_change_status' )
            {
                echo $this->do_change_status();
            }

            if( $this->post[ 'pKEY' ] == 'do_delete' )
            {
                echo $this->do_delete();
            }
        }

        exit;
    }
}

?>