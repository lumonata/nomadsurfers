function init_table()
{
	var limit = parseInt( jQuery('#limit').val() ) || 1;
    var table = jQuery('#table').DataTable({
        lengthChange: false,
        deferRender: true,
        pageLength: limit,
        processing: true,
        serverSide: true,
        info: false,
        order: [],
        ajax: {
            url: jQuery('#ajax_url').val(),
            type: 'POST',
            data: function( d ){
                d.pKEY = 'load';
            }
        },
        columns: [
            {
                data: 'lname', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'lemail', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'lphone', render: function ( data, type, row ){
                    return data;
                }
            },
            {
                data: 'lstatus', render: function ( data, type, row ) {
                    if( data == 0 )
                    {
                        var html = 
                        '<span class="tcheck tcheckoff actionicon mt10" rel="' + row.luser_id + '" title="Not Active"></span>'+
                        '<input type="checkbox" value="0" name="status" id="' + row.luser_id + '" style="display:none;" checked="checked">'+
                        '<span class="textaction-' + row.luser_id + '">Not Active</span>';
                    }
                    else if( data == 1 )
                    {
                        var html = 
                        '<span class="tcheck tcheckon actionicon mt10" rel="' + row.luser_id + '" title="Active"></span>'+
                        '<input type="checkbox" value="1" name="status" id="' + row.luser_id + '" style="display:none;" checked="checked">'+
                        '<span class="textaction-' + row.luser_id + '">Active</span>';
                    }
                    else if( data == 2 )
                    {
                        var html = 
                        '<span class="tcheck tcheckoff waiting actionicon mt10" rel="' + row.luser_id + '" title="Waiting Approval"></span>'+
                        '<input type="checkbox" value="2" name="status" id="' + row.luser_id + '" style="display:none;" checked="checked">'+
                        '<span class="textaction-' + row.luser_id + '">Waiting Approval</span>';
                    }

                    return html;
                },
                className: 'text-center'
            },
            {
                data: 'luser_id', render: function ( data, type, row ) {
                    var html = 
                    '<a class="actione actione2" href="' + row.leditlink + '"  title="Edit"></a>'+
                    '<a class="actiond actiond2" data-id="' + row.luser_id + '" title="Delete"></a>';

                    return html;
                },
                className: 'text-center'
            }           
        ],
        dom: 'tp'
    });

    table.on( 'draw.dt', function(){
        jQuery(this).closest('.dataTables_wrapper').find('.dataTables_paginate').toggle( table.page.info().pages > 1 );
        init_action( table );
    });

    jQuery('[name=search]').on('keyup change', function(){
        table.search( this.value ).draw() ;
    });
}

function init_action( table )
{
    jQuery('.tcheck').unbind();
    jQuery('.tcheck').on( 'click', function(){
        if( jQuery(this).parent().find('input').attr('checked') == 'checked' )
        {
            jQuery(this).parent().find('input').prop('checked', false).change();
        }
        else
        {
            jQuery(this).parent().find('input').prop('checked', true).change();
        }
    });

    jQuery('.actionc').unbind();
    jQuery('.actionc').on('change', function(){
        var url = jQuery('#ajax_url').val();
        var sel = jQuery(this).parent();
        var prm = new Object;
            prm.id     = jQuery(this).attr('id');
            prm.status = this.checked ? 1 : 0;
            prm.pKEY   = 'do_change_status';

        jQuery.ajax({
            url: url,
            data: prm,
            type: 'POST',
            dataType : 'json',
            success: function(e){
                if( e.result == 'success' )
                {
                    if( prm.status == 1 )
                    {
                        sel.find('.tcheck').removeClass('tcheckoff').addClass('tcheckon');
                        sel.find('input').prop('checked', true);
                    }
                    else
                    {
                        sel.find('.tcheck').removeClass('tcheckon').addClass('tcheckoff');
                        sel.find('input').prop('checked', false);
                    }
                }
                else
                {
                    show_popup( 'Failed to change data status' );

                    if( prm.status == 1 )
                    {
                        sel.find('input').prop('checked', false);
                    }
                    else
                    {
                        sel.find('input').prop('checked', true);
                    }
                }
            },
            error: function(e){
                show_popup( 'Something wrong on the request' );

                if( prm.status == 1 )
                {
                    sel.find('input').prop('checked', false);
                }
                else
                {
                    sel.find('input').prop('checked', true);
                }
            }
        });
    });

    jQuery('.actiond').unbind();
    jQuery('.actiond').on('click', function(){
        var id = jQuery(this).attr('data-id');

        jQuery('<div></div>').dialog({
            dialogClass: 'ui-dialog-no-titlebar',
            draggable: false,
            resizable: false,
            modal: true,
            open: function(){
                jQuery(this).html( 'Do you really want to DELETE this data?' );
            },
            buttons: [{
                text: 'Yes',
                click: function(){
                    var pop = jQuery( this ).dialog( 'close' );
                    var url = jQuery('#ajax_url').val();
                    var prm = new Object;
                        prm.pKEY = 'do_delete';
                        prm.id   = id;

                    jQuery.ajax({
                        url: url,
                        data: prm,
                        type: 'POST',
                        dataType : 'json',
                        success: function( e ){
                            pop.dialog('close');

                            if( e.result == 'success' )
                            {                        
                                table.ajax.reload( function(){
                                    show_popup( 'Successfully deleted data' );
                                    init_load_more( table );
                                    init_action( table );
                                }, false);
                            }
                            else
                            {
                                show_popup( 'Failed to delete data!' );
                            }
                        },
                        error: function( e ){
                            pop.dialog('close');
                                          
                            show_popup( 'Failed to delete data!' );
                        }
                    });
                }
            },{
                text: 'No',
                click: function(){
                    jQuery( this ).dialog( 'close' );
                }
            }]
        });
    });
}

jQuery(document).ready(function(){
    init_table();
});