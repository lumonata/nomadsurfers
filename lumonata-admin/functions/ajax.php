<?php

require_once( '../../config.php' );
require_once( '../../functions/db.php' );

if( isset( $_POST[ 'pKEY' ] ) && $_POST[ 'pKEY' ] == 'upload_file' )
{
	if( isset( $_POST[ 'user' ] ) && ( $_POST[ 'user' ] == '' || $_POST[ 'user' ] == 'undefined' ) )
	{
		echo json_encode( array( 'result' => 'not-login' ) );
	}
	else if( isset( $_POST[ 'mod' ] ) && ( $_POST[ 'mod' ] == '' || $_POST[ 'mod' ] == 'undefined' ) )
	{
		echo json_encode( array( 'result' => 'not-found' ) );
	}
	else
	{
		global $db;

		require_once( 'globals.php' );
		require_once( 'upload.php' );

	    $global = new globalAdmin();
	    $upload = new upload();

	    $file_name = $_FILES[ 'attachment' ][ 'name' ];
	    $file_size = $_FILES[ 'attachment' ][ 'size' ];
	    $file_type = $_FILES[ 'attachment' ][ 'type' ];
	    $file_tmp  = $_FILES[ 'attachment' ][ 'tmp_name' ];

	    $sef_img  = $upload->file_name_filter( $file_name ) . '-' . time();
	    $file     = $upload->rename_file( $file_name, $sef_img );

	    $upload->upload_constructor( IMAGE_DIR . '/Uploads/' );

	    if( in_array( $file_type, array( 'image/jpg', 'image/jpeg', 'image/pjpeg', 'image/gif', 'image/png' ) ) )
	    {
	        $upload->upload_resize( $file_name, $sef_img, $file_tmp, $file_type, 940, 940, 1 );

		    if( $upload->upload_file( $file_name, $sef_img, $file_tmp, 0 ) )
		    {
		        $order_id = $global->setCode( 'lumonata_attachment', 'lorder_id' );

		        if( isset( $_POST[ 'lang' ] ) && $_POST[ 'lang' ] != ''  && $_POST[ 'lang' ] != 'undefined' )
		        {
		        	$lang_id = $_POST[ 'lang' ];
		        }
		        else
		        {
		        	$lang_id = $global->getSettingValue( 'llanguage' );
		        }

		        $r = $db->insert( 'lumonata_attachment', array(
		            'lapp_id'       => $_POST[ 'app_id' ],
		            'lcreated_by'   => $_POST[ 'user' ],
		            'lusername'     => $_POST[ 'user' ],
		            'lmodule_id'    => $_POST[ 'mod' ],
		            'lmimetype'     => $file_type,
		            'lorder_id'     => $order_id,
		            'llang_id'      => $lang_id,
		            'lsef_url'      => $sef_img,
		            'ltitle'        => $title,
		            'lattach'       => $file,
		            'lcreated_date' => time(),
		            'ldlu'          => time(),
		            'lstatus'       => 2
		        ));

		        if( is_array( $r ) )
		        {
		            $upload->delete_file_thumb( $file );

		            echo json_encode( array( 'result' => 'failed' ) );
		        }
		        else
		        {
		            $id = $db->insert_id();

		            if( $file_type == 'image/jpg' || $file_type == 'image/jpeg' || $file_type == 'image/pjpeg' || $file_type == 'image/gif' || $file_type == 'image/png' )
		            {
		                //-- If file type is an image show the thumbnail
		                $src = HT_SERVER . IMAGE_URL . '/Uploads/' . $file;
		            }
		            else
		            {
		                if( in_array( $file_type, array( 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ) ) )
		                {
		                    //-- Word file type
		                    $src = HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-doc.png';
		                }
		                elseif( $file_type == 'application/pdf' )
		                {
		                    //-- PDF file type
		                    $src = HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-pdf.png';
		                }
		                elseif( $file_type == 'image/svg+xml' )
		                {
		                    //-- SVG file type
		                    $src = HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-svg.png';
		                }
		                else
		                {
		                    //-- Other file type
		                    $src = HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-file.png';
		                }
		            }

		            echo json_encode( array( 'result' => 'success', 'src' => $src, 'id' => $id ) );
		        }
		    }
		    else
		    {
		        $upload->delete_thumb( $file );

		        echo json_encode( array( 'result' => 'failed' ) );
		    }
	    }
	    else
	    {
	        echo json_encode( array( 'result' => 'not-supported' ) );
	    }
	}

	exit;
}

?>