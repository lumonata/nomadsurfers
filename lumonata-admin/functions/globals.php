<?php

use Dompdf\Dompdf;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\OAuth;
use League\OAuth2\Client\Provider\Google;

class globalAdmin extends db
{
    function __construct()
    {
        parent::__construct();
    }

    function getPosts( $conditions = array(), $fetch = true, $operator = 'AND' )
    {
        //-- PREPARE parameter
        $w = array();

        if( is_array( $conditions ) && empty( $conditions ) === false )
        {
            foreach( $conditions as $field => $value )
            {
                if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
                {
                    $field .= ' = ';
                }

                $w[] = parent::prepare_query( $field . '%s', $value );
            }
        }

        if( empty( $w ) === false )
        {
            $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
        }
        else
        {
            $where = '';
        }

        $data = array();

        $r = parent::query( 'SELECT * FROM lumonata_post' . $where );

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_assoc( $r ) )
            {
                //-- GET additional field
                $sa = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lapp_id = %d AND a.lmodule_id = ( SELECT a2.lmodule_id FROM lumonata_module AS a2 WHERE a2.lapps = %s )';
                $qa = parent::prepare_query( $sa, $d[ 'lpost_id' ], $d[ 'ltype' ] );
                $ra = parent::query( $qa );

                if( parent::num_rows( $ra ) > 0 )
                {
                    while( $da = parent::fetch_assoc( $ra ) )
                    {
                        $d[ $da[ 'ladditional_key' ] ] = $da[ 'ladditional_value' ];
                    }
                }

                //-- GET term field
                $st = 'SELECT b.* FROM lumonata_post_relationship AS a LEFT JOIN lumonata_post_terms AS b ON a.lterm_id = b.lterm_id WHERE a.lpost_id = %d AND b.lgroup = %s';
                $qt = parent::prepare_query( $st, $d[ 'lpost_id' ], $d[ 'ltype' ] );
                $rt = parent::query( $qt );

                if( parent::num_rows( $rt ) > 0 )
                {
                    while( $dt = parent::fetch_assoc( $rt ) )
                    {
                        //-- GET term additional field
                        $sf = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lterm_id = %d';
                        $qf = parent::prepare_query( $sf, $dt[ 'lterm_id' ] );
                        $rf = parent::query( $qf );

                        if( parent::num_rows( $rf ) > 0 )
                        {
                            while( $df = parent::fetch_assoc( $rf ) )
                            {
                                $dt[ $df[ 'ladditional_key' ] ] = $df[ 'ladditional_value' ];
                            }
                        }

                        $d[ $dt[ 'lrule' ] ][] = $dt;
                    }
                }

                if( $fetch )
                {
                    return $d;

                    break;
                }

                $data[] = $d;
            }
        }

        return $data;
    }

    function getPostTerms( $conditions = array(), $fetch = true, $operator = 'AND' )
    {
        //-- PREPARE parameter
        $w = array();

        if( is_array( $conditions ) && empty( $conditions ) === false )
        {
            foreach( $conditions as $field => $value )
            {
                if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
                {
                    $field .= ' = ';
                }

                $w[] = parent::prepare_query( $field . '%s', $value );
            }
        }

        if( empty( $w ) === false )
        {
            $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
        }
        else
        {
            $where = '';
        }

        $data = array();

        $r = parent::query( 'SELECT * FROM lumonata_post_terms' . $where );

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_assoc( $r ) )
            {
                //-- GET additional field
                $sa = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lterm_id = %d';
                $qa = parent::prepare_query( $sa, $d[ 'lterm_id' ] );
                $ra = parent::query( $qa );

                if( parent::num_rows( $ra ) > 0 )
                {
                    while( $da = parent::fetch_assoc( $ra ) )
                    {
                        $d[ $da[ 'ladditional_key' ] ] = $da[ 'ladditional_value' ];
                    }
                }

                if( $fetch )
                {
                    return $d;

                    break;
                }

                $data[] = $d;
            }
        }

        return $data;
    }

    function getBooking( $booking_id = null )
    {
        $s = 'SELECT * FROM lumonata_booking AS a WHERE a.lbooking_id = %d';
        $q = parent::prepare_query( $s, $booking_id );
        $r = parent::query( $q );

        $booking = array();

        if( parent::num_rows( $r ) > 0 )
        {
            $d = parent::fetch_assoc( $r );

            $d[ 'lposts_data' ] = $this->getPosts( array( 
                'lpost_id' => $d[ 'lpost_id' ] 
            ));

            if( empty( $d[ 'ltrips' ] ) === false )
            {
                $d[ 'ltrips' ] = json_decode( $d[ 'ltrips' ], true );
            }

            if( empty( $d[ 'lrooms' ] ) === false )
            {
                $d[ 'lrooms' ] = json_decode( $d[ 'lrooms' ], true );
            }

            if( empty( $d[ 'laddons' ] ) === false )
            {
                $d[ 'laddons' ] = json_decode( $d[ 'laddons' ], true );
            }

            if( empty( $d[ 'lguests' ] ) === false )
            {
                $d[ 'lguests' ] = json_decode( $d[ 'lguests' ], true );
            }

            $booking = $d;
        }

        return $booking;
    }

    function getSchedule( $conditions = array(), $fetch = true, $order_by = array(), $limit = 0, $operator = 'AND' )
    {
        //-- PREPARE parameter
        $w = array();

        if( is_array( $conditions ) && empty( $conditions ) === false )
        {
            foreach( $conditions as $field => $value )
            {
                if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
                {
                    $field .= ' = ';
                }

                $w[] = parent::prepare_query( $field . '%s', $value );
            }
        }

        if( empty( $w ) === false )
        {
            $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
        }
        else
        {
            $where = '';
        }

        if( empty( $order_by ) === false )
        {
            if( array_key_exists( 0, $order_by ) )
            {
                $order = ' ORDER BY '. implode( ', ', $order_by );
            }
            else
            {
                $order = ' ORDER BY ' . str_replace( '=', ' ', http_build_query( $order_by, null, ',' ) );
            }
        }

        if( empty( $limit ) === false )
        {
            if( is_array( $limit ) )
            {
                $view = ' LIMIT '. implode( ', ', $limit );
            }
            else
            {
                $view = ' LIMIT '. $limit;
            }
        }

        $data = array();

        $r = parent::query( 'SELECT * FROM lumonata_schedules' . $where . $order . $view );

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_assoc( $r ) )
            {
                if( $fetch )
                {
                    return $d;

                    break;
                }

                $data[] = $d;
            }
        }

        return $data;
    }

    function getPackages( $conditions = array(), $fetch = true, $order_by = array(), $limit = 0, $operator = 'AND' )
    {
        //-- PREPARE parameter
        $w = array();

        if( is_array( $conditions ) && empty( $conditions ) === false )
        {
            foreach( $conditions as $field => $value )
            {
                if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
                {
                    $field .= ' = ';
                }

                $w[] = parent::prepare_query( $field . '%s', $value );
            }
        }

        if( empty( $w ) === false )
        {
            $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
        }
        else
        {
            $where = '';
        }

        if( empty( $order_by ) === false )
        {
            if( array_key_exists( 0, $order_by ) )
            {
                $order = ' ORDER BY '. implode( ', ', $order_by );
            }
            else
            {
                $order = ' ORDER BY ' . str_replace( '=', ' ', http_build_query( $order_by, null, ',' ) );
            }
        }

        if( empty( $limit ) === false )
        {
            if( is_array( $limit ) )
            {
                $view = ' LIMIT '. implode( ', ', $limit );
            }
            else
            {
                $view = ' LIMIT '. $limit;
            }
        }

        $data = array();

        $r = parent::query( 'SELECT * FROM lumonata_packages' . $where . $order . $view );

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_assoc( $r ) )
            {
                if( $fetch )
                {
                    return $d;

                    break;
                }

                $data[] = $d;
            }
        }

        return $data;
    }

    function getFields( $table, $select = array(), $conditions = array(), $fetch = true, $operator = 'AND' )
    {
        //-- PREPARE parameter
        $w = array();

        if( is_array( $conditions ) && empty( $conditions ) === false )
        {
            foreach( $conditions as $field => $value )
            {
                if( is_null( $value ) )
                {
                    $w[] = sprintf( $field . ' %s', 'IS NULL' );
                }
                else
                {
                    if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
                    {
                        $field .= ' = ';
                    }

                    $w[] = parent::prepare_query( $field . '%s', $value );
                }
            }
        }

        if( empty( $w ) === false )
        {
            $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
        }
        else
        {
            $where = '';
        }

        if( empty( $select ) )
        {
            $fields = '*';
        }
        else
        {
            if( is_array( $select ) )
            {
                $fields = implode( ',', $select );
            }
            else
            {
                $fields = $select;
            }
        }
        
        $r = parent::query( 'SELECT ' . $fields . ' FROM ' . $table . $where );

        if( $fetch === true )
        {
            $d = array();

            if( parent::num_rows( $r ) > 0 )
            {
                $d = parent::fetch_assoc( $r );
            }

            if( $fields != '*' && is_string( $select ) )
            {
                if( isset( $d[ $select ] ) )
                {
                    return $d[ $select ];
                }
                else
                {
                    return '';
                }
            }
            else
            {
                return $d;
            }
        }
        else
        {
            return $r;
        }
    }

    function getRowsNumber( $table, $conditions = array(), $operator = 'AND' )
    {
        //-- PREPARE parameter
        $w = array();

        if( is_array( $conditions ) && empty( $conditions ) === false )
        {
            foreach( $conditions as $field => $value )
            {
                if( !preg_match('/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
                {
                    $field .= ' = ';
                }

                $w[] = parent::prepare_query( $field . '%s', $value );
            }
        }

        if( empty( $w ) === false )
        {
            $where = ' WHERE '. implode( ' ' . $operator . ' ', $w );
        }
        else
        {
            $where = '';
        }

        $r = parent::query( 'SELECT * FROM ' . $table . $where );

        if( is_array( $r ) === false )
        {
            return parent::num_rows( $r );
        }
        else
        {
            return  0;
        }
    }

    function getStartPrice( $post_data )
    {
        if( isset( $post_data[ 'room_type' ] ) && !empty( $post_data[ 'room_type' ] ) )
        {
            $stack = array();

            foreach( $post_data[ 'room_type' ] as $d )
            {
                $stack[] = $this->roomPrice( $post_data, $d );
            }

            return min( $stack );
        }
        else if( isset( $post_data[ 'room_price' ] ) && !empty( $post_data[ 'room_price' ] ) )
        {
            return $post_data[ 'room_price' ];
        }
        else
        {
            return 0;
        }
    }

    function getSwitcherLanguage()
    {

    }

    function getSettingValue( $option_name = '', $app_name = 'global_setting' )
    {
        $d = $this->getPosts( array( 'ltype' => $app_name ) );

        if( isset( $d[ $option_name ] ) && $option_name != '' )
        {
            return $d[ $option_name ];
        }
        else
        {
            return $d;
        }
    }

    function getUserSession()
    {
        $sess = array( 'userid' => null, 'username' => null, 'usertype' => null );

        if( isset( $_SESSION[ SESSION_NAME ] ) )
        {
            $prm = json_decode( base64_decode( $_SESSION[ SESSION_NAME ] ), true );

            if( $prm !== null && json_last_error() === JSON_ERROR_NONE )
            {        
                $sess = $prm;
            }
        }

        return $sess;
    }

    function getAgentSession()
    {
        $sess = array( 'userid' => null, 'username' => null, 'usertype' => null );

        if( isset( $_SESSION[ AGENT_SESSION_NAME ] ) )
        {
            $prm = json_decode( base64_decode( $_SESSION[ AGENT_SESSION_NAME ] ), true );

            if( $prm !== null && json_last_error() === JSON_ERROR_NONE )
            {        
                $sess = $prm;
            }
        }

        return $sess;
    }

    function getMemberSession()
    {
        $sess = array( 'userid' => null, 'username' => null, 'usertype' => null );

        if( isset( $_SESSION[ MEMBER_SESSION_NAME ] ) )
        {
            $prm = json_decode( base64_decode( $_SESSION[ MEMBER_SESSION_NAME ] ), true );

            if( $prm !== null && json_last_error() === JSON_ERROR_NONE )
            {        
                $sess = $prm;
            }
        }

        return $sess;
    }

    function getCurrentSession()
    {
        $path = explode( '/', ltrim( $_SERVER['REQUEST_URI'], '/' ) );

        if( in_array( 'lumonata-admin', $path ) )
        {
            return $this->getUserSession();
        }
        elseif( in_array( 'agent', $path ) )
        {
            return $this->getAgentSession();
        }
        elseif( in_array( 'member', $path ) )
        {
            return $this->getMemberSession();
        }
    }

    function getCurrentLanguageCode()
    {
        $path = explode( '/', ltrim( $_SERVER['REQUEST_URI'], '/' ) );

        foreach( array_keys( $path, 'nomad', true ) as $key )
        {
            unset( $path[ $key ] );
        }

        $mod = array_shift( $path );

        if( $this->isTranslation( $mod ) && isset( $path[ 0 ] ) )
        {
            $lang = $this->getFields( 'lumonata_language', 'llang_code', array( 'llang_code' => $mod ) );
        }
        else
        {
            $lang = $this->getFields( 'lumonata_language', 'llang_code', array( 'llang_id' => $this->getSettingValue( 'llanguage' ) ) );
        }

        return $lang;
    }

    function getAgentSidebarMenu( $data )
    {
        $tpml = $this->getFields( 'lumonata_additional_field', 'ladditional_value', array( 'ladditional_key' => 'page_template', 'lapp_id' => $data[ 'id' ] ) );
        $ptyp = $this->getFields( 'lumonata_additional_field', 'ladditional_value', array( 'ladditional_key' => 'price_type', 'lapp_id' => $data[ 'id' ] ) );

        if( $data[ 'type' ] == 'general'  )
        {
            $glink  = '';
            $gclass = ' actv';
        }
        else
        {
            $glink  = sprintf( '//%s/agent/listings/edit/?%s', SITE_URL, http_build_query( array_merge( $data, array( 'type' => 'general' ) ) ) );
            $gclass = '';
        }

        if( $data[ 'type' ] == 'details'  )
        {
            $dlink  = '';
            $dclass = ' actv';
        }
        else
        {
            $dlink  = sprintf( '//%s/agent/listings/edit/?%s', SITE_URL, http_build_query( array_merge( $data, array( 'type' => 'details' ) ) ) );
            $dclass = '';
        }

        if( $data[ 'type' ] == 'activity'  )
        {
            $alink  = '';
            $aclass = ' actv';
        }
        else
        {
            $alink  = sprintf( '//%s/agent/listings/edit/?%s', SITE_URL, http_build_query( array_merge( $data, array( 'type' => 'activity' ) ) ) );
            $aclass = '';
        }

        if( $data[ 'type' ] == 'review'  )
        {
            $rlink  = '';
            $rclass = ' actv';
        }
        else
        {
            $rlink  = sprintf( '//%s/agent/listings/edit/?%s', SITE_URL, http_build_query( array_merge( $data, array( 'type' => 'review' ) ) ) );
            $rclass = '';
        }

        if( $data[ 'type' ] == 'settings'  )
        {
            $slink  = '';
            $sclass = ' actv';
        }
        else
        {
            $slink  = sprintf( '//%s/agent/listings/edit/?%s', SITE_URL, http_build_query( array_merge( $data, array( 'type' => 'settings' ) ) ) );
            $sclass = '';
        }

        if( $tpml == '0' )
        {
            $amenu = array(
                array(
                    'label'     => 'Rooms',
                    'permalink' => sprintf( '%s#section-rooms', $dlink ),
                ),
                array(
                    'label'     => 'Packages',
                    'permalink' => sprintf( '%s#section-package', $dlink ),
                )
            );
        }
        elseif( $tpml == '1' )
        {
            $amenu = array(
                array(
                    'label'     => 'Cabins',
                    'permalink' => sprintf( '%s#section-rooms', $dlink ),
                ),
                array(
                    'label'     => 'Timetable',
                    'permalink' => sprintf( '%s#section-schedule', $dlink ),
                )
            );
        }
        elseif( $tpml == '2' )
        {
            $amenu = array(
                array(
                    'label'     => 'Rooms',
                    'permalink' => sprintf( '%s#section-rooms', $dlink ),
                ),
                array(
                    'label'     => 'Packages',
                    'permalink' => sprintf( '%s#section-package', $dlink ),
                )
            );
        }
        elseif( $tpml == '3' )
        {
            $amenu = array(
                array(
                    'label'     => 'Venue',
                    'permalink' => sprintf( '%s#section-venue', $dlink ),
                ),
                array(
                    'label'     => 'Packages',
                    'permalink' => sprintf( '%s#section-package', $dlink ),
                )
            );
        }

        $menu = array(
            array(
                'label'     => 'General',
                'permalink' => 'javascript:;',
                'class'     => $gclass,
                'submenu'   => array(
                    array(
                        'label'     => 'Photos',
                        'permalink' => sprintf( '%s#section-photos', $glink ),
                    ),                    
                    array(
                        'label'     => 'Description',
                        'permalink' => sprintf( '%s#section-description', $glink ),
                    ),                    
                    array(
                        'label'     => 'Hightlights',
                        'permalink' => sprintf( '%s#section-hightlights', $glink ),
                    ),                    
                    array(
                        'label'     => 'Location',
                        'permalink' => sprintf( '%s#section-location', $glink ),
                    )
                )
            ),
            array(
                'label'     => 'Listing Details',
                'permalink' => 'javascript:;',
                'class'     => $dclass,
                'submenu'   => array_merge( $amenu, array(
                    array(
                        'label'     => 'Add-ons',
                        'permalink' => sprintf( '%s#section-add-ons', $dlink ),
                    ),                    
                    array(
                        'label'     => 'Type of Surf',
                        'permalink' => sprintf( '%s#section-type-of-surf', $dlink ),
                    ),                    
                    array(
                        'label'     => 'Amenities',
                        'permalink' => sprintf( '%s#section-amenities', $dlink ),
                    ),                    
                    array(
                        'label'     => 'What\'s Included',
                        'permalink' => sprintf( '%s#section-whats-included', $dlink ),
                    ),                    
                    array(
                        'label'     => 'Food',
                        'permalink' => sprintf( '%s#section-food', $dlink ),
                    )
                ))
            ),
            array(
                'label'     => 'Activity',
                'permalink' => 'javascript:;',
                'class'     => $aclass,
                'submenu'   => array(
                    array(
                        'label'     => 'Things To Do',
                        'permalink' => sprintf( '%s#things-to-do', $alink ),
                    ),                    
                    array(
                        'label'     => 'Surfing Information',
                        'permalink' => sprintf( '%s#surfing-information', $alink ),
                    )
                )
            ),
            array(
                'label'     => 'Reviews',
                'permalink' => $rlink,
                'class'     => $rclass
            ),
            array(
                'label'     => 'Booking Setings',
                'permalink' => $slink,
                'class'     => $sclass
            )
        );

        return $menu;
    }

    function getAgentSidebarMenuContent( $data )
    {
        $content =
        '<ul>';
            
            $menus = $this->getAgentSidebarMenu( $data );

            foreach( $menus as $menu )
            {
                $content .= '
                <li>
                    <a href="' . $menu[ 'permalink' ] . '" class="pmh_' . $menu[ 'class' ] . '">
                        ' . $menu[ 'label' ];

                        if( isset( $menu[ 'submenu' ] ) )
                        {
                            $content .= '
                            <img src="//' . MODULES_THEME_URL . '/asset/img/arw.svg">';
                        }

                        $content .= '
                    </a>';

                    if( isset( $menu[ 'submenu' ] ) )
                    {
                        $content .= '
                        <div class="ddnav' . $menu[ 'class' ] . '">
                            <ul>';

                                foreach( $menu[ 'submenu' ] as $smenu )
                                {
                                    $content .= '
                                    <li>
                                        <a href="' . $smenu[ 'permalink' ] . '">' . $smenu[ 'label' ] . '</a>
                                    </li>';
                                }

                                $content .= '
                            </ul>
                        </div>';
                    }

                    $content .= '
                </li>';
            }

            $content .=
        '</ul>';

        return $content;
    }

    function getAddUrl( $mod, $usertype )
    {
        if( $usertype == 2 )
        {
            //-- FOR agent panel
            $apps = str_replace( '_', '-', $this->getApps( $mod ) );
            
            return HT_SERVER . SITE_URL . '/agent/' . $apps . '/add/';
        }
        else
        {
            //-- FOR admin panel
            return HT_SERVER . ADMIN_URL . '/home.php?mod=' . $mod . '&prc=new';
        }
    }

    function getViewUrl( $mod, $usertype )
    {
        if( $usertype == 2 )
        {
            //-- FOR agent panel
            $apps = str_replace( '_', '-', $this->getApps( $mod ) );
            
            return HT_SERVER . SITE_URL . '/agent/' . $apps . '/';
        }
        else
        {
            //-- FOR admin panel
            return HT_SERVER . ADMIN_URL . '/home.php?mod=' . $mod . '&prc=view';
        }
    }

    function getAjaxUrl( $mod, $usertype )
    {
        if( $usertype == 2 )
        {
            //-- FOR agent panel
            $apps = str_replace( '_', '-', $this->getAppsAgent( $mod ) );

            return HT_SERVER . SITE_URL . '/agent/' . $apps . '/request/';
        }
        elseif( $usertype == 4 )
        {
            //-- FOR member panel
            $apps = str_replace( '_', '-', $this->getAppsMember( $mod ) );

            return HT_SERVER . SITE_URL . '/member/' . $apps . '/request/';
        }
        else
        {
            //-- FOR admin panel
            return HT_SERVER . ADMIN_URL . '/home.php?mod=' . $mod . '&prc=request';
        }
    }

    function getEditUrl( $mod, $usertype, $id, $ref = null )
    {
        if( $usertype == 2 )
        {
            //-- FOR agent panel
            $prm  = array_filter( array( 'id' => $id, 'ref' => $ref ) );
            $apps = str_replace( '_', '-', $this->getApps( $mod ) );

            return HT_SERVER . SITE_URL . '/agent/' . $apps . '/edit/?' . http_build_query( $prm );
        }
        else
        {
            //-- FOR admin panel
            $prm  = array_filter( array( 'mod' => $mod, 'prc' => 'edit', 'id' => $id, 'ref' => $ref ) );

            return HT_SERVER . ADMIN_URL . '/home.php?' . http_build_query( $prm );
        }
    }

    function getDuplicateUrl( $mod, $usertype, $id, $ref = null )
    {
        if( $usertype == 2 )
        {
            //-- FOR agent panel
            $prm  = array_filter( array( 'id' => $id, 'ref' => $ref ) );
            $apps = str_replace( '_', '-', $this->getApps( $mod ) );

            return HT_SERVER . SITE_URL . '/agent/' . $apps . '/duplicate/?' . http_build_query( $prm );
        }
        else
        {
            //-- FOR admin panel
            $prm  = array_filter( array( 'mod' => $mod, 'prc' => 'duplicate', 'id' => $id, 'ref' => $ref ) );

            return HT_SERVER . ADMIN_URL . '/home.php?' . http_build_query( $prm );
        }
    }

    function getPermalink( $data, $type )
    {
        $lang_id   = $data[ 'llang_id' ];
        $lang_code = $this->getFields( 'lumonata_language', 'llang_code', array( 'llang_id' => $lang_id ) );
        $lang_def  = $this->getSettingValue( 'llanguage' );

        if( $lang_id == $lang_def )
        {
            return sprintf( '%s%s/%s/%s/', HT_SERVER, SITE_URL, $type, $data[ 'lsef_url' ] );
        }
        else
        {
            return sprintf( '%s%s/%s/%s/%s/', HT_SERVER, SITE_URL, $lang_code, $type, $data[ 'lsef_url' ] );
        }
    }

    function getPaymentUrl( $id )
    {
        return HT_SERVER . SITE_URL . '/booking/payment/' . base64_encode( json_encode( array( 'id' => $id, 'expired' => strtotime('+48 hours') ) ) );
    }

    function getCalendarUrl( $mod, $usertype, $id )
    {
        if( $usertype == 2 )
        {
            //-- FOR agent panel
            $apps = str_replace( '_', '-', $this->getApps( $mod ) );

            return HT_SERVER . SITE_URL . '/agent/' . $apps . '/calendar/?id=' . $id;
        }
        else
        {
            //-- FOR admin panel
            return HT_SERVER . ADMIN_URL . '/home.php?mod=' . $mod . '&prc=calendar&id=' . $id;
        }
    }
    
    function getMetaTitle()
    {
        if( isset( $this->metaTitle ) )
        {
            return $this->metaTitle;
        }
    }

    function getMetaKeywords()
    {
        if( isset( $this->metaKeywords ) )
        {
            return $this->metaKeywords;
        }
    }

    function getMetaDescriptions()
    {
        if( isset( $this->metaDescriptions ) )
        {
            return $this->metaDescriptions;
        }
    }
    
    function setMetaTitle( $metaTitle = '' )
    {
        if( empty( $metaTitle ) )
        {
            $this->metaTitle = $this->getSettingValue( 'meta_title' );
        }
        else
        {
            $this->metaTitle = $metaTitle;
        }
    }

    function setMetaKeywords( $metaKeywords = '' )
    {
        if( empty( $metaKeywords ) )
        {
            $this->metaKeywords = '';
        }
        else
        {
            $this->metaKeywords = $metaKeywords;
        }
    }

    function setMetaDescriptions( $metaDescriptions = '' )
    {
        if( empty( $metaDescriptions ) )
        {
            $this->metaDescriptions = $metaDescriptions;
        }
        else
        {
            $this->metaDescriptions = '';
        }
    }
    
    function setError( $msg = NO_AUTHO )
    {
        return '
        <div class="error">
            ' . $msg . '
            <input type="button" name="Button" class="button_yesno" value="Back" onClick="javascript:history.back();">
        </div>';
    }

    function getModule( $mod )
    {
        $s = 'SELECT * FROM lumonata_module WHERE lmodule_id = %d';
        $q = parent::prepare_query( $s, $mod );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $d = parent::fetch_array( $r );

            return sprintf( '%s/%s', $d[ 'lfolder' ], $d[ 'lapps' ] );
        }
    }
    
    function getModuleID( $apps )
    {
        $s = 'SELECT * FROM lumonata_module WHERE lapps = %s';
        $q = parent::prepare_query( $s, $apps );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $d = parent::fetch_array( $r );

            return $d[ 'lmodule_id' ];
        }
    }
    
    function getApps( $mod )
    {
        $s = 'SELECT * FROM lumonata_module WHERE lmodule_id = %d';
        $q = parent::prepare_query( $s, $mod );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $d = parent::fetch_array( $r );

            return $d[ 'lapps' ];
        }
    }
    
    function getAppsAgent( $mod )
    {
        $s = 'SELECT * FROM lumonata_module WHERE lmodule_id = %d';
        $q = parent::prepare_query( $s, $mod );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $d = parent::fetch_array( $r );

            return $d[ 'lappsagent' ];
        }
    }
    
    function getAppsMember( $mod )
    {
        $s = 'SELECT * FROM lumonata_module WHERE lmodule_id = %d';
        $q = parent::prepare_query( $s, $mod );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $d = parent::fetch_array( $r );

            return $d[ 'lappsmember' ];
        }
    }

    function getImageFolder( $mod )
    {
        $s = 'SELECT * FROM lumonata_module WHERE lmodule_id = %d';
        $q = parent::prepare_query( $s, $mod );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $d = parent::fetch_array( $r );

            return $d[ 'limage_folder' ];
        }
    }

    function getPView( $mod, $usertype_id )
    {
        $query  = parent::prepare_query( "SELECT lview FROM lumonata_user_permissions WHERE lmodule_id=%d AND lusertype_id=%d", $mod, $usertype_id );
        $result = parent::query( $query );
        $data   = parent::fetch_array( $result );

        return ( $data[ 'lview' ] == 1 ) ? TRUE : FALSE;
    }
    
    function getPViewP( $mod, $usertype_id )
    {
        $query  = parent::prepare_query( "SELECT lview FROM lumonata_user_permissions WHERE lmodule_id=%d AND lusertype_id=%d", $mod, $usertype_id );
        $result = parent::query( $query );
        $data   = parent::fetch_array( $result );

        return ( $data[ 'lview' ] == 1 ) ? TRUE : FALSE;
    }
    
    function getPInsert( $mod, $usertype_id )
    {
        $query  = parent::prepare_query( "SELECT linsert FROM lumonata_user_permissions WHERE lmodule_id=%d AND lusertype_id=%d", $mod, $usertype_id );
        $result = parent::query( $query );
        $data   = parent::fetch_array( $result );

        return ( $data[ 'linsert' ] == 1 ) ? TRUE : FALSE;
    }
    
    function getPEdit( $mod, $usertype_id )
    {
        $query  = parent::prepare_query( "SELECT ledit FROM lumonata_user_permissions WHERE lmodule_id=%d AND lusertype_id=%d", $mod, $usertype_id );
        $result = parent::query( $query );
        $data   = parent::fetch_array( $result );

        return ( $data[ 'ledit' ] == 1 ) ? TRUE : FALSE;
    }
    
    function getPDelete( $mod, $usertype_id )
    {
        $query  = parent::prepare_query( "SELECT ldelete FROM lumonata_user_permissions WHERE lmodule_id=%d AND lusertype_id=%d", $mod, $usertype_id );
        $result = parent::query( $query );
        $data   = parent::fetch_array( $result );

        return ( $data[ 'ldelete' ] == 1 ) ? TRUE : FALSE;
    }
    
    function getPPublish( $mod, $usertype_id )
    {
        $query  = parent::prepare_query( "SELECT lpublish FROM lumonata_user_permissions WHERE lmodule_id=%d AND lusertype_id=%d", $mod, $usertype_id );
        $result = parent::query( $query );
        $data   = parent::fetch_array( $result );

        return ( $data[ 'lpublish' ] == 1 ) ? TRUE : FALSE;
    }
    
    function getPViewOwn( $mod, $usertype_id )
    {
        $query  = parent::prepare_query( "SELECT lview_own FROM lumonata_user_permissions WHERE lmodule_id=%d AND lusertype_id=%d", $mod, $usertype_id );
        $result = parent::query( $query );
        $data   = parent::fetch_array( $result );

        return ( $data[ 'lview_own' ] == 1 ) ? TRUE : FALSE;
    }
    
    function getPViewOwnP( $mod, $usertype_id )
    {
        $query  = parent::prepare_query( "SELECT lview_own FROM lumonata_user_permissions WHERE lmodule_id=%d AND lusertype_id=%d", $mod, $usertype_id );
        $result = parent::query( $query );
        $data   = parent::fetch_array( $result );

        return ( $data[ 'lview_own' ] == 1 ) ? TRUE : FALSE;
    }
    
    function getPInsertOwn( $mod, $usertype_id )
    {
        $query  = parent::prepare_query( "SELECT linsert_own FROM lumonata_user_permissions WHERE lmodule_id=%d AND lusertype_id=%d", $mod, $usertype_id );
        $result = parent::query( $query );
        $data   = parent::fetch_array( $result );

        return ( $data[ 'linsert_own' ] == 1 ) ? TRUE : FALSE;
    }
    
    function getPEditOwn( $mod, $usertype_id )
    {
        $query  = parent::prepare_query( "SELECT ledit_own FROM lumonata_user_permissions WHERE lmodule_id=%d AND lusertype_id=%d", $mod, $usertype_id );
        $result = parent::query( $query );
        $data   = parent::fetch_array( $result );

        return ( $data[ 'ledit_own' ] == 1 ) ? TRUE : FALSE;
    }
    
    function getPDeleteOwn( $mod, $usertype_id )
    {
        $query  = parent::prepare_query( "SELECT ldelete_own FROM lumonata_user_permissions WHERE lmodule_id=%d AND lusertype_id=%d", $mod, $usertype_id );
        $result = parent::query( $query );
        $data   = parent::fetch_array( $result );

        return ( $data[ 'ldelete_own' ] == 1 ) ? TRUE : FALSE;
    }
    
    function getPPublishOwn( $mod, $usertype_id )
    {
        $query  = parent::prepare_query( "SELECT lpublish_own FROM lumonata_user_permissions WHERE lmodule_id=%d AND lusertype_id=%d", $mod, $usertype_id );
        $result = parent::query( $query );
        $data   = parent::fetch_array( $result );

        return ( $data[ 'lpublish_own' ] == 1 ) ? TRUE : FALSE;
    }
    
    function getPViewButton( $mod, $usertype_id )
    {
        return ( $this->getPView( $mod, $usertype_id ) || $this->getPViewOwn( $mod, $usertype_id ) ) ? 1 : 0;
    }
    
    function getPViewButtonP( $mod, $usertype_id )
    {
        return ( $this->getPViewP( $mod, $usertype_id ) || $this->getPViewOwnP( $mod, $usertype_id ) ) ? 1 : 0;
    }
    
    function getPInsertButton( $mod, $usertype_id )
    {
        return ( $this->getPInsert( $mod, $usertype_id ) || $this->getPInsertOwn( $mod, $usertype_id ) ) ? 1 : 0;
    }
    
    function getPEditButton( $mod, $usertype_id )
    {
        return ( $this->getPEdit( $mod, $usertype_id ) || $this->getPEditOwn( $mod, $usertype_id ) ) ? 1 : 0;
    }
    
    function getPDeleteButton( $mod, $usertype_id )
    {
        return ( $this->getPDelete( $mod, $usertype_id ) || $this->getPDeleteOwn( $mod, $usertype_id ) ) ? 1 : 0;
    }
    
    function getPPublishButton( $mod, $usertype_id )
    {
        return ( $this->getPPublish( $mod, $usertype_id ) || $this->getPPublishOwn( $mod, $usertype_id ) ) ? 1 : 0;
    }
    
    function getPViewBoth( $mod, $usertype_id )
    {
        return ( $this->getPView( $mod, $usertype_id ) || $this->getPViewOwn( $mod, $usertype_id ) ) ? TRUE : FALSE;
    }
    
    function getPViewBothP( $mod, $usertype_id )
    {
        return ( $this->getPViewP( $mod, $usertype_id ) || $this->getPViewOwnP( $mod, $usertype_id ) ) ? TRUE : FALSE;
    }
    
    function getPInsertBoth( $mod, $usertype_id )
    {
        return ( $this->getPInsert( $mod, $usertype_id ) || $this->getPInsertOwn( $mod, $usertype_id ) ) ? TRUE : FALSE;
    }
    
    function getPEditBoth( $mod, $usertype_id )
    {
        return ( $this->getPEdit( $mod, $usertype_id ) || $this->getPEditOwn( $mod, $usertype_id ) ) ? TRUE : FALSE;
    }
    
    function getPDeleteBoth( $mod, $usertype_id )
    {
        return ( $this->getPDelete( $mod, $usertype_id ) || $this->getPDeleteOwn( $mod, $usertype_id ) ) ? TRUE : FALSE;
    }
    
    function getPPublishBoth( $mod, $usertype_id )
    {
        return ( $this->getPPublish( $mod, $usertype_id ) || $this->getPPublishOwn( $mod, $usertype_id ) ) ? TRUE : FALSE;
    }

    function setOrderID( $table, $order_id )
    {
        $query  = parent::prepare_query( "UPDATE " . $table . " SET lorder_id = lorder_id + 1 WHERE lorder_id >= %d", $order_id );
        $result = parent::query( $query );

        if( is_array( $result ) )
        {
        	return false;
        }
        else
        {
        	return true;
        }
    }
    
    function setOrderID2( $table, $order_id, $field, $id )
    {
        $query  = parent::prepare_query( "UPDATE " . $table . " SET lorder_id = lorder_id + 1 WHERE lorder_id >= %d And $field <> %d", $order_id, $id );
        $result = parent::query( $query );

        if( is_array( $result ) )
        {
        	return false;
        }
        else
        {
        	return true;
        }
    }

    function getValueField( $table, $field_get, $field_cond, $id )
    {
        $query  = parent::prepare_query( "SELECT " . $field_get . " FROM " . $table . " WHERE " . $field_cond . "=%s", $id );
        $result = parent::query( $query );
        $data   = parent::fetch_array( $result );

        return $data[ $field_get ];
    }

    function getValueField2( $table, $field_get, $field_cond, $id, $field_cond2, $id2 )
    {
        $query  = parent::prepare_query( "SELECT " . $field_get . " FROM " . $table . " WHERE " . $field_cond . "=%s AND " . $field_cond2 . "=%s", $id, $id2 );
        $result = parent::query( $query );
        $data   = parent::fetch_array( $result );

        return $data[ $field_get ];
    }

    function getValueField3( $table, $field_get, $field_cond, $id, $field_cond2, $id2, $field_cond3, $id3 )
    {
        $query  = parent::prepare_query( "SELECT " . $field_get . " FROM " . $table . " WHERE " . $field_cond . "=%s AND " . $field_cond2 . "=%s AND " . $field_cond3 . "=%s", $id, $id2, $id3 );
        $result = parent::query( $query );
        $data   = parent::fetch_array( $result );

        return $data[ $field_get ];
    }

    function getNumRow( $table )
    {
        $query  = parent::prepare_query( "SELECT * FROM " . $table );
        $result = parent::query( $query );

        return parent::num_rows( $result );
    }
    
    function getNumRows( $table, $field, $id )
    {
        $query  = parent::prepare_query( "SELECT * FROM " . $table . " WHERE " . $field . "=%s", $id );
        $result = parent::query( $query );

        return parent::num_rows( $result );
    }

    function getNumRows2( $table, $field, $id, $field2, $id2 )
    {
        $query  = parent::prepare_query( "SELECT * FROM " . $table . " WHERE " . $field . "=%s AND " . $field2 . "=%s", $id, $id2 );
        $result = parent::query( $query );

        return parent::num_rows( $result );
    }

    function getName( $usr )
    {
        $query  = parent::prepare_query( "SELECT lname FROM lumonata_user WHERE lusername =%s", $usr );
        $result = parent::query( $query );
        $data   = parent::fetch_array( $result );

        return $data[ 'lname' ];
    }
    
    function setCode( $table, $field )
    {
        $query  = parent::prepare_query( "SELECT MAX(" . $field . ") from " . $table . "" );
        $result = parent::query( $query );
        $data   = parent::fetch_array( $result );

        if( $data[ 0 ] == NULL )
        {
            return "1";
        }
        else
        {
            $cnt = $data[ 0 ] + 1;

            return $cnt;
        }
    }

    function isTranslation( $lang = '' )
    {
        $s = 'SELECT * FROM lumonata_language AS a WHERE a.lstatus = 1 AND a.llang_code = %s';
        $q = parent::prepare_query( $s, $lang );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function isInteger( $str )
    {
        return preg_match( "/^-?([0-9])+$/", $str );
    }

    function isFloat( $str )
    {
        return preg_match( "/^-?([0-9])+([\.|,]([0-9])*)?$/", $str );
    }

    function isAlpha( $str )
    {
        return preg_match( "/^[a-z]+$/i", $str );
    }

    function isAlphaNum( $str )
    {
        return preg_match( "/^[a-z0-9]*$/i", $str );
    }

    function isEmailAddress( $str )
    {
        if( eregi( "^([a-z0-9_-])+([\.a-z0-9_-])*@([a-z0-9-])+(\.[a-z0-9-]+)*\.([a-z]{2,6})$", $str ) )
        {
            list( $username, $domain ) = split( "@", $str );

            if( function_exists( "getmxrr" ) && !getmxrr( $domain, $MXHost ) )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    function isUrl( $str )
    {
        return preg_match( "/^(http|https|ftp):\/\/([a-z0-9]([a-z0-9_-]*[a-z0-9])?\.)+[a-z]{2,6}\/?([a-z0-9\?\._-~&#=+%]*)?/", $str );
    }
    
    function isAllowed( $content )
    {
        $pattern     = "/\b(@|www|WWW|http|hotmail|gmail|badword1|badword2|badword3)\b/i";
        $replacement = "";
        $content_new = preg_replace( $pattern, $replacement, $content );
        
        $pattern     = "/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i";
        $replacement = "";
        $content_new = preg_replace( $pattern, $replacement, $content_new );
        
        $pattern     = "/\b\(?\d{3}\)?[-\s.]?\d{3}[-\s.]\d{4}\b/i";
        $replacement = "";
        $content_new = preg_replace( $pattern, $replacement, $content_new );
        
        return $content_new;
    }
    
    function clear_phone_number( $content )
    {
        $pattern     = '!(\b\+?[0-9()\[\]./ -]{7,17}\b|\b\+?[0-9()\[\]./ -]{7,17}\s+(extension|x|#|-|code|ext)\s+[0-9]{1,6})!i';
        $replacement = "";
        $content_new = preg_replace( $pattern, $replacement, $content );
        
        return $content_new;
    }
    
    function clear_email( $content )
    {
        $pattern     = "/[^@\s]*@[^@\s]*\.[^@\s]*/";
        $replacement = "";
        $content_new = preg_replace( $pattern, $replacement, $content );
        
        return $content_new;
    }

    function is_phone_number( $content )
    {
        $pattern = "/\+?[0-9][0-9()-\s+]{4,20}[0-9]/";
        $exit    = preg_match( $pattern, $content );

        if( $exit )
        {
            return true;
        }
    }
    
    function object_to_array( $data )
    {
        if( is_array( $data ) || is_object( $data ) )
        {
            $result = array();

            foreach( $data as $key => $value )
            {
                $result[ $key ] = $this->object_to_array( $value );
            }

            return $result;
        }

        return $data;
    }

    function sef_url( $URL )
    {
        $karakter = array(
             "\\",
            "/",
            ":",
            "*",
            "?",
            "<",
            ">",
            "`",
            "~",
            "!",
            "@",
            "#",
            "$",
            "%",
            "^",
            "&",
            "(",
            ")",
            "_",
            "+",
            "=",
            "|",
            "}",
            "{",
            "[",
            "]",
            ";",
            "\"",
            "'",
            ".",
            " " 
        );
        //print_r($karakter);
        
        $sef_url = $URL;
        for( $i = 0; $i < count( $karakter ); $i++ )
        {
            $sef_url = str_replace( $karakter[ $i ], "-", $sef_url );
            $sef_url = str_replace( "--", "-", $sef_url );
        }
        $sef_url = str_replace( "--", "-", $sef_url );
        
        $karakter2nd = array(
             "," 
        );
        for( $i = 0; $i < count( $karakter2nd ); $i++ )
        {
            $sef_url = str_replace( $karakter2nd[ $i ], "-", $sef_url );
        }
        
        $strlen = strlen( $sef_url );
        if( substr( $sef_url, -1 ) == "-" )
        {
            $sef_url = substr( $sef_url, 0, ( $strlen - 1 ) );
        }
        $strlen = strlen( $sef_url );
        if( substr( $sef_url, 0, 1 ) == "-" )
        {
            $sef_url = substr( $sef_url, 1, $strlen );
        }
        $sef_url = str_replace( "ä", "a", $sef_url );
        $sef_url = str_replace( "Ä", "A", $sef_url );
        $sef_url = str_replace( "é", "e", $sef_url );
        $sef_url = str_replace( "ö", "o", $sef_url );
        $sef_url = str_replace( "Ö", "O", $sef_url );
        $sef_url = str_replace( "ü", "u", $sef_url );
        $sef_url = str_replace( "Ü", "U", $sef_url );
        $sef_url = str_replace( "ß", "B", $sef_url );
        $sef_url = str_replace( "ä", "a", $sef_url );
        return strtolower( $sef_url );
    }
    
    function get_css( $url = '', $data = array(), $media = 'screen' )
    {
        if( empty( $url ) === false )
        {
            if( empty( $data ) )
            {
                return sprintf( '<link rel="stylesheet" href="%s" media="%s" />',  $url, $media );
            }
            else
            {
                return sprintf( '<link rel="stylesheet" href="%s" media="%s" %s />', $url, $media, implode(', ', array_map( function( $v, $k ){ return $k . '="' . $v . '"'; }, $data, array_keys( $data ) ) ) );
            }
        }
    }

    function get_jvs( $url = '', $data = array() )
    {
        if( empty( $url ) === false )
        {
            if( empty( $data ) )
            {
                return sprintf( '<script src="%s"></script>', $url );
            }
            else
            {
                return sprintf( '<script src="%s" %s></script>', $url, http_build_query( $data, null, ', ' ) );
            }
        }
    }

    function get_breadcrumber_field( $id, $table, $field_get, $field_id, $field_parent )
    {
        $q = parent::prepare_query( 'SELECT * FROM ' . $table );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) == 0 )
        {
            return array();
        }

        $data = array();

        while( $d = parent::fetch_array( $r ) )
        {
            $data[ $d[ $field_id ] ] = array(
                'id'        => $d[ $field_id ],
                'title'     => $d[ $field_get ],
                'parent_id' => $d[ $field_parent ]
            );
        }

        return array_reverse( $this->breadcrumber( $data, $id ) );
    }

    function get_recursive_field( $query, $field_get, $field_id, $field_parent )
    {
        $q = parent::prepare_query( $query );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) == 0 )
        {
            return array();
        }

        $data = array();

        while( $d = parent::fetch_array( $r ) )
        {
            $data[] = array(
                'id'        => $d[ $field_id ],
                'title'     => $d[ $field_get ],
                'parent_id' => $d[ $field_parent ]
            );
        }

        return $this->build_tree( $data );
    }

    function set_option( $q, $value, $label, $selected_id = '', $use_empty = true, $empty_value = '', $empty_label = 'Select option' )
    {
        $r = parent::query( $q );
        $o = array();

        if( $use_empty )
        {
            $o[] = sprintf( '<option value="%s">%s</option>', $empty_value, $empty_label );
        }

        if( !empty( $selected_id ) && !is_array( $selected_id ) )
        {
            $selected_id = array( $selected_id );
        }

        while( $d = parent::fetch_array( $r ) )
        {
            if( !empty( $selected_id ) && in_array( $d[ $value ], $selected_id ) )
            {
                $o[] = sprintf( '<option value="%s" selected>%s</option>', $d[ $value ], $d[ $label ] );
            }
            else
            {
                $o[] = sprintf( '<option value="%s">%s</option>', $d[ $value ], $d[ $label ] );
            }
        }

        return empty( $o ) ? '' : implode( "\n", $o );
    }

    function set_flag_option( $selected_id = '', $use_empty = true, $empty_value = '', $empty_label = 'Select option' )
    {
        require_once( INCLUDE_DIR . '/vendor/autoload.php' );

        $o = array();

        if( $use_empty )
        {
            $o[] = sprintf( '<option value="%s">%s</option>', $empty_value, $empty_label );
        }

        $countries = countries();

        foreach( $countries as $code => $dt )
        {
            if( $selected_id != '' && $selected_id == $code )
            {
                $o[] = sprintf( '<option value="%s" selected>%s&nbsp;&nbsp;%s</option>', $code, $dt[ 'emoji' ], $dt[ 'name' ] );
            }
            else
            {
                $o[] = sprintf( '<option value="%s">%s&nbsp;&nbsp;%s</option>', $code, $dt[ 'emoji' ], $dt[ 'name' ] );
            }
        }

        return empty( $o ) ? '' : implode( "\n", $o );
    }

    function set_all_language_option( $selected_id = '', $use_empty = true, $empty_value = '', $empty_label = 'Select option' )
    {
        require_once( INCLUDE_DIR . '/vendor/autoload.php' );

        $o = array();

        if( $use_empty )
        {
            $o[] = sprintf( '<option value="%s">%s</option>', $empty_value, $empty_label );
        }

        $languages = languages();

        foreach( $languages as $code => $dt )
        {
            if( empty( $dt[ 'cultures' ] ) === false )
            {
                foreach( $dt[ 'cultures' ] as $codes => $d )
                {
                    try
                    {
                        list( $lcode, $ccode ) = explode( '-', $codes );

                        $ccode = strtolower( $ccode );
                        
                        $c = country( $ccode );

                        if( $selected_id != '' && $selected_id == $lcode )
                        {
                            $o[] = sprintf( '<option data-country-code="%s" data-name="%s" value="%s" selected>%s&nbsp;&nbsp;%s</option>', $ccode, $dt[ 'name' ], $lcode, $c->getEmoji(), $d[ 'name' ] );
                        }
                        else
                        {
                            $o[] = sprintf( '<option data-country-code="%s" data-name="%s" value="%s">%s&nbsp;&nbsp;%s</option>', $ccode, $dt[ 'name' ], $lcode, $c->getEmoji(), $d[ 'name' ] );
                        }
                    }
                    catch( Exception $e )
                    {
                        //-- do nothing
                    }
                }
            }
            else
            {
                try
                {
                    $c = country( $code );

                    if( $selected_id != '' && $selected_id == $code )
                    {
                        $o[] = sprintf( '<option data-country-code="%s" data-name="%s" value="%s" selected>%s&nbsp;&nbsp;%s</option>', $code, $dt[ 'name' ], $code, $c->getEmoji(), $dt[ 'name' ] );
                    }
                    else
                    {
                        $o[] = sprintf( '<option data-country-code="%s" data-name="%s" value="%s">%s&nbsp;&nbsp;%s</option>', $code, $dt[ 'name' ], $code, $c->getEmoji(), $dt[ 'name' ] );
                    }
                }
                catch( Exception $e )
                {
                    //-- do nothing
                }
            }
        }

        return empty( $o ) ? '' : implode( "\n", $o );
    }

    function set_language_option( $selected_id = '', $use_empty = true, $empty_value = '', $empty_label = 'Select option' )
    {
        return $this->set_static_option( $this->get_language_option(), $selected_id, false );
    }

    function set_filter_language_option( $mod, $lang = '' )
    {
        $data = $this->get_language_option();

        if( empty( $data ) === false )
        {
            $opts = array();

            foreach( $data as $id => $label )
            {
                $prm = http_build_query( array_filter( array( 'mod' => $mod, 'prc' => 'view', 'lang' => $id ) ) );

                if( $lang == $id )
                {
                    array_push( $opts, sprintf( '<option value="%s%s/lumonata-admin/home.php?%s" selected>%s</option>', HT_SERVER, SITE_URL, $prm, $label ) );
                }
                else
                {
                    array_push( $opts, sprintf( '<option value="%s%s/lumonata-admin/home.php?%s">%s</option>', HT_SERVER, SITE_URL, $prm, $label ) );
                }
            }

            return empty( $opts ) ? '' : implode( "\n", $opts );
        }
    }

    function set_switcher_language_option( $data = array(), $mod, $table, $lang, $ref, $translation = array(), $is_landing_page = false, $is_edit = false )
    {
        if( $table == 'lumonata_post' )
        {
            $field = 'lpost_id';
        }
        else if( $table == 'lumonata_post_terms' )
        {
            $field = 'lterm_id';
        }
        else if( $table == 'lumonata_menu' )
        {
            $field = 'lmenu_id';
        }
        else if( $table == 'lumonata_menu_group' )
        {
            $field = 'lgroup_id';
        }
        else if( $table == 'lumonata_packages' )
        {
            $field = 'lpackage_id';
        }
        else if( $table == 'lumonata_schedules' )
        {
            $field = 'lschedule_id';
        }

        $l = $this->getFields( $table, 'llang_id', array( $field => $ref ) );
        $o = array();

        foreach( $data as $id => $label )
        {
            //-- Select current language
            if( $lang == $id )
            {
                $o[] = sprintf( '<option value="javascript:;" selected>%s</option>', $label );
            }
            else
            {
                $a = array();

                if( $is_landing_page )
                {
                    $prm = http_build_query( array_filter( array( 'mod' => $mod, 'prc' => 'view', 'lang' => $id ) ) );
                }
                else
                {
                    if( in_array( $id, $translation ) )
                    {
                        $pid = $this->getFields( $table, $field, array( 'lref_id' => $ref, 'llang_id' => $id ) );
                        $prm = http_build_query( array_filter( array( 'mod' => $mod, 'prc' => 'edit', 'id' => $pid ) ) );
                    }
                    elseif( $l == $id )
                    {
                        $prm = http_build_query( array_filter( array( 'mod' => $mod, 'prc' => 'edit', 'id' => $ref ) ) );
                    }
                    else
                    {
                        $prm = http_build_query( array_filter( array( 'mod' => $mod, 'prc' => 'new', 'lang' => $id, 'ref' => $ref ) ) );
                    }

                    //-- Disabled other language except default language
                    //-- if current page is the new one
                    if( $is_edit === false && $id != $this->getSettingValue( 'llanguage' ) )
                    {
                        array_push( $a, 'disabled' );
                    }
                }

                $o[] = sprintf( '<option value="%s/lumonata-admin/home.php?%s"%s>%s</option>', HT_SERVER . SITE_URL, $prm, implode( ' ', $a ), $label );
            }
        }

        return empty( $o ) ? '' : implode( "\n", $o );
    }

    function set_static_option( $data = array(), $selected_id = '', $use_empty = true, $empty_value = '', $empty_label = 'Select option' )
    {
        $o = array();

        if( $use_empty )
        {
            $o[] = sprintf( '<option value="%s">%s</option>', $empty_value, $empty_label );
        }

        if( !empty( $data ) )
        {
            foreach( $data as $value => $label )
            {
                if( is_array( $label ) )
                {
                    $attr = array();
                    $name = '';

                    foreach( $label as $key => $lbl )
                    {
                        if( $key == 'label' )
                        {
                            $name = $lbl;
                        }
                        else
                        {
                            array_push( $attr, 'data-' . $key . '="' . $lbl . '"' );
                        }
                    }

                    if( $selected_id != '' && $selected_id == $value )
                    {
                        $o[] = sprintf( '<option value="%s" %s selected>%s</option>', $value, implode( ' ', $attr ), $name );
                    }
                    else
                    {
                        $o[] = sprintf( '<option value="%s" %s>%s</option>', $value, implode( ' ', $attr ), $name );
                    }
                }
                else
                {
                    if( $selected_id != '' && $selected_id == $value )
                    {
                        $o[] = sprintf( '<option value="%s" selected>%s</option>', $value, $label );
                    }
                    else
                    {
                        $o[] = sprintf( '<option value="%s">%s</option>', $value, $label );
                    }
                }
            }
        }

        return empty( $o ) ? '' : implode( "\n", $o );
    }

    function set_recursive_label( $id, $table, $field_get, $field_id, $field_parent )
    {
        $data = $this->get_breadcrumber_field( $id, $table, $field_get, $field_id, $field_parent );

        if( !empty( $data ) )
        {
            $label = '<label style="float:left;">Root</label>';

            foreach( $data as $dt )
            {
                $label .= '
                <label style="float: left;padding:0 10px;">&rarr;</label>
                <label style="float:left;">'. $dt . '</label>';
            }

            return $label;
        }
    }

    function set_recursive_option( $selected_id, $query, $field_get, $field_id, $field_parent, $type = 0 )
    {
        $data = $this->get_recursive_field( $query, $field_get, $field_id, $field_parent );
        $list = is_array( $selected_id ) ? $selected_id : array( $selected_id );

        if( $type == 0 )
        {
            $option = '';

            if( !empty( $data ) )
            {
                foreach( $data as $dt )
                {
                    if( in_array( $dt[ 'id' ], $list ) )
                    {
                        $option .= '<option value="' . $dt[ 'id' ] . '" selected="selected">' . $dt[ 'title' ] . '</option>';
                    }
                    else
                    {
                        $option .= '<option value="' . $dt[ 'id' ] . '">' . $dt[ 'title' ] . '</option>';
                    }

                    if( isset( $dt[ 'child' ] ) )
                    {
                        $option .= $this->set_recursive_child_option( $selected_id, $dt[ 'child' ], $type );
                    }
                }
            }   
        }
        else
        {
            $option = '
            <ul>';

                if( !empty( $data ) )
                {
                    foreach( $data as $dt )
                    {
                        $option .= '
                        <li>
                            <p class="custom-checkbox">';

                                if( in_array( $dt[ 'id' ], $list ) )
                                {
                                    $option .= '<input id="opt-' . $dt[ 'id' ] . '" type="checkbox" name="fields[' . $field_id . '][]" value="' . $dt[ 'id' ] . '" checked autocomplete="off" />';
                                }
                                else
                                {
                                    $option .= '<input id="opt-' . $dt[ 'id' ] . '" type="checkbox" name="fields[' . $field_id . '][]" value="' . $dt[ 'id' ] . '" autocomplete="off" />';
                                }

                                $option .= '
                                <label for="opt-' . $dt[ 'id' ] . '">' . $dt[ 'title' ] . '</label>';
                            
                                if( isset( $dt[ 'child' ] ) )
                                {
                                    $option .= $this->set_recursive_child_option( $selected_id, $dt[ 'child' ], $field_id, $type );
                                }

                                $option .= '
                            </p>
                        </li>';
                    }
                }

                $option .= '
            </ul>';
        }

        return $option;
    }

    function set_recursive_child_option( $selected_id, $child_data, $field_id, $type = 0, $dep = 1 )
    {
        $list = is_array( $selected_id ) ? $selected_id : array( $selected_id );

        if( $type == 0 )
        {
            $option = '';

            foreach( $child_data as $dt )
            {
                $dashed = '';

                for( $i = 0; $i < $dep; $i++ )
                {
                   $dashed .= '&#8212;';
                }

                if( in_array( $dt[ 'id' ], $list ) )
                {
                    $option .= '<option value="' . $dt[ 'id' ] . '" selected="selected">' . $dashed . ' ' . $dt[ 'title' ] . '</option>';
                }
                else
                {
                    $option .= '<option value="' . $dt[ 'id' ] . '">' . $dashed . ' ' . $dt[ 'title' ] . '</option>';
                }

                if( isset( $dt[ 'child' ] ) )
                {
                    $option .= $this->set_recursive_child_option( $selected_id, $dt[ 'child' ], $field_id, $type, $dep + 1 );
                }
            }
        }
        else
        {
            $option = '
            <ul>';

                foreach( $child_data as $dt )
                {
                    $option .= '
                    <li>
                        <p class="custom-checkbox">';

                            if( in_array( $dt[ 'id' ], $list ) )
                            {
                                $option .= '<input id="opt-' . $dt[ 'id' ] . '" type="checkbox" name="fields[' . $field_id . '][]" value="' . $dt[ 'id' ] . '" checked autocomplete="off" />';
                            }
                            else
                            {
                                $option .= '<input id="opt-' . $dt[ 'id' ] . '" type="checkbox" name="fields[' . $field_id . '][]" value="' . $dt[ 'id' ] . '" autocomplete="off" />';
                            }

                            $option .= '
                            <label for="opt-' . $dt[ 'id' ] . '">' . $dt[ 'title' ] . '</label>';

                            if( isset( $dt[ 'child' ] ) )
                            {
                                $option .= $this->set_recursive_child_option( $selected_id, $dt[ 'child' ], $field_id, $dep + 1 );
                            }

                            $option .= '
                        </p>
                    </li>';
                }

                $option .= '
            </ul>';
        }

        return $option;
    }

    function build_tree( Array $data, $parent = 0 )
    {
        $tree = array();

        foreach( $data as $d )
        {
            if( $d[ 'parent_id' ] == $parent )
            {
                $children = $this->build_tree( $data, $d[ 'id' ] );

                if( $children )
                {
                    $d[ 'child' ] = $children;
                }

                $tree[ $d[ 'id' ] ] = $d;
            }
        }

        return $tree;
    }

    function breadcrumber( Array $data, $id = 0, $result = array() )
    {
        if( isset( $data[ $id ] ) )
        {
            $result[] = $data[ $id ][ 'title' ];

            $parent = $data[ $id ]['parent_id'];

            unset( $data[ $id ] );

            return $this->breadcrumber( $data, $parent, $result );
        }

        return $result;
    }

    function calculateRate( $post_id )
    {
        $s = 'SELECT 
                a.ltype,
                a.ltitle,
                a.lpost_id,
                a.ldescription,
                a.lcreated_date
               FROM lumonata_post AS a 
               WHERE a.ltype = %s AND (
                SELECT a2.ladditional_value 
                FROM lumonata_additional_field AS a2 
                WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s
               ) = %d ORDER BY a.lcreated_date DESC';
        $q = parent::prepare_query( $s, 'accommodation_reviews', 'accommodation_id', $post_id );
        $r = parent::query( $q );
        $n = parent::num_rows( $r );

        if( $n > 0 )
        {
            $rate  = array();
            $arate = array();
            $brate = array();
            $crate = array();
            $drate = array();
            $erate = array();
            $frate = array();

            while( $d = parent::fetch_array( $r ) )
            {
                //-- GET additional field
                $sa = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lapp_id = %d AND a.lmodule_id = ( SELECT a2.lmodule_id FROM lumonata_module AS a2 WHERE a2.lapps = %s )';
                $qa = parent::prepare_query( $sa, $d[ 'lpost_id' ], $d[ 'ltype' ] );
                $ra = parent::query( $qa );

                if( parent::num_rows( $ra ) > 0 )
                {
                    while( $da = parent::fetch_assoc( $ra ) )
                    {
                        $d[ $da[ 'ladditional_key' ] ] = $da[ 'ladditional_value' ];
                    }

                    $rate[]  = isset( $d[ 'rating' ] ) ? intval( $d[ 'rating' ] ) : 0;
                    $arate[] = isset( $d[ 'accuracy_rate' ] ) ? intval( $d[ 'accuracy_rate' ] ) : 0;
                    $brate[] = isset( $d[ 'location_rate' ] ) ? intval( $d[ 'location_rate' ] ) : 0;
                    $crate[] = isset( $d[ 'activities_rate' ] ) ? intval( $d[ 'activities_rate' ] ) : 0;
                    $drate[] = isset( $d[ 'cleanliness_rate' ] ) ? intval( $d[ 'cleanliness_rate' ] ) : 0;
                    $erate[] = isset( $d[ 'staff_rate' ] ) ? intval( $d[ 'staff_rate' ] ) : 0;
                    $frate[] = isset( $d[ 'overall_rate' ] ) ? intval( $d[ 'overall_rate' ] ) : 0;
                }
            }

            $result[ 'rating' ] = empty( $rate ) ? 0 :  ceil( array_sum( $rate ) / count( $rate ) );
            $result[ 'accuracy_rate' ] = empty( $arate ) ? 0 : ceil( array_sum( $arate ) / count( $arate ) );
            $result[ 'location_rate' ] = empty( $brate ) ? 0 : ceil( array_sum( $brate ) / count( $brate ) );
            $result[ 'activities_rate' ] = empty( $crate ) ? 0 : ceil( array_sum( $crate ) / count( $crate ) );
            $result[ 'cleanliness_rate' ] = empty( $drate ) ? 0 : ceil( array_sum( $drate ) / count( $drate ) );
            $result[ 'staff_rate' ] = empty( $erate ) ? 0 : ceil( array_sum( $erate ) / count( $erate ) );
            $result[ 'overall_rate' ] = empty( $frate ) ? 0 : ceil( array_sum( $frate ) / count( $frate ) );

            return $result;
        }
    }

    function get_rate_price( $post_data, $checkin = '', $checkout = '' )
    {
        $s = 'SELECT 
                b.lstart_period, 
                b.lend_period, 
                a.lrate_price,
                a.lrate_id
              FROM lumonata_rates AS a 
              LEFT JOIN lumonata_rate_periods AS b ON b.lrate_id = a.lrate_id
              WHERE a.lpost_id = %d';
        $q = parent::prepare_query( $s, $post_data[ 'lpost_id' ] );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $rates = array();

            while( $d = parent::fetch_assoc( $r ) )
            {
                $start = date( 'Y-m-d', strtotime( $d[ 'lstart_period' ] ) );
                $end   = date( 'Y-m-d', strtotime( $d[ 'lend_period' ] ) );

                if( empty( $checkin ) && empty( $checkout ) )
                {
                    $dstart = date( 'Y-m-d' );
                    $dend   = date( 'Y-m-d' );
                }
                else
                {
                    $dstart = date( 'Y-m-d', strtotime( $checkin ) );
                    $dend   = date( 'Y-m-d', strtotime( $checkout ) );
                }

                if( $dstart >= $start && $dend <= $end )
                {
                    $rates[ 'price' ] = $d[ 'lrate_price' ];

                    //-- GET detail rate
                    $s2 = 'SELECT a.lterm_id, a.lrate_price FROM lumonata_rate_details AS a WHERE a.lrate_id = %d';
                    $q2 = parent::prepare_query( $s2, $d[ 'lrate_id' ] );
                    $r2 = parent::query( $q2 );

                    if( parent::num_rows( $r2 ) > 0 )
                    {
                        while( $d2 = parent::fetch_assoc( $r2 ) )
                        {
                            $rates[ 'rooms_price' ][ $d2[ 'lterm_id' ] ] = $d2[ 'lrate_price' ];
                        }
                    }

                    break;
                }
            }

            return $rates;
        }
    }

    function get_start_price( $post_data )
    {
        if( $post_data[ 'page_template' ] == '3' )
        {
            $rates = $this->get_rate_price( $post_data );

            if( empty( $rates ) )
            {
                //-- Use default price
                if( empty( $post_data[ 'room_price' ] ) )
                {
                    return 0;
                }
                else
                {
                    return floatval( $post_data[ 'room_price' ] );
                }
            }
            else
            {
                //-- Use rate price
                if( empty( $rates[ 'price' ] ) )
                {
                    return 0;
                }
                else
                {
                    return floatval( $rates[ 'price' ] );
                }
            }
        }
        else
        {
            if( isset( $post_data[ 'room_type' ] ) && !empty( $post_data[ 'room_type' ] ) )
            {
                $stack = array();

                foreach( $post_data[ 'room_type' ] as $d )
                {
                    $stack[] = $this->room_price( $post_data, $d );
                }

                return min( $stack );
            }
            else
            {
                $rates = $this->get_rate_price( $post_data );

                if( empty( $rates ) )
                {
                    //-- Use default price
                    if( empty( $post_data[ 'room_price' ] ) )
                    {
                        return 0;
                    }
                    else
                    {
                        return floatval( $post_data[ 'room_price' ] );
                    }
                }
                else
                {
                    //-- Use rate price
                    if( empty( $rates[ 'price' ] ) )
                    {
                        return 0;
                    }
                    else
                    {
                        return floatval( $rates[ 'price' ] );
                    }
                }
            }
        }
    }

    function room_price( $acco, $room, $checkin = '', $checkout = '' )
    {
        $rates = $this->get_rate_price( $acco, $checkin, $checkout );

        if( empty( $rates ) )
        {
            //-- Use default room price
            if( isset( $room[ 'room_price' ] ) && !empty( $room[ 'room_price' ] ) )
            {
                $rate = $room[ 'room_price' ];
            }
            else
            {
                $rate = 0;
            }
        }
        else
        {
            //-- Use rate room price
            $rid = $room[ 'lterm_id' ];

            if( isset( $rates[ 'rooms_price' ][ $rid ] ) && !empty( $rates[ 'rooms_price' ][ $rid ] ) )
            {
                $rate = $rates[ 'rooms_price' ][ $rid ];
            }
            else if( isset( $room[ 'room_price' ] ) && !empty( $room[ 'room_price' ] ) )
            {
                $rate = $room[ 'room_price' ];
            }
            else
            {
                $rate = 0;
            }
        }

        if( isset( $acco[ 'policy' ] ) && !empty( $acco[ 'policy' ] ) )
        {
            $policy = $acco[ 'policy' ];
        }
        else
        {
            $policy = '0';
        }
        
        if( $policy == '0' )
        {
            $crate = 0;

            if( $checkin != '' && $checkout != '' )
            {
                $s = 'SELECT a.lrate FROM lumonata_calendar as a WHERE a.lpost_id = %d AND a.lterm_id = %d AND a.ldate BETWEEN %s AND %s';
                $q = parent::prepare_query( $s, $acco[ 'lpost_id' ], $room[ 'lterm_id' ], $checkin, $checkout );
                $r = parent::query( $q );

                if( parent::num_rows( $r ) > 0 )
                {
                    $arate = array();

                    while( $d = parent::fetch_array( $r ) )
                    {
                        $arate[] = $d[ 'lrate' ];
                    }

                    $crate = max( $arate );
                }
            }
            else
            {
                $crate = $this->getFields( 'lumonata_calendar', 'lrate', array(
                    'lpost_id' => $acco[ 'lpost_id' ],
                    'lterm_id' => $room[ 'lterm_id' ],
                    'ldate'    => date( 'Y-m-d' ),
                ));
            }

            if( empty( $crate ) === false )
            {
                $rate = $crate;
            }
        }

        return $rate;
    }

    function get_monthly_earnings( $agent_id )
    {
        $s = 'SELECT 
                a.ldeposit,
                FROM_UNIXTIME( a.lcreated_date, %s ) AS fdate
              FROM lumonata_booking AS a 
              WHERE FROM_UNIXTIME( a.lcreated_date, %s ) = %s 
              AND a.lagent = %s AND a.lstatus_payment = 1';
        $q = parent::prepare_query( $s, '%b', '%Y-%m', date( 'Y-m' ), $agent_id );
        $r = parent::query( $q );

        $earning = 0;

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_array( $r ) )
            {
                $earning += floatval( $d[ 'ldeposit' ] );
            }
        }

        return $this->get_format_price( $earning );
    }

    function get_property_view( $agent_id, $range = 30 )
    {
        $s = 'SELECT COUNT( a.lpvid ) AS num
              FROM lumonata_page_view AS a
              LEFT JOIN lumonata_post AS b ON a.lpost_id = b.lpost_id
              WHERE b.lcreated_by = %s AND a.lpvdate BETWEEN CURDATE() - INTERVAL %d DAY AND CURDATE()';
        $q = parent::prepare_query( $s, $agent_id, $range );
        $r = parent::query( $q );

        $view = 0;

        if( parent::num_rows( $r ) > 0 )
        {
            $d = parent::fetch_array( $r );

            $view = $d[ 'num' ];
        }

        return $view;
    }

    function get_booking_num( $username, $range = 30 )
    {
        $s = 'SELECT COUNT( a.lbooking_id ) AS num 
              FROM lumonata_booking AS a 
              WHERE a.lagent = %s AND FROM_UNIXTIME( a.lcreated_date ) BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()';
        $q = parent::prepare_query( $s, $username, $range );
        $r = parent::query( $q );

        $view = 0;

        if( parent::num_rows( $r ) > 0 )
        {
            $d = parent::fetch_array( $r );

            $view = $d[ 'num' ];
        }

        return $view;
    }

    function get_overall_rating( $username )
    {
        $data = $this->getPosts( array( 'lcreated_by' => $username ), false );

        if( empty( $data ) )
        {
            return '-';
        }
        else
        {
            $result = array();

            foreach( $data as $dt )
            {
                $s = 'SELECT 
                        a.ltype,
                        a.lpost_id
                       FROM lumonata_post AS a 
                       WHERE a.ltype = %s AND (
                        SELECT a2.ladditional_value 
                        FROM lumonata_additional_field AS a2 
                        WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s
                       ) = %d ORDER BY a.lcreated_date DESC';
                $q = parent::prepare_query( $s, 'accommodation_reviews', 'accommodation_id', $dt[ 'lpost_id' ] );
                $r = parent::query( $q );
                $n = parent::num_rows( $r );

                if( $n > 0 )
                {
                    $rate = array();

                    while( $d = parent::fetch_array( $r ) )
                    {
                        //-- GET additional field
                        $sa = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lapp_id = %d AND a.ladditional_key = %s AND a.lmodule_id = ( SELECT a2.lmodule_id FROM lumonata_module AS a2 WHERE a2.lapps = %s )';
                        $qa = parent::prepare_query( $sa, $d[ 'lpost_id' ], 'overall_rate', $d[ 'ltype' ] );
                        $ra = parent::query( $qa );

                        if( parent::num_rows( $ra ) > 0 )
                        {
                            $da = parent::fetch_array( $ra );

                            $rate[] = $da[ 'ladditional_value' ];
                        }
                    }

                    $result[ $dt[ 'lpost_id' ] ] = empty( $rate ) ? 0 : ceil( array_sum( $rate ) / count( $rate ) );
                }
            }

            return empty( $result ) ? 0 : ceil( array_sum( $result ) / count( $result ) );
        }
    }

    function get_total_review( $username )
    {
        $data = $this->getPosts( array( 'lcreated_by' => $username ), false );

        if( empty( $data ) )
        {
            return 0;
        }
        else
        {
            $num = array();

            foreach( $data as $dt )
            {
                $s = 'SELECT 
                        a.ltype,
                        a.lpost_id
                       FROM lumonata_post AS a 
                       WHERE a.ltype = %s AND (
                        SELECT a2.ladditional_value 
                        FROM lumonata_additional_field AS a2 
                        WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s
                       ) = %d ORDER BY a.lcreated_date DESC';
                $q = parent::prepare_query( $s, 'accommodation_reviews', 'accommodation_id', $dt[ 'lpost_id' ] );
                $r = parent::query( $q );
                $n = parent::num_rows( $r );

                if( $n > 0 )
                {
                    $num[] = $n;
                }
            }
            
            return empty( $num ) ? 0 : array_sum( $num );
        }
    }

    function getLocations( $data = array() )
    {
        $include  = array( 'accommodation_state', 'accommodation_country' );
        $location = array();

        foreach( $include as $dt )
        {
            if( isset( $data[ $dt ] ) && !empty( $data[ $dt ] ) )
            {
                $dest = $this->getFields( 'lumonata_post', array( 'ltitle', 'lsef_url' ), array( 'lpost_id' => $data[ $dt ] ) );

                if( empty( $dest ) === false )
                {
                    $location[] = $dest[ 'ltitle' ];
                }
            }
        }

        if( !empty( $location ) )
        {
            return implode( ',&nbsp;', $location );
        }
    }

    function getSimmilarProperty( $data = array() )
    {
        $country = $data[ 'lposts_data' ][ 'accommodation_country' ];
        $state   = $data[ 'lposts_data' ][ 'accommodation_state' ];
        $city    = $data[ 'lposts_data' ][ 'accommodation_city' ];
        $deflang = $this->getSettingValue( 'llanguage' );

        $s = 'SELECT a.lpost_id
              FROM lumonata_post AS a 
              WHERE a.ltype = %s 
              AND a.lpost_id <> %d
              AND a.llang_id = %d
              AND (
                ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d OR
                ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d OR
                ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d
              ) ORDER BY a.lcreated_date DESC LIMIT 2';
        $q = parent::prepare_query( $s, 'accommodation', $data[ 'lposts_data' ][ 'lpost_id' ], $deflang, 'accommodation_country', $country, 'accommodation_state', $state, 'accommodation_city', $city );
        $r = parent::query( $q );
        $n = parent::num_rows( $r );

        if( $n > 0 )
        {
            $data = array();

            while( $d = parent::fetch_array( $r ) )
            {
                $data[] = $this->getPosts( array( 'lpost_id' => $d[ 'lpost_id' ] ) );
            }

            return $data;
        }
    }

    function get_currency_sign( $id = '', $use_post_id = true )
    {
        if( empty( $id ) )
        {
            $currency = $this->getSettingValue( 'lcurrency' );
        }
        else
        {
            if( $use_post_id )
            {
                $currency = $this->getFields( 'lumonata_additional_field', 'ladditional_value', array( 'ladditional_key' => 'currency', 'lapp_id' => $id ) );

                if( empty( $currency ) )
                {
                    $currency = $this->getSettingValue( 'lcurrency' );
                }
            }
            else
            {
                $currency = $id;
            }
        }

        $display = $this->getSettingValue( 'lcurrency_display' );

        if( $currency != '' && $display != '' )
        {
            if( $display == '0' )
            {
                return $this->getFields( 'lumonata_currency', 'lsymbol', array( 'lcurrency_id' => $currency ) );
            }
            else
            {
                return $this->getFields( 'lumonata_currency', 'lcode', array( 'lcurrency_id' => $currency ) );
            }
        }
    }

    function get_currency_code( $id = '' )
    {
        $currency = $this->getSettingValue( 'lcurrency' );

        if( empty( $id ) )
        {
            $currency = $this->getSettingValue( 'lcurrency' );
        }
        else
        {
            $currency = $this->getFields( 'lumonata_additional_field', 'ladditional_value', array( 'ladditional_key' => 'currency', 'lapp_id' => $id ) );
        }

        if( $currency != '' )
        {
            return $this->getFields( 'lumonata_currency', 'lcode', array( 'lcurrency_id' => $currency ) );
        }
    }

    function get_currency_id( $id = '' )
    {
        $currency = $this->getSettingValue( 'lcurrency' );

        if( empty( $id ) )
        {
            $currency = $this->getSettingValue( 'lcurrency' );
        }
        else
        {
            $currency = $this->getFields( 'lumonata_additional_field', 'ladditional_value', array( 'ladditional_key' => 'currency', 'lapp_id' => $id ) );
        }

        return $currency;
    }

    function get_currency_option()
    {
        $s = 'SELECT * FROM lumonata_currency AS a ORDER BY a.lorder_id';
        $q = parent::prepare_query( $s );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $option = array();

            while( $d = parent::fetch_assoc( $r ) )
            {
                $option[ $d[ 'lcurrency_id' ] ] = $d[ 'lcurrency' ];
            }

            return $option;
        }
    }

    function get_country_flag( $lcountry_code )
    {
        require_once( INCLUDE_DIR . '/vendor/autoload.php' );

        if( empty( $lcountry_code ) )
        {
            return '<img src="https://dummyimage.com/32x21/333333/fff&text=NF" alt="" width="32" />';
        }
        else
        {
            try
            {
                $c = country( $lcountry_code );

                return '<img src="data:image/svg+xml;base64,' . base64_encode( $c->getFlag() ) . '" alt="" width="32" />';
            }
            catch( Exception $e )
            {
                return '<img src="https://dummyimage.com/32x21/333333/fff&text=NF" alt="" width="32" />';
            }
        }
    }

    function get_country_name( $lcountry_code )
    {
        require_once( INCLUDE_DIR . '/vendor/autoload.php' );

        if( empty( $lcountry_code ) )
        {
            return '-';
        }
        else
        {
            try
            {
                $c = country( $lcountry_code );

                return $c->getName();
            }
            catch( Exception $e )
            {
                return '-';
            }
        }
    }

    function get_language_option()
    {
        require_once( INCLUDE_DIR . '/vendor/autoload.php' );

        $s = 'SELECT * FROM lumonata_language AS a WHERE a.lstatus = 1 ORDER BY a.lorder_id';
        $q = parent::prepare_query( $s );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $option = array();

            while( $d = parent::fetch_assoc( $r ) )
            {
                try
                {
                    $c = country( $d[ 'lcountry_code' ] );

                    $option[ $d[ 'llang_id' ] ] = sprintf( '%s&nbsp;&nbsp;%s', $c->getEmoji(), $d[ 'llanguage' ] );
                }
                catch( Exception $e )
                {
                    //-- do nothing
                }
            }

            return $option;
        }
    }

    function get_language_flag( $llang_id )
    {
        require_once( INCLUDE_DIR . '/vendor/autoload.php' );

        $code = $this->getFields( 'lumonata_language', 'lcountry_code', array( 'llang_id' => $llang_id ) );

        try
        {
            $c = country( $code );

            return '<img src="data:image/svg+xml;base64,' . base64_encode( $c->getFlag() ) . '" alt="" width="32" />';
        }
        catch( Exception $e )
        {
            return '-';
        }
    }

    function get_switcher_language( $mod, $table, $lang, $ref, $translation = array(), $is_landing_page = false, $is_edit = false, $as_field = false )
    {
        $dt = $this->get_language_option();

        if( !empty( $dt ) )
        {
            if( $as_field )
            {
                $content = '
                <div class="list">
                    <label>Translation</label>
                    <div class="chzn-selecto">
                        <select name="translation" class="chzn-select translation-select" style="width:100%;">
                            ' . $this->set_switcher_language_option( $dt, $mod, $table, $lang, $ref, $translation, $is_landing_page, $is_edit ) . '
                        </select>
                    </div>
                </div>';
            }
            else
            {
                $content = '
                <div class="mb0" style="margin-top: 0px; height: auto; border:none; float:right;">
                    <p class="f14 fwbold mb0">
                        Translation :
                        <select name="translation" class="chzn-select translation-select" style="width:200px;">
                            ' . $this->set_switcher_language_option( $dt, $mod, $table, $lang, $ref, $translation, $is_landing_page, $is_edit ) . '
                        </select>
                    </p>
                </div>';                
            }

            return $content;
        }
    }

    function get_format_date_range( $start, $end, $yformat = 'Y', $mformat = 'M', $dformat = 'd' )
    {
        $syear = date( $yformat, $start );
        $eyear = date( $yformat, $end );

        if( $syear == $eyear )
        {
            $smonth = date( $mformat, $start );
            $emonth = date( $mformat, $end );

            if( $smonth == $emonth )
            {
                return sprintf( '%s %s - %s, %s', $smonth, date( $dformat, $start ), date( $dformat, $end ), $syear );
            }
            else
            {
                return sprintf( '%s - %s', date( $mformat . ' ' . $dformat, $start ), date( $mformat . ' ' . $dformat . ', ' . $yformat, $end ) );
            }
        }
        else
        {
            return sprintf( '%s - %s', date( $mformat . ' ' . $dformat . ', ' . $yformat, $start ), date( $mformat . ' ' . $dformat . ', ' . $yformat, $end ) );
        }
    }

    function get_format_guest_num( $adult = 0, $child = 0, $infant = 0 )
    {
        $num = array();

        if( $adult != 0 )
        {
            $num[] = $adult . ( $adult > 1 ? ' adults' : ' adult' );
        }

        if( $child != 0 )
        {
            $num[] = $child . ( $child > 1 ? ' childs' : ' child' );
        }

        if( $infant != 0 )
        {
            $num[] = $infant . ( $infant > 1 ? ' infants' : ' infant' );
        }

        if( empty( $num ) === false )
        {
            return implode( ', ', $num );
        }
    }

    function get_format_listings( $num )
    {
        if( $num > 1 )
        {
            return sprintf( '%s listings', $num );
        }
        else
        {
            return sprintf( '%s listing', $num );
        }
    }

    function get_format_nights( $nights )
    {
        if( $nights > 1 )
        {
            return sprintf( '%s nights', $nights );
        }
        else
        {
            return sprintf( '%s night', $nights );
        }
    }

    function get_format_price( $price = 0, $id = null )
    {
        $sign  = $this->get_currency_sign( $id );
        $price = number_format( $price, 0, ',', '.' );

        return sprintf( '%s%s', $sign, $price );
    }

    function init_custom_field( $id, $mod, $param = array(), $lang = '' )
    {
        if( isset( $param[ 'field' ] ) && empty( $param[ 'field' ] ) === false )
        {
            $deflang = $this->getSettingValue( 'llanguage' );
            $content = '';

            //-- Show On
            if( isset( $param[ 'show_on' ] ) )
            {
                $show_on = sprintf( ' data-show-on-group-key="%s" data-show-on-group-value="%s"', $param[ 'show_on' ][ 'key' ], $param[ 'show_on' ][ 'value' ] );
            }
            else
            {
                $show_on = '';
            }

            foreach( $param[ 'field' ] as $idx => $obj )
            {
                if( isset( $obj[ 'hide_on_translation' ] ) && $obj[ 'hide_on_translation' ] === true )
                {
                    //-- Hide this field on translation page
                    if( $deflang != $lang )
                    {
                        continue;
                    }
                }

                //-- Field Value
                $data = '';

                if( is_array( $obj[ 'name' ] ) === false )
                {
                    if( $param[ 'types' ] == 1 )
                    {
                        $data = $this->getFields( 'lumonata_additional_field', 'ladditional_value', array( 'lapp_id' => $id, 'ladditional_key' => $obj[ 'name' ], 'lmodule_id' => $mod ) );
                    }
                    elseif( $param[ 'types' ] == 2 )
                    {                        
                        $data = $this->getFields( 'lumonata_additional_field', 'ladditional_value', array( 'lterm_id' => $id, 'ladditional_key' => $obj[ 'name' ], 'lmodule_id' => $mod ) );
                    }
                }

                //-- Field Type
                if( $obj[ 'type' ] == 'text' )
                {
                    $content .= $this->add_text_field( $obj, $data );
                }
                elseif( $obj['type'] == 'select' )
                {
                    $content .= $this->add_select_field( $obj, $data );
                }
                elseif( $obj['type'] == 'checkbox' )
                {
                    $content .= $this->add_checkbox_field( $obj, $data );
                }
                elseif( $obj['type'] == 'radio' )
                {
                    $content .= $this->add_radio_field( $obj, $data );
                }
                elseif( $obj['type'] == 'textarea' )
                {
                    $content .= $this->add_textarea_field( $obj, $data );
                }
                elseif( $obj['type'] == 'file' )
                {
                    $content .= $this->add_file_field( $obj, $data);
                }
                elseif( $obj['type'] == 'image' )
                {
                    $content .= $this->add_image_field( $obj, $data );
                }
                elseif( $obj['type'] == 'gallery' )
                {
                    $content .= $this->add_gallery_field( $obj, $data );
                }
                elseif( $obj['type'] == 'repeater' )
                {
                    $content .= $this->add_repeater_field( $obj, $data );
                }
                elseif( $obj['type'] == 'fonticon' )
                {
                    $content .= $this->add_fonticon_field( $obj, $data );
                }
                elseif( $obj['type'] == 'rate' )
                {
                    $content .= $this->add_rate_field( $obj, $data );
                }
                elseif( $obj['type'] == 'gmail-xoauth' )
                {
                    $content .= $this->add_gmail_xoauth_field( $obj, $data );
                }
                elseif( $obj['type'] == 'instagram-xoauth' )
                {
                    $content .= $this->add_instagram_xoauth_field( $obj, $data );
                }
            }

            if( isset( $param[ 'group' ] ) )
            {
                if( isset( $param[ 'hide_on_translation' ] ) && $param[ 'hide_on_translation' ] === true )
                {
                    //-- Hide this field on translation page
                    if( $deflang != $lang )
                    {
                        return '';
                    }
                }

                return '
                <div class="content-body-1020 content-body-1042 clearfix"' . $show_on . '>
                    <div class="header-1020 header-1042">' . $param[ 'group' ] . '</div>
                    <div class="content-1042 new-style-content-1024 clearfix">
                        <div class="block-mini box-size" style="margin-top: 0px; width: 100%; height: auto; border-right:none; margin: 0;  float: left;">
                            ' . $content . '
                        </div>
                    </div>
                </div>';
            }
            else
            {
                return $content;
            }
        }
        else
        {
            $content = '';

            foreach( $param as $idx => $prm )
            {
                $content .= $this->init_custom_field( $id, $mod, $prm, $lang );
            }

            return $content;
        }
    }

    function concat_attribute( $attrs = array() )
    {
        if( empty( $attrs ) )
        {
            return '';
        }

        $attributes = '';

        foreach( $attrs as $attr => $val )
        {
            if( $val != '' && 'value' !== $attr )
            {
                $val = is_array( $val ) ? implode( ',', $val ) : $val;

                //-- if data attribute, use single quote wraps, else double.
                $attributes .= sprintf( ' %1$s=%3$s%2$s%3$s', $attr, $val, '"' );
            }
        }

        return $attributes;
    }

    function add_text_field( $obj, $data = '', $group_name = '', $group_index = 0 )
    {
        extract( $obj );

        $attrs = array( 'type' => 'text', 'class' => 'text' );

        if( isset( $attributes ) )
        {
            $attrs = array_merge( $attrs, $attributes );
        }

        if( isset( $show_on ) )
        {
            $show_on = sprintf( ' data-show-on-key="%s" data-show-on-value="%s"', $show_on[ 'key' ], $show_on[ 'value' ] );
        }
        else
        {
            $show_on = '';
        }

        if( isset( $show ) && $show === false )
        {
            $show_class = ' hidden';

            $attrs = array_merge( $attrs, array( 'disabled' => 'disabled' ) );
        }
        else
        {
            $show_class = '';
        }

        if( isset( $obj[ 'value' ] ) && $obj[ 'value' ] !== '' )
        {
            $data = $obj[ 'value' ];
        }

        $attributes = $this->concat_attribute( $attrs );

        $content = '
        <div class="list list-field' . $show_class . '"' . $show_on . '>
            <label class="list-field-title">' . $label . '</label>';

            if( empty( $group_name ) )
            {
                $content .= '<input value="' . $data . '" id="' . $id . '" name="fields[ladditional][' . $name . ']" ' . $attributes . '>';
            }
            else
            {
                $content .= '<input value="' . $data . '" id="' . $id . '_' . substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 ) . '" name="fields[ladditional][' . $group_name . '][' . $group_index . '][' . $name . ']" ' . $attributes . '>';   
            }

            if( isset( $desc ) && $desc != '' )
            {
                $content .= '<small class="list-field-desc"><i>' . $desc . '</i></small>';
            }

            $content .= '
        </div>';

        return $content;
    }

    function add_select_field( $obj, $data = '', $group_name = '', $group_index = 0 )
    {
        extract( $obj );

        $attrs = array( 'class' => 'chzn-select', 'show_empty' => true, 'style' => 'width:300px;' );

        if( isset( $multiple ) && $multiple )
        {
            $attrs[ 'multiple' ] = $multiple;
        }

        if( isset( $attributes ) )
        {
            $attrs = array_merge( $attrs, $attributes );
        }

        if( isset( $show_on ) )
        {
            $show_on = sprintf( ' data-show-on-key="%s" data-show-on-value="%s"', $show_on[ 'key' ], $show_on[ 'value' ] );
        }
        else
        {
            $show_on = '';
        }

        if( isset( $show ) && $show === false )
        {
            $show_class = ' hidden';

            $attrs = array_merge( $attrs, array( 'disabled' => 'disabled' ) );
        }
        else
        {
            $show_class = '';
        }

        //-- GET value
        $ndata = json_decode( $data, true );

        if( $ndata !== null && json_last_error() === JSON_ERROR_NONE )
        {
            if( is_array( $ndata ) )
            {
                $data = $ndata;
            }
            else
            {
                $data = array( $data );
            }
        }
        else
        {
            $data = array( $data );
        }

        $attributes = $this->concat_attribute( $attrs );

        $content = '
        <div class="list list-field' . $show_class . '"' . $show_on . '>
            <label class="list-field-title">' . $label . '</label>
            <div class="chzn-selecto">';

                if( empty( $group_name ) )
                {
                    if( isset( $attrs[ 'multiple' ] ) )
                    {
                        $fname = 'fields[ladditional][' . $name . '][]';
                    }
                    else
                    {
                        $fname = 'fields[ladditional][' . $name . ']';
                    }

                    $content .= '
                    <select id="' . $id . '" name="' . $fname . '" ' . $attributes . '>';

                        if( $attrs[ 'show_empty' ] === true )
                        {
                            $content .= '<option value=""></option>';
                        }

                        foreach( $options as $val => $text )
                        {
                            $content .= '<option value="' . $val . '" ' . ( in_array( $val, $data ) ? 'selected' : '' ) . '>' . $text . '</option>';
                        }

                        $content .= '
                    </select>';
                }
                else
                {
                    if( isset( $attrs[ 'multiple' ] ) )
                    {
                        $fname = 'fields[ladditional][' . $group_name . '][' . $group_index . '][' . $name . '][]';
                    }
                    else
                    {
                        $fname = 'fields[ladditional][' . $group_name . '][' . $group_index . '][' . $name . ']';
                    }

                    $content .= '
                    <select id="' . $id . '_' . substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 ) . '" name="' . $fname . '" ' . $attributes . '>';

                        if( $attrs[ 'show_empty' ] === true )
                        {
                            $content .= '<option value=""></option>';
                        }

                        foreach( $options as $val => $text )
                        {
                            $content .= '<option value="' . $val . '" ' . ( in_array( $val, $data ) ? 'selected' : '' ) . '>' . $text . '</option>';
                        }

                        $content .= '
                    </select>';
                }

                $content .= '
            </div>';

            if( isset( $desc ) && $desc != '' )
            {
                $content .= '<small class="list-field-desc"><i>' . $desc . '</i></small>';
            }

            $content .= '
        </div>';

        return $content;
    }

    function add_checkbox_field( $obj, $data = '', $group_name = '', $group_index = 0 )
    {
        extract( $obj );

        $attrs = array( 'class' => 'checkbox-input' );

        if( isset( $attributes ) )
        {
            $attrs = array_merge( $attrs, $attributes );
        }

        if( isset( $show_on ) )
        {
            $show_on = sprintf( ' data-show-on-key="%s" data-show-on-value="%s"', $show_on[ 'key' ], $show_on[ 'value' ] );
        }
        else
        {
            $show_on = '';
        }

        if( isset( $show ) && $show === false )
        {
            $show_class = ' hidden';

            $attrs = array_merge( $attrs, array( 'disabled' => 'disabled' ) );
        }
        else
        {
            $show_class = '';
        }

        $attributes = $this->concat_attribute( $attrs );

        $content = '
        <div class="list list-field list-field-checkbox' . $show_class . '"' . $show_on . '>
            <label class="list-field-title">' . $label . '</label>
            <div class="list-checkbox">
                <ul>';

                    if( empty( $data ) === false )
                    {
                        $data = json_decode( $data, true );
                    }

                    foreach( $options as $val => $text )
                    {
                        if( empty( $group_name ) )
                        {
                            $optid = substr( base_convert( md5( $val . '||' . rand() ), 16, 32 ), 0, 12 );
                        }
                        else
                        {
                            $optid = substr( base_convert( md5( $group_name . '||' . $val . '||' . rand() ), 16, 32 ), 0, 12 );
                        }

                        $content .= '
                        <li>
                            <p class="custom-checkbox">';
                            
                                if( empty( $group_name ) )
                                {
                                    if( is_array( $data ) && in_array( $val, $data ) )
                                    {
                                        $content .= '<input id="' . $optid . '" type="checkbox" name="fields[ladditional][' . $name . '][]" value="' . $val . '" ' . $attributes . ' checked />';
                                    }
                                    else
                                    {
                                        $content .= '<input id="' . $optid . '" type="checkbox" name="fields[ladditional][' . $name . '][]" value="' . $val . '" ' . $attributes . ' />';
                                    }

                                    $content .= '
                                    <label for="' . $optid . '">' . $text . '</label>';
                                }
                                else
                                {
                                    if( is_array( $data ) && in_array( $val, $data ) )
                                    {
                                        $content .= '<input id="' . $optid . '" type="checkbox" name="fields[ladditional][' . $group_name . '][' . $group_index . '][' . $name . '][]" value="' . $val . '" ' . $attributes . ' checked />';
                                    }
                                    else
                                    {
                                        $content .= '<input id="' . $optid . '" type="checkbox" name="fields[ladditional][' . $group_name . '][' . $group_index . '][' . $name . '][]" value="' . $val . '" ' . $attributes . ' />';
                                    }

                                    $content .= '
                                    <label for="' . $optid . '">' . $text . '</label>';
                                }

                                $content .= '
                            </p>
                        </li>';
                    }

                    $content .= '
                </ul>
            </div>';

            if( isset( $desc ) && $desc != '' )
            {
                $content .= '<small class="list-field-desc"><i>' . $desc . '</i></small>';
            }

            $content .= '
        </div>';

        return $content;
    }

    function add_radio_field( $obj, $data = '', $group_name = '', $group_index = 0 )
    {
        extract( $obj );

        $attrs = array( 'class' => 'radio-input' );

        if( isset( $attributes ) )
        {
            $attrs = array_merge( $attrs, $attributes );
        }

        if( isset( $show_on ) )
        {
            $show_on = sprintf( ' data-show-on-key="%s" data-show-on-value="%s"', $show_on[ 'key' ], $show_on[ 'value' ] );
        }
        else
        {
            $show_on = '';
        }

        if( isset( $show ) && $show === false )
        {
            $show_class = ' hidden';

            $attrs = array_merge( $attrs, array( 'disabled' => 'disabled' ) );
        }
        else
        {
            $show_class = '';
        }

        if( $data == '' && isset( $default ) )
        {
            $data = $default;
        }

        $attributes = $this->concat_attribute( $attrs );

        $content = '
        <div class="list list-field' . $show_class . '"' . $show_on . '>
            <label class="list-field-title">' . $label . '</label>';

            if( isset( $desc ) && $desc != '' )
            {
                $content .= '
                <small class="list-field-desc" style="margin-top:-5px; margin-bottom:10px;"><i>' . $desc . '</i></small>
                <div class="clearfix"></div>';
            }

            $content .= '
            <div class="list-checkbox">
                <ul>';

                    foreach( $options as $val => $text )
                    {                            
                        if( empty( $group_name ) )
                        {
                            $optid = substr( base_convert( md5( $val . '||' . rand() ), 16, 32 ), 0, 12 );
                        }
                        else
                        {
                            $optid = substr( base_convert( md5( $group_name . '||' . $val . '||' . rand() ), 16, 32 ), 0, 12 );
                        }

                        $content .= '
                        <li>
                            <p class="custom-checkbox">';
                            
                                if( empty( $group_name ) )
                                {
                                    if( $val == $data )
                                    {
                                        $content .= '<input id="' . $optid . '" type="radio" name="fields[ladditional][' . $name . ']" value="' . $val . '" ' . $attributes . ' checked />';
                                    }
                                    else
                                    {
                                        $content .= '<input id="' . $optid . '" type="radio" name="fields[ladditional][' . $name . ']" value="' . $val . '" ' . $attributes . ' />';
                                    }

                                    $content .= '
                                    <label for="' . $optid . '">' . $text . '</label>';
                                }
                                else
                                {
                                    if( $val == $data )
                                    {
                                        $content .= '<input id="' . $optid . '" type="radio" name="fields[ladditional][' . $group_name . '][' . $group_index . '][' . $name . ']" value="' . $val . '" ' . $attributes . ' checked />';
                                    }
                                    else
                                    {
                                        $content .= '<input id="' . $optid . '" type="radio" name="fields[ladditional][' . $group_name . '][' . $group_index . '][' . $name . ']" value="' . $val . '" ' . $attributes . ' />';
                                    }

                                    $content .= '
                                    <label for="' . $optid . '">' . $text . '</label>';
                                }

                                $content .= '
                            </p>
                        </li>';
                    }

                    $content .= '
                </ul>
            </div>
        </div>';

        return $content;
    }

    function add_textarea_field( $obj, $data = '', $group_name = '', $group_index = 0 )
    {
        extract( $obj );

        $attrs = array( 'class' => 'textarea' );

        if( isset( $attributes ) )
        {
            $attrs = array_merge( $attrs, $attributes );
        }

        if( isset( $show_on ) )
        {
            $show_on = sprintf( ' data-show-on-key="%s" data-show-on-value="%s"', $show_on[ 'key' ], $show_on[ 'value' ] );
        }
        else
        {
            $show_on = '';
        }

        if( isset( $show ) && $show === false )
        {
            $show_class = ' hidden';

            $attrs = array_merge( $attrs, array( 'disabled' => 'disabled' ) );
        }
        else
        {
            $show_class = '';
        }

        $attributes = $this->concat_attribute( $attrs );

        $content = '
        <div class="list list-field' . $show_class . '"' . $show_on . '>
            <label class="list-field-title">' . $label . '</label>';

            if( empty( $group_name ) )
            {
                $content .= '<div><textarea id="' . $id . '" name="fields[ladditional][' . $name . ']" ' . $attributes . '>' . $data . '</textarea></div>';
            }
            else
            {                
                $content .= '<div><textarea id="' . $id . '_' . substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 ) . '" name="fields[ladditional][' . $group_name . '][' . $group_index . '][' . $name . ']" ' . $attributes . '>' . $data . '</textarea></div>';
            }

            if( isset( $desc ) && $desc != '' )
            {
                $content .= '<small class="list-field-desc"><i>' . $desc . '</i></small>';
            }

            $content .= '
        </div>';

        return $content;
    }

    function add_file_field( $obj, $data = '', $group_name = '', $group_index = 0 )
    {
        extract( $obj );

        $attrs = array( 'autocomplete' => 'off', 'data-filetype' => 'images', 'data-src' => HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-file.png' );

        if( isset( $attributes ) )
        {
            $attrs = array_merge( $attrs, $attributes );
        }

        if( isset( $show_on ) )
        {
            $show_on = sprintf( ' data-show-on-key="%s" data-show-on-value="%s"', $show_on[ 'key' ], $show_on[ 'value' ] );
        }
        else
        {
            $show_on = '';
        }

        if( isset( $show ) && $show === false )
        {
            $show_class = ' hidden';

            $attrs = array_merge( $attrs, array( 'disabled' => 'disabled' ) );
        }
        else
        {
            $show_class = '';
        }

        $attributes = $this->concat_attribute( $attrs );

        $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );

        $content = '
        <div class="list list-field' . $show_class . '"' . $show_on . '>
            <label class="list-field-title">' . $label . '</label>
            <div id="file-wrap-' . $hash . '"class="file-wrap uploaded">';

                if( empty( $group_name ) )
                {
                    $content .= '<input type="hidden" name="fields[ladditional][' . $name . ']" value="' . $data . '" autocomplete="off">';
                }
                else
                {                
                    $content .= '<input type="hidden" name="fields[ladditional][' . $group_name . '][' . $group_index . '][' . $name . ']" value="' . $data . '" autocomplete="off">';
                }
                
                $content .= '
                <figure class="file-not-image">';
            
                    $attach = $this->getFields( 'lumonata_attachment', 'lattach', array( 'lattach_id' => $data ) );

                    if( empty( $attach ) || ( !empty( $attach ) && !file_exists( IMAGE_DIR . '/Uploads/' . $attach ) ) )
                    {
                        $content .= '<img class="fileoutput" src="' . HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-file.png" alt="" /><span>No uploaded file yet</span>';
                    }
                    else
                    {
                        if( $attrs[ 'data-filetype' ] == 'doc' )
                        {
                            //-- Word file type
                            $content .= '<img class="fileoutput" src="' . HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-doc.png" alt="" /><span>' . $attach . '</span>';
                        }
                        elseif( $attrs[ 'data-filetype' ] == 'pdf' )
                        {
                            //-- PDF file type
                            $content .= '<img class="fileoutput" src="' . HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-pdf.png" alt="" /><span>' . $attach . '</span>';
                        }
                        elseif( $attrs[ 'data-filetype' ] == 'svg' )
                        {
                            //-- SVG file type
                            $content .= '<img class="fileoutput" src="' . HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-svg.png" alt="" /><span>' . $attach . '</span>';
                        }
                        else
                        {
                            //-- Other file type
                            $content .= '<img class="fileoutput" src="' . HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-file.png" alt="" /><span>' . $attach . '</span>';
                        }
                    }

                    $content .= '
                    <div class="progress"></div>
                </figure>
            </div>
            <label class="upload-file">
                <span class="ico-attachment"></span>';

                if( isset( $btntxt ) && $btntxt != '' )
                {
                    $content .= $btntxt;
                }
                else
                {
                    $content .= 'Browse';

                }

                $content .= '
                <input type="file" name="file_' . $hash . '" data-field="' . $name . '" data-hash="' . $hash . '" ' . $attributes . '>
            </label>';

            if( isset( $desc ) && $desc != '' )
            {
                $content .= '<small class="list-field-desc"><i>' . $desc . '</i></small>';
            }

            $content .= '
        </div>';

        return $content;   
    }

    function add_image_field( $obj, $data = '', $group_name = '', $group_index = 0 )
    {
        extract( $obj );

        $attrs = array( 'autocomplete' => 'off', 'data-filetype' => 'images', 'data-src' => HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=180&h=180&src=' . HT_SERVER . SITE_URL . '/images/Uploads/default.png' );

        if( isset( $attributes ) )
        {
            $attrs = array_merge( $attrs, $attributes );
        }

        if( isset( $show_on ) )
        {
            $show_on = sprintf( ' data-show-on-key="%s" data-show-on-value="%s"', $show_on[ 'key' ], $show_on[ 'value' ] );
        }
        else
        {
            $show_on = '';
        }

        if( isset( $show ) && $show === false )
        {
            $show_class = ' hidden';

            $attrs = array_merge( $attrs, array( 'disabled' => 'disabled' ) );
        }
        else
        {
            $show_class = '';
        }

        $attributes = $this->concat_attribute( $attrs );

        $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );

        $content = '
        <div class="list list-field' . $show_class . '"' . $show_on . '>
            <label class="list-field-title">' . $label . '</label>
            <div id="file-wrap-' . $hash . '"class="file-wrap uploaded">';

                if( empty( $group_name ) )
                {
                    $content .= '<input type="hidden" name="fields[ladditional][' . $name . ']" value="' . $data . '" autocomplete="off">';
                }
                else
                {                
                    $content .= '<input type="hidden" name="fields[ladditional][' . $group_name . '][' . $group_index . '][' . $name . ']" value="' . $data . '" autocomplete="off">';
                }

                $content .= '
                <figure class="file-image">';
            
                    $attach = $this->getFields( 'lumonata_attachment', 'lattach', array( 'lattach_id' => $data ) );

                    if( empty( $attach ) || ( !empty( $attach ) && !file_exists( IMAGE_DIR . '/Uploads/' . $attach ) ) )
                    {
                        $content .= '
                        <img class="fileoutput" src="' . HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=180&h=180&src=' . HT_SERVER . SITE_URL . '/images/Uploads/default.png" alt="" />
                        <div class="overlay hidden"><a class="delete" data-id="' . $data . '">Delete</a></div>';
                    }
                    else
                    {
                        $content .= '
                        <img class="fileoutput" src="' . HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=180&h=180&src=' . HT_SERVER . SITE_URL . '/images/Uploads/' . $attach . '" alt="" />
                        <div class="overlay"><a class="delete" data-id="' . $data . '">Delete</a></div>';
                    }

                    $content .= '
                    <div class="progress"></div>
                </figure>
            </div>
            <label class="upload-file">
                <span class="ico-attachment"></span>';

                if( isset( $btntxt ) && $btntxt != '' )
                {
                    $content .= $btntxt;
                }
                else
                {
                    $content .= 'Browse';

                }

                $content .= '
                <input type="file" name="file_' . $hash . '" data-field="' . $name . '" data-hash="' . $hash . '" ' . $attributes . '>
            </label>';

            if( isset( $desc ) && $desc != '' )
            {
                $content .= '<small class="list-field-desc no-float"><i>' . $desc . '</i></small>';
            }

            $content .= '
        </div>';

        return $content;
    }

    function add_gallery_field( $obj, $data = '', $group_name = '', $group_index = 0 )
    {
        extract( $obj );

        $attrs = array( 'autocomplete' => 'off', 'data-filetype' => 'images' );

        if( isset( $attributes ) )
        {
            $attrs = array_merge( $attrs, $attributes );
        }

        if( isset( $show_on ) )
        {
            $show_on = sprintf( ' data-show-on-key="%s" data-show-on-value="%s"', $show_on[ 'key' ], $show_on[ 'value' ] );
        }
        else
        {
            $show_on = '';
        }

        if( isset( $show ) && $show === false )
        {
            $show_class = ' hidden';

            $attrs = array_merge( $attrs, array( 'disabled' => 'disabled' ) );
        }
        else
        {
            $show_class = '';
        }

        $attributes = $this->concat_attribute( $attrs );

        $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );

        $content = '
        <div class="list list-field' . $show_class . '"' . $show_on . '>
            <label class="list-field-title">' . $label . '</label>
            <div id="file-drop-zone-' . $hash . '" class="file-drop-zone">';

                $data = json_decode( $data, true );

                if( $data !== null && json_last_error() === JSON_ERROR_NONE )
                {
                    $content .= '
                    <div class="drag-drop" style="display:none;">
                        <span></span>
                        <b>Drag & drop your images here</b>
                    </div>';

                    foreach( $data as $attach_id )
                    {
                        $attach = $this->getFields( 'lumonata_attachment', 'lattach', array( 'lattach_id' => $attach_id ) );

                        if( !empty( $attach ) && file_exists( IMAGE_DIR . '/Uploads/' . $attach ) )
                        {
                            $content .= '                            
                            <figure class="item uploaded">
                                <input type="hidden" name="fields[ladditional][' . $name . '][]" value="' . $attach_id . '" autocomplete="off">
                                <img src="' . HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=100&h=100&src=' . HT_SERVER . SITE_URL . '/images/Uploads/' . $attach . '" alt="" />
                                <div class="action">
                                    <a class="delete" data-id="' . $attach_id . '">Delete</a>
                                </div>
                                <div class="progress"></div>
                            </figure>';
                        }
                    }
                }
                else
                {
                    $content .= '
                    <div class="drag-drop">
                        <span></span>
                        <b>Drag & drop your images here</b>
                    </div>';
                }

                $content .= '
            </div>
            <div id="cloned-wrapp-' . $hash . '" class="cloned-wrapp">
                <figure class="item">
                    <input type="hidden" autocomplete="off">
                    <img src="' . HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=100&h=100&src=' . HT_SERVER . SITE_URL . '/images/Uploads/default.png" alt="" />
                    <div class="action">
                        <a class="delete">Delete</a>
                    </div>
                    <div class="progress"></div>
                </figure>
            </div>
            <label class="upload-gallery">
                <span class="ico-attachment"></span>';

                if( isset( $btntxt ) && $btntxt != '' )
                {
                    $content .= $btntxt;
                }
                else
                {
                    $content .= 'Browse';

                }

                $content .= '
                <input type="file" name="file_' . $hash . '" data-field="' . $name . '" data-hash="' . $hash . '" ' . $attributes . ' multiple>
            </label>';

            if( isset( $desc ) && $desc != '' )
            {
                $content .= '<small class="list-field-desc no-float"><i>' . $desc . '</i></small>';
            }

            $content .= '
        </div>';

        return $content;
    }

    function add_repeater_field( $obj, $data = '' )
    {
        extract( $obj );

        if( isset( $items ) && !empty( $items ) )
        {
            if( isset( $show_on ) )
            {
                $show_on = sprintf( ' data-show-on-key="%s" data-show-on-value="%s"', $show_on[ 'key' ], $show_on[ 'value' ] );
            }
            else
            {
                $show_on = '';
            }

            if( isset( $show ) && $show === false )
            {
                $show_class = ' hidden';

                $attrs = array_merge( $attrs, array( 'disabled' => 'disabled' ) );
            }
            else
            {
                $show_class = '';
            }

            $content = '
            <div class="list list-field' . $show_class . '"' . $show_on . '>';

                if( isset( $label ) && !empty( $label ) )
                {
                    $content .= '<p class="repeater-group-title">' . $label . '</p>';
                }

                $content .= '
                <div class="repeater-item-wrapp accordion-field">';

                    if( empty( $data ) )
                    {
                        $content .= $this->add_repeater_items( $items, $obj );
                    }
                    else
                    {
                        $data = json_decode( $data, true );

                        if( $data !== null && json_last_error() === JSON_ERROR_NONE )
                        {
                            foreach( $data as $groupidx => $group )
                            {
                                $content .= $this->add_repeater_items( $items, $obj, $group );
                            }

                        }
                    }

                    $content .= '
                </div>';

                if( isset( $desc ) && $desc != '' )
                {
                    $content .= '<small class="list-field-desc"><i>' . $desc . '</i></small>';
                }

                $content .= '
            </div>';

            return $content;
        }
    }

    function add_repeater_items( $items, $obj, $group = '' )
    {
        extract( $obj );

        $opt = array( 'title' => 'Item', 'add_text' => 'Add Item', 'remove_text' => 'Delete Item' );

        if( isset( $option ) )
        {
            $opts = array_merge( $opt, $option );
        }

        $groupidx = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );

        $content = '
        <div class="repeater-item" data-section-id="' . $groupidx . '">
            <div class="repeater-anchor" data-title="' . $opts[ 'title' ] . '">
                <h4></h4>
            </div>
            <div class="repeater-body">
                <div class="repeater-field clearfix">';

                    foreach( $items as $item )
                    {
                        if( isset( $group[ $item[ 'name' ] ] ) )
                        {
                            $data = $group[ $item[ 'name' ] ];
                        }
                        else
                        {
                            $data = null;
                        }

                        //-- Field Type
                        if( $item[ 'type' ] == 'text' )
                        {
                            $content .= $this->add_text_field( $item, $data, $name, $groupidx );
                        }
                        elseif( $item[ 'type' ] == 'select' )
                        {
                            $content .= $this->add_select_field( $item, $data, $name, $groupidx );
                        }
                        elseif( $item[ 'type' ] == 'checkbox' )
                        {
                            $content .= $this->add_checkbox_field( $item, $data, $name, $groupidx );
                        }
                        elseif( $item[ 'type' ] == 'radio' )
                        {
                            $content .= $this->add_radio_field( $item, $data, $name, $groupidx );
                        }
                        elseif( $item[ 'type' ] == 'textarea' )
                        {
                            $content .= $this->add_textarea_field( $item, $data, $name, $groupidx );
                        }
                        elseif( $item[ 'type' ] == 'file' )
                        {
                            $content .= $this->add_file_field( $item, $data, $name, $groupidx );
                        }
                        elseif( $item[ 'type' ] == 'image' )
                        {
                            $content .= $this->add_image_field( $item, $data, $name, $groupidx );
                        }
                        elseif( $item[ 'type' ] == 'gallery' )
                        {
                            $content .= $this->add_gallery_field( $item, $data, $name, $groupidx );
                        }
                    }

                    $content .= '
                </div>
                <div class="repeater-action">
                    <button type="button" class="add-item" data-object="' . base64_encode( json_encode( $obj ) ) . '" data-items="' . base64_encode( json_encode( $items ) ) . '">' . $opts[ 'add_text' ] . '</button>
                    <button type="button" class="delete-item" data-id="' . $groupidx . '">' . $opts[ 'remove_text' ] . '</button>
                </div>
            </div>
        </div>';

        return $content;
    }

    function add_gmail_xoauth_field( $obj, $data = '' )
    {
        require_once( INCLUDE_DIR . '/vendor/autoload.php' );

        extract( $obj );

        $attrs = array( 'type' => 'text', 'class' => 'text gmail-xoauth' );

        if( isset( $attributes ) )
        {
            $attrs = array_merge( $attrs, $attributes );
        }

        if( isset( $show_on ) )
        {
            $show_on = sprintf( ' data-show-on-key="%s" data-show-on-value="%s"', $show_on[ 'key' ], $show_on[ 'value' ] );
        }
        else
        {
            $show_on = '';
        }

        if( isset( $show ) && $show === false )
        {
            $show_class = ' hidden';

            $attrs = array_merge( $attrs, array( 'disabled' => 'disabled' ) );
        }
        else
        {
            $show_class = '';
        }

        $attributes = $this->concat_attribute( $attrs );

        $content = '
        <div class="list list-field' . $show_class . '"' . $show_on . '>
            <label class="list-field-title">' . $label . '</label>
            <input value="' . $data . '" id="' . $id . '" name="fields[ladditional][' . $name . ']" ' . $attributes . '>';

                $client_id     = $this->getSettingValue( $options[ 'client_id' ] );
                $client_secret = $this->getSettingValue( $options[ 'client_secret' ] );
                $redirect_uri  = $this->getSettingValue( $options[ 'redirect_uri' ] );

                if( $client_id != '' && $client_secret != '' && $redirect_uri != '' )
                {               
                    //-- LOAD google API
                    $client = new Google( array(
                        'clientId'     => $client_id,
                        'clientSecret' => $client_secret,
                        'redirectUri'  => $redirect_uri,
                        'accessType'   => 'offline'
                    ));

                    //-- REQUEST authorization from the user.
                    $authUrl = $client->getAuthorizationUrl( array(
                        'scope' => array( 'https://mail.google.com/' ),
                        'prompt' => 'consent'
                    ));

                    $content .= '
                    <div class="clearfix">
                        <a class="connect" href="' . $authUrl . '">Generate Token</a>
                    </div>';
                }

            $content .= '            
        </div>';

        return $content;
    }

    function add_instagram_xoauth_field( $obj, $data = '' )
    {
        require_once( INCLUDE_DIR . '/vendor/autoload.php' );

        extract( $obj );

        $attrs = array( 'type' => 'text', 'class' => 'text instagram-xoauth' );

        if( isset( $attributes ) )
        {
            $attrs = array_merge( $attrs, $attributes );
        }

        if( isset( $show_on ) )
        {
            $show_on = sprintf( ' data-show-on-key="%s" data-show-on-value="%s"', $show_on[ 'key' ], $show_on[ 'value' ] );
        }
        else
        {
            $show_on = '';
        }

        if( isset( $show ) && $show === false )
        {
            $show_class = ' hidden';

            $attrs = array_merge( $attrs, array( 'disabled' => 'disabled' ) );
        }
        else
        {
            $show_class = '';
        }

        $attributes = $this->concat_attribute( $attrs );

        $content = '
        <div class="list list-field' . $show_class . '"' . $show_on . '>
            <label class="list-field-title">' . $label . '</label>
            <input value="' . $data . '" id="' . $id . '" name="fields[ladditional][' . $name . ']" ' . $attributes . '>';

                $client_id     = $this->getSettingValue( $options[ 'client_id' ] );
                $client_secret = $this->getSettingValue( $options[ 'client_secret' ] );
                $redirect_uri  = $this->getSettingValue( $options[ 'redirect_uri' ] );

                if( $client_id != '' && $client_secret != '' && $redirect_uri != '' )
                {               
                    //-- LOAD google API
                    $client = new League\OAuth2\Client\Provider\Instagram( array(
                        'clientId'     => $client_id,
                        'clientSecret' => $client_secret,
                        'redirectUri'  => $redirect_uri
                    ));

                    $authUrl = $client->getAuthorizationUrl();

                    $content .= '
                    <div class="clearfix">
                        <a class="connect" href="' . $authUrl . '">Generate Token</a>
                    </div>';
                }

            $content .= '            
        </div>';

        return $content;
    }

    function add_rate_field( $obj, $data = '' )
    {
        extract( $obj );

        $attrs = array( 'class' => 'full' );

        if( isset( $attributes ) )
        {
            $attrs = array_merge( $attrs, $attributes );
        }

        if( isset( $show_on ) )
        {
            $show_on = sprintf( ' data-show-on-key="%s" data-show-on-value="%s"', $show_on[ 'key' ], $show_on[ 'value' ] );
        }
        else
        {
            $show_on = '';
        }

        if( isset( $show ) && $show === false )
        {
            $show_class = ' hidden';

            $attrs = array_merge( $attrs, array( 'disabled' => 'disabled' ) );
        }
        else
        {
            $show_class = '';
        }

        $attributes = $this->concat_attribute( $attrs );

        $content = '
        <div class="list list-field' . $show_class . '"' . $show_on . '>
            <label class="list-field-title">' . $label . '</label>';

            if( empty( $group_name ) )
            {
                $content .= '
                <div class="rating">
                    <input type="radio" id="' . $id . '-star5" name="fields[ladditional][' . $name . ']" value="5" ' . $attributes . ( intval( $data ) == 5 ? ' checked' : '' ) . '/><label class="full" for="' . $id . '-star5" title="Excellent - 5 stars"></label>
                    <input type="radio" id="' . $id . '-star4" name="fields[ladditional][' . $name . ']" value="4" ' . $attributes . ( intval( $data ) == 4 ? ' checked' : '' ) . '/><label class="full" for="' . $id . '-star4" title="Pretty good - 4 stars"></label>
                    <input type="radio" id="' . $id . '-star3" name="fields[ladditional][' . $name . ']" value="3" ' . $attributes . ( intval( $data ) == 3 ? ' checked' : '' ) . '/><label class="full" for="' . $id . '-star3" title="Good - 3 stars"></label>
                    <input type="radio" id="' . $id . '-star2" name="fields[ladditional][' . $name . ']" value="2" ' . $attributes . ( intval( $data ) == 2 ? ' checked' : '' ) . '/><label class="full" for="' . $id . '-star2" title="Kinda bad - 2 stars"></label>
                    <input type="radio" id="' . $id . '-star1" name="fields[ladditional][' . $name . ']" value="1" ' . $attributes . ( intval( $data ) == 1 ? ' checked' : '' ) . '/><label class="full" for="' . $id . '-star1" title="Bad - 1 stars"></label>
                </div>';
            }
            else
            {
                $random   = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );     
                $content .= '
                <div class="rating">
                    <input type="radio" id="' . $id . '-' . $random . '-star5" name="fields[ladditional][' . $name . ']" value="5" ' . $attributes . ( intval( $data ) == 5 ? ' checked' : '' ) . '/><label class="full" for="' . $id . '-' . $random . '-star5" title="Excellent - 5 stars"></label>
                    <input type="radio" id="' . $id . '-' . $random . '-star4" name="fields[ladditional][' . $name . ']" value="4" ' . $attributes . ( intval( $data ) == 4 ? ' checked' : '' ) . '/><label class="full" for="' . $id . '-' . $random . '-star4" title="Pretty good - 4 stars"></label>
                    <input type="radio" id="' . $id . '-' . $random . '-star3" name="fields[ladditional][' . $name . ']" value="3" ' . $attributes . ( intval( $data ) == 3 ? ' checked' : '' ) . '/><label class="full" for="' . $id . '-' . $random . '-star3" title="Good - 3 stars"></label>
                    <input type="radio" id="' . $id . '-' . $random . '-star2" name="fields[ladditional][' . $name . ']" value="2" ' . $attributes . ( intval( $data ) == 2 ? ' checked' : '' ) . '/><label class="full" for="' . $id . '-' . $random . '-star2" title="Kinda bad - 2 stars"></label>
                    <input type="radio" id="' . $id . '-' . $random . '-star1" name="fields[ladditional][' . $name . ']" value="1" ' . $attributes . ( intval( $data ) == 1 ? ' checked' : '' ) . '/><label class="full" for="' . $id . '-' . $random . '-star1" title="Bad - 1 stars"></label>
                </div>';
            }

            if( isset( $desc ) && $desc != '' )
            {
                $content .= '<small class="list-field-desc"><i>' . $desc . '</i></small>';
            }

            $content .= '
        </div>';

        return $content;
    }

    function add_fonticon_field( $obj, $data = '' )
    {

    }

    function get_user_name( $userid = '' )
    {
        $data = $this->getFields( 'lumonata_user', array( 'lusername', 'lname' ), array( 'luser_id' => $userid ) );

        if( empty( $data[ 'lname' ] ) )
        {
            return $data[ 'lusername' ];
        }
        else
        {
            return $data[ 'lname' ];
        }
    }

    function get_user_avatar( $userid = '', $width = null, $height = null )
    {
        $param = array();

        if( is_array( $userid ) )
        {
            $attach = $this->getFields( 'lumonata_user', 'limage', $userid );
        }
        else
        {
            $attach = $this->getFields( 'lumonata_user', 'limage', array( 'luser_id' => $userid ) );
        }

        if( $width !== null && $width > 0 )
        {
            $param[] = 'w=' . $width;
        }

        if( $height !== null && $height > 0 )
        {
            $param[] = 'h=' . $height;
        }

        if( !empty( $attach ) && file_exists( IMAGE_DIR . '/Users/' . $attach ) )
        {
            if( empty( $param ) )
            {
                return HT_SERVER . SITE_URL . '/images/Users/' . $attach;
            }
            else
            {
                return HT_SERVER . ADMIN_URL . '/functions/mthumb.php?' . implode( '&', $param ) . '&src=' . HT_SERVER . SITE_URL . '/images/Users/' . $attach;
            }
        }
        else
        {
            $name = $this->getFields( 'lumonata_user', 'lname', array( 'luser_id' => $userid ) );

            return 'https://ui-avatars.com/api/?name=' . $name . 'background=random';
        }
    }

    function get_video_source( $lattach_id = '' )
    {
        $a = $this->getFields( 'lumonata_attachment', array( 'lattach', 'lmimetype' ), array( 'lattach_id' => $lattach_id ) );        

        if( !empty( $a[ 'attach' ] ) && file_exists( IMAGE_DIR . '/Uploads/' . $a[ 'attach' ] ) )
        {
            return '<source src="' . HT_SERVER . SITE_URL . '/images/Uploads/' . $a[ 'attach' ] . '" type="' . $a[ 'lmimetype' ] . '">';
        }
    }

    function get_attachment_url( $lattach_id = '', $width = null, $height = null )
    {
        $attach = $this->getFields( 'lumonata_attachment', 'lattach', array( 'lattach_id' => $lattach_id ) );
        $param  = array();

        if( $width !== null && $width > 0 )
        {
            $param[] = 'w=' . $width;
        }

        if( $height !== null && $height > 0 )
        {
            $param[] = 'h=' . $height;
        }

        if( !empty( $attach ) && file_exists( IMAGE_DIR . '/Uploads/' . $attach ) )
        {
            if( empty( $param ) )
            {
                return HT_SERVER . SITE_URL . '/images/Uploads/' . $attach;
            }
            else
            {
                return HT_SERVER . ADMIN_URL . '/functions/mthumb.php?' . implode( '&', $param ) . '&src=' . HT_SERVER . SITE_URL . '/images/Uploads/' . $attach;
            }
        }
        else
        {
            return 'https://via.placeholder.com/' . $width . 'x' . $height . '/666/666';
        }
    }

    function getTypeOption( $type = '' )
    {
        return $this->set_static_option( array( 
            'adult'  => 'Adult',
            'child'  => 'Child',
            'infant' => 'Infant'
        ), $type, false );
    }

    function getTitleOption( $ltitle = '' )
    {
        return $this->set_static_option( array( 
            'Mr.'   => 'Mr.',
            'Mrs.'  => 'Mrs.',
            'Miss.' => 'Miss.' 
        ), $ltitle, false );
    }

    function getListingTitle( $data = array() )
    {
        $title = array();

        if( isset( $data[ 'title_part_1' ] ) && !empty( $data[ 'title_part_1' ] ) )
        {
            $title[] = $data[ 'title_part_1' ];
        }

        if( isset( $data[ 'title_part_2' ] ) && !empty( $data[ 'title_part_2' ] ) )
        {
            $title[] = $data[ 'title_part_2' ];
        }

        if( isset( $data[ 'title_part_3' ] ) && !empty( $data[ 'title_part_3' ] ) )
        {
            $title[] = $data[ 'title_part_3' ];
        }

        if( isset( $data[ 'title_part_4' ] ) && !empty( $data[ 'title_part_4' ] ) )
        {
            $title[] = $data[ 'title_part_4' ];
        }

        if( empty( $title ) )
        {
            return $data[ 'ltitle' ];
        }
        else
        {
            return implode( ' ', $title );
        }
    }

    function getNumberOfGuest( $data = array() )
    {
        $guest = array();

        if( isset( $data[ 'ladult' ] ) && $data[ 'ladult' ] > 0 )
        {
            $guest[] = $data[ 'ladult' ] . ( $data[ 'ladult' ] > 1 ? ' adults' : ' adult' );
        }

        if( isset( $data[ 'lchild' ] ) && $data[ 'lchild' ] > 0 )
        {
            $guest[] = $data[ 'lchild' ] . ( $data[ 'lchild' ] > 1 ? ' childs' : ' child' );
        }

        if( isset( $data[ 'linfant' ] ) && $data[ 'linfant' ] > 0 )
        {
            $guest[] = $data[ 'linfant' ] . ( $data[ 'linfant' ] > 1 ? ' babies' : ' baby' );
        }

        return implode( ', ', $guest );
    }

    function getBookingTrips( $data = array() )
    {
        if( isset( $data[ 'ltrips' ] ) && !empty( $data[ 'ltrips' ] ) )
        {
            $trips = array();

            foreach( $data[ 'ltrips' ] as $dt )
            {
                if( isset( $dt[ 'itinerary' ] ) )
                {
                    $trips[] = $dt[ 'itinerary' ] . ', ' . $dt[ 'duration' ];
                }
            }

            return implode( '<br />', $trips );
        }
    }

    function getNumberOfRooms( $data = array() )
    {
        if( isset( $data[ 'lrooms' ] ) && !empty( $data[ 'lrooms' ] ) )
        {
            $rooms = array();

            foreach( $data[ 'lrooms' ] as $dt )
            {
                if( isset( $dt[ 'name' ] ) )
                {
                    $rooms[] = $dt[ 'num' ] . ' ' . $dt[ 'name' ];
                }
                else
                {
                    $room = $this->getPostTerms( array( 'lterm_id' => $dt[ 'id' ] ) );

                    $rooms[] = $dt[ 'num' ] . ' ' . $room[ 'lname' ];
                }
            }

            return implode( '<br />', $rooms );
        }
    }

    function getNumberOfAddons( $data = array() )
    {
        if( isset( $data[ 'laddons' ] ) && !empty( $data[ 'laddons' ] ) )
        {
            $rooms = array();

            foreach( $data[ 'laddons' ] as $dt )
            {
                if( isset( $dt[ 'name' ] ) )
                {
                    $rooms[] = $dt[ 'num' ] . ' ' . $dt[ 'name' ];
                }
                else
                {
                    $room = $this->getPostTerms( array( 'lterm_id' => $dt[ 'id' ] ) );

                    $rooms[] = $dt[ 'num' ] . ' ' . $room[ 'lname' ];
                }
            }

            return implode( '<br />', $rooms );
        }
    }

    function getRoomLabel( $data = array() )
    {
        if( $data[ 'lposts_data' ][ 'page_template' ] == '1' )
        {
            return 'CABINS';
        }
        else
        {
            return 'ROOMS';
        }
    }

    function roomPrice( $acco, $room, $checkin = '', $checkout = '' )
    {
        if( isset( $acco[ 'policy' ] ) && !empty( $acco[ 'policy' ] ) )
        {
            $policy = $acco[ 'policy' ];
        }
        else
        {
            $policy = '0';
        }

        $rate  = 0;
        
        if( $policy == '0' )
        {
            if( $checkin != '' && $checkout != '' )
            {
                $s = 'SELECT a.lrate FROM lumonata_calendar as a WHERE a.lpost_id = %d AND a.lterm_id = %d AND a.ldate BETWEEN %s AND %s';
                $q = parent::prepare_query( $s, $acco[ 'lpost_id' ], $room[ 'lterm_id' ], $checkin, $checkout );
                $r = parent::query( $q );

                if( parent::num_rows( $r ) > 0 )
                {
                    $arate = array();

                    while( $d = parent::fetch_array( $r ) )
                    {
                        $arate[] = $d[ 'lrate' ];
                    }

                    $rate = max( $arate );
                }
                else
                {
                    $rate = $room[ 'room_price' ];
                }
            }
            else
            {
                $rate = $this->getFields( 'lumonata_calendar', 'lrate', array(
                    'lpost_id' => $acco[ 'lpost_id' ],
                    'lterm_id' => $room[ 'lterm_id' ],
                    'ldate'    => date( 'Y-m-d' ),
                ));
            }

            if( empty( $rate ) )
            {
                $rate = $room[ 'room_price' ];
            }
        }
        else
        {
            $rate = $room[ 'room_price' ];
        }

        return $rate;
    }

    function uniqueCode( $prefix = '', $table = '', $field = '' )
    {
        $p = $prefix . date('y') . date('m');
        $n = strlen( $p );
        $l = $n + 1;

        $q = parent::prepare_query( 'SELECT MAX( substring( ' . $field . ', ' . $l . ', 4 ) ) as tnumber FROM ' . $table . ' WHERE substring( ' . $field . ', 1, ' . $n . ' ) = %s', $p );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            $number = 1;
        }
        else
        {
            $d = parent::fetch_array( $r );

            $number = $d['tnumber'] == NULL ? 1 : intval( $d['tnumber'] ) + 1;
        }

        if( $number > 9999 )
        {
            $number = 1;
        }
        elseif( $number < 10 )
        {
            $number = '000' . $number;
        }
        elseif( $number < 100 )
        {
            $number = '00' . $number;
        }
        elseif( $number < 1000 )
        {
            $number = '0' . $number;
        }
        else
        {
            $number = $number;
        }

        $unique_code = $p . $number;

        return $unique_code;
    }

    function getNumDayNight( $checkin, $checkout )
    {
        $checkin  = new DateTime( $checkin );
        $checkout = new DateTime( $checkout );

        $nights = $checkout->diff( $checkin )->format( '%a' );
        $nights = $nights == 0 ? 1 : $nights;

        if( $nights == 1 )
        {
            return '1D1N';
        }
        else
        {
            return sprintf( '%sD%sN', ( $nights + 1 ), $nights ) ;
        }
    }

    function getNumNight( $checkin, $checkout )
    {
        $checkin  = new DateTime( $checkin );
        $checkout = new DateTime( $checkout );

        $nights = $checkout->diff( $checkin )->format( '%a' );
        $nights = $nights == 0 ? 1 : $nights;

        return $nights;
    }

    function generateInvoice( $lbooking_id, $stream = true )
    {
        $data = $this->getBooking( $lbooking_id );

        if( empty( $data ) === false )
        {
            require_once( INCLUDE_DIR . '/vendor/autoload.php' );

            $dompdf = new Dompdf();

            $html = '
            <style type="text/css">
                .tg {
                    border-collapse: collapse;
                    border-spacing: 0;
                    width: 100%;
                }

                .tg td {
                    border-color: #ddd;
                    border-style: solid;
                    border-width: 1px;
                    font-family: Arial, sans-serif;
                    font-size: 14px;
                    overflow: hidden;
                    padding: 10px 5px;
                    word-break: normal;
                }

                .tg th {
                    border-color: #ddd;
                    border-style: solid;
                    border-width: 1px;
                    font-family: Arial, sans-serif;
                    font-size: 14px;
                    font-weight: normal;
                    overflow: hidden;
                    padding: 10px 5px;
                    word-break: normal;
                }

                .tg .tg-1wig {
                    font-weight: bold;
                    text-align: left;
                    vertical-align: top
                }

                .tg .tg-j3sv {
                    background-color: #eff5fc;
                    font-weight: bold;
                    text-align: right;
                    vertical-align: top
                }

                .tg .tg-ovpe {
                    background-color: #eff5fc;
                    color: #cb0000;
                    font-weight: bold;
                    text-align: right;
                    vertical-align: top
                }

                .tg .tg-c7at {
                    background-color: #efefef;
                    font-weight: bold;
                    text-align: right;
                    vertical-align: top
                }

                .tg .tg-lqy6 {
                    text-align: right;
                    vertical-align: top
                }

                .tg .tg-0lax {
                    text-align: left;
                    vertical-align: top;
                    padding:5px;
                }

                .tg .tg-gt19 {
                    font-size: 100%;
                    text-align: right;
                    vertical-align: top
                }

                .tg .tg-b3sw {
                    background-color: #efefef;
                    font-weight: bold;
                    text-align: left;
                    vertical-align: top
                }

                .tg .tg-b4sw {
                    font-weight: bold;
                    text-align: left;
                    vertical-align: top
                }

                .tg .tg-nwi2 {
                    background-color: #eff5fc;
                    font-weight: bold;
                    text-align: left;
                    vertical-align: top
                }

                .tg .tg-head-border{
                    border: 0;
                    border-bottom:3px solid #f5f5f5;
                    padding-bottom:20px;
                }

                .tg .tg-no-border{
                    border: 0;
                }

                .tg .logo{
                    width:200px;
                }
            </style>
            <table class="tg">
                <thead>
                    <tr>
                        <th class="tg-0lax tg-head-border" colspan="3">
                            <img class="logo" alt="NomadSurfers" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAASABIAAD/4QBARXhpZgAATU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAAqACAAQAAAABAAABwqADAAQAAAABAAAAUQAAAAD/7QA4UGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAAA4QklNBCUAAAAAABDUHYzZjwCyBOmACZjs+EJ+/8AAEQgAUQHCAwERAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/bAEMAAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/bAEMBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAf/dAAQAOf/aAAwDAQACEQMRAD8A/v4oAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD/9D+/igAoAKAI2Iz9/b2x6//AF/p/hQBVN3D5pgW4gMwODEZk8wAHr5Ycv27jpnr/EAWw6kDn0B+v1oAfQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUAFABQAUANLqpwSAf8/5/wD1UAKCCMjoaAFoAKACgAoAKACgAoAKACgAoAKAEyB1I/PH+f8APrQAtABQAUAFABQAUAFABQAUAFABQB//0f7+KACgCtPMEj3Kw6kZz9M8dzjpyPx6UAfjL+2n/wAFI/EOlePY/wBkr9h7QIPjX+1Z4niW0ur/AE5zqPgP4O2s4xPr3jvVrLfp1rc2gKJHpF5rGjXjTXVu4kO0K4B4Pc/8EuP22tK+H9v8WtD/AG/PjDqf7a8TnxBeT6lr123wC1fV8R3DeGIvAJukvbLQp7pZrcGXxpNFFbzgM7bWLAH0b+xB/wAFKf8AhY3i6f8AZg/a20KT4F/tdeDP+JRqWheIC+j+F/ik1ttZNf8AhvqWpR2tnrkN3ayRXEtjpN7rM0EkN95lw32acIAfsQsibsbmzjOD6dOmPX0P4d6AJQwOcHp+H+f8+tAC0AISB1/x/wA9P84oAMjgevPT/OKAFoATcM4zz9PSgBaACgAoAKACgAoAKACgAoAKACgAoAQkDr/LP+f8+lAC0AFACZGcd/p/WgBaACgAoA/Pv9qH9uPRf2bP2if2XPgHqfgzU/EV7+054rm8LaVrtnqcVnbeG5oElLXV9avYXTXqExEeXHcWrYYYJ2ncAffsJO1ck/dB598du1AExIHX+Wf8/wCfSgBA6k4B59O/+fw/PNADqACgAoAKACgAoATcOeenXj0oAw/EWuad4d0fUde1a8hsNK0izn1DUb24lSGC2tLeJpZJJHcqigKOrHBJA4x8wB/DJ+3r/wAHNX7RfiX41+IPgr/wT28DW+p6BouoT6Fa+LZfDereM/Efi2+t5ZUmvvDuk6LPpt/a2W3yjG+2/RiJAJTtY0Afvd/wQu/aM/ba/aR/Zs8Y+LP23vD+u6F8QNP8Zx2fh4a94T17whdXeg3Ed7OJfsGvyy3UqxssEYdG2AYGOQGAP3IBwoyf0/8A1dv8nrQAeYp6H9M/5/z6UAKCD0/w/wA9f85oAWgAJxzQAgYHoffpQAFgOpxQAm9fX9KAHUAFABQB/9L+/igCpf3UFlZXV5dTx21tawSXFxcSttjhhiXfJI7HoqKCxPYCgD+ff42ftsfH/wDbz+KfiT9kb/gnhHJo/gbSnk0z4xftf3dmW8MeHLGd57XUNG8AG5iuJNR8SfZba6WG5l0T7JBPd6dKl+vzugB+k/7Fv7CXwi/Yx+H48N+B7K417xpq1xJqnjz4oeJGOp+N/Guv3shudRv9R1u7e4vUtZbt3a302K4SxgiWBIrWFYkRAD7f8tzGAFIPYYwVHp/nP9FAPgb9tr9gH4Q/tpeFLeDxVHdeD/iX4VlGq/Dj4v8AhN30jxt4N16DcLa6i1SyeC5vLJhLPDcWF5JcWrw3MpNs7BBQB8Ifs5/t0fHH9lH4taB+xj/wUR05kv72RNG+DH7Uum2bt4N+KVnbyRWVhZ+IzbRQvpnimdJdPecLotrYyXF3NtuXSLKgH7x6Zcx3lvHdQyJLFcRpNHIhJSSOVEeKRchSVkjKuDgcMM9KANKgBrkBST0AP8v/ANdAHGR+OfC03iK48JQ6/pUnimztFvrjw+l3GdWgsSsTC7ls9/nJblZoWEhTYRKhG4MNoB5d47/ao/Z4+GVzdWXj74zeAPC9/YyLFe2Go+ILb7dayudojns7dp7iNiT0MPHU4FAHQ/D749/Bv4qSBPh18TvBfjKZo1mFtoWuWd3diEgHzGtBKLoA7lzmHI3YIXnaAeweaoBLEADqc5H6Lx+Z/XKgFee7hjGfMAAGdxOFHc7j8oAABJJAAHOcUAfP3jj9rL9nD4a3N1ZeOfjZ8PPDl7YyCK7srzxDbSXlvKzMoSW2tGuZ0bcpBUplSDu6igDo/AH7RnwL+J9xDZeAPiz4F8WX1xAbm3sdI8QWM99NDkgslm8qXRwRyvkBwMkpgE0Aew/a4ugIJ5456jgjtjr/AFGelAHOeJfG3hjwfawXvirXtK8O2l1dQ2Vtc6texWUM95cSRxQWsUkzIrzTSSokaKSzO6qASRQBxvjv46fCX4XQR3HxF+I/hDwXHPC1xAviDWrSwnngTG6WG2eY3MiDcvKRHduG0nJFAHN+Af2o/wBn34pXdrp/w8+MXgLxZqV8ZVs9N0vXrdtQuTBG0swhspmguX8uJHkfEPCKxGcGgD1vX/F/hzwrpV1rPibW9L8P6TZR+Zdanq95DYWNui9XmubiSOKMcE/M+T2DYyoB8/Qftr/spXOtyeHYf2gfhk+sRY32h8SQKMksoC3L7LRzlTwlwx491oA+kdJ1nT9asbbU9KvrTUtOvYkns72xuY7q1uYJF3RywTwtLG6OrBgVJyDz1BoA0jOq9eMkgc9x1/hP+ew4NAHAeNfin8PPh5bQ3vjvxt4Z8H2k7mOCfxBq9ppaTOFZysQupVaRtqOSFycIfTLAHjWl/tu/sm67qNzpOlftA/DK41Czk8u4gPiKC3COCykLNciC3l+ZTzFLIvGQx3UAe/t4u8ProzeIv7b0x9AW1+3nWor6GXTfsQTe119sjZoPJVPmLhsYyeMEKAO8NeKdC8X6ZZ6/4Y1iw1/RNQjWWx1XS7lbqxuo2UMHhnj3JIpVgQytgqeMAgqAb73UabwzBSgO7ORt46knoBnOTwPwoA+fvHH7V/7OXw2nvbXx18a/h54cvNOcR39le+IrV720kO793NaWrT3CuNrBl8rKlSGwTigDa8B/tG/A34oTW9r8Pvi14E8W3d1D9ot7LSPENnNfTQ/Nl0snljuuAjEqYdwCk7QBhgD8Vf8Agqo2/wD4KN/8Elm9PjDfqfr5V9689c/5xQB/QkswWNfVUHf0UdemPz/LHygH5yftRf8ABVv9hf8AZC1CbQPjJ8dvCul+K4RmXwtZTXWo6tCQMlJRZWtzawuMgGOa4VwTg5x8wB5d+zp/wW6/4JyftH+IrPwl4L/aA8N6f4n1GZbfT9J8Qm906a9mkYJHFFcPYCxBZyqgy3MfzHsM0AfrCur2EtkmpQXdvPYS2/2uK7hlWa3ktvLM3nRyxFkePygXDKWyoyP9oA+M/Bv/AAUb/Y0+IHxuvf2c/CHx18Ia38ZNPvtS0y58E2v9q/2kt/pMogv7UNLpyWplt5WCOBckbsbSwOFAPt8cgH1H+fT+X5UAeE/H/wDaY+B/7LvhO18c/Hf4g6L8OfCl5qlro1vrOufbPssmo3gkMFuPsdrdS7nETkZRV+XlugoAv/BH4+/Cj9pHwLYfE34J+NNL8feBtTeSOx8QaN9p+xTvE7JIqfa4bSYEMh+9Cvr3AUA8Evf+Cj37GOm/HS3/AGab/wCOvhO2+Nl3q1voMHgWUaqNVk1e7YpBYiT+zTZCaRgyruvFXK8vwGoA4/4xf8FTv2Fvgf8AEDRvhT46+P8A4NtviJr3iHSvC9l4VsJr7UNQOs61qFvpljaSS2djc2ccj3VzEh33Shd3OMnaAfCn/BY7/gpZ+zL8Nv2Qv2pPgvovxu8PWvx/uvhnBa6R4Jtn1WLxH5/iKGw1LTRbSx2K2wluNOYyKwvEwjEFlJIoA/mY/wCDa34zfsG/ATxD8Yvih+1T448B+FPiRdNbaV4G/wCE00+61RbbTEluZLi701DpepW8FzIl9OJLg+VP8iHOVRaAP70v2b/2of2eP2nfCWpeLf2ePH3h34geFtG1IaRqOo+HILmC0tNR2ysLZ0ubKxO7bDIQUQphcbqAPN/2nP8Agoh+x1+yLbOPjx8cfBvgzVAAYtBmu7m91eU9kFrplretC+cDbc+TgnBzhgoB8E+Bv+Dhr/gmB448Sp4bh+PFjoU8twLSK+1iy1KKweRmwjb4NPmdVfg7mRcA/N0NAH7FfDj4qfDv4reF7Dxl8N/GOg+NfDOqQrPY6zoF8t5aTRSKsikgBZYmKsp2TRxOM4MeQTQBb+IvxL8FfCfwT4h+InxA16z8M+DfC1gdS17Xb/zvsmnWQkjh8+byIZptvmyIg2Rs25gPU0AfMfwf/wCChf7HXx58C+MPiV8Mfj14J8ReBfARhHi/xGLq8sdP0T7Sl1Lbi5fUrO0Z2misbt0SCOVyIGwuQu4Ay/2cf+CjH7Iv7V3xJ8X/AAu/Z++L2h/ErxR4I04al4gg0WLUFt7O0a6W1EnnXljZrJmdljBiLjJ7jJoAs/tT/wDBQ/8AZC/Y5thL8fPjR4U8Gaq0XnWvh2a5ubvW7pSgdSlpY2t35O5SuPtLRE71xkZKgHx58If+C+H/AATP+MfiW38J6X8ftE0DVby7WysV8QQahaw3dw5CxolxFYSxR+Y7KoaZ4kBYb2UfNQB+wvh/xRoHijTNP1fw7q+n63pWp20V1Yalpl1HeWd1bzIrxzRTws0bK6Opxu3LnDKrBgwB0NABQB//0/7+KAPI/j4Cfgr8UAMZPgrXuvTH2KTOemeO2eenFAH5w/8ABDDRdN0n/gmb+zcdKsrXT1vfDMeoXaWsKwrcXlzaWSzXMoXdvnlEaeZISxbYo4xQB+vI3dzn/P8Aur/n0/iAK73CxnDkA7tqjqWz0x7npjn075UA8z8Z/GH4WeAZbWx8dfEHwj4Qu9SfyLG28Q69YaVPdytnEcEd3NC0jkKx2oG+775oAyfG3ww+Evx28M6bpHjrwz4W+JHhOO/ste0m31a1tdX01b6yJksdSs2cSRCa3Mm+GZBkbsrj+IA9a0yxg061gsrWNILS0ghtbW3jULHb21vGkMEMag4WOKKNVVQMAAc9qANGgCKYAxsCSBg8jg9D04P+fXOGAP47/wBtb4y/GLwJ/wAFyviN8JP2cDHafH39oL9nrwZ8P/h/4lv1kfSvBMOo+GPAQ1DxbfJFJbySDTZre3jSJLuzZ2uQRcLtwwB+sfwn/wCCF/7CtppFn4j/AGgvhZoX7Sfx11UNqPxB+MnxTsbHxJ4n8U6/dxILyV7y/tJruOzjxstbaa6uDHl2EpD5UA+cf2uv+COfw6/Z28Gar+07/wAEyPD9r+zZ+0p8J1bxZbaf4Iji0jw18StGtphcaz4X8SafpUWnPdLc2haaCWW5kjjazRTbnINAH6y/sD/tUWX7Zv7KXwq/aAt9Kn0LUPGGhwr4n8Pzvuk0TxNb21tLqemSN5cOXtzdQ/MYYid3KggUAfnf+2t+0z8f/wBob9pGw/4J5/sQ+JrjwV4m0+GDUf2nvjhDa3U5+Evha+tkubfTNGnsbqzkPiLU7fUtCaA/b7d4rO/mfyJURt4B6T8Nf+CD3/BOnw7ZW2p/Ef4KaL8dvidM8l54o+LHxXtdO8V+NPFWr3IjN7fanq15pyXMyzTK0yxyySsjyyEyNvNAHC/F3/ghJ+yJptvN4/8A2OfDcP7Iv7R3hzzNW8C/FP4VxW2hGDVlypsfENppdrYXWq6TqFtLc2U9oupWf+vSZnKxGOUA7/8A4Jqfto/Fr4i+KPif+x3+1vBY6f8AtZ/s8XFmuu6jZ74tL+JfgrUG1CPw5440e3mkknL6la6PPf6igub77MbyASXLlgXAPl3/AIOXPE954M/Ym+Hniuwsf7V1Dw78dPh5qmm6bnH27UrXxb4Yn021LbG4mvUgjPyNw3XpuAOm/Zo/4JV+Af2vfCGk/tPf8FKPDWnftB/F34lWWn+JvB/hTxbHDqnhX4PeBtUtxqGgeEfD+manFqP2W8s7C4s7XVbm3lt0u7nT45PssGERADS/ap/4Iifsr+F/A+t/HT9jD4eaN+zV+1H8I9N1Hxz8N/H3w5trTw895e6DZSX994b1kada2tzeaX4j0y1u9DuIhexII9RlZ/NTdE4B8H/sgfEr44/8F49Wbwn8e4tY8B/sp/s7yWfw++N3gPTjcCx+P3xY8PRHQ/E8GraidljP4fj1vR/E8U2m3VhqDSedblrgGI7wD9b9Z/4IU/8ABKy98Jp4b/4Y8+FVvaQRyRWtzZ+GdFh1K0lmjEK3sN0NNO26tyqzwzCPKTIr4GM0AfM3/BPjX/G37FH7bnxU/wCCZHj34j6948+Gcng9fi9+zDqXiiS5m1LSfCFzc6JHqfga3vrmZlks/D+o6/JpunW9rFCiWOmxKsQCAqAfqD+3p+2B4Z/Ym/Zu8Y/G/XrF9c1mwS30TwH4SglRNQ8XeO9ckGn+GPD1hG0cryzahrE9laOkcE7qLhMRsWXcAflJ8Bf+CWPiP9uCx0f9oz/gq5rk3xs1zxlbjxP4F/Z3vmf/AIVn8J9J1XM+i2Umh6o+r21/rseizJ9suYE00pPeXUZgXDigD6/1v/ghX/wSs1vSLbSLr9jr4SxW1jAYLOW18M6Jb3NsrIqb4Z/7Nfy5ECKUcJwVBHSgD8qP22tB+Ov/AASO+E3xI0b/AITnxN8Z/wDgn58VvBXirwPaeH9eF5qfif8AZ11jUNHvf7GMOqSTzWtz4TvNRFnpsEMGl6TFavqyIZ5WhxKAfqt/wQuhEH/BL39lko8UiDwHoxRosGN1bRtK2srAsCCBnPfOeM5UA8J/4KAftc/tAfFj9pfwx/wTf/YY19PDXxT8SaR/wknx1+NVu011D8F/BDy30RtwthPaTW3iPUG0+K2tll1C0dE1i0l8lwVSUA734V/8EE/+Ce3huytNf+Lvwh0f9oj4vXu3UPG/xb+LFrYeJvE/i/xHdfv9V1K6ur/T3ukgur97m4gtpbqd7eOURGeUgvQBnfFT/ghV+x5aQN42/ZJ8NRfsg/H/AEueTUfCXxZ+EMNv4bvbTUlRfLs9bi0qCzu9U0W62G1u7BdQsxLbT3MZlAlJoA/Gz4gftJ/Gnxp/wUZ/4Jvfs4ftW6bDpn7RXwH+Okmj3mtRb4bb4o+D5LfWP7E+IOnW9wHnQ6rpUWl6jqkZub8295q3lSXTMMuAf0//APBQD4367+zn+x58f/jD4ajlfxF4R+Gnii60N4QztZ6q+g6gunXwCfMDZXfk3JYFGHlZDrgsoB/n/wD/AARf/wCCYvhL/gsF8ZPjT8Yf2mviPrb2mka42u+JdF0vUPK8X+J9U8RvDq7XL6nI1w1vplot89qkcljcAYt08wYG0A/W/wDb/wD+DVbwHZeCbfxp+wHreuWvxA0jULE/8IL481xL2y1KPz4zLfaZq0dnp0Wk3VgqtKm60ujKxiAKbMsAf0U/8E1/gP8AtQfs6fsSaF8Kv2sPiPZ/Er4jeHfDerRWupWzTXM2k6S9rqEtro11qU1/ef2k+nW8sFnFPGLZUggSEQLtU0AfxF/8Ez7C2m/4OJfFbywQu0Xxb+McyExgssq6/ZFXGcjcjDII788cNQB/pb0Afy//APB1vZ2tz/wTv0t5oI5Jovit4VEEsih/JLQavlkGMhsqpBB4xyOhoA9N/wCDXgBf+CXfgFFAVU17U1AHA+++SB2yecc/rhQD+L7/AIKj+KfH/gj/AILB/Gjxb8JxdwfEvSviNYzeDLnTEle/t9fS5vlsZrIQFZhdq2fKMbbzlto6hgD9zv2H/wDg2C+JvjbVvhn+0/8AtVfHI6J4/vPF3hj4qX/gzTdLurrWZLrTtV07xBBbaxrx1X5rmaS0CTCTT1ZY2AKkHLAHpn/BdH/ghr4LHh79pL/golb/ABe1SLXfC/gDQLyx8ETabPPbvJ4W0zTNBhSS+F9HHi4hjMhP2X5SQDnFAH41/wDBF7/gi/8ADn/gqP4B+KPizxZ8UtX8DXfgXWJNItNO02zlnRhKkKi5nZLmESKTMf3exeEIDfN8oB/QZ8VdM8Lf8G1X/BOP4geEvhz49l+JPxX+MXi02/w9u9StHsUs9Zns9VdLtrKW7uJpYLOzhuUzHLD/AKQI3DrgBgD8Y/8Agm1/wRR+Of8AwV3u9R/bU/a0+K+vab4E8b69PcDWLpby48aeO5Y5FuLuTTr+e7Emn6RsuofsNyYdQhczzFGxGysAft38Zv8Ag0//AGH/ABD8OrrSvhF4v+JXgT4gQWLDTvEes68mvaVdagq5Q6ppcGn6VLcRTNmNs3ybAwdiwTDAH4afsUftEftT/wDBCT/goLY/se/HDXZ9R+BviPxToml+IdKT7RF4cl0LxFq0lhZ+PPDcM1w9raMlsLi9uFUXBkC2w+0rt3UAf2df8FaNTtNf/wCCX/7TWt6bMs+n6z8KNO1OxmTBWW0vtY0O8hYFSQcxSgHGcnPK0Af5uH/BNT9mb9qv9vK/1L9in4Da/N4b+Gvji60jxZ8Vr2aK6bwzYW+hC5t7W61yGKe3t7qWK31vUGisJ5oZLtPPCSptc0Af3K/8E3/+CL2h/wDBIDRvjv8AH2z+Ld/8UfGNx8HteU2FvpU+kaZZTeH9NudfjVLaS/1Iz7ruz8oMsihUYH5toFAH8nX7E/7NWof8Fu/+CmPxRm/aK+Jt7p+nrrPi7xJrMBu3PiS98N6L4ght9N8LeGpZZ2kslstPvEsROsN1HbeREv2dgwFAH7//ALXf/BqT+zVrHwq1S/8A2SPEfizwv8UtJ065udN0zxhrCa1oXiO4tYDKtjNHBZ6QLO5vWVoFvpGnEbyRv5D+XhgD9Sf+CGH7HX7Y37FX7M118Lv2rvH2m+JT/a6XngTwra3M2q3HgfR3e+kuNJbWXu5I57aWW4hlt7eKxtRbQolufNMQdgD9yhyAfUf59P5flQAtAH//1P7+KAPJPj1/yRf4of8AYla9/wCkMtAH56/8EP8A/lGX+zJ/2Jlp/wCktrQB+sLvtBxy+MhfWgD8Yf8AgoL+178XtQ+LHgX9gv8AY/uLG1/aO+LVtNe+LPHksf2yy+Cfw/URfavFl/H5dzCNSltG1OXSreax1BJbnS5FktmX5GAPOdE/4I5fsefDn4feJviB+1v428b/ABx8YWOkz638QPit8Q/H3jHRre2uGZXurzTtM8L6zoekWEERkW2s0s9JgkZPLDRmd3FAHwD/AME8f2j9M+Ff/BQDRfgZ+x34t+L3xo/Yp+Kum649wPGej+IptE+FniXT7iyisrjw34n8T6bZ6tf6FfnU7mTN5qmqZg063aELvfeAf1s2k7Sq5kwCGKjGeMeufXrkHGMfRQC4CD0/w/z1/wA5oAZIMoQfQ/jweKAP5wND+GmgeLP+Dknx/wCNdWgjn1LwL+yNpMeiiRA/2a4vtH+FlwbmPPSWMwbUfhlDsBwSGAP6PoohEDg5J6nHX8sf1z6jpQB518VoQfht8QyWPPgzxKAPTGj3gyPT8S31oA/Ib/ggS5tv+CbfgyVUMrReMvGk4QZy7R6Z4dYL153EAcuPwoA4D/gh/oNn4u1b9uP9ojXme8+JHxN/aZ8X6Jrt9dNvubXSPBt9eeE9H02PkiOFdN8O6a4QquSituJGaAP37ggEAIDMwJz82ODx6f8A1/wwdwBHNapK29i2QSwA9cED3OPc46ccDaAfgJ+294Z0/wCFv/BWz/gnj8WfBe7SPGHxQt/HHw38dz2zeVH4h8MnUvAVjDFqcKkLcy2dtcXK2kskRaD7RMI3UuTQBm/8HE9ha6p+y/8AAzTL2Nbiyv8A9pz4QwXML52yxP4+8IBkbBBwRkH7vsDQB+9ngrS7PSPCvh7TdOiFvZWGi6ZZ2kCZ2w21taRxQRKSxO2ONQozuOByTyVAM74kp/xb/wAeHcc/8IX4pH56HffTpn1574oA/Ez/AIN6/Dmj6d+xn8Rr6ysoba71n9rH9p6fU54k2yXktp8cvH8Fu8zYJZoonZF6YBPDZoA/eWaAGIJuIG5eQOSM5Izk9en3fz/iAP5tP2uAbL/g4F/YHNu7RNe/BDxjbXTRnY08A8T2OIpiu3zYwUQ7HGMovIx8wB7b/wAFYNNsvE/7U3/BL7wFrdq1/wCGtQ+PXiXWtQsbws+lX13oK+A9Q0pbq23iK5ltLpDLbefG4SV9w25bcAfuvb2NvEixRKscMKxxRRRrsjijiQRxxoqj5FRFCKF2gKMDgYoAvum9duSMnnHp3H4igD4Y/wCCkXgnw540/Ye/aY0LxPpmn6vpcnwo8UXX2fVLdLi3jubGza9tZ1UqzJJDcW8MkbodwdF4GKAPmX/gg6vl/wDBLD9kyLIYJ8OfD67gchsaHpI3DIBII5ydp/2R1YA+d/8Agjb4d0zWP2jf+CnvxH1WEah41vP2p5fDkniC8H2jUE0K1+HXw5vINKgnl3tBZR3LvMIISieY7sVLOzKAf0EpGIwQDnPtj+X+H55oAint1mxuJGDnj6Y9f/if60Afy/f8FXPhH4Zt/wDgsb/wSN+LlubeLxTdeM9b8M33yiO4vbFzB5TuUH70W0enwxK0u7YG2p8pIoA/o1+Lnwt8MfGj4XeOfhX4xtluvDXj3wlrvhXVo2QOUtNd0m60uW4iBJHn28d280JIGJFXrg0AfwBfGf8A4I1/8FTv+CX3xh8QfGX9gvxJr3j7wQNUvr6wbwJPYJqsOiSXJurTS/EWk+JY9M029WyhWC03WlrdykQJtlb5nYA9X+Cn/Bzp+2/+zh4ms/BH7cX7NV5r1lDdQ2es6mNJ1DQvF1onmeVNLaPcXujeFro43FvLmm+ZRjIZaAP7If2VP2xfg1+3F+zmnxt+COuLqvh7W9B1WG902Q41Pw7qi2l7Fc6Tqsf3UuYpoZcGNponQBkdlZQwB/Bn/wAE0opLf/g4o8YwSKVkT4sfGUOD2I16z6cHIyO5B+tAH+lLQB/L1/wdb6ha23/BPjQ7KaTbcXvxW8L/AGZOMuY4NUBA/GRe350Aep/8GvAI/wCCXngLJyTr+qE+37yQc8D09/f1oA/ly+LGj6f4j/4OSvD2g6tbx3el3/7SXhNbu1ljWSKdBqV6NrxvlWGCRg8HPKtigD/Ss0+zihs7JYwEWO0to0UAYVI4Y1VQMAABVA4b8udoB+PX/Bf66ntP+CUH7W8tvI0Un/CvZ13LjODe2eRyCOfpz7YoA/Ez/gz8+b4WftMDoP8AhKLR/Xnfa8YwOvc/+OjqoB87/wDB4NrXiBvin+yh4f3zv4aPhPXdS+yZK2j6vFrs0MMjfdUzGCWaMHO4IxHOaAPlL9l3/gsl/wAFa/gV+z/8LPhP8Gv2RodX+Gfgvwnpui+D9YsvDuryJqulWce2HUjNayiCaS5jwZZhuLhVIc9aAPf1/wCC9P8AwW5PDfscSt7f8Izr3/yR7+n5UAfkf/wUG+LX/BR3/gov488H/Ef4ufsk+LPDvinwTYy2WnXfhXwxqEU95Gwtxbrd3E8plc2xtv8ARxvKxGSQ8bjQB/c5+1vL4nb/AIIXeM/+EysrvTvFEX7OHhyDWbG/ULeWt1b61pduIblBkLKkMUWRknkZ2kk0Afi9/wAGdvg+xl8AftNeM57O3/tAaz4H0q1vxGpuEt2tPFS3MIkPzhXaGLeucHAznFAH9qXiPQ7LW9E1bQdQiFzpuvabf6PqNswysthqNpLZ3kRywzvt55F6oRnO7jNAH8Ff7bf/AAQQ/bl/ZI/aE8S/tT/8E6PEmpeJdPuPEer+MdL0Hwzd2+n+M/Dc2sag+s32lLFqaWGg3+lxXLCKGC6n1Bmit4BIC2/aAcj8M/8Ag4s/4Kefsc6tbeD/ANsv9ne68Y2emywWurXPifRL3SNfjtkkEUr2eoaHPpXhea7wGaPzL14zJt3bkI3AH9gf/BOb/gpN8Dv+CkvwkT4n/CKa40zU9Ing03xn4J1doRrvhbVXWb9xdC3lnt5YZ/s8s1vLb3V0nkNH5kiuxRQD9Ih0H0FAC0Af/9X+/igDyj46wTXPwb+JkFvG808vgzXUjijUu7sbGXhVGSTjPGPzoA/Nb/ghprmn3/8AwTU/Z0tLS6guLrRNCfQtYgikEkmn6vp9np7XdjdKCDDcwCePzYX2su9cg5FAH35+0v8AHvwX+zb8GPH/AMaPHerWmkaD4I8P3V+0l1NHELvUJClrpdhEJJArz3eo3VrCiDLHfwrZAoA/MX/gkP8AA/xTreg/ET9vv44aNc2fx2/bF1YeM1sNWhdtQ8D/AAxuHn1Xwd4NgFyEns4dLOuazBLaRpbxqr7fKXmgD1r/AILN+HfFHiT/AIJ6fHeDwrDdXK6faeENV8RWNn5hnv8Aw1p3xA8I3euWsaRAl0GmW91LOuGXyElLDYGoA+sv2MvEfwh8bfsyfBHxT8E7TSofhlq/w58M3Hhi20ZLYW1lpsmj2TwWLC1CxRz28LwxTRhchhggYzQB4h/wUr/bT8T/ALE37MfjX46fDH4Yah8bPEvhG5sLKXwXok8yzQSXl7p9sZNQktbDU3to4oL8TZa0bdhR8u7KgHr/AOwn+0X4k/at/Zi+Ffx28W+AtR+Gev8Ajvw/aapqPg/U2lefTZ57O1nco81rZSSW8rTsbeQ2sQdBuAAO1QD6/k+7+P8ASgD8Bfh7/wArC/x09P8Ahk7w3/6j3w0oA/f2gDzr4qkD4a/EIf8AUmeJj/5SLz8/pQB+RX/BAP5f+CcvgRm+6vjbxgTx1H9n+G/z6f070AfNv7NPjjSP+CbP/BSn9oT9nT4uaxP4S+DX7ZPjK1+J37N2uayPI8OXvjG50nTI/EnhdNUupYra31O61SLxXqUNhB5ss0MJm2ffKgH9H0cjIz785wCBjjHOSPmHXj6fjhQDK1jWbLSLK+1bUbuKw0rTLO4v9Sv7iRYraztbaN57ie4kZgkMUMSNI8jttVV5JBNAH86PwU1u+/4KTf8ABV+8/aN0O4u9T/ZP/Yu0XVfBHwu8T2iu/h3x38VtVuII/FOoaXqUZNlqEHh3U/CenlXiadgmpIT5W/a4B6F/wcLHP7N3wBxj/k6P4P8ATp/yP/g/OPw/w7UAfvr4fZf7H0sDH/IOsiAPTyF/L/PpQBz/AMSiB8P/AB4PXwZ4ox+GhX1AH4zf8G/g2/sT+MOxP7Vv7U2e3X47+Pf84/xoA/c+X7o/3l/nQB/Nf+2GQf8Ag4E/4J+kdD8F/GX6+J7M/wAv8aAPqD/gs58O/iJbfD74F/tY/C/S7jxF4g/Y6+KNr8UNd8MW9u9zPrPgOa/8OS+NxawRrKz3dr4f0C6kgxC+JWH3SC1AH6Ufs1/tEfDf9p34QeEfjB8MPFGl+JvDnifSLG6uJdJu4bxdJ1Z4VXVtGvjBI4hv9L1FLuwuoG2vHNbSIyIylaAPfZpk2ZjLZyDkdCvfGOvXtQB+GH/Bc39syb4H/so/EH4JfC+xHjn9oT4z+DdZ03wv4D09BfarpfheKOSbxF4u1awiSW5i0K30ux1e2W+kgFut+sce8NgUAev/APBCi0lsf+CWv7KNtI6SSWvw80CJ2j+4zR6JpSnaQT8p7cdO5waAPjT4I+MH/wCCaf8AwU4+PPwa+MOqWuhfAn9ujxRB8V/gr481N1ttMtviKdL0vw3f+B9Q1W4ZIP7Sl07wbLe21sJjM/2+2AtxvVmAP6P0nURK+QVIDBhyNrDKkHIyCOQc9+hzmgChqmrWWm6feapqF1DY6dptpcX9/fXEiwW1nZWsLzXN3cys5WOC3giklkkb5URGY4xmgD+Lv9qb9pb/AIbG/wCC2f8AwT0+IXwtvLjVP2evhF8Vdc+F+neKFnaTSfF/jpZprfXptC2eZa3NvoOreHtX02e5gmlPmOhdYmkMdAH9Qv7d37T+tfsdfsyfEn9oHSPh7q/xOuPAulR3UfhjRTOt1M02Ymv7h4LW8aHTdMGL3UJjbukNnDM74VSaAPxO/wCCYH/Bxv8AAP8Aae0XV/D37V/ivwj8BvieniC7/sGHU7+xsPDWo6BPc3EllGNUuf7MjF7aW8lrbTbYHaaWKeVipJRQDhv+DgX9r/8A4Jp/Ef8AYu8ZeHJPHvwr+K3xr1ZIh8M28FahoWv+KNK1dR+5vmvbWWS+s9NW4e0ku9oVJIonDcL8wB5Z/wAGjvg3xva/stfH/W9Yiurfwb4s8b21v4d8wSC0upl0OKxvrmzZisUix3EN4jmPpKrqxyPmAPwm+PnjPWP+CXn/AAXp8XfFvxZo2pjw3Y/FzUvGlwzwyw/2t8PvG/iSS5vbyxdkIuZI9PsC8ca5XMigSDcGUA/u58D/APBW/wD4J9+P/h9pXxBsv2qPhPpNnf6VHqVxpeseLtE07WtPdovMntLvTnvjJFcQMHi2OwLMoJC5G0A/i4/4OKP+Csvw6/bk1/wT8Bf2ddTm8UfCX4Zahdaz4k8axFTp/iLxMXs1sFsGhea3ms7H7NqIW4W5cSpcI+xchWAP6Uv+DX+3eD/gl18Oy5Q+drWqSqVbccedKvz8DDZXpzgY56UAfzCeO1Df8HL/AIWYc+X+0l4TZvXnUrzgfr6464PSgD/Ses+bS1Prbwf+il+n8vyoA/IL/gvTpkGq/wDBKr9re2nmECL8ObqUOx2jcl7ZYBORwckdfrmgD8Nv+DP1sfC79prI2hfFdsv5Pa8Y9AMevr7KAfen/Byf+wD4r/a//ZRsfiV8LtDuPEPxK+A12/iK30qwt3udT1bwyIL7+0bOxihR57iWO7voLryURgEt2k+XYdoB+bn/AAQh/wCC8XwU+GvwP8H/ALHn7X3iT/hX2tfDQJ4d8C+PdcZIdJvdCiEdta6Nrs97Lappt3pfkkySPcXDSfbMGKLywXAP6bfGX/BUj9gDwV4Wm8X6x+1Z8G5NJgs3vlTTvGWg319cxBNwS1tlv0aaZjgLGp+ZvlBPVQD8R/gr/wAHKOhftAf8FAPD/wCzP8JvgbrPjv4M+Mde0zwx4X+IWlyyv4ikvbzUhYXHiG58PW2lXBh0CATWt3Nc/wBqvDDDvJYqN1AH7Ff8FeGWT/gmh+1RMyGMy/DeJyjcMjNr2jtsI7MpOCD0b6UAfzxf8Gc19LL8Jv2pNPYjyrXxR4DkRR1VpbXxez57kkqM9PU54WgD+vf9oD4l3nwd+EHj34n2HhbVPGlz4I8M6x4iTwzo0csupaqNMsZrxre2SGKeTJWElisL7UBbBxhgD+Yn/gnv/wAHNfws+OHxR+IXgD9rvS9J/Z7il8RPafDS+vLu3XTrWzt7q8t7vTvFN/c2+krZ6pHI1kkgmWWSJ4bpSpKksAfa/wDwU8/bg/4Jb+NP2R/inY/FT4qfBr4t3Wt+CfEdp4H0TQtY8PeJvEp8VXek3EOgT6VGJZLize31aSxnkuLdXeJEaTnYpoA/Cb/g0O8LeNH+Mf7THi7TYr2D4XppkekuWaWSyk1meawl0mJ24ha9tdOtrqB2P7zKy/d3MqgH97g449P8+/8AP86ACgD/1v7+KAK15DFcWtxbzxrNDPE8MsTDcskci7HRgeqspKt7GgD8K/Fn/BO39qT9mv4z+NfjP/wTq+Mvh7wh4V+Iupz654x/Zs+J9ne6l8L21+7llkvtc8NnRoLXxDplzeJLDHNbnxKlkEsLTyrWM+d5oBWs/wDgnp+1p+1l8T/BHxI/4KJ/FzwlrXw7+HOpx694a/Zu+Edvq2leA9W8RWizQWup+MJ9bW/1nU4Iobi5ljs4vEUdt562sjW5MfygH7n6Rp1rp9la2tpa29la28KQ29laRpBa2sSDCQW8MYCRxRjhFQKFHGDnNAEOv+H9J8RaJq2g63YW2qaPrNjcafqenXcSy217ZXUbRTwTRuxDLIjEdip5X5grIAfhHqv/AASD+OXw51/xdp37HX7dnxV/Zy+CvjbU73Vrv4U2Nl4R1rTfCNzqU00t6ngm41zwL4gvbK2bzykNvf6heQwpBbpFGgDigD9Dv2Pv2H/AP7KHw51rwNb+IfFfxa1bxfqp8RePvGXxRu7bxFqfizxBLBa20l7dWLW0GiWkKw2VrFDa6bpVjAqwI5gEzO9AH2vpml2emW0NpY2drYWtuqxwWllbw2ltBGgCpHDbW6RQxIqgKqxqFAAAHGKANCb/AFbcZ4PH4H3X+f5fxAH50+E/2KdY0X/go54+/bkl8W2UmleMvhFpfwzh8FLBML6ym0/TvDFi+pSXJh8ho5W8PtIEW5dgJ1BRcEKAfo1QBxXjfRJfEnhjxJ4ehk+zvrei6ppCztkpE2oWU9qsrgZbbGZdxCjJx35oA+Of+Cdf7IetfsQ/sx+H/gJrHie18aXej6/rGsf23ZRSW1vJFqtvpkC2wjnit5AYfsDMxaMgiQAPx8oB3v7WX7GfwT/bF+HF78PvjD4Y+3gqJPDnirTpXsvFfgrVI38231nw1qcciNaX0DeYi+Ys9u8c0olgYPigD8v/AAp+yX/wVo/ZHjk8K/s7ftR+AP2jfhfFNOPD+l/tO6Rqlz430S0fbHDbz6r8PbXwRp92kMSRbWfz5C6yFmYMooAzvE//AAT0/wCCh37ZmpaYf22P2u7H4bfCi3uop9X+Cf7MdhLoll4tt4TltM8Saz400rxHq4065cA3C6Rr1hOyKiCUAuGAP2b+CPwH+Gf7O/w60H4X/CTwjp3g/wAGeHbeOGx0nTVc+awjjjnu7y4uJZrm6vbnykee4nuJZHcFix3HcAfKn/BRn9iPVf24vhf8PPAOleL7PwXP4K+Kvg34iyX19BNcxXkPhfxDo+tyaeqQ21wwkuk0owI21VDSAlgMugB+hGi27WtjbWjjL2lvb25cfdcxx7Mr3wcZGcEA47EsAZPjDSX13w94g0SNjC+s6HrGkLN1WN9U064sVlcddsRnEh28nHGMZYA+Iv8AgnN+xzrX7EHwL1n4Q6z4qtPGlxqvxb+K/wASo9WsoZbeGC2+JHj3xB4zh00xz29vIZNPj1lLSR9hVnhLCRwQzgH6BTK7RDYMt8vGccd/xx68fnQB+Xnxm/YG1j4of8FE/wBnf9t1PG9npml/BDwPrPhGfwZJbTSXutSarqcOoi6juFtZII0hERjw1xCfRW5ZgD9Lr/SrbWLG607U7K2vtOvoJLa7srqFLm2ubeVSkkMsMu5JIpEZldJAwZWIPTDAH4c/En/gkx8UPhb8Utf+Nv8AwTz/AGmPE/7Oes69q7a3rnwT1mHTdZ+Bet3bw4u2OmNoOp+KdPa/u1+1zrpuv2MQnllMYRCFUAdqPgD/AILkfEmwPgfWfin+yz8INLvIzZ6h8RvA3h7x7deK7eHHlyXmlQa7rHiLSVvHVmlh+16PNCsqrviC5RgD074Rf8Eo/Dvw2+Fnxms/GHxU8XfHb9oT4zeEdZ8L6t8dvit/Zd5q+hwazYtbSad4dstD0XRNN03RYp2+0iOHRxfMd4kuGV3RgD7D/YE/Zivf2Nf2VfhR+zpqniS38XXnw30Cz0SbxBaQyw2+otaWVpaedHFNBbyKHNuX+aJPvcoOigHT/tTfsk/Bz9r/AOHN78N/i/4aTVLBybjQ/EFoxtfEfhLVl2va634e1GNo5bXULWVEmh3+bbMybJreSNnRgD8oPCv7Gv8AwVe/Y7hfw1+zN+1d4N/aL+FiO8OgeHf2otKvLzxZ4cs93+jW6av8PbHwRbXdvZw7YEa6lu7l0SMvJI/mOwBpXv7CX/BRT9rnUrQftr/tV6d8N/hfa3Gb74P/ALMtg2j2niyzmAW90vxJq3jfTPE+qf2ZfQg2lwuka5p92LZ5vInSZklQA9w+KH/BLLwbqnxU/Yh8WfBq40L4WeBP2QPFMviI+ELW0uZZ/FZljmWaaS7aK7kl1C8kme6u7m7u0eS4klfI3tuAP1g1jw9pHijRdQ0DxBpVpq+iazYXOm6ppmowpc2d9Y3du9tdWtxE+7ck0EkkRxtcbjhw216AP5Yf2w/+DVL9mv45/EDV/iP8Bfih4s/Z/wBS1++utTv/AAtpv9nXXhOK+u5HluJbFbnQtX1i3SWZnmMS6gI0Z2WOONAiKAeF/BT/AINDPhf4d8X6b4g+PP7Tfj34maFp9zb3EvhfSDptvZ6kkMvmvZXc2peFo7yO1uQohnNpeQTCNmMcqOFZQD+tH4DfAb4Xfs4/DPw58JvhD4Q07wX4K8L2aWmm6Tp8bAcF3kubiWSSWW4uZ5pJZ5ZJJSQ8rKuxdq0AfCX/AAUd/wCCTn7N3/BSbwnDpfxU0u68M+ONHSRfDHxQ8KJZ2/inSzIqYt5pL21vrC8s90MJaO6064dFVhCU3uaAP5z4f+DPC3h1o+Z+2X4sj8LfbN32e2tbJNWayMm4wv8A8UiNN80plcrBsJ5OcYoA/T3Xf+Dan9jmf9k2y/Zk8L+IvGXhfVX8Tab4q8Q/GC1Ggz+O/EWo6fHdqLa7mvdBvNJg09nvbh/JsdIs2ff+8ZtiFQD9Vf8Agnj+wx4O/wCCeX7OPhz9nTwF4o8Q+MtE8P3FzcjXPEp086nczXMjyMZP7NsdLtQo34AW1Xpz2oA/OjxB/wAEB/gxr3/BQbT/APgoBN8XviTb+L9P8c6Z48TwTEfDQ8PS6lpk8lxHbuzeGn1P7K7SOCE1RZduP3oIoA/oEgjEUMMQJIjijjBPUhECgn3wKAPk39uL9lLw5+2z+zP8Tv2afFviDWfC3h/4naFJoeoa94fa0XVdOjeWKXzrU31pfWu8GIDEtpMDnGw5yoB8ff8ABLH/AIJNfDr/AIJc+EfH/hnwF8Q/GPxCPxA1OPUtRvvFf9jLJbuhiKpbrpGi6QgQmIZ8xJGwxwR/CAfrRPYW93azWt1bxTwTwvDPBNGJIpo3XDxyRvuV43BIKMMEccZwoB/Ob+3z/wAG2X7IH7ZPirUviZ4I1DXf2e/iXrUr3Wr3vgUaaNB1W8di8l1eadqmla39mlkZmONNW0jycbBtQqAfl9o//Bnh/wATaFPEn7aHi+78NLMvnWenw2rag9sGyyL/AGh4RmsVdhkcQhM84NAH9Fn/AAT9/wCCQX7JX/BOvQVj+EvhSTxJ4/kh2aj8UPGC2194rvSUwyQm2gstJsVDF3RrHTLRgz8sQqBQD7E/ag/Z80n9qD4AfEr4A+JtT1PQ9F+Jegf2FqOr6QbT+0bCH7ba3ouLX7ZBdW3mB7VFIltpVKk/ITh6APhP/glN/wAEkfht/wAEq/DnxF8NfDz4ieMviJF8SdT0XUNQvPF/9iCawOiJqkdtFajR9F0dSsi6rL5nmxynMabWXksAfrdqdol9bSWstvHdQTxywz28yq8M8MqeXJFKj/K8ciMyurZDLkEHIFAH82v7fH/BtF+yj+2B411j4r/DrxJ4j/Z4+Imuzte6wvg7+zW8Marfy7pLnULmy1LSNduYLme4LTSLp8tnDumlCoqhAgB+e3gX/gz68LweIdPuvif+15468ReFra6imudH0OPTkubuGOQO9uZNW8JyxRLKq7GMTwsAx2sOKAP6q/2Qv2Mvgb+xD8J9G+DnwD8GweGPDGnFZb66JE2ra7qLGWS51XV7t2Z57u7uLie4kWPZbxvM0cEMUKoigH1wOg+goAWgD//X/v4oAQgMCD0P+fb/AD69KAI/JT3oAPJT3oAkVQowOlAAQGBB6H/Pt/n16UAR+SnvQA5UVc4/z/n/AD1oAfQAhAIwaAGrGqnIzn/P+e/65UAfQBG6Ag8c/wCefbH059sGgB6jCge3+fX/AD6dKAEZQ3XP+fxHp6H8KAGeSnvQAeSnvQBIFGNvb8v8f8+nSgBpjUjGP6/5/X+qgCqgTOO9ADXTd2yc/wBOv4fTn2xQA8DAA9ABQAtADGjVyC3OOn+f/rfnmgBwGBj/AD/X+f50AR+Unv8A5/P+f50ABiA5XOff/P8An86AE8vP3hx9c/5/z6UAL5Ke9AEgUAbe3T/PX/Pp0oATYvp+tACGNT1H9f8AP+fSgBvkp70ASABQAOg/z7/59OlAC0AFABQAUAIQD1/w/wA9f85oATYvp+tACgAdP55/z/n0oAWgAoAQgHr/ADx/n/PrQAAAdP55/wA/59KAFoAKACgAoAKAG7B6frQA6gAoAKACgAoAKAP/0P7+KACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoA//R/v4oAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgAoAKACgD/9k=" />
                        </th>
                        <th class="tg-gt19 tg-head-border" colspan="3"><span style="font-weight:bold">BOOKING CODE : #' . $data[ 'lbooking_code' ] . '</span><br>' . date( 'D, jS M Y H:i:a' ) . '<br></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="tg-no-border" colspan="6"></td>
                    </tr>
                    <tr>
                        <td class="tg-b4sw tg-no-border" colspan="3">CUSTOMER DETAILS</td>
                        <td class="tg-b4sw tg-no-border" colspan="3">BOOKING DETAILS</td>
                    </tr>
                    <tr>
                        <td class="tg-0lax tg-no-border" width="50">Name</td>
                        <td class="tg-0lax tg-no-border" colspan="2">: ' . sprintf( '%s %s', $data[ 'ltitle' ], $data[ 'lfullname' ] ) . '</td>
                        <td class="tg-0lax tg-no-border" width="90">Booking date</td>
                        <td class="tg-0lax tg-no-border" colspan="2">: ' . date( 'd F Y', $data[ 'lcreated_date' ] ) . '</td>
                    </tr>
                    <tr>
                        <td class="tg-0lax tg-no-border">Email</td>
                        <td class="tg-0lax tg-no-border" colspan="2">: ' . $data[ 'lemail' ] . '</td>
                        <td class="tg-0lax tg-no-border">Check-in</td>
                        <td class="tg-0lax tg-no-border" colspan="2">: ' . date( 'd F Y', $data[ 'lcheck_in' ] ) . '</td>
                    </tr>
                    <tr>
                        <td class="tg-0lax tg-no-border">Phone</td>
                        <td class="tg-0lax tg-no-border" colspan="2">: ' . $data[ 'lphone' ] . '</td>
                        <td class="tg-0lax tg-no-border">Check-Out</td>
                        <td class="tg-0lax tg-no-border" colspan="2">: ' . date( 'd F Y', $data[ 'lcheck_out' ] ) . '</td>
                    </tr>';                    

                    if( empty( $data[ 'lguests' ] ) === false )
                    {
                        $html .= '
                        <tr>
                            <td class="tg-no-border" colspan="6"></td>
                        </tr>
                        <tr>
                            <td class="tg-nwi2">No</td>
                            <td class="tg-nwi2" colspan="3">Guest Name</td>
                            <td class="tg-nwi2" colspan="2">Type</td>
                        </tr>';

                        $no = 1;

                        foreach( $data[ 'lguests' ] as $type => $guest )
                        {
                            foreach( $guest as $idx => $gs )
                            {
                                if( $type == 'adult' )
                                {
                                    $name = sprintf( '%s %s %s', $gs[ 'title' ], $gs[ 'firstname' ], $gs[ 'lastname' ] );
                                }
                                else
                                {
                                    $name = sprintf( '%s %s', $gs[ 'firstname' ], $gs[ 'lastname' ] );
                                }

                                $html .= '
                                <tr>
                                    <td class="tg-0lax">' . $no . '</td>
                                    <td class="tg-0lax" colspan="3">' . $name . '</td>
                                    <td class="tg-0lax" colspan="2">' . ucfirst( $type ) . '</td>
                                </tr>';

                                $no++;
                            }
                        }
                    }

                    $html .= '
                    <tr>
                        <td class="tg-no-border" colspan="6"></td>
                    </tr>
                    <tr>
                        <td class="tg-1wig tg-no-border" colspan="6">ACCOMMODATION DETAILS</td>
                    </tr>
                    <tr>
                        <td class="tg-nwi2" colspan="6">' . $data[ 'lposts_data' ][ 'ltitle' ] . '</td>
                    </tr>';

                    if( empty( $data[ 'ltrips' ] ) === false )
                    {
                        $html .= '
                        <tr>
                            <td class="tg-b3sw">No</td>
                            <td class="tg-b3sw">Detail Trip</td>
                            <td class="tg-c7at">Guest</td>
                            <td class="tg-c7at">Night</td>
                            <td class="tg-c7at">Price</td>
                            <td class="tg-c7at">Total</td>
                        </tr>';

                        $no = 1;

                        foreach( $data[ 'ltrips' ] as $rm )
                        {
                            $guest = $data[ 'ladult' ] + $data[ 'lchild' ] + $data[ 'linfant' ];
                            $total = $rm[ 'price' ] * $guest * $data[ 'lnights' ];

                            $detail = array();

                            if( isset( $rm[ 'duration' ] ) && !empty( $rm[ 'duration' ] ) )
                            {
                                $detail[] = sprintf( 'Duration : %s', $rm[ 'duration' ] );
                            }

                            if( isset( $rm[ 'departure' ] ) && !empty( $rm[ 'departure' ] ) )
                            {
                                $detail[] = sprintf( 'Departure : %s', $rm[ 'departure' ] );
                            }

                            if( isset( $rm[ 'itinerary' ] ) && is_array( $rm[ 'itinerary' ] ) && !empty( $rm[ 'itinerary' ] ) )
                            {
                                $detail[] = sprintf( 'Itinerary : %s', implode( ' - ', $rm[ 'itinerary' ] ) );
                            }

                            if( empty( $detail ) )
                            {
                                $detail = '-';
                            }
                            else
                            {
                                $detail = implode( '<br />', $detail );
                            }

                            $html .= '
                            <tr>
                                <td class="tg-0lax">' . $no . '</td>
                                <td class="tg-0lax">' . $detail . '</td>
                                <td class="tg-lqy6">' . $guest . '</td>
                                <td class="tg-lqy6">' . $data[ 'lnights' ] . '</td>
                                <td class="tg-lqy6">' . $this->get_format_price( $rm[ 'price' ], $data[ 'lpost_id' ] ) . '</td>
                                <td class="tg-lqy6">' . $this->get_format_price( $total, $data[ 'lpost_id' ] ) . '</td>
                            </tr>';

                            $no++;
                        }
                    }

                    if( empty( $data[ 'laddons' ] ) === false )
                    {
                        $html .= '
                        <tr>
                            <td class="tg-nwi2" colspan="6">Add-ons</td>
                        </tr>
                        <tr>
                            <td class="tg-b3sw">No</td>
                            <td class="tg-b3sw" colspan="2">Add-ons Name</td>
                            <td class="tg-c7at">Qty.</td>
                            <td class="tg-c7at">Price</td>
                            <td class="tg-c7at">Subtotal</td>
                        </tr>';

                        $no = 1;

                        foreach( $data[ 'laddons' ] as $rm )
                        {
                            $html .= '
                            <tr>
                                <td class="tg-0lax">' . $no . '</td>
                                <td class="tg-0lax" colspan="2">' . $rm[ 'name' ] . '</td>
                                <td class="tg-lqy6">' . $rm[ 'num' ] . '</td>
                                <td class="tg-lqy6">' . $this->get_format_price( $rm[ 'price' ], $data[ 'lpost_id' ] ) . '</td>
                                <td class="tg-lqy6">' . $this->get_format_price( $rm[ 'total' ], $data[ 'lpost_id' ] ) . '</td>
                            </tr>';

                            $no++;
                        }

                        $html .= '
                        <tr>
                            <td class="tg-c7at" colspan="5">Total</td>
                            <td class="tg-c7at">' . $this->get_format_price( $data[ 'laddons_total' ], $data[ 'lpost_id' ] ) . '</td>
                        </tr>';
                    }

                    $html .= '
                    <tr>
                        <td class="tg-j3sv" colspan="5">Grandtotal</td>
                        <td class="tg-j3sv">' . $this->get_format_price( $data[ 'ltotal' ], $data[ 'lpost_id' ] ) . '</td>
                    </tr>
                    <tr>
                        <td class="tg-j3sv" colspan="5">Deposit</td>
                        <td class="tg-j3sv">' . $this->get_format_price( $data[ 'ldeposit' ], $data[ 'lpost_id' ] ) . '</td>
                    </tr>
                    <tr>
                        <td class="tg-j3sv" colspan="5">Balance</td>
                        <td class="tg-ovpe">' . $this->get_format_price( $data[ 'lbalance' ], $data[ 'lpost_id' ] ) . '</td>
                    </tr>
                </tbody>
            </table>';

            $dompdf->loadHtml( $html );

            $dompdf->setPaper( 'A4', 'potrait' );

            $dompdf->render();

            if( $stream === true )
            {
                ob_end_clean();

                $dompdf->stream( 'invoice.pdf' );
            }
            else
            {
                $invoice = ROOT_PATH . '/invoices/'. $data[ 'lbooking_code' ] . '.pdf';

                file_put_contents( $invoice, $dompdf->output() );

                if( file_exists( $invoice ) )
                {
                    return $invoice;
                }
            }
        }
    }

    function sendInvoice( $lbooking_id )
    {
        $data  = $this->getBooking( $lbooking_id );
        $agent = $this->getFields( 'lumonata_user', array( 'lname', 'lemail' ), array( 'lusername' => $data[ 'lposts_data' ][ 'lcreated_by' ] ) );

        if( $data[ 'lemail' ] != '' )
        {
            return $this->sendEmail( array(
                'subject'    => sprintf( '%s INVOICE #%s', $this->getSettingValue( 'name' ), $data[ 'lbooking_code' ] ),
                'message'    => $this->bookingInvoiceMessage( $data, $agent ),
                'to'         => array(
                    'email' => $data[ 'lemail' ],
                    'label' => $data[ 'lfullname' ]
                )
            ));
        }

        return false;
    }

    function sendBookingRequestEmail( $lbooking_id )
    {
        $data  = $this->getBooking( $lbooking_id );
        $agent = $this->getFields( 'lumonata_user', array( 'lname', 'lemail' ), array( 'lusername' => $data[ 'lposts_data' ][ 'lcreated_by' ] ) );

        if( $agent[ 'lemail' ] != '' )
        {
            return $this->sendEmail( array(
                'subject'    => sprintf( '%s - Booking Request #%s', $this->getSettingValue( 'name' ), $data[ 'lbooking_code' ] ),
                'message'    => $this->bookingRequestMessage( $data, $agent ),
                'to'         => array(
                    'email' => $agent[ 'lemail' ],
                    'label' => $agent[ 'lname' ]
                )
            ));
        }

        return false;
    }

    function sendBookingAcceptedEmail( $lbooking_id )
    {
        $data = $this->getBooking( $lbooking_id );

        if( $data[ 'lemail' ] != '' )
        {
            return $this->sendEmail( array(
                'subject'    => sprintf( '%s - Booking Accepted #%s', $this->getSettingValue( 'name' ), $data[ 'lbooking_code' ] ),
                'message'    => $this->bookingAcceptedMessage( $data ),
                'to'         => array(
                    'email' => $data[ 'lemail' ],
                    'label' => $data[ 'lfullname' ]
                )
            ));
        }

        return false;
    }

    function sendBookingRefusedEmail( $lbooking_id )
    {
        $data = $this->getBooking( $lbooking_id );

        if( $data[ 'lemail' ] != '' )
        {
            return $this->sendEmail( array(
                'subject' => sprintf( '%s - Booking Declined #%s', $this->getSettingValue( 'name' ), $data[ 'lbooking_code' ] ),
                'message' => $this->bookingRefuseMessage( $data ),
                'to'      => array(
                    'email' => $data[ 'lemail' ],
                    'label' => $data[ 'lfullname' ]
                )
            ));
        }

        return false;
    }

    function sendBookingChangedDateEmail( $lbooking_id )
    {
        $data = $this->getBooking( $lbooking_id );

        if( $data[ 'lemail' ] != '' )
        {
            $mod = $this->getFields( 'lumonata_module', 'lmodule_id', array( 'lappsagent' => 'reservations' ) );
            $prm = json_decode( $this->getFields( 'lumonata_additional_field', 'ladditional_value', array( 
                'ladditional_key' => 'alternative_rsv_date', 
                'lapp_id'         => $lbooking_id, 
                'lmodule_id'      => $mod
            )), true );

            if( $prm !== null && json_last_error() === JSON_ERROR_NONE )
            {
                if( isset( $prm[ 'lcheck_in' ] ) && isset( $prm[ 'lcheck_out' ] ) && isset( $prm[ 'lmessage' ] ) )
                {
                    return $this->sendEmail( array(
                        'subject' => sprintf( 'NomadSurfers - Alternative Booking Date Notification #%s', $data[ 'lbooking_code' ] ),
                        'message' => $this->bookingChangedDateMessage( $data, $prm ),
                        'to'      => array(
                            'email' => $data[ 'lemail' ],
                            'label' => $data[ 'lfullname' ]
                        )
                    ));
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        return false;
    }

    function sendBookingPaidNotification( $lbooking_id )
    {
        $data = $this->getBooking( $lbooking_id );
        $agent = $this->getFields( 'lumonata_user', array( 'lname', 'lemail' ), array( 'lusername' => $data[ 'lposts_data' ][ 'lcreated_by' ] ) );

        if( $agent[ 'lemail' ] != '' )
        {
            return $this->sendEmail( array(
                'subject'    => sprintf( '%s - Booking Request #%s', $this->getSettingValue( 'name' ), $data[ 'lbooking_code' ] ),
                'message'    => $this->bookingPaidMessage( $data, $agent ),
                'to'         => array(
                    'email' => $agent[ 'lemail' ],
                    'label' => $agent[ 'lname' ]
                )
            ));
        }

        return false;
    }

    function sendUserNewCredential( $luser_id, $password )
    {
        $d = $this->getFields( 'lumonata_user', array( 'lname', 'lemail' ), array( 'luser_id' => $luser_id ) );

        if( $d[ 'lemail' ] != '' )
        {
            return $this->sendEmail( array(
                'subject'    => sprintf( '%s - Reset Password', $this->getSettingValue( 'name' ) ),
                'message'    => $this->newCredentialMessage( $d, $password ),
                'to'         => array(
                    'email' => $d[ 'lemail' ],
                    'label' => $d[ 'lname' ]
                )
            ));
        }

        return false;
    }

    function sendUserActivation( $luser_id )
    {
        $d = $this->getFields( 'lumonata_user', array( 'lname', 'lemail', 'lactivate_code' ), array( 'luser_id' => $luser_id ) );

        if( $d[ 'lemail' ] != '' )
        {
            return $this->sendEmail( array(
                'subject'    => sprintf( '%s - Account Activation', $this->getSettingValue( 'name' ) ),
                'message'    => $this->activationAccountMessage( $d ),
                'to'         => array(
                    'email' => $d[ 'lemail' ],
                    'label' => $d[ 'lname' ]
                )
            ));
        }

        return false;
    }

    function bookingRequestMessage( $data, $agent )
    {
        $content = '
        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
            <tr>
                <td style="padding:25px; background-color:#FFFFFF; text-align:center;">
                    <img style="width:250px;" alt="NomadSurfers" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAYsAAABXCAYAAAD1a2lrAAAuWklEQVR42uzdb2hVdRzH8XP/ue3u3uvdH9e2dP91azDm1BabKQtqOlFRmyVTiqT2INAI90iy7GE9uA/S8EHSIgh6EEVJIBJB9cCCKMyKkFAzliSR2B902z339rnjd+Jw2GX3Thfaeb/gy++c6+7dFPf7nu/vd87vZwEAAAAAAAAAAAAAAAAAAAAAANweAjMBAIBH0IRXYOZ1AIDvhSyXsbGx8lQqVWmJS9gCAPjTzp07Q5Zs3br1rs7OzoM6/HTRokWXEonErzo+E41GjzY3N3eTMADA5xVFOBzui0Qi53WYdUcgEJhpg8HgVF1d3bMkDADwaaKoqalZq+aaSQ6Tam1FxoStmDLHWSWVsX/fCwDwh9WrV0dKS0vPmkpiyrSzha1IK5lk6uvr+y1h0hsA/DOhvcckg2nT5g0lipmvSSaT71FdAICPkoWqhBPuRDBH2Lm2pKTk54aGhhaqCwD4fwsogocPHw5qCOpzkwjSBSSLTC5y8xqaDO+lugAAHyQLE6eLTRaKSQXJAgB8dCfUyWKHoRQTijaGoQDAPxPc+5Qo5roTKuueBNcDe+9TVQCAPwQsGRkZqQiFQhcLuCMq4yQUVSMPkSwAwGfVRTwe36IJaycpTCpsT5KYdhLFsmXLjpAoAMCnCUNrQu0uKyv73VtNuM+XL1/+SjabDbF8OQD4OGG0tbW1qspIaQ7jnNaCmtbwVC5ZXIzFYu8sXbp0E/tcAID/BEySCJrziMKR3LhxY/vQ0FBnX19fpTupkCgAwD+Cs3X6GmIK5Pt65igAwF8ilqFbYB/RkNOH5eXlZzVf8Y3Ov9SQ07FVq1ZtYcMjAPCngNPxd3d3363mA0U2X9TW1p4YHBxs5sE7APCPsGW0trY+qoUAf3E9V5FW2E64b5NVtXFBbYcnYQQUQSa6AeDOXesplK+a0Jap8bJo2avmae1Cnti+YZYBOa0FB73Jwfv5ESa/AeA2SwieyD8prXC2TFU18a2rmrDNcUHLe+gW2hHXz7BE0aFbbKvN3t2zToaTPADgv00OYRN5HThwoFrDS21NTU0bdJp0f4b2zT6Ye2bCW00UkywUxxWO5xW2Ko4zmhA/uXjx4mPaDOkZvTbY399fo9YrTOIAgFvFe1Xu0d7eHlfTpKSwVlf1T+j4ZcW7Wqbjq4qKigl13tcTicQXeniu0pLe3t5m/dnHriex0+Z4PsnidUvMKrXjeXbRm1QFc1nHHyk5HdJ2rQ9oY6Vq73MdTJYDwPyFFBHLZXR0NHd+r4aPntZ6TG+o4z+t89/m6NwPWdLT09MYjUZ/NJ34lGvZDttEUclCn/WaJZq7CCsxfeL63LSpVqad7+GJ7/XzH6murt48Pj5e6q42SBoAUGwlYaxbt65OQznDujI/rjinl/7Ms6dE2hMZdco39LR1v9kq9UnTod9wvS9tXitmOCpt2jFLpEXvv+De28IJTwUz7a5k9HexNVz1Q1VV1XNqW0kaWGgBd3Dr3sKPlw8MDIQV/EIv8O2sqhru0xDSEXXEl2a7unddxWfybUikIaCvdeXvfOZbTmfvaa9omOopdd5OhZKZa2c8fa7d0dGxwZKWlpYHzXts0xayWdKUaZ3hqmuRkshRHXfw4B9wp68X5MECcguzcJ867i5NEr/t3KY6y7MPmUKHirSX9guW7N27t17Jx50MMmbDop+6urpatm3b1qrjQqqKjLkT6oozcd3Q0LB/PhPlJmzPfhl/qNI4unLlyiZLzJ7gXJTg5j22fXvVnh076nS7Xu1MbNpUu2vXrvpUKlVm4VYIWsa+3bsTanrX9PQMD6xf/3g0ElnjrBtEwrgFCVlWrFgxquZvz8ZCdnEdsLnKV2jo6X5LGhsbH1blMNM5u7dL1YqyL1pSWVk5bIai7AK3UP3M+b+hu57edFcq84yMJ2lc1s++35UoIhZwM79ciVjsVCIevxovL5+Ix2KXy6PRiYpk8urmoaFhS5RAWOhs/oJmUjV6T3v7S/q3Pa/TvzQ0kq1ZsiTX4VzXcMR3em2QjW/+Ye86oKQqsjbvdU/35JxjM4GJDDDDMDAwpAGGHAUFRJK6rP6uGFbWrOuaRRBXBGVd18iCi4FVdA2sq4LZVTCsa0DF9KMkCcLM9PxfNbf469zz3vR7PdMDeLrOqfN6pvu9V3Wr6n51YwVYaI6SZ9OvQU/GOAOqzSQ1vAojcgy95k9S4lDBJCUlpV58iTxQCwksmq1ILGDkt9Fzw1Hf5faKjgINRIv/s7a2tozoFHK1DZV27XjfMpp0gwYNmhUCi/aD8YIFC1LBSN5UXSFxfRX1BdRd4iQ17FpHhcCifXQeMGBAgthNt2eHzhk6QP2P4tmwL6VDcviYxq9FPh/j+hZUUAnUjpvlvVae3bVrV9/6gvG8ECopVRJq7aDaoqi19qDO7BIqodJOsHiV7ZjE4SytWCAzQ2DRfv05dqbSKOoFg1g5ZsyYfHyO7uLxhKeHh3twvsEpGzduDBkiAyxSzTJ37twq2Bc6iuF6AeJeGJ59BxNBehitSA1eqYKCrWGZbAd28A9aAAvZtv1IKNhH3IfrNFW91ZGVtwe2jEvXrFnj6hIqoRIgWLzGROBmMXmhUw+BRftp2xN1t6ArGM89RjvikL2i/WBBBujeBBaBVm6A/kzkfSJ7xJXcAA3AaIbKq788rwIBfS9bkGqkh9VWSCXp1IVrVKYejCpdfZEmvXXIkCG1XUIlVDoQLEKSRQe4byLoa4FvsWInmZ+dXST+hwjcsNDhN8ctWHiFmpBiGJYqUd7nEtM9qIDBFnJ/7jJ9+vRklA/oN6YSgpRI0tLS/i6fjfs2qN91ZKX++AAO8SK7YCcZ1iVUQiUEFscfWOCwm6U+/bTH884keJ2FpIjgggVUQfbVOcSouUQAe0Wjsk6iUJ+RYEAM/w8KmBTj3d+wdWSqEtK76FfTrZl43ocdaNzmffKS++/LDQ0N3UJnaIRKCCyO38CwWwVN4Rr5d5FmIhRTETwDN861jgWD3GZHpUOJ+1SQ+Boqpy9hr3gYn+OZ00Es0n3Mw+783+K3yCVVJxuA8R0EtZWVGI4W3aG3pmelTxP3IcVHPS4H1biNDqjNSlbbJjhP3KQEFYZ1RFpePUiTWCNm46todBDeY61/du6ldgarzbJtQaSLeT+CDBYaq7/ISHc7YEGG0b/T/7RgRd13dv+PlznBGPpJCtM8ZMaAuW0BBuw1UBmeIjSHigFYJ+aqMf7xCpj8ewL8Jd8EgESCMW9q22ZBQXxu197hw4f7VJLIXHtqR9krBOip70Z7nkPMSQ13L+ZRorzqjEHwScsP3NCDleFRlkDzsnMma/Ac3ey5/Dub32t222v7/fSOoL+H3tExYME3BFTanxHT0U7mqtuZy4zuVp7jMPmfrG7631Ki6ZPkT++kar5GA6ez08iPvtP6z1VD5r9xyHYGSVIWwXE7mOqnhQOFpmutIk9UYUXhCBaz4TrnnHNi2bMlgNTS/ZepacLJPlVvpk5i/98id/gZaRl3tdNe4eX5omg9T5UND2gd8UlWVVFRGgFrfmZqat3Afv26k6jCftu+lAZzEBE8duTIfvjYV6QABgPqedFFF8XwXZj155sXaWySA16Sn98dH/sX5OXV4TsPD+Ixa7egA87IrUDK4P7wWqgbPXp0T+w2HNbba35YivScEGmOS46IsX3Li4v7Txg1qgzvDefj1Z68OKLNo/BcuPcJj426YQMH9hLBYez5AYMFpzm9MwKiego+JuK7ZFM6UAl43IN/v8b7Tf1KO++88+xEtl9HNH20ne10MmbsqqysTBV0xlxNxZzS2/p98PtvPsbjxo1LFu1EpHPKypUrI/n8CYaEgZ1/Li53ou4zMPYeIgb9dnl5ea68ETaIBtQVUNs8j9iJzdDz/wV/nyEyzcrfwKPtKnFvaWlphQpQeI6LvKaWKgBlJsksl8+DRHJlAGDRwm0sJB39E3aTGXIecb7GS3fUB1DvokM1VqI+iIW/UP4APt3FIMINuqZtjQwP34GX/uR0OH6Ki4nZCT3d1sTE+FvhLlas7A40u0CUiuyHuPF8PHdjYkLCdjDdvSJfidvl+gmRwTuRHfFTREau9+TkzJ0zZ066xfwl8ruzUR+ivt1NUZUrUD004Pmw+v8R9T+usLAfRXZJ8d7oqKjvwtzul9xhYWdjkUUpbdZEpYlbgUjmxS63ewsm2w8O0EXD/SnJyT7a4H83KaDjtMu8YTCrweU2tO3diPDwb8Pd7p8EXfD5p+SEhB2xcXEfx0VFPZgYFTWC2mQPSIlG4jD4nKycxRjn95MTE3eg7eI9PtrDde4Th67fRykMZHnTDljwviFuIAHxA6fh41p4dvwX129FdbqcXyckJbzVNS9vWe/evfv4BUHqM+ZHAi5L8MzV0yZP7i/bYHN3eQYdnD+Ov5O/Dy6PSYKx5GZn3z9l/Pje6nMqKioq01JSrg93uTaDjiIr6KdYkB+h3o/P1QrzTgQoLxHrDrRbQWtwGeq/iaafE/NayepyWrPTeTs5nWfNmhVVXlIyHWvnYYzjFqwvYVDdHhkR8TWiwd/G5zvgaTVGUaM4rYBTPIpoR1ZGxv2gcx+1/2VlZeVpSUnX4Z2bUI/2H3NLxBRIWml8jPpXVdWLMcTaeQv3fS3nRBIC2xKjo9fVVFXNhEE6xi6w2V1zVX2rSjOyMq5z6s6PVAYucjxhrhbTz6Kx1u8yY85Yr9+B3o/17dt3ZhzWKP5+xwzcKUDvEy5hqICAOXWTcn8k6vttqK+8LBtuC2vbdlTBB+tILWZZCzLepMPPii+z0tLOA4Pa7w+5wFT29ejRY54EDKuDA8YfjwFYjN34Twb6NMN35eXkfAPm/eujOw08yw8YPWmEtBjMcniejNVY6mGTdmyFtNBbPpdoc4mVJF7hmGRor8KA/NOlHCdtgTmvwbu9VukSFR7+fHVlZZXF3ZcuP/SsrDzX7XbttrJDwU1iwUeg/ss2WFDfKkpLh8dGRX2kPregq+cHT17OF5jE+9T/5+bk/AXXhDb6pNPuLAeXXd2KilqnTplypkUa8Hmymt77B27Y4+8D4/OI33pyc1snT5gwQzLmpLi469vyUEmIi7tZAYt8gFsTuVya7CjNK+55QG0n3wD0rKgYnpqa8g67bz/mz27+fADHpu4lJf0sGDR1CibLFvdlZ2W1nnzSSaeRNBCJzdEf2mp7YmzsrUyt1KVPSUkS6Pag+ru4mNi9hQUFXyQnJuxgfObfSEMxJEiAoavPhFrJjUsjAP0m0PpdrMf/kbF0qA8rO/bDqGL+N1NW2ia+VpEp9nzZZukKLU7Uk8waO/yTTNJ+eOk53/okHyoIlJtE3/vejSrf22zCwz7BBvw+AN4sdoKeLTo2KoadJiXj4loswCuwo+Q6PC+rLUd9cpE9MTstbb7fBtB3hXl5VfGQGHiGR+Fmhnr0HeKzREp1IHKzstaD2HHsfUZM4BG65yARV3zeVti169nJSUn75OEhJv1rlv3DgO4ZOHCgz8hUWlR0p1zgBBiGbZb3QvpoheFokhET46BXXFg4HrvSHyXTMKK9xugi2kCAsQfXoew9pqqLqIiI20TktUIDM9o3Kbusp5W8NF4rYCH/HtCv31QnUlV0wRjER0dvLe3W7fS03Nz8sWPHZk0dNSoFasccbDoGJMbF3Q3gkDn/38A1w2QjopHfeaZvTPPzW6dOnjwrQLCQp45dzOaUEbMU4LQbYCbAwjeukLTXSm+SmMjIDZD4LqytqZlb3bPnbPz/94X5XV8cPXponuJIEI1dpQCa03HPXHFFPVnQl9rxDha3uHceq3NQf431KZm7ztsHEPtNmNPpGxvklvrS5XReAzVmPVQh4v3ZYFYFWD8NeP5i5Pb6kVwlmyvLy0/hgGFEbxzAI8ZjZ1ZmZuu0iROn+fofG/uQD8Qcjha88xnR/xr0H++aLYauID//X42NjYXqfMB3cZEu1+viPkj1B0q6dbsd66xnBebBrEmTUvF9BlQ8pcX5+QvBXz5UoopP8wdsHWm/I81COGkiLmXGcH92AQHO5epc69+/fwwM1RsRiV0u36EAUBOXLoj/rFZtIVgf6wxAQcy/fRifrzGe/xB2EmTTHYQ2M4AIzEFplMlO5qBETolcxGh91UBckvc3A7kG+hOPSRzdRfeqRCfGRH/LNuDd8n9gSj4mLA0zwv2NLxr2/kcNBmG/2kdFp2fcPwq6waJ7NCE29n9Yqgsvu7fZhDY7kYumG2PYvK0TUQ+p7xSVAJTTRTBotU+H6be7KoqLe3Oa8HdBpXidvI8FBXl16o8EKwlYrG+8LRwsuJtidnRkpC8PDxjdqqvIFsKLskDrcfExiCi3+0U8O9wsihnMIwuXrwRYTJsyZXaAYPEXZogMawMscoWaTjDLOaeeOjwzPf0Wn7trbOzLws7WTmYlx2WdnZtkf/M9ngXQBvjGBYx2w4QRI3Lauq8Bc7Kga9eNXWgX3K2goK2cU7oCFnvT09JaoR0YCXXUdSQ5bYa0NNAqzRFLci/19UOkx65p6waxztOxO6Zo5laoeAYr/Q5G0bjTxLBhw3LBjHdazMkkc0FtkLST6wI0qqd1sEa+DOCRhzX0o4E6yiv/LioqGiN/D61IKcbhDWyUns2BBA5QEHEYp0ECqhMaGyPea2K4DhwsuFHEHzFklYyrKD//NQPjmTC1O0hfXQqd+065mzXKR6JBwsGE34Nn7pHvkUyTM3BPXu4LzFbCmcA69nwzn2m/qgBIWocxWVqUQbR2L0k0ifHxfzbYtTrIEDYFO71D/LliDFgI/m5hu8Du7cizpSQgvifAgMT0BhnYNVH5GNT36zdRo3YaALOXdLSiv60a6xfzC7cCFmFkmLuIVB7vjSAbUDW+kzsc5h7spkVVHIustsT4liv0MgeLSZMCBQvJuC71BxakEjgAZrd/3JgxVwuALy8rexnzkDYupt5LmoHEJasEw9ukNxTRIYw/j3sbSYnLk51dC7uEb+5Avfg41uFRtYfSBl2hcxjdHwlbwRPiPkib28s9nnSlv2ZgsR82m4OTxo0T/T9QVlz8Gp4T77f/9Iz8rKxu2JT4znqAfWo83edG1fmcqFbGoqAg/yHSZHwNlVRaJwWNOSkn0yKsQatuq00UL3MBY9ZdwENWSUkA6+Jk+RKovBZiHRk9v5mC/t6WB2BJiYfUZUZFN/NYCwZYyCCN77Fbug8oeBX0XpdjN7EMHfrCiAGT2kYM/mA5ibkuFd9LMfsQJwZ+9JlT1y/BqhkCdUQviF69RBuBnjeDQR5Qd+rqMzB5L5cDYgMsvBJ8IIp/UFRQcCd2LJdAD7sUIvGnbRmQ1OehvTuA6sJofgl2B9dD/NtM7fTyeyJg+KquqChgE9xBJ3QtZLT3yr6C+f8HuwfRxyGoPSrLymoxUU7Kycpa4QDgMv1mEwUAzTGgiSaMmbnZOVsMaNkigQ39WVdeWnoZJucM2IjOSoHHBsZ8G40R/c4yWDjICP1XSqR2pTUVAon9KSliMW3tVVl5Cp3joAULLGxKFvsgWRwqKyk5KAyUSibRsI4IyqM4izCLC10XtIHTwz/FvTjz4h2MQbQVQ79MJTK+oSEN60DO+2sYbThYZArJKi019XD3igqxLrdBzZpsqf/UT9gMffENYbq+BfMtUY6nv3GCBBKPdfpCQnT0negbuy9oRa7RjRZtSl7iS/uhYi1SxwGquRw4SHyl/PZzrAk5dwSQbDThV4cpX9jvGJ01BfidZuqlYIJFk4bFn5KU9IhPH0xFPZ82JTX1GYN7m0hMvI4tACcN9GyuYvExGgAMgUiyWUORlrdXZlraN0zC8F2F91IPeGFIercFFly0i0tIuHEYdKfqu3rBZQ/eUP/wB6TC46EX9MAGu8VbDO49TGmGT5c04Yuw25GjErdJGhFzvZcWhWFJT0qa7Twi7XjZjuZxI1CC5DeZH7gi6YmNwHfYrY0wek8JDJFdYFQlwGi2CBaaEouznvp/jmWmSkbHwR4PU0EdW7BAyRVegZKGWCMz2D0dChZWXGQxPuPgReSbA1B3TlWAwG+RbpxYX7+Rdo758+cbMWJdSTmxV/Y/PSVlto3+h5FH1SxxL7QMG6FTj7IYgKdTqowYce1MoEDpgbrHjgoKNHpStlvOR2yA58lNneQJkFTvUjwg+wAwDvP3SFpjjH8Qp+wx78zgFw4W6mfol59VJyRNPFFdMhc8Ovyp7Ii6U83MyPDdq4qTQi0CZH5F/b32/+/aKrM1wrfaLcVWrpYASg8WRnhidl5VlZUHrwXZVgs2C9nf29VJTNVN6rJ87CB3G+nnCRBfUumj0kZ4OUBX+QabOL73g6FxdQrfcaViF+4DuESaRFQUVQJNPlp4xIi5Wuk7iLjJagAWHbhyn9oeaXdxhTkPIX5jCOuPk6pLNgLS0Qs0dk0WJQuddLLrjqgN8x5UDHW6XWZ6vICFZBxw7Xxf7HZle44BWDiIvqtIffgKNlYRttpDv8NGIRuX3bBntcLldoo6lhwsZGZcuFp/COO5nRxWTlLlzTgyx+P3DB06tEDOCWttDT6j5O2FpP5bG/ENTbSJmMnnLoDgecaDWsTagYpLrj3hmn0tz1irvhs84gE2dzsdLLgevkHZdfDiIs+A8yRxVLCA/vPts846K1qdbJgQfYTHBUdLQSh4Jk21MFlctChWSMIx4PlIiYfQ2pAsvMTsvwGKJ3Kiq4wGO6wnDOjToqFGh4cPNWmzi3YIl8v3qs+IjIp6ip6vmU1M8oqaR0FNGj3TwasENhj+fsv7KLychLsvW/CpqB9IuqlgCyOnBE5XW7vPPlVVQ6ES9NowcIdRtOrvFeeJUazPfrwz2P+PD8liF0ljt7Ox6wyw4HSJR91CEuXldqQcPsZJCQk+jQGM9kxdyCQLOGz4bFCJiSts9l9Xduq7yRPrTjYeNuZD8AuM626sZxZb5Pf8ie+hXitktKlQN3SMt7ynaFaiwJ+MYyno/dhQDpf06nywIEZIouFHmDjpbQyMg3S3tdLdVdwPhuG7v7hbty/POfPMAgY2FxoxbaELJ6nC2sHqHs8g3MMJKN7tRfCK4gVkAhZ0BcO7kxObvws2mivkPaokBKPrOxTZrJkZ1mMcjgk66MKlqKT4+BemEk14sbHj5oE9A/luBWMi9JvD2ILvqYyzyvD3o9YyepgudBynaieCW6O5kiVPJQNddsLWc5FwmTXz2uA0ON7AQjI6zJEF7PedCRY6qZu6g/5yLKcpNh+nv8okag3u16uoX/cp9OH9z5D9Rz1Htt9uX+E2K2ku1CurSwsL+xr1kcbScawSZ8LluMpfwj+++4dU/6iahobigW4x0HK0kH1juwdFSUM+hr7n720isLiW0b1zJQvJhMFkXhIncXF05141EFursbP6iYh01GAMP/nttT17FknmR4S614jxpiUnP+zPo4FFfcbBh/x77ufvxE5a+O1LAhqCBe2kyVvkt0bEZv871wBo0OaU9f7iOyAqDlXdgp3kRdWnpubzRYgPMXEDdUjHABHVDvCb7IFBGGLpChFta1CXk/fMakkPxsSHqGMASeUUDirk3rdVBFVZVXmg3MCCiAzBgt+HZGiTwpUAzPj4uG2QgB4QOm8RSS8wxWAc9OMULGQ/JsnndDZYHA1sq66eSrrvnTNmzCgKtAEUzX8HObY87Acs9kpwst9/WsuNjYllpaUviueQ7fEA1s1G2EovhW2rDzaaiYYePp2fCfgaG15QzRQLMkfOIxlbAQlqi4F00kSR1bcpv3eSI8Eq5ZkcjO49dmChMkVNe5w1xHCSnn766dWV3bvvJ7WHV3rKIOBqe7/y8qMimPDUwOBvlICkvgsBaMwYbslCtonvbOm6UHkWt1kcVokNb6uTOVPhkyQjNfV8I1XSiGENEiwcZkBa06vXMEhohzlYYGGrYKFxoHRHRo6PhaoKXxwQExTVSvZIswRkqqpMeHMsMOqPBj2qpQVP3/fo0V1RP/oFC35eQyU2DmvlzpQtms0ZaWlLIfKfAiNmJnuvTbAIfpwF1K2tQ+rrp7D3daZk4aDgyuk0D/aQJ9NFqJegXmyjXirUylBHbqb3r+a053EWsNe0NgwefDIbc1u79gsR+Q4nDiHBf2Qwtz+Do8kD4iCh5Li4ap6VtjMy5k69aqorJjbmdTteUCRBx8t5RHN0sqHNgzaw+L6WAyLU6kMpaNZrABbPM7vwsQEL1PVWwGL27NnV3Q3AIiEmZnu/qio1WlNH3Sh3o2pcBoyDN9oAC52uT/N2ixOvaqqrl9PCNQYLuofaMc6fZAGp5zwj5jpu3NgOAwvpXrxkyZL4vNzsR/guRQmMO9RGPWwFLFDOMOpPrx49NtoBi5EjRkivNstgwWmN4M0KFJG25TFITl9jVyk95KRaYhs2EvenJSR0P14M3Bws4FQhggA7HSx41P+oxsZroeP2jbvIFgC7g0iNIRwkbFVkVBCeUMJo3YqI9L/5A4tsBCWeMnVqoGDBtQnJMJRPTIiNvgObhfcjoqKk5CLTZ+8C3V+CeuxUdn+wigygGy5ijiyqoZpp7qoqPJ0cS9aYqaAwdpvVcZb3IAShGP1uNgn2EzEXBEi/ELCgcxGcsPa/KI3gKqNB0jhLujdGlOd4u0W6kaqePa9VwYKroVQDPto7IVCwgOG4w8BCTozYiKj7HLpDTb3iZX20Xs3B4kyj/oDp2ZIshjc0nG0fLMxVCchKmljWrVt9VEzMBRnp6YJJfafspPbBaDtbjsvxJFmIdB9TJk7sdLDgc23QoEEX0w50R+/Kyvlitz+wf/+ZAwcMmGG74hz1QXV1Z8Kjqo4zIg4WiNwW6VUCBwt6Pqe1UIGDnsIYPBNq/OVY2++oUjQyKaxVVJZaUAPxUtNvlLt/q15QMGxPka7LlIzVg/Wxz0iioMjsBfKdTOWeiHH91kjFjDH4NguBjQw0T3CwoImAQV/HGEwTRfTeYUfnKYLKUD41UUOd3ZYaiv12vG2woOvYsR0FFpQny+MZLNvIdy8wBgtXud3os3DFfRl1E+orVDeBnv+CrprlajIGi5Kiol8ZgQU91zJYIBjwUutqKPPzC9RoVLUgWrtrj+7dFyE7qs/jiOxgUyXNTMGioMBWug/2vHtI4rz0BAALDnSCYQvJ4IeJEyd6OvpEOVOwyMwMCCwsnM3CS1z/vn1HIQjwZWVtPELvDBpggOlH+DK8apa8oFrIs+tzeP5lsk3aInW9MW/EbzwoJkzfje/fZ2vaS/z2MG5jTiknPli4KfHVjewdLbTDf82O58u4ESNKMCAHOHMUOysEtY2WxDOKszhewQIeUktNAgdb3Q59lT8GAOMZ04cag0UdAu6MDNwYmy+gH822GkUL5rwWFwsGbvt5eFSa1kBHCcb0Cdm2vlHSO2hGYIEYjtbxY8ZYBwu6/6mnnnJXV1U9QyoEW5LFtOMALGAIbiQaNSG300QZryTaFGiVfQ0WWFgNyOR0JVD3kgp7Nvm6d3gGWso6MJTshrSO/UsVqCulVEFqZhdsdG8YZEyQGYfvM5OaBU0pWNnQyA0VGVOln+BgIUUxkdES7qocoYW+7nBDfX2lBXEqjIJ5zpaeCQww9sDbJtOP66y3nWDR3MFg4SCOxeI5KCYjMvwZNoH5UZNuunKvCQ4WkvF1Q92tsbQlAmiRIHG634yjdBYF1AJf2ck6CwcHu6cG6rJviMEZ4dD1Q5RDaAqju3yeDyxc0C0nxcbOsqvWnDlzZixUcZto8V5+woAFtWdATU2+cIgQYwCp7/wgMJBggAXp8+2rqsIoU22k2/1YkNQwDnJfPV3JytzCTtAzyjMn1ENj1Q0aVKiDCSS4zcMr7Ez5ygbXqA0Y07vlOuXqK2wSFv6iwEL+dtKkSUmarn/bxYBRZaWn38MSifESRudfhCN/zbsSpdUrzpeQbdb9RHAfP2BBbXVwGwwZ4dOTky8wDIyk/9EOsszpDDvoRw3loLGIACj4RHmNARO2MJuV3EthDJh0maupwOO5iDzm/Edwtz+gSl+2bJkIiPqPeA/E7ouNwEJIRTDKbvdFt8bFLQjABpaC+jFlXr3iBAILjZI0RoIgL/n876Oj/0qLNGAbQieBhX0jNY0J7JzX0I7/vaB5QqEsWrQoDjmvrsAG9RWecVlmZWbr7guRk4tlZFhuZtiG4XyLkDxM5wOdjKfcz2MtbvrFqKH4Lr+4qGgJMxZ5RSUjz1xGKFnDlFn1R13XDFN35OflTTZJ97HueFVDyfthsFtvNA6ISbhBgqiiHpBVnrLF3PpMwILuQRrtK4wmn1h82EkhaMi8DB8yZFBSQvzPMq7GUm4oFDgE5GFO3AjJr6eNA7I0UUXmVCTrk3rb33GwkD7sAMFX6ICcZTbAQroo9lXcj688UcBCbWdqevrVMuARu9WiAHfcuqxBAgvN10Ecs4oEkfOrevc+zw6zk+9AMOdlsGdR1HOQCx2EBFWnyEBxqWDwkAha2Nr5mWVGdlI+u0TFWcNroLLyG+sFVdN8bmBXnHQe+aUZuHVVXI6PjdnFspd6ZY56pK24ypewjhXkri/FznG1yghVAsZERTyv7Io1DhYngM3idhM10o9ZWWkNJhyiLxj3WxwoOFhwX2xM+K4ybz5PO0D2i7/hN/X4nPjm+vWR4iB6cZwkAucWQc0jD4uylkiQxj05LuEp8uBYpUiQfgvpfCNhuP1Y3A9XzXMM6O4kcf12Opnwv7DxWHUpdJFb41Kak60Cy04wsNApvXU55pgvdgWHCN1rl84Gc1nrcLCgd4gMwmK+oL3fg/ZplgGDxgS84nqah68G2SNKMzrLnDIjLMH7t6rrADFEIyXdaa3NVcMFWOaKveK0TrO+y/kE/jqhjQzRb7LbTnywYAnEFviIRYyeh7TjzOvP4HcvGPxiEPQ26CU34H+7VIKxz00whLId64kFFvCemKCAH/OucItdy18rKypuGT929FJ4hNyKxfosaH1I9scCWDhYX5fy7L8Gnmof4nSzLalJSVuxOP9XpTuq1fMsHOTdNCbMFeb7LU6PO5WrHHlRF1xJYeE4ROz7+oe8VPXKnOIBlFPkyY4pKUkSVNxtMD03ta0fJLsDsTjX3DcvTyDJgt8b4XTeIhkSAGO2Aohm9iKNxkpmZBgBF/fXADxlsr/BkCxQYuFI4FMnFxUWPkEbPbJvtmnM11auXBmGzYPYJInN5XKrQBMMTy2Riw4AUQ0D9p/gxrpYyfLrpHnyhIG9QSYZfNSs7ez//XDPQamFYSnQvxfHszLanthgwQmANL23q0eSckLyyr+TTBXGo1a8fyZfrBwsjlvXWaINpeB+nTNwlTZgmOLcDfG9ShOf+siCGkpnuYTiIE5/agQYbRjxmttzUl5WVsZKEp13IhhvmsFYObk3FJ6XDHB8h9InbGan5XEJxBXhcr15hNb6wbS05LH8N/w+iPg9kOdrP5jOU/Ck+hPt+E6MOAveN7JjYbf6Fq3Bg7AvzbHqnpqXmTkfc0y6Km+yABZ7bIIFj+Rv1I8cAyo80O4hN1Uei+PkQOfEEbE6HcoFW+eQzk6mx8Y6Be2eD+mou9E56Fhjm82OcUC/T/bTdp28SAtw4Q4lvor3HkCOulxyIukksCBPoQDBYp8BWHzFwYIv8oqyMnFwj8L8+dGqVCnJITti1cc0PTk50xnBrYBFUyeCxSF/QXly4iHl+2AYrLwGDNzL6SF296oUkgI1FmwR8nAirylY0EQnr6Y67G5+lu/jnhqUEbhFJonUpKsfAKq0uHgbs1scAYuBA03BAgUkcDwk34G+3pMO1Yk8BY0b77ERGISPb9PvD0ACGGpEc3awTB0WqGzTPkQyXyvcjoUbqQgM3Ygq1h+CrosAEBfSmfN7KWnmHQHEWRyzCG6z+S7SpGiUWZj8/leDJph21ZF0eqKkmQuXaARDjhJHCgAofL8HOH8MevULBljwvsLOdKoST/Q2NAoT582bF8N/fNWR92aKMZLrGCC/pLOBgoomJRwYn5+mDca3kMg8XHUl8j3J+UtrSq7r//rJsM1Bx2ePkxKGuNJzNnZmBPdIudgZWDwRDMmC/40I0F+BED+yHXGT2DFjAESV53/z86dfrygpqVMnSzDAIjU5+XwTsHjCAlg02MgNJY+/nCgOcmLGs8M6p4UqbUREXCi8nDBZFS8xY7Dg/UPSuFqcT/EJkx58tBfvMqI9FvhMt64vMArKazCQLLhkgxTzi910RrSoaMjmivLyu6traq7Aj84HKAhXQZ80QQfx7IY31BgLjMFJqoHTmU/6HqTA2CzUHcgHtl7MHczVQ/i/iEvYjl1tJdk8HmTHqjrbAIscCRYnTZo0uYPBYokCFk4GFpYAox6p7XGELz/Q/2N4kz2G6+2Y13dBxSii9r9UJVWk/FiDZJOZal/9gQXAclqA3lBOOoxrDLIYfybbEBHu/iIjLfWhAXV1i7Fr/w3OkbgZGfg2yMSNJGXewZhqZxaNJNlwpB55W6Yuwrwt5fZBeFJl4SL7ptYploNgyf1WJm6UFer8g8iG27czAXOMkqL6Z7paBgu4stZUHpEsDmEBHgSFBOK1xEOy6NOjBwtFNzaoocN50Pctxc7iOxhQOVEpL4wuRK5mLPoP4PV0FiZ9uLr78wMWa+VOk/ooA/om+sscS+d1SPockvSBN8aT/sCid8+ewwEWB+m+nwEWPtrU9u69beHChUcNsPyd40eOLIYq4WFxEpu6kFWAAJ2E6mRD/9raIWSrCcdkfYGYo2jjQQ4WZotVHFwDdcwyjN8P0PmbJSlsAvN5Dd4dDT4lam3tuSTFHKD+HSCwOJUzDs5oiK61uDyEd+41TJSI/+F5O+Kjolb1xcH0NhaEkySTPjnZ2S/qDn0/fzbAQThT7IYd7BY6a1qWe1Atuc5it54nFi+BxdQOBovFUrpnkkUgh0RNBo2fAy33S9UNOzdfSB77EB3/HMZkAp+LbYBllug/gcX0gPtPNK6tqEjDmFyGttLGhVU6Xx6bjJfBK07i7TlWqcuLK4orcVkjjzDmqVEoB1oeaLsUNHsN0vTasrKyBptt1yljdO+s7Kz7kxKTRIDfCtiXyjqbBgmog1D7ow6gq0CxMiuHzogDjrAwffdisMVV1HrsBPvSaV2WJ/YYnLwnjvwUvA8G3xVA5Q3wjHochF4W43JePGzw4GEi1Qe714oxrZT3kf5u63QvjdKqZwt68HvHjWss93fvALi1kleRSp96BJb1ESoRf/SAXr8gAudyQ2JYDrfi9VANbMjOzr0b2TcvbBwypAdf2Ng1l4u2KeMwENWfV5BDPSo3MSbmDGyxbwHd1wFAnsY4/FlkJMUOqk7RiWo4DCaF0xTvHdSAc5z9zxt6J51HHO1yTUFOqN8l4pz1OLjXwkh7ocBMLLJU1k6rxaFEtvfG5VfYiV6H3dlinLN+DVKeTG9sbMwwoHkx6jDUHNYHM+N7HXa+mAvj2m1gZPfn08FjFYE+k2ciFbveeAQrwuh9FRjWzZjXNzt1/QIwsKk4S6aY3WeF+bhQ+2Wnpw/CxiAloP5z+lMcUDLUjzg586zc7OwbAA43QVV6tVBiFKSlVRj071gW3e5v7IKcVfD+5ZfAc9Q77RwXGej3x3AyOqjao1vw6W81Cltrb/Qu/12ANNTa8Wyt4wLZOv85fLys/vYY8wBn0NoZfN5lMXEmfW7/OqV3dr7+TefVbpStYQ18cjtVXa3MD0NusUHuY/DvtZnigNPCIfP2WHyfFiD9df63PbrYfyerxOQ6ZjFbfLZmsw+8z4GXwNsRfDp3fv9143bi/6ESKuYTHDVUjtIiRPtQCZVQCZVQCZVQ+b/24IAEAAAAQND/1+0IVAAAAAAAAAAAAAAAAAAAzgL4cn2g77WR0AAAAABJRU5ErkJggg==" />
                </td>
            </tr>
            <tr>
                <td style="padding:25px; background-color:#EDEDED;">
                    <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                        <tr>
                            <td style="padding:37px 47px 0;"' . ( $data[ 'lposts_data' ][ 'policy' ] == '1' ? ' colspan="2"' : '' ) . '>
                                <p>
                                    Hello ' . $agent[ 'lname' ] . ',<br/>
                                    <b>You received a booking request!</b>
                                </p>
                                <h3 style="margin-top:30px; font-weight:bold; font-size:13px;">BOOKING REQUEST DETAIL</h3>
                                <table border="0" cellpadding="10" cellspacing="0" style="font-size:12px; margin-bottom:40px; width:100%; border-top:1px solid #C1C1C1; border-bottom:1px solid #C1C1C1;">
                                    <tr>
                                        <td style="color:#AAAAAA; padding-left:0;">CUSTOMER</td>
                                        <td>' . $data[ 'lfullname' ] . '</td>
                                    </tr>
                                    <tr>
                                        <td style="color:#AAAAAA; padding-left:0;">PROPERTY</td>
                                        <td>' . $data[ 'lposts_data' ][ 'ltitle' ] . '</td>
                                    </tr>
                                    <tr>
                                        <td style="color:#AAAAAA; padding-left:0;">CHECK IN</td>
                                        <td>' . date( 'F d, Y', $data[ 'lcheck_in' ] ) . '</td>
                                    </tr>
                                    <tr>
                                        <td style="color:#AAAAAA; padding-left:0;">CHECK OUT</td>
                                        <td>' . date( 'F d, Y', $data[ 'lcheck_out' ] ) . '</td>
                                    </tr>
                                    <tr>
                                        <td style="color:#AAAAAA; padding-left:0;">NUMBER OF GUESTS</td>
                                        <td>' . $this->get_format_guest_num( $data[ 'ladult' ], $data[ 'lchild' ], $data[ 'linfant' ] ) . '</td>
                                    </tr>
                                    <tr>
                                        <td style="color:#AAAAAA; padding-left:0;">PRICE</td>
                                        <td>' . $this->get_format_price( $data[ 'ltotal' ], $data[ 'lposts_data' ][ 'lpost_id' ] ) . '</td>
                                    </tr>
                                    <tr>
                                        <td style="color:#AAAAAA; padding-left:0;">DEPOSIT</td>
                                        <td>' . $this->get_format_price( $data[ 'ldeposit' ], $data[ 'lposts_data' ][ 'lpost_id' ] ) . '</td>
                                    </tr>
                                    <tr>
                                        <td style="color:#AAAAAA; padding-left:0;">BALANCE</td>
                                        <td style="color:red;">' . $this->get_format_price( $data[ 'lbalance' ], $data[ 'lposts_data' ][ 'lpost_id' ] ) . '</td>
                                    </tr>';

                                    if( !empty( $data[ 'lrooms' ] ) )
                                    {
                                        $content .= '
                                        <tr>
                                            <td style="color:#AAAAAA; padding-left:0;">ROOMS</td>';

                                            $arr = array();

                                            foreach( $data[ 'lrooms' ] as $rm )
                                            {
                                                $arr[] = sprintf( '%s %s', $rm[ 'num' ], $rm[ 'name' ] );
                                            }

                                            $content .= '
                                            <td>' . implode( '<br/>', $arr ) . '</td>
                                        </tr>';
                                    }

                                    $content .= '
                                </table>';

                                if( $data[ 'lposts_data' ][ 'policy' ] == '1' )
                                {
                                    $content .= '
                                    <p>
                                        <b>You have 24 hours to respond.</b><br/><br/>
                                        The sooner your answer the better it is. Timely and friendly reply
                                        ensure more successful Bookings!
                                    </p>
                                    <ul style="padding-left:10px;">
                                        <li style="padding-left:20px;">If you Confirm this request, the customer will be notified and will be asked to pay the deposit.</li>
                                        <li style="padding-left:20px;">If you Decline this request, please contact our staff to offer an alternative as soon as possible.</li>
                                        <li style="padding-left:20px;">If you are not answering within 24 hours, the booking request will be cancelled.</li>
                                    </ul>';
                                }

                                $content .= '
                            </td>
                        </tr>';

                        if( $data[ 'lposts_data' ][ 'policy' ] == '1' )
                        {
                            $content .= '
                            <tr>
                                <td style="text-align:center; padding-bottom:47px;"><a href="' . HT_SERVER . SITE_URL . '/agent/confirm-booking/' . base64_encode( $data[ 'lbooking_id' ] ) . '" style="display:block; border:1px solid #2FAC66; background-color:#2FAC66; border-radius:3px; padding:10px; width:196px; text-align:center; margin:7px auto; color:#FFFFFF; font-weight:bold; text-decoration:none;">CONFRIM</a></td>
                                <td style="text-align:center; padding-bottom:47px;"><a href="' . HT_SERVER . SITE_URL . '/agent/decline-booking/' . base64_encode( $data[ 'lbooking_id' ] ) . '" style="display:block; border:1px solid #D0021B; background-color:#D0021B; border-radius:3px; padding:10px; width:196px; text-align:center; margin:7px auto; color:#FFFFFF; font-weight:bold; text-decoration:none;">DECLINE</a></td>
                            </tr>';
                        }
                        else
                        {
                            $content .= '
                            <tr>
                                <td style="text-align:center; padding-bottom:47px;"><a href="' . HT_SERVER . SITE_URL . '/agent/login/" style="display:block; border:1px solid #2FAC66; background-color:#2FAC66; border-radius:3px; padding:10px; width:196px; text-align:center; margin:7px auto; color:#FFFFFF; font-weight:bold; text-decoration:none;">CHECK DETAIL</a></td>
                            </tr>';
                        }

                        $content .= '
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding:25px; background-color:#EDEDED;">
                    <table border="0" cellspacing="0" style="width:100%; font-family:Arial; font-size:13px;">
                        <tr>
                            <td style="text-align:center;">
                                <a href="mailto:' . $this->getSettingValue( 'email_booking' ) . '" style="color:#FFC200; text-decoration:underline;">Contact our Booking Manager</a><br/>
                                <p style="color:#C1C1C1; text-decoration:underline; margin-top:2px;">
                                    Manage your Bussiness on 
                                    <a style="color:#C1C1C1;" href="' . HT_SERVER . SITE_URL . '">NomadSurfers.com</a>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center; padding-top:20px;">
                                <a href="' . $this->getSettingValue( 'fb_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1Ni42OTMgNTYuNjkzIiBoZWlnaHQ9IjIwcHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1Ni42OTMgNTYuNjkzIiB3aWR0aD0iMjBweCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+PHBhdGggZmlsbD0iI0MxQzFDMSIgZD0iTTQwLjQzLDIxLjczOWgtNy42NDV2LTUuMDE0YzAtMS44ODMsMS4yNDgtMi4zMjIsMi4xMjctMi4zMjJjMC44NzcsMCw1LjM5NSwwLDUuMzk1LDBWNi4xMjVsLTcuNDMtMC4wMjkgIGMtOC4yNDgsMC0xMC4xMjUsNi4xNzQtMTAuMTI1LDEwLjEyNXY1LjUxOGgtNC43N3Y4LjUzaDQuNzdjMCwxMC45NDcsMCwyNC4xMzcsMCwyNC4xMzdoMTAuMDMzYzAsMCwwLTEzLjMyLDAtMjQuMTM3aDYuNzcgIEw0MC40MywyMS43Mzl6Ii8+PC9zdmc+" alt="Facebook" /></a>
                                <a href="' . $this->getSettingValue( 'ig_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDU2LjcgNTYuNyIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgNTYuNyA1Ni43IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnPg0KCTxwYXRoIGZpbGw9IiNDMUMxQzEiIGQ9Ik0yOC4yLDE2LjdjLTcsMC0xMi44LDUuNy0xMi44LDEyLjhzNS43LDEyLjgsMTIuOCwxMi44UzQxLDM2LjUsNDEsMjkuNVMzNS4yLDE2LjcsMjguMiwxNi43eiBNMjguMiwzNy43ICAgYy00LjUsMC04LjItMy43LTguMi04LjJzMy43LTguMiw4LjItOC4yczguMiwzLjcsOC4yLDguMlMzMi43LDM3LjcsMjguMiwzNy43eiIvPg0KCTxjaXJjbGUgZmlsbD0iI0MxQzFDMSIgY3g9IjQxLjUiIGN5PSIxNi40IiByPSIyLjkiLz4NCgk8cGF0aCBmaWxsPSIjQzFDMUMxIiBkPSJNNDksOC45Yy0yLjYtMi43LTYuMy00LjEtMTAuNS00LjFIMTcuOWMtOC43LDAtMTQuNSw1LjgtMTQuNSwxNC41djIwLjVjMCw0LjMsMS40LDgsNC4yLDEwLjdjMi43LDIuNiw2LjMsMy45LDEwLjQsMy45ICAgaDIwLjRjNC4zLDAsNy45LTEuNCwxMC41LTMuOWMyLjctMi42LDQuMS02LjMsNC4xLTEwLjZWMTkuM0M1MywxNS4xLDUxLjYsMTEuNSw0OSw4Ljl6IE00OC42LDM5LjljMCwzLjEtMS4xLDUuNi0yLjksNy4zICAgcy00LjMsMi42LTcuMywyLjZIMThjLTMsMC01LjUtMC45LTcuMy0yLjZDOC45LDQ1LjQsOCw0Mi45LDgsMzkuOFYxOS4zYzAtMywwLjktNS41LDIuNy03LjNjMS43LTEuNyw0LjMtMi42LDcuMy0yLjZoMjAuNiAgIGMzLDAsNS41LDAuOSw3LjMsMi43YzEuNywxLjgsMi43LDQuMywyLjcsNy4yVjM5LjlMNDguNiwzOS45eiIvPg0KPC9nPg0KPC9zdmc+" alt="Instagram" /></a>
                                <a href="' . $this->getSettingValue( 'yt_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjEwMCUiIHN0eWxlPSJmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6MjsiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHdpZHRoPSIxMDAlIiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnNlcmlmPSJodHRwOi8vd3d3LnNlcmlmLmNvbS8iIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48cGF0aCBmaWxsPSIjQzFDMUMxIiBkPSJNNTAxLjMwMywxMzIuNzY1Yy01Ljg4NywtMjIuMDMgLTIzLjIzNSwtMzkuMzc3IC00NS4yNjUsLTQ1LjI2NWMtMzkuOTMyLC0xMC43IC0yMDAuMDM4LC0xMC43IC0yMDAuMDM4LC0xMC43YzAsMCAtMTYwLjEwNywwIC0yMDAuMDM5LDEwLjdjLTIyLjAyNiw1Ljg4OCAtMzkuMzc3LDIzLjIzNSAtNDUuMjY0LDQ1LjI2NWMtMTAuNjk3LDM5LjkyOCAtMTAuNjk3LDEyMy4yMzggLTEwLjY5NywxMjMuMjM4YzAsMCAwLDgzLjMwOCAxMC42OTcsMTIzLjIzMmM1Ljg4NywyMi4wMyAyMy4yMzgsMzkuMzgyIDQ1LjI2NCw0NS4yNjljMzkuOTMyLDEwLjY5NiAyMDAuMDM5LDEwLjY5NiAyMDAuMDM5LDEwLjY5NmMwLDAgMTYwLjEwNiwwIDIwMC4wMzgsLTEwLjY5NmMyMi4wMywtNS44ODcgMzkuMzc4LC0yMy4yMzkgNDUuMjY1LC00NS4yNjljMTAuNjk2LC0zOS45MjQgMTAuNjk2LC0xMjMuMjMyIDEwLjY5NiwtMTIzLjIzMmMwLDAgMCwtODMuMzEgLTEwLjY5NiwtMTIzLjIzOFptLTI5Ni41MDYsMjAwLjAzOWwwLC0xNTMuNjAzbDEzMy4wMTksNzYuODAybC0xMzMuMDE5LDc2LjgwMVoiIHN0eWxlPSJmaWxsLXJ1bGU6bm9uemVybzsiLz48L3N2Zz4=" alt="Youtube" /></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>';

        return $content;
    }

    function bookingAcceptedMessage( $data )
    {
        $content = '
        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
            <tr>
                <td style="padding:25px; background-color:#FFFFFF; text-align:center;">
                    <img style="width:250px;" alt="NomadSurfers" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAYsAAABXCAYAAAD1a2lrAAAuWklEQVR42uzdb2hVdRzH8XP/ue3u3uvdH9e2dP91azDm1BabKQtqOlFRmyVTiqT2INAI90iy7GE9uA/S8EHSIgh6EEVJIBJB9cCCKMyKkFAzliSR2B902z339rnjd+Jw2GX3Thfaeb/gy++c6+7dFPf7nu/vd87vZwEAAAAAAAAAAAAAAAAAAAAAANweAjMBAIBH0IRXYOZ1AIDvhSyXsbGx8lQqVWmJS9gCAPjTzp07Q5Zs3br1rs7OzoM6/HTRokWXEonErzo+E41GjzY3N3eTMADA5xVFOBzui0Qi53WYdUcgEJhpg8HgVF1d3bMkDADwaaKoqalZq+aaSQ6Tam1FxoStmDLHWSWVsX/fCwDwh9WrV0dKS0vPmkpiyrSzha1IK5lk6uvr+y1h0hsA/DOhvcckg2nT5g0lipmvSSaT71FdAICPkoWqhBPuRDBH2Lm2pKTk54aGhhaqCwD4fwsogocPHw5qCOpzkwjSBSSLTC5y8xqaDO+lugAAHyQLE6eLTRaKSQXJAgB8dCfUyWKHoRQTijaGoQDAPxPc+5Qo5roTKuueBNcDe+9TVQCAPwQsGRkZqQiFQhcLuCMq4yQUVSMPkSwAwGfVRTwe36IJaycpTCpsT5KYdhLFsmXLjpAoAMCnCUNrQu0uKyv73VtNuM+XL1/+SjabDbF8OQD4OGG0tbW1qspIaQ7jnNaCmtbwVC5ZXIzFYu8sXbp0E/tcAID/BEySCJrziMKR3LhxY/vQ0FBnX19fpTupkCgAwD+Cs3X6GmIK5Pt65igAwF8ilqFbYB/RkNOH5eXlZzVf8Y3Ov9SQ07FVq1ZtYcMjAPCngNPxd3d3363mA0U2X9TW1p4YHBxs5sE7APCPsGW0trY+qoUAf3E9V5FW2E64b5NVtXFBbYcnYQQUQSa6AeDOXesplK+a0Jap8bJo2avmae1Cnti+YZYBOa0FB73Jwfv5ESa/AeA2SwieyD8prXC2TFU18a2rmrDNcUHLe+gW2hHXz7BE0aFbbKvN3t2zToaTPADgv00OYRN5HThwoFrDS21NTU0bdJp0f4b2zT6Ye2bCW00UkywUxxWO5xW2Ko4zmhA/uXjx4mPaDOkZvTbY399fo9YrTOIAgFvFe1Xu0d7eHlfTpKSwVlf1T+j4ZcW7Wqbjq4qKigl13tcTicQXeniu0pLe3t5m/dnHriex0+Z4PsnidUvMKrXjeXbRm1QFc1nHHyk5HdJ2rQ9oY6Vq73MdTJYDwPyFFBHLZXR0NHd+r4aPntZ6TG+o4z+t89/m6NwPWdLT09MYjUZ/NJ34lGvZDttEUclCn/WaJZq7CCsxfeL63LSpVqad7+GJ7/XzH6murt48Pj5e6q42SBoAUGwlYaxbt65OQznDujI/rjinl/7Ms6dE2hMZdco39LR1v9kq9UnTod9wvS9tXitmOCpt2jFLpEXvv+De28IJTwUz7a5k9HexNVz1Q1VV1XNqW0kaWGgBd3Dr3sKPlw8MDIQV/EIv8O2sqhru0xDSEXXEl2a7unddxWfybUikIaCvdeXvfOZbTmfvaa9omOopdd5OhZKZa2c8fa7d0dGxwZKWlpYHzXts0xayWdKUaZ3hqmuRkshRHXfw4B9wp68X5MECcguzcJ867i5NEr/t3KY6y7MPmUKHirSX9guW7N27t17Jx50MMmbDop+6urpatm3b1qrjQqqKjLkT6oozcd3Q0LB/PhPlJmzPfhl/qNI4unLlyiZLzJ7gXJTg5j22fXvVnh076nS7Xu1MbNpUu2vXrvpUKlVm4VYIWsa+3bsTanrX9PQMD6xf/3g0ElnjrBtEwrgFCVlWrFgxquZvz8ZCdnEdsLnKV2jo6X5LGhsbH1blMNM5u7dL1YqyL1pSWVk5bIai7AK3UP3M+b+hu57edFcq84yMJ2lc1s++35UoIhZwM79ciVjsVCIevxovL5+Ix2KXy6PRiYpk8urmoaFhS5RAWOhs/oJmUjV6T3v7S/q3Pa/TvzQ0kq1ZsiTX4VzXcMR3em2QjW/+Ye86oKQqsjbvdU/35JxjM4GJDDDDMDAwpAGGHAUFRJK6rP6uGFbWrOuaRRBXBGVd18iCi4FVdA2sq4LZVTCsa0DF9KMkCcLM9PxfNbf469zz3vR7PdMDeLrOqfN6pvu9V3Wr6n51YwVYaI6SZ9OvQU/GOAOqzSQ1vAojcgy95k9S4lDBJCUlpV58iTxQCwksmq1ILGDkt9Fzw1Hf5faKjgINRIv/s7a2tozoFHK1DZV27XjfMpp0gwYNmhUCi/aD8YIFC1LBSN5UXSFxfRX1BdRd4iQ17FpHhcCifXQeMGBAgthNt2eHzhk6QP2P4tmwL6VDcviYxq9FPh/j+hZUUAnUjpvlvVae3bVrV9/6gvG8ECopVRJq7aDaoqi19qDO7BIqodJOsHiV7ZjE4SytWCAzQ2DRfv05dqbSKOoFg1g5ZsyYfHyO7uLxhKeHh3twvsEpGzduDBkiAyxSzTJ37twq2Bc6iuF6AeJeGJ59BxNBehitSA1eqYKCrWGZbAd28A9aAAvZtv1IKNhH3IfrNFW91ZGVtwe2jEvXrFnj6hIqoRIgWLzGROBmMXmhUw+BRftp2xN1t6ArGM89RjvikL2i/WBBBujeBBaBVm6A/kzkfSJ7xJXcAA3AaIbKq788rwIBfS9bkGqkh9VWSCXp1IVrVKYejCpdfZEmvXXIkCG1XUIlVDoQLEKSRQe4byLoa4FvsWInmZ+dXST+hwjcsNDhN8ctWHiFmpBiGJYqUd7nEtM9qIDBFnJ/7jJ9+vRklA/oN6YSgpRI0tLS/i6fjfs2qN91ZKX++AAO8SK7YCcZ1iVUQiUEFscfWOCwm6U+/bTH884keJ2FpIjgggVUQfbVOcSouUQAe0Wjsk6iUJ+RYEAM/w8KmBTj3d+wdWSqEtK76FfTrZl43ocdaNzmffKS++/LDQ0N3UJnaIRKCCyO38CwWwVN4Rr5d5FmIhRTETwDN861jgWD3GZHpUOJ+1SQ+Boqpy9hr3gYn+OZ00Es0n3Mw+783+K3yCVVJxuA8R0EtZWVGI4W3aG3pmelTxP3IcVHPS4H1biNDqjNSlbbJjhP3KQEFYZ1RFpePUiTWCNm46todBDeY61/du6ldgarzbJtQaSLeT+CDBYaq7/ISHc7YEGG0b/T/7RgRd13dv+PlznBGPpJCtM8ZMaAuW0BBuw1UBmeIjSHigFYJ+aqMf7xCpj8ewL8Jd8EgESCMW9q22ZBQXxu197hw4f7VJLIXHtqR9krBOip70Z7nkPMSQ13L+ZRorzqjEHwScsP3NCDleFRlkDzsnMma/Ac3ey5/Dub32t222v7/fSOoL+H3tExYME3BFTanxHT0U7mqtuZy4zuVp7jMPmfrG7631Ki6ZPkT++kar5GA6ez08iPvtP6z1VD5r9xyHYGSVIWwXE7mOqnhQOFpmutIk9UYUXhCBaz4TrnnHNi2bMlgNTS/ZepacLJPlVvpk5i/98id/gZaRl3tdNe4eX5omg9T5UND2gd8UlWVVFRGgFrfmZqat3Afv26k6jCftu+lAZzEBE8duTIfvjYV6QABgPqedFFF8XwXZj155sXaWySA16Sn98dH/sX5OXV4TsPD+Ixa7egA87IrUDK4P7wWqgbPXp0T+w2HNbba35YivScEGmOS46IsX3Li4v7Txg1qgzvDefj1Z68OKLNo/BcuPcJj426YQMH9hLBYez5AYMFpzm9MwKiego+JuK7ZFM6UAl43IN/v8b7Tf1KO++88+xEtl9HNH20ne10MmbsqqysTBV0xlxNxZzS2/p98PtvPsbjxo1LFu1EpHPKypUrI/n8CYaEgZ1/Li53ou4zMPYeIgb9dnl5ea68ETaIBtQVUNs8j9iJzdDz/wV/nyEyzcrfwKPtKnFvaWlphQpQeI6LvKaWKgBlJsksl8+DRHJlAGDRwm0sJB39E3aTGXIecb7GS3fUB1DvokM1VqI+iIW/UP4APt3FIMINuqZtjQwP34GX/uR0OH6Ki4nZCT3d1sTE+FvhLlas7A40u0CUiuyHuPF8PHdjYkLCdjDdvSJfidvl+gmRwTuRHfFTREau9+TkzJ0zZ066xfwl8ruzUR+ivt1NUZUrUD004Pmw+v8R9T+usLAfRXZJ8d7oqKjvwtzul9xhYWdjkUUpbdZEpYlbgUjmxS63ewsm2w8O0EXD/SnJyT7a4H83KaDjtMu8YTCrweU2tO3diPDwb8Pd7p8EXfD5p+SEhB2xcXEfx0VFPZgYFTWC2mQPSIlG4jD4nKycxRjn95MTE3eg7eI9PtrDde4Th67fRykMZHnTDljwviFuIAHxA6fh41p4dvwX129FdbqcXyckJbzVNS9vWe/evfv4BUHqM+ZHAi5L8MzV0yZP7i/bYHN3eQYdnD+Ov5O/Dy6PSYKx5GZn3z9l/Pje6nMqKioq01JSrg93uTaDjiIr6KdYkB+h3o/P1QrzTgQoLxHrDrRbQWtwGeq/iaafE/NayepyWrPTeTs5nWfNmhVVXlIyHWvnYYzjFqwvYVDdHhkR8TWiwd/G5zvgaTVGUaM4rYBTPIpoR1ZGxv2gcx+1/2VlZeVpSUnX4Z2bUI/2H3NLxBRIWml8jPpXVdWLMcTaeQv3fS3nRBIC2xKjo9fVVFXNhEE6xi6w2V1zVX2rSjOyMq5z6s6PVAYucjxhrhbTz6Kx1u8yY85Yr9+B3o/17dt3ZhzWKP5+xwzcKUDvEy5hqICAOXWTcn8k6vttqK+8LBtuC2vbdlTBB+tILWZZCzLepMPPii+z0tLOA4Pa7w+5wFT29ejRY54EDKuDA8YfjwFYjN34Twb6NMN35eXkfAPm/eujOw08yw8YPWmEtBjMcniejNVY6mGTdmyFtNBbPpdoc4mVJF7hmGRor8KA/NOlHCdtgTmvwbu9VukSFR7+fHVlZZXF3ZcuP/SsrDzX7XbttrJDwU1iwUeg/ss2WFDfKkpLh8dGRX2kPregq+cHT17OF5jE+9T/5+bk/AXXhDb6pNPuLAeXXd2KilqnTplypkUa8Hmymt77B27Y4+8D4/OI33pyc1snT5gwQzLmpLi469vyUEmIi7tZAYt8gFsTuVya7CjNK+55QG0n3wD0rKgYnpqa8g67bz/mz27+fADHpu4lJf0sGDR1CibLFvdlZ2W1nnzSSaeRNBCJzdEf2mp7YmzsrUyt1KVPSUkS6Pag+ru4mNi9hQUFXyQnJuxgfObfSEMxJEiAoavPhFrJjUsjAP0m0PpdrMf/kbF0qA8rO/bDqGL+N1NW2ia+VpEp9nzZZukKLU7Uk8waO/yTTNJ+eOk53/okHyoIlJtE3/vejSrf22zCwz7BBvw+AN4sdoKeLTo2KoadJiXj4loswCuwo+Q6PC+rLUd9cpE9MTstbb7fBtB3hXl5VfGQGHiGR+Fmhnr0HeKzREp1IHKzstaD2HHsfUZM4BG65yARV3zeVti169nJSUn75OEhJv1rlv3DgO4ZOHCgz8hUWlR0p1zgBBiGbZb3QvpoheFokhET46BXXFg4HrvSHyXTMKK9xugi2kCAsQfXoew9pqqLqIiI20TktUIDM9o3Kbusp5W8NF4rYCH/HtCv31QnUlV0wRjER0dvLe3W7fS03Nz8sWPHZk0dNSoFasccbDoGJMbF3Q3gkDn/38A1w2QjopHfeaZvTPPzW6dOnjwrQLCQp45dzOaUEbMU4LQbYCbAwjeukLTXSm+SmMjIDZD4LqytqZlb3bPnbPz/94X5XV8cPXponuJIEI1dpQCa03HPXHFFPVnQl9rxDha3uHceq3NQf431KZm7ztsHEPtNmNPpGxvklvrS5XReAzVmPVQh4v3ZYFYFWD8NeP5i5Pb6kVwlmyvLy0/hgGFEbxzAI8ZjZ1ZmZuu0iROn+fofG/uQD8Qcjha88xnR/xr0H++aLYauID//X42NjYXqfMB3cZEu1+viPkj1B0q6dbsd66xnBebBrEmTUvF9BlQ8pcX5+QvBXz5UoopP8wdsHWm/I81COGkiLmXGcH92AQHO5epc69+/fwwM1RsRiV0u36EAUBOXLoj/rFZtIVgf6wxAQcy/fRifrzGe/xB2EmTTHYQ2M4AIzEFplMlO5qBETolcxGh91UBckvc3A7kG+hOPSRzdRfeqRCfGRH/LNuDd8n9gSj4mLA0zwv2NLxr2/kcNBmG/2kdFp2fcPwq6waJ7NCE29n9Yqgsvu7fZhDY7kYumG2PYvK0TUQ+p7xSVAJTTRTBotU+H6be7KoqLe3Oa8HdBpXidvI8FBXl16o8EKwlYrG+8LRwsuJtidnRkpC8PDxjdqqvIFsKLskDrcfExiCi3+0U8O9wsihnMIwuXrwRYTJsyZXaAYPEXZogMawMscoWaTjDLOaeeOjwzPf0Wn7trbOzLws7WTmYlx2WdnZtkf/M9ngXQBvjGBYx2w4QRI3Lauq8Bc7Kga9eNXWgX3K2goK2cU7oCFnvT09JaoR0YCXXUdSQ5bYa0NNAqzRFLci/19UOkx65p6waxztOxO6Zo5laoeAYr/Q5G0bjTxLBhw3LBjHdazMkkc0FtkLST6wI0qqd1sEa+DOCRhzX0o4E6yiv/LioqGiN/D61IKcbhDWyUns2BBA5QEHEYp0ECqhMaGyPea2K4DhwsuFHEHzFklYyrKD//NQPjmTC1O0hfXQqd+065mzXKR6JBwsGE34Nn7pHvkUyTM3BPXu4LzFbCmcA69nwzn2m/qgBIWocxWVqUQbR2L0k0ifHxfzbYtTrIEDYFO71D/LliDFgI/m5hu8Du7cizpSQgvifAgMT0BhnYNVH5GNT36zdRo3YaALOXdLSiv60a6xfzC7cCFmFkmLuIVB7vjSAbUDW+kzsc5h7spkVVHIustsT4liv0MgeLSZMCBQvJuC71BxakEjgAZrd/3JgxVwuALy8rexnzkDYupt5LmoHEJasEw9ukNxTRIYw/j3sbSYnLk51dC7uEb+5Avfg41uFRtYfSBl2hcxjdHwlbwRPiPkib28s9nnSlv2ZgsR82m4OTxo0T/T9QVlz8Gp4T77f/9Iz8rKxu2JT4znqAfWo83edG1fmcqFbGoqAg/yHSZHwNlVRaJwWNOSkn0yKsQatuq00UL3MBY9ZdwENWSUkA6+Jk+RKovBZiHRk9v5mC/t6WB2BJiYfUZUZFN/NYCwZYyCCN77Fbug8oeBX0XpdjN7EMHfrCiAGT2kYM/mA5ibkuFd9LMfsQJwZ+9JlT1y/BqhkCdUQviF69RBuBnjeDQR5Qd+rqMzB5L5cDYgMsvBJ8IIp/UFRQcCd2LJdAD7sUIvGnbRmQ1OehvTuA6sJofgl2B9dD/NtM7fTyeyJg+KquqChgE9xBJ3QtZLT3yr6C+f8HuwfRxyGoPSrLymoxUU7Kycpa4QDgMv1mEwUAzTGgiSaMmbnZOVsMaNkigQ39WVdeWnoZJucM2IjOSoHHBsZ8G40R/c4yWDjICP1XSqR2pTUVAon9KSliMW3tVVl5Cp3joAULLGxKFvsgWRwqKyk5KAyUSibRsI4IyqM4izCLC10XtIHTwz/FvTjz4h2MQbQVQ79MJTK+oSEN60DO+2sYbThYZArJKi019XD3igqxLrdBzZpsqf/UT9gMffENYbq+BfMtUY6nv3GCBBKPdfpCQnT0negbuy9oRa7RjRZtSl7iS/uhYi1SxwGquRw4SHyl/PZzrAk5dwSQbDThV4cpX9jvGJ01BfidZuqlYIJFk4bFn5KU9IhPH0xFPZ82JTX1GYN7m0hMvI4tACcN9GyuYvExGgAMgUiyWUORlrdXZlraN0zC8F2F91IPeGFIercFFly0i0tIuHEYdKfqu3rBZQ/eUP/wB6TC46EX9MAGu8VbDO49TGmGT5c04Yuw25GjErdJGhFzvZcWhWFJT0qa7Twi7XjZjuZxI1CC5DeZH7gi6YmNwHfYrY0wek8JDJFdYFQlwGi2CBaaEouznvp/jmWmSkbHwR4PU0EdW7BAyRVegZKGWCMz2D0dChZWXGQxPuPgReSbA1B3TlWAwG+RbpxYX7+Rdo758+cbMWJdSTmxV/Y/PSVlto3+h5FH1SxxL7QMG6FTj7IYgKdTqowYce1MoEDpgbrHjgoKNHpStlvOR2yA58lNneQJkFTvUjwg+wAwDvP3SFpjjH8Qp+wx78zgFw4W6mfol59VJyRNPFFdMhc8Ovyp7Ii6U83MyPDdq4qTQi0CZH5F/b32/+/aKrM1wrfaLcVWrpYASg8WRnhidl5VlZUHrwXZVgs2C9nf29VJTNVN6rJ87CB3G+nnCRBfUumj0kZ4OUBX+QabOL73g6FxdQrfcaViF+4DuESaRFQUVQJNPlp4xIi5Wuk7iLjJagAWHbhyn9oeaXdxhTkPIX5jCOuPk6pLNgLS0Qs0dk0WJQuddLLrjqgN8x5UDHW6XWZ6vICFZBxw7Xxf7HZle44BWDiIvqtIffgKNlYRttpDv8NGIRuX3bBntcLldoo6lhwsZGZcuFp/COO5nRxWTlLlzTgyx+P3DB06tEDOCWttDT6j5O2FpP5bG/ENTbSJmMnnLoDgecaDWsTagYpLrj3hmn0tz1irvhs84gE2dzsdLLgevkHZdfDiIs+A8yRxVLCA/vPts846K1qdbJgQfYTHBUdLQSh4Jk21MFlctChWSMIx4PlIiYfQ2pAsvMTsvwGKJ3Kiq4wGO6wnDOjToqFGh4cPNWmzi3YIl8v3qs+IjIp6ip6vmU1M8oqaR0FNGj3TwasENhj+fsv7KLychLsvW/CpqB9IuqlgCyOnBE5XW7vPPlVVQ6ES9NowcIdRtOrvFeeJUazPfrwz2P+PD8liF0ljt7Ox6wyw4HSJR91CEuXldqQcPsZJCQk+jQGM9kxdyCQLOGz4bFCJiSts9l9Xduq7yRPrTjYeNuZD8AuM626sZxZb5Pf8ie+hXitktKlQN3SMt7ynaFaiwJ+MYyno/dhQDpf06nywIEZIouFHmDjpbQyMg3S3tdLdVdwPhuG7v7hbty/POfPMAgY2FxoxbaELJ6nC2sHqHs8g3MMJKN7tRfCK4gVkAhZ0BcO7kxObvws2mivkPaokBKPrOxTZrJkZ1mMcjgk66MKlqKT4+BemEk14sbHj5oE9A/luBWMi9JvD2ILvqYyzyvD3o9YyepgudBynaieCW6O5kiVPJQNddsLWc5FwmTXz2uA0ON7AQjI6zJEF7PedCRY6qZu6g/5yLKcpNh+nv8okag3u16uoX/cp9OH9z5D9Rz1Htt9uX+E2K2ku1CurSwsL+xr1kcbScawSZ8LluMpfwj+++4dU/6iahobigW4x0HK0kH1juwdFSUM+hr7n720isLiW0b1zJQvJhMFkXhIncXF05141EFursbP6iYh01GAMP/nttT17FknmR4S614jxpiUnP+zPo4FFfcbBh/x77ufvxE5a+O1LAhqCBe2kyVvkt0bEZv871wBo0OaU9f7iOyAqDlXdgp3kRdWnpubzRYgPMXEDdUjHABHVDvCb7IFBGGLpChFta1CXk/fMakkPxsSHqGMASeUUDirk3rdVBFVZVXmg3MCCiAzBgt+HZGiTwpUAzPj4uG2QgB4QOm8RSS8wxWAc9OMULGQ/JsnndDZYHA1sq66eSrrvnTNmzCgKtAEUzX8HObY87Acs9kpwst9/WsuNjYllpaUviueQ7fEA1s1G2EovhW2rDzaaiYYePp2fCfgaG15QzRQLMkfOIxlbAQlqi4F00kSR1bcpv3eSI8Eq5ZkcjO49dmChMkVNe5w1xHCSnn766dWV3bvvJ7WHV3rKIOBqe7/y8qMimPDUwOBvlICkvgsBaMwYbslCtonvbOm6UHkWt1kcVokNb6uTOVPhkyQjNfV8I1XSiGENEiwcZkBa06vXMEhohzlYYGGrYKFxoHRHRo6PhaoKXxwQExTVSvZIswRkqqpMeHMsMOqPBj2qpQVP3/fo0V1RP/oFC35eQyU2DmvlzpQtms0ZaWlLIfKfAiNmJnuvTbAIfpwF1K2tQ+rrp7D3daZk4aDgyuk0D/aQJ9NFqJegXmyjXirUylBHbqb3r+a053EWsNe0NgwefDIbc1u79gsR+Q4nDiHBf2Qwtz+Do8kD4iCh5Li4ap6VtjMy5k69aqorJjbmdTteUCRBx8t5RHN0sqHNgzaw+L6WAyLU6kMpaNZrABbPM7vwsQEL1PVWwGL27NnV3Q3AIiEmZnu/qio1WlNH3Sh3o2pcBoyDN9oAC52uT/N2ixOvaqqrl9PCNQYLuofaMc6fZAGp5zwj5jpu3NgOAwvpXrxkyZL4vNzsR/guRQmMO9RGPWwFLFDOMOpPrx49NtoBi5EjRkivNstgwWmN4M0KFJG25TFITl9jVyk95KRaYhs2EvenJSR0P14M3Bws4FQhggA7HSx41P+oxsZroeP2jbvIFgC7g0iNIRwkbFVkVBCeUMJo3YqI9L/5A4tsBCWeMnVqoGDBtQnJMJRPTIiNvgObhfcjoqKk5CLTZ+8C3V+CeuxUdn+wigygGy5ijiyqoZpp7qoqPJ0cS9aYqaAwdpvVcZb3IAShGP1uNgn2EzEXBEi/ELCgcxGcsPa/KI3gKqNB0jhLujdGlOd4u0W6kaqePa9VwYKroVQDPto7IVCwgOG4w8BCTozYiKj7HLpDTb3iZX20Xs3B4kyj/oDp2ZIshjc0nG0fLMxVCchKmljWrVt9VEzMBRnp6YJJfafspPbBaDtbjsvxJFmIdB9TJk7sdLDgc23QoEEX0w50R+/Kyvlitz+wf/+ZAwcMmGG74hz1QXV1Z8Kjqo4zIg4WiNwW6VUCBwt6Pqe1UIGDnsIYPBNq/OVY2++oUjQyKaxVVJZaUAPxUtNvlLt/q15QMGxPka7LlIzVg/Wxz0iioMjsBfKdTOWeiHH91kjFjDH4NguBjQw0T3CwoImAQV/HGEwTRfTeYUfnKYLKUD41UUOd3ZYaiv12vG2woOvYsR0FFpQny+MZLNvIdy8wBgtXud3os3DFfRl1E+orVDeBnv+CrprlajIGi5Kiol8ZgQU91zJYIBjwUutqKPPzC9RoVLUgWrtrj+7dFyE7qs/jiOxgUyXNTMGioMBWug/2vHtI4rz0BAALDnSCYQvJ4IeJEyd6OvpEOVOwyMwMCCwsnM3CS1z/vn1HIQjwZWVtPELvDBpggOlH+DK8apa8oFrIs+tzeP5lsk3aInW9MW/EbzwoJkzfje/fZ2vaS/z2MG5jTiknPli4KfHVjewdLbTDf82O58u4ESNKMCAHOHMUOysEtY2WxDOKszhewQIeUktNAgdb3Q59lT8GAOMZ04cag0UdAu6MDNwYmy+gH822GkUL5rwWFwsGbvt5eFSa1kBHCcb0Cdm2vlHSO2hGYIEYjtbxY8ZYBwu6/6mnnnJXV1U9QyoEW5LFtOMALGAIbiQaNSG300QZryTaFGiVfQ0WWFgNyOR0JVD3kgp7Nvm6d3gGWso6MJTshrSO/UsVqCulVEFqZhdsdG8YZEyQGYfvM5OaBU0pWNnQyA0VGVOln+BgIUUxkdES7qocoYW+7nBDfX2lBXEqjIJ5zpaeCQww9sDbJtOP66y3nWDR3MFg4SCOxeI5KCYjMvwZNoH5UZNuunKvCQ4WkvF1Q92tsbQlAmiRIHG634yjdBYF1AJf2ck6CwcHu6cG6rJviMEZ4dD1Q5RDaAqju3yeDyxc0C0nxcbOsqvWnDlzZixUcZto8V5+woAFtWdATU2+cIgQYwCp7/wgMJBggAXp8+2rqsIoU22k2/1YkNQwDnJfPV3JytzCTtAzyjMn1ENj1Q0aVKiDCSS4zcMr7Ez5ygbXqA0Y07vlOuXqK2wSFv6iwEL+dtKkSUmarn/bxYBRZaWn38MSifESRudfhCN/zbsSpdUrzpeQbdb9RHAfP2BBbXVwGwwZ4dOTky8wDIyk/9EOsszpDDvoRw3loLGIACj4RHmNARO2MJuV3EthDJh0maupwOO5iDzm/Edwtz+gSl+2bJkIiPqPeA/E7ouNwEJIRTDKbvdFt8bFLQjABpaC+jFlXr3iBAILjZI0RoIgL/n876Oj/0qLNGAbQieBhX0jNY0J7JzX0I7/vaB5QqEsWrQoDjmvrsAG9RWecVlmZWbr7guRk4tlZFhuZtiG4XyLkDxM5wOdjKfcz2MtbvrFqKH4Lr+4qGgJMxZ5RSUjz1xGKFnDlFn1R13XDFN35OflTTZJ97HueFVDyfthsFtvNA6ISbhBgqiiHpBVnrLF3PpMwILuQRrtK4wmn1h82EkhaMi8DB8yZFBSQvzPMq7GUm4oFDgE5GFO3AjJr6eNA7I0UUXmVCTrk3rb33GwkD7sAMFX6ICcZTbAQroo9lXcj688UcBCbWdqevrVMuARu9WiAHfcuqxBAgvN10Ecs4oEkfOrevc+zw6zk+9AMOdlsGdR1HOQCx2EBFWnyEBxqWDwkAha2Nr5mWVGdlI+u0TFWcNroLLyG+sFVdN8bmBXnHQe+aUZuHVVXI6PjdnFspd6ZY56pK24ypewjhXkri/FznG1yghVAsZERTyv7Io1DhYngM3idhM10o9ZWWkNJhyiLxj3WxwoOFhwX2xM+K4ybz5PO0D2i7/hN/X4nPjm+vWR4iB6cZwkAucWQc0jD4uylkiQxj05LuEp8uBYpUiQfgvpfCNhuP1Y3A9XzXMM6O4kcf12Opnwv7DxWHUpdJFb41Kak60Cy04wsNApvXU55pgvdgWHCN1rl84Gc1nrcLCgd4gMwmK+oL3fg/ZplgGDxgS84nqah68G2SNKMzrLnDIjLMH7t6rrADFEIyXdaa3NVcMFWOaKveK0TrO+y/kE/jqhjQzRb7LbTnywYAnEFviIRYyeh7TjzOvP4HcvGPxiEPQ26CU34H+7VIKxz00whLId64kFFvCemKCAH/OucItdy18rKypuGT929FJ4hNyKxfosaH1I9scCWDhYX5fy7L8Gnmof4nSzLalJSVuxOP9XpTuq1fMsHOTdNCbMFeb7LU6PO5WrHHlRF1xJYeE4ROz7+oe8VPXKnOIBlFPkyY4pKUkSVNxtMD03ta0fJLsDsTjX3DcvTyDJgt8b4XTeIhkSAGO2Aohm9iKNxkpmZBgBF/fXADxlsr/BkCxQYuFI4FMnFxUWPkEbPbJvtmnM11auXBmGzYPYJInN5XKrQBMMTy2Riw4AUQ0D9p/gxrpYyfLrpHnyhIG9QSYZfNSs7ez//XDPQamFYSnQvxfHszLanthgwQmANL23q0eSckLyyr+TTBXGo1a8fyZfrBwsjlvXWaINpeB+nTNwlTZgmOLcDfG9ShOf+siCGkpnuYTiIE5/agQYbRjxmttzUl5WVsZKEp13IhhvmsFYObk3FJ6XDHB8h9InbGan5XEJxBXhcr15hNb6wbS05LH8N/w+iPg9kOdrP5jOU/Ck+hPt+E6MOAveN7JjYbf6Fq3Bg7AvzbHqnpqXmTkfc0y6Km+yABZ7bIIFj+Rv1I8cAyo80O4hN1Uei+PkQOfEEbE6HcoFW+eQzk6mx8Y6Be2eD+mou9E56Fhjm82OcUC/T/bTdp28SAtw4Q4lvor3HkCOulxyIukksCBPoQDBYp8BWHzFwYIv8oqyMnFwj8L8+dGqVCnJITti1cc0PTk50xnBrYBFUyeCxSF/QXly4iHl+2AYrLwGDNzL6SF296oUkgI1FmwR8nAirylY0EQnr6Y67G5+lu/jnhqUEbhFJonUpKsfAKq0uHgbs1scAYuBA03BAgUkcDwk34G+3pMO1Yk8BY0b77ERGISPb9PvD0ACGGpEc3awTB0WqGzTPkQyXyvcjoUbqQgM3Ygq1h+CrosAEBfSmfN7KWnmHQHEWRyzCG6z+S7SpGiUWZj8/leDJph21ZF0eqKkmQuXaARDjhJHCgAofL8HOH8MevULBljwvsLOdKoST/Q2NAoT582bF8N/fNWR92aKMZLrGCC/pLOBgoomJRwYn5+mDca3kMg8XHUl8j3J+UtrSq7r//rJsM1Bx2ePkxKGuNJzNnZmBPdIudgZWDwRDMmC/40I0F+BED+yHXGT2DFjAESV53/z86dfrygpqVMnSzDAIjU5+XwTsHjCAlg02MgNJY+/nCgOcmLGs8M6p4UqbUREXCi8nDBZFS8xY7Dg/UPSuFqcT/EJkx58tBfvMqI9FvhMt64vMArKazCQLLhkgxTzi910RrSoaMjmivLyu6traq7Aj84HKAhXQZ80QQfx7IY31BgLjMFJqoHTmU/6HqTA2CzUHcgHtl7MHczVQ/i/iEvYjl1tJdk8HmTHqjrbAIscCRYnTZo0uYPBYokCFk4GFpYAox6p7XGELz/Q/2N4kz2G6+2Y13dBxSii9r9UJVWk/FiDZJOZal/9gQXAclqA3lBOOoxrDLIYfybbEBHu/iIjLfWhAXV1i7Fr/w3OkbgZGfg2yMSNJGXewZhqZxaNJNlwpB55W6Yuwrwt5fZBeFJl4SL7ptYploNgyf1WJm6UFer8g8iG27czAXOMkqL6Z7paBgu4stZUHpEsDmEBHgSFBOK1xEOy6NOjBwtFNzaoocN50Pctxc7iOxhQOVEpL4wuRK5mLPoP4PV0FiZ9uLr78wMWa+VOk/ooA/om+sscS+d1SPockvSBN8aT/sCid8+ewwEWB+m+nwEWPtrU9u69beHChUcNsPyd40eOLIYq4WFxEpu6kFWAAJ2E6mRD/9raIWSrCcdkfYGYo2jjQQ4WZotVHFwDdcwyjN8P0PmbJSlsAvN5Dd4dDT4lam3tuSTFHKD+HSCwOJUzDs5oiK61uDyEd+41TJSI/+F5O+Kjolb1xcH0NhaEkySTPjnZ2S/qDn0/fzbAQThT7IYd7BY6a1qWe1Atuc5it54nFi+BxdQOBovFUrpnkkUgh0RNBo2fAy33S9UNOzdfSB77EB3/HMZkAp+LbYBllug/gcX0gPtPNK6tqEjDmFyGttLGhVU6Xx6bjJfBK07i7TlWqcuLK4orcVkjjzDmqVEoB1oeaLsUNHsN0vTasrKyBptt1yljdO+s7Kz7kxKTRIDfCtiXyjqbBgmog1D7ow6gq0CxMiuHzogDjrAwffdisMVV1HrsBPvSaV2WJ/YYnLwnjvwUvA8G3xVA5Q3wjHochF4W43JePGzw4GEi1Qe714oxrZT3kf5u63QvjdKqZwt68HvHjWss93fvALi1kleRSp96BJb1ESoRf/SAXr8gAudyQ2JYDrfi9VANbMjOzr0b2TcvbBwypAdf2Ng1l4u2KeMwENWfV5BDPSo3MSbmDGyxbwHd1wFAnsY4/FlkJMUOqk7RiWo4DCaF0xTvHdSAc5z9zxt6J51HHO1yTUFOqN8l4pz1OLjXwkh7ocBMLLJU1k6rxaFEtvfG5VfYiV6H3dlinLN+DVKeTG9sbMwwoHkx6jDUHNYHM+N7HXa+mAvj2m1gZPfn08FjFYE+k2ciFbveeAQrwuh9FRjWzZjXNzt1/QIwsKk4S6aY3WeF+bhQ+2Wnpw/CxiAloP5z+lMcUDLUjzg586zc7OwbAA43QVV6tVBiFKSlVRj071gW3e5v7IKcVfD+5ZfAc9Q77RwXGej3x3AyOqjao1vw6W81Cltrb/Qu/12ANNTa8Wyt4wLZOv85fLys/vYY8wBn0NoZfN5lMXEmfW7/OqV3dr7+TefVbpStYQ18cjtVXa3MD0NusUHuY/DvtZnigNPCIfP2WHyfFiD9df63PbrYfyerxOQ6ZjFbfLZmsw+8z4GXwNsRfDp3fv9143bi/6ESKuYTHDVUjtIiRPtQCZVQCZVQCZVQ+b/24IAEAAAAQND/1+0IVAAAAAAAAAAAAAAAAAAAzgL4cn2g77WR0AAAAABJRU5ErkJggg==" />
                </td>
            </tr>
            <tr>
                <td style="padding:25px; background-color:#EDEDED;">
                    <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                        <tr>
                            <td style="padding:37px 47px 0;">
                                <p>
                                    Hello ' . $data[ 'lfname' ] . ',<br/>
                                    <b>Great news, your booking request is accepted!</b><br/><br/>
                                </p>
                                <p>
                                    Your spot at <b>"' . $this->getListingTitle( $data[ 'lposts_data' ] ) . '"</b> is now secured.<br/>
                                    We are holding your spot for 48 hours. You can now book and live
                                    your dream trip!
                                </p>
                                <p>
                                    You have until ' . date( 'l, F d, Y', strtotime( '+ 2 day', $data[ 'lcreated_date' ] ) ) . ' at 00:00 UTC to pay the deposit,
                                    after which the link will be expired. Don’t forget to check your timezone.
                                </p>
                                <p>
                                    As a reminder, payment methods availabe for your deposit are:
                                </p>
                                <ul style="padding-left:10px;">
                                    <li style="padding-left:20px;">Credit cards (Visa, Mastercard, American Express)</li>
                                    <li style="padding-left:20px;">Paypal</li>
                                </ul>
                                <br/>
                                <p>
                                    <a style="display:block; border:1px solid #FFC200; background-color:#FFC200; border-radius:3px; padding:10px; width:300px; text-align:center; margin:7px auto; color:#FFFFFF; font-weight:bold; text-decoration:none;"  href="' . $this->getPaymentUrl( $data[ 'lbooking_id' ] ) . '">BOOK MY TRIP NOW</a>
                                    <a style="display:block; border:1px solid #C1C1C1; border-radius:3px; padding:10px; width:300px; text-align:center; margin:7px auto; color:#C1C1C1; text-decoration:none;" href="' . HT_SERVER . SITE_URL . '/contact-us/">NEED HELP? CONTACT US</a>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:37px 47px 20px">
                                <strong style="display:block; border-bottom:1px solid #C1C1C1; padding-bottom:10px; margin-bottom:15px; color:#2FAC66;">AVAILABLE</strong>
                                <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                                    <tr>
                                        <td width="130" style="vertical-align:top;"><img src="' . $this->get_attachment_url( $data[ 'lposts_data' ][ 'accommodation_image' ], 115, 115 ) . '" /></td>
                                        <td style="vertical-align:top;">
                                            <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                                                <tr>
                                                    <th colspan="2" style="text-align:left; text-transform:uppercase;">' . $this->getListingTitle( $data[ 'lposts_data' ] ) . '</th>
                                                <tr>
                                                <tr>
                                                    <td style="color:#C1C1C1; padding-left:0;">CUSTOMER</td>
                                                    <td>' . $data[ 'lfullname' ] . '</td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#C1C1C1; padding-left:0;">CHECK-IN</td>
                                                    <td>' . date( 'F d, Y', $data[ 'lcheck_in' ] ) . '</td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#C1C1C1; padding-left:0;">CHECK-OUT</td>
                                                    <td>' . date( 'F d, Y', $data[ 'lcheck_out' ] ) . '</td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#C1C1C1; padding-left:0;">NUMBER OF GUESTS</td>
                                                    <td>' . $this->getNumberOfGuest( $data ) . '</td>
                                                </tr>';

                                                if( isset( $data[ 'lrooms' ] ) && !empty( $data[ 'lrooms' ] ) )
                                                {
                                                    $content .= '
                                                    <tr>
                                                        <td style="color:#C1C1C1; padding-left:0;">' . $this->getRoomLabel( $data ) . '</td>
                                                        <td>' . $this->getNumberOfRooms( $data ) . '</td>
                                                    </tr>';
                                                }

                                                $content .= '
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:0 47px 37px;">
                                <table border="0" cellspacing="0" style="width:100%; font-family:Arial; font-size:13px;">
                                    <tr style="background:#C1C1C1; color:#FFFFFF; font-weight:700;">
                                        <td style="padding:10px 20px;">DEPOSIT</td>
                                        <td style="padding:10px 20px; text-align:right;">' . $this->get_format_price( $data[ 'ldeposit' ], $data[ 'lposts_data' ][ 'lpost_id' ] ) . '</td>
                                    </tr>
                                    <tr style="background:#EDEDED; font-weight:700;">
                                        <td style="padding:10px 20px;">PAY AT HOTEL</td>
                                        <td style="padding:10px 20px; text-align:right;">' . $this->get_format_price( $data[ 'lbalance' ], $data[ 'lposts_data' ][ 'lpost_id' ] ) . '</td>
                                    </tr>
                                    <tr style="background:#000000; color:#FFFFFF; font-weight:700;">
                                        <td style="padding:10px 20px;">TOTAL</td>
                                        <td style="padding:10px 20px; text-align:right;">' . $this->get_format_price( $data[ 'ltotal' ], $data[ 'lposts_data' ][ 'lpost_id' ] ) . '</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:0 47px 37px; text-align:center; color:#C1C1C1; line-height:2;">
                                <p style="border-top:1px solid #C1C1C1; padding-top:30px;">WE WISH YOU A GREAT TRIP,<br/>NOMAD SURFERS BOOKING TEAM</p>
                            <td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding:25px; background-color:#EDEDED;">
                    <table border="0" cellspacing="0" style="width:100%; font-family:Arial; font-size:13px;">
                        <tr>
                            <td style="text-align:center;">
                                <a href="mailto:' . $this->getSettingValue( 'email_booking' ) . '" style="color:#FFC200; text-decoration:underline;">Contact our Booking Manager</a><br/>
                                <p style="color:#C1C1C1; text-decoration:underline; margin-top:2px;">
                                    Manage your Bussiness on 
                                    <a style="color:#C1C1C1;" href="' . HT_SERVER . SITE_URL . '">NomadSurfers.com</a>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center; padding-top:20px;">
                                <a href="' . $this->getSettingValue( 'fb_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1Ni42OTMgNTYuNjkzIiBoZWlnaHQ9IjIwcHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1Ni42OTMgNTYuNjkzIiB3aWR0aD0iMjBweCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+PHBhdGggZmlsbD0iI0MxQzFDMSIgZD0iTTQwLjQzLDIxLjczOWgtNy42NDV2LTUuMDE0YzAtMS44ODMsMS4yNDgtMi4zMjIsMi4xMjctMi4zMjJjMC44NzcsMCw1LjM5NSwwLDUuMzk1LDBWNi4xMjVsLTcuNDMtMC4wMjkgIGMtOC4yNDgsMC0xMC4xMjUsNi4xNzQtMTAuMTI1LDEwLjEyNXY1LjUxOGgtNC43N3Y4LjUzaDQuNzdjMCwxMC45NDcsMCwyNC4xMzcsMCwyNC4xMzdoMTAuMDMzYzAsMCwwLTEzLjMyLDAtMjQuMTM3aDYuNzcgIEw0MC40MywyMS43Mzl6Ii8+PC9zdmc+" alt="Facebook" /></a>
                                <a href="' . $this->getSettingValue( 'ig_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDU2LjcgNTYuNyIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgNTYuNyA1Ni43IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnPg0KCTxwYXRoIGZpbGw9IiNDMUMxQzEiIGQ9Ik0yOC4yLDE2LjdjLTcsMC0xMi44LDUuNy0xMi44LDEyLjhzNS43LDEyLjgsMTIuOCwxMi44UzQxLDM2LjUsNDEsMjkuNVMzNS4yLDE2LjcsMjguMiwxNi43eiBNMjguMiwzNy43ICAgYy00LjUsMC04LjItMy43LTguMi04LjJzMy43LTguMiw4LjItOC4yczguMiwzLjcsOC4yLDguMlMzMi43LDM3LjcsMjguMiwzNy43eiIvPg0KCTxjaXJjbGUgZmlsbD0iI0MxQzFDMSIgY3g9IjQxLjUiIGN5PSIxNi40IiByPSIyLjkiLz4NCgk8cGF0aCBmaWxsPSIjQzFDMUMxIiBkPSJNNDksOC45Yy0yLjYtMi43LTYuMy00LjEtMTAuNS00LjFIMTcuOWMtOC43LDAtMTQuNSw1LjgtMTQuNSwxNC41djIwLjVjMCw0LjMsMS40LDgsNC4yLDEwLjdjMi43LDIuNiw2LjMsMy45LDEwLjQsMy45ICAgaDIwLjRjNC4zLDAsNy45LTEuNCwxMC41LTMuOWMyLjctMi42LDQuMS02LjMsNC4xLTEwLjZWMTkuM0M1MywxNS4xLDUxLjYsMTEuNSw0OSw4Ljl6IE00OC42LDM5LjljMCwzLjEtMS4xLDUuNi0yLjksNy4zICAgcy00LjMsMi42LTcuMywyLjZIMThjLTMsMC01LjUtMC45LTcuMy0yLjZDOC45LDQ1LjQsOCw0Mi45LDgsMzkuOFYxOS4zYzAtMywwLjktNS41LDIuNy03LjNjMS43LTEuNyw0LjMtMi42LDcuMy0yLjZoMjAuNiAgIGMzLDAsNS41LDAuOSw3LjMsMi43YzEuNywxLjgsMi43LDQuMywyLjcsNy4yVjM5LjlMNDguNiwzOS45eiIvPg0KPC9nPg0KPC9zdmc+" alt="Instagram" /></a>
                                <a href="' . $this->getSettingValue( 'yt_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjEwMCUiIHN0eWxlPSJmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6MjsiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHdpZHRoPSIxMDAlIiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnNlcmlmPSJodHRwOi8vd3d3LnNlcmlmLmNvbS8iIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48cGF0aCBmaWxsPSIjQzFDMUMxIiBkPSJNNTAxLjMwMywxMzIuNzY1Yy01Ljg4NywtMjIuMDMgLTIzLjIzNSwtMzkuMzc3IC00NS4yNjUsLTQ1LjI2NWMtMzkuOTMyLC0xMC43IC0yMDAuMDM4LC0xMC43IC0yMDAuMDM4LC0xMC43YzAsMCAtMTYwLjEwNywwIC0yMDAuMDM5LDEwLjdjLTIyLjAyNiw1Ljg4OCAtMzkuMzc3LDIzLjIzNSAtNDUuMjY0LDQ1LjI2NWMtMTAuNjk3LDM5LjkyOCAtMTAuNjk3LDEyMy4yMzggLTEwLjY5NywxMjMuMjM4YzAsMCAwLDgzLjMwOCAxMC42OTcsMTIzLjIzMmM1Ljg4NywyMi4wMyAyMy4yMzgsMzkuMzgyIDQ1LjI2NCw0NS4yNjljMzkuOTMyLDEwLjY5NiAyMDAuMDM5LDEwLjY5NiAyMDAuMDM5LDEwLjY5NmMwLDAgMTYwLjEwNiwwIDIwMC4wMzgsLTEwLjY5NmMyMi4wMywtNS44ODcgMzkuMzc4LC0yMy4yMzkgNDUuMjY1LC00NS4yNjljMTAuNjk2LC0zOS45MjQgMTAuNjk2LC0xMjMuMjMyIDEwLjY5NiwtMTIzLjIzMmMwLDAgMCwtODMuMzEgLTEwLjY5NiwtMTIzLjIzOFptLTI5Ni41MDYsMjAwLjAzOWwwLC0xNTMuNjAzbDEzMy4wMTksNzYuODAybC0xMzMuMDE5LDc2LjgwMVoiIHN0eWxlPSJmaWxsLXJ1bGU6bm9uemVybzsiLz48L3N2Zz4=" alt="Youtube" /></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>';

        return $content;
    }

    function bookingChangedDateMessage( $data, $param )
    {
        $content = '
        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
            <tr>
                <td style="padding:25px; background-color:#FFFFFF; text-align:center;">
                    <img style="width:250px;" alt="NomadSurfers" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAYsAAABXCAYAAAD1a2lrAAAuWklEQVR42uzdb2hVdRzH8XP/ue3u3uvdH9e2dP91azDm1BabKQtqOlFRmyVTiqT2INAI90iy7GE9uA/S8EHSIgh6EEVJIBJB9cCCKMyKkFAzliSR2B902z339rnjd+Jw2GX3Thfaeb/gy++c6+7dFPf7nu/vd87vZwEAAAAAAAAAAAAAAAAAAAAAANweAjMBAIBH0IRXYOZ1AIDvhSyXsbGx8lQqVWmJS9gCAPjTzp07Q5Zs3br1rs7OzoM6/HTRokWXEonErzo+E41GjzY3N3eTMADA5xVFOBzui0Qi53WYdUcgEJhpg8HgVF1d3bMkDADwaaKoqalZq+aaSQ6Tam1FxoStmDLHWSWVsX/fCwDwh9WrV0dKS0vPmkpiyrSzha1IK5lk6uvr+y1h0hsA/DOhvcckg2nT5g0lipmvSSaT71FdAICPkoWqhBPuRDBH2Lm2pKTk54aGhhaqCwD4fwsogocPHw5qCOpzkwjSBSSLTC5y8xqaDO+lugAAHyQLE6eLTRaKSQXJAgB8dCfUyWKHoRQTijaGoQDAPxPc+5Qo5roTKuueBNcDe+9TVQCAPwQsGRkZqQiFQhcLuCMq4yQUVSMPkSwAwGfVRTwe36IJaycpTCpsT5KYdhLFsmXLjpAoAMCnCUNrQu0uKyv73VtNuM+XL1/+SjabDbF8OQD4OGG0tbW1qspIaQ7jnNaCmtbwVC5ZXIzFYu8sXbp0E/tcAID/BEySCJrziMKR3LhxY/vQ0FBnX19fpTupkCgAwD+Cs3X6GmIK5Pt65igAwF8ilqFbYB/RkNOH5eXlZzVf8Y3Ov9SQ07FVq1ZtYcMjAPCngNPxd3d3363mA0U2X9TW1p4YHBxs5sE7APCPsGW0trY+qoUAf3E9V5FW2E64b5NVtXFBbYcnYQQUQSa6AeDOXesplK+a0Jap8bJo2avmae1Cnti+YZYBOa0FB73Jwfv5ESa/AeA2SwieyD8prXC2TFU18a2rmrDNcUHLe+gW2hHXz7BE0aFbbKvN3t2zToaTPADgv00OYRN5HThwoFrDS21NTU0bdJp0f4b2zT6Ye2bCW00UkywUxxWO5xW2Ko4zmhA/uXjx4mPaDOkZvTbY399fo9YrTOIAgFvFe1Xu0d7eHlfTpKSwVlf1T+j4ZcW7Wqbjq4qKigl13tcTicQXeniu0pLe3t5m/dnHriex0+Z4PsnidUvMKrXjeXbRm1QFc1nHHyk5HdJ2rQ9oY6Vq73MdTJYDwPyFFBHLZXR0NHd+r4aPntZ6TG+o4z+t89/m6NwPWdLT09MYjUZ/NJ34lGvZDttEUclCn/WaJZq7CCsxfeL63LSpVqad7+GJ7/XzH6murt48Pj5e6q42SBoAUGwlYaxbt65OQznDujI/rjinl/7Ms6dE2hMZdco39LR1v9kq9UnTod9wvS9tXitmOCpt2jFLpEXvv+De28IJTwUz7a5k9HexNVz1Q1VV1XNqW0kaWGgBd3Dr3sKPlw8MDIQV/EIv8O2sqhru0xDSEXXEl2a7unddxWfybUikIaCvdeXvfOZbTmfvaa9omOopdd5OhZKZa2c8fa7d0dGxwZKWlpYHzXts0xayWdKUaZ3hqmuRkshRHXfw4B9wp68X5MECcguzcJ867i5NEr/t3KY6y7MPmUKHirSX9guW7N27t17Jx50MMmbDop+6urpatm3b1qrjQqqKjLkT6oozcd3Q0LB/PhPlJmzPfhl/qNI4unLlyiZLzJ7gXJTg5j22fXvVnh076nS7Xu1MbNpUu2vXrvpUKlVm4VYIWsa+3bsTanrX9PQMD6xf/3g0ElnjrBtEwrgFCVlWrFgxquZvz8ZCdnEdsLnKV2jo6X5LGhsbH1blMNM5u7dL1YqyL1pSWVk5bIai7AK3UP3M+b+hu57edFcq84yMJ2lc1s++35UoIhZwM79ciVjsVCIevxovL5+Ix2KXy6PRiYpk8urmoaFhS5RAWOhs/oJmUjV6T3v7S/q3Pa/TvzQ0kq1ZsiTX4VzXcMR3em2QjW/+Ye86oKQqsjbvdU/35JxjM4GJDDDDMDAwpAGGHAUFRJK6rP6uGFbWrOuaRRBXBGVd18iCi4FVdA2sq4LZVTCsa0DF9KMkCcLM9PxfNbf469zz3vR7PdMDeLrOqfN6pvu9V3Wr6n51YwVYaI6SZ9OvQU/GOAOqzSQ1vAojcgy95k9S4lDBJCUlpV58iTxQCwksmq1ILGDkt9Fzw1Hf5faKjgINRIv/s7a2tozoFHK1DZV27XjfMpp0gwYNmhUCi/aD8YIFC1LBSN5UXSFxfRX1BdRd4iQ17FpHhcCifXQeMGBAgthNt2eHzhk6QP2P4tmwL6VDcviYxq9FPh/j+hZUUAnUjpvlvVae3bVrV9/6gvG8ECopVRJq7aDaoqi19qDO7BIqodJOsHiV7ZjE4SytWCAzQ2DRfv05dqbSKOoFg1g5ZsyYfHyO7uLxhKeHh3twvsEpGzduDBkiAyxSzTJ37twq2Bc6iuF6AeJeGJ59BxNBehitSA1eqYKCrWGZbAd28A9aAAvZtv1IKNhH3IfrNFW91ZGVtwe2jEvXrFnj6hIqoRIgWLzGROBmMXmhUw+BRftp2xN1t6ArGM89RjvikL2i/WBBBujeBBaBVm6A/kzkfSJ7xJXcAA3AaIbKq788rwIBfS9bkGqkh9VWSCXp1IVrVKYejCpdfZEmvXXIkCG1XUIlVDoQLEKSRQe4byLoa4FvsWInmZ+dXST+hwjcsNDhN8ctWHiFmpBiGJYqUd7nEtM9qIDBFnJ/7jJ9+vRklA/oN6YSgpRI0tLS/i6fjfs2qN91ZKX++AAO8SK7YCcZ1iVUQiUEFscfWOCwm6U+/bTH884keJ2FpIjgggVUQfbVOcSouUQAe0Wjsk6iUJ+RYEAM/w8KmBTj3d+wdWSqEtK76FfTrZl43ocdaNzmffKS++/LDQ0N3UJnaIRKCCyO38CwWwVN4Rr5d5FmIhRTETwDN861jgWD3GZHpUOJ+1SQ+Boqpy9hr3gYn+OZ00Es0n3Mw+783+K3yCVVJxuA8R0EtZWVGI4W3aG3pmelTxP3IcVHPS4H1biNDqjNSlbbJjhP3KQEFYZ1RFpePUiTWCNm46todBDeY61/du6ldgarzbJtQaSLeT+CDBYaq7/ISHc7YEGG0b/T/7RgRd13dv+PlznBGPpJCtM8ZMaAuW0BBuw1UBmeIjSHigFYJ+aqMf7xCpj8ewL8Jd8EgESCMW9q22ZBQXxu197hw4f7VJLIXHtqR9krBOip70Z7nkPMSQ13L+ZRorzqjEHwScsP3NCDleFRlkDzsnMma/Ac3ey5/Dub32t222v7/fSOoL+H3tExYME3BFTanxHT0U7mqtuZy4zuVp7jMPmfrG7631Ki6ZPkT++kar5GA6ez08iPvtP6z1VD5r9xyHYGSVIWwXE7mOqnhQOFpmutIk9UYUXhCBaz4TrnnHNi2bMlgNTS/ZepacLJPlVvpk5i/98id/gZaRl3tdNe4eX5omg9T5UND2gd8UlWVVFRGgFrfmZqat3Afv26k6jCftu+lAZzEBE8duTIfvjYV6QABgPqedFFF8XwXZj155sXaWySA16Sn98dH/sX5OXV4TsPD+Ixa7egA87IrUDK4P7wWqgbPXp0T+w2HNbba35YivScEGmOS46IsX3Li4v7Txg1qgzvDefj1Z68OKLNo/BcuPcJj426YQMH9hLBYez5AYMFpzm9MwKiego+JuK7ZFM6UAl43IN/v8b7Tf1KO++88+xEtl9HNH20ne10MmbsqqysTBV0xlxNxZzS2/p98PtvPsbjxo1LFu1EpHPKypUrI/n8CYaEgZ1/Li53ou4zMPYeIgb9dnl5ea68ETaIBtQVUNs8j9iJzdDz/wV/nyEyzcrfwKPtKnFvaWlphQpQeI6LvKaWKgBlJsksl8+DRHJlAGDRwm0sJB39E3aTGXIecb7GS3fUB1DvokM1VqI+iIW/UP4APt3FIMINuqZtjQwP34GX/uR0OH6Ki4nZCT3d1sTE+FvhLlas7A40u0CUiuyHuPF8PHdjYkLCdjDdvSJfidvl+gmRwTuRHfFTREau9+TkzJ0zZ066xfwl8ruzUR+ivt1NUZUrUD004Pmw+v8R9T+usLAfRXZJ8d7oqKjvwtzul9xhYWdjkUUpbdZEpYlbgUjmxS63ewsm2w8O0EXD/SnJyT7a4H83KaDjtMu8YTCrweU2tO3diPDwb8Pd7p8EXfD5p+SEhB2xcXEfx0VFPZgYFTWC2mQPSIlG4jD4nKycxRjn95MTE3eg7eI9PtrDde4Th67fRykMZHnTDljwviFuIAHxA6fh41p4dvwX129FdbqcXyckJbzVNS9vWe/evfv4BUHqM+ZHAi5L8MzV0yZP7i/bYHN3eQYdnD+Ov5O/Dy6PSYKx5GZn3z9l/Pje6nMqKioq01JSrg93uTaDjiIr6KdYkB+h3o/P1QrzTgQoLxHrDrRbQWtwGeq/iaafE/NayepyWrPTeTs5nWfNmhVVXlIyHWvnYYzjFqwvYVDdHhkR8TWiwd/G5zvgaTVGUaM4rYBTPIpoR1ZGxv2gcx+1/2VlZeVpSUnX4Z2bUI/2H3NLxBRIWml8jPpXVdWLMcTaeQv3fS3nRBIC2xKjo9fVVFXNhEE6xi6w2V1zVX2rSjOyMq5z6s6PVAYucjxhrhbTz6Kx1u8yY85Yr9+B3o/17dt3ZhzWKP5+xwzcKUDvEy5hqICAOXWTcn8k6vttqK+8LBtuC2vbdlTBB+tILWZZCzLepMPPii+z0tLOA4Pa7w+5wFT29ejRY54EDKuDA8YfjwFYjN34Twb6NMN35eXkfAPm/eujOw08yw8YPWmEtBjMcniejNVY6mGTdmyFtNBbPpdoc4mVJF7hmGRor8KA/NOlHCdtgTmvwbu9VukSFR7+fHVlZZXF3ZcuP/SsrDzX7XbttrJDwU1iwUeg/ss2WFDfKkpLh8dGRX2kPregq+cHT17OF5jE+9T/5+bk/AXXhDb6pNPuLAeXXd2KilqnTplypkUa8Hmymt77B27Y4+8D4/OI33pyc1snT5gwQzLmpLi469vyUEmIi7tZAYt8gFsTuVya7CjNK+55QG0n3wD0rKgYnpqa8g67bz/mz27+fADHpu4lJf0sGDR1CibLFvdlZ2W1nnzSSaeRNBCJzdEf2mp7YmzsrUyt1KVPSUkS6Pag+ru4mNi9hQUFXyQnJuxgfObfSEMxJEiAoavPhFrJjUsjAP0m0PpdrMf/kbF0qA8rO/bDqGL+N1NW2ia+VpEp9nzZZukKLU7Uk8waO/yTTNJ+eOk53/okHyoIlJtE3/vejSrf22zCwz7BBvw+AN4sdoKeLTo2KoadJiXj4loswCuwo+Q6PC+rLUd9cpE9MTstbb7fBtB3hXl5VfGQGHiGR+Fmhnr0HeKzREp1IHKzstaD2HHsfUZM4BG65yARV3zeVti169nJSUn75OEhJv1rlv3DgO4ZOHCgz8hUWlR0p1zgBBiGbZb3QvpoheFokhET46BXXFg4HrvSHyXTMKK9xugi2kCAsQfXoew9pqqLqIiI20TktUIDM9o3Kbusp5W8NF4rYCH/HtCv31QnUlV0wRjER0dvLe3W7fS03Nz8sWPHZk0dNSoFasccbDoGJMbF3Q3gkDn/38A1w2QjopHfeaZvTPPzW6dOnjwrQLCQp45dzOaUEbMU4LQbYCbAwjeukLTXSm+SmMjIDZD4LqytqZlb3bPnbPz/94X5XV8cPXponuJIEI1dpQCa03HPXHFFPVnQl9rxDha3uHceq3NQf431KZm7ztsHEPtNmNPpGxvklvrS5XReAzVmPVQh4v3ZYFYFWD8NeP5i5Pb6kVwlmyvLy0/hgGFEbxzAI8ZjZ1ZmZuu0iROn+fofG/uQD8Qcjha88xnR/xr0H++aLYauID//X42NjYXqfMB3cZEu1+viPkj1B0q6dbsd66xnBebBrEmTUvF9BlQ8pcX5+QvBXz5UoopP8wdsHWm/I81COGkiLmXGcH92AQHO5epc69+/fwwM1RsRiV0u36EAUBOXLoj/rFZtIVgf6wxAQcy/fRifrzGe/xB2EmTTHYQ2M4AIzEFplMlO5qBETolcxGh91UBckvc3A7kG+hOPSRzdRfeqRCfGRH/LNuDd8n9gSj4mLA0zwv2NLxr2/kcNBmG/2kdFp2fcPwq6waJ7NCE29n9Yqgsvu7fZhDY7kYumG2PYvK0TUQ+p7xSVAJTTRTBotU+H6be7KoqLe3Oa8HdBpXidvI8FBXl16o8EKwlYrG+8LRwsuJtidnRkpC8PDxjdqqvIFsKLskDrcfExiCi3+0U8O9wsihnMIwuXrwRYTJsyZXaAYPEXZogMawMscoWaTjDLOaeeOjwzPf0Wn7trbOzLws7WTmYlx2WdnZtkf/M9ngXQBvjGBYx2w4QRI3Lauq8Bc7Kga9eNXWgX3K2goK2cU7oCFnvT09JaoR0YCXXUdSQ5bYa0NNAqzRFLci/19UOkx65p6waxztOxO6Zo5laoeAYr/Q5G0bjTxLBhw3LBjHdazMkkc0FtkLST6wI0qqd1sEa+DOCRhzX0o4E6yiv/LioqGiN/D61IKcbhDWyUns2BBA5QEHEYp0ECqhMaGyPea2K4DhwsuFHEHzFklYyrKD//NQPjmTC1O0hfXQqd+065mzXKR6JBwsGE34Nn7pHvkUyTM3BPXu4LzFbCmcA69nwzn2m/qgBIWocxWVqUQbR2L0k0ifHxfzbYtTrIEDYFO71D/LliDFgI/m5hu8Du7cizpSQgvifAgMT0BhnYNVH5GNT36zdRo3YaALOXdLSiv60a6xfzC7cCFmFkmLuIVB7vjSAbUDW+kzsc5h7spkVVHIustsT4liv0MgeLSZMCBQvJuC71BxakEjgAZrd/3JgxVwuALy8rexnzkDYupt5LmoHEJasEw9ukNxTRIYw/j3sbSYnLk51dC7uEb+5Avfg41uFRtYfSBl2hcxjdHwlbwRPiPkib28s9nnSlv2ZgsR82m4OTxo0T/T9QVlz8Gp4T77f/9Iz8rKxu2JT4znqAfWo83edG1fmcqFbGoqAg/yHSZHwNlVRaJwWNOSkn0yKsQatuq00UL3MBY9ZdwENWSUkA6+Jk+RKovBZiHRk9v5mC/t6WB2BJiYfUZUZFN/NYCwZYyCCN77Fbug8oeBX0XpdjN7EMHfrCiAGT2kYM/mA5ibkuFd9LMfsQJwZ+9JlT1y/BqhkCdUQviF69RBuBnjeDQR5Qd+rqMzB5L5cDYgMsvBJ8IIp/UFRQcCd2LJdAD7sUIvGnbRmQ1OehvTuA6sJofgl2B9dD/NtM7fTyeyJg+KquqChgE9xBJ3QtZLT3yr6C+f8HuwfRxyGoPSrLymoxUU7Kycpa4QDgMv1mEwUAzTGgiSaMmbnZOVsMaNkigQ39WVdeWnoZJucM2IjOSoHHBsZ8G40R/c4yWDjICP1XSqR2pTUVAon9KSliMW3tVVl5Cp3joAULLGxKFvsgWRwqKyk5KAyUSibRsI4IyqM4izCLC10XtIHTwz/FvTjz4h2MQbQVQ79MJTK+oSEN60DO+2sYbThYZArJKi019XD3igqxLrdBzZpsqf/UT9gMffENYbq+BfMtUY6nv3GCBBKPdfpCQnT0negbuy9oRa7RjRZtSl7iS/uhYi1SxwGquRw4SHyl/PZzrAk5dwSQbDThV4cpX9jvGJ01BfidZuqlYIJFk4bFn5KU9IhPH0xFPZ82JTX1GYN7m0hMvI4tACcN9GyuYvExGgAMgUiyWUORlrdXZlraN0zC8F2F91IPeGFIercFFly0i0tIuHEYdKfqu3rBZQ/eUP/wB6TC46EX9MAGu8VbDO49TGmGT5c04Yuw25GjErdJGhFzvZcWhWFJT0qa7Twi7XjZjuZxI1CC5DeZH7gi6YmNwHfYrY0wek8JDJFdYFQlwGi2CBaaEouznvp/jmWmSkbHwR4PU0EdW7BAyRVegZKGWCMz2D0dChZWXGQxPuPgReSbA1B3TlWAwG+RbpxYX7+Rdo758+cbMWJdSTmxV/Y/PSVlto3+h5FH1SxxL7QMG6FTj7IYgKdTqowYce1MoEDpgbrHjgoKNHpStlvOR2yA58lNneQJkFTvUjwg+wAwDvP3SFpjjH8Qp+wx78zgFw4W6mfol59VJyRNPFFdMhc8Ovyp7Ii6U83MyPDdq4qTQi0CZH5F/b32/+/aKrM1wrfaLcVWrpYASg8WRnhidl5VlZUHrwXZVgs2C9nf29VJTNVN6rJ87CB3G+nnCRBfUumj0kZ4OUBX+QabOL73g6FxdQrfcaViF+4DuESaRFQUVQJNPlp4xIi5Wuk7iLjJagAWHbhyn9oeaXdxhTkPIX5jCOuPk6pLNgLS0Qs0dk0WJQuddLLrjqgN8x5UDHW6XWZ6vICFZBxw7Xxf7HZle44BWDiIvqtIffgKNlYRttpDv8NGIRuX3bBntcLldoo6lhwsZGZcuFp/COO5nRxWTlLlzTgyx+P3DB06tEDOCWttDT6j5O2FpP5bG/ENTbSJmMnnLoDgecaDWsTagYpLrj3hmn0tz1irvhs84gE2dzsdLLgevkHZdfDiIs+A8yRxVLCA/vPts846K1qdbJgQfYTHBUdLQSh4Jk21MFlctChWSMIx4PlIiYfQ2pAsvMTsvwGKJ3Kiq4wGO6wnDOjToqFGh4cPNWmzi3YIl8v3qs+IjIp6ip6vmU1M8oqaR0FNGj3TwasENhj+fsv7KLychLsvW/CpqB9IuqlgCyOnBE5XW7vPPlVVQ6ES9NowcIdRtOrvFeeJUazPfrwz2P+PD8liF0ljt7Ox6wyw4HSJR91CEuXldqQcPsZJCQk+jQGM9kxdyCQLOGz4bFCJiSts9l9Xduq7yRPrTjYeNuZD8AuM626sZxZb5Pf8ie+hXitktKlQN3SMt7ynaFaiwJ+MYyno/dhQDpf06nywIEZIouFHmDjpbQyMg3S3tdLdVdwPhuG7v7hbty/POfPMAgY2FxoxbaELJ6nC2sHqHs8g3MMJKN7tRfCK4gVkAhZ0BcO7kxObvws2mivkPaokBKPrOxTZrJkZ1mMcjgk66MKlqKT4+BemEk14sbHj5oE9A/luBWMi9JvD2ILvqYyzyvD3o9YyepgudBynaieCW6O5kiVPJQNddsLWc5FwmTXz2uA0ON7AQjI6zJEF7PedCRY6qZu6g/5yLKcpNh+nv8okag3u16uoX/cp9OH9z5D9Rz1Htt9uX+E2K2ku1CurSwsL+xr1kcbScawSZ8LluMpfwj+++4dU/6iahobigW4x0HK0kH1juwdFSUM+hr7n720isLiW0b1zJQvJhMFkXhIncXF05141EFursbP6iYh01GAMP/nttT17FknmR4S614jxpiUnP+zPo4FFfcbBh/x77ufvxE5a+O1LAhqCBe2kyVvkt0bEZv871wBo0OaU9f7iOyAqDlXdgp3kRdWnpubzRYgPMXEDdUjHABHVDvCb7IFBGGLpChFta1CXk/fMakkPxsSHqGMASeUUDirk3rdVBFVZVXmg3MCCiAzBgt+HZGiTwpUAzPj4uG2QgB4QOm8RSS8wxWAc9OMULGQ/JsnndDZYHA1sq66eSrrvnTNmzCgKtAEUzX8HObY87Acs9kpwst9/WsuNjYllpaUviueQ7fEA1s1G2EovhW2rDzaaiYYePp2fCfgaG15QzRQLMkfOIxlbAQlqi4F00kSR1bcpv3eSI8Eq5ZkcjO49dmChMkVNe5w1xHCSnn766dWV3bvvJ7WHV3rKIOBqe7/y8qMimPDUwOBvlICkvgsBaMwYbslCtonvbOm6UHkWt1kcVokNb6uTOVPhkyQjNfV8I1XSiGENEiwcZkBa06vXMEhohzlYYGGrYKFxoHRHRo6PhaoKXxwQExTVSvZIswRkqqpMeHMsMOqPBj2qpQVP3/fo0V1RP/oFC35eQyU2DmvlzpQtms0ZaWlLIfKfAiNmJnuvTbAIfpwF1K2tQ+rrp7D3daZk4aDgyuk0D/aQJ9NFqJegXmyjXirUylBHbqb3r+a053EWsNe0NgwefDIbc1u79gsR+Q4nDiHBf2Qwtz+Do8kD4iCh5Li4ap6VtjMy5k69aqorJjbmdTteUCRBx8t5RHN0sqHNgzaw+L6WAyLU6kMpaNZrABbPM7vwsQEL1PVWwGL27NnV3Q3AIiEmZnu/qio1WlNH3Sh3o2pcBoyDN9oAC52uT/N2ixOvaqqrl9PCNQYLuofaMc6fZAGp5zwj5jpu3NgOAwvpXrxkyZL4vNzsR/guRQmMO9RGPWwFLFDOMOpPrx49NtoBi5EjRkivNstgwWmN4M0KFJG25TFITl9jVyk95KRaYhs2EvenJSR0P14M3Bws4FQhggA7HSx41P+oxsZroeP2jbvIFgC7g0iNIRwkbFVkVBCeUMJo3YqI9L/5A4tsBCWeMnVqoGDBtQnJMJRPTIiNvgObhfcjoqKk5CLTZ+8C3V+CeuxUdn+wigygGy5ijiyqoZpp7qoqPJ0cS9aYqaAwdpvVcZb3IAShGP1uNgn2EzEXBEi/ELCgcxGcsPa/KI3gKqNB0jhLujdGlOd4u0W6kaqePa9VwYKroVQDPto7IVCwgOG4w8BCTozYiKj7HLpDTb3iZX20Xs3B4kyj/oDp2ZIshjc0nG0fLMxVCchKmljWrVt9VEzMBRnp6YJJfafspPbBaDtbjsvxJFmIdB9TJk7sdLDgc23QoEEX0w50R+/Kyvlitz+wf/+ZAwcMmGG74hz1QXV1Z8Kjqo4zIg4WiNwW6VUCBwt6Pqe1UIGDnsIYPBNq/OVY2++oUjQyKaxVVJZaUAPxUtNvlLt/q15QMGxPka7LlIzVg/Wxz0iioMjsBfKdTOWeiHH91kjFjDH4NguBjQw0T3CwoImAQV/HGEwTRfTeYUfnKYLKUD41UUOd3ZYaiv12vG2woOvYsR0FFpQny+MZLNvIdy8wBgtXud3os3DFfRl1E+orVDeBnv+CrprlajIGi5Kiol8ZgQU91zJYIBjwUutqKPPzC9RoVLUgWrtrj+7dFyE7qs/jiOxgUyXNTMGioMBWug/2vHtI4rz0BAALDnSCYQvJ4IeJEyd6OvpEOVOwyMwMCCwsnM3CS1z/vn1HIQjwZWVtPELvDBpggOlH+DK8apa8oFrIs+tzeP5lsk3aInW9MW/EbzwoJkzfje/fZ2vaS/z2MG5jTiknPli4KfHVjewdLbTDf82O58u4ESNKMCAHOHMUOysEtY2WxDOKszhewQIeUktNAgdb3Q59lT8GAOMZ04cag0UdAu6MDNwYmy+gH822GkUL5rwWFwsGbvt5eFSa1kBHCcb0Cdm2vlHSO2hGYIEYjtbxY8ZYBwu6/6mnnnJXV1U9QyoEW5LFtOMALGAIbiQaNSG300QZryTaFGiVfQ0WWFgNyOR0JVD3kgp7Nvm6d3gGWso6MJTshrSO/UsVqCulVEFqZhdsdG8YZEyQGYfvM5OaBU0pWNnQyA0VGVOln+BgIUUxkdES7qocoYW+7nBDfX2lBXEqjIJ5zpaeCQww9sDbJtOP66y3nWDR3MFg4SCOxeI5KCYjMvwZNoH5UZNuunKvCQ4WkvF1Q92tsbQlAmiRIHG634yjdBYF1AJf2ck6CwcHu6cG6rJviMEZ4dD1Q5RDaAqju3yeDyxc0C0nxcbOsqvWnDlzZixUcZto8V5+woAFtWdATU2+cIgQYwCp7/wgMJBggAXp8+2rqsIoU22k2/1YkNQwDnJfPV3JytzCTtAzyjMn1ENj1Q0aVKiDCSS4zcMr7Ez5ygbXqA0Y07vlOuXqK2wSFv6iwEL+dtKkSUmarn/bxYBRZaWn38MSifESRudfhCN/zbsSpdUrzpeQbdb9RHAfP2BBbXVwGwwZ4dOTky8wDIyk/9EOsszpDDvoRw3loLGIACj4RHmNARO2MJuV3EthDJh0maupwOO5iDzm/Edwtz+gSl+2bJkIiPqPeA/E7ouNwEJIRTDKbvdFt8bFLQjABpaC+jFlXr3iBAILjZI0RoIgL/n876Oj/0qLNGAbQieBhX0jNY0J7JzX0I7/vaB5QqEsWrQoDjmvrsAG9RWecVlmZWbr7guRk4tlZFhuZtiG4XyLkDxM5wOdjKfcz2MtbvrFqKH4Lr+4qGgJMxZ5RSUjz1xGKFnDlFn1R13XDFN35OflTTZJ97HueFVDyfthsFtvNA6ISbhBgqiiHpBVnrLF3PpMwILuQRrtK4wmn1h82EkhaMi8DB8yZFBSQvzPMq7GUm4oFDgE5GFO3AjJr6eNA7I0UUXmVCTrk3rb33GwkD7sAMFX6ICcZTbAQroo9lXcj688UcBCbWdqevrVMuARu9WiAHfcuqxBAgvN10Ecs4oEkfOrevc+zw6zk+9AMOdlsGdR1HOQCx2EBFWnyEBxqWDwkAha2Nr5mWVGdlI+u0TFWcNroLLyG+sFVdN8bmBXnHQe+aUZuHVVXI6PjdnFspd6ZY56pK24ypewjhXkri/FznG1yghVAsZERTyv7Io1DhYngM3idhM10o9ZWWkNJhyiLxj3WxwoOFhwX2xM+K4ybz5PO0D2i7/hN/X4nPjm+vWR4iB6cZwkAucWQc0jD4uylkiQxj05LuEp8uBYpUiQfgvpfCNhuP1Y3A9XzXMM6O4kcf12Opnwv7DxWHUpdJFb41Kak60Cy04wsNApvXU55pgvdgWHCN1rl84Gc1nrcLCgd4gMwmK+oL3fg/ZplgGDxgS84nqah68G2SNKMzrLnDIjLMH7t6rrADFEIyXdaa3NVcMFWOaKveK0TrO+y/kE/jqhjQzRb7LbTnywYAnEFviIRYyeh7TjzOvP4HcvGPxiEPQ26CU34H+7VIKxz00whLId64kFFvCemKCAH/OucItdy18rKypuGT929FJ4hNyKxfosaH1I9scCWDhYX5fy7L8Gnmof4nSzLalJSVuxOP9XpTuq1fMsHOTdNCbMFeb7LU6PO5WrHHlRF1xJYeE4ROz7+oe8VPXKnOIBlFPkyY4pKUkSVNxtMD03ta0fJLsDsTjX3DcvTyDJgt8b4XTeIhkSAGO2Aohm9iKNxkpmZBgBF/fXADxlsr/BkCxQYuFI4FMnFxUWPkEbPbJvtmnM11auXBmGzYPYJInN5XKrQBMMTy2Riw4AUQ0D9p/gxrpYyfLrpHnyhIG9QSYZfNSs7ez//XDPQamFYSnQvxfHszLanthgwQmANL23q0eSckLyyr+TTBXGo1a8fyZfrBwsjlvXWaINpeB+nTNwlTZgmOLcDfG9ShOf+siCGkpnuYTiIE5/agQYbRjxmttzUl5WVsZKEp13IhhvmsFYObk3FJ6XDHB8h9InbGan5XEJxBXhcr15hNb6wbS05LH8N/w+iPg9kOdrP5jOU/Ck+hPt+E6MOAveN7JjYbf6Fq3Bg7AvzbHqnpqXmTkfc0y6Km+yABZ7bIIFj+Rv1I8cAyo80O4hN1Uei+PkQOfEEbE6HcoFW+eQzk6mx8Y6Be2eD+mou9E56Fhjm82OcUC/T/bTdp28SAtw4Q4lvor3HkCOulxyIukksCBPoQDBYp8BWHzFwYIv8oqyMnFwj8L8+dGqVCnJITti1cc0PTk50xnBrYBFUyeCxSF/QXly4iHl+2AYrLwGDNzL6SF296oUkgI1FmwR8nAirylY0EQnr6Y67G5+lu/jnhqUEbhFJonUpKsfAKq0uHgbs1scAYuBA03BAgUkcDwk34G+3pMO1Yk8BY0b77ERGISPb9PvD0ACGGpEc3awTB0WqGzTPkQyXyvcjoUbqQgM3Ygq1h+CrosAEBfSmfN7KWnmHQHEWRyzCG6z+S7SpGiUWZj8/leDJph21ZF0eqKkmQuXaARDjhJHCgAofL8HOH8MevULBljwvsLOdKoST/Q2NAoT582bF8N/fNWR92aKMZLrGCC/pLOBgoomJRwYn5+mDca3kMg8XHUl8j3J+UtrSq7r//rJsM1Bx2ePkxKGuNJzNnZmBPdIudgZWDwRDMmC/40I0F+BED+yHXGT2DFjAESV53/z86dfrygpqVMnSzDAIjU5+XwTsHjCAlg02MgNJY+/nCgOcmLGs8M6p4UqbUREXCi8nDBZFS8xY7Dg/UPSuFqcT/EJkx58tBfvMqI9FvhMt64vMArKazCQLLhkgxTzi910RrSoaMjmivLyu6traq7Aj84HKAhXQZ80QQfx7IY31BgLjMFJqoHTmU/6HqTA2CzUHcgHtl7MHczVQ/i/iEvYjl1tJdk8HmTHqjrbAIscCRYnTZo0uYPBYokCFk4GFpYAox6p7XGELz/Q/2N4kz2G6+2Y13dBxSii9r9UJVWk/FiDZJOZal/9gQXAclqA3lBOOoxrDLIYfybbEBHu/iIjLfWhAXV1i7Fr/w3OkbgZGfg2yMSNJGXewZhqZxaNJNlwpB55W6Yuwrwt5fZBeFJl4SL7ptYploNgyf1WJm6UFer8g8iG27czAXOMkqL6Z7paBgu4stZUHpEsDmEBHgSFBOK1xEOy6NOjBwtFNzaoocN50Pctxc7iOxhQOVEpL4wuRK5mLPoP4PV0FiZ9uLr78wMWa+VOk/ooA/om+sscS+d1SPockvSBN8aT/sCid8+ewwEWB+m+nwEWPtrU9u69beHChUcNsPyd40eOLIYq4WFxEpu6kFWAAJ2E6mRD/9raIWSrCcdkfYGYo2jjQQ4WZotVHFwDdcwyjN8P0PmbJSlsAvN5Dd4dDT4lam3tuSTFHKD+HSCwOJUzDs5oiK61uDyEd+41TJSI/+F5O+Kjolb1xcH0NhaEkySTPjnZ2S/qDn0/fzbAQThT7IYd7BY6a1qWe1Atuc5it54nFi+BxdQOBovFUrpnkkUgh0RNBo2fAy33S9UNOzdfSB77EB3/HMZkAp+LbYBllug/gcX0gPtPNK6tqEjDmFyGttLGhVU6Xx6bjJfBK07i7TlWqcuLK4orcVkjjzDmqVEoB1oeaLsUNHsN0vTasrKyBptt1yljdO+s7Kz7kxKTRIDfCtiXyjqbBgmog1D7ow6gq0CxMiuHzogDjrAwffdisMVV1HrsBPvSaV2WJ/YYnLwnjvwUvA8G3xVA5Q3wjHochF4W43JePGzw4GEi1Qe714oxrZT3kf5u63QvjdKqZwt68HvHjWss93fvALi1kleRSp96BJb1ESoRf/SAXr8gAudyQ2JYDrfi9VANbMjOzr0b2TcvbBwypAdf2Ng1l4u2KeMwENWfV5BDPSo3MSbmDGyxbwHd1wFAnsY4/FlkJMUOqk7RiWo4DCaF0xTvHdSAc5z9zxt6J51HHO1yTUFOqN8l4pz1OLjXwkh7ocBMLLJU1k6rxaFEtvfG5VfYiV6H3dlinLN+DVKeTG9sbMwwoHkx6jDUHNYHM+N7HXa+mAvj2m1gZPfn08FjFYE+k2ciFbveeAQrwuh9FRjWzZjXNzt1/QIwsKk4S6aY3WeF+bhQ+2Wnpw/CxiAloP5z+lMcUDLUjzg586zc7OwbAA43QVV6tVBiFKSlVRj071gW3e5v7IKcVfD+5ZfAc9Q77RwXGej3x3AyOqjao1vw6W81Cltrb/Qu/12ANNTa8Wyt4wLZOv85fLys/vYY8wBn0NoZfN5lMXEmfW7/OqV3dr7+TefVbpStYQ18cjtVXa3MD0NusUHuY/DvtZnigNPCIfP2WHyfFiD9df63PbrYfyerxOQ6ZjFbfLZmsw+8z4GXwNsRfDp3fv9143bi/6ESKuYTHDVUjtIiRPtQCZVQCZVQCZVQ+b/24IAEAAAAQND/1+0IVAAAAAAAAAAAAAAAAAAAzgL4cn2g77WR0AAAAABJRU5ErkJggg==" />
                </td>
            </tr>
            <tr>
                <td style="padding:25px; background-color:#EDEDED;">
                    <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                        <tr>
                            <td style="padding:37px 47px 20px">
                                <p>
                                    Hello ' . $data[ 'lfname' ] . ',<br />
                                    <b>Unfortunately, "' . $this->getListingTitle( $data[ 'lposts_data' ] ) . '" isn\'t available on your requested date (' . $this->get_format_date_range( $data[ 'lcheck_in' ], $data[ 'lcheck_out' ], 'Y', 'F', 'd' ) . ').</b>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:0 47px 10px;">
                                <strong style="display:block; border-bottom:1px solid #C1C1C1; padding-bottom:10px; margin-bottom:15px; color:#D0021B;">UNAVAILABLE</strong>
                                <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                                    <tr>
                                        <td width="130" style="vertical-align:top;"><img src="' . $this->get_attachment_url( $data[ 'lposts_data' ][ 'accommodation_image' ], 115, 115 ) . '" /></td>
                                        <td style="vertical-align:top;">
                                            <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                                                <tr>
                                                    <th colspan="2" style="text-align:left; text-transform:uppercase;">' . $this->getListingTitle( $data[ 'lposts_data' ] ) . '</th>
                                                <tr>
                                                <tr>
                                                    <td style="color:#C1C1C1; padding-left:0;">CUSTOMER</td>
                                                    <td>' . $data[ 'lfullname' ] . '</td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#C1C1C1; padding-left:0;">CHECK-IN</td>
                                                    <td>' . date( 'F d, Y', $data[ 'lcheck_in' ] ) . '</td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#C1C1C1; padding-left:0;">CHECK-OUT</td>
                                                    <td>' . date( 'F d, Y', $data[ 'lcheck_out' ] ) . '</td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#C1C1C1; padding-left:0;">NUMBER OF GUESTS</td>
                                                    <td>' . $this->getNumberOfGuest( $data ) . '</td>
                                                </tr>';

                                                if( isset( $data[ 'lrooms' ] ) && !empty( $data[ 'lrooms' ] ) )
                                                {
                                                    $content .= '
                                                    <tr>
                                                        <td style="color:#C1C1C1; padding-left:0;">' . $this->getRoomLabel( $data ) . '</td>
                                                        <td>' . $this->getNumberOfRooms( $data ) . '</td>
                                                    </tr>';
                                                }

                                                $content .= '
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:0 47px 37px;">
                                <p style="border-top:1px solid #C1C1C1; padding-top:41px;">
                                    But, don\'t be disappointed.<br/>
                                    If your dates are flexible, ' . $this->getListingTitle( $data[ 'lposts_data' ] ) . ' is available
                                    on the following dates:
                                </p>
                                <ul style="padding-left:10px;">
                                    <li style="padding-left:20px;">' . $this->get_format_date_range( $param[ 'lcheck_in' ], $param[ 'lcheck_out' ], 'Y', 'F', 'd' ) . '</li>
                                </ul>
                                <br/>
                                <p>
                                    <a style="display:block; border:1px solid #FFC200; background-color:#FFC200; border-radius:3px; padding:10px; width:300px; text-align:center; margin:7px auto; color:#FFFFFF; font-weight:bold; text-decoration:none;" href="' . HT_SERVER . SITE_URL . '/accept-alternative-date/' . base64_encode( $data[ 'lbooking_id' ] ) . '">BOOK ON NEW DATES</a>
                                </p>
                            </td>
                        </tr>';

                        $other = $this->getSimmilarProperty( $data );

                        if( empty( $other ) === false )
                        {
                            $content .= '
                            <tr>
                                <td style="padding:0 0 37px;">
                                    <p style="padding: 0 47px;">If your dates aren\'t flexible, here are similar available options in ' . $this->getLocations( $data[ 'lposts_data' ] ) . ' for your options:</p>
                                    <p style="padding: 0 37px;">
                                        <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                                            <tr>';
                                            
                                                foreach( $other as $d )
                                                {
                                                    $content .= '
                                                    <td style="padding:5px 10px; vertical-align:top; width:50%;">
                                                        <a href="' . HT_SERVER . SITE_URL . '/accommodation/' . $d[ 'lsef_url' ] . '/" style="border:1px solid #C1C1C1; display:block; color:inherit; text-decoration:none;">
                                                            <img style="width:100%;" src="' . $this->get_attachment_url( $d[ 'accommodation_image' ], 250, 180 ) . '" />
                                                            <div style="position:relative; padding:10px;">
                                                                <h3 style="font-size:11px; text-transform:uppercase; line-height:1.38; padding-right:50px; margin:0;">' . $this->getListingTitle( $d ) . '</h3>
                                                                <img style="position:absolute; right:10px; width:29px; top:10px;" src="' . HT_SERVER . SITE_URL . '/templates/assets/img/certified.svg" alt="" />
                                                            </div>
                                                            <div style="position:relative; padding:10px;">
                                                                <span style="color:#868686; float:left;">
                                                                    <img style="width:10px;" src="' . HT_SERVER . SITE_URL . '/templates/assets/img/pin.svg" alt="">
                                                                    <small style="font-size:12px; margin-left:2px;">' . $this->getLocations( $d ) . '</small>
                                                                </span>
                                                                <span style="color:#868686; float:right;">
                                                                    <small style="font-size:11px; margin-right:5px;">FROM</small>
                                                                    <b style="color:#000000; font-size:13px;">' . $this->get_format_price( $this->getStartPrice( $d ), $d[ 'lpost_id' ] ) . '</b>
                                                                </span>
                                                                <div style="clear:both;"></div>
                                                            </div>
                                                        </a>
                                                    </td>';
                                                }

                                                $content .= '
                                            </tr>
                                        </table>
                                    </p>
                                </td>
                            </tr>';
                        }

                        $content .= '
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding:25px; background-color:#EDEDED;">
                    <table border="0" cellspacing="0" style="width:100%; font-family:Arial; font-size:13px;">
                        <tr>
                            <td style="text-align:center;">
                                <a href="mailto:' . $this->getSettingValue( 'email_booking' ) . '" style="color:#FFC200; text-decoration:underline;">Contact our Booking Manager</a><br/>
                                <p style="color:#C1C1C1; text-decoration:underline; margin-top:2px;">
                                    Manage your Bussiness on 
                                    <a style="color:#C1C1C1;" href="' . HT_SERVER . SITE_URL . '">NomadSurfers.com</a>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center; padding-top:20px;">
                                <a href="' . $this->getSettingValue( 'fb_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1Ni42OTMgNTYuNjkzIiBoZWlnaHQ9IjIwcHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1Ni42OTMgNTYuNjkzIiB3aWR0aD0iMjBweCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+PHBhdGggZmlsbD0iI0MxQzFDMSIgZD0iTTQwLjQzLDIxLjczOWgtNy42NDV2LTUuMDE0YzAtMS44ODMsMS4yNDgtMi4zMjIsMi4xMjctMi4zMjJjMC44NzcsMCw1LjM5NSwwLDUuMzk1LDBWNi4xMjVsLTcuNDMtMC4wMjkgIGMtOC4yNDgsMC0xMC4xMjUsNi4xNzQtMTAuMTI1LDEwLjEyNXY1LjUxOGgtNC43N3Y4LjUzaDQuNzdjMCwxMC45NDcsMCwyNC4xMzcsMCwyNC4xMzdoMTAuMDMzYzAsMCwwLTEzLjMyLDAtMjQuMTM3aDYuNzcgIEw0MC40MywyMS43Mzl6Ii8+PC9zdmc+" alt="Facebook" /></a>
                                <a href="' . $this->getSettingValue( 'ig_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDU2LjcgNTYuNyIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgNTYuNyA1Ni43IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnPg0KCTxwYXRoIGZpbGw9IiNDMUMxQzEiIGQ9Ik0yOC4yLDE2LjdjLTcsMC0xMi44LDUuNy0xMi44LDEyLjhzNS43LDEyLjgsMTIuOCwxMi44UzQxLDM2LjUsNDEsMjkuNVMzNS4yLDE2LjcsMjguMiwxNi43eiBNMjguMiwzNy43ICAgYy00LjUsMC04LjItMy43LTguMi04LjJzMy43LTguMiw4LjItOC4yczguMiwzLjcsOC4yLDguMlMzMi43LDM3LjcsMjguMiwzNy43eiIvPg0KCTxjaXJjbGUgZmlsbD0iI0MxQzFDMSIgY3g9IjQxLjUiIGN5PSIxNi40IiByPSIyLjkiLz4NCgk8cGF0aCBmaWxsPSIjQzFDMUMxIiBkPSJNNDksOC45Yy0yLjYtMi43LTYuMy00LjEtMTAuNS00LjFIMTcuOWMtOC43LDAtMTQuNSw1LjgtMTQuNSwxNC41djIwLjVjMCw0LjMsMS40LDgsNC4yLDEwLjdjMi43LDIuNiw2LjMsMy45LDEwLjQsMy45ICAgaDIwLjRjNC4zLDAsNy45LTEuNCwxMC41LTMuOWMyLjctMi42LDQuMS02LjMsNC4xLTEwLjZWMTkuM0M1MywxNS4xLDUxLjYsMTEuNSw0OSw4Ljl6IE00OC42LDM5LjljMCwzLjEtMS4xLDUuNi0yLjksNy4zICAgcy00LjMsMi42LTcuMywyLjZIMThjLTMsMC01LjUtMC45LTcuMy0yLjZDOC45LDQ1LjQsOCw0Mi45LDgsMzkuOFYxOS4zYzAtMywwLjktNS41LDIuNy03LjNjMS43LTEuNyw0LjMtMi42LDcuMy0yLjZoMjAuNiAgIGMzLDAsNS41LDAuOSw3LjMsMi43YzEuNywxLjgsMi43LDQuMywyLjcsNy4yVjM5LjlMNDguNiwzOS45eiIvPg0KPC9nPg0KPC9zdmc+" alt="Instagram" /></a>
                                <a href="' . $this->getSettingValue( 'yt_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjEwMCUiIHN0eWxlPSJmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6MjsiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHdpZHRoPSIxMDAlIiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnNlcmlmPSJodHRwOi8vd3d3LnNlcmlmLmNvbS8iIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48cGF0aCBmaWxsPSIjQzFDMUMxIiBkPSJNNTAxLjMwMywxMzIuNzY1Yy01Ljg4NywtMjIuMDMgLTIzLjIzNSwtMzkuMzc3IC00NS4yNjUsLTQ1LjI2NWMtMzkuOTMyLC0xMC43IC0yMDAuMDM4LC0xMC43IC0yMDAuMDM4LC0xMC43YzAsMCAtMTYwLjEwNywwIC0yMDAuMDM5LDEwLjdjLTIyLjAyNiw1Ljg4OCAtMzkuMzc3LDIzLjIzNSAtNDUuMjY0LDQ1LjI2NWMtMTAuNjk3LDM5LjkyOCAtMTAuNjk3LDEyMy4yMzggLTEwLjY5NywxMjMuMjM4YzAsMCAwLDgzLjMwOCAxMC42OTcsMTIzLjIzMmM1Ljg4NywyMi4wMyAyMy4yMzgsMzkuMzgyIDQ1LjI2NCw0NS4yNjljMzkuOTMyLDEwLjY5NiAyMDAuMDM5LDEwLjY5NiAyMDAuMDM5LDEwLjY5NmMwLDAgMTYwLjEwNiwwIDIwMC4wMzgsLTEwLjY5NmMyMi4wMywtNS44ODcgMzkuMzc4LC0yMy4yMzkgNDUuMjY1LC00NS4yNjljMTAuNjk2LC0zOS45MjQgMTAuNjk2LC0xMjMuMjMyIDEwLjY5NiwtMTIzLjIzMmMwLDAgMCwtODMuMzEgLTEwLjY5NiwtMTIzLjIzOFptLTI5Ni41MDYsMjAwLjAzOWwwLC0xNTMuNjAzbDEzMy4wMTksNzYuODAybC0xMzMuMDE5LDc2LjgwMVoiIHN0eWxlPSJmaWxsLXJ1bGU6bm9uemVybzsiLz48L3N2Zz4=" alt="Youtube" /></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>';

        return $content;
    }

    function bookingRefuseMessage( $data )
    {
        $content = '
        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
            <tr>
                <td style="padding:25px; background-color:#FFFFFF; text-align:center;">
                    <img style="width:250px;" alt="NomadSurfers" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAYsAAABXCAYAAAD1a2lrAAAuWklEQVR42uzdb2hVdRzH8XP/ue3u3uvdH9e2dP91azDm1BabKQtqOlFRmyVTiqT2INAI90iy7GE9uA/S8EHSIgh6EEVJIBJB9cCCKMyKkFAzliSR2B902z339rnjd+Jw2GX3Thfaeb/gy++c6+7dFPf7nu/vd87vZwEAAAAAAAAAAAAAAAAAAAAAANweAjMBAIBH0IRXYOZ1AIDvhSyXsbGx8lQqVWmJS9gCAPjTzp07Q5Zs3br1rs7OzoM6/HTRokWXEonErzo+E41GjzY3N3eTMADA5xVFOBzui0Qi53WYdUcgEJhpg8HgVF1d3bMkDADwaaKoqalZq+aaSQ6Tam1FxoStmDLHWSWVsX/fCwDwh9WrV0dKS0vPmkpiyrSzha1IK5lk6uvr+y1h0hsA/DOhvcckg2nT5g0lipmvSSaT71FdAICPkoWqhBPuRDBH2Lm2pKTk54aGhhaqCwD4fwsogocPHw5qCOpzkwjSBSSLTC5y8xqaDO+lugAAHyQLE6eLTRaKSQXJAgB8dCfUyWKHoRQTijaGoQDAPxPc+5Qo5roTKuueBNcDe+9TVQCAPwQsGRkZqQiFQhcLuCMq4yQUVSMPkSwAwGfVRTwe36IJaycpTCpsT5KYdhLFsmXLjpAoAMCnCUNrQu0uKyv73VtNuM+XL1/+SjabDbF8OQD4OGG0tbW1qspIaQ7jnNaCmtbwVC5ZXIzFYu8sXbp0E/tcAID/BEySCJrziMKR3LhxY/vQ0FBnX19fpTupkCgAwD+Cs3X6GmIK5Pt65igAwF8ilqFbYB/RkNOH5eXlZzVf8Y3Ov9SQ07FVq1ZtYcMjAPCngNPxd3d3363mA0U2X9TW1p4YHBxs5sE7APCPsGW0trY+qoUAf3E9V5FW2E64b5NVtXFBbYcnYQQUQSa6AeDOXesplK+a0Jap8bJo2avmae1Cnti+YZYBOa0FB73Jwfv5ESa/AeA2SwieyD8prXC2TFU18a2rmrDNcUHLe+gW2hHXz7BE0aFbbKvN3t2zToaTPADgv00OYRN5HThwoFrDS21NTU0bdJp0f4b2zT6Ye2bCW00UkywUxxWO5xW2Ko4zmhA/uXjx4mPaDOkZvTbY399fo9YrTOIAgFvFe1Xu0d7eHlfTpKSwVlf1T+j4ZcW7Wqbjq4qKigl13tcTicQXeniu0pLe3t5m/dnHriex0+Z4PsnidUvMKrXjeXbRm1QFc1nHHyk5HdJ2rQ9oY6Vq73MdTJYDwPyFFBHLZXR0NHd+r4aPntZ6TG+o4z+t89/m6NwPWdLT09MYjUZ/NJ34lGvZDttEUclCn/WaJZq7CCsxfeL63LSpVqad7+GJ7/XzH6murt48Pj5e6q42SBoAUGwlYaxbt65OQznDujI/rjinl/7Ms6dE2hMZdco39LR1v9kq9UnTod9wvS9tXitmOCpt2jFLpEXvv+De28IJTwUz7a5k9HexNVz1Q1VV1XNqW0kaWGgBd3Dr3sKPlw8MDIQV/EIv8O2sqhru0xDSEXXEl2a7unddxWfybUikIaCvdeXvfOZbTmfvaa9omOopdd5OhZKZa2c8fa7d0dGxwZKWlpYHzXts0xayWdKUaZ3hqmuRkshRHXfw4B9wp68X5MECcguzcJ867i5NEr/t3KY6y7MPmUKHirSX9guW7N27t17Jx50MMmbDop+6urpatm3b1qrjQqqKjLkT6oozcd3Q0LB/PhPlJmzPfhl/qNI4unLlyiZLzJ7gXJTg5j22fXvVnh076nS7Xu1MbNpUu2vXrvpUKlVm4VYIWsa+3bsTanrX9PQMD6xf/3g0ElnjrBtEwrgFCVlWrFgxquZvz8ZCdnEdsLnKV2jo6X5LGhsbH1blMNM5u7dL1YqyL1pSWVk5bIai7AK3UP3M+b+hu57edFcq84yMJ2lc1s++35UoIhZwM79ciVjsVCIevxovL5+Ix2KXy6PRiYpk8urmoaFhS5RAWOhs/oJmUjV6T3v7S/q3Pa/TvzQ0kq1ZsiTX4VzXcMR3em2QjW/+Ye86oKQqsjbvdU/35JxjM4GJDDDDMDAwpAGGHAUFRJK6rP6uGFbWrOuaRRBXBGVd18iCi4FVdA2sq4LZVTCsa0DF9KMkCcLM9PxfNbf469zz3vR7PdMDeLrOqfN6pvu9V3Wr6n51YwVYaI6SZ9OvQU/GOAOqzSQ1vAojcgy95k9S4lDBJCUlpV58iTxQCwksmq1ILGDkt9Fzw1Hf5faKjgINRIv/s7a2tozoFHK1DZV27XjfMpp0gwYNmhUCi/aD8YIFC1LBSN5UXSFxfRX1BdRd4iQ17FpHhcCifXQeMGBAgthNt2eHzhk6QP2P4tmwL6VDcviYxq9FPh/j+hZUUAnUjpvlvVae3bVrV9/6gvG8ECopVRJq7aDaoqi19qDO7BIqodJOsHiV7ZjE4SytWCAzQ2DRfv05dqbSKOoFg1g5ZsyYfHyO7uLxhKeHh3twvsEpGzduDBkiAyxSzTJ37twq2Bc6iuF6AeJeGJ59BxNBehitSA1eqYKCrWGZbAd28A9aAAvZtv1IKNhH3IfrNFW91ZGVtwe2jEvXrFnj6hIqoRIgWLzGROBmMXmhUw+BRftp2xN1t6ArGM89RjvikL2i/WBBBujeBBaBVm6A/kzkfSJ7xJXcAA3AaIbKq788rwIBfS9bkGqkh9VWSCXp1IVrVKYejCpdfZEmvXXIkCG1XUIlVDoQLEKSRQe4byLoa4FvsWInmZ+dXST+hwjcsNDhN8ctWHiFmpBiGJYqUd7nEtM9qIDBFnJ/7jJ9+vRklA/oN6YSgpRI0tLS/i6fjfs2qN91ZKX++AAO8SK7YCcZ1iVUQiUEFscfWOCwm6U+/bTH884keJ2FpIjgggVUQfbVOcSouUQAe0Wjsk6iUJ+RYEAM/w8KmBTj3d+wdWSqEtK76FfTrZl43ocdaNzmffKS++/LDQ0N3UJnaIRKCCyO38CwWwVN4Rr5d5FmIhRTETwDN861jgWD3GZHpUOJ+1SQ+Boqpy9hr3gYn+OZ00Es0n3Mw+783+K3yCVVJxuA8R0EtZWVGI4W3aG3pmelTxP3IcVHPS4H1biNDqjNSlbbJjhP3KQEFYZ1RFpePUiTWCNm46todBDeY61/du6ldgarzbJtQaSLeT+CDBYaq7/ISHc7YEGG0b/T/7RgRd13dv+PlznBGPpJCtM8ZMaAuW0BBuw1UBmeIjSHigFYJ+aqMf7xCpj8ewL8Jd8EgESCMW9q22ZBQXxu197hw4f7VJLIXHtqR9krBOip70Z7nkPMSQ13L+ZRorzqjEHwScsP3NCDleFRlkDzsnMma/Ac3ey5/Dub32t222v7/fSOoL+H3tExYME3BFTanxHT0U7mqtuZy4zuVp7jMPmfrG7631Ki6ZPkT++kar5GA6ez08iPvtP6z1VD5r9xyHYGSVIWwXE7mOqnhQOFpmutIk9UYUXhCBaz4TrnnHNi2bMlgNTS/ZepacLJPlVvpk5i/98id/gZaRl3tdNe4eX5omg9T5UND2gd8UlWVVFRGgFrfmZqat3Afv26k6jCftu+lAZzEBE8duTIfvjYV6QABgPqedFFF8XwXZj155sXaWySA16Sn98dH/sX5OXV4TsPD+Ixa7egA87IrUDK4P7wWqgbPXp0T+w2HNbba35YivScEGmOS46IsX3Li4v7Txg1qgzvDefj1Z68OKLNo/BcuPcJj426YQMH9hLBYez5AYMFpzm9MwKiego+JuK7ZFM6UAl43IN/v8b7Tf1KO++88+xEtl9HNH20ne10MmbsqqysTBV0xlxNxZzS2/p98PtvPsbjxo1LFu1EpHPKypUrI/n8CYaEgZ1/Li53ou4zMPYeIgb9dnl5ea68ETaIBtQVUNs8j9iJzdDz/wV/nyEyzcrfwKPtKnFvaWlphQpQeI6LvKaWKgBlJsksl8+DRHJlAGDRwm0sJB39E3aTGXIecb7GS3fUB1DvokM1VqI+iIW/UP4APt3FIMINuqZtjQwP34GX/uR0OH6Ki4nZCT3d1sTE+FvhLlas7A40u0CUiuyHuPF8PHdjYkLCdjDdvSJfidvl+gmRwTuRHfFTREau9+TkzJ0zZ066xfwl8ruzUR+ivt1NUZUrUD004Pmw+v8R9T+usLAfRXZJ8d7oqKjvwtzul9xhYWdjkUUpbdZEpYlbgUjmxS63ewsm2w8O0EXD/SnJyT7a4H83KaDjtMu8YTCrweU2tO3diPDwb8Pd7p8EXfD5p+SEhB2xcXEfx0VFPZgYFTWC2mQPSIlG4jD4nKycxRjn95MTE3eg7eI9PtrDde4Th67fRykMZHnTDljwviFuIAHxA6fh41p4dvwX129FdbqcXyckJbzVNS9vWe/evfv4BUHqM+ZHAi5L8MzV0yZP7i/bYHN3eQYdnD+Ov5O/Dy6PSYKx5GZn3z9l/Pje6nMqKioq01JSrg93uTaDjiIr6KdYkB+h3o/P1QrzTgQoLxHrDrRbQWtwGeq/iaafE/NayepyWrPTeTs5nWfNmhVVXlIyHWvnYYzjFqwvYVDdHhkR8TWiwd/G5zvgaTVGUaM4rYBTPIpoR1ZGxv2gcx+1/2VlZeVpSUnX4Z2bUI/2H3NLxBRIWml8jPpXVdWLMcTaeQv3fS3nRBIC2xKjo9fVVFXNhEE6xi6w2V1zVX2rSjOyMq5z6s6PVAYucjxhrhbTz6Kx1u8yY85Yr9+B3o/17dt3ZhzWKP5+xwzcKUDvEy5hqICAOXWTcn8k6vttqK+8LBtuC2vbdlTBB+tILWZZCzLepMPPii+z0tLOA4Pa7w+5wFT29ejRY54EDKuDA8YfjwFYjN34Twb6NMN35eXkfAPm/eujOw08yw8YPWmEtBjMcniejNVY6mGTdmyFtNBbPpdoc4mVJF7hmGRor8KA/NOlHCdtgTmvwbu9VukSFR7+fHVlZZXF3ZcuP/SsrDzX7XbttrJDwU1iwUeg/ss2WFDfKkpLh8dGRX2kPregq+cHT17OF5jE+9T/5+bk/AXXhDb6pNPuLAeXXd2KilqnTplypkUa8Hmymt77B27Y4+8D4/OI33pyc1snT5gwQzLmpLi469vyUEmIi7tZAYt8gFsTuVya7CjNK+55QG0n3wD0rKgYnpqa8g67bz/mz27+fADHpu4lJf0sGDR1CibLFvdlZ2W1nnzSSaeRNBCJzdEf2mp7YmzsrUyt1KVPSUkS6Pag+ru4mNi9hQUFXyQnJuxgfObfSEMxJEiAoavPhFrJjUsjAP0m0PpdrMf/kbF0qA8rO/bDqGL+N1NW2ia+VpEp9nzZZukKLU7Uk8waO/yTTNJ+eOk53/okHyoIlJtE3/vejSrf22zCwz7BBvw+AN4sdoKeLTo2KoadJiXj4loswCuwo+Q6PC+rLUd9cpE9MTstbb7fBtB3hXl5VfGQGHiGR+Fmhnr0HeKzREp1IHKzstaD2HHsfUZM4BG65yARV3zeVti169nJSUn75OEhJv1rlv3DgO4ZOHCgz8hUWlR0p1zgBBiGbZb3QvpoheFokhET46BXXFg4HrvSHyXTMKK9xugi2kCAsQfXoew9pqqLqIiI20TktUIDM9o3Kbusp5W8NF4rYCH/HtCv31QnUlV0wRjER0dvLe3W7fS03Nz8sWPHZk0dNSoFasccbDoGJMbF3Q3gkDn/38A1w2QjopHfeaZvTPPzW6dOnjwrQLCQp45dzOaUEbMU4LQbYCbAwjeukLTXSm+SmMjIDZD4LqytqZlb3bPnbPz/94X5XV8cPXponuJIEI1dpQCa03HPXHFFPVnQl9rxDha3uHceq3NQf431KZm7ztsHEPtNmNPpGxvklvrS5XReAzVmPVQh4v3ZYFYFWD8NeP5i5Pb6kVwlmyvLy0/hgGFEbxzAI8ZjZ1ZmZuu0iROn+fofG/uQD8Qcjha88xnR/xr0H++aLYauID//X42NjYXqfMB3cZEu1+viPkj1B0q6dbsd66xnBebBrEmTUvF9BlQ8pcX5+QvBXz5UoopP8wdsHWm/I81COGkiLmXGcH92AQHO5epc69+/fwwM1RsRiV0u36EAUBOXLoj/rFZtIVgf6wxAQcy/fRifrzGe/xB2EmTTHYQ2M4AIzEFplMlO5qBETolcxGh91UBckvc3A7kG+hOPSRzdRfeqRCfGRH/LNuDd8n9gSj4mLA0zwv2NLxr2/kcNBmG/2kdFp2fcPwq6waJ7NCE29n9Yqgsvu7fZhDY7kYumG2PYvK0TUQ+p7xSVAJTTRTBotU+H6be7KoqLe3Oa8HdBpXidvI8FBXl16o8EKwlYrG+8LRwsuJtidnRkpC8PDxjdqqvIFsKLskDrcfExiCi3+0U8O9wsihnMIwuXrwRYTJsyZXaAYPEXZogMawMscoWaTjDLOaeeOjwzPf0Wn7trbOzLws7WTmYlx2WdnZtkf/M9ngXQBvjGBYx2w4QRI3Lauq8Bc7Kga9eNXWgX3K2goK2cU7oCFnvT09JaoR0YCXXUdSQ5bYa0NNAqzRFLci/19UOkx65p6waxztOxO6Zo5laoeAYr/Q5G0bjTxLBhw3LBjHdazMkkc0FtkLST6wI0qqd1sEa+DOCRhzX0o4E6yiv/LioqGiN/D61IKcbhDWyUns2BBA5QEHEYp0ECqhMaGyPea2K4DhwsuFHEHzFklYyrKD//NQPjmTC1O0hfXQqd+065mzXKR6JBwsGE34Nn7pHvkUyTM3BPXu4LzFbCmcA69nwzn2m/qgBIWocxWVqUQbR2L0k0ifHxfzbYtTrIEDYFO71D/LliDFgI/m5hu8Du7cizpSQgvifAgMT0BhnYNVH5GNT36zdRo3YaALOXdLSiv60a6xfzC7cCFmFkmLuIVB7vjSAbUDW+kzsc5h7spkVVHIustsT4liv0MgeLSZMCBQvJuC71BxakEjgAZrd/3JgxVwuALy8rexnzkDYupt5LmoHEJasEw9ukNxTRIYw/j3sbSYnLk51dC7uEb+5Avfg41uFRtYfSBl2hcxjdHwlbwRPiPkib28s9nnSlv2ZgsR82m4OTxo0T/T9QVlz8Gp4T77f/9Iz8rKxu2JT4znqAfWo83edG1fmcqFbGoqAg/yHSZHwNlVRaJwWNOSkn0yKsQatuq00UL3MBY9ZdwENWSUkA6+Jk+RKovBZiHRk9v5mC/t6WB2BJiYfUZUZFN/NYCwZYyCCN77Fbug8oeBX0XpdjN7EMHfrCiAGT2kYM/mA5ibkuFd9LMfsQJwZ+9JlT1y/BqhkCdUQviF69RBuBnjeDQR5Qd+rqMzB5L5cDYgMsvBJ8IIp/UFRQcCd2LJdAD7sUIvGnbRmQ1OehvTuA6sJofgl2B9dD/NtM7fTyeyJg+KquqChgE9xBJ3QtZLT3yr6C+f8HuwfRxyGoPSrLymoxUU7Kycpa4QDgMv1mEwUAzTGgiSaMmbnZOVsMaNkigQ39WVdeWnoZJucM2IjOSoHHBsZ8G40R/c4yWDjICP1XSqR2pTUVAon9KSliMW3tVVl5Cp3joAULLGxKFvsgWRwqKyk5KAyUSibRsI4IyqM4izCLC10XtIHTwz/FvTjz4h2MQbQVQ79MJTK+oSEN60DO+2sYbThYZArJKi019XD3igqxLrdBzZpsqf/UT9gMffENYbq+BfMtUY6nv3GCBBKPdfpCQnT0negbuy9oRa7RjRZtSl7iS/uhYi1SxwGquRw4SHyl/PZzrAk5dwSQbDThV4cpX9jvGJ01BfidZuqlYIJFk4bFn5KU9IhPH0xFPZ82JTX1GYN7m0hMvI4tACcN9GyuYvExGgAMgUiyWUORlrdXZlraN0zC8F2F91IPeGFIercFFly0i0tIuHEYdKfqu3rBZQ/eUP/wB6TC46EX9MAGu8VbDO49TGmGT5c04Yuw25GjErdJGhFzvZcWhWFJT0qa7Twi7XjZjuZxI1CC5DeZH7gi6YmNwHfYrY0wek8JDJFdYFQlwGi2CBaaEouznvp/jmWmSkbHwR4PU0EdW7BAyRVegZKGWCMz2D0dChZWXGQxPuPgReSbA1B3TlWAwG+RbpxYX7+Rdo758+cbMWJdSTmxV/Y/PSVlto3+h5FH1SxxL7QMG6FTj7IYgKdTqowYce1MoEDpgbrHjgoKNHpStlvOR2yA58lNneQJkFTvUjwg+wAwDvP3SFpjjH8Qp+wx78zgFw4W6mfol59VJyRNPFFdMhc8Ovyp7Ii6U83MyPDdq4qTQi0CZH5F/b32/+/aKrM1wrfaLcVWrpYASg8WRnhidl5VlZUHrwXZVgs2C9nf29VJTNVN6rJ87CB3G+nnCRBfUumj0kZ4OUBX+QabOL73g6FxdQrfcaViF+4DuESaRFQUVQJNPlp4xIi5Wuk7iLjJagAWHbhyn9oeaXdxhTkPIX5jCOuPk6pLNgLS0Qs0dk0WJQuddLLrjqgN8x5UDHW6XWZ6vICFZBxw7Xxf7HZle44BWDiIvqtIffgKNlYRttpDv8NGIRuX3bBntcLldoo6lhwsZGZcuFp/COO5nRxWTlLlzTgyx+P3DB06tEDOCWttDT6j5O2FpP5bG/ENTbSJmMnnLoDgecaDWsTagYpLrj3hmn0tz1irvhs84gE2dzsdLLgevkHZdfDiIs+A8yRxVLCA/vPts846K1qdbJgQfYTHBUdLQSh4Jk21MFlctChWSMIx4PlIiYfQ2pAsvMTsvwGKJ3Kiq4wGO6wnDOjToqFGh4cPNWmzi3YIl8v3qs+IjIp6ip6vmU1M8oqaR0FNGj3TwasENhj+fsv7KLychLsvW/CpqB9IuqlgCyOnBE5XW7vPPlVVQ6ES9NowcIdRtOrvFeeJUazPfrwz2P+PD8liF0ljt7Ox6wyw4HSJR91CEuXldqQcPsZJCQk+jQGM9kxdyCQLOGz4bFCJiSts9l9Xduq7yRPrTjYeNuZD8AuM626sZxZb5Pf8ie+hXitktKlQN3SMt7ynaFaiwJ+MYyno/dhQDpf06nywIEZIouFHmDjpbQyMg3S3tdLdVdwPhuG7v7hbty/POfPMAgY2FxoxbaELJ6nC2sHqHs8g3MMJKN7tRfCK4gVkAhZ0BcO7kxObvws2mivkPaokBKPrOxTZrJkZ1mMcjgk66MKlqKT4+BemEk14sbHj5oE9A/luBWMi9JvD2ILvqYyzyvD3o9YyepgudBynaieCW6O5kiVPJQNddsLWc5FwmTXz2uA0ON7AQjI6zJEF7PedCRY6qZu6g/5yLKcpNh+nv8okag3u16uoX/cp9OH9z5D9Rz1Htt9uX+E2K2ku1CurSwsL+xr1kcbScawSZ8LluMpfwj+++4dU/6iahobigW4x0HK0kH1juwdFSUM+hr7n720isLiW0b1zJQvJhMFkXhIncXF05141EFursbP6iYh01GAMP/nttT17FknmR4S614jxpiUnP+zPo4FFfcbBh/x77ufvxE5a+O1LAhqCBe2kyVvkt0bEZv871wBo0OaU9f7iOyAqDlXdgp3kRdWnpubzRYgPMXEDdUjHABHVDvCb7IFBGGLpChFta1CXk/fMakkPxsSHqGMASeUUDirk3rdVBFVZVXmg3MCCiAzBgt+HZGiTwpUAzPj4uG2QgB4QOm8RSS8wxWAc9OMULGQ/JsnndDZYHA1sq66eSrrvnTNmzCgKtAEUzX8HObY87Acs9kpwst9/WsuNjYllpaUviueQ7fEA1s1G2EovhW2rDzaaiYYePp2fCfgaG15QzRQLMkfOIxlbAQlqi4F00kSR1bcpv3eSI8Eq5ZkcjO49dmChMkVNe5w1xHCSnn766dWV3bvvJ7WHV3rKIOBqe7/y8qMimPDUwOBvlICkvgsBaMwYbslCtonvbOm6UHkWt1kcVokNb6uTOVPhkyQjNfV8I1XSiGENEiwcZkBa06vXMEhohzlYYGGrYKFxoHRHRo6PhaoKXxwQExTVSvZIswRkqqpMeHMsMOqPBj2qpQVP3/fo0V1RP/oFC35eQyU2DmvlzpQtms0ZaWlLIfKfAiNmJnuvTbAIfpwF1K2tQ+rrp7D3daZk4aDgyuk0D/aQJ9NFqJegXmyjXirUylBHbqb3r+a053EWsNe0NgwefDIbc1u79gsR+Q4nDiHBf2Qwtz+Do8kD4iCh5Li4ap6VtjMy5k69aqorJjbmdTteUCRBx8t5RHN0sqHNgzaw+L6WAyLU6kMpaNZrABbPM7vwsQEL1PVWwGL27NnV3Q3AIiEmZnu/qio1WlNH3Sh3o2pcBoyDN9oAC52uT/N2ixOvaqqrl9PCNQYLuofaMc6fZAGp5zwj5jpu3NgOAwvpXrxkyZL4vNzsR/guRQmMO9RGPWwFLFDOMOpPrx49NtoBi5EjRkivNstgwWmN4M0KFJG25TFITl9jVyk95KRaYhs2EvenJSR0P14M3Bws4FQhggA7HSx41P+oxsZroeP2jbvIFgC7g0iNIRwkbFVkVBCeUMJo3YqI9L/5A4tsBCWeMnVqoGDBtQnJMJRPTIiNvgObhfcjoqKk5CLTZ+8C3V+CeuxUdn+wigygGy5ijiyqoZpp7qoqPJ0cS9aYqaAwdpvVcZb3IAShGP1uNgn2EzEXBEi/ELCgcxGcsPa/KI3gKqNB0jhLujdGlOd4u0W6kaqePa9VwYKroVQDPto7IVCwgOG4w8BCTozYiKj7HLpDTb3iZX20Xs3B4kyj/oDp2ZIshjc0nG0fLMxVCchKmljWrVt9VEzMBRnp6YJJfafspPbBaDtbjsvxJFmIdB9TJk7sdLDgc23QoEEX0w50R+/Kyvlitz+wf/+ZAwcMmGG74hz1QXV1Z8Kjqo4zIg4WiNwW6VUCBwt6Pqe1UIGDnsIYPBNq/OVY2++oUjQyKaxVVJZaUAPxUtNvlLt/q15QMGxPka7LlIzVg/Wxz0iioMjsBfKdTOWeiHH91kjFjDH4NguBjQw0T3CwoImAQV/HGEwTRfTeYUfnKYLKUD41UUOd3ZYaiv12vG2woOvYsR0FFpQny+MZLNvIdy8wBgtXud3os3DFfRl1E+orVDeBnv+CrprlajIGi5Kiol8ZgQU91zJYIBjwUutqKPPzC9RoVLUgWrtrj+7dFyE7qs/jiOxgUyXNTMGioMBWug/2vHtI4rz0BAALDnSCYQvJ4IeJEyd6OvpEOVOwyMwMCCwsnM3CS1z/vn1HIQjwZWVtPELvDBpggOlH+DK8apa8oFrIs+tzeP5lsk3aInW9MW/EbzwoJkzfje/fZ2vaS/z2MG5jTiknPli4KfHVjewdLbTDf82O58u4ESNKMCAHOHMUOysEtY2WxDOKszhewQIeUktNAgdb3Q59lT8GAOMZ04cag0UdAu6MDNwYmy+gH822GkUL5rwWFwsGbvt5eFSa1kBHCcb0Cdm2vlHSO2hGYIEYjtbxY8ZYBwu6/6mnnnJXV1U9QyoEW5LFtOMALGAIbiQaNSG300QZryTaFGiVfQ0WWFgNyOR0JVD3kgp7Nvm6d3gGWso6MJTshrSO/UsVqCulVEFqZhdsdG8YZEyQGYfvM5OaBU0pWNnQyA0VGVOln+BgIUUxkdES7qocoYW+7nBDfX2lBXEqjIJ5zpaeCQww9sDbJtOP66y3nWDR3MFg4SCOxeI5KCYjMvwZNoH5UZNuunKvCQ4WkvF1Q92tsbQlAmiRIHG634yjdBYF1AJf2ck6CwcHu6cG6rJviMEZ4dD1Q5RDaAqju3yeDyxc0C0nxcbOsqvWnDlzZixUcZto8V5+woAFtWdATU2+cIgQYwCp7/wgMJBggAXp8+2rqsIoU22k2/1YkNQwDnJfPV3JytzCTtAzyjMn1ENj1Q0aVKiDCSS4zcMr7Ez5ygbXqA0Y07vlOuXqK2wSFv6iwEL+dtKkSUmarn/bxYBRZaWn38MSifESRudfhCN/zbsSpdUrzpeQbdb9RHAfP2BBbXVwGwwZ4dOTky8wDIyk/9EOsszpDDvoRw3loLGIACj4RHmNARO2MJuV3EthDJh0maupwOO5iDzm/Edwtz+gSl+2bJkIiPqPeA/E7ouNwEJIRTDKbvdFt8bFLQjABpaC+jFlXr3iBAILjZI0RoIgL/n876Oj/0qLNGAbQieBhX0jNY0J7JzX0I7/vaB5QqEsWrQoDjmvrsAG9RWecVlmZWbr7guRk4tlZFhuZtiG4XyLkDxM5wOdjKfcz2MtbvrFqKH4Lr+4qGgJMxZ5RSUjz1xGKFnDlFn1R13XDFN35OflTTZJ97HueFVDyfthsFtvNA6ISbhBgqiiHpBVnrLF3PpMwILuQRrtK4wmn1h82EkhaMi8DB8yZFBSQvzPMq7GUm4oFDgE5GFO3AjJr6eNA7I0UUXmVCTrk3rb33GwkD7sAMFX6ICcZTbAQroo9lXcj688UcBCbWdqevrVMuARu9WiAHfcuqxBAgvN10Ecs4oEkfOrevc+zw6zk+9AMOdlsGdR1HOQCx2EBFWnyEBxqWDwkAha2Nr5mWVGdlI+u0TFWcNroLLyG+sFVdN8bmBXnHQe+aUZuHVVXI6PjdnFspd6ZY56pK24ypewjhXkri/FznG1yghVAsZERTyv7Io1DhYngM3idhM10o9ZWWkNJhyiLxj3WxwoOFhwX2xM+K4ybz5PO0D2i7/hN/X4nPjm+vWR4iB6cZwkAucWQc0jD4uylkiQxj05LuEp8uBYpUiQfgvpfCNhuP1Y3A9XzXMM6O4kcf12Opnwv7DxWHUpdJFb41Kak60Cy04wsNApvXU55pgvdgWHCN1rl84Gc1nrcLCgd4gMwmK+oL3fg/ZplgGDxgS84nqah68G2SNKMzrLnDIjLMH7t6rrADFEIyXdaa3NVcMFWOaKveK0TrO+y/kE/jqhjQzRb7LbTnywYAnEFviIRYyeh7TjzOvP4HcvGPxiEPQ26CU34H+7VIKxz00whLId64kFFvCemKCAH/OucItdy18rKypuGT929FJ4hNyKxfosaH1I9scCWDhYX5fy7L8Gnmof4nSzLalJSVuxOP9XpTuq1fMsHOTdNCbMFeb7LU6PO5WrHHlRF1xJYeE4ROz7+oe8VPXKnOIBlFPkyY4pKUkSVNxtMD03ta0fJLsDsTjX3DcvTyDJgt8b4XTeIhkSAGO2Aohm9iKNxkpmZBgBF/fXADxlsr/BkCxQYuFI4FMnFxUWPkEbPbJvtmnM11auXBmGzYPYJInN5XKrQBMMTy2Riw4AUQ0D9p/gxrpYyfLrpHnyhIG9QSYZfNSs7ez//XDPQamFYSnQvxfHszLanthgwQmANL23q0eSckLyyr+TTBXGo1a8fyZfrBwsjlvXWaINpeB+nTNwlTZgmOLcDfG9ShOf+siCGkpnuYTiIE5/agQYbRjxmttzUl5WVsZKEp13IhhvmsFYObk3FJ6XDHB8h9InbGan5XEJxBXhcr15hNb6wbS05LH8N/w+iPg9kOdrP5jOU/Ck+hPt+E6MOAveN7JjYbf6Fq3Bg7AvzbHqnpqXmTkfc0y6Km+yABZ7bIIFj+Rv1I8cAyo80O4hN1Uei+PkQOfEEbE6HcoFW+eQzk6mx8Y6Be2eD+mou9E56Fhjm82OcUC/T/bTdp28SAtw4Q4lvor3HkCOulxyIukksCBPoQDBYp8BWHzFwYIv8oqyMnFwj8L8+dGqVCnJITti1cc0PTk50xnBrYBFUyeCxSF/QXly4iHl+2AYrLwGDNzL6SF296oUkgI1FmwR8nAirylY0EQnr6Y67G5+lu/jnhqUEbhFJonUpKsfAKq0uHgbs1scAYuBA03BAgUkcDwk34G+3pMO1Yk8BY0b77ERGISPb9PvD0ACGGpEc3awTB0WqGzTPkQyXyvcjoUbqQgM3Ygq1h+CrosAEBfSmfN7KWnmHQHEWRyzCG6z+S7SpGiUWZj8/leDJph21ZF0eqKkmQuXaARDjhJHCgAofL8HOH8MevULBljwvsLOdKoST/Q2NAoT582bF8N/fNWR92aKMZLrGCC/pLOBgoomJRwYn5+mDca3kMg8XHUl8j3J+UtrSq7r//rJsM1Bx2ePkxKGuNJzNnZmBPdIudgZWDwRDMmC/40I0F+BED+yHXGT2DFjAESV53/z86dfrygpqVMnSzDAIjU5+XwTsHjCAlg02MgNJY+/nCgOcmLGs8M6p4UqbUREXCi8nDBZFS8xY7Dg/UPSuFqcT/EJkx58tBfvMqI9FvhMt64vMArKazCQLLhkgxTzi910RrSoaMjmivLyu6traq7Aj84HKAhXQZ80QQfx7IY31BgLjMFJqoHTmU/6HqTA2CzUHcgHtl7MHczVQ/i/iEvYjl1tJdk8HmTHqjrbAIscCRYnTZo0uYPBYokCFk4GFpYAox6p7XGELz/Q/2N4kz2G6+2Y13dBxSii9r9UJVWk/FiDZJOZal/9gQXAclqA3lBOOoxrDLIYfybbEBHu/iIjLfWhAXV1i7Fr/w3OkbgZGfg2yMSNJGXewZhqZxaNJNlwpB55W6Yuwrwt5fZBeFJl4SL7ptYploNgyf1WJm6UFer8g8iG27czAXOMkqL6Z7paBgu4stZUHpEsDmEBHgSFBOK1xEOy6NOjBwtFNzaoocN50Pctxc7iOxhQOVEpL4wuRK5mLPoP4PV0FiZ9uLr78wMWa+VOk/ooA/om+sscS+d1SPockvSBN8aT/sCid8+ewwEWB+m+nwEWPtrU9u69beHChUcNsPyd40eOLIYq4WFxEpu6kFWAAJ2E6mRD/9raIWSrCcdkfYGYo2jjQQ4WZotVHFwDdcwyjN8P0PmbJSlsAvN5Dd4dDT4lam3tuSTFHKD+HSCwOJUzDs5oiK61uDyEd+41TJSI/+F5O+Kjolb1xcH0NhaEkySTPjnZ2S/qDn0/fzbAQThT7IYd7BY6a1qWe1Atuc5it54nFi+BxdQOBovFUrpnkkUgh0RNBo2fAy33S9UNOzdfSB77EB3/HMZkAp+LbYBllug/gcX0gPtPNK6tqEjDmFyGttLGhVU6Xx6bjJfBK07i7TlWqcuLK4orcVkjjzDmqVEoB1oeaLsUNHsN0vTasrKyBptt1yljdO+s7Kz7kxKTRIDfCtiXyjqbBgmog1D7ow6gq0CxMiuHzogDjrAwffdisMVV1HrsBPvSaV2WJ/YYnLwnjvwUvA8G3xVA5Q3wjHochF4W43JePGzw4GEi1Qe714oxrZT3kf5u63QvjdKqZwt68HvHjWss93fvALi1kleRSp96BJb1ESoRf/SAXr8gAudyQ2JYDrfi9VANbMjOzr0b2TcvbBwypAdf2Ng1l4u2KeMwENWfV5BDPSo3MSbmDGyxbwHd1wFAnsY4/FlkJMUOqk7RiWo4DCaF0xTvHdSAc5z9zxt6J51HHO1yTUFOqN8l4pz1OLjXwkh7ocBMLLJU1k6rxaFEtvfG5VfYiV6H3dlinLN+DVKeTG9sbMwwoHkx6jDUHNYHM+N7HXa+mAvj2m1gZPfn08FjFYE+k2ciFbveeAQrwuh9FRjWzZjXNzt1/QIwsKk4S6aY3WeF+bhQ+2Wnpw/CxiAloP5z+lMcUDLUjzg586zc7OwbAA43QVV6tVBiFKSlVRj071gW3e5v7IKcVfD+5ZfAc9Q77RwXGej3x3AyOqjao1vw6W81Cltrb/Qu/12ANNTa8Wyt4wLZOv85fLys/vYY8wBn0NoZfN5lMXEmfW7/OqV3dr7+TefVbpStYQ18cjtVXa3MD0NusUHuY/DvtZnigNPCIfP2WHyfFiD9df63PbrYfyerxOQ6ZjFbfLZmsw+8z4GXwNsRfDp3fv9143bi/6ESKuYTHDVUjtIiRPtQCZVQCZVQCZVQ+b/24IAEAAAAQND/1+0IVAAAAAAAAAAAAAAAAAAAzgL4cn2g77WR0AAAAABJRU5ErkJggg==" />
                </td>
            </tr>
            <tr>
                <td style="padding:25px; background-color:#EDEDED;">
                    <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                        <tr>
                            <td style="padding:37px 47px">
                                <p>
                                    Dear ' . $data[ 'lfname' ] . ',<br/><br/>
                                    Thank you for making a reservation in "' . $this->getListingTitle( $data[ 'lposts_data' ] ) . '", unfortunately we have to refuse your request for some reason.<br/>
                                    If you want more information about this, please contact us.<br/><br/>
                                    Best Regards
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding:25px; background-color:#EDEDED;">
                    <table border="0" cellspacing="0" style="width:100%; font-family:Arial; font-size:13px;">
                        <tr>
                            <td style="text-align:center;">
                                <a href="mailto:' . $this->getSettingValue( 'email_booking' ) . '" style="color:#FFC200; text-decoration:underline;">Contact our Booking Manager</a><br/>
                                <p style="color:#C1C1C1; text-decoration:underline; margin-top:2px;">
                                    Manage your Bussiness on 
                                    <a style="color:#C1C1C1;" href="' . HT_SERVER . SITE_URL . '">NomadSurfers.com</a>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center; padding-top:20px;">
                                <a href="' . $this->getSettingValue( 'fb_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1Ni42OTMgNTYuNjkzIiBoZWlnaHQ9IjIwcHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1Ni42OTMgNTYuNjkzIiB3aWR0aD0iMjBweCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+PHBhdGggZmlsbD0iI0MxQzFDMSIgZD0iTTQwLjQzLDIxLjczOWgtNy42NDV2LTUuMDE0YzAtMS44ODMsMS4yNDgtMi4zMjIsMi4xMjctMi4zMjJjMC44NzcsMCw1LjM5NSwwLDUuMzk1LDBWNi4xMjVsLTcuNDMtMC4wMjkgIGMtOC4yNDgsMC0xMC4xMjUsNi4xNzQtMTAuMTI1LDEwLjEyNXY1LjUxOGgtNC43N3Y4LjUzaDQuNzdjMCwxMC45NDcsMCwyNC4xMzcsMCwyNC4xMzdoMTAuMDMzYzAsMCwwLTEzLjMyLDAtMjQuMTM3aDYuNzcgIEw0MC40MywyMS43Mzl6Ii8+PC9zdmc+" alt="Facebook" /></a>
                                <a href="' . $this->getSettingValue( 'ig_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDU2LjcgNTYuNyIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgNTYuNyA1Ni43IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnPg0KCTxwYXRoIGZpbGw9IiNDMUMxQzEiIGQ9Ik0yOC4yLDE2LjdjLTcsMC0xMi44LDUuNy0xMi44LDEyLjhzNS43LDEyLjgsMTIuOCwxMi44UzQxLDM2LjUsNDEsMjkuNVMzNS4yLDE2LjcsMjguMiwxNi43eiBNMjguMiwzNy43ICAgYy00LjUsMC04LjItMy43LTguMi04LjJzMy43LTguMiw4LjItOC4yczguMiwzLjcsOC4yLDguMlMzMi43LDM3LjcsMjguMiwzNy43eiIvPg0KCTxjaXJjbGUgZmlsbD0iI0MxQzFDMSIgY3g9IjQxLjUiIGN5PSIxNi40IiByPSIyLjkiLz4NCgk8cGF0aCBmaWxsPSIjQzFDMUMxIiBkPSJNNDksOC45Yy0yLjYtMi43LTYuMy00LjEtMTAuNS00LjFIMTcuOWMtOC43LDAtMTQuNSw1LjgtMTQuNSwxNC41djIwLjVjMCw0LjMsMS40LDgsNC4yLDEwLjdjMi43LDIuNiw2LjMsMy45LDEwLjQsMy45ICAgaDIwLjRjNC4zLDAsNy45LTEuNCwxMC41LTMuOWMyLjctMi42LDQuMS02LjMsNC4xLTEwLjZWMTkuM0M1MywxNS4xLDUxLjYsMTEuNSw0OSw4Ljl6IE00OC42LDM5LjljMCwzLjEtMS4xLDUuNi0yLjksNy4zICAgcy00LjMsMi42LTcuMywyLjZIMThjLTMsMC01LjUtMC45LTcuMy0yLjZDOC45LDQ1LjQsOCw0Mi45LDgsMzkuOFYxOS4zYzAtMywwLjktNS41LDIuNy03LjNjMS43LTEuNyw0LjMtMi42LDcuMy0yLjZoMjAuNiAgIGMzLDAsNS41LDAuOSw3LjMsMi43YzEuNywxLjgsMi43LDQuMywyLjcsNy4yVjM5LjlMNDguNiwzOS45eiIvPg0KPC9nPg0KPC9zdmc+" alt="Instagram" /></a>
                                <a href="' . $this->getSettingValue( 'yt_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjEwMCUiIHN0eWxlPSJmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6MjsiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHdpZHRoPSIxMDAlIiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnNlcmlmPSJodHRwOi8vd3d3LnNlcmlmLmNvbS8iIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48cGF0aCBmaWxsPSIjQzFDMUMxIiBkPSJNNTAxLjMwMywxMzIuNzY1Yy01Ljg4NywtMjIuMDMgLTIzLjIzNSwtMzkuMzc3IC00NS4yNjUsLTQ1LjI2NWMtMzkuOTMyLC0xMC43IC0yMDAuMDM4LC0xMC43IC0yMDAuMDM4LC0xMC43YzAsMCAtMTYwLjEwNywwIC0yMDAuMDM5LDEwLjdjLTIyLjAyNiw1Ljg4OCAtMzkuMzc3LDIzLjIzNSAtNDUuMjY0LDQ1LjI2NWMtMTAuNjk3LDM5LjkyOCAtMTAuNjk3LDEyMy4yMzggLTEwLjY5NywxMjMuMjM4YzAsMCAwLDgzLjMwOCAxMC42OTcsMTIzLjIzMmM1Ljg4NywyMi4wMyAyMy4yMzgsMzkuMzgyIDQ1LjI2NCw0NS4yNjljMzkuOTMyLDEwLjY5NiAyMDAuMDM5LDEwLjY5NiAyMDAuMDM5LDEwLjY5NmMwLDAgMTYwLjEwNiwwIDIwMC4wMzgsLTEwLjY5NmMyMi4wMywtNS44ODcgMzkuMzc4LC0yMy4yMzkgNDUuMjY1LC00NS4yNjljMTAuNjk2LC0zOS45MjQgMTAuNjk2LC0xMjMuMjMyIDEwLjY5NiwtMTIzLjIzMmMwLDAgMCwtODMuMzEgLTEwLjY5NiwtMTIzLjIzOFptLTI5Ni41MDYsMjAwLjAzOWwwLC0xNTMuNjAzbDEzMy4wMTksNzYuODAybC0xMzMuMDE5LDc2LjgwMVoiIHN0eWxlPSJmaWxsLXJ1bGU6bm9uemVybzsiLz48L3N2Zz4=" alt="Youtube" /></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>';

        return $content;
    }

    function bookingInvoiceMessage( $data, $agent )
    {
        $content = '
        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
            <tr>
                <td style="padding:25px; background-color:#FFFFFF; text-align:center;">
                    <img style="width:250px;" alt="NomadSurfers" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAYsAAABXCAYAAAD1a2lrAAAuWklEQVR42uzdb2hVdRzH8XP/ue3u3uvdH9e2dP91azDm1BabKQtqOlFRmyVTiqT2INAI90iy7GE9uA/S8EHSIgh6EEVJIBJB9cCCKMyKkFAzliSR2B902z339rnjd+Jw2GX3Thfaeb/gy++c6+7dFPf7nu/vd87vZwEAAAAAAAAAAAAAAAAAAAAAANweAjMBAIBH0IRXYOZ1AIDvhSyXsbGx8lQqVWmJS9gCAPjTzp07Q5Zs3br1rs7OzoM6/HTRokWXEonErzo+E41GjzY3N3eTMADA5xVFOBzui0Qi53WYdUcgEJhpg8HgVF1d3bMkDADwaaKoqalZq+aaSQ6Tam1FxoStmDLHWSWVsX/fCwDwh9WrV0dKS0vPmkpiyrSzha1IK5lk6uvr+y1h0hsA/DOhvcckg2nT5g0lipmvSSaT71FdAICPkoWqhBPuRDBH2Lm2pKTk54aGhhaqCwD4fwsogocPHw5qCOpzkwjSBSSLTC5y8xqaDO+lugAAHyQLE6eLTRaKSQXJAgB8dCfUyWKHoRQTijaGoQDAPxPc+5Qo5roTKuueBNcDe+9TVQCAPwQsGRkZqQiFQhcLuCMq4yQUVSMPkSwAwGfVRTwe36IJaycpTCpsT5KYdhLFsmXLjpAoAMCnCUNrQu0uKyv73VtNuM+XL1/+SjabDbF8OQD4OGG0tbW1qspIaQ7jnNaCmtbwVC5ZXIzFYu8sXbp0E/tcAID/BEySCJrziMKR3LhxY/vQ0FBnX19fpTupkCgAwD+Cs3X6GmIK5Pt65igAwF8ilqFbYB/RkNOH5eXlZzVf8Y3Ov9SQ07FVq1ZtYcMjAPCngNPxd3d3363mA0U2X9TW1p4YHBxs5sE7APCPsGW0trY+qoUAf3E9V5FW2E64b5NVtXFBbYcnYQQUQSa6AeDOXesplK+a0Jap8bJo2avmae1Cnti+YZYBOa0FB73Jwfv5ESa/AeA2SwieyD8prXC2TFU18a2rmrDNcUHLe+gW2hHXz7BE0aFbbKvN3t2zToaTPADgv00OYRN5HThwoFrDS21NTU0bdJp0f4b2zT6Ye2bCW00UkywUxxWO5xW2Ko4zmhA/uXjx4mPaDOkZvTbY399fo9YrTOIAgFvFe1Xu0d7eHlfTpKSwVlf1T+j4ZcW7Wqbjq4qKigl13tcTicQXeniu0pLe3t5m/dnHriex0+Z4PsnidUvMKrXjeXbRm1QFc1nHHyk5HdJ2rQ9oY6Vq73MdTJYDwPyFFBHLZXR0NHd+r4aPntZ6TG+o4z+t89/m6NwPWdLT09MYjUZ/NJ34lGvZDttEUclCn/WaJZq7CCsxfeL63LSpVqad7+GJ7/XzH6murt48Pj5e6q42SBoAUGwlYaxbt65OQznDujI/rjinl/7Ms6dE2hMZdco39LR1v9kq9UnTod9wvS9tXitmOCpt2jFLpEXvv+De28IJTwUz7a5k9HexNVz1Q1VV1XNqW0kaWGgBd3Dr3sKPlw8MDIQV/EIv8O2sqhru0xDSEXXEl2a7unddxWfybUikIaCvdeXvfOZbTmfvaa9omOopdd5OhZKZa2c8fa7d0dGxwZKWlpYHzXts0xayWdKUaZ3hqmuRkshRHXfw4B9wp68X5MECcguzcJ867i5NEr/t3KY6y7MPmUKHirSX9guW7N27t17Jx50MMmbDop+6urpatm3b1qrjQqqKjLkT6oozcd3Q0LB/PhPlJmzPfhl/qNI4unLlyiZLzJ7gXJTg5j22fXvVnh076nS7Xu1MbNpUu2vXrvpUKlVm4VYIWsa+3bsTanrX9PQMD6xf/3g0ElnjrBtEwrgFCVlWrFgxquZvz8ZCdnEdsLnKV2jo6X5LGhsbH1blMNM5u7dL1YqyL1pSWVk5bIai7AK3UP3M+b+hu57edFcq84yMJ2lc1s++35UoIhZwM79ciVjsVCIevxovL5+Ix2KXy6PRiYpk8urmoaFhS5RAWOhs/oJmUjV6T3v7S/q3Pa/TvzQ0kq1ZsiTX4VzXcMR3em2QjW/+Ye86oKQqsjbvdU/35JxjM4GJDDDDMDAwpAGGHAUFRJK6rP6uGFbWrOuaRRBXBGVd18iCi4FVdA2sq4LZVTCsa0DF9KMkCcLM9PxfNbf469zz3vR7PdMDeLrOqfN6pvu9V3Wr6n51YwVYaI6SZ9OvQU/GOAOqzSQ1vAojcgy95k9S4lDBJCUlpV58iTxQCwksmq1ILGDkt9Fzw1Hf5faKjgINRIv/s7a2tozoFHK1DZV27XjfMpp0gwYNmhUCi/aD8YIFC1LBSN5UXSFxfRX1BdRd4iQ17FpHhcCifXQeMGBAgthNt2eHzhk6QP2P4tmwL6VDcviYxq9FPh/j+hZUUAnUjpvlvVae3bVrV9/6gvG8ECopVRJq7aDaoqi19qDO7BIqodJOsHiV7ZjE4SytWCAzQ2DRfv05dqbSKOoFg1g5ZsyYfHyO7uLxhKeHh3twvsEpGzduDBkiAyxSzTJ37twq2Bc6iuF6AeJeGJ59BxNBehitSA1eqYKCrWGZbAd28A9aAAvZtv1IKNhH3IfrNFW91ZGVtwe2jEvXrFnj6hIqoRIgWLzGROBmMXmhUw+BRftp2xN1t6ArGM89RjvikL2i/WBBBujeBBaBVm6A/kzkfSJ7xJXcAA3AaIbKq788rwIBfS9bkGqkh9VWSCXp1IVrVKYejCpdfZEmvXXIkCG1XUIlVDoQLEKSRQe4byLoa4FvsWInmZ+dXST+hwjcsNDhN8ctWHiFmpBiGJYqUd7nEtM9qIDBFnJ/7jJ9+vRklA/oN6YSgpRI0tLS/i6fjfs2qN91ZKX++AAO8SK7YCcZ1iVUQiUEFscfWOCwm6U+/bTH884keJ2FpIjgggVUQfbVOcSouUQAe0Wjsk6iUJ+RYEAM/w8KmBTj3d+wdWSqEtK76FfTrZl43ocdaNzmffKS++/LDQ0N3UJnaIRKCCyO38CwWwVN4Rr5d5FmIhRTETwDN861jgWD3GZHpUOJ+1SQ+Boqpy9hr3gYn+OZ00Es0n3Mw+783+K3yCVVJxuA8R0EtZWVGI4W3aG3pmelTxP3IcVHPS4H1biNDqjNSlbbJjhP3KQEFYZ1RFpePUiTWCNm46todBDeY61/du6ldgarzbJtQaSLeT+CDBYaq7/ISHc7YEGG0b/T/7RgRd13dv+PlznBGPpJCtM8ZMaAuW0BBuw1UBmeIjSHigFYJ+aqMf7xCpj8ewL8Jd8EgESCMW9q22ZBQXxu197hw4f7VJLIXHtqR9krBOip70Z7nkPMSQ13L+ZRorzqjEHwScsP3NCDleFRlkDzsnMma/Ac3ey5/Dub32t222v7/fSOoL+H3tExYME3BFTanxHT0U7mqtuZy4zuVp7jMPmfrG7631Ki6ZPkT++kar5GA6ez08iPvtP6z1VD5r9xyHYGSVIWwXE7mOqnhQOFpmutIk9UYUXhCBaz4TrnnHNi2bMlgNTS/ZepacLJPlVvpk5i/98id/gZaRl3tdNe4eX5omg9T5UND2gd8UlWVVFRGgFrfmZqat3Afv26k6jCftu+lAZzEBE8duTIfvjYV6QABgPqedFFF8XwXZj155sXaWySA16Sn98dH/sX5OXV4TsPD+Ixa7egA87IrUDK4P7wWqgbPXp0T+w2HNbba35YivScEGmOS46IsX3Li4v7Txg1qgzvDefj1Z68OKLNo/BcuPcJj426YQMH9hLBYez5AYMFpzm9MwKiego+JuK7ZFM6UAl43IN/v8b7Tf1KO++88+xEtl9HNH20ne10MmbsqqysTBV0xlxNxZzS2/p98PtvPsbjxo1LFu1EpHPKypUrI/n8CYaEgZ1/Li53ou4zMPYeIgb9dnl5ea68ETaIBtQVUNs8j9iJzdDz/wV/nyEyzcrfwKPtKnFvaWlphQpQeI6LvKaWKgBlJsksl8+DRHJlAGDRwm0sJB39E3aTGXIecb7GS3fUB1DvokM1VqI+iIW/UP4APt3FIMINuqZtjQwP34GX/uR0OH6Ki4nZCT3d1sTE+FvhLlas7A40u0CUiuyHuPF8PHdjYkLCdjDdvSJfidvl+gmRwTuRHfFTREau9+TkzJ0zZ066xfwl8ruzUR+ivt1NUZUrUD004Pmw+v8R9T+usLAfRXZJ8d7oqKjvwtzul9xhYWdjkUUpbdZEpYlbgUjmxS63ewsm2w8O0EXD/SnJyT7a4H83KaDjtMu8YTCrweU2tO3diPDwb8Pd7p8EXfD5p+SEhB2xcXEfx0VFPZgYFTWC2mQPSIlG4jD4nKycxRjn95MTE3eg7eI9PtrDde4Th67fRykMZHnTDljwviFuIAHxA6fh41p4dvwX129FdbqcXyckJbzVNS9vWe/evfv4BUHqM+ZHAi5L8MzV0yZP7i/bYHN3eQYdnD+Ov5O/Dy6PSYKx5GZn3z9l/Pje6nMqKioq01JSrg93uTaDjiIr6KdYkB+h3o/P1QrzTgQoLxHrDrRbQWtwGeq/iaafE/NayepyWrPTeTs5nWfNmhVVXlIyHWvnYYzjFqwvYVDdHhkR8TWiwd/G5zvgaTVGUaM4rYBTPIpoR1ZGxv2gcx+1/2VlZeVpSUnX4Z2bUI/2H3NLxBRIWml8jPpXVdWLMcTaeQv3fS3nRBIC2xKjo9fVVFXNhEE6xi6w2V1zVX2rSjOyMq5z6s6PVAYucjxhrhbTz6Kx1u8yY85Yr9+B3o/17dt3ZhzWKP5+xwzcKUDvEy5hqICAOXWTcn8k6vttqK+8LBtuC2vbdlTBB+tILWZZCzLepMPPii+z0tLOA4Pa7w+5wFT29ejRY54EDKuDA8YfjwFYjN34Twb6NMN35eXkfAPm/eujOw08yw8YPWmEtBjMcniejNVY6mGTdmyFtNBbPpdoc4mVJF7hmGRor8KA/NOlHCdtgTmvwbu9VukSFR7+fHVlZZXF3ZcuP/SsrDzX7XbttrJDwU1iwUeg/ss2WFDfKkpLh8dGRX2kPregq+cHT17OF5jE+9T/5+bk/AXXhDb6pNPuLAeXXd2KilqnTplypkUa8Hmymt77B27Y4+8D4/OI33pyc1snT5gwQzLmpLi469vyUEmIi7tZAYt8gFsTuVya7CjNK+55QG0n3wD0rKgYnpqa8g67bz/mz27+fADHpu4lJf0sGDR1CibLFvdlZ2W1nnzSSaeRNBCJzdEf2mp7YmzsrUyt1KVPSUkS6Pag+ru4mNi9hQUFXyQnJuxgfObfSEMxJEiAoavPhFrJjUsjAP0m0PpdrMf/kbF0qA8rO/bDqGL+N1NW2ia+VpEp9nzZZukKLU7Uk8waO/yTTNJ+eOk53/okHyoIlJtE3/vejSrf22zCwz7BBvw+AN4sdoKeLTo2KoadJiXj4loswCuwo+Q6PC+rLUd9cpE9MTstbb7fBtB3hXl5VfGQGHiGR+Fmhnr0HeKzREp1IHKzstaD2HHsfUZM4BG65yARV3zeVti169nJSUn75OEhJv1rlv3DgO4ZOHCgz8hUWlR0p1zgBBiGbZb3QvpoheFokhET46BXXFg4HrvSHyXTMKK9xugi2kCAsQfXoew9pqqLqIiI20TktUIDM9o3Kbusp5W8NF4rYCH/HtCv31QnUlV0wRjER0dvLe3W7fS03Nz8sWPHZk0dNSoFasccbDoGJMbF3Q3gkDn/38A1w2QjopHfeaZvTPPzW6dOnjwrQLCQp45dzOaUEbMU4LQbYCbAwjeukLTXSm+SmMjIDZD4LqytqZlb3bPnbPz/94X5XV8cPXponuJIEI1dpQCa03HPXHFFPVnQl9rxDha3uHceq3NQf431KZm7ztsHEPtNmNPpGxvklvrS5XReAzVmPVQh4v3ZYFYFWD8NeP5i5Pb6kVwlmyvLy0/hgGFEbxzAI8ZjZ1ZmZuu0iROn+fofG/uQD8Qcjha88xnR/xr0H++aLYauID//X42NjYXqfMB3cZEu1+viPkj1B0q6dbsd66xnBebBrEmTUvF9BlQ8pcX5+QvBXz5UoopP8wdsHWm/I81COGkiLmXGcH92AQHO5epc69+/fwwM1RsRiV0u36EAUBOXLoj/rFZtIVgf6wxAQcy/fRifrzGe/xB2EmTTHYQ2M4AIzEFplMlO5qBETolcxGh91UBckvc3A7kG+hOPSRzdRfeqRCfGRH/LNuDd8n9gSj4mLA0zwv2NLxr2/kcNBmG/2kdFp2fcPwq6waJ7NCE29n9Yqgsvu7fZhDY7kYumG2PYvK0TUQ+p7xSVAJTTRTBotU+H6be7KoqLe3Oa8HdBpXidvI8FBXl16o8EKwlYrG+8LRwsuJtidnRkpC8PDxjdqqvIFsKLskDrcfExiCi3+0U8O9wsihnMIwuXrwRYTJsyZXaAYPEXZogMawMscoWaTjDLOaeeOjwzPf0Wn7trbOzLws7WTmYlx2WdnZtkf/M9ngXQBvjGBYx2w4QRI3Lauq8Bc7Kga9eNXWgX3K2goK2cU7oCFnvT09JaoR0YCXXUdSQ5bYa0NNAqzRFLci/19UOkx65p6waxztOxO6Zo5laoeAYr/Q5G0bjTxLBhw3LBjHdazMkkc0FtkLST6wI0qqd1sEa+DOCRhzX0o4E6yiv/LioqGiN/D61IKcbhDWyUns2BBA5QEHEYp0ECqhMaGyPea2K4DhwsuFHEHzFklYyrKD//NQPjmTC1O0hfXQqd+065mzXKR6JBwsGE34Nn7pHvkUyTM3BPXu4LzFbCmcA69nwzn2m/qgBIWocxWVqUQbR2L0k0ifHxfzbYtTrIEDYFO71D/LliDFgI/m5hu8Du7cizpSQgvifAgMT0BhnYNVH5GNT36zdRo3YaALOXdLSiv60a6xfzC7cCFmFkmLuIVB7vjSAbUDW+kzsc5h7spkVVHIustsT4liv0MgeLSZMCBQvJuC71BxakEjgAZrd/3JgxVwuALy8rexnzkDYupt5LmoHEJasEw9ukNxTRIYw/j3sbSYnLk51dC7uEb+5Avfg41uFRtYfSBl2hcxjdHwlbwRPiPkib28s9nnSlv2ZgsR82m4OTxo0T/T9QVlz8Gp4T77f/9Iz8rKxu2JT4znqAfWo83edG1fmcqFbGoqAg/yHSZHwNlVRaJwWNOSkn0yKsQatuq00UL3MBY9ZdwENWSUkA6+Jk+RKovBZiHRk9v5mC/t6WB2BJiYfUZUZFN/NYCwZYyCCN77Fbug8oeBX0XpdjN7EMHfrCiAGT2kYM/mA5ibkuFd9LMfsQJwZ+9JlT1y/BqhkCdUQviF69RBuBnjeDQR5Qd+rqMzB5L5cDYgMsvBJ8IIp/UFRQcCd2LJdAD7sUIvGnbRmQ1OehvTuA6sJofgl2B9dD/NtM7fTyeyJg+KquqChgE9xBJ3QtZLT3yr6C+f8HuwfRxyGoPSrLymoxUU7Kycpa4QDgMv1mEwUAzTGgiSaMmbnZOVsMaNkigQ39WVdeWnoZJucM2IjOSoHHBsZ8G40R/c4yWDjICP1XSqR2pTUVAon9KSliMW3tVVl5Cp3joAULLGxKFvsgWRwqKyk5KAyUSibRsI4IyqM4izCLC10XtIHTwz/FvTjz4h2MQbQVQ79MJTK+oSEN60DO+2sYbThYZArJKi019XD3igqxLrdBzZpsqf/UT9gMffENYbq+BfMtUY6nv3GCBBKPdfpCQnT0negbuy9oRa7RjRZtSl7iS/uhYi1SxwGquRw4SHyl/PZzrAk5dwSQbDThV4cpX9jvGJ01BfidZuqlYIJFk4bFn5KU9IhPH0xFPZ82JTX1GYN7m0hMvI4tACcN9GyuYvExGgAMgUiyWUORlrdXZlraN0zC8F2F91IPeGFIercFFly0i0tIuHEYdKfqu3rBZQ/eUP/wB6TC46EX9MAGu8VbDO49TGmGT5c04Yuw25GjErdJGhFzvZcWhWFJT0qa7Twi7XjZjuZxI1CC5DeZH7gi6YmNwHfYrY0wek8JDJFdYFQlwGi2CBaaEouznvp/jmWmSkbHwR4PU0EdW7BAyRVegZKGWCMz2D0dChZWXGQxPuPgReSbA1B3TlWAwG+RbpxYX7+Rdo758+cbMWJdSTmxV/Y/PSVlto3+h5FH1SxxL7QMG6FTj7IYgKdTqowYce1MoEDpgbrHjgoKNHpStlvOR2yA58lNneQJkFTvUjwg+wAwDvP3SFpjjH8Qp+wx78zgFw4W6mfol59VJyRNPFFdMhc8Ovyp7Ii6U83MyPDdq4qTQi0CZH5F/b32/+/aKrM1wrfaLcVWrpYASg8WRnhidl5VlZUHrwXZVgs2C9nf29VJTNVN6rJ87CB3G+nnCRBfUumj0kZ4OUBX+QabOL73g6FxdQrfcaViF+4DuESaRFQUVQJNPlp4xIi5Wuk7iLjJagAWHbhyn9oeaXdxhTkPIX5jCOuPk6pLNgLS0Qs0dk0WJQuddLLrjqgN8x5UDHW6XWZ6vICFZBxw7Xxf7HZle44BWDiIvqtIffgKNlYRttpDv8NGIRuX3bBntcLldoo6lhwsZGZcuFp/COO5nRxWTlLlzTgyx+P3DB06tEDOCWttDT6j5O2FpP5bG/ENTbSJmMnnLoDgecaDWsTagYpLrj3hmn0tz1irvhs84gE2dzsdLLgevkHZdfDiIs+A8yRxVLCA/vPts846K1qdbJgQfYTHBUdLQSh4Jk21MFlctChWSMIx4PlIiYfQ2pAsvMTsvwGKJ3Kiq4wGO6wnDOjToqFGh4cPNWmzi3YIl8v3qs+IjIp6ip6vmU1M8oqaR0FNGj3TwasENhj+fsv7KLychLsvW/CpqB9IuqlgCyOnBE5XW7vPPlVVQ6ES9NowcIdRtOrvFeeJUazPfrwz2P+PD8liF0ljt7Ox6wyw4HSJR91CEuXldqQcPsZJCQk+jQGM9kxdyCQLOGz4bFCJiSts9l9Xduq7yRPrTjYeNuZD8AuM626sZxZb5Pf8ie+hXitktKlQN3SMt7ynaFaiwJ+MYyno/dhQDpf06nywIEZIouFHmDjpbQyMg3S3tdLdVdwPhuG7v7hbty/POfPMAgY2FxoxbaELJ6nC2sHqHs8g3MMJKN7tRfCK4gVkAhZ0BcO7kxObvws2mivkPaokBKPrOxTZrJkZ1mMcjgk66MKlqKT4+BemEk14sbHj5oE9A/luBWMi9JvD2ILvqYyzyvD3o9YyepgudBynaieCW6O5kiVPJQNddsLWc5FwmTXz2uA0ON7AQjI6zJEF7PedCRY6qZu6g/5yLKcpNh+nv8okag3u16uoX/cp9OH9z5D9Rz1Htt9uX+E2K2ku1CurSwsL+xr1kcbScawSZ8LluMpfwj+++4dU/6iahobigW4x0HK0kH1juwdFSUM+hr7n720isLiW0b1zJQvJhMFkXhIncXF05141EFursbP6iYh01GAMP/nttT17FknmR4S614jxpiUnP+zPo4FFfcbBh/x77ufvxE5a+O1LAhqCBe2kyVvkt0bEZv871wBo0OaU9f7iOyAqDlXdgp3kRdWnpubzRYgPMXEDdUjHABHVDvCb7IFBGGLpChFta1CXk/fMakkPxsSHqGMASeUUDirk3rdVBFVZVXmg3MCCiAzBgt+HZGiTwpUAzPj4uG2QgB4QOm8RSS8wxWAc9OMULGQ/JsnndDZYHA1sq66eSrrvnTNmzCgKtAEUzX8HObY87Acs9kpwst9/WsuNjYllpaUviueQ7fEA1s1G2EovhW2rDzaaiYYePp2fCfgaG15QzRQLMkfOIxlbAQlqi4F00kSR1bcpv3eSI8Eq5ZkcjO49dmChMkVNe5w1xHCSnn766dWV3bvvJ7WHV3rKIOBqe7/y8qMimPDUwOBvlICkvgsBaMwYbslCtonvbOm6UHkWt1kcVokNb6uTOVPhkyQjNfV8I1XSiGENEiwcZkBa06vXMEhohzlYYGGrYKFxoHRHRo6PhaoKXxwQExTVSvZIswRkqqpMeHMsMOqPBj2qpQVP3/fo0V1RP/oFC35eQyU2DmvlzpQtms0ZaWlLIfKfAiNmJnuvTbAIfpwF1K2tQ+rrp7D3daZk4aDgyuk0D/aQJ9NFqJegXmyjXirUylBHbqb3r+a053EWsNe0NgwefDIbc1u79gsR+Q4nDiHBf2Qwtz+Do8kD4iCh5Li4ap6VtjMy5k69aqorJjbmdTteUCRBx8t5RHN0sqHNgzaw+L6WAyLU6kMpaNZrABbPM7vwsQEL1PVWwGL27NnV3Q3AIiEmZnu/qio1WlNH3Sh3o2pcBoyDN9oAC52uT/N2ixOvaqqrl9PCNQYLuofaMc6fZAGp5zwj5jpu3NgOAwvpXrxkyZL4vNzsR/guRQmMO9RGPWwFLFDOMOpPrx49NtoBi5EjRkivNstgwWmN4M0KFJG25TFITl9jVyk95KRaYhs2EvenJSR0P14M3Bws4FQhggA7HSx41P+oxsZroeP2jbvIFgC7g0iNIRwkbFVkVBCeUMJo3YqI9L/5A4tsBCWeMnVqoGDBtQnJMJRPTIiNvgObhfcjoqKk5CLTZ+8C3V+CeuxUdn+wigygGy5ijiyqoZpp7qoqPJ0cS9aYqaAwdpvVcZb3IAShGP1uNgn2EzEXBEi/ELCgcxGcsPa/KI3gKqNB0jhLujdGlOd4u0W6kaqePa9VwYKroVQDPto7IVCwgOG4w8BCTozYiKj7HLpDTb3iZX20Xs3B4kyj/oDp2ZIshjc0nG0fLMxVCchKmljWrVt9VEzMBRnp6YJJfafspPbBaDtbjsvxJFmIdB9TJk7sdLDgc23QoEEX0w50R+/Kyvlitz+wf/+ZAwcMmGG74hz1QXV1Z8Kjqo4zIg4WiNwW6VUCBwt6Pqe1UIGDnsIYPBNq/OVY2++oUjQyKaxVVJZaUAPxUtNvlLt/q15QMGxPka7LlIzVg/Wxz0iioMjsBfKdTOWeiHH91kjFjDH4NguBjQw0T3CwoImAQV/HGEwTRfTeYUfnKYLKUD41UUOd3ZYaiv12vG2woOvYsR0FFpQny+MZLNvIdy8wBgtXud3os3DFfRl1E+orVDeBnv+CrprlajIGi5Kiol8ZgQU91zJYIBjwUutqKPPzC9RoVLUgWrtrj+7dFyE7qs/jiOxgUyXNTMGioMBWug/2vHtI4rz0BAALDnSCYQvJ4IeJEyd6OvpEOVOwyMwMCCwsnM3CS1z/vn1HIQjwZWVtPELvDBpggOlH+DK8apa8oFrIs+tzeP5lsk3aInW9MW/EbzwoJkzfje/fZ2vaS/z2MG5jTiknPli4KfHVjewdLbTDf82O58u4ESNKMCAHOHMUOysEtY2WxDOKszhewQIeUktNAgdb3Q59lT8GAOMZ04cag0UdAu6MDNwYmy+gH822GkUL5rwWFwsGbvt5eFSa1kBHCcb0Cdm2vlHSO2hGYIEYjtbxY8ZYBwu6/6mnnnJXV1U9QyoEW5LFtOMALGAIbiQaNSG300QZryTaFGiVfQ0WWFgNyOR0JVD3kgp7Nvm6d3gGWso6MJTshrSO/UsVqCulVEFqZhdsdG8YZEyQGYfvM5OaBU0pWNnQyA0VGVOln+BgIUUxkdES7qocoYW+7nBDfX2lBXEqjIJ5zpaeCQww9sDbJtOP66y3nWDR3MFg4SCOxeI5KCYjMvwZNoH5UZNuunKvCQ4WkvF1Q92tsbQlAmiRIHG634yjdBYF1AJf2ck6CwcHu6cG6rJviMEZ4dD1Q5RDaAqju3yeDyxc0C0nxcbOsqvWnDlzZixUcZto8V5+woAFtWdATU2+cIgQYwCp7/wgMJBggAXp8+2rqsIoU22k2/1YkNQwDnJfPV3JytzCTtAzyjMn1ENj1Q0aVKiDCSS4zcMr7Ez5ygbXqA0Y07vlOuXqK2wSFv6iwEL+dtKkSUmarn/bxYBRZaWn38MSifESRudfhCN/zbsSpdUrzpeQbdb9RHAfP2BBbXVwGwwZ4dOTky8wDIyk/9EOsszpDDvoRw3loLGIACj4RHmNARO2MJuV3EthDJh0maupwOO5iDzm/Edwtz+gSl+2bJkIiPqPeA/E7ouNwEJIRTDKbvdFt8bFLQjABpaC+jFlXr3iBAILjZI0RoIgL/n876Oj/0qLNGAbQieBhX0jNY0J7JzX0I7/vaB5QqEsWrQoDjmvrsAG9RWecVlmZWbr7guRk4tlZFhuZtiG4XyLkDxM5wOdjKfcz2MtbvrFqKH4Lr+4qGgJMxZ5RSUjz1xGKFnDlFn1R13XDFN35OflTTZJ97HueFVDyfthsFtvNA6ISbhBgqiiHpBVnrLF3PpMwILuQRrtK4wmn1h82EkhaMi8DB8yZFBSQvzPMq7GUm4oFDgE5GFO3AjJr6eNA7I0UUXmVCTrk3rb33GwkD7sAMFX6ICcZTbAQroo9lXcj688UcBCbWdqevrVMuARu9WiAHfcuqxBAgvN10Ecs4oEkfOrevc+zw6zk+9AMOdlsGdR1HOQCx2EBFWnyEBxqWDwkAha2Nr5mWVGdlI+u0TFWcNroLLyG+sFVdN8bmBXnHQe+aUZuHVVXI6PjdnFspd6ZY56pK24ypewjhXkri/FznG1yghVAsZERTyv7Io1DhYngM3idhM10o9ZWWkNJhyiLxj3WxwoOFhwX2xM+K4ybz5PO0D2i7/hN/X4nPjm+vWR4iB6cZwkAucWQc0jD4uylkiQxj05LuEp8uBYpUiQfgvpfCNhuP1Y3A9XzXMM6O4kcf12Opnwv7DxWHUpdJFb41Kak60Cy04wsNApvXU55pgvdgWHCN1rl84Gc1nrcLCgd4gMwmK+oL3fg/ZplgGDxgS84nqah68G2SNKMzrLnDIjLMH7t6rrADFEIyXdaa3NVcMFWOaKveK0TrO+y/kE/jqhjQzRb7LbTnywYAnEFviIRYyeh7TjzOvP4HcvGPxiEPQ26CU34H+7VIKxz00whLId64kFFvCemKCAH/OucItdy18rKypuGT929FJ4hNyKxfosaH1I9scCWDhYX5fy7L8Gnmof4nSzLalJSVuxOP9XpTuq1fMsHOTdNCbMFeb7LU6PO5WrHHlRF1xJYeE4ROz7+oe8VPXKnOIBlFPkyY4pKUkSVNxtMD03ta0fJLsDsTjX3DcvTyDJgt8b4XTeIhkSAGO2Aohm9iKNxkpmZBgBF/fXADxlsr/BkCxQYuFI4FMnFxUWPkEbPbJvtmnM11auXBmGzYPYJInN5XKrQBMMTy2Riw4AUQ0D9p/gxrpYyfLrpHnyhIG9QSYZfNSs7ez//XDPQamFYSnQvxfHszLanthgwQmANL23q0eSckLyyr+TTBXGo1a8fyZfrBwsjlvXWaINpeB+nTNwlTZgmOLcDfG9ShOf+siCGkpnuYTiIE5/agQYbRjxmttzUl5WVsZKEp13IhhvmsFYObk3FJ6XDHB8h9InbGan5XEJxBXhcr15hNb6wbS05LH8N/w+iPg9kOdrP5jOU/Ck+hPt+E6MOAveN7JjYbf6Fq3Bg7AvzbHqnpqXmTkfc0y6Km+yABZ7bIIFj+Rv1I8cAyo80O4hN1Uei+PkQOfEEbE6HcoFW+eQzk6mx8Y6Be2eD+mou9E56Fhjm82OcUC/T/bTdp28SAtw4Q4lvor3HkCOulxyIukksCBPoQDBYp8BWHzFwYIv8oqyMnFwj8L8+dGqVCnJITti1cc0PTk50xnBrYBFUyeCxSF/QXly4iHl+2AYrLwGDNzL6SF296oUkgI1FmwR8nAirylY0EQnr6Y67G5+lu/jnhqUEbhFJonUpKsfAKq0uHgbs1scAYuBA03BAgUkcDwk34G+3pMO1Yk8BY0b77ERGISPb9PvD0ACGGpEc3awTB0WqGzTPkQyXyvcjoUbqQgM3Ygq1h+CrosAEBfSmfN7KWnmHQHEWRyzCG6z+S7SpGiUWZj8/leDJph21ZF0eqKkmQuXaARDjhJHCgAofL8HOH8MevULBljwvsLOdKoST/Q2NAoT582bF8N/fNWR92aKMZLrGCC/pLOBgoomJRwYn5+mDca3kMg8XHUl8j3J+UtrSq7r//rJsM1Bx2ePkxKGuNJzNnZmBPdIudgZWDwRDMmC/40I0F+BED+yHXGT2DFjAESV53/z86dfrygpqVMnSzDAIjU5+XwTsHjCAlg02MgNJY+/nCgOcmLGs8M6p4UqbUREXCi8nDBZFS8xY7Dg/UPSuFqcT/EJkx58tBfvMqI9FvhMt64vMArKazCQLLhkgxTzi910RrSoaMjmivLyu6traq7Aj84HKAhXQZ80QQfx7IY31BgLjMFJqoHTmU/6HqTA2CzUHcgHtl7MHczVQ/i/iEvYjl1tJdk8HmTHqjrbAIscCRYnTZo0uYPBYokCFk4GFpYAox6p7XGELz/Q/2N4kz2G6+2Y13dBxSii9r9UJVWk/FiDZJOZal/9gQXAclqA3lBOOoxrDLIYfybbEBHu/iIjLfWhAXV1i7Fr/w3OkbgZGfg2yMSNJGXewZhqZxaNJNlwpB55W6Yuwrwt5fZBeFJl4SL7ptYploNgyf1WJm6UFer8g8iG27czAXOMkqL6Z7paBgu4stZUHpEsDmEBHgSFBOK1xEOy6NOjBwtFNzaoocN50Pctxc7iOxhQOVEpL4wuRK5mLPoP4PV0FiZ9uLr78wMWa+VOk/ooA/om+sscS+d1SPockvSBN8aT/sCid8+ewwEWB+m+nwEWPtrU9u69beHChUcNsPyd40eOLIYq4WFxEpu6kFWAAJ2E6mRD/9raIWSrCcdkfYGYo2jjQQ4WZotVHFwDdcwyjN8P0PmbJSlsAvN5Dd4dDT4lam3tuSTFHKD+HSCwOJUzDs5oiK61uDyEd+41TJSI/+F5O+Kjolb1xcH0NhaEkySTPjnZ2S/qDn0/fzbAQThT7IYd7BY6a1qWe1Atuc5it54nFi+BxdQOBovFUrpnkkUgh0RNBo2fAy33S9UNOzdfSB77EB3/HMZkAp+LbYBllug/gcX0gPtPNK6tqEjDmFyGttLGhVU6Xx6bjJfBK07i7TlWqcuLK4orcVkjjzDmqVEoB1oeaLsUNHsN0vTasrKyBptt1yljdO+s7Kz7kxKTRIDfCtiXyjqbBgmog1D7ow6gq0CxMiuHzogDjrAwffdisMVV1HrsBPvSaV2WJ/YYnLwnjvwUvA8G3xVA5Q3wjHochF4W43JePGzw4GEi1Qe714oxrZT3kf5u63QvjdKqZwt68HvHjWss93fvALi1kleRSp96BJb1ESoRf/SAXr8gAudyQ2JYDrfi9VANbMjOzr0b2TcvbBwypAdf2Ng1l4u2KeMwENWfV5BDPSo3MSbmDGyxbwHd1wFAnsY4/FlkJMUOqk7RiWo4DCaF0xTvHdSAc5z9zxt6J51HHO1yTUFOqN8l4pz1OLjXwkh7ocBMLLJU1k6rxaFEtvfG5VfYiV6H3dlinLN+DVKeTG9sbMwwoHkx6jDUHNYHM+N7HXa+mAvj2m1gZPfn08FjFYE+k2ciFbveeAQrwuh9FRjWzZjXNzt1/QIwsKk4S6aY3WeF+bhQ+2Wnpw/CxiAloP5z+lMcUDLUjzg586zc7OwbAA43QVV6tVBiFKSlVRj071gW3e5v7IKcVfD+5ZfAc9Q77RwXGej3x3AyOqjao1vw6W81Cltrb/Qu/12ANNTa8Wyt4wLZOv85fLys/vYY8wBn0NoZfN5lMXEmfW7/OqV3dr7+TefVbpStYQ18cjtVXa3MD0NusUHuY/DvtZnigNPCIfP2WHyfFiD9df63PbrYfyerxOQ6ZjFbfLZmsw+8z4GXwNsRfDp3fv9143bi/6ESKuYTHDVUjtIiRPtQCZVQCZVQCZVQ+b/24IAEAAAAQND/1+0IVAAAAAAAAAAAAAAAAAAAzgL4cn2g77WR0AAAAABJRU5ErkJggg==" />
                </td>
            </tr>
            <tr>
                <td style="padding:25px; background-color:#EDEDED;">
                    <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                        <tr>
                            <td style="padding:37px 47px 0">
                                <p>Dear ' . $data[ 'lfname' ] . ',</p>
                                <p>
                                    NOMAD SURFERS has the pleasure to confirm your booking for <b>"' . $this->getListingTitle( $data[ 'lposts_data' ] ) . '"</b> with our partner <b>"' . $data[ 'lposts_data' ][ 'ltitle' ] . '"</b>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:20px 47px 20px">
                                <strong style="display:block; border-bottom:1px solid #C1C1C1; padding-bottom:10px; margin-bottom:15px; color:#2FAC66;">BOOKING INFORMATION</strong>
                                <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                                    <tr>
                                        <td width="130" style="vertical-align:top;"><img src="' . $this->get_attachment_url( $data[ 'lposts_data' ][ 'accommodation_image' ], 115, 115 ) . '" /></td>
                                        <td style="vertical-align:top;">
                                            <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                                                <tr>
                                                    <th colspan="2" style="text-align:left; text-transform:uppercase;">' . $data[ 'lposts_data' ][ 'ltitle' ] . '</th>
                                                <tr>
                                                <tr>
                                                    <td style="color:#C1C1C1; padding-left:0;">CHECK-IN</td>
                                                    <td>' . date( 'F d, Y', $data[ 'lcheck_in' ] ) . '</td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#C1C1C1; padding-left:0;">CHECK-OUT</td>
                                                    <td>' . date( 'F d, Y', $data[ 'lcheck_out' ] ) . '</td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#C1C1C1; padding-left:0;">NIGHTS</td>
                                                    <td>' . $this->getNumNight( date( 'F d, Y', $data[ 'lcheck_in' ] ), date( 'F d, Y', $data[ 'lcheck_out' ] ) ) . '</td>
                                                </tr>';

                                                $content .= '
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>';

                        if( isset( $data[ 'lrooms' ] ) && !empty( $data[ 'lrooms' ] ) )
                        {
                            $content .= '
                            <tr>
                                <td style="padding:0 47px;">
                                    <table border="0" cellspacing="0" style="width:100%; font-family:Arial; font-size:13px; background:#f9f9f9;">
                                        <tr style="color:#333; font-weight:700;">
                                            <td style="padding:10px 20px; border-top:1px solid #C1C1C1;">' . $this->getRoomLabel( $data ) . '</td>
                                            <td style="padding:10px 20px; text-align:right; border-top:1px solid #C1C1C1;">' . $this->getNumberOfRooms( $data ) . '</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>';
                        }

                        if( isset( $data[ 'ltrips' ] ) && !empty( $data[ 'ltrips' ] ) )
                        {
                            $content .= '
                            <tr>
                                <td style="padding:0 47px;">
                                    <table border="0" cellspacing="0" style="width:100%; font-family:Arial; font-size:13px; background:#f9f9f9;">
                                        <tr style="color:#333; font-weight:700;">
                                            <td style="padding:10px 20px; border-top:1px solid #C1C1C1;">TRIPS</td>
                                            <td style="padding:10px 20px; text-align:right; border-top:1px solid #C1C1C1;">' . $this->getBookingTrips( $data ) . '</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>';
                        }

                        if( isset( $data[ 'laddons' ] ) && !empty( $data[ 'laddons' ] ) )
                        {
                            $content .= '
                            <tr>
                                <td style="padding:0 47px;">
                                    <table border="0" cellspacing="0" style="width:100%; font-family:Arial; font-size:13px; background:#f9f9f9;">
                                        <tr style="color:#333; font-weight:700;">
                                            <td style="padding:10px 20px; border-top:1px solid #C1C1C1;">ADD-ONS</td>
                                            <td style="padding:10px 20px; text-align:right; border-top:1px solid #C1C1C1;">' . $this->getNumberOfAddons( $data ) . '</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>';
                        }

                        if( isset( $data[ 'lguests' ] ) && !empty( $data[ 'lguests' ] ) )
                        {
                            $content .= '
                            <tr>
                                <td style="padding:20px 47px 20px">
                                    <strong style="display:block; border-bottom:1px solid #C1C1C1; padding-bottom:10px; color:#2FAC66;">GUEST INFORMATION</strong>
                                    <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                                        <tr style="background:#000000; color:#FFFFFF; font-weight:700;">
                                            <th style="padding:5px 20px; text-align:left;">FIRST NAME</th>
                                            <th style="padding:5px 20px; text-align:left;">LAST NAME</th>
                                            <th style="padding:5px 20px; text-align:right;">TYPE</th>
                                        </tr>';

                                        foreach( $data[ 'lguests' ] as $type => $guests )
                                        {
                                            foreach( $guests as $dt )
                                            {
                                                $content .= '
                                                <tr style="background:#f9f9f9;">
                                                    <td style="padding:5px 20px; text-align:left; border-bottom:1px solid #C1C1C1;">' . $dt[ 'firstname' ] . '</td>
                                                    <td style="padding:5px 20px; text-align:left; border-bottom:1px solid #C1C1C1;">' . $dt[ 'lastname' ] . '</td>
                                                    <td style="padding:5px 20px; text-align:right; border-bottom:1px solid #C1C1C1;">' . ucfirst( $type ) . '</td>
                                                </tr>';
                                            }
                                        }

                                        $content .= '
                                    </table>
                                </td>
                            </tr>';
                        }

                        $content .= '
                        <tr>
                            <td style="padding:20px 47px 37px;">
                                <table border="0" cellspacing="0" style="width:100%; font-family:Arial; font-size:13px;">
                                    <tr style="background:#000000; color:#FFFFFF; font-weight:700;">
                                        <td style="padding:10px 20px;">TOTAL</td>
                                        <td style="padding:10px 20px; text-align:right;">' . $this->get_format_price( $data[ 'ltotal' ], $data[ 'lposts_data' ][ 'lpost_id' ] ) . '</td>
                                    </tr>
                                    <tr style="background:#C1C1C1; color:#FFFFFF; font-weight:700;">
                                        <td style="padding:10px 20px;">PAID</td>
                                        <td style="padding:10px 20px; text-align:right;">' . $this->get_format_price( $data[ 'ldeposit' ], $data[ 'lposts_data' ][ 'lpost_id' ] ) . '</td>
                                    </tr>
                                    <tr style="background:#EDEDED; font-weight:700;">
                                        <td style="padding:10px 20px;">BALANCE</td>
                                        <td style="padding:10px 20px; text-align:right; color:red;">' . $this->get_format_price( $data[ 'lbalance' ], $data[ 'lposts_data' ][ 'lpost_id' ] ) . '</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:0 47px 37px; text-align:left; line-height:2;">';

                                if( isset( $data[ 'lposts_data' ][ 'payment_method' ] ) && $data[ 'lposts_data' ][ 'payment_method' ] == '0' )
                                {
                                    if( $data[ 'lposts_data' ][ 'payment_policy' ] == '0' )
                                    {
                                        $content .= '<p style="border-top:1px solid #C1C1C1; padding-top:10px;">Outstanding balance will be paid immediately after reservation. If not paid in time the reservation will automatically be cancelled.</p>';
                                    }
                                    elseif( $data[ 'lposts_data' ][ 'payment_policy' ] == '1' )
                                    {
                                        $content .= '<p style="border-top:1px solid #C1C1C1; padding-top:10px;">Outstanding balance will be paid in 2 months prior to arrival. If not paid in time the reservation will automatically be cancelled.</p>';
                                    }
                                    elseif( $data[ 'lposts_data' ][ 'payment_policy' ] == '2' )
                                    {
                                        $content .= '<p style="border-top:1px solid #C1C1C1; padding-top:10px;">Outstanding balance will be paid in 1 month prior to arrival. If not paid in time the reservation will automatically be cancelled.</p>';
                                    }
                                    elseif( $data[ 'lposts_data' ][ 'payment_policy' ] == '3' )
                                    {
                                        $content .= '<p style="border-top:1px solid #C1C1C1; padding-top:10px;">Outstanding balance will be paid in upon arrival. If not paid in time the reservation will automatically be cancelled.</p>';
                                    }

                                    if( isset( $data[ 'lposts_data' ][ 'bank_detail' ] ) && $data[ 'lposts_data' ][ 'bank_detail' ] != '' )
                                    {
                                        $content .= '<strong style="display:block; border-bottom:1px solid #C1C1C1; padding-bottom:10px; color:#2FAC66;">BANK TRANSFER DETAIL</strong>';
                                        $content .= '<p>' . nl2br( $data[ 'lposts_data' ][ 'bank_detail' ] ) . '</p>';
                                    }
                                }
                                else
                                {
                                    if( $data[ 'lposts_data' ][ 'payment_policy' ] == '0' )
                                    {
                                        $content .= '<p style="border-top:1px solid #C1C1C1; padding-top:10px;">Outstanding balance will be paid in cash immediately after reservation.</p>';
                                    }
                                    elseif( $data[ 'lposts_data' ][ 'payment_policy' ] == '1' )
                                    {
                                        $content .= '<p style="border-top:1px solid #C1C1C1; padding-top:10px;">Outstanding balance will be paid in cash 2 months prior to arrival.</p>';
                                    }
                                    elseif( $data[ 'lposts_data' ][ 'payment_policy' ] == '2' )
                                    {
                                        $content .= '<p style="border-top:1px solid #C1C1C1; padding-top:10px;">Outstanding balance will be paid in cash 1 month prior to arrival.</p>';
                                    }
                                    elseif( $data[ 'lposts_data' ][ 'payment_policy' ] == '3' )
                                    {
                                        $content .= '<p style="border-top:1px solid #C1C1C1; padding-top:10px;">Outstanding balance will be paid in cash upon arrival.</p>';
                                    }
                                }

                                $content .= '
                                <p>Please email copy of bank transfer to  <a style="color:#FFC200; text-decoration:underline;" href="mailto:' . $agent[ 'lemail' ] . '">(' . $agent[ 'lemail' ] . ')</a>.</p>
                            <td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding:25px; background-color:#EDEDED;">
                    <table border="0" cellspacing="0" style="width:100%; font-family:Arial; font-size:13px;">
                        <tr>
                            <td style="text-align:center;">
                                <a href="mailto:' . $this->getSettingValue( 'email_booking' ) . '" style="color:#FFC200; text-decoration:underline;">Contact our Booking Manager</a><br/>
                                <p style="color:#C1C1C1; text-decoration:underline; margin-top:2px;">
                                    Manage your Bussiness on 
                                    <a style="color:#C1C1C1;" href="' . HT_SERVER . SITE_URL . '">NomadSurfers.com</a>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center; padding-top:20px;">
                                <a href="' . $this->getSettingValue( 'fb_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1Ni42OTMgNTYuNjkzIiBoZWlnaHQ9IjIwcHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1Ni42OTMgNTYuNjkzIiB3aWR0aD0iMjBweCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+PHBhdGggZmlsbD0iI0MxQzFDMSIgZD0iTTQwLjQzLDIxLjczOWgtNy42NDV2LTUuMDE0YzAtMS44ODMsMS4yNDgtMi4zMjIsMi4xMjctMi4zMjJjMC44NzcsMCw1LjM5NSwwLDUuMzk1LDBWNi4xMjVsLTcuNDMtMC4wMjkgIGMtOC4yNDgsMC0xMC4xMjUsNi4xNzQtMTAuMTI1LDEwLjEyNXY1LjUxOGgtNC43N3Y4LjUzaDQuNzdjMCwxMC45NDcsMCwyNC4xMzcsMCwyNC4xMzdoMTAuMDMzYzAsMCwwLTEzLjMyLDAtMjQuMTM3aDYuNzcgIEw0MC40MywyMS43Mzl6Ii8+PC9zdmc+" alt="Facebook" /></a>
                                <a href="' . $this->getSettingValue( 'ig_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDU2LjcgNTYuNyIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgNTYuNyA1Ni43IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnPg0KCTxwYXRoIGZpbGw9IiNDMUMxQzEiIGQ9Ik0yOC4yLDE2LjdjLTcsMC0xMi44LDUuNy0xMi44LDEyLjhzNS43LDEyLjgsMTIuOCwxMi44UzQxLDM2LjUsNDEsMjkuNVMzNS4yLDE2LjcsMjguMiwxNi43eiBNMjguMiwzNy43ICAgYy00LjUsMC04LjItMy43LTguMi04LjJzMy43LTguMiw4LjItOC4yczguMiwzLjcsOC4yLDguMlMzMi43LDM3LjcsMjguMiwzNy43eiIvPg0KCTxjaXJjbGUgZmlsbD0iI0MxQzFDMSIgY3g9IjQxLjUiIGN5PSIxNi40IiByPSIyLjkiLz4NCgk8cGF0aCBmaWxsPSIjQzFDMUMxIiBkPSJNNDksOC45Yy0yLjYtMi43LTYuMy00LjEtMTAuNS00LjFIMTcuOWMtOC43LDAtMTQuNSw1LjgtMTQuNSwxNC41djIwLjVjMCw0LjMsMS40LDgsNC4yLDEwLjdjMi43LDIuNiw2LjMsMy45LDEwLjQsMy45ICAgaDIwLjRjNC4zLDAsNy45LTEuNCwxMC41LTMuOWMyLjctMi42LDQuMS02LjMsNC4xLTEwLjZWMTkuM0M1MywxNS4xLDUxLjYsMTEuNSw0OSw4Ljl6IE00OC42LDM5LjljMCwzLjEtMS4xLDUuNi0yLjksNy4zICAgcy00LjMsMi42LTcuMywyLjZIMThjLTMsMC01LjUtMC45LTcuMy0yLjZDOC45LDQ1LjQsOCw0Mi45LDgsMzkuOFYxOS4zYzAtMywwLjktNS41LDIuNy03LjNjMS43LTEuNyw0LjMtMi42LDcuMy0yLjZoMjAuNiAgIGMzLDAsNS41LDAuOSw3LjMsMi43YzEuNywxLjgsMi43LDQuMywyLjcsNy4yVjM5LjlMNDguNiwzOS45eiIvPg0KPC9nPg0KPC9zdmc+" alt="Instagram" /></a>
                                <a href="' . $this->getSettingValue( 'yt_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjEwMCUiIHN0eWxlPSJmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6MjsiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHdpZHRoPSIxMDAlIiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnNlcmlmPSJodHRwOi8vd3d3LnNlcmlmLmNvbS8iIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48cGF0aCBmaWxsPSIjQzFDMUMxIiBkPSJNNTAxLjMwMywxMzIuNzY1Yy01Ljg4NywtMjIuMDMgLTIzLjIzNSwtMzkuMzc3IC00NS4yNjUsLTQ1LjI2NWMtMzkuOTMyLC0xMC43IC0yMDAuMDM4LC0xMC43IC0yMDAuMDM4LC0xMC43YzAsMCAtMTYwLjEwNywwIC0yMDAuMDM5LDEwLjdjLTIyLjAyNiw1Ljg4OCAtMzkuMzc3LDIzLjIzNSAtNDUuMjY0LDQ1LjI2NWMtMTAuNjk3LDM5LjkyOCAtMTAuNjk3LDEyMy4yMzggLTEwLjY5NywxMjMuMjM4YzAsMCAwLDgzLjMwOCAxMC42OTcsMTIzLjIzMmM1Ljg4NywyMi4wMyAyMy4yMzgsMzkuMzgyIDQ1LjI2NCw0NS4yNjljMzkuOTMyLDEwLjY5NiAyMDAuMDM5LDEwLjY5NiAyMDAuMDM5LDEwLjY5NmMwLDAgMTYwLjEwNiwwIDIwMC4wMzgsLTEwLjY5NmMyMi4wMywtNS44ODcgMzkuMzc4LC0yMy4yMzkgNDUuMjY1LC00NS4yNjljMTAuNjk2LC0zOS45MjQgMTAuNjk2LC0xMjMuMjMyIDEwLjY5NiwtMTIzLjIzMmMwLDAgMCwtODMuMzEgLTEwLjY5NiwtMTIzLjIzOFptLTI5Ni41MDYsMjAwLjAzOWwwLC0xNTMuNjAzbDEzMy4wMTksNzYuODAybC0xMzMuMDE5LDc2LjgwMVoiIHN0eWxlPSJmaWxsLXJ1bGU6bm9uemVybzsiLz48L3N2Zz4=" alt="Youtube" /></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>';

        return $content;
    }

    function bookingPaidMessage( $data, $agent )
    {
        $content = '
        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
            <tr>
                <td style="padding:25px; background-color:#FFFFFF; text-align:center;">
                    <img style="width:250px;" alt="NomadSurfers" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAYsAAABXCAYAAAD1a2lrAAAuWklEQVR42uzdb2hVdRzH8XP/ue3u3uvdH9e2dP91azDm1BabKQtqOlFRmyVTiqT2INAI90iy7GE9uA/S8EHSIgh6EEVJIBJB9cCCKMyKkFAzliSR2B902z339rnjd+Jw2GX3Thfaeb/gy++c6+7dFPf7nu/vd87vZwEAAAAAAAAAAAAAAAAAAAAAANweAjMBAIBH0IRXYOZ1AIDvhSyXsbGx8lQqVWmJS9gCAPjTzp07Q5Zs3br1rs7OzoM6/HTRokWXEonErzo+E41GjzY3N3eTMADA5xVFOBzui0Qi53WYdUcgEJhpg8HgVF1d3bMkDADwaaKoqalZq+aaSQ6Tam1FxoStmDLHWSWVsX/fCwDwh9WrV0dKS0vPmkpiyrSzha1IK5lk6uvr+y1h0hsA/DOhvcckg2nT5g0lipmvSSaT71FdAICPkoWqhBPuRDBH2Lm2pKTk54aGhhaqCwD4fwsogocPHw5qCOpzkwjSBSSLTC5y8xqaDO+lugAAHyQLE6eLTRaKSQXJAgB8dCfUyWKHoRQTijaGoQDAPxPc+5Qo5roTKuueBNcDe+9TVQCAPwQsGRkZqQiFQhcLuCMq4yQUVSMPkSwAwGfVRTwe36IJaycpTCpsT5KYdhLFsmXLjpAoAMCnCUNrQu0uKyv73VtNuM+XL1/+SjabDbF8OQD4OGG0tbW1qspIaQ7jnNaCmtbwVC5ZXIzFYu8sXbp0E/tcAID/BEySCJrziMKR3LhxY/vQ0FBnX19fpTupkCgAwD+Cs3X6GmIK5Pt65igAwF8ilqFbYB/RkNOH5eXlZzVf8Y3Ov9SQ07FVq1ZtYcMjAPCngNPxd3d3363mA0U2X9TW1p4YHBxs5sE7APCPsGW0trY+qoUAf3E9V5FW2E64b5NVtXFBbYcnYQQUQSa6AeDOXesplK+a0Jap8bJo2avmae1Cnti+YZYBOa0FB73Jwfv5ESa/AeA2SwieyD8prXC2TFU18a2rmrDNcUHLe+gW2hHXz7BE0aFbbKvN3t2zToaTPADgv00OYRN5HThwoFrDS21NTU0bdJp0f4b2zT6Ye2bCW00UkywUxxWO5xW2Ko4zmhA/uXjx4mPaDOkZvTbY399fo9YrTOIAgFvFe1Xu0d7eHlfTpKSwVlf1T+j4ZcW7Wqbjq4qKigl13tcTicQXeniu0pLe3t5m/dnHriex0+Z4PsnidUvMKrXjeXbRm1QFc1nHHyk5HdJ2rQ9oY6Vq73MdTJYDwPyFFBHLZXR0NHd+r4aPntZ6TG+o4z+t89/m6NwPWdLT09MYjUZ/NJ34lGvZDttEUclCn/WaJZq7CCsxfeL63LSpVqad7+GJ7/XzH6murt48Pj5e6q42SBoAUGwlYaxbt65OQznDujI/rjinl/7Ms6dE2hMZdco39LR1v9kq9UnTod9wvS9tXitmOCpt2jFLpEXvv+De28IJTwUz7a5k9HexNVz1Q1VV1XNqW0kaWGgBd3Dr3sKPlw8MDIQV/EIv8O2sqhru0xDSEXXEl2a7unddxWfybUikIaCvdeXvfOZbTmfvaa9omOopdd5OhZKZa2c8fa7d0dGxwZKWlpYHzXts0xayWdKUaZ3hqmuRkshRHXfw4B9wp68X5MECcguzcJ867i5NEr/t3KY6y7MPmUKHirSX9guW7N27t17Jx50MMmbDop+6urpatm3b1qrjQqqKjLkT6oozcd3Q0LB/PhPlJmzPfhl/qNI4unLlyiZLzJ7gXJTg5j22fXvVnh076nS7Xu1MbNpUu2vXrvpUKlVm4VYIWsa+3bsTanrX9PQMD6xf/3g0ElnjrBtEwrgFCVlWrFgxquZvz8ZCdnEdsLnKV2jo6X5LGhsbH1blMNM5u7dL1YqyL1pSWVk5bIai7AK3UP3M+b+hu57edFcq84yMJ2lc1s++35UoIhZwM79ciVjsVCIevxovL5+Ix2KXy6PRiYpk8urmoaFhS5RAWOhs/oJmUjV6T3v7S/q3Pa/TvzQ0kq1ZsiTX4VzXcMR3em2QjW/+Ye86oKQqsjbvdU/35JxjM4GJDDDDMDAwpAGGHAUFRJK6rP6uGFbWrOuaRRBXBGVd18iCi4FVdA2sq4LZVTCsa0DF9KMkCcLM9PxfNbf469zz3vR7PdMDeLrOqfN6pvu9V3Wr6n51YwVYaI6SZ9OvQU/GOAOqzSQ1vAojcgy95k9S4lDBJCUlpV58iTxQCwksmq1ILGDkt9Fzw1Hf5faKjgINRIv/s7a2tozoFHK1DZV27XjfMpp0gwYNmhUCi/aD8YIFC1LBSN5UXSFxfRX1BdRd4iQ17FpHhcCifXQeMGBAgthNt2eHzhk6QP2P4tmwL6VDcviYxq9FPh/j+hZUUAnUjpvlvVae3bVrV9/6gvG8ECopVRJq7aDaoqi19qDO7BIqodJOsHiV7ZjE4SytWCAzQ2DRfv05dqbSKOoFg1g5ZsyYfHyO7uLxhKeHh3twvsEpGzduDBkiAyxSzTJ37twq2Bc6iuF6AeJeGJ59BxNBehitSA1eqYKCrWGZbAd28A9aAAvZtv1IKNhH3IfrNFW91ZGVtwe2jEvXrFnj6hIqoRIgWLzGROBmMXmhUw+BRftp2xN1t6ArGM89RjvikL2i/WBBBujeBBaBVm6A/kzkfSJ7xJXcAA3AaIbKq788rwIBfS9bkGqkh9VWSCXp1IVrVKYejCpdfZEmvXXIkCG1XUIlVDoQLEKSRQe4byLoa4FvsWInmZ+dXST+hwjcsNDhN8ctWHiFmpBiGJYqUd7nEtM9qIDBFnJ/7jJ9+vRklA/oN6YSgpRI0tLS/i6fjfs2qN91ZKX++AAO8SK7YCcZ1iVUQiUEFscfWOCwm6U+/bTH884keJ2FpIjgggVUQfbVOcSouUQAe0Wjsk6iUJ+RYEAM/w8KmBTj3d+wdWSqEtK76FfTrZl43ocdaNzmffKS++/LDQ0N3UJnaIRKCCyO38CwWwVN4Rr5d5FmIhRTETwDN861jgWD3GZHpUOJ+1SQ+Boqpy9hr3gYn+OZ00Es0n3Mw+783+K3yCVVJxuA8R0EtZWVGI4W3aG3pmelTxP3IcVHPS4H1biNDqjNSlbbJjhP3KQEFYZ1RFpePUiTWCNm46todBDeY61/du6ldgarzbJtQaSLeT+CDBYaq7/ISHc7YEGG0b/T/7RgRd13dv+PlznBGPpJCtM8ZMaAuW0BBuw1UBmeIjSHigFYJ+aqMf7xCpj8ewL8Jd8EgESCMW9q22ZBQXxu197hw4f7VJLIXHtqR9krBOip70Z7nkPMSQ13L+ZRorzqjEHwScsP3NCDleFRlkDzsnMma/Ac3ey5/Dub32t222v7/fSOoL+H3tExYME3BFTanxHT0U7mqtuZy4zuVp7jMPmfrG7631Ki6ZPkT++kar5GA6ez08iPvtP6z1VD5r9xyHYGSVIWwXE7mOqnhQOFpmutIk9UYUXhCBaz4TrnnHNi2bMlgNTS/ZepacLJPlVvpk5i/98id/gZaRl3tdNe4eX5omg9T5UND2gd8UlWVVFRGgFrfmZqat3Afv26k6jCftu+lAZzEBE8duTIfvjYV6QABgPqedFFF8XwXZj155sXaWySA16Sn98dH/sX5OXV4TsPD+Ixa7egA87IrUDK4P7wWqgbPXp0T+w2HNbba35YivScEGmOS46IsX3Li4v7Txg1qgzvDefj1Z68OKLNo/BcuPcJj426YQMH9hLBYez5AYMFpzm9MwKiego+JuK7ZFM6UAl43IN/v8b7Tf1KO++88+xEtl9HNH20ne10MmbsqqysTBV0xlxNxZzS2/p98PtvPsbjxo1LFu1EpHPKypUrI/n8CYaEgZ1/Li53ou4zMPYeIgb9dnl5ea68ETaIBtQVUNs8j9iJzdDz/wV/nyEyzcrfwKPtKnFvaWlphQpQeI6LvKaWKgBlJsksl8+DRHJlAGDRwm0sJB39E3aTGXIecb7GS3fUB1DvokM1VqI+iIW/UP4APt3FIMINuqZtjQwP34GX/uR0OH6Ki4nZCT3d1sTE+FvhLlas7A40u0CUiuyHuPF8PHdjYkLCdjDdvSJfidvl+gmRwTuRHfFTREau9+TkzJ0zZ066xfwl8ruzUR+ivt1NUZUrUD004Pmw+v8R9T+usLAfRXZJ8d7oqKjvwtzul9xhYWdjkUUpbdZEpYlbgUjmxS63ewsm2w8O0EXD/SnJyT7a4H83KaDjtMu8YTCrweU2tO3diPDwb8Pd7p8EXfD5p+SEhB2xcXEfx0VFPZgYFTWC2mQPSIlG4jD4nKycxRjn95MTE3eg7eI9PtrDde4Th67fRykMZHnTDljwviFuIAHxA6fh41p4dvwX129FdbqcXyckJbzVNS9vWe/evfv4BUHqM+ZHAi5L8MzV0yZP7i/bYHN3eQYdnD+Ov5O/Dy6PSYKx5GZn3z9l/Pje6nMqKioq01JSrg93uTaDjiIr6KdYkB+h3o/P1QrzTgQoLxHrDrRbQWtwGeq/iaafE/NayepyWrPTeTs5nWfNmhVVXlIyHWvnYYzjFqwvYVDdHhkR8TWiwd/G5zvgaTVGUaM4rYBTPIpoR1ZGxv2gcx+1/2VlZeVpSUnX4Z2bUI/2H3NLxBRIWml8jPpXVdWLMcTaeQv3fS3nRBIC2xKjo9fVVFXNhEE6xi6w2V1zVX2rSjOyMq5z6s6PVAYucjxhrhbTz6Kx1u8yY85Yr9+B3o/17dt3ZhzWKP5+xwzcKUDvEy5hqICAOXWTcn8k6vttqK+8LBtuC2vbdlTBB+tILWZZCzLepMPPii+z0tLOA4Pa7w+5wFT29ejRY54EDKuDA8YfjwFYjN34Twb6NMN35eXkfAPm/eujOw08yw8YPWmEtBjMcniejNVY6mGTdmyFtNBbPpdoc4mVJF7hmGRor8KA/NOlHCdtgTmvwbu9VukSFR7+fHVlZZXF3ZcuP/SsrDzX7XbttrJDwU1iwUeg/ss2WFDfKkpLh8dGRX2kPregq+cHT17OF5jE+9T/5+bk/AXXhDb6pNPuLAeXXd2KilqnTplypkUa8Hmymt77B27Y4+8D4/OI33pyc1snT5gwQzLmpLi469vyUEmIi7tZAYt8gFsTuVya7CjNK+55QG0n3wD0rKgYnpqa8g67bz/mz27+fADHpu4lJf0sGDR1CibLFvdlZ2W1nnzSSaeRNBCJzdEf2mp7YmzsrUyt1KVPSUkS6Pag+ru4mNi9hQUFXyQnJuxgfObfSEMxJEiAoavPhFrJjUsjAP0m0PpdrMf/kbF0qA8rO/bDqGL+N1NW2ia+VpEp9nzZZukKLU7Uk8waO/yTTNJ+eOk53/okHyoIlJtE3/vejSrf22zCwz7BBvw+AN4sdoKeLTo2KoadJiXj4loswCuwo+Q6PC+rLUd9cpE9MTstbb7fBtB3hXl5VfGQGHiGR+Fmhnr0HeKzREp1IHKzstaD2HHsfUZM4BG65yARV3zeVti169nJSUn75OEhJv1rlv3DgO4ZOHCgz8hUWlR0p1zgBBiGbZb3QvpoheFokhET46BXXFg4HrvSHyXTMKK9xugi2kCAsQfXoew9pqqLqIiI20TktUIDM9o3Kbusp5W8NF4rYCH/HtCv31QnUlV0wRjER0dvLe3W7fS03Nz8sWPHZk0dNSoFasccbDoGJMbF3Q3gkDn/38A1w2QjopHfeaZvTPPzW6dOnjwrQLCQp45dzOaUEbMU4LQbYCbAwjeukLTXSm+SmMjIDZD4LqytqZlb3bPnbPz/94X5XV8cPXponuJIEI1dpQCa03HPXHFFPVnQl9rxDha3uHceq3NQf431KZm7ztsHEPtNmNPpGxvklvrS5XReAzVmPVQh4v3ZYFYFWD8NeP5i5Pb6kVwlmyvLy0/hgGFEbxzAI8ZjZ1ZmZuu0iROn+fofG/uQD8Qcjha88xnR/xr0H++aLYauID//X42NjYXqfMB3cZEu1+viPkj1B0q6dbsd66xnBebBrEmTUvF9BlQ8pcX5+QvBXz5UoopP8wdsHWm/I81COGkiLmXGcH92AQHO5epc69+/fwwM1RsRiV0u36EAUBOXLoj/rFZtIVgf6wxAQcy/fRifrzGe/xB2EmTTHYQ2M4AIzEFplMlO5qBETolcxGh91UBckvc3A7kG+hOPSRzdRfeqRCfGRH/LNuDd8n9gSj4mLA0zwv2NLxr2/kcNBmG/2kdFp2fcPwq6waJ7NCE29n9Yqgsvu7fZhDY7kYumG2PYvK0TUQ+p7xSVAJTTRTBotU+H6be7KoqLe3Oa8HdBpXidvI8FBXl16o8EKwlYrG+8LRwsuJtidnRkpC8PDxjdqqvIFsKLskDrcfExiCi3+0U8O9wsihnMIwuXrwRYTJsyZXaAYPEXZogMawMscoWaTjDLOaeeOjwzPf0Wn7trbOzLws7WTmYlx2WdnZtkf/M9ngXQBvjGBYx2w4QRI3Lauq8Bc7Kga9eNXWgX3K2goK2cU7oCFnvT09JaoR0YCXXUdSQ5bYa0NNAqzRFLci/19UOkx65p6waxztOxO6Zo5laoeAYr/Q5G0bjTxLBhw3LBjHdazMkkc0FtkLST6wI0qqd1sEa+DOCRhzX0o4E6yiv/LioqGiN/D61IKcbhDWyUns2BBA5QEHEYp0ECqhMaGyPea2K4DhwsuFHEHzFklYyrKD//NQPjmTC1O0hfXQqd+065mzXKR6JBwsGE34Nn7pHvkUyTM3BPXu4LzFbCmcA69nwzn2m/qgBIWocxWVqUQbR2L0k0ifHxfzbYtTrIEDYFO71D/LliDFgI/m5hu8Du7cizpSQgvifAgMT0BhnYNVH5GNT36zdRo3YaALOXdLSiv60a6xfzC7cCFmFkmLuIVB7vjSAbUDW+kzsc5h7spkVVHIustsT4liv0MgeLSZMCBQvJuC71BxakEjgAZrd/3JgxVwuALy8rexnzkDYupt5LmoHEJasEw9ukNxTRIYw/j3sbSYnLk51dC7uEb+5Avfg41uFRtYfSBl2hcxjdHwlbwRPiPkib28s9nnSlv2ZgsR82m4OTxo0T/T9QVlz8Gp4T77f/9Iz8rKxu2JT4znqAfWo83edG1fmcqFbGoqAg/yHSZHwNlVRaJwWNOSkn0yKsQatuq00UL3MBY9ZdwENWSUkA6+Jk+RKovBZiHRk9v5mC/t6WB2BJiYfUZUZFN/NYCwZYyCCN77Fbug8oeBX0XpdjN7EMHfrCiAGT2kYM/mA5ibkuFd9LMfsQJwZ+9JlT1y/BqhkCdUQviF69RBuBnjeDQR5Qd+rqMzB5L5cDYgMsvBJ8IIp/UFRQcCd2LJdAD7sUIvGnbRmQ1OehvTuA6sJofgl2B9dD/NtM7fTyeyJg+KquqChgE9xBJ3QtZLT3yr6C+f8HuwfRxyGoPSrLymoxUU7Kycpa4QDgMv1mEwUAzTGgiSaMmbnZOVsMaNkigQ39WVdeWnoZJucM2IjOSoHHBsZ8G40R/c4yWDjICP1XSqR2pTUVAon9KSliMW3tVVl5Cp3joAULLGxKFvsgWRwqKyk5KAyUSibRsI4IyqM4izCLC10XtIHTwz/FvTjz4h2MQbQVQ79MJTK+oSEN60DO+2sYbThYZArJKi019XD3igqxLrdBzZpsqf/UT9gMffENYbq+BfMtUY6nv3GCBBKPdfpCQnT0negbuy9oRa7RjRZtSl7iS/uhYi1SxwGquRw4SHyl/PZzrAk5dwSQbDThV4cpX9jvGJ01BfidZuqlYIJFk4bFn5KU9IhPH0xFPZ82JTX1GYN7m0hMvI4tACcN9GyuYvExGgAMgUiyWUORlrdXZlraN0zC8F2F91IPeGFIercFFly0i0tIuHEYdKfqu3rBZQ/eUP/wB6TC46EX9MAGu8VbDO49TGmGT5c04Yuw25GjErdJGhFzvZcWhWFJT0qa7Twi7XjZjuZxI1CC5DeZH7gi6YmNwHfYrY0wek8JDJFdYFQlwGi2CBaaEouznvp/jmWmSkbHwR4PU0EdW7BAyRVegZKGWCMz2D0dChZWXGQxPuPgReSbA1B3TlWAwG+RbpxYX7+Rdo758+cbMWJdSTmxV/Y/PSVlto3+h5FH1SxxL7QMG6FTj7IYgKdTqowYce1MoEDpgbrHjgoKNHpStlvOR2yA58lNneQJkFTvUjwg+wAwDvP3SFpjjH8Qp+wx78zgFw4W6mfol59VJyRNPFFdMhc8Ovyp7Ii6U83MyPDdq4qTQi0CZH5F/b32/+/aKrM1wrfaLcVWrpYASg8WRnhidl5VlZUHrwXZVgs2C9nf29VJTNVN6rJ87CB3G+nnCRBfUumj0kZ4OUBX+QabOL73g6FxdQrfcaViF+4DuESaRFQUVQJNPlp4xIi5Wuk7iLjJagAWHbhyn9oeaXdxhTkPIX5jCOuPk6pLNgLS0Qs0dk0WJQuddLLrjqgN8x5UDHW6XWZ6vICFZBxw7Xxf7HZle44BWDiIvqtIffgKNlYRttpDv8NGIRuX3bBntcLldoo6lhwsZGZcuFp/COO5nRxWTlLlzTgyx+P3DB06tEDOCWttDT6j5O2FpP5bG/ENTbSJmMnnLoDgecaDWsTagYpLrj3hmn0tz1irvhs84gE2dzsdLLgevkHZdfDiIs+A8yRxVLCA/vPts846K1qdbJgQfYTHBUdLQSh4Jk21MFlctChWSMIx4PlIiYfQ2pAsvMTsvwGKJ3Kiq4wGO6wnDOjToqFGh4cPNWmzi3YIl8v3qs+IjIp6ip6vmU1M8oqaR0FNGj3TwasENhj+fsv7KLychLsvW/CpqB9IuqlgCyOnBE5XW7vPPlVVQ6ES9NowcIdRtOrvFeeJUazPfrwz2P+PD8liF0ljt7Ox6wyw4HSJR91CEuXldqQcPsZJCQk+jQGM9kxdyCQLOGz4bFCJiSts9l9Xduq7yRPrTjYeNuZD8AuM626sZxZb5Pf8ie+hXitktKlQN3SMt7ynaFaiwJ+MYyno/dhQDpf06nywIEZIouFHmDjpbQyMg3S3tdLdVdwPhuG7v7hbty/POfPMAgY2FxoxbaELJ6nC2sHqHs8g3MMJKN7tRfCK4gVkAhZ0BcO7kxObvws2mivkPaokBKPrOxTZrJkZ1mMcjgk66MKlqKT4+BemEk14sbHj5oE9A/luBWMi9JvD2ILvqYyzyvD3o9YyepgudBynaieCW6O5kiVPJQNddsLWc5FwmTXz2uA0ON7AQjI6zJEF7PedCRY6qZu6g/5yLKcpNh+nv8okag3u16uoX/cp9OH9z5D9Rz1Htt9uX+E2K2ku1CurSwsL+xr1kcbScawSZ8LluMpfwj+++4dU/6iahobigW4x0HK0kH1juwdFSUM+hr7n720isLiW0b1zJQvJhMFkXhIncXF05141EFursbP6iYh01GAMP/nttT17FknmR4S614jxpiUnP+zPo4FFfcbBh/x77ufvxE5a+O1LAhqCBe2kyVvkt0bEZv871wBo0OaU9f7iOyAqDlXdgp3kRdWnpubzRYgPMXEDdUjHABHVDvCb7IFBGGLpChFta1CXk/fMakkPxsSHqGMASeUUDirk3rdVBFVZVXmg3MCCiAzBgt+HZGiTwpUAzPj4uG2QgB4QOm8RSS8wxWAc9OMULGQ/JsnndDZYHA1sq66eSrrvnTNmzCgKtAEUzX8HObY87Acs9kpwst9/WsuNjYllpaUviueQ7fEA1s1G2EovhW2rDzaaiYYePp2fCfgaG15QzRQLMkfOIxlbAQlqi4F00kSR1bcpv3eSI8Eq5ZkcjO49dmChMkVNe5w1xHCSnn766dWV3bvvJ7WHV3rKIOBqe7/y8qMimPDUwOBvlICkvgsBaMwYbslCtonvbOm6UHkWt1kcVokNb6uTOVPhkyQjNfV8I1XSiGENEiwcZkBa06vXMEhohzlYYGGrYKFxoHRHRo6PhaoKXxwQExTVSvZIswRkqqpMeHMsMOqPBj2qpQVP3/fo0V1RP/oFC35eQyU2DmvlzpQtms0ZaWlLIfKfAiNmJnuvTbAIfpwF1K2tQ+rrp7D3daZk4aDgyuk0D/aQJ9NFqJegXmyjXirUylBHbqb3r+a053EWsNe0NgwefDIbc1u79gsR+Q4nDiHBf2Qwtz+Do8kD4iCh5Li4ap6VtjMy5k69aqorJjbmdTteUCRBx8t5RHN0sqHNgzaw+L6WAyLU6kMpaNZrABbPM7vwsQEL1PVWwGL27NnV3Q3AIiEmZnu/qio1WlNH3Sh3o2pcBoyDN9oAC52uT/N2ixOvaqqrl9PCNQYLuofaMc6fZAGp5zwj5jpu3NgOAwvpXrxkyZL4vNzsR/guRQmMO9RGPWwFLFDOMOpPrx49NtoBi5EjRkivNstgwWmN4M0KFJG25TFITl9jVyk95KRaYhs2EvenJSR0P14M3Bws4FQhggA7HSx41P+oxsZroeP2jbvIFgC7g0iNIRwkbFVkVBCeUMJo3YqI9L/5A4tsBCWeMnVqoGDBtQnJMJRPTIiNvgObhfcjoqKk5CLTZ+8C3V+CeuxUdn+wigygGy5ijiyqoZpp7qoqPJ0cS9aYqaAwdpvVcZb3IAShGP1uNgn2EzEXBEi/ELCgcxGcsPa/KI3gKqNB0jhLujdGlOd4u0W6kaqePa9VwYKroVQDPto7IVCwgOG4w8BCTozYiKj7HLpDTb3iZX20Xs3B4kyj/oDp2ZIshjc0nG0fLMxVCchKmljWrVt9VEzMBRnp6YJJfafspPbBaDtbjsvxJFmIdB9TJk7sdLDgc23QoEEX0w50R+/Kyvlitz+wf/+ZAwcMmGG74hz1QXV1Z8Kjqo4zIg4WiNwW6VUCBwt6Pqe1UIGDnsIYPBNq/OVY2++oUjQyKaxVVJZaUAPxUtNvlLt/q15QMGxPka7LlIzVg/Wxz0iioMjsBfKdTOWeiHH91kjFjDH4NguBjQw0T3CwoImAQV/HGEwTRfTeYUfnKYLKUD41UUOd3ZYaiv12vG2woOvYsR0FFpQny+MZLNvIdy8wBgtXud3os3DFfRl1E+orVDeBnv+CrprlajIGi5Kiol8ZgQU91zJYIBjwUutqKPPzC9RoVLUgWrtrj+7dFyE7qs/jiOxgUyXNTMGioMBWug/2vHtI4rz0BAALDnSCYQvJ4IeJEyd6OvpEOVOwyMwMCCwsnM3CS1z/vn1HIQjwZWVtPELvDBpggOlH+DK8apa8oFrIs+tzeP5lsk3aInW9MW/EbzwoJkzfje/fZ2vaS/z2MG5jTiknPli4KfHVjewdLbTDf82O58u4ESNKMCAHOHMUOysEtY2WxDOKszhewQIeUktNAgdb3Q59lT8GAOMZ04cag0UdAu6MDNwYmy+gH822GkUL5rwWFwsGbvt5eFSa1kBHCcb0Cdm2vlHSO2hGYIEYjtbxY8ZYBwu6/6mnnnJXV1U9QyoEW5LFtOMALGAIbiQaNSG300QZryTaFGiVfQ0WWFgNyOR0JVD3kgp7Nvm6d3gGWso6MJTshrSO/UsVqCulVEFqZhdsdG8YZEyQGYfvM5OaBU0pWNnQyA0VGVOln+BgIUUxkdES7qocoYW+7nBDfX2lBXEqjIJ5zpaeCQww9sDbJtOP66y3nWDR3MFg4SCOxeI5KCYjMvwZNoH5UZNuunKvCQ4WkvF1Q92tsbQlAmiRIHG634yjdBYF1AJf2ck6CwcHu6cG6rJviMEZ4dD1Q5RDaAqju3yeDyxc0C0nxcbOsqvWnDlzZixUcZto8V5+woAFtWdATU2+cIgQYwCp7/wgMJBggAXp8+2rqsIoU22k2/1YkNQwDnJfPV3JytzCTtAzyjMn1ENj1Q0aVKiDCSS4zcMr7Ez5ygbXqA0Y07vlOuXqK2wSFv6iwEL+dtKkSUmarn/bxYBRZaWn38MSifESRudfhCN/zbsSpdUrzpeQbdb9RHAfP2BBbXVwGwwZ4dOTky8wDIyk/9EOsszpDDvoRw3loLGIACj4RHmNARO2MJuV3EthDJh0maupwOO5iDzm/Edwtz+gSl+2bJkIiPqPeA/E7ouNwEJIRTDKbvdFt8bFLQjABpaC+jFlXr3iBAILjZI0RoIgL/n876Oj/0qLNGAbQieBhX0jNY0J7JzX0I7/vaB5QqEsWrQoDjmvrsAG9RWecVlmZWbr7guRk4tlZFhuZtiG4XyLkDxM5wOdjKfcz2MtbvrFqKH4Lr+4qGgJMxZ5RSUjz1xGKFnDlFn1R13XDFN35OflTTZJ97HueFVDyfthsFtvNA6ISbhBgqiiHpBVnrLF3PpMwILuQRrtK4wmn1h82EkhaMi8DB8yZFBSQvzPMq7GUm4oFDgE5GFO3AjJr6eNA7I0UUXmVCTrk3rb33GwkD7sAMFX6ICcZTbAQroo9lXcj688UcBCbWdqevrVMuARu9WiAHfcuqxBAgvN10Ecs4oEkfOrevc+zw6zk+9AMOdlsGdR1HOQCx2EBFWnyEBxqWDwkAha2Nr5mWVGdlI+u0TFWcNroLLyG+sFVdN8bmBXnHQe+aUZuHVVXI6PjdnFspd6ZY56pK24ypewjhXkri/FznG1yghVAsZERTyv7Io1DhYngM3idhM10o9ZWWkNJhyiLxj3WxwoOFhwX2xM+K4ybz5PO0D2i7/hN/X4nPjm+vWR4iB6cZwkAucWQc0jD4uylkiQxj05LuEp8uBYpUiQfgvpfCNhuP1Y3A9XzXMM6O4kcf12Opnwv7DxWHUpdJFb41Kak60Cy04wsNApvXU55pgvdgWHCN1rl84Gc1nrcLCgd4gMwmK+oL3fg/ZplgGDxgS84nqah68G2SNKMzrLnDIjLMH7t6rrADFEIyXdaa3NVcMFWOaKveK0TrO+y/kE/jqhjQzRb7LbTnywYAnEFviIRYyeh7TjzOvP4HcvGPxiEPQ26CU34H+7VIKxz00whLId64kFFvCemKCAH/OucItdy18rKypuGT929FJ4hNyKxfosaH1I9scCWDhYX5fy7L8Gnmof4nSzLalJSVuxOP9XpTuq1fMsHOTdNCbMFeb7LU6PO5WrHHlRF1xJYeE4ROz7+oe8VPXKnOIBlFPkyY4pKUkSVNxtMD03ta0fJLsDsTjX3DcvTyDJgt8b4XTeIhkSAGO2Aohm9iKNxkpmZBgBF/fXADxlsr/BkCxQYuFI4FMnFxUWPkEbPbJvtmnM11auXBmGzYPYJInN5XKrQBMMTy2Riw4AUQ0D9p/gxrpYyfLrpHnyhIG9QSYZfNSs7ez//XDPQamFYSnQvxfHszLanthgwQmANL23q0eSckLyyr+TTBXGo1a8fyZfrBwsjlvXWaINpeB+nTNwlTZgmOLcDfG9ShOf+siCGkpnuYTiIE5/agQYbRjxmttzUl5WVsZKEp13IhhvmsFYObk3FJ6XDHB8h9InbGan5XEJxBXhcr15hNb6wbS05LH8N/w+iPg9kOdrP5jOU/Ck+hPt+E6MOAveN7JjYbf6Fq3Bg7AvzbHqnpqXmTkfc0y6Km+yABZ7bIIFj+Rv1I8cAyo80O4hN1Uei+PkQOfEEbE6HcoFW+eQzk6mx8Y6Be2eD+mou9E56Fhjm82OcUC/T/bTdp28SAtw4Q4lvor3HkCOulxyIukksCBPoQDBYp8BWHzFwYIv8oqyMnFwj8L8+dGqVCnJITti1cc0PTk50xnBrYBFUyeCxSF/QXly4iHl+2AYrLwGDNzL6SF296oUkgI1FmwR8nAirylY0EQnr6Y67G5+lu/jnhqUEbhFJonUpKsfAKq0uHgbs1scAYuBA03BAgUkcDwk34G+3pMO1Yk8BY0b77ERGISPb9PvD0ACGGpEc3awTB0WqGzTPkQyXyvcjoUbqQgM3Ygq1h+CrosAEBfSmfN7KWnmHQHEWRyzCG6z+S7SpGiUWZj8/leDJph21ZF0eqKkmQuXaARDjhJHCgAofL8HOH8MevULBljwvsLOdKoST/Q2NAoT582bF8N/fNWR92aKMZLrGCC/pLOBgoomJRwYn5+mDca3kMg8XHUl8j3J+UtrSq7r//rJsM1Bx2ePkxKGuNJzNnZmBPdIudgZWDwRDMmC/40I0F+BED+yHXGT2DFjAESV53/z86dfrygpqVMnSzDAIjU5+XwTsHjCAlg02MgNJY+/nCgOcmLGs8M6p4UqbUREXCi8nDBZFS8xY7Dg/UPSuFqcT/EJkx58tBfvMqI9FvhMt64vMArKazCQLLhkgxTzi910RrSoaMjmivLyu6traq7Aj84HKAhXQZ80QQfx7IY31BgLjMFJqoHTmU/6HqTA2CzUHcgHtl7MHczVQ/i/iEvYjl1tJdk8HmTHqjrbAIscCRYnTZo0uYPBYokCFk4GFpYAox6p7XGELz/Q/2N4kz2G6+2Y13dBxSii9r9UJVWk/FiDZJOZal/9gQXAclqA3lBOOoxrDLIYfybbEBHu/iIjLfWhAXV1i7Fr/w3OkbgZGfg2yMSNJGXewZhqZxaNJNlwpB55W6Yuwrwt5fZBeFJl4SL7ptYploNgyf1WJm6UFer8g8iG27czAXOMkqL6Z7paBgu4stZUHpEsDmEBHgSFBOK1xEOy6NOjBwtFNzaoocN50Pctxc7iOxhQOVEpL4wuRK5mLPoP4PV0FiZ9uLr78wMWa+VOk/ooA/om+sscS+d1SPockvSBN8aT/sCid8+ewwEWB+m+nwEWPtrU9u69beHChUcNsPyd40eOLIYq4WFxEpu6kFWAAJ2E6mRD/9raIWSrCcdkfYGYo2jjQQ4WZotVHFwDdcwyjN8P0PmbJSlsAvN5Dd4dDT4lam3tuSTFHKD+HSCwOJUzDs5oiK61uDyEd+41TJSI/+F5O+Kjolb1xcH0NhaEkySTPjnZ2S/qDn0/fzbAQThT7IYd7BY6a1qWe1Atuc5it54nFi+BxdQOBovFUrpnkkUgh0RNBo2fAy33S9UNOzdfSB77EB3/HMZkAp+LbYBllug/gcX0gPtPNK6tqEjDmFyGttLGhVU6Xx6bjJfBK07i7TlWqcuLK4orcVkjjzDmqVEoB1oeaLsUNHsN0vTasrKyBptt1yljdO+s7Kz7kxKTRIDfCtiXyjqbBgmog1D7ow6gq0CxMiuHzogDjrAwffdisMVV1HrsBPvSaV2WJ/YYnLwnjvwUvA8G3xVA5Q3wjHochF4W43JePGzw4GEi1Qe714oxrZT3kf5u63QvjdKqZwt68HvHjWss93fvALi1kleRSp96BJb1ESoRf/SAXr8gAudyQ2JYDrfi9VANbMjOzr0b2TcvbBwypAdf2Ng1l4u2KeMwENWfV5BDPSo3MSbmDGyxbwHd1wFAnsY4/FlkJMUOqk7RiWo4DCaF0xTvHdSAc5z9zxt6J51HHO1yTUFOqN8l4pz1OLjXwkh7ocBMLLJU1k6rxaFEtvfG5VfYiV6H3dlinLN+DVKeTG9sbMwwoHkx6jDUHNYHM+N7HXa+mAvj2m1gZPfn08FjFYE+k2ciFbveeAQrwuh9FRjWzZjXNzt1/QIwsKk4S6aY3WeF+bhQ+2Wnpw/CxiAloP5z+lMcUDLUjzg586zc7OwbAA43QVV6tVBiFKSlVRj071gW3e5v7IKcVfD+5ZfAc9Q77RwXGej3x3AyOqjao1vw6W81Cltrb/Qu/12ANNTa8Wyt4wLZOv85fLys/vYY8wBn0NoZfN5lMXEmfW7/OqV3dr7+TefVbpStYQ18cjtVXa3MD0NusUHuY/DvtZnigNPCIfP2WHyfFiD9df63PbrYfyerxOQ6ZjFbfLZmsw+8z4GXwNsRfDp3fv9143bi/6ESKuYTHDVUjtIiRPtQCZVQCZVQCZVQ+b/24IAEAAAAQND/1+0IVAAAAAAAAAAAAAAAAAAAzgL4cn2g77WR0AAAAABJRU5ErkJggg==" />
                </td>
            </tr>
            <tr>
                <td style="padding:25px; background-color:#EDEDED;">
                    <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                        <tr>
                            <td style="padding:37px 47px 0">
                                <p>Hello ' . $agent[ 'lname' ] . ',</p>
                                <p>
                                    You received a booking payment in your property with following detail:
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:20px 47px 20px">
                                <strong style="display:block; border-bottom:1px solid #C1C1C1; padding-bottom:10px; margin-bottom:15px; color:#2FAC66;">BOOKING INFORMATION</strong>
                                <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                                    <tr>
                                        <td width="130" style="vertical-align:top;"><img src="' . $this->get_attachment_url( $data[ 'lposts_data' ][ 'accommodation_image' ], 115, 115 ) . '" /></td>
                                        <td style="vertical-align:top;">
                                            <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                                                <tr>
                                                    <th colspan="2" style="text-align:left; text-transform:uppercase;">' . $data[ 'lposts_data' ][ 'ltitle' ] . '</th>
                                                <tr>
                                                <tr>
                                                    <td style="color:#C1C1C1; padding-left:0;">CHECK-IN</td>
                                                    <td>' . date( 'F d, Y', $data[ 'lcheck_in' ] ) . '</td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#C1C1C1; padding-left:0;">CHECK-OUT</td>
                                                    <td>' . date( 'F d, Y', $data[ 'lcheck_out' ] ) . '</td>
                                                </tr>
                                                <tr>
                                                    <td style="color:#C1C1C1; padding-left:0;">NIGHTS</td>
                                                    <td>' . $this->getNumNight( date( 'F d, Y', $data[ 'lcheck_in' ] ), date( 'F d, Y', $data[ 'lcheck_out' ] ) ) . '</td>
                                                </tr>';

                                                $content .= '
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>';

                        if( isset( $data[ 'lrooms' ] ) && !empty( $data[ 'lrooms' ] ) )
                        {
                            $content .= '
                            <tr>
                                <td style="padding:0 47px;">
                                    <table border="0" cellspacing="0" style="width:100%; font-family:Arial; font-size:13px; background:#f9f9f9;">
                                        <tr style="color:#333; font-weight:700;">
                                            <td style="padding:10px 20px; border-top:1px solid #C1C1C1;">' . $this->getRoomLabel( $data ) . '</td>
                                            <td style="padding:10px 20px; text-align:right; border-top:1px solid #C1C1C1;">' . $this->getNumberOfRooms( $data ) . '</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>';
                        }

                        if( isset( $data[ 'ltrips' ] ) && !empty( $data[ 'ltrips' ] ) )
                        {
                            $content .= '
                            <tr>
                                <td style="padding:0 47px;">
                                    <table border="0" cellspacing="0" style="width:100%; font-family:Arial; font-size:13px; background:#f9f9f9;">
                                        <tr style="color:#333; font-weight:700;">
                                            <td style="padding:10px 20px; border-top:1px solid #C1C1C1;">TRIPS</td>
                                            <td style="padding:10px 20px; text-align:right; border-top:1px solid #C1C1C1;">' . $this->getBookingTrips( $data ) . '</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>';
                        }

                        if( isset( $data[ 'laddons' ] ) && !empty( $data[ 'laddons' ] ) )
                        {
                            $content .= '
                            <tr>
                                <td style="padding:0 47px;">
                                    <table border="0" cellspacing="0" style="width:100%; font-family:Arial; font-size:13px; background:#f9f9f9;">
                                        <tr style="color:#333; font-weight:700;">
                                            <td style="padding:10px 20px; border-top:1px solid #C1C1C1;">ADD-ONS</td>
                                            <td style="padding:10px 20px; text-align:right; border-top:1px solid #C1C1C1;">' . $this->getNumberOfAddons( $data ) . '</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>';
                        }

                        if( isset( $data[ 'lguests' ] ) && !empty( $data[ 'lguests' ] ) )
                        {
                            $content .= '
                            <tr>
                                <td style="padding:20px 47px 20px">
                                    <strong style="display:block; border-bottom:1px solid #C1C1C1; padding-bottom:10px; color:#2FAC66;">GUEST INFORMATION</strong>
                                    <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                                        <tr style="background:#000000; color:#FFFFFF; font-weight:700;">
                                            <th style="padding:5px 20px; text-align:left;">FIRST NAME</th>
                                            <th style="padding:5px 20px; text-align:left;">LAST NAME</th>
                                            <th style="padding:5px 20px; text-align:right;">TYPE</th>
                                        </tr>';

                                        foreach( $data[ 'lguests' ] as $type => $guests )
                                        {
                                            foreach( $guests as $dt )
                                            {
                                                $content .= '
                                                <tr style="background:#f9f9f9;">
                                                    <td style="padding:5px 20px; text-align:left; border-bottom:1px solid #C1C1C1;">' . $dt[ 'firstname' ] . '</td>
                                                    <td style="padding:5px 20px; text-align:left; border-bottom:1px solid #C1C1C1;">' . $dt[ 'lastname' ] . '</td>
                                                    <td style="padding:5px 20px; text-align:right; border-bottom:1px solid #C1C1C1;">' . ucfirst( $type ) . '</td>
                                                </tr>';
                                            }
                                        }

                                        $content .= '
                                    </table>
                                </td>
                            </tr>';
                        }

                        $content .= '
                        <tr>
                            <td style="padding:20px 47px 37px;">
                                <table border="0" cellspacing="0" style="width:100%; font-family:Arial; font-size:13px;">
                                    <tr style="background:#000000; color:#FFFFFF; font-weight:700;">
                                        <td style="padding:10px 20px;">TOTAL</td>
                                        <td style="padding:10px 20px; text-align:right;">' . $this->get_format_price( $data[ 'ltotal' ], $data[ 'lposts_data' ][ 'lpost_id' ] ) . '</td>
                                    </tr>
                                    <tr style="background:#C1C1C1; color:#FFFFFF; font-weight:700;">
                                        <td style="padding:10px 20px;">PAID</td>
                                        <td style="padding:10px 20px; text-align:right;">' . $this->get_format_price( $data[ 'ldeposit' ], $data[ 'lposts_data' ][ 'lpost_id' ] ) . '</td>
                                    </tr>
                                    <tr style="background:#EDEDED; font-weight:700;">
                                        <td style="padding:10px 20px;">BALANCE</td>
                                        <td style="padding:10px 20px; text-align:right; color:red;">' . $this->get_format_price( $data[ 'lbalance' ], $data[ 'lposts_data' ][ 'lpost_id' ] ) . '</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding:25px; background-color:#EDEDED;">
                    <table border="0" cellspacing="0" style="width:100%; font-family:Arial; font-size:13px;">
                        <tr>
                            <td style="text-align:center;">
                                <a href="mailto:' . $this->getSettingValue( 'email_booking' ) . '" style="color:#FFC200; text-decoration:underline;">Contact our Booking Manager</a><br/>
                                <p style="color:#C1C1C1; text-decoration:underline; margin-top:2px;">
                                    Manage your Bussiness on 
                                    <a style="color:#C1C1C1;" href="' . HT_SERVER . SITE_URL . '">NomadSurfers.com</a>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center; padding-top:20px;">
                                <a href="' . $this->getSettingValue( 'fb_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1Ni42OTMgNTYuNjkzIiBoZWlnaHQ9IjIwcHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1Ni42OTMgNTYuNjkzIiB3aWR0aD0iMjBweCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+PHBhdGggZmlsbD0iI0MxQzFDMSIgZD0iTTQwLjQzLDIxLjczOWgtNy42NDV2LTUuMDE0YzAtMS44ODMsMS4yNDgtMi4zMjIsMi4xMjctMi4zMjJjMC44NzcsMCw1LjM5NSwwLDUuMzk1LDBWNi4xMjVsLTcuNDMtMC4wMjkgIGMtOC4yNDgsMC0xMC4xMjUsNi4xNzQtMTAuMTI1LDEwLjEyNXY1LjUxOGgtNC43N3Y4LjUzaDQuNzdjMCwxMC45NDcsMCwyNC4xMzcsMCwyNC4xMzdoMTAuMDMzYzAsMCwwLTEzLjMyLDAtMjQuMTM3aDYuNzcgIEw0MC40MywyMS43Mzl6Ii8+PC9zdmc+" alt="Facebook" /></a>
                                <a href="' . $this->getSettingValue( 'ig_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDU2LjcgNTYuNyIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgNTYuNyA1Ni43IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnPg0KCTxwYXRoIGZpbGw9IiNDMUMxQzEiIGQ9Ik0yOC4yLDE2LjdjLTcsMC0xMi44LDUuNy0xMi44LDEyLjhzNS43LDEyLjgsMTIuOCwxMi44UzQxLDM2LjUsNDEsMjkuNVMzNS4yLDE2LjcsMjguMiwxNi43eiBNMjguMiwzNy43ICAgYy00LjUsMC04LjItMy43LTguMi04LjJzMy43LTguMiw4LjItOC4yczguMiwzLjcsOC4yLDguMlMzMi43LDM3LjcsMjguMiwzNy43eiIvPg0KCTxjaXJjbGUgZmlsbD0iI0MxQzFDMSIgY3g9IjQxLjUiIGN5PSIxNi40IiByPSIyLjkiLz4NCgk8cGF0aCBmaWxsPSIjQzFDMUMxIiBkPSJNNDksOC45Yy0yLjYtMi43LTYuMy00LjEtMTAuNS00LjFIMTcuOWMtOC43LDAtMTQuNSw1LjgtMTQuNSwxNC41djIwLjVjMCw0LjMsMS40LDgsNC4yLDEwLjdjMi43LDIuNiw2LjMsMy45LDEwLjQsMy45ICAgaDIwLjRjNC4zLDAsNy45LTEuNCwxMC41LTMuOWMyLjctMi42LDQuMS02LjMsNC4xLTEwLjZWMTkuM0M1MywxNS4xLDUxLjYsMTEuNSw0OSw4Ljl6IE00OC42LDM5LjljMCwzLjEtMS4xLDUuNi0yLjksNy4zICAgcy00LjMsMi42LTcuMywyLjZIMThjLTMsMC01LjUtMC45LTcuMy0yLjZDOC45LDQ1LjQsOCw0Mi45LDgsMzkuOFYxOS4zYzAtMywwLjktNS41LDIuNy03LjNjMS43LTEuNyw0LjMtMi42LDcuMy0yLjZoMjAuNiAgIGMzLDAsNS41LDAuOSw3LjMsMi43YzEuNywxLjgsMi43LDQuMywyLjcsNy4yVjM5LjlMNDguNiwzOS45eiIvPg0KPC9nPg0KPC9zdmc+" alt="Instagram" /></a>
                                <a href="' . $this->getSettingValue( 'yt_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjEwMCUiIHN0eWxlPSJmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6MjsiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHdpZHRoPSIxMDAlIiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnNlcmlmPSJodHRwOi8vd3d3LnNlcmlmLmNvbS8iIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48cGF0aCBmaWxsPSIjQzFDMUMxIiBkPSJNNTAxLjMwMywxMzIuNzY1Yy01Ljg4NywtMjIuMDMgLTIzLjIzNSwtMzkuMzc3IC00NS4yNjUsLTQ1LjI2NWMtMzkuOTMyLC0xMC43IC0yMDAuMDM4LC0xMC43IC0yMDAuMDM4LC0xMC43YzAsMCAtMTYwLjEwNywwIC0yMDAuMDM5LDEwLjdjLTIyLjAyNiw1Ljg4OCAtMzkuMzc3LDIzLjIzNSAtNDUuMjY0LDQ1LjI2NWMtMTAuNjk3LDM5LjkyOCAtMTAuNjk3LDEyMy4yMzggLTEwLjY5NywxMjMuMjM4YzAsMCAwLDgzLjMwOCAxMC42OTcsMTIzLjIzMmM1Ljg4NywyMi4wMyAyMy4yMzgsMzkuMzgyIDQ1LjI2NCw0NS4yNjljMzkuOTMyLDEwLjY5NiAyMDAuMDM5LDEwLjY5NiAyMDAuMDM5LDEwLjY5NmMwLDAgMTYwLjEwNiwwIDIwMC4wMzgsLTEwLjY5NmMyMi4wMywtNS44ODcgMzkuMzc4LC0yMy4yMzkgNDUuMjY1LC00NS4yNjljMTAuNjk2LC0zOS45MjQgMTAuNjk2LC0xMjMuMjMyIDEwLjY5NiwtMTIzLjIzMmMwLDAgMCwtODMuMzEgLTEwLjY5NiwtMTIzLjIzOFptLTI5Ni41MDYsMjAwLjAzOWwwLC0xNTMuNjAzbDEzMy4wMTksNzYuODAybC0xMzMuMDE5LDc2LjgwMVoiIHN0eWxlPSJmaWxsLXJ1bGU6bm9uemVybzsiLz48L3N2Zz4=" alt="Youtube" /></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>';

        return $content;
    }

    function newCredentialMessage( $data, $password )
    {
        $content = '
        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
            <tr>
                <td style="padding:25px; background-color:#FFFFFF; text-align:center;">
                    <img style="width:250px;" alt="NomadSurfers" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAYsAAABXCAYAAAD1a2lrAAAuWklEQVR42uzdb2hVdRzH8XP/ue3u3uvdH9e2dP91azDm1BabKQtqOlFRmyVTiqT2INAI90iy7GE9uA/S8EHSIgh6EEVJIBJB9cCCKMyKkFAzliSR2B902z339rnjd+Jw2GX3Thfaeb/gy++c6+7dFPf7nu/vd87vZwEAAAAAAAAAAAAAAAAAAAAAANweAjMBAIBH0IRXYOZ1AIDvhSyXsbGx8lQqVWmJS9gCAPjTzp07Q5Zs3br1rs7OzoM6/HTRokWXEonErzo+E41GjzY3N3eTMADA5xVFOBzui0Qi53WYdUcgEJhpg8HgVF1d3bMkDADwaaKoqalZq+aaSQ6Tam1FxoStmDLHWSWVsX/fCwDwh9WrV0dKS0vPmkpiyrSzha1IK5lk6uvr+y1h0hsA/DOhvcckg2nT5g0lipmvSSaT71FdAICPkoWqhBPuRDBH2Lm2pKTk54aGhhaqCwD4fwsogocPHw5qCOpzkwjSBSSLTC5y8xqaDO+lugAAHyQLE6eLTRaKSQXJAgB8dCfUyWKHoRQTijaGoQDAPxPc+5Qo5roTKuueBNcDe+9TVQCAPwQsGRkZqQiFQhcLuCMq4yQUVSMPkSwAwGfVRTwe36IJaycpTCpsT5KYdhLFsmXLjpAoAMCnCUNrQu0uKyv73VtNuM+XL1/+SjabDbF8OQD4OGG0tbW1qspIaQ7jnNaCmtbwVC5ZXIzFYu8sXbp0E/tcAID/BEySCJrziMKR3LhxY/vQ0FBnX19fpTupkCgAwD+Cs3X6GmIK5Pt65igAwF8ilqFbYB/RkNOH5eXlZzVf8Y3Ov9SQ07FVq1ZtYcMjAPCngNPxd3d3363mA0U2X9TW1p4YHBxs5sE7APCPsGW0trY+qoUAf3E9V5FW2E64b5NVtXFBbYcnYQQUQSa6AeDOXesplK+a0Jap8bJo2avmae1Cnti+YZYBOa0FB73Jwfv5ESa/AeA2SwieyD8prXC2TFU18a2rmrDNcUHLe+gW2hHXz7BE0aFbbKvN3t2zToaTPADgv00OYRN5HThwoFrDS21NTU0bdJp0f4b2zT6Ye2bCW00UkywUxxWO5xW2Ko4zmhA/uXjx4mPaDOkZvTbY399fo9YrTOIAgFvFe1Xu0d7eHlfTpKSwVlf1T+j4ZcW7Wqbjq4qKigl13tcTicQXeniu0pLe3t5m/dnHriex0+Z4PsnidUvMKrXjeXbRm1QFc1nHHyk5HdJ2rQ9oY6Vq73MdTJYDwPyFFBHLZXR0NHd+r4aPntZ6TG+o4z+t89/m6NwPWdLT09MYjUZ/NJ34lGvZDttEUclCn/WaJZq7CCsxfeL63LSpVqad7+GJ7/XzH6murt48Pj5e6q42SBoAUGwlYaxbt65OQznDujI/rjinl/7Ms6dE2hMZdco39LR1v9kq9UnTod9wvS9tXitmOCpt2jFLpEXvv+De28IJTwUz7a5k9HexNVz1Q1VV1XNqW0kaWGgBd3Dr3sKPlw8MDIQV/EIv8O2sqhru0xDSEXXEl2a7unddxWfybUikIaCvdeXvfOZbTmfvaa9omOopdd5OhZKZa2c8fa7d0dGxwZKWlpYHzXts0xayWdKUaZ3hqmuRkshRHXfw4B9wp68X5MECcguzcJ867i5NEr/t3KY6y7MPmUKHirSX9guW7N27t17Jx50MMmbDop+6urpatm3b1qrjQqqKjLkT6oozcd3Q0LB/PhPlJmzPfhl/qNI4unLlyiZLzJ7gXJTg5j22fXvVnh076nS7Xu1MbNpUu2vXrvpUKlVm4VYIWsa+3bsTanrX9PQMD6xf/3g0ElnjrBtEwrgFCVlWrFgxquZvz8ZCdnEdsLnKV2jo6X5LGhsbH1blMNM5u7dL1YqyL1pSWVk5bIai7AK3UP3M+b+hu57edFcq84yMJ2lc1s++35UoIhZwM79ciVjsVCIevxovL5+Ix2KXy6PRiYpk8urmoaFhS5RAWOhs/oJmUjV6T3v7S/q3Pa/TvzQ0kq1ZsiTX4VzXcMR3em2QjW/+Ye86oKQqsjbvdU/35JxjM4GJDDDDMDAwpAGGHAUFRJK6rP6uGFbWrOuaRRBXBGVd18iCi4FVdA2sq4LZVTCsa0DF9KMkCcLM9PxfNbf469zz3vR7PdMDeLrOqfN6pvu9V3Wr6n51YwVYaI6SZ9OvQU/GOAOqzSQ1vAojcgy95k9S4lDBJCUlpV58iTxQCwksmq1ILGDkt9Fzw1Hf5faKjgINRIv/s7a2tozoFHK1DZV27XjfMpp0gwYNmhUCi/aD8YIFC1LBSN5UXSFxfRX1BdRd4iQ17FpHhcCifXQeMGBAgthNt2eHzhk6QP2P4tmwL6VDcviYxq9FPh/j+hZUUAnUjpvlvVae3bVrV9/6gvG8ECopVRJq7aDaoqi19qDO7BIqodJOsHiV7ZjE4SytWCAzQ2DRfv05dqbSKOoFg1g5ZsyYfHyO7uLxhKeHh3twvsEpGzduDBkiAyxSzTJ37twq2Bc6iuF6AeJeGJ59BxNBehitSA1eqYKCrWGZbAd28A9aAAvZtv1IKNhH3IfrNFW91ZGVtwe2jEvXrFnj6hIqoRIgWLzGROBmMXmhUw+BRftp2xN1t6ArGM89RjvikL2i/WBBBujeBBaBVm6A/kzkfSJ7xJXcAA3AaIbKq788rwIBfS9bkGqkh9VWSCXp1IVrVKYejCpdfZEmvXXIkCG1XUIlVDoQLEKSRQe4byLoa4FvsWInmZ+dXST+hwjcsNDhN8ctWHiFmpBiGJYqUd7nEtM9qIDBFnJ/7jJ9+vRklA/oN6YSgpRI0tLS/i6fjfs2qN91ZKX++AAO8SK7YCcZ1iVUQiUEFscfWOCwm6U+/bTH884keJ2FpIjgggVUQfbVOcSouUQAe0Wjsk6iUJ+RYEAM/w8KmBTj3d+wdWSqEtK76FfTrZl43ocdaNzmffKS++/LDQ0N3UJnaIRKCCyO38CwWwVN4Rr5d5FmIhRTETwDN861jgWD3GZHpUOJ+1SQ+Boqpy9hr3gYn+OZ00Es0n3Mw+783+K3yCVVJxuA8R0EtZWVGI4W3aG3pmelTxP3IcVHPS4H1biNDqjNSlbbJjhP3KQEFYZ1RFpePUiTWCNm46todBDeY61/du6ldgarzbJtQaSLeT+CDBYaq7/ISHc7YEGG0b/T/7RgRd13dv+PlznBGPpJCtM8ZMaAuW0BBuw1UBmeIjSHigFYJ+aqMf7xCpj8ewL8Jd8EgESCMW9q22ZBQXxu197hw4f7VJLIXHtqR9krBOip70Z7nkPMSQ13L+ZRorzqjEHwScsP3NCDleFRlkDzsnMma/Ac3ey5/Dub32t222v7/fSOoL+H3tExYME3BFTanxHT0U7mqtuZy4zuVp7jMPmfrG7631Ki6ZPkT++kar5GA6ez08iPvtP6z1VD5r9xyHYGSVIWwXE7mOqnhQOFpmutIk9UYUXhCBaz4TrnnHNi2bMlgNTS/ZepacLJPlVvpk5i/98id/gZaRl3tdNe4eX5omg9T5UND2gd8UlWVVFRGgFrfmZqat3Afv26k6jCftu+lAZzEBE8duTIfvjYV6QABgPqedFFF8XwXZj155sXaWySA16Sn98dH/sX5OXV4TsPD+Ixa7egA87IrUDK4P7wWqgbPXp0T+w2HNbba35YivScEGmOS46IsX3Li4v7Txg1qgzvDefj1Z68OKLNo/BcuPcJj426YQMH9hLBYez5AYMFpzm9MwKiego+JuK7ZFM6UAl43IN/v8b7Tf1KO++88+xEtl9HNH20ne10MmbsqqysTBV0xlxNxZzS2/p98PtvPsbjxo1LFu1EpHPKypUrI/n8CYaEgZ1/Li53ou4zMPYeIgb9dnl5ea68ETaIBtQVUNs8j9iJzdDz/wV/nyEyzcrfwKPtKnFvaWlphQpQeI6LvKaWKgBlJsksl8+DRHJlAGDRwm0sJB39E3aTGXIecb7GS3fUB1DvokM1VqI+iIW/UP4APt3FIMINuqZtjQwP34GX/uR0OH6Ki4nZCT3d1sTE+FvhLlas7A40u0CUiuyHuPF8PHdjYkLCdjDdvSJfidvl+gmRwTuRHfFTREau9+TkzJ0zZ066xfwl8ruzUR+ivt1NUZUrUD004Pmw+v8R9T+usLAfRXZJ8d7oqKjvwtzul9xhYWdjkUUpbdZEpYlbgUjmxS63ewsm2w8O0EXD/SnJyT7a4H83KaDjtMu8YTCrweU2tO3diPDwb8Pd7p8EXfD5p+SEhB2xcXEfx0VFPZgYFTWC2mQPSIlG4jD4nKycxRjn95MTE3eg7eI9PtrDde4Th67fRykMZHnTDljwviFuIAHxA6fh41p4dvwX129FdbqcXyckJbzVNS9vWe/evfv4BUHqM+ZHAi5L8MzV0yZP7i/bYHN3eQYdnD+Ov5O/Dy6PSYKx5GZn3z9l/Pje6nMqKioq01JSrg93uTaDjiIr6KdYkB+h3o/P1QrzTgQoLxHrDrRbQWtwGeq/iaafE/NayepyWrPTeTs5nWfNmhVVXlIyHWvnYYzjFqwvYVDdHhkR8TWiwd/G5zvgaTVGUaM4rYBTPIpoR1ZGxv2gcx+1/2VlZeVpSUnX4Z2bUI/2H3NLxBRIWml8jPpXVdWLMcTaeQv3fS3nRBIC2xKjo9fVVFXNhEE6xi6w2V1zVX2rSjOyMq5z6s6PVAYucjxhrhbTz6Kx1u8yY85Yr9+B3o/17dt3ZhzWKP5+xwzcKUDvEy5hqICAOXWTcn8k6vttqK+8LBtuC2vbdlTBB+tILWZZCzLepMPPii+z0tLOA4Pa7w+5wFT29ejRY54EDKuDA8YfjwFYjN34Twb6NMN35eXkfAPm/eujOw08yw8YPWmEtBjMcniejNVY6mGTdmyFtNBbPpdoc4mVJF7hmGRor8KA/NOlHCdtgTmvwbu9VukSFR7+fHVlZZXF3ZcuP/SsrDzX7XbttrJDwU1iwUeg/ss2WFDfKkpLh8dGRX2kPregq+cHT17OF5jE+9T/5+bk/AXXhDb6pNPuLAeXXd2KilqnTplypkUa8Hmymt77B27Y4+8D4/OI33pyc1snT5gwQzLmpLi469vyUEmIi7tZAYt8gFsTuVya7CjNK+55QG0n3wD0rKgYnpqa8g67bz/mz27+fADHpu4lJf0sGDR1CibLFvdlZ2W1nnzSSaeRNBCJzdEf2mp7YmzsrUyt1KVPSUkS6Pag+ru4mNi9hQUFXyQnJuxgfObfSEMxJEiAoavPhFrJjUsjAP0m0PpdrMf/kbF0qA8rO/bDqGL+N1NW2ia+VpEp9nzZZukKLU7Uk8waO/yTTNJ+eOk53/okHyoIlJtE3/vejSrf22zCwz7BBvw+AN4sdoKeLTo2KoadJiXj4loswCuwo+Q6PC+rLUd9cpE9MTstbb7fBtB3hXl5VfGQGHiGR+Fmhnr0HeKzREp1IHKzstaD2HHsfUZM4BG65yARV3zeVti169nJSUn75OEhJv1rlv3DgO4ZOHCgz8hUWlR0p1zgBBiGbZb3QvpoheFokhET46BXXFg4HrvSHyXTMKK9xugi2kCAsQfXoew9pqqLqIiI20TktUIDM9o3Kbusp5W8NF4rYCH/HtCv31QnUlV0wRjER0dvLe3W7fS03Nz8sWPHZk0dNSoFasccbDoGJMbF3Q3gkDn/38A1w2QjopHfeaZvTPPzW6dOnjwrQLCQp45dzOaUEbMU4LQbYCbAwjeukLTXSm+SmMjIDZD4LqytqZlb3bPnbPz/94X5XV8cPXponuJIEI1dpQCa03HPXHFFPVnQl9rxDha3uHceq3NQf431KZm7ztsHEPtNmNPpGxvklvrS5XReAzVmPVQh4v3ZYFYFWD8NeP5i5Pb6kVwlmyvLy0/hgGFEbxzAI8ZjZ1ZmZuu0iROn+fofG/uQD8Qcjha88xnR/xr0H++aLYauID//X42NjYXqfMB3cZEu1+viPkj1B0q6dbsd66xnBebBrEmTUvF9BlQ8pcX5+QvBXz5UoopP8wdsHWm/I81COGkiLmXGcH92AQHO5epc69+/fwwM1RsRiV0u36EAUBOXLoj/rFZtIVgf6wxAQcy/fRifrzGe/xB2EmTTHYQ2M4AIzEFplMlO5qBETolcxGh91UBckvc3A7kG+hOPSRzdRfeqRCfGRH/LNuDd8n9gSj4mLA0zwv2NLxr2/kcNBmG/2kdFp2fcPwq6waJ7NCE29n9Yqgsvu7fZhDY7kYumG2PYvK0TUQ+p7xSVAJTTRTBotU+H6be7KoqLe3Oa8HdBpXidvI8FBXl16o8EKwlYrG+8LRwsuJtidnRkpC8PDxjdqqvIFsKLskDrcfExiCi3+0U8O9wsihnMIwuXrwRYTJsyZXaAYPEXZogMawMscoWaTjDLOaeeOjwzPf0Wn7trbOzLws7WTmYlx2WdnZtkf/M9ngXQBvjGBYx2w4QRI3Lauq8Bc7Kga9eNXWgX3K2goK2cU7oCFnvT09JaoR0YCXXUdSQ5bYa0NNAqzRFLci/19UOkx65p6waxztOxO6Zo5laoeAYr/Q5G0bjTxLBhw3LBjHdazMkkc0FtkLST6wI0qqd1sEa+DOCRhzX0o4E6yiv/LioqGiN/D61IKcbhDWyUns2BBA5QEHEYp0ECqhMaGyPea2K4DhwsuFHEHzFklYyrKD//NQPjmTC1O0hfXQqd+065mzXKR6JBwsGE34Nn7pHvkUyTM3BPXu4LzFbCmcA69nwzn2m/qgBIWocxWVqUQbR2L0k0ifHxfzbYtTrIEDYFO71D/LliDFgI/m5hu8Du7cizpSQgvifAgMT0BhnYNVH5GNT36zdRo3YaALOXdLSiv60a6xfzC7cCFmFkmLuIVB7vjSAbUDW+kzsc5h7spkVVHIustsT4liv0MgeLSZMCBQvJuC71BxakEjgAZrd/3JgxVwuALy8rexnzkDYupt5LmoHEJasEw9ukNxTRIYw/j3sbSYnLk51dC7uEb+5Avfg41uFRtYfSBl2hcxjdHwlbwRPiPkib28s9nnSlv2ZgsR82m4OTxo0T/T9QVlz8Gp4T77f/9Iz8rKxu2JT4znqAfWo83edG1fmcqFbGoqAg/yHSZHwNlVRaJwWNOSkn0yKsQatuq00UL3MBY9ZdwENWSUkA6+Jk+RKovBZiHRk9v5mC/t6WB2BJiYfUZUZFN/NYCwZYyCCN77Fbug8oeBX0XpdjN7EMHfrCiAGT2kYM/mA5ibkuFd9LMfsQJwZ+9JlT1y/BqhkCdUQviF69RBuBnjeDQR5Qd+rqMzB5L5cDYgMsvBJ8IIp/UFRQcCd2LJdAD7sUIvGnbRmQ1OehvTuA6sJofgl2B9dD/NtM7fTyeyJg+KquqChgE9xBJ3QtZLT3yr6C+f8HuwfRxyGoPSrLymoxUU7Kycpa4QDgMv1mEwUAzTGgiSaMmbnZOVsMaNkigQ39WVdeWnoZJucM2IjOSoHHBsZ8G40R/c4yWDjICP1XSqR2pTUVAon9KSliMW3tVVl5Cp3joAULLGxKFvsgWRwqKyk5KAyUSibRsI4IyqM4izCLC10XtIHTwz/FvTjz4h2MQbQVQ79MJTK+oSEN60DO+2sYbThYZArJKi019XD3igqxLrdBzZpsqf/UT9gMffENYbq+BfMtUY6nv3GCBBKPdfpCQnT0negbuy9oRa7RjRZtSl7iS/uhYi1SxwGquRw4SHyl/PZzrAk5dwSQbDThV4cpX9jvGJ01BfidZuqlYIJFk4bFn5KU9IhPH0xFPZ82JTX1GYN7m0hMvI4tACcN9GyuYvExGgAMgUiyWUORlrdXZlraN0zC8F2F91IPeGFIercFFly0i0tIuHEYdKfqu3rBZQ/eUP/wB6TC46EX9MAGu8VbDO49TGmGT5c04Yuw25GjErdJGhFzvZcWhWFJT0qa7Twi7XjZjuZxI1CC5DeZH7gi6YmNwHfYrY0wek8JDJFdYFQlwGi2CBaaEouznvp/jmWmSkbHwR4PU0EdW7BAyRVegZKGWCMz2D0dChZWXGQxPuPgReSbA1B3TlWAwG+RbpxYX7+Rdo758+cbMWJdSTmxV/Y/PSVlto3+h5FH1SxxL7QMG6FTj7IYgKdTqowYce1MoEDpgbrHjgoKNHpStlvOR2yA58lNneQJkFTvUjwg+wAwDvP3SFpjjH8Qp+wx78zgFw4W6mfol59VJyRNPFFdMhc8Ovyp7Ii6U83MyPDdq4qTQi0CZH5F/b32/+/aKrM1wrfaLcVWrpYASg8WRnhidl5VlZUHrwXZVgs2C9nf29VJTNVN6rJ87CB3G+nnCRBfUumj0kZ4OUBX+QabOL73g6FxdQrfcaViF+4DuESaRFQUVQJNPlp4xIi5Wuk7iLjJagAWHbhyn9oeaXdxhTkPIX5jCOuPk6pLNgLS0Qs0dk0WJQuddLLrjqgN8x5UDHW6XWZ6vICFZBxw7Xxf7HZle44BWDiIvqtIffgKNlYRttpDv8NGIRuX3bBntcLldoo6lhwsZGZcuFp/COO5nRxWTlLlzTgyx+P3DB06tEDOCWttDT6j5O2FpP5bG/ENTbSJmMnnLoDgecaDWsTagYpLrj3hmn0tz1irvhs84gE2dzsdLLgevkHZdfDiIs+A8yRxVLCA/vPts846K1qdbJgQfYTHBUdLQSh4Jk21MFlctChWSMIx4PlIiYfQ2pAsvMTsvwGKJ3Kiq4wGO6wnDOjToqFGh4cPNWmzi3YIl8v3qs+IjIp6ip6vmU1M8oqaR0FNGj3TwasENhj+fsv7KLychLsvW/CpqB9IuqlgCyOnBE5XW7vPPlVVQ6ES9NowcIdRtOrvFeeJUazPfrwz2P+PD8liF0ljt7Ox6wyw4HSJR91CEuXldqQcPsZJCQk+jQGM9kxdyCQLOGz4bFCJiSts9l9Xduq7yRPrTjYeNuZD8AuM626sZxZb5Pf8ie+hXitktKlQN3SMt7ynaFaiwJ+MYyno/dhQDpf06nywIEZIouFHmDjpbQyMg3S3tdLdVdwPhuG7v7hbty/POfPMAgY2FxoxbaELJ6nC2sHqHs8g3MMJKN7tRfCK4gVkAhZ0BcO7kxObvws2mivkPaokBKPrOxTZrJkZ1mMcjgk66MKlqKT4+BemEk14sbHj5oE9A/luBWMi9JvD2ILvqYyzyvD3o9YyepgudBynaieCW6O5kiVPJQNddsLWc5FwmTXz2uA0ON7AQjI6zJEF7PedCRY6qZu6g/5yLKcpNh+nv8okag3u16uoX/cp9OH9z5D9Rz1Htt9uX+E2K2ku1CurSwsL+xr1kcbScawSZ8LluMpfwj+++4dU/6iahobigW4x0HK0kH1juwdFSUM+hr7n720isLiW0b1zJQvJhMFkXhIncXF05141EFursbP6iYh01GAMP/nttT17FknmR4S614jxpiUnP+zPo4FFfcbBh/x77ufvxE5a+O1LAhqCBe2kyVvkt0bEZv871wBo0OaU9f7iOyAqDlXdgp3kRdWnpubzRYgPMXEDdUjHABHVDvCb7IFBGGLpChFta1CXk/fMakkPxsSHqGMASeUUDirk3rdVBFVZVXmg3MCCiAzBgt+HZGiTwpUAzPj4uG2QgB4QOm8RSS8wxWAc9OMULGQ/JsnndDZYHA1sq66eSrrvnTNmzCgKtAEUzX8HObY87Acs9kpwst9/WsuNjYllpaUviueQ7fEA1s1G2EovhW2rDzaaiYYePp2fCfgaG15QzRQLMkfOIxlbAQlqi4F00kSR1bcpv3eSI8Eq5ZkcjO49dmChMkVNe5w1xHCSnn766dWV3bvvJ7WHV3rKIOBqe7/y8qMimPDUwOBvlICkvgsBaMwYbslCtonvbOm6UHkWt1kcVokNb6uTOVPhkyQjNfV8I1XSiGENEiwcZkBa06vXMEhohzlYYGGrYKFxoHRHRo6PhaoKXxwQExTVSvZIswRkqqpMeHMsMOqPBj2qpQVP3/fo0V1RP/oFC35eQyU2DmvlzpQtms0ZaWlLIfKfAiNmJnuvTbAIfpwF1K2tQ+rrp7D3daZk4aDgyuk0D/aQJ9NFqJegXmyjXirUylBHbqb3r+a053EWsNe0NgwefDIbc1u79gsR+Q4nDiHBf2Qwtz+Do8kD4iCh5Li4ap6VtjMy5k69aqorJjbmdTteUCRBx8t5RHN0sqHNgzaw+L6WAyLU6kMpaNZrABbPM7vwsQEL1PVWwGL27NnV3Q3AIiEmZnu/qio1WlNH3Sh3o2pcBoyDN9oAC52uT/N2ixOvaqqrl9PCNQYLuofaMc6fZAGp5zwj5jpu3NgOAwvpXrxkyZL4vNzsR/guRQmMO9RGPWwFLFDOMOpPrx49NtoBi5EjRkivNstgwWmN4M0KFJG25TFITl9jVyk95KRaYhs2EvenJSR0P14M3Bws4FQhggA7HSx41P+oxsZroeP2jbvIFgC7g0iNIRwkbFVkVBCeUMJo3YqI9L/5A4tsBCWeMnVqoGDBtQnJMJRPTIiNvgObhfcjoqKk5CLTZ+8C3V+CeuxUdn+wigygGy5ijiyqoZpp7qoqPJ0cS9aYqaAwdpvVcZb3IAShGP1uNgn2EzEXBEi/ELCgcxGcsPa/KI3gKqNB0jhLujdGlOd4u0W6kaqePa9VwYKroVQDPto7IVCwgOG4w8BCTozYiKj7HLpDTb3iZX20Xs3B4kyj/oDp2ZIshjc0nG0fLMxVCchKmljWrVt9VEzMBRnp6YJJfafspPbBaDtbjsvxJFmIdB9TJk7sdLDgc23QoEEX0w50R+/Kyvlitz+wf/+ZAwcMmGG74hz1QXV1Z8Kjqo4zIg4WiNwW6VUCBwt6Pqe1UIGDnsIYPBNq/OVY2++oUjQyKaxVVJZaUAPxUtNvlLt/q15QMGxPka7LlIzVg/Wxz0iioMjsBfKdTOWeiHH91kjFjDH4NguBjQw0T3CwoImAQV/HGEwTRfTeYUfnKYLKUD41UUOd3ZYaiv12vG2woOvYsR0FFpQny+MZLNvIdy8wBgtXud3os3DFfRl1E+orVDeBnv+CrprlajIGi5Kiol8ZgQU91zJYIBjwUutqKPPzC9RoVLUgWrtrj+7dFyE7qs/jiOxgUyXNTMGioMBWug/2vHtI4rz0BAALDnSCYQvJ4IeJEyd6OvpEOVOwyMwMCCwsnM3CS1z/vn1HIQjwZWVtPELvDBpggOlH+DK8apa8oFrIs+tzeP5lsk3aInW9MW/EbzwoJkzfje/fZ2vaS/z2MG5jTiknPli4KfHVjewdLbTDf82O58u4ESNKMCAHOHMUOysEtY2WxDOKszhewQIeUktNAgdb3Q59lT8GAOMZ04cag0UdAu6MDNwYmy+gH822GkUL5rwWFwsGbvt5eFSa1kBHCcb0Cdm2vlHSO2hGYIEYjtbxY8ZYBwu6/6mnnnJXV1U9QyoEW5LFtOMALGAIbiQaNSG300QZryTaFGiVfQ0WWFgNyOR0JVD3kgp7Nvm6d3gGWso6MJTshrSO/UsVqCulVEFqZhdsdG8YZEyQGYfvM5OaBU0pWNnQyA0VGVOln+BgIUUxkdES7qocoYW+7nBDfX2lBXEqjIJ5zpaeCQww9sDbJtOP66y3nWDR3MFg4SCOxeI5KCYjMvwZNoH5UZNuunKvCQ4WkvF1Q92tsbQlAmiRIHG634yjdBYF1AJf2ck6CwcHu6cG6rJviMEZ4dD1Q5RDaAqju3yeDyxc0C0nxcbOsqvWnDlzZixUcZto8V5+woAFtWdATU2+cIgQYwCp7/wgMJBggAXp8+2rqsIoU22k2/1YkNQwDnJfPV3JytzCTtAzyjMn1ENj1Q0aVKiDCSS4zcMr7Ez5ygbXqA0Y07vlOuXqK2wSFv6iwEL+dtKkSUmarn/bxYBRZaWn38MSifESRudfhCN/zbsSpdUrzpeQbdb9RHAfP2BBbXVwGwwZ4dOTky8wDIyk/9EOsszpDDvoRw3loLGIACj4RHmNARO2MJuV3EthDJh0maupwOO5iDzm/Edwtz+gSl+2bJkIiPqPeA/E7ouNwEJIRTDKbvdFt8bFLQjABpaC+jFlXr3iBAILjZI0RoIgL/n876Oj/0qLNGAbQieBhX0jNY0J7JzX0I7/vaB5QqEsWrQoDjmvrsAG9RWecVlmZWbr7guRk4tlZFhuZtiG4XyLkDxM5wOdjKfcz2MtbvrFqKH4Lr+4qGgJMxZ5RSUjz1xGKFnDlFn1R13XDFN35OflTTZJ97HueFVDyfthsFtvNA6ISbhBgqiiHpBVnrLF3PpMwILuQRrtK4wmn1h82EkhaMi8DB8yZFBSQvzPMq7GUm4oFDgE5GFO3AjJr6eNA7I0UUXmVCTrk3rb33GwkD7sAMFX6ICcZTbAQroo9lXcj688UcBCbWdqevrVMuARu9WiAHfcuqxBAgvN10Ecs4oEkfOrevc+zw6zk+9AMOdlsGdR1HOQCx2EBFWnyEBxqWDwkAha2Nr5mWVGdlI+u0TFWcNroLLyG+sFVdN8bmBXnHQe+aUZuHVVXI6PjdnFspd6ZY56pK24ypewjhXkri/FznG1yghVAsZERTyv7Io1DhYngM3idhM10o9ZWWkNJhyiLxj3WxwoOFhwX2xM+K4ybz5PO0D2i7/hN/X4nPjm+vWR4iB6cZwkAucWQc0jD4uylkiQxj05LuEp8uBYpUiQfgvpfCNhuP1Y3A9XzXMM6O4kcf12Opnwv7DxWHUpdJFb41Kak60Cy04wsNApvXU55pgvdgWHCN1rl84Gc1nrcLCgd4gMwmK+oL3fg/ZplgGDxgS84nqah68G2SNKMzrLnDIjLMH7t6rrADFEIyXdaa3NVcMFWOaKveK0TrO+y/kE/jqhjQzRb7LbTnywYAnEFviIRYyeh7TjzOvP4HcvGPxiEPQ26CU34H+7VIKxz00whLId64kFFvCemKCAH/OucItdy18rKypuGT929FJ4hNyKxfosaH1I9scCWDhYX5fy7L8Gnmof4nSzLalJSVuxOP9XpTuq1fMsHOTdNCbMFeb7LU6PO5WrHHlRF1xJYeE4ROz7+oe8VPXKnOIBlFPkyY4pKUkSVNxtMD03ta0fJLsDsTjX3DcvTyDJgt8b4XTeIhkSAGO2Aohm9iKNxkpmZBgBF/fXADxlsr/BkCxQYuFI4FMnFxUWPkEbPbJvtmnM11auXBmGzYPYJInN5XKrQBMMTy2Riw4AUQ0D9p/gxrpYyfLrpHnyhIG9QSYZfNSs7ez//XDPQamFYSnQvxfHszLanthgwQmANL23q0eSckLyyr+TTBXGo1a8fyZfrBwsjlvXWaINpeB+nTNwlTZgmOLcDfG9ShOf+siCGkpnuYTiIE5/agQYbRjxmttzUl5WVsZKEp13IhhvmsFYObk3FJ6XDHB8h9InbGan5XEJxBXhcr15hNb6wbS05LH8N/w+iPg9kOdrP5jOU/Ck+hPt+E6MOAveN7JjYbf6Fq3Bg7AvzbHqnpqXmTkfc0y6Km+yABZ7bIIFj+Rv1I8cAyo80O4hN1Uei+PkQOfEEbE6HcoFW+eQzk6mx8Y6Be2eD+mou9E56Fhjm82OcUC/T/bTdp28SAtw4Q4lvor3HkCOulxyIukksCBPoQDBYp8BWHzFwYIv8oqyMnFwj8L8+dGqVCnJITti1cc0PTk50xnBrYBFUyeCxSF/QXly4iHl+2AYrLwGDNzL6SF296oUkgI1FmwR8nAirylY0EQnr6Y67G5+lu/jnhqUEbhFJonUpKsfAKq0uHgbs1scAYuBA03BAgUkcDwk34G+3pMO1Yk8BY0b77ERGISPb9PvD0ACGGpEc3awTB0WqGzTPkQyXyvcjoUbqQgM3Ygq1h+CrosAEBfSmfN7KWnmHQHEWRyzCG6z+S7SpGiUWZj8/leDJph21ZF0eqKkmQuXaARDjhJHCgAofL8HOH8MevULBljwvsLOdKoST/Q2NAoT582bF8N/fNWR92aKMZLrGCC/pLOBgoomJRwYn5+mDca3kMg8XHUl8j3J+UtrSq7r//rJsM1Bx2ePkxKGuNJzNnZmBPdIudgZWDwRDMmC/40I0F+BED+yHXGT2DFjAESV53/z86dfrygpqVMnSzDAIjU5+XwTsHjCAlg02MgNJY+/nCgOcmLGs8M6p4UqbUREXCi8nDBZFS8xY7Dg/UPSuFqcT/EJkx58tBfvMqI9FvhMt64vMArKazCQLLhkgxTzi910RrSoaMjmivLyu6traq7Aj84HKAhXQZ80QQfx7IY31BgLjMFJqoHTmU/6HqTA2CzUHcgHtl7MHczVQ/i/iEvYjl1tJdk8HmTHqjrbAIscCRYnTZo0uYPBYokCFk4GFpYAox6p7XGELz/Q/2N4kz2G6+2Y13dBxSii9r9UJVWk/FiDZJOZal/9gQXAclqA3lBOOoxrDLIYfybbEBHu/iIjLfWhAXV1i7Fr/w3OkbgZGfg2yMSNJGXewZhqZxaNJNlwpB55W6Yuwrwt5fZBeFJl4SL7ptYploNgyf1WJm6UFer8g8iG27czAXOMkqL6Z7paBgu4stZUHpEsDmEBHgSFBOK1xEOy6NOjBwtFNzaoocN50Pctxc7iOxhQOVEpL4wuRK5mLPoP4PV0FiZ9uLr78wMWa+VOk/ooA/om+sscS+d1SPockvSBN8aT/sCid8+ewwEWB+m+nwEWPtrU9u69beHChUcNsPyd40eOLIYq4WFxEpu6kFWAAJ2E6mRD/9raIWSrCcdkfYGYo2jjQQ4WZotVHFwDdcwyjN8P0PmbJSlsAvN5Dd4dDT4lam3tuSTFHKD+HSCwOJUzDs5oiK61uDyEd+41TJSI/+F5O+Kjolb1xcH0NhaEkySTPjnZ2S/qDn0/fzbAQThT7IYd7BY6a1qWe1Atuc5it54nFi+BxdQOBovFUrpnkkUgh0RNBo2fAy33S9UNOzdfSB77EB3/HMZkAp+LbYBllug/gcX0gPtPNK6tqEjDmFyGttLGhVU6Xx6bjJfBK07i7TlWqcuLK4orcVkjjzDmqVEoB1oeaLsUNHsN0vTasrKyBptt1yljdO+s7Kz7kxKTRIDfCtiXyjqbBgmog1D7ow6gq0CxMiuHzogDjrAwffdisMVV1HrsBPvSaV2WJ/YYnLwnjvwUvA8G3xVA5Q3wjHochF4W43JePGzw4GEi1Qe714oxrZT3kf5u63QvjdKqZwt68HvHjWss93fvALi1kleRSp96BJb1ESoRf/SAXr8gAudyQ2JYDrfi9VANbMjOzr0b2TcvbBwypAdf2Ng1l4u2KeMwENWfV5BDPSo3MSbmDGyxbwHd1wFAnsY4/FlkJMUOqk7RiWo4DCaF0xTvHdSAc5z9zxt6J51HHO1yTUFOqN8l4pz1OLjXwkh7ocBMLLJU1k6rxaFEtvfG5VfYiV6H3dlinLN+DVKeTG9sbMwwoHkx6jDUHNYHM+N7HXa+mAvj2m1gZPfn08FjFYE+k2ciFbveeAQrwuh9FRjWzZjXNzt1/QIwsKk4S6aY3WeF+bhQ+2Wnpw/CxiAloP5z+lMcUDLUjzg586zc7OwbAA43QVV6tVBiFKSlVRj071gW3e5v7IKcVfD+5ZfAc9Q77RwXGej3x3AyOqjao1vw6W81Cltrb/Qu/12ANNTa8Wyt4wLZOv85fLys/vYY8wBn0NoZfN5lMXEmfW7/OqV3dr7+TefVbpStYQ18cjtVXa3MD0NusUHuY/DvtZnigNPCIfP2WHyfFiD9df63PbrYfyerxOQ6ZjFbfLZmsw+8z4GXwNsRfDp3fv9143bi/6ESKuYTHDVUjtIiRPtQCZVQCZVQCZVQ+b/24IAEAAAAQND/1+0IVAAAAAAAAAAAAAAAAAAAzgL4cn2g77WR0AAAAABJRU5ErkJggg==" />
                </td>
            </tr>
            <tr>
                <td style="padding:25px; background-color:#EDEDED;">
                    <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                        <tr>
                            <td style="padding:37px 47px 10px">
                                <p>
                                    Hi, ' . $data[ 'lname' ] . '<br/><br/>
                                    As requested, we have reset your password on "' . $this->getSettingValue( 'name' ) . '" website. Here are the new details:
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:20px 47px 20px">
                                <strong style="display:block; border-bottom:1px solid #C1C1C1; padding-bottom:10px; margin-bottom:15px; color:#2FAC66;">NEW CREDENTIALS</strong>
                                <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                                    <tr>
                                        <td style="color:#C1C1C1; padding-left:0;">Name</td>
                                        <td>' . $data[ 'lname' ] . '</td>
                                    </tr>
                                    <tr>
                                        <td style="color:#C1C1C1; padding-left:0;">Email</td>
                                        <td>' . $data[ 'lemail' ] . '</td>
                                    </tr>
                                    <tr>
                                        <td style="color:#C1C1C1; padding-left:0;">Password</td>
                                        <td>' . $password . '</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:10px 47px 37px">
                                If you have any questions or need assistance, you can email <a href="mailto:' . $this->getSettingValue( 'email' ) . '">our support team</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding:25px; background-color:#EDEDED;">
                    <table border="0" cellspacing="0" style="width:100%; font-family:Arial; font-size:13px;">
                        <tr>
                            <td style="text-align:center;">
                                <a href="mailto:' . $this->getSettingValue( 'email_booking' ) . '" style="color:#FFC200; text-decoration:underline;">Contact our Booking Manager</a><br/>
                                <p style="color:#C1C1C1; text-decoration:underline; margin-top:2px;">
                                    Manage your Bussiness on 
                                    <a style="color:#C1C1C1;" href="' . HT_SERVER . SITE_URL . '">NomadSurfers.com</a>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center; padding-top:20px;">
                                <a href="' . $this->getSettingValue( 'fb_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1Ni42OTMgNTYuNjkzIiBoZWlnaHQ9IjIwcHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1Ni42OTMgNTYuNjkzIiB3aWR0aD0iMjBweCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+PHBhdGggZmlsbD0iI0MxQzFDMSIgZD0iTTQwLjQzLDIxLjczOWgtNy42NDV2LTUuMDE0YzAtMS44ODMsMS4yNDgtMi4zMjIsMi4xMjctMi4zMjJjMC44NzcsMCw1LjM5NSwwLDUuMzk1LDBWNi4xMjVsLTcuNDMtMC4wMjkgIGMtOC4yNDgsMC0xMC4xMjUsNi4xNzQtMTAuMTI1LDEwLjEyNXY1LjUxOGgtNC43N3Y4LjUzaDQuNzdjMCwxMC45NDcsMCwyNC4xMzcsMCwyNC4xMzdoMTAuMDMzYzAsMCwwLTEzLjMyLDAtMjQuMTM3aDYuNzcgIEw0MC40MywyMS43Mzl6Ii8+PC9zdmc+" alt="Facebook" /></a>
                                <a href="' . $this->getSettingValue( 'ig_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDU2LjcgNTYuNyIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgNTYuNyA1Ni43IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnPg0KCTxwYXRoIGZpbGw9IiNDMUMxQzEiIGQ9Ik0yOC4yLDE2LjdjLTcsMC0xMi44LDUuNy0xMi44LDEyLjhzNS43LDEyLjgsMTIuOCwxMi44UzQxLDM2LjUsNDEsMjkuNVMzNS4yLDE2LjcsMjguMiwxNi43eiBNMjguMiwzNy43ICAgYy00LjUsMC04LjItMy43LTguMi04LjJzMy43LTguMiw4LjItOC4yczguMiwzLjcsOC4yLDguMlMzMi43LDM3LjcsMjguMiwzNy43eiIvPg0KCTxjaXJjbGUgZmlsbD0iI0MxQzFDMSIgY3g9IjQxLjUiIGN5PSIxNi40IiByPSIyLjkiLz4NCgk8cGF0aCBmaWxsPSIjQzFDMUMxIiBkPSJNNDksOC45Yy0yLjYtMi43LTYuMy00LjEtMTAuNS00LjFIMTcuOWMtOC43LDAtMTQuNSw1LjgtMTQuNSwxNC41djIwLjVjMCw0LjMsMS40LDgsNC4yLDEwLjdjMi43LDIuNiw2LjMsMy45LDEwLjQsMy45ICAgaDIwLjRjNC4zLDAsNy45LTEuNCwxMC41LTMuOWMyLjctMi42LDQuMS02LjMsNC4xLTEwLjZWMTkuM0M1MywxNS4xLDUxLjYsMTEuNSw0OSw4Ljl6IE00OC42LDM5LjljMCwzLjEtMS4xLDUuNi0yLjksNy4zICAgcy00LjMsMi42LTcuMywyLjZIMThjLTMsMC01LjUtMC45LTcuMy0yLjZDOC45LDQ1LjQsOCw0Mi45LDgsMzkuOFYxOS4zYzAtMywwLjktNS41LDIuNy03LjNjMS43LTEuNyw0LjMtMi42LDcuMy0yLjZoMjAuNiAgIGMzLDAsNS41LDAuOSw3LjMsMi43YzEuNywxLjgsMi43LDQuMywyLjcsNy4yVjM5LjlMNDguNiwzOS45eiIvPg0KPC9nPg0KPC9zdmc+" alt="Instagram" /></a>
                                <a href="' . $this->getSettingValue( 'yt_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjEwMCUiIHN0eWxlPSJmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6MjsiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHdpZHRoPSIxMDAlIiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnNlcmlmPSJodHRwOi8vd3d3LnNlcmlmLmNvbS8iIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48cGF0aCBmaWxsPSIjQzFDMUMxIiBkPSJNNTAxLjMwMywxMzIuNzY1Yy01Ljg4NywtMjIuMDMgLTIzLjIzNSwtMzkuMzc3IC00NS4yNjUsLTQ1LjI2NWMtMzkuOTMyLC0xMC43IC0yMDAuMDM4LC0xMC43IC0yMDAuMDM4LC0xMC43YzAsMCAtMTYwLjEwNywwIC0yMDAuMDM5LDEwLjdjLTIyLjAyNiw1Ljg4OCAtMzkuMzc3LDIzLjIzNSAtNDUuMjY0LDQ1LjI2NWMtMTAuNjk3LDM5LjkyOCAtMTAuNjk3LDEyMy4yMzggLTEwLjY5NywxMjMuMjM4YzAsMCAwLDgzLjMwOCAxMC42OTcsMTIzLjIzMmM1Ljg4NywyMi4wMyAyMy4yMzgsMzkuMzgyIDQ1LjI2NCw0NS4yNjljMzkuOTMyLDEwLjY5NiAyMDAuMDM5LDEwLjY5NiAyMDAuMDM5LDEwLjY5NmMwLDAgMTYwLjEwNiwwIDIwMC4wMzgsLTEwLjY5NmMyMi4wMywtNS44ODcgMzkuMzc4LC0yMy4yMzkgNDUuMjY1LC00NS4yNjljMTAuNjk2LC0zOS45MjQgMTAuNjk2LC0xMjMuMjMyIDEwLjY5NiwtMTIzLjIzMmMwLDAgMCwtODMuMzEgLTEwLjY5NiwtMTIzLjIzOFptLTI5Ni41MDYsMjAwLjAzOWwwLC0xNTMuNjAzbDEzMy4wMTksNzYuODAybC0xMzMuMDE5LDc2LjgwMVoiIHN0eWxlPSJmaWxsLXJ1bGU6bm9uemVybzsiLz48L3N2Zz4=" alt="Youtube" /></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>';

        return $content;
    }

    function activationAccountMessage( $data )
    {
        $content = '
        <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
            <tr>
                <td style="padding:25px; background-color:#FFFFFF; text-align:center;">
                    <img style="width:250px;" alt="NomadSurfers" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAYsAAABXCAYAAAD1a2lrAAAuWklEQVR42uzdb2hVdRzH8XP/ue3u3uvdH9e2dP91azDm1BabKQtqOlFRmyVTiqT2INAI90iy7GE9uA/S8EHSIgh6EEVJIBJB9cCCKMyKkFAzliSR2B902z339rnjd+Jw2GX3Thfaeb/gy++c6+7dFPf7nu/vd87vZwEAAAAAAAAAAAAAAAAAAAAAANweAjMBAIBH0IRXYOZ1AIDvhSyXsbGx8lQqVWmJS9gCAPjTzp07Q5Zs3br1rs7OzoM6/HTRokWXEonErzo+E41GjzY3N3eTMADA5xVFOBzui0Qi53WYdUcgEJhpg8HgVF1d3bMkDADwaaKoqalZq+aaSQ6Tam1FxoStmDLHWSWVsX/fCwDwh9WrV0dKS0vPmkpiyrSzha1IK5lk6uvr+y1h0hsA/DOhvcckg2nT5g0lipmvSSaT71FdAICPkoWqhBPuRDBH2Lm2pKTk54aGhhaqCwD4fwsogocPHw5qCOpzkwjSBSSLTC5y8xqaDO+lugAAHyQLE6eLTRaKSQXJAgB8dCfUyWKHoRQTijaGoQDAPxPc+5Qo5roTKuueBNcDe+9TVQCAPwQsGRkZqQiFQhcLuCMq4yQUVSMPkSwAwGfVRTwe36IJaycpTCpsT5KYdhLFsmXLjpAoAMCnCUNrQu0uKyv73VtNuM+XL1/+SjabDbF8OQD4OGG0tbW1qspIaQ7jnNaCmtbwVC5ZXIzFYu8sXbp0E/tcAID/BEySCJrziMKR3LhxY/vQ0FBnX19fpTupkCgAwD+Cs3X6GmIK5Pt65igAwF8ilqFbYB/RkNOH5eXlZzVf8Y3Ov9SQ07FVq1ZtYcMjAPCngNPxd3d3363mA0U2X9TW1p4YHBxs5sE7APCPsGW0trY+qoUAf3E9V5FW2E64b5NVtXFBbYcnYQQUQSa6AeDOXesplK+a0Jap8bJo2avmae1Cnti+YZYBOa0FB73Jwfv5ESa/AeA2SwieyD8prXC2TFU18a2rmrDNcUHLe+gW2hHXz7BE0aFbbKvN3t2zToaTPADgv00OYRN5HThwoFrDS21NTU0bdJp0f4b2zT6Ye2bCW00UkywUxxWO5xW2Ko4zmhA/uXjx4mPaDOkZvTbY399fo9YrTOIAgFvFe1Xu0d7eHlfTpKSwVlf1T+j4ZcW7Wqbjq4qKigl13tcTicQXeniu0pLe3t5m/dnHriex0+Z4PsnidUvMKrXjeXbRm1QFc1nHHyk5HdJ2rQ9oY6Vq73MdTJYDwPyFFBHLZXR0NHd+r4aPntZ6TG+o4z+t89/m6NwPWdLT09MYjUZ/NJ34lGvZDttEUclCn/WaJZq7CCsxfeL63LSpVqad7+GJ7/XzH6murt48Pj5e6q42SBoAUGwlYaxbt65OQznDujI/rjinl/7Ms6dE2hMZdco39LR1v9kq9UnTod9wvS9tXitmOCpt2jFLpEXvv+De28IJTwUz7a5k9HexNVz1Q1VV1XNqW0kaWGgBd3Dr3sKPlw8MDIQV/EIv8O2sqhru0xDSEXXEl2a7unddxWfybUikIaCvdeXvfOZbTmfvaa9omOopdd5OhZKZa2c8fa7d0dGxwZKWlpYHzXts0xayWdKUaZ3hqmuRkshRHXfw4B9wp68X5MECcguzcJ867i5NEr/t3KY6y7MPmUKHirSX9guW7N27t17Jx50MMmbDop+6urpatm3b1qrjQqqKjLkT6oozcd3Q0LB/PhPlJmzPfhl/qNI4unLlyiZLzJ7gXJTg5j22fXvVnh076nS7Xu1MbNpUu2vXrvpUKlVm4VYIWsa+3bsTanrX9PQMD6xf/3g0ElnjrBtEwrgFCVlWrFgxquZvz8ZCdnEdsLnKV2jo6X5LGhsbH1blMNM5u7dL1YqyL1pSWVk5bIai7AK3UP3M+b+hu57edFcq84yMJ2lc1s++35UoIhZwM79ciVjsVCIevxovL5+Ix2KXy6PRiYpk8urmoaFhS5RAWOhs/oJmUjV6T3v7S/q3Pa/TvzQ0kq1ZsiTX4VzXcMR3em2QjW/+Ye86oKQqsjbvdU/35JxjM4GJDDDDMDAwpAGGHAUFRJK6rP6uGFbWrOuaRRBXBGVd18iCi4FVdA2sq4LZVTCsa0DF9KMkCcLM9PxfNbf469zz3vR7PdMDeLrOqfN6pvu9V3Wr6n51YwVYaI6SZ9OvQU/GOAOqzSQ1vAojcgy95k9S4lDBJCUlpV58iTxQCwksmq1ILGDkt9Fzw1Hf5faKjgINRIv/s7a2tozoFHK1DZV27XjfMpp0gwYNmhUCi/aD8YIFC1LBSN5UXSFxfRX1BdRd4iQ17FpHhcCifXQeMGBAgthNt2eHzhk6QP2P4tmwL6VDcviYxq9FPh/j+hZUUAnUjpvlvVae3bVrV9/6gvG8ECopVRJq7aDaoqi19qDO7BIqodJOsHiV7ZjE4SytWCAzQ2DRfv05dqbSKOoFg1g5ZsyYfHyO7uLxhKeHh3twvsEpGzduDBkiAyxSzTJ37twq2Bc6iuF6AeJeGJ59BxNBehitSA1eqYKCrWGZbAd28A9aAAvZtv1IKNhH3IfrNFW91ZGVtwe2jEvXrFnj6hIqoRIgWLzGROBmMXmhUw+BRftp2xN1t6ArGM89RjvikL2i/WBBBujeBBaBVm6A/kzkfSJ7xJXcAA3AaIbKq788rwIBfS9bkGqkh9VWSCXp1IVrVKYejCpdfZEmvXXIkCG1XUIlVDoQLEKSRQe4byLoa4FvsWInmZ+dXST+hwjcsNDhN8ctWHiFmpBiGJYqUd7nEtM9qIDBFnJ/7jJ9+vRklA/oN6YSgpRI0tLS/i6fjfs2qN91ZKX++AAO8SK7YCcZ1iVUQiUEFscfWOCwm6U+/bTH884keJ2FpIjgggVUQfbVOcSouUQAe0Wjsk6iUJ+RYEAM/w8KmBTj3d+wdWSqEtK76FfTrZl43ocdaNzmffKS++/LDQ0N3UJnaIRKCCyO38CwWwVN4Rr5d5FmIhRTETwDN861jgWD3GZHpUOJ+1SQ+Boqpy9hr3gYn+OZ00Es0n3Mw+783+K3yCVVJxuA8R0EtZWVGI4W3aG3pmelTxP3IcVHPS4H1biNDqjNSlbbJjhP3KQEFYZ1RFpePUiTWCNm46todBDeY61/du6ldgarzbJtQaSLeT+CDBYaq7/ISHc7YEGG0b/T/7RgRd13dv+PlznBGPpJCtM8ZMaAuW0BBuw1UBmeIjSHigFYJ+aqMf7xCpj8ewL8Jd8EgESCMW9q22ZBQXxu197hw4f7VJLIXHtqR9krBOip70Z7nkPMSQ13L+ZRorzqjEHwScsP3NCDleFRlkDzsnMma/Ac3ey5/Dub32t222v7/fSOoL+H3tExYME3BFTanxHT0U7mqtuZy4zuVp7jMPmfrG7631Ki6ZPkT++kar5GA6ez08iPvtP6z1VD5r9xyHYGSVIWwXE7mOqnhQOFpmutIk9UYUXhCBaz4TrnnHNi2bMlgNTS/ZepacLJPlVvpk5i/98id/gZaRl3tdNe4eX5omg9T5UND2gd8UlWVVFRGgFrfmZqat3Afv26k6jCftu+lAZzEBE8duTIfvjYV6QABgPqedFFF8XwXZj155sXaWySA16Sn98dH/sX5OXV4TsPD+Ixa7egA87IrUDK4P7wWqgbPXp0T+w2HNbba35YivScEGmOS46IsX3Li4v7Txg1qgzvDefj1Z68OKLNo/BcuPcJj426YQMH9hLBYez5AYMFpzm9MwKiego+JuK7ZFM6UAl43IN/v8b7Tf1KO++88+xEtl9HNH20ne10MmbsqqysTBV0xlxNxZzS2/p98PtvPsbjxo1LFu1EpHPKypUrI/n8CYaEgZ1/Li53ou4zMPYeIgb9dnl5ea68ETaIBtQVUNs8j9iJzdDz/wV/nyEyzcrfwKPtKnFvaWlphQpQeI6LvKaWKgBlJsksl8+DRHJlAGDRwm0sJB39E3aTGXIecb7GS3fUB1DvokM1VqI+iIW/UP4APt3FIMINuqZtjQwP34GX/uR0OH6Ki4nZCT3d1sTE+FvhLlas7A40u0CUiuyHuPF8PHdjYkLCdjDdvSJfidvl+gmRwTuRHfFTREau9+TkzJ0zZ066xfwl8ruzUR+ivt1NUZUrUD004Pmw+v8R9T+usLAfRXZJ8d7oqKjvwtzul9xhYWdjkUUpbdZEpYlbgUjmxS63ewsm2w8O0EXD/SnJyT7a4H83KaDjtMu8YTCrweU2tO3diPDwb8Pd7p8EXfD5p+SEhB2xcXEfx0VFPZgYFTWC2mQPSIlG4jD4nKycxRjn95MTE3eg7eI9PtrDde4Th67fRykMZHnTDljwviFuIAHxA6fh41p4dvwX129FdbqcXyckJbzVNS9vWe/evfv4BUHqM+ZHAi5L8MzV0yZP7i/bYHN3eQYdnD+Ov5O/Dy6PSYKx5GZn3z9l/Pje6nMqKioq01JSrg93uTaDjiIr6KdYkB+h3o/P1QrzTgQoLxHrDrRbQWtwGeq/iaafE/NayepyWrPTeTs5nWfNmhVVXlIyHWvnYYzjFqwvYVDdHhkR8TWiwd/G5zvgaTVGUaM4rYBTPIpoR1ZGxv2gcx+1/2VlZeVpSUnX4Z2bUI/2H3NLxBRIWml8jPpXVdWLMcTaeQv3fS3nRBIC2xKjo9fVVFXNhEE6xi6w2V1zVX2rSjOyMq5z6s6PVAYucjxhrhbTz6Kx1u8yY85Yr9+B3o/17dt3ZhzWKP5+xwzcKUDvEy5hqICAOXWTcn8k6vttqK+8LBtuC2vbdlTBB+tILWZZCzLepMPPii+z0tLOA4Pa7w+5wFT29ejRY54EDKuDA8YfjwFYjN34Twb6NMN35eXkfAPm/eujOw08yw8YPWmEtBjMcniejNVY6mGTdmyFtNBbPpdoc4mVJF7hmGRor8KA/NOlHCdtgTmvwbu9VukSFR7+fHVlZZXF3ZcuP/SsrDzX7XbttrJDwU1iwUeg/ss2WFDfKkpLh8dGRX2kPregq+cHT17OF5jE+9T/5+bk/AXXhDb6pNPuLAeXXd2KilqnTplypkUa8Hmymt77B27Y4+8D4/OI33pyc1snT5gwQzLmpLi469vyUEmIi7tZAYt8gFsTuVya7CjNK+55QG0n3wD0rKgYnpqa8g67bz/mz27+fADHpu4lJf0sGDR1CibLFvdlZ2W1nnzSSaeRNBCJzdEf2mp7YmzsrUyt1KVPSUkS6Pag+ru4mNi9hQUFXyQnJuxgfObfSEMxJEiAoavPhFrJjUsjAP0m0PpdrMf/kbF0qA8rO/bDqGL+N1NW2ia+VpEp9nzZZukKLU7Uk8waO/yTTNJ+eOk53/okHyoIlJtE3/vejSrf22zCwz7BBvw+AN4sdoKeLTo2KoadJiXj4loswCuwo+Q6PC+rLUd9cpE9MTstbb7fBtB3hXl5VfGQGHiGR+Fmhnr0HeKzREp1IHKzstaD2HHsfUZM4BG65yARV3zeVti169nJSUn75OEhJv1rlv3DgO4ZOHCgz8hUWlR0p1zgBBiGbZb3QvpoheFokhET46BXXFg4HrvSHyXTMKK9xugi2kCAsQfXoew9pqqLqIiI20TktUIDM9o3Kbusp5W8NF4rYCH/HtCv31QnUlV0wRjER0dvLe3W7fS03Nz8sWPHZk0dNSoFasccbDoGJMbF3Q3gkDn/38A1w2QjopHfeaZvTPPzW6dOnjwrQLCQp45dzOaUEbMU4LQbYCbAwjeukLTXSm+SmMjIDZD4LqytqZlb3bPnbPz/94X5XV8cPXponuJIEI1dpQCa03HPXHFFPVnQl9rxDha3uHceq3NQf431KZm7ztsHEPtNmNPpGxvklvrS5XReAzVmPVQh4v3ZYFYFWD8NeP5i5Pb6kVwlmyvLy0/hgGFEbxzAI8ZjZ1ZmZuu0iROn+fofG/uQD8Qcjha88xnR/xr0H++aLYauID//X42NjYXqfMB3cZEu1+viPkj1B0q6dbsd66xnBebBrEmTUvF9BlQ8pcX5+QvBXz5UoopP8wdsHWm/I81COGkiLmXGcH92AQHO5epc69+/fwwM1RsRiV0u36EAUBOXLoj/rFZtIVgf6wxAQcy/fRifrzGe/xB2EmTTHYQ2M4AIzEFplMlO5qBETolcxGh91UBckvc3A7kG+hOPSRzdRfeqRCfGRH/LNuDd8n9gSj4mLA0zwv2NLxr2/kcNBmG/2kdFp2fcPwq6waJ7NCE29n9Yqgsvu7fZhDY7kYumG2PYvK0TUQ+p7xSVAJTTRTBotU+H6be7KoqLe3Oa8HdBpXidvI8FBXl16o8EKwlYrG+8LRwsuJtidnRkpC8PDxjdqqvIFsKLskDrcfExiCi3+0U8O9wsihnMIwuXrwRYTJsyZXaAYPEXZogMawMscoWaTjDLOaeeOjwzPf0Wn7trbOzLws7WTmYlx2WdnZtkf/M9ngXQBvjGBYx2w4QRI3Lauq8Bc7Kga9eNXWgX3K2goK2cU7oCFnvT09JaoR0YCXXUdSQ5bYa0NNAqzRFLci/19UOkx65p6waxztOxO6Zo5laoeAYr/Q5G0bjTxLBhw3LBjHdazMkkc0FtkLST6wI0qqd1sEa+DOCRhzX0o4E6yiv/LioqGiN/D61IKcbhDWyUns2BBA5QEHEYp0ECqhMaGyPea2K4DhwsuFHEHzFklYyrKD//NQPjmTC1O0hfXQqd+065mzXKR6JBwsGE34Nn7pHvkUyTM3BPXu4LzFbCmcA69nwzn2m/qgBIWocxWVqUQbR2L0k0ifHxfzbYtTrIEDYFO71D/LliDFgI/m5hu8Du7cizpSQgvifAgMT0BhnYNVH5GNT36zdRo3YaALOXdLSiv60a6xfzC7cCFmFkmLuIVB7vjSAbUDW+kzsc5h7spkVVHIustsT4liv0MgeLSZMCBQvJuC71BxakEjgAZrd/3JgxVwuALy8rexnzkDYupt5LmoHEJasEw9ukNxTRIYw/j3sbSYnLk51dC7uEb+5Avfg41uFRtYfSBl2hcxjdHwlbwRPiPkib28s9nnSlv2ZgsR82m4OTxo0T/T9QVlz8Gp4T77f/9Iz8rKxu2JT4znqAfWo83edG1fmcqFbGoqAg/yHSZHwNlVRaJwWNOSkn0yKsQatuq00UL3MBY9ZdwENWSUkA6+Jk+RKovBZiHRk9v5mC/t6WB2BJiYfUZUZFN/NYCwZYyCCN77Fbug8oeBX0XpdjN7EMHfrCiAGT2kYM/mA5ibkuFd9LMfsQJwZ+9JlT1y/BqhkCdUQviF69RBuBnjeDQR5Qd+rqMzB5L5cDYgMsvBJ8IIp/UFRQcCd2LJdAD7sUIvGnbRmQ1OehvTuA6sJofgl2B9dD/NtM7fTyeyJg+KquqChgE9xBJ3QtZLT3yr6C+f8HuwfRxyGoPSrLymoxUU7Kycpa4QDgMv1mEwUAzTGgiSaMmbnZOVsMaNkigQ39WVdeWnoZJucM2IjOSoHHBsZ8G40R/c4yWDjICP1XSqR2pTUVAon9KSliMW3tVVl5Cp3joAULLGxKFvsgWRwqKyk5KAyUSibRsI4IyqM4izCLC10XtIHTwz/FvTjz4h2MQbQVQ79MJTK+oSEN60DO+2sYbThYZArJKi019XD3igqxLrdBzZpsqf/UT9gMffENYbq+BfMtUY6nv3GCBBKPdfpCQnT0negbuy9oRa7RjRZtSl7iS/uhYi1SxwGquRw4SHyl/PZzrAk5dwSQbDThV4cpX9jvGJ01BfidZuqlYIJFk4bFn5KU9IhPH0xFPZ82JTX1GYN7m0hMvI4tACcN9GyuYvExGgAMgUiyWUORlrdXZlraN0zC8F2F91IPeGFIercFFly0i0tIuHEYdKfqu3rBZQ/eUP/wB6TC46EX9MAGu8VbDO49TGmGT5c04Yuw25GjErdJGhFzvZcWhWFJT0qa7Twi7XjZjuZxI1CC5DeZH7gi6YmNwHfYrY0wek8JDJFdYFQlwGi2CBaaEouznvp/jmWmSkbHwR4PU0EdW7BAyRVegZKGWCMz2D0dChZWXGQxPuPgReSbA1B3TlWAwG+RbpxYX7+Rdo758+cbMWJdSTmxV/Y/PSVlto3+h5FH1SxxL7QMG6FTj7IYgKdTqowYce1MoEDpgbrHjgoKNHpStlvOR2yA58lNneQJkFTvUjwg+wAwDvP3SFpjjH8Qp+wx78zgFw4W6mfol59VJyRNPFFdMhc8Ovyp7Ii6U83MyPDdq4qTQi0CZH5F/b32/+/aKrM1wrfaLcVWrpYASg8WRnhidl5VlZUHrwXZVgs2C9nf29VJTNVN6rJ87CB3G+nnCRBfUumj0kZ4OUBX+QabOL73g6FxdQrfcaViF+4DuESaRFQUVQJNPlp4xIi5Wuk7iLjJagAWHbhyn9oeaXdxhTkPIX5jCOuPk6pLNgLS0Qs0dk0WJQuddLLrjqgN8x5UDHW6XWZ6vICFZBxw7Xxf7HZle44BWDiIvqtIffgKNlYRttpDv8NGIRuX3bBntcLldoo6lhwsZGZcuFp/COO5nRxWTlLlzTgyx+P3DB06tEDOCWttDT6j5O2FpP5bG/ENTbSJmMnnLoDgecaDWsTagYpLrj3hmn0tz1irvhs84gE2dzsdLLgevkHZdfDiIs+A8yRxVLCA/vPts846K1qdbJgQfYTHBUdLQSh4Jk21MFlctChWSMIx4PlIiYfQ2pAsvMTsvwGKJ3Kiq4wGO6wnDOjToqFGh4cPNWmzi3YIl8v3qs+IjIp6ip6vmU1M8oqaR0FNGj3TwasENhj+fsv7KLychLsvW/CpqB9IuqlgCyOnBE5XW7vPPlVVQ6ES9NowcIdRtOrvFeeJUazPfrwz2P+PD8liF0ljt7Ox6wyw4HSJR91CEuXldqQcPsZJCQk+jQGM9kxdyCQLOGz4bFCJiSts9l9Xduq7yRPrTjYeNuZD8AuM626sZxZb5Pf8ie+hXitktKlQN3SMt7ynaFaiwJ+MYyno/dhQDpf06nywIEZIouFHmDjpbQyMg3S3tdLdVdwPhuG7v7hbty/POfPMAgY2FxoxbaELJ6nC2sHqHs8g3MMJKN7tRfCK4gVkAhZ0BcO7kxObvws2mivkPaokBKPrOxTZrJkZ1mMcjgk66MKlqKT4+BemEk14sbHj5oE9A/luBWMi9JvD2ILvqYyzyvD3o9YyepgudBynaieCW6O5kiVPJQNddsLWc5FwmTXz2uA0ON7AQjI6zJEF7PedCRY6qZu6g/5yLKcpNh+nv8okag3u16uoX/cp9OH9z5D9Rz1Htt9uX+E2K2ku1CurSwsL+xr1kcbScawSZ8LluMpfwj+++4dU/6iahobigW4x0HK0kH1juwdFSUM+hr7n720isLiW0b1zJQvJhMFkXhIncXF05141EFursbP6iYh01GAMP/nttT17FknmR4S614jxpiUnP+zPo4FFfcbBh/x77ufvxE5a+O1LAhqCBe2kyVvkt0bEZv871wBo0OaU9f7iOyAqDlXdgp3kRdWnpubzRYgPMXEDdUjHABHVDvCb7IFBGGLpChFta1CXk/fMakkPxsSHqGMASeUUDirk3rdVBFVZVXmg3MCCiAzBgt+HZGiTwpUAzPj4uG2QgB4QOm8RSS8wxWAc9OMULGQ/JsnndDZYHA1sq66eSrrvnTNmzCgKtAEUzX8HObY87Acs9kpwst9/WsuNjYllpaUviueQ7fEA1s1G2EovhW2rDzaaiYYePp2fCfgaG15QzRQLMkfOIxlbAQlqi4F00kSR1bcpv3eSI8Eq5ZkcjO49dmChMkVNe5w1xHCSnn766dWV3bvvJ7WHV3rKIOBqe7/y8qMimPDUwOBvlICkvgsBaMwYbslCtonvbOm6UHkWt1kcVokNb6uTOVPhkyQjNfV8I1XSiGENEiwcZkBa06vXMEhohzlYYGGrYKFxoHRHRo6PhaoKXxwQExTVSvZIswRkqqpMeHMsMOqPBj2qpQVP3/fo0V1RP/oFC35eQyU2DmvlzpQtms0ZaWlLIfKfAiNmJnuvTbAIfpwF1K2tQ+rrp7D3daZk4aDgyuk0D/aQJ9NFqJegXmyjXirUylBHbqb3r+a053EWsNe0NgwefDIbc1u79gsR+Q4nDiHBf2Qwtz+Do8kD4iCh5Li4ap6VtjMy5k69aqorJjbmdTteUCRBx8t5RHN0sqHNgzaw+L6WAyLU6kMpaNZrABbPM7vwsQEL1PVWwGL27NnV3Q3AIiEmZnu/qio1WlNH3Sh3o2pcBoyDN9oAC52uT/N2ixOvaqqrl9PCNQYLuofaMc6fZAGp5zwj5jpu3NgOAwvpXrxkyZL4vNzsR/guRQmMO9RGPWwFLFDOMOpPrx49NtoBi5EjRkivNstgwWmN4M0KFJG25TFITl9jVyk95KRaYhs2EvenJSR0P14M3Bws4FQhggA7HSx41P+oxsZroeP2jbvIFgC7g0iNIRwkbFVkVBCeUMJo3YqI9L/5A4tsBCWeMnVqoGDBtQnJMJRPTIiNvgObhfcjoqKk5CLTZ+8C3V+CeuxUdn+wigygGy5ijiyqoZpp7qoqPJ0cS9aYqaAwdpvVcZb3IAShGP1uNgn2EzEXBEi/ELCgcxGcsPa/KI3gKqNB0jhLujdGlOd4u0W6kaqePa9VwYKroVQDPto7IVCwgOG4w8BCTozYiKj7HLpDTb3iZX20Xs3B4kyj/oDp2ZIshjc0nG0fLMxVCchKmljWrVt9VEzMBRnp6YJJfafspPbBaDtbjsvxJFmIdB9TJk7sdLDgc23QoEEX0w50R+/Kyvlitz+wf/+ZAwcMmGG74hz1QXV1Z8Kjqo4zIg4WiNwW6VUCBwt6Pqe1UIGDnsIYPBNq/OVY2++oUjQyKaxVVJZaUAPxUtNvlLt/q15QMGxPka7LlIzVg/Wxz0iioMjsBfKdTOWeiHH91kjFjDH4NguBjQw0T3CwoImAQV/HGEwTRfTeYUfnKYLKUD41UUOd3ZYaiv12vG2woOvYsR0FFpQny+MZLNvIdy8wBgtXud3os3DFfRl1E+orVDeBnv+CrprlajIGi5Kiol8ZgQU91zJYIBjwUutqKPPzC9RoVLUgWrtrj+7dFyE7qs/jiOxgUyXNTMGioMBWug/2vHtI4rz0BAALDnSCYQvJ4IeJEyd6OvpEOVOwyMwMCCwsnM3CS1z/vn1HIQjwZWVtPELvDBpggOlH+DK8apa8oFrIs+tzeP5lsk3aInW9MW/EbzwoJkzfje/fZ2vaS/z2MG5jTiknPli4KfHVjewdLbTDf82O58u4ESNKMCAHOHMUOysEtY2WxDOKszhewQIeUktNAgdb3Q59lT8GAOMZ04cag0UdAu6MDNwYmy+gH822GkUL5rwWFwsGbvt5eFSa1kBHCcb0Cdm2vlHSO2hGYIEYjtbxY8ZYBwu6/6mnnnJXV1U9QyoEW5LFtOMALGAIbiQaNSG300QZryTaFGiVfQ0WWFgNyOR0JVD3kgp7Nvm6d3gGWso6MJTshrSO/UsVqCulVEFqZhdsdG8YZEyQGYfvM5OaBU0pWNnQyA0VGVOln+BgIUUxkdES7qocoYW+7nBDfX2lBXEqjIJ5zpaeCQww9sDbJtOP66y3nWDR3MFg4SCOxeI5KCYjMvwZNoH5UZNuunKvCQ4WkvF1Q92tsbQlAmiRIHG634yjdBYF1AJf2ck6CwcHu6cG6rJviMEZ4dD1Q5RDaAqju3yeDyxc0C0nxcbOsqvWnDlzZixUcZto8V5+woAFtWdATU2+cIgQYwCp7/wgMJBggAXp8+2rqsIoU22k2/1YkNQwDnJfPV3JytzCTtAzyjMn1ENj1Q0aVKiDCSS4zcMr7Ez5ygbXqA0Y07vlOuXqK2wSFv6iwEL+dtKkSUmarn/bxYBRZaWn38MSifESRudfhCN/zbsSpdUrzpeQbdb9RHAfP2BBbXVwGwwZ4dOTky8wDIyk/9EOsszpDDvoRw3loLGIACj4RHmNARO2MJuV3EthDJh0maupwOO5iDzm/Edwtz+gSl+2bJkIiPqPeA/E7ouNwEJIRTDKbvdFt8bFLQjABpaC+jFlXr3iBAILjZI0RoIgL/n876Oj/0qLNGAbQieBhX0jNY0J7JzX0I7/vaB5QqEsWrQoDjmvrsAG9RWecVlmZWbr7guRk4tlZFhuZtiG4XyLkDxM5wOdjKfcz2MtbvrFqKH4Lr+4qGgJMxZ5RSUjz1xGKFnDlFn1R13XDFN35OflTTZJ97HueFVDyfthsFtvNA6ISbhBgqiiHpBVnrLF3PpMwILuQRrtK4wmn1h82EkhaMi8DB8yZFBSQvzPMq7GUm4oFDgE5GFO3AjJr6eNA7I0UUXmVCTrk3rb33GwkD7sAMFX6ICcZTbAQroo9lXcj688UcBCbWdqevrVMuARu9WiAHfcuqxBAgvN10Ecs4oEkfOrevc+zw6zk+9AMOdlsGdR1HOQCx2EBFWnyEBxqWDwkAha2Nr5mWVGdlI+u0TFWcNroLLyG+sFVdN8bmBXnHQe+aUZuHVVXI6PjdnFspd6ZY56pK24ypewjhXkri/FznG1yghVAsZERTyv7Io1DhYngM3idhM10o9ZWWkNJhyiLxj3WxwoOFhwX2xM+K4ybz5PO0D2i7/hN/X4nPjm+vWR4iB6cZwkAucWQc0jD4uylkiQxj05LuEp8uBYpUiQfgvpfCNhuP1Y3A9XzXMM6O4kcf12Opnwv7DxWHUpdJFb41Kak60Cy04wsNApvXU55pgvdgWHCN1rl84Gc1nrcLCgd4gMwmK+oL3fg/ZplgGDxgS84nqah68G2SNKMzrLnDIjLMH7t6rrADFEIyXdaa3NVcMFWOaKveK0TrO+y/kE/jqhjQzRb7LbTnywYAnEFviIRYyeh7TjzOvP4HcvGPxiEPQ26CU34H+7VIKxz00whLId64kFFvCemKCAH/OucItdy18rKypuGT929FJ4hNyKxfosaH1I9scCWDhYX5fy7L8Gnmof4nSzLalJSVuxOP9XpTuq1fMsHOTdNCbMFeb7LU6PO5WrHHlRF1xJYeE4ROz7+oe8VPXKnOIBlFPkyY4pKUkSVNxtMD03ta0fJLsDsTjX3DcvTyDJgt8b4XTeIhkSAGO2Aohm9iKNxkpmZBgBF/fXADxlsr/BkCxQYuFI4FMnFxUWPkEbPbJvtmnM11auXBmGzYPYJInN5XKrQBMMTy2Riw4AUQ0D9p/gxrpYyfLrpHnyhIG9QSYZfNSs7ez//XDPQamFYSnQvxfHszLanthgwQmANL23q0eSckLyyr+TTBXGo1a8fyZfrBwsjlvXWaINpeB+nTNwlTZgmOLcDfG9ShOf+siCGkpnuYTiIE5/agQYbRjxmttzUl5WVsZKEp13IhhvmsFYObk3FJ6XDHB8h9InbGan5XEJxBXhcr15hNb6wbS05LH8N/w+iPg9kOdrP5jOU/Ck+hPt+E6MOAveN7JjYbf6Fq3Bg7AvzbHqnpqXmTkfc0y6Km+yABZ7bIIFj+Rv1I8cAyo80O4hN1Uei+PkQOfEEbE6HcoFW+eQzk6mx8Y6Be2eD+mou9E56Fhjm82OcUC/T/bTdp28SAtw4Q4lvor3HkCOulxyIukksCBPoQDBYp8BWHzFwYIv8oqyMnFwj8L8+dGqVCnJITti1cc0PTk50xnBrYBFUyeCxSF/QXly4iHl+2AYrLwGDNzL6SF296oUkgI1FmwR8nAirylY0EQnr6Y67G5+lu/jnhqUEbhFJonUpKsfAKq0uHgbs1scAYuBA03BAgUkcDwk34G+3pMO1Yk8BY0b77ERGISPb9PvD0ACGGpEc3awTB0WqGzTPkQyXyvcjoUbqQgM3Ygq1h+CrosAEBfSmfN7KWnmHQHEWRyzCG6z+S7SpGiUWZj8/leDJph21ZF0eqKkmQuXaARDjhJHCgAofL8HOH8MevULBljwvsLOdKoST/Q2NAoT582bF8N/fNWR92aKMZLrGCC/pLOBgoomJRwYn5+mDca3kMg8XHUl8j3J+UtrSq7r//rJsM1Bx2ePkxKGuNJzNnZmBPdIudgZWDwRDMmC/40I0F+BED+yHXGT2DFjAESV53/z86dfrygpqVMnSzDAIjU5+XwTsHjCAlg02MgNJY+/nCgOcmLGs8M6p4UqbUREXCi8nDBZFS8xY7Dg/UPSuFqcT/EJkx58tBfvMqI9FvhMt64vMArKazCQLLhkgxTzi910RrSoaMjmivLyu6traq7Aj84HKAhXQZ80QQfx7IY31BgLjMFJqoHTmU/6HqTA2CzUHcgHtl7MHczVQ/i/iEvYjl1tJdk8HmTHqjrbAIscCRYnTZo0uYPBYokCFk4GFpYAox6p7XGELz/Q/2N4kz2G6+2Y13dBxSii9r9UJVWk/FiDZJOZal/9gQXAclqA3lBOOoxrDLIYfybbEBHu/iIjLfWhAXV1i7Fr/w3OkbgZGfg2yMSNJGXewZhqZxaNJNlwpB55W6Yuwrwt5fZBeFJl4SL7ptYploNgyf1WJm6UFer8g8iG27czAXOMkqL6Z7paBgu4stZUHpEsDmEBHgSFBOK1xEOy6NOjBwtFNzaoocN50Pctxc7iOxhQOVEpL4wuRK5mLPoP4PV0FiZ9uLr78wMWa+VOk/ooA/om+sscS+d1SPockvSBN8aT/sCid8+ewwEWB+m+nwEWPtrU9u69beHChUcNsPyd40eOLIYq4WFxEpu6kFWAAJ2E6mRD/9raIWSrCcdkfYGYo2jjQQ4WZotVHFwDdcwyjN8P0PmbJSlsAvN5Dd4dDT4lam3tuSTFHKD+HSCwOJUzDs5oiK61uDyEd+41TJSI/+F5O+Kjolb1xcH0NhaEkySTPjnZ2S/qDn0/fzbAQThT7IYd7BY6a1qWe1Atuc5it54nFi+BxdQOBovFUrpnkkUgh0RNBo2fAy33S9UNOzdfSB77EB3/HMZkAp+LbYBllug/gcX0gPtPNK6tqEjDmFyGttLGhVU6Xx6bjJfBK07i7TlWqcuLK4orcVkjjzDmqVEoB1oeaLsUNHsN0vTasrKyBptt1yljdO+s7Kz7kxKTRIDfCtiXyjqbBgmog1D7ow6gq0CxMiuHzogDjrAwffdisMVV1HrsBPvSaV2WJ/YYnLwnjvwUvA8G3xVA5Q3wjHochF4W43JePGzw4GEi1Qe714oxrZT3kf5u63QvjdKqZwt68HvHjWss93fvALi1kleRSp96BJb1ESoRf/SAXr8gAudyQ2JYDrfi9VANbMjOzr0b2TcvbBwypAdf2Ng1l4u2KeMwENWfV5BDPSo3MSbmDGyxbwHd1wFAnsY4/FlkJMUOqk7RiWo4DCaF0xTvHdSAc5z9zxt6J51HHO1yTUFOqN8l4pz1OLjXwkh7ocBMLLJU1k6rxaFEtvfG5VfYiV6H3dlinLN+DVKeTG9sbMwwoHkx6jDUHNYHM+N7HXa+mAvj2m1gZPfn08FjFYE+k2ciFbveeAQrwuh9FRjWzZjXNzt1/QIwsKk4S6aY3WeF+bhQ+2Wnpw/CxiAloP5z+lMcUDLUjzg586zc7OwbAA43QVV6tVBiFKSlVRj071gW3e5v7IKcVfD+5ZfAc9Q77RwXGej3x3AyOqjao1vw6W81Cltrb/Qu/12ANNTa8Wyt4wLZOv85fLys/vYY8wBn0NoZfN5lMXEmfW7/OqV3dr7+TefVbpStYQ18cjtVXa3MD0NusUHuY/DvtZnigNPCIfP2WHyfFiD9df63PbrYfyerxOQ6ZjFbfLZmsw+8z4GXwNsRfDp3fv9143bi/6ESKuYTHDVUjtIiRPtQCZVQCZVQCZVQ+b/24IAEAAAAQND/1+0IVAAAAAAAAAAAAAAAAAAAzgL4cn2g77WR0AAAAABJRU5ErkJggg==" />
                </td>
            </tr>
            <tr>
                <td style="padding:25px; background-color:#EDEDED;">
                    <table border="0" cellspacing="0" style="background-color:#FFFFFF; width:100%; font-family:Arial; font-size:13px; line-height:2;">
                        <tr>
                            <td style="padding:37px 47px 10px">
                                <p>
                                    Welcome, ' . $data[ 'lname' ] . '<br/><br/>
                                    Thank you for registering as a partner on our website. To activate your account please follow link below. If you have any questions or need assistance, you can email <a href="mailto:' . $this->getSettingValue( 'email' ) . '">our support team</a>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:10px 47px 37px">
                                <a href="' . HT_SERVER . SITE_URL . '/account-activation/?code=' . $data[ 'lactivate_code' ] . '" style="display:block; border:1px solid #2FAC66; background-color:#2FAC66; border-radius:3px; padding:10px; width:196px; text-align:center; margin:7px auto; color:#FFFFFF; font-weight:bold; text-decoration:none;">ACTIVATE ACCOUNT</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="padding:25px; background-color:#EDEDED;">
                    <table border="0" cellspacing="0" style="width:100%; font-family:Arial; font-size:13px;">
                        <tr>
                            <td style="text-align:center;">
                                <a href="mailto:' . $this->getSettingValue( 'email_booking' ) . '" style="color:#FFC200; text-decoration:underline;">Contact our Booking Manager</a><br/>
                                <p style="color:#C1C1C1; text-decoration:underline; margin-top:2px;">
                                    Manage your Bussiness on 
                                    <a style="color:#C1C1C1;" href="' . HT_SERVER . SITE_URL . '">NomadSurfers.com</a>
                                </p>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center; padding-top:20px;">
                                <a href="' . $this->getSettingValue( 'fb_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1Ni42OTMgNTYuNjkzIiBoZWlnaHQ9IjIwcHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1Ni42OTMgNTYuNjkzIiB3aWR0aD0iMjBweCIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayI+PHBhdGggZmlsbD0iI0MxQzFDMSIgZD0iTTQwLjQzLDIxLjczOWgtNy42NDV2LTUuMDE0YzAtMS44ODMsMS4yNDgtMi4zMjIsMi4xMjctMi4zMjJjMC44NzcsMCw1LjM5NSwwLDUuMzk1LDBWNi4xMjVsLTcuNDMtMC4wMjkgIGMtOC4yNDgsMC0xMC4xMjUsNi4xNzQtMTAuMTI1LDEwLjEyNXY1LjUxOGgtNC43N3Y4LjUzaDQuNzdjMCwxMC45NDcsMCwyNC4xMzcsMCwyNC4xMzdoMTAuMDMzYzAsMCwwLTEzLjMyLDAtMjQuMTM3aDYuNzcgIEw0MC40MywyMS43Mzl6Ii8+PC9zdmc+" alt="Facebook" /></a>
                                <a href="' . $this->getSettingValue( 'ig_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDU2LjcgNTYuNyIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgNTYuNyA1Ni43IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxnPg0KCTxwYXRoIGZpbGw9IiNDMUMxQzEiIGQ9Ik0yOC4yLDE2LjdjLTcsMC0xMi44LDUuNy0xMi44LDEyLjhzNS43LDEyLjgsMTIuOCwxMi44UzQxLDM2LjUsNDEsMjkuNVMzNS4yLDE2LjcsMjguMiwxNi43eiBNMjguMiwzNy43ICAgYy00LjUsMC04LjItMy43LTguMi04LjJzMy43LTguMiw4LjItOC4yczguMiwzLjcsOC4yLDguMlMzMi43LDM3LjcsMjguMiwzNy43eiIvPg0KCTxjaXJjbGUgZmlsbD0iI0MxQzFDMSIgY3g9IjQxLjUiIGN5PSIxNi40IiByPSIyLjkiLz4NCgk8cGF0aCBmaWxsPSIjQzFDMUMxIiBkPSJNNDksOC45Yy0yLjYtMi43LTYuMy00LjEtMTAuNS00LjFIMTcuOWMtOC43LDAtMTQuNSw1LjgtMTQuNSwxNC41djIwLjVjMCw0LjMsMS40LDgsNC4yLDEwLjdjMi43LDIuNiw2LjMsMy45LDEwLjQsMy45ICAgaDIwLjRjNC4zLDAsNy45LTEuNCwxMC41LTMuOWMyLjctMi42LDQuMS02LjMsNC4xLTEwLjZWMTkuM0M1MywxNS4xLDUxLjYsMTEuNSw0OSw4Ljl6IE00OC42LDM5LjljMCwzLjEtMS4xLDUuNi0yLjksNy4zICAgcy00LjMsMi42LTcuMywyLjZIMThjLTMsMC01LjUtMC45LTcuMy0yLjZDOC45LDQ1LjQsOCw0Mi45LDgsMzkuOFYxOS4zYzAtMywwLjktNS41LDIuNy03LjNjMS43LTEuNyw0LjMtMi42LDcuMy0yLjZoMjAuNiAgIGMzLDAsNS41LDAuOSw3LjMsMi43YzEuNywxLjgsMi43LDQuMywyLjcsNy4yVjM5LjlMNDguNiwzOS45eiIvPg0KPC9nPg0KPC9zdmc+" alt="Instagram" /></a>
                                <a href="' . $this->getSettingValue( 'yt_link' ) . '" style="margin:0 5px;"><img height="18" src="data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjEwMCUiIHN0eWxlPSJmaWxsLXJ1bGU6ZXZlbm9kZDtjbGlwLXJ1bGU6ZXZlbm9kZDtzdHJva2UtbGluZWpvaW46cm91bmQ7c3Ryb2tlLW1pdGVybGltaXQ6MjsiIHZlcnNpb249IjEuMSIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHdpZHRoPSIxMDAlIiB4bWw6c3BhY2U9InByZXNlcnZlIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnNlcmlmPSJodHRwOi8vd3d3LnNlcmlmLmNvbS8iIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIj48cGF0aCBmaWxsPSIjQzFDMUMxIiBkPSJNNTAxLjMwMywxMzIuNzY1Yy01Ljg4NywtMjIuMDMgLTIzLjIzNSwtMzkuMzc3IC00NS4yNjUsLTQ1LjI2NWMtMzkuOTMyLC0xMC43IC0yMDAuMDM4LC0xMC43IC0yMDAuMDM4LC0xMC43YzAsMCAtMTYwLjEwNywwIC0yMDAuMDM5LDEwLjdjLTIyLjAyNiw1Ljg4OCAtMzkuMzc3LDIzLjIzNSAtNDUuMjY0LDQ1LjI2NWMtMTAuNjk3LDM5LjkyOCAtMTAuNjk3LDEyMy4yMzggLTEwLjY5NywxMjMuMjM4YzAsMCAwLDgzLjMwOCAxMC42OTcsMTIzLjIzMmM1Ljg4NywyMi4wMyAyMy4yMzgsMzkuMzgyIDQ1LjI2NCw0NS4yNjljMzkuOTMyLDEwLjY5NiAyMDAuMDM5LDEwLjY5NiAyMDAuMDM5LDEwLjY5NmMwLDAgMTYwLjEwNiwwIDIwMC4wMzgsLTEwLjY5NmMyMi4wMywtNS44ODcgMzkuMzc4LC0yMy4yMzkgNDUuMjY1LC00NS4yNjljMTAuNjk2LC0zOS45MjQgMTAuNjk2LC0xMjMuMjMyIDEwLjY5NiwtMTIzLjIzMmMwLDAgMCwtODMuMzEgLTEwLjY5NiwtMTIzLjIzOFptLTI5Ni41MDYsMjAwLjAzOWwwLC0xNTMuNjAzbDEzMy4wMTksNzYuODAybC0xMzMuMDE5LDc2LjgwMVoiIHN0eWxlPSJmaWxsLXJ1bGU6bm9uemVybzsiLz48L3N2Zz4=" alt="Youtube" /></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>';

        return $content;
    }

    function saveLog( $message )
    {
        //-- Check if folder exist
        if( !file_exists( ROOT_PATH . '/logs' ) )
        {
            mkdir( ROOT_PATH . '/logs', 0777 );
        }

        error_log( $message, 3, ROOT_PATH . '/logs/error-' . date( 'Y-m-d' ) . '.log' );
    }

    function sendEmail( $options = array() )
    {
        require_once( INCLUDE_DIR . '/vendor/autoload.php' );

        $mail = new PHPMailer();

        try
        {
            $mail->isSMTP();

            $default = array(
                'type'          => 'xoauth2',
                'from'          => array(),
                'to'            => array(),
                'replyto'       => array(),
                'attachment'    => array(),
                'strattachment' => array(),
                'subject'       => '',
                'altbody'       => '',
                'message'       => '',
            );

            $prm = array_merge( $default, $options );

            if( empty( $prm[ 'to' ] ) === false && empty( $prm[ 'message' ] ) === false )
            {
                if( $prm[ 'type' ] == 'xoauth2' )
                {
                    $username      = $this->getSettingValue( 'gmail_xoauth_from_email' );
                    $client_id     = $this->getSettingValue( 'gmail_xoauth_client_id' );
                    $client_secret = $this->getSettingValue( 'gmail_xoauth_client_secret' );
                    $refresh_token = $this->getSettingValue( 'gmail_xoauth_refresh_token' );

                    if( $client_id != '' && $client_secret != '' && $refresh_token != '' )
                    {
                        $mail->Host     = 'smtp.gmail.com';
                        $mail->AuthType = 'XOAUTH2';
                        $mail->Port     = 465;

                        //-- CREATE a new OAuth2 provider instance
                        $provider = new Google( array(
                            'clientId'     => $client_id,
                            'clientSecret' => $client_secret,
                        ));

                        //-- PASS the OAuth provider instance to PHPMailer
                        $mail->setOAuth( new OAuth( array(
                            'refreshToken' => $refresh_token,
                            'clientSecret' => $client_secret,
                            'clientId'     => $client_id,
                            'provider'     => $provider,
                            'userName'     => $username,
                        )));
                    }
                    else
                    {
                        return json_encode( array( 'result' => 'failed', 'message' => 'Not valid credentials' ) );
                    }
                }
                else
                {
                    $mail->Host     = $this->getSettingValue( 'smtp_server' );
                    $mail->Port     = $this->getSettingValue( 'smtp_port' );
                    $mail->Username = $this->getSettingValue( 'smtp_email' );
                    $mail->Password = $this->getSettingValue( 'smtp_password' );
                }

                $mail->CharSet    = PHPMailer::CHARSET_UTF8;
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
                $mail->SMTPDebug  = SMTP::DEBUG_OFF;
                $mail->SMTPAuth   = true;

                //-- SET from where the message is send
                if( empty( $prm[ 'from' ] ) === false )
                {
                    if( isset( $prm[ 'from' ][ 'email' ] ) && isset( $prm[ 'from' ][ 'label' ] ) )
                    {
                        $mail->setFrom( $prm[ 'from' ][ 'email' ], $prm[ 'from' ][ 'label' ] );
                    }
                    else
                    {
                        $mail->setFrom( $prm[ 'from' ] );
                    }
                }
                else
                {
                    $flabel = sprintf( '%s Automail', $this->getSettingValue( 'name' ) );

                    if( $type == 'xoauth2' )
                    {
                        $mail->setFrom( $this->getSettingValue( 'gmail_xoauth_from_email' ), $flabel );
                    }
                    else
                    {
                        $mail->setFrom( $this->getSettingValue( 'smtp_email' ), $flabel );
                    }
                }

                //-- SET who the message is to be sent to
                if( isset( $prm[ 'to' ][ 'email' ] ) && isset( $prm[ 'to' ][ 'label' ] ) )
                {
                    $mail->addAddress( $prm[ 'to' ][ 'email' ], $prm[ 'to' ][ 'label' ] );
                }
                else
                {
                    $mail->addAddress( $prm[ 'to' ] );
                }

                //-- SET who the message will replay to
                if( empty( $prm[ 'replyto' ] ) === false )
                {
                    if( isset( $prm[ 'replyto' ][ 'email' ] ) && isset( $prm[ 'replyto' ][ 'label' ] ) )
                    {
                        $mail->addReplyTo( $prm[ 'replyto' ][ 'email' ], $prm[ 'replyto' ][ 'label' ] );
                    }
                    else
                    {
                        $mail->addReplyTo( $prm[ 'replyto' ] );
                    }
                }

                //-- SET the subject line
                if( empty( $prm[ 'subject' ] ) === false )
                {
                    $mail->Subject = $prm[ 'subject' ];
                }

                //-- SET the alt body
                if( empty( $prm[ 'altbody' ] ) === false )
                {
                    $mail->AltBody = $prm[ 'altbody' ];
                }

                //-- ATTACH file
                if( empty( $prm[ 'attachment' ] ) === false )
                {
                    if( isset( $prm[ 'attachment' ][ 'path' ] ) && isset( $prm[ 'attachment' ][ 'name' ] ) )
                    {
                        $mail->addAttachment( $prm[ 'attachment' ][ 'path' ], $prm[ 'attachment' ][ 'name' ] );
                    }
                    else
                    {
                        $mail->addAttachment( $prm[ 'attachment' ] );
                    }
                }

                //-- ATTACH string
                if( empty( $prm[ 'strattachment' ] ) === false )
                {
                    $mail->addStringAttachment( 
                        $prm[ 'strattachment' ][ 'string' ], 
                        $prm[ 'strattachment' ][ 'filename' ], 
                        $prm[ 'strattachment' ][ 'encoding' ], 
                        $prm[ 'strattachment' ][ 'type' ]
                    );
                }

                //-- SET the message
                $mail->msgHTML( $prm[ 'message' ] );

                $mail->send();

                return true;
            }
            else
            {
                return false;
            }
        }
        catch( phpmailerException $e )
        {
            return false;
        }
        catch( Exception $e )
        {
            return false;
        }
    }
}

?> 