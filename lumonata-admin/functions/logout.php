<?php

ob_start();

session_start();

require_once( '../../config.php' );

//-- CLEAR cookie
if( isset( $_COOKIE[ session_name() ] ) )
{
	setcookie( session_name(), '', time() - 3600, '/' );
}

//-- CLEAR session from globals
$_SESSION = array();

//-- CLEAR session from disk
session_destroy();

header( 'location:'. HT_SERVER . ADMIN_URL . '/login.php' );

exit;

?>