<?php

class navigationMenu extends db
{
    function __construct( $site, $mod = '' )
    {
        parent::__construct();

        $this->global = new globalAdmin();

        $this->sess = $this->global->getCurrentSession();
        $this->site = $site;
        $this->mod  = $mod;
    }

    function getAdminMenu( $usertype_id, $parent_id = 0, $level = 0, $itemId = 0 )
    {
        $s = 'SELECT 
				m.lmodule_id, 
				m.lname,
				m.lfolder,
				m.ltitle, 
				m.lparent_id,
				m.llink_type,
				m.lapps,
				m.lappsagent,
				p.lview 
              FROM lumonata_module AS m, lumonata_user_permissions AS p
              WHERE m.lmodule_id = p.lmodule_id 
              AND p.lusertype_id = %s 
              AND m.lview = 1 
              AND m.lparent_id = %s 
              ORDER BY m.lorder_id';
        $q = parent::prepare_query( $s, $usertype_id, $parent_id );
        $r = parent::query( $q );

        $items = '';

        if( !is_array( $r ) )
        {
        	$n = parent::num_rows( $r );

	        if( $n > 0 )
	        {
	            $level += 1;
	        }
	        else
	        {
	            $level = $level;
	        }

	        if( $level > 1 )
	        {
	            $id    = $parent_id;
	            $class = '';
	        }
	        else
	        {
	            $id    = '';
	            $class = 'menubo';
	        }

	        if( $n > 0 )
	        {
	            $items .='
	            <ul class="' . $class . '" id="' . $id . '" >';
	        }

	        $li = 1;

	        while( list( $item_id, $item_name, $item_folder, $item_title, $parent_id, $link_type, $lapps, $lappsagent, $lview ) = parent::fetch_array( $r ) )
	        {
	            if( $li == 1 && $level == 1 )
	            {
	            	$su = 'SELECT * FROM lumonata_user AS a WHERE a.lusername = %s';
	                $qu = parent::prepare_query( $su, $this->sess[ 'username' ] );
	                $ru = parent::query( $qu );
	                $du = parent::fetch_array( $ru );

	                if( empty( $du[ 'limage' ] ) )
	                {
	                    $img = 'default.png';
	                }
	                else
	                {
	                    $img = $du[ 'limage' ];
	                }

	                $avatar = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?src=' . HT_SERVER . SITE_URL . '/images/Users/' . $img . '&w=48&h=48';

	                if( !empty( $du[ 'llastname' ] ) )
	                {
	                    $name = $du[ 'llastname' ];
	                }
	                elseif( !empty( $du[ 'lfirstname' ] ) )
	                {
	                    $name = $du[ 'lfirstname' ];
	                }
	                elseif( !empty( $du[ 'lname' ] ) )
	                {
	                    $name = $du[ 'lname' ];
	                }

	                if( $usertype_id < 4 || $usertype_id == 8 )
	                {
	                	$st = 'SELECT lname FROM lumonata_usertype AS a WHERE a.lusertype_id = %d';
	                    $qt = parent::prepare_query( $st, $usertype_id );
	                    $rt = parent::query( $qt );
	                    $dt = parent::fetch_array( $rt );

	                    $uname = $dt[ 'lname' ];
	                }
	                else
	                {
	                    $uname = "Custom";
	                }

	                if( $usertype_id != 8 )
	                {
	                    $items .= '
	                    <li>
	                        <div class="box-avar48">
	                        	<img src="' . $avatar . '" alt="" />
	                        	<h4>Hi, ' . $name . '<br /><span>' . $uname . '</span></h4>
	                        	<div class="clear"></div>
	                        </div>
	                    </li>';
	                }
	                else
	                {
	                    $items .= '
	                    <li>
	                        <div class="box-avar48"> 
	                            <img src="' . $avatar . '" />
	                            <h4>Hi, ' . $name . '<br /><a href="//' . $this->site . '/home.php?mod=354&prc=view">' . $uname . '</a></h4>
	                            <div class="clear"></div>
	                        </div>
	                    </li>';
	                }
	            }

	            if( $link_type == 'P' )
	            {
	                $url = '#';
	            }
	            elseif( $link_type == 'A' )
	            {
	            	if( $usertype_id == 2 )
	                {
	                	$url = HT_SERVER . $this->site . '/agent/' . str_replace( '_', '-', $lapps ) . '/';
	                }
	                else
	                {
	                	$url = HT_SERVER . $this->site . '/home.php?mod=' . $item_id . '&prc=view';
	                }
	            }
	            elseif( $link_type == 'U' )
	            {
	            	if( $usertype_id == 2 )
	                {
	                	$url = $lappsagent;
	                }
	                else
	                {
	                	$url = $lapps;
	                }
	            }

	            $next_level = $this->getAdminMenu( $usertype_id, $item_id, $level, $itemId );

	            $aktif = '';

	            if( isset( $_GET[ 'mod' ] ) )
	            {
	            	if( empty( $_GET[ 'mod' ] ) && $item_id == 1 )
		            {
		                $aktif = 'class="aktif"';
		            }
		            elseif( $_GET[ 'mod' ] == $item_id )
		            {
		                $aktif = 'class="aktif"';
		            }
		        }
		        else
		        {
	            	if( empty( $this->mod ) && $item_id == 1 )
		            {
		                $aktif = 'class="aktif"';
		            }
		            elseif( $this->mod == $item_id )
		            {
		                $aktif = 'class="aktif"';
		            }
		        }

	            if( !empty( $next_level ) )
	            {
	                $arrowsub = 'arrowsub';
	            }
	            else
	            {
	                $arrowsub = '';
	            }

	            if( $level > 1 )
                {
                    $items .= '
                    <li class="' . $arrowsub . '">
                    	<a href="' . $url . '" ' . $aktif . '>' . $item_name . '</a>';
                }
                elseif( $level == 1 & !empty( $next_level ) )
                {
                    $items .= '
                    <li class="' . $arrowsub . '">
                    	<a ' . $aktif . '>
                    		<span class="arrow" rel="' . $item_id . '"></span>
                    		<span class="arrow" rel="' . $item_id . '" style="background: none repeat scroll 0% 0% transparent; display: inline-block; margin: 0px; float: none; width: 160px;">' . $item_name . '</span>
                    	</a>';
                }
                else
                {
                    $items .= '
                    <li>
                    	<a ' . $aktif . ' href="' . $url . '">
                    		<span class="arrow"></span>' . $item_name . '
                    	</a>';
                }

	            $items .= $next_level . '</li>';

	            $li++;
	        }

	        if( $n > 0 )
	        {
	            $items .= '</ul>';
	        }
	        
	        return $items;
    	}
    }
}
?> 