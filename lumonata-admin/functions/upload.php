<?php

class upload
{
    function upload_constructor( $dest )
    {
        $this->dest = $dest;
    }
    
    function upload_file( $original_file_name, $new_file_name, $temp_file )
    {
        if( empty( $new_file_name ) )
        {
            //-- Using original filename
            $dest = $this->dest . $original_file_name;
        }
        else
        {
            //-- Rename original filename
            $dest = $this->dest . $this->rename_file( $original_file_name, $new_file_name );
        }
        
        if( move_uploaded_file( $temp_file, $dest ) )
        {
            chmod( $dest, 0644 );
            
            return true;
        }
        
        return false;
    }
    
    function rename_file( $original_file_name, $new_file_name )
    {
        return $new_file_name . strchr( $original_file_name, "." );
    }
    
    function file_name_filter( $file_name, $ext = false )
    {
        $fileext  = strchr( $file_name, '.' );
        $filename = str_replace( $fileext, '', $file_name );
        
        if( $ext == true )
        {
            return strtolower( $fileext );
        }
        
        return $this->sef_url( $filename );
    }
    
    function file_size( $file_size, $max_size )
    {
        if( $file_size <= $max_size || $file_size = 0 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function delete_file( $file_name, $num = 0 )
    {
        if( $num == 0 )
        {
            if( file_exists( $this->dest . $file_name ) )
            {
                unlink( $this->dest . $file_name );
            }
        }
        else if( $num == 1 )
        {
            if( file_exists( $this->dest . 'thumbs/' . $file_name ) )
            {
                unlink( $this->dest . 'thumbs/' . $file_name );
            }
        }

        return $this->dest . $file_name;
    }

    function delete_thumb( $file_name )
    {
        if( file_exists( $this->dest . 'thumbs/' . $file_name ) )
        {
            unlink( $this->dest . 'thumbs/' . $file_name );
        }
    }

    function delete_file_thumb( $file_name )
    {
        if( file_exists( $this->dest . $file_name ) )
        {
            unlink( $this->dest . $file_name );
        }

        if( file_exists( $this->dest . 'thumbs/' . $file_name ) )
        {
            unlink( $this->dest . 'thumbs/' . $file_name );
        }
    }

    function upload_resize( $original_file_name, $new_file_name, $temp_file, $file_ext, $twidth, $theight, $thumb = 1, $justwidth = false )
    {
        //-- Check if folder exist
        if( !file_exists( $this->dest ) )
        {
            mkdir( $this->dest, 0777 );
        }

        //-- Check if folder thumb exist
        if( !file_exists( $this->dest . 'thumbs' ) )
        {
            mkdir( $this->dest . 'thumbs', 0777 );
        }

        if( empty( $new_file_name ) ) //using original filename
        {
            if( $thumb == 1 )
            {
                $dest = $this->dest . 'thumbs/' . $original_file_name;
            }
            else
            {
                $dest = $this->dest . $original_file_name;
            }
        }
        else
        {
            if( $thumb == 1 )
            {
                $dest = $this->dest . 'thumbs/' . $this->rename_file( $original_file_name, $new_file_name );
            }
            else
            {
                $dest = $this->dest . $this->rename_file( $original_file_name, $new_file_name );
            }
        }

        //-- This is the temporary file created by PHP
        $uploadedfile = $temp_file;
        
        //-- Create an Image from it so we can do the resize
        if( $file_ext == 'image/jpg' || $file_ext == 'image/jpeg' || $file_ext == 'image/pjpeg' )
        {
            $src = imagecreatefromjpeg( $uploadedfile );
        }
        else if( $file_ext == 'image/gif' )
        {
            $src = imagecreatefromgif( $uploadedfile );
        }
        else if( $file_ext == 'image/png' )
        {
            $src = imagecreatefrompng( $uploadedfile );
        }
        
        //-- Capture the original size of the uploaded image
        list( $width, $height ) = getimagesize( $uploadedfile );

        $des_x = 0;

        if( $height <= $width && $width >= $twidth && $justwidth == FALSE )
        {
            $newwidth  = $twidth;
            $newheight = $twidth * ( $height / $width );

            if( $newheight > $theight )
            {
                $newheight = $theight;
                $newwidth  = $newheight * ( $width / $height );
            }
        }
        elseif( $height <= $width && $width >= $twidth && $justwidth == TRUE )
        {
            $newwidth  = $twidth;
            $newheight = $twidth * ( $height / $width );
        }
        elseif( $height <= $width && $width < $twidth )
        {
            $newwidth  = $width;
            $newheight = $newwidth * ( $height / $width );

            if( $newheight > $theight )
            {
                $newheight = $theight;
                $newwidth  = ( $width * $newheight ) / $height;
            }
        }
        elseif( $height >= $width && $height >= $theight && $justwidth == FALSE )
        {
            $newwidth  = $twidth;
            $newheight = $theight;
            $tmp       = imagecreatetruecolor( $twidth, $newheight );
            
        }
        elseif( $height >= $width && $height >= $theight && $justwidth == TRUE )
        {
            $newwidth  = $width;
            $newheight = $height;
            $tmp       = imagecreatetruecolor( $twidth, $newheight );
            $des_x     = ( $twidth - $width ) / 2;
        }
        elseif( $height >= $width && $height < $theight )
        {
            $newheight = $height;
            $newwidth  = $newheight * ( $width / $height );

            if( $newwidth > $twidth )
            {
                $newwidth  = $twidth;
                $newheight = $newwidth * ( $height / $width );
            }
        }
        
        if( !isset( $tmp ) && empty( $tmp ) )
        {
            $tmp = imagecreatetruecolor( $newwidth, $newheight );
        }

        imagecopyresampled( $tmp, $src, $des_x, 0, 0, 0, $newwidth, $newheight, $width, $height );

        //-- Now write the resized image to disk. I have assumed that you want the
        //-- Resized, uploaded image file to reside in the ./images subdirectory.
        if( $file_ext == 'image/jpg' || $file_ext == 'image/jpeg' || $file_ext == 'image/pjpeg' )
        {
            imagejpeg( $tmp, $dest, 100 );
        }
        else if( $file_ext == 'image/gif' )
        {
            imagegif( $tmp, $dest );
        }
        else if( $file_ext == 'image/png' )
        {
            imagepng( $tmp, $dest );
        }
        
        imagedestroy( $src );
        imagedestroy( $tmp );

        chmod( $dest, 0644 );
        
        if( file_exists( $dest ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    function sef_url( $URL )
    {
        $char = array(
            "\\",
            "/",
            ":",
            "*",
            "?",
            "<",
            ">",
            "`",
            "~",
            "!",
            "@",
            "#",
            "$",
            "%",
            "^",
            "&",
            "(",
            ")",
            "_",
            "+",
            "=",
            "|",
            "}",
            "{",
            "[",
            "]",
            ";",
            "\"",
            "'",
            ",",
            ".",
            " " 
        );

        $sef_url  = $URL;

        for( $i = 0; $i < count( $char ); $i++ )
        {
            $sef_url = str_replace( $char[ $i ], '-', $sef_url );
            $sef_url = str_replace( '--', '-', $sef_url );
        }

        $sef_url = str_replace( '--', '-', $sef_url );
        $strlen  = strlen( $sef_url );

        if( substr( $sef_url, -1 ) == '-' )
        {
            $sef_url = substr( $sef_url, 0, ( $strlen - 1 ) );
        }

        $strlen = strlen( $sef_url );

        if( substr( $sef_url, 0, 1 ) == '-' )
        {
            $sef_url = substr( $sef_url, 1, $strlen );
        }

        $sef_url = str_replace( 'ä', 'a', $sef_url );
        $sef_url = str_replace( 'Ä', 'A', $sef_url );
        $sef_url = str_replace( 'é', 'e', $sef_url );
        $sef_url = str_replace( 'ö', 'o', $sef_url );
        $sef_url = str_replace( 'Ö', 'O', $sef_url );
        $sef_url = str_replace( 'ü', 'u', $sef_url );
        $sef_url = str_replace( 'Ü', 'U', $sef_url );
        $sef_url = str_replace( 'ß', 'B', $sef_url );
        $sef_url = str_replace( 'ä', 'a', $sef_url );

        return strtolower( $sef_url );
    }
}

?>