<?php

ob_start();

session_start();

require_once( '../config.php' );

if( UNDER_CONSTRUCTION )
{
    require_once( '../functions/db.php' );
    require_once( '../functions/template.php' );
    
    require_once( 'functions/globals.php' );

    $t = new Template( ADMIN_THEME_DIR );
    $g = new globalAdmin();

    $t->set_file( 'maintenance', 'maintenance.html' );

    $t->set_var( 'bemail', $g->getSettingValue( 'email_booking' ) );
    
    $t->Parse( 'mtBlock', 'maintenanceBlock', false );
    
    $t->pparse( 'Output', 'maintenance' );
}
else
{
    if( isset( $_SESSION[ SESSION_NAME ] ) && isset( $_COOKIE[ SESSION_NAME ] ) )
    {
        $p = json_decode( base64_decode( $_SESSION[ SESSION_NAME ] ), true );

        if( $p === null && json_last_error() !== JSON_ERROR_NONE )
        {        
            header( 'location:'. HT_SERVER . ADMIN_URL . '/login.php' );

            exit;
        }

        require_once( '../functions/db.php' );
        require_once( '../functions/actions.php' );
        require_once( '../functions/template.php' );
        require_once( '../functions/directory.php' );
        require_once( '../functions/flash-message.php' );

        require_once( 'functions/navigation.php' );
        require_once( 'functions/globals.php' );
        require_once( 'functions/upload.php' );
        
        $t = new Template( ADMIN_THEME_DIR );
        $n = new navigationMenu( ADMIN_URL );
        $g = new globalAdmin();
        $a = new actions();

        $prc = isset( $_REQUEST[ 'prc' ] ) ? $_REQUEST[ 'prc' ] : 'view';
        $mod = isset( $_REQUEST[ 'mod' ] ) ? $_REQUEST[ 'mod' ] : 1;
        $utp = isset( $p[ 'usertype' ] ) ? $p[ 'usertype' ] : '';

        if( $prc == 'request' )
        {
            $t->set_file( 'request', 'request.html' );

            $t->set_block( 'request', 'mainBlock', 'mBlock' );
        }
        else
        {
            $t->set_file( 'home', 'home.html' );

            $t->set_block( 'home', 'mainBlock', 'mBlock' );
        }

        $dir = $g->getModule( $mod );
        $app = $g->getApps( $mod );

        if( file_exists( ADMIN_APPS_DIR . '/' . $dir . '/class.php' ) && empty( $app ) === false )
        {
            require_once( ADMIN_APPS_DIR . '/' . $dir . '/class.php' );
            
            $o = new $app( ADMIN_APPS_DIR . '/' . $dir, $a );

            if( $prc == 'calendar' )
            {
                $t->set_var( 'main_body', $o->calendar( $mod, $utp ) );
            }
            elseif( $prc == 'request' )
            {
                $t->set_var( 'main_body', $o->request( $mod, $utp ) );
            }
            elseif( $prc == 'new' )
            {
                $t->set_var( 'main_body', $o->insert( $mod, $utp ) );
            }
            elseif( $prc == 'popup' )
            {
                $t->set_var( 'main_body', $o->popup( $mod, $utp ) );
            }
            elseif( $prc == 'edit' )
            {
                $t->set_var( 'main_body', $o->edit( $mod, $utp ) );
            }
            elseif( $prc == 'view' )
            {
                $t->set_var( 'main_body', $o->view( $mod, $utp ) );
            }
            else
            {
                $t->set_var( 'main_body', $g->setError() );
            }
        }
        else
        {
            $t->set_var( 'main_body', $g->setError() );
        }

        $t->set_var( 'site_url', SITE_URL );
        $t->set_var( 'admin_url', ADMIN_URL );
        $t->set_var( 'image_url', IMAGE_URL );
        $t->set_var( 'version', '?v=' . VERSION );
        $t->set_var( 'template_url', ADMIN_THEME_URL );
        $t->set_var( 'include_url', ADMIN_INCLUDE_URL );

        $t->set_var( 'left_menu', $n->getAdminMenu( $utp ) );
        $t->set_var( 'jvs_links', $a->attemp_actions( 'jvs' ) );
        $t->set_var( 'css_links', $a->attemp_actions( 'css' ) );
        
        $t->set_var( 'year', date( 'Y', time() ) );

        $t->Parse( 'mBlock', 'mainBlock', false );

        if( in_array( $prc, array( 'request' ) ) )
        {
            $t->pparse( 'Output', $prc );
        }
        else
        {
            $t->pparse( 'Output', 'home' );
        }
    }
    else
    {
        header( 'location:'. HT_SERVER . ADMIN_URL . '/login.php' );

        exit;
    }
}

?> 