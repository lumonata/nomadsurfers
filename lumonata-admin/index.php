<?php

ob_start();

require_once( '../config.php' );

if( isset( $_COOKIE[ SESSION_NAME ] ) === false )
{
	header( 'location:'. HT_SERVER . ADMIN_URL . '/login.php' );
}
else
{
	header( 'location:'. HT_SERVER . ADMIN_URL . '/home.php' );
}

exit;

?> 