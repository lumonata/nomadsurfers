<?php

ob_start();

session_start();

require_once( '../config.php' );
require_once( ROOT_PATH . '/functions/db.php' );
require_once( ROOT_PATH . '/functions/template.php' );
require_once( ROOT_PATH . '/functions/flash-message.php' );
require_once( ROOT_PATH . '/functions/authentication.php' );

$t = new Template( ADMIN_THEME_DIR );
$f = new flash_message();

$t->set_file( 'home', 'login.html' );

$t->set_block( 'home', 'mainBlock', 'mBlock' );

$t->set_var( 'theme_url', ADMIN_THEME_URL );
$t->set_var( 'message', $f->message() );

$t->Parse( 'mBlock', 'mainBlock', true );

$t->pparse( 'Output', 'home' );

?> 