function init_tinymce( tselector, options )
{
    if( typeof tselector == 'undefined' )
    {
        tselector = '.tinymce';
    }

    if( typeof options == 'undefined' )
    {
        if( window.location.pathname.search( 'agent' ) > 0  )
        {
            content_css = [ '../../../lumonata-admin/templates/assets/tinymce.css' ];
        }
        else
        {
            content_css = [ 'templates/assets/tinymce.css' ];
        }

        tinymce.init({
            fontsize_formats: "8pt 9pt 10pt 11pt 12pt 14pt 18pt 24pt 30pt 36pt 48pt 60pt 72pt 96pt",
            images_upload_handler: init_tinymce_upload,
            remove_script_host : false,
            content_css: content_css,
            automatic_uploads: true, 
            relative_urls : false,
            convert_urls : true,
            selector: tselector,
            image_advtab: true,
            image_title: true,
            height: 500,
            plugins: [
                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
            ],
            templates: [
                {
                    title: 'What\'s Include List',
                    description: 'Adds a 3 box colomn with title and list',
                    content: '<div class="obig-include"><div><h4>Title 1</h4><ul><li>Item 1</li></ul></div><div><h4>Title 2</h4><ul><li>Item 2</li></ul></div><div><h4>Title 3</h4><ul><li>Item 3</li></ul></div></div>'
                },
                {
                    title: 'Food/Meals Include List',
                    description: 'Adds a 2 box colomn with title and list',
                    content: '<div class="obig-meals"><div><label>Title 1</label><ul><li>Item 1</li></ul></div><div><label>Title 2</label><ul><li>Item 2</li></ul></div>'
                }
            ]
        });
    }
    else
    {
        options.selector = selector;
        
        tinymce.init( options );
    }
}

function init_tinymce_upload( blobInfo, success, failure, progress )
{
    var xhr, formData, appid, modid;

    xhr = new XMLHttpRequest();

    xhr.withCredentials = false;

    xhr.open( 'POST', 'functions/ajax.php');

    xhr.upload.onprogress = function( e ){
        progress( e.loaded / e.total * 100 );
    };

    xhr.onload = function(){
        var json;

        if( xhr.status === 403 )
        {
            failure('HTTP Error: ' + xhr.status, { remove: true });

            return;
        }

        if( xhr.status < 200 || xhr.status >= 300 )
        {
            failure( 'HTTP Error: ' + xhr.status );

            return;
        }

        json = JSON.parse( xhr.responseText );

        if( !json )
        {
            failure( 'Invalid JSON: ' + xhr.responseText );

            return;
        }
        else
        {
            if( json.result == 'success' )
            {
                success( json.src );
            }
            else if( json.result == 'not-login' )
            {
                failure( 'You don\'t have access to uploaded file' );

                return;
            }
            else if( json.result == 'not-found' )
            {
                failure( 'Module not found!' );

                return;
            }
            else if( json.result == 'not-supported' )
            {
                failure( 'Not supported file!' );

                return;
            }
            else if( json.result == 'failed' )
            {
                failure( 'Upload failed! Something wrong' );

                return;
            }
            else
            {                
                failure( 'Invalid JSON: ' + xhr.responseText );

                return;
            }
        }
    };

    xhr.onerror = function(){
        failure( 'Image upload failed due to a XHR Transport error. Code: ' + xhr.status );
    };

    //-- Set parameter
    user  = jQuery('#lcreated_by').val();
    lang  = jQuery('#llang_ida').val();
    appid = jQuery('#lpost_id').val();
    modid = jQuery('#lmodule').val();

    if( typeof appid == 'undefined' )
    {
        appid = jQuery('#lterm_id').val();
    }

    formData = new FormData();

    formData.append( 'attachment', blobInfo.blob(), blobInfo.filename() );
    formData.append( 'pKEY', 'upload_file' );
    formData.append( 'app_id', appid );
    formData.append( 'mod', modid );
    formData.append( 'lang', lang );
    formData.append( 'user', user );

    xhr.send( formData );
}

function init_number( selector, types )
{
    if( typeof selector == 'undefined' )
    {
        selector = '.text-number';
    }

    if( jQuery( selector ).length > 0 )
    {
        var opt = Array();
        var dom = Array();

        jQuery( selector ).each( function( e ){
            var data = jQuery(this).data();
            var prm  = new Object();
                prm.roundingMethod   = AutoNumeric.options.roundingMethod.halfUpSymmetric;
                prm.unformatOnSubmit = true;
                
            if( typeof data.aSep != 'undefined' )
            {
                prm.digitGroupSeparator = data.aSep;
            }
                
            if( typeof data.aDec != 'undefined' )
            {
                prm.decimalCharacter = data.aDec;
            }
                
            if( typeof data.mDec != 'undefined' )
            {
                prm.decimalPlaces = data.mDec;
            }

            if( typeof data.aMin != 'undefined' )
            {
                prm.minimumValue = data.aMin;
            }

            if( typeof data.aMax != 'undefined' )
            {
                prm.maximumValue = data.aMax;
            }

            if( typeof data.aSign != 'undefined' )
            {
                prm.currencySymbol = data.pSign;
            }
            else
            {
                prm.currencySymbol = '';
            }

            if( typeof data.aPos != 'undefined' )
            {
                prm.currencySymbolPlacement = data.aPos;
            }

            dom.push( this );
            opt.push( prm );
        });
        
        new AutoNumeric.multiple( dom, opt );
    }
}

function init_datepicker( selector, options )
{
    if( typeof selector == 'undefined' )
    {
        selector = '.text-date';
    }

    if( jQuery( selector ).length > 0 )
    {
        if( typeof options == 'undefined' )
        {
            jQuery( selector ).datepicker({
                dateFormat: 'dd MM yy'
            });
        }
        else
        {
            jQuery( selector ).datepicker( options );
        }
    }
}

function init_date_range_picker( selector, options )
{
    if( typeof selector == 'undefined' )
    {
        selector = '.text-date-range';
    }

    if( jQuery( selector ).length > 0 )
    {
        var prt = jQuery( selector ).parent();
        var utl = prt.find('.text-date-end');
        var stt = prt.find('.text-date-start');

        if( typeof options == 'undefined' )
        {
            options = {
                autoUpdateInput: false,
                autoApply: true,
                locale:{
                    format: 'MMMM DD, YYYY',
                }
            }
        }

        jQuery( selector ).daterangepicker( options, function( start, end ) {
            stt.val( start.format( 'YYYY-MM-DD' ) );
            utl.val( end.format( 'YYYY-MM-DD' ) );
        }).on('apply.daterangepicker', function( ev, picker ) {
            var start = picker.startDate.format( 'MMMM DD, YYYY' );
            var end   = picker.endDate.format( 'MMMM DD, YYYY' );

            jQuery( this ).val( start + ' - ' + end );
        });

        //-- SET daterangepicker data
        if( stt.val() != '' && utl.val() != '' )
        {
            var start = moment( stt.val() ).format( 'MMMM DD, YYYY' );
            var end   = moment( utl.val() ).format( 'MMMM DD, YYYY' );

            jQuery( selector ).data('daterangepicker').setStartDate( start );
            jQuery( selector ).data('daterangepicker').setEndDate( end );
            jQuery( selector ).val( start + ' - ' + end );
        }
    }
}

function init_schedule_date_range_picker( selector )
{
    if( typeof selector == 'undefined' )
    {
        selector = '.text-daterange';
    }

    if( jQuery( selector ).length > 0 )
    {
        jQuery( selector ).each( function( i, e ){
            var prt = jQuery( this ).parent().parent();
            var out = prt.find('.lcheck_out').val();
            var ins = prt.find('.lcheck_in').val();
            var opt = new Object;
                opt.locale = { format: 'MMMM DD, YYYY' };
                opt.parentEl = '.popup-wrap';
                opt.autoUpdateInput = false;
                opt.autoApply = true;

            if( ins != '' )
            {
                opt.startDate = moment( ins ).format( 'MMMM DD, YYYY' );
            }

            if( out != '' )
            {
                opt.endDate = moment( out ).format( 'MMMM DD, YYYY' );
            }

            jQuery( this ).daterangepicker( opt, function( start, end, label ) {
                var sdate = start.format( 'MMMM DD, YYYY' );
                var edate = end.format( 'MMMM DD, YYYY' );
                
                prt.find('.lcheck_in').val( sdate );
                prt.find('.lcheck_out').val( edate );

                prt.find('.lcheck_in').data('daterangepicker').setStartDate( sdate );
                prt.find('.lcheck_in').data('daterangepicker').setEndDate( edate );
                
                prt.find('.lcheck_out').data('daterangepicker').setStartDate( sdate );
                prt.find('.lcheck_out').data('daterangepicker').setEndDate( edate );

                if( prt.find('.lduration').length > 0 )
                {
                    var night = moment( edate ).diff( moment( sdate ), 'days');
                    var days  = night + 1;

                    if( night == 0 )
                    {
                        night = 1;
                    }
                    
                    prt.find('.lduration').val( days + 'D' + night + 'N' );
                }
            });
        });
    }
}

function init_upload()
{
    if( jQuery('.upload-file').length > 0 )
    {
        jQuery('.upload-file').each( function( e ){
            var cont  = jQuery(this);
            var input = cont.find('input[type=file]');

            var url   = jQuery('#ajax_url').val();
            var appid = jQuery('#lpost_id').val();
            var modid = jQuery('#lmodule').val();

            var types = input.data('filetype');
            var field = input.data('field');
            var hash  = input.data('hash');
            var dfsrc = input.data('src');

            if( typeof appid == 'undefined' )
            {
                appid = jQuery('#lterm_id').val();
            }

            input.fileupload({
                paramName: 'attachment',
                dataType: 'json',
                dropZone: null,
                formData: {
                    pKEY: 'upload_file',
                    file_type: types,
                    module_id: modid,
                    app_id: appid,
                    hash: hash
                },
                url: url,
                add: function( e, data ){
                    jQuery('#file-wrap-' + hash).removeClass('uploaded');

                    if( types == 'images' )
                    {
                        jQuery('#file-wrap-' + hash).find('img').prop('src', window.URL.createObjectURL( data.files[0] ) );
                    }
                    else
                    {
                        jQuery('#file-wrap-' + hash).find('img').prop('src', dfsrc );
                        jQuery('#file-wrap-' + hash).find('span').text( data.files[0].name );
                    }

                    delete_uploaded_file( hash, dfsrc );

                    data.context = jQuery('#file-wrap-' + hash);
                    data.submit();
                },
                progress: function( e, data ){
                    var progress = parseInt( data.loaded / data.total * 100, 10 );
                    
                    jQuery('#file-wrap-' + hash + ' .progress').text( progress + '%' );
                },
                always: function( e, data ){                
                    data.context.addClass('uploaded');
                },
                fail: function( e, data ){
                    alert( 'Upload failed! Something wrong' );

                    data.context.find('img').prop('src', dfsrc );
                },
                done: function( e, data ){
                    if( data.textStatus == 'success' )
                    {
                        if( data.jqXHR.responseJSON.result == 'success' )
                        {
                            data.context.find('.overlay a').attr('data-id', data.jqXHR.responseJSON.id);
                            data.context.find('.overlay').removeClass('hidden');

                            data.context.find('input').val( data.jqXHR.responseJSON.id );
                            data.context.find('img').prop('src', data.jqXHR.responseJSON.src );
                        }
                        else
                        {
                            if( data.jqXHR.responseJSON.result == 'not-supported' )
                            {
                                alert( 'Not supported file' );
                            }
                            else
                            {
                                alert( 'Upload failed! Something wrong' );
                            }

                            data.context.find('.overlay a').attr('data-id', '' );
                            data.context.find('.overlay').addClass('hidden');

                            data.context.find('img').prop('src', dfsrc );
                        }
                    }
                    else
                    {
                        alert( 'Upload failed! Something wrong' );

                        data.context.find('.overlay a').attr('data-id', '' );
                        data.context.find('.overlay').addClass('hidden');

                        data.context.find('input').val( '' );
                        data.context.find('img').prop('src', dfsrc );
                    }
                }
            });

            delete_uploaded_file( hash, dfsrc );
        });
    }

    if( jQuery('.upload-gallery').length > 0 )
    {
        jQuery('.upload-gallery').each( function( e ){
            var cont  = jQuery(this);
            var drop  = cont.parent().find('.file-drop-zone');
            var input = cont.find('input[type=file]');

            var url   = jQuery('#ajax_url').val();
            var appid = jQuery('#lpost_id').val();
            var modid = jQuery('#lmodule').val();

            var types = input.data('filetype');
            var field = input.data('field');
            var hash  = input.data('hash');
            var dfsrc = input.data('src');

            if( typeof appid == 'undefined' )
            {
                appid = jQuery('#lterm_id').val();
            }

            var sel = jQuery('#file-drop-zone-' + hash);

            input.fileupload({
                paramName: 'attachment',
                dataType: 'json',
                dropZone: drop,
                formData: {
                    pKEY: 'upload_file',
                    file_type: types,
                    module_id: modid,
                    app_id: appid,
                    hash: hash
                },
                url: url,
                add: function( e, data ){
                    var temp = jQuery('#cloned-wrapp-' + hash).find('.item').clone();
                        temp.find('img').prop('src', window.URL.createObjectURL( data.files[0] ) );

                    sel.find('.drag-drop').hide();
                    sel.append( temp );

                    delete_drop_file( hash );

                    data.context = sel.find('.item:last-child');
                    data.submit();
                },
                progress: function( e, data ){
                    var progress = parseInt( data.loaded / data.total * 100, 10 );
                    
                    data.context.find('progress').text( progress + '%' );
                },
                fail: function( e, data ){
                    data.context.addClass('not-uploaded');
                },
                done: function( e, data ){
                    if( data.textStatus == 'success' )
                    {
                        if( data.jqXHR.responseJSON.result == 'success' )
                        {
                            data.context.find('input').attr('name', 'fields[ladditional][' + field + '][]');
                            data.context.find('.delete').attr('data-id', data.jqXHR.responseJSON.id);
                            data.context.find('img').prop('src', data.jqXHR.responseJSON.src);
                            data.context.find('input').val( data.jqXHR.responseJSON.id );
                            data.context.addClass('uploaded');
                        }
                        else
                        {
                            data.context.addClass('not-uploaded');
                        }
                    }
                    else
                    {
                        data.context.addClass('not-uploaded');
                    }
                },
                stop: function( e ){
                    if( sel.find('.not-uploaded').length > 1 )
                    {
                        alert( 'Some files can\'t be uploaded, maybe because you uploaded a file with the wrong format or some other reason' );
                    }
                    else if( sel.find('.not-uploaded').length == 1 )
                    {
                        alert( 'Upload failed! Something wrong, maybe because you uploaded a file with the wrong format or some other reason' );
                    }

                    if( sel.find('.not-uploaded').length > 0 )
                    {
                        sel.find('.not-uploaded').each(function(){
                            jQuery(this).remove();
                        })
                    }

                    if( sel.find('.item').length == 0 )
                    {
                        sel.find('.drag-drop').show();
                    }
                }
            });

            delete_drop_file( hash );
        });
    }
}

function delete_uploaded_file( hash, dfsrc )
{
    var sel = jQuery('#file-wrap-' + hash);

    sel.find('.delete').unbind('click');
    sel.find('.delete').on('click', function(){
        var id = jQuery(this).attr('data-id');

        if( typeof id == 'undefined'  )
        {
            alert( 'Something wrong on the request' );
        }
        else
        {
            var url = jQuery('#ajax_url').val();
            var prm = new Object;
                prm.pKEY       = 'delete_file';
                prm.lattach_id = id;

            jQuery.ajax({
                url: url,
                data: prm,
                type: 'POST',
                dataType : 'json',
                success: function(e){
                    if( e.result == 'success' )
                    {
                        sel.find('.overlay a').prop('data-id', '' );
                        sel.find('.overlay').addClass('hidden');

                        sel.find('input').val( '' );
                        sel.find('img').prop('src', dfsrc );
                    }
                    else
                    {
                        alert( e.message );
                    }
                },
                error: function(e){
                   alert( 'Something wrong on the request' );
                }
            })
        }
    });
}

function delete_drop_file( hash )
{
    var sel = jQuery('#file-drop-zone-' + hash);

    sel.find('.delete').unbind('click');
    sel.find('.delete').on('click', function(){
        var id = jQuery(this).attr('data-id');

        if( typeof id == 'undefined'  )
        {
            jQuery(this).parent().parent().fadeOut(80, function(){
                jQuery(this).remove();
            });

            if( sel.find('.item').length == 0 )
            {
                sel.find('.drag-drop').show();
            }
        }
        else
        {
            var prt = jQuery(this).parent().parent();
            var url = jQuery('#ajax_url').val();
            var prm = new Object;
                prm.pKEY       = 'delete_file';
                prm.lattach_id = id;

            jQuery.ajax({
                url: url,
                data: prm,
                type: 'POST',
                dataType : 'json',
                success: function(e){
                    if( e.result == 'success' )
                    {
                        prt.fadeOut(80, function(){
                            jQuery(this).remove();
                            
                            if( sel.find('.item').length == 0 )
                            {
                                sel.find('.drag-drop').show();
                            };
                        });
                    }
                    else
                    {
                        alert( e.message );
                    }
                },
                error: function(e){
                   alert( 'Something wrong on the request' );
                }
            })
        }
    });
}

function init_accordion()
{
    if( jQuery('.accordion-field').length > 0 )
    {
        jQuery('.accordion-field').each( function( e ){
            var sel = jQuery(this);

            sel.accordion({
                header: '.repeater-anchor',
                collapsible: true,
                event: 'click',
                active: 0,
                create: function( event, ui ) {
                    init_group_field_action( sel );
                }
            }).sortable({
                items: '.repeater-item',
                handle: '.repeater-anchor',
                revert: false,
                start: function(e, ui){
                    console.log(jQuery( ui.item ));
                    var id = jQuery( ui.item ).find('.tinymce').attr('id');

                    tinymce.execCommand( 'mceRemoveEditor', false, id );
                },
                stop: function(e, ui){
                    var id = jQuery( ui.item ).find('.tinymce').attr('id');

                    tinymce.execCommand( 'mceAddEditor', false, id );

                    init_group_field_action( sel );
                }
            });
        });
    }
}

function init_group_field_action( sel )
{
    init_tinymce( '.accordion-field .tinymce' );
    init_upload();

    sel.find('.repeater-anchor').each( function( i, e ){
        var title = jQuery(this).data('title');

        jQuery(this).find('h4').text( title + ' ' + ( i + 1 ) );
    });

    sel.find('.repeater-action .add-item').unbind('click');
    sel.find('.repeater-action .add-item').on('click', function( ele ){
        var url = jQuery('#ajax_url').val();
        var prm = new Object;
            prm.pKEY   = 'add_group';
            prm.items  = jQuery(this).data('items');
            prm.object = jQuery(this).data('object');

        jQuery.ajax({
            url: url,
            data: prm,
            type: 'POST',
            dataType : 'html',
            success: function( content ){
                if( content != '' )
                {
                    sel.append( content );
                    sel.accordion( 'refresh' );

                    init_group_field_action( sel );
                }
                else
                {
                    alert( 'Something wrong on the request' );
                }
            },
            error: function(){
               alert( 'Something wrong on the request' );
            }
        });
    });

    sel.find('.repeater-action .delete-item').unbind('click');
    sel.find('.repeater-action .delete-item').on('click', function( ele ){
        if( sel.find('.repeater-item').length > 1 )
        {
            jQuery(this).parents('.repeater-item').remove();

            sel.accordion( 'refresh' );

            init_group_field_action( sel );
        }
        else
        {
            alert( 'There\'s at least one group left and can\'t be deleted' );
        }
    });
}

function init_show_on()
{
    init_show_on_func();
    
    jQuery('select, input[type=checkbox], input[type=radio]').on('change', function(){
        init_show_on_func();
    });
}

function init_show_on_func()
{
    //-- Conditional Field Show
    var data = jQuery('[data-show-on-key]').length;

    if( data > 0 )
    {
        jQuery('[data-show-on-key]').each(function( i, e ){
            var sval  = jQuery(this).data('show-on-value');
            var field = jQuery(this).data('show-on-key');

            var cval = jQuery('#' + field).val();
                sval = sval.toString().split(',');

            if( cval != sval )
            {
                jQuery(this).hide();
            }
            else
            {
                jQuery(this).show();
            }
        })
    }

    //-- Conditional Field Show on Group
    var data = jQuery('[data-show-on-group-key]').length;

    if( data > 0 )
    {
        jQuery('[data-show-on-group-key]').each(function( i, e ){
            var sval  = jQuery(this).data('show-on-group-value');
            var field = jQuery(this).data('show-on-group-key');

            var cval = jQuery('#' + field).val();
                sval = sval.toString().split(',');

            if( jQuery.inArray( cval, sval ) < 0 )
            {
                jQuery(this).hide();
            }
            else
            {
                jQuery(this).show();
            }
        });
    }
}

function init_translation()
{
    jQuery('.translation-select').on('change', function( ele ){
        location.href = this.value;
    });
}

function trim( str )
{
    var str = str.replace(/^\s\s*/, ''),
        i   = str.length,
        ws  = /\s/;
    while( ws.test( str.charAt(--i) ) );
    return str.slice(0, i + 1);
}

function show_popup( message, callback )
{
    jQuery('<div></div>').dialog({
        dialogClass: 'ui-dialog-no-titlebar',
        draggable: false,
        resizable: false,
        modal: true,
        open: function(){
            jQuery(this).html( message );
        },
        close: function(){
            if( callback != '' )
            {
                eval( callback );
            }
        },
        buttons:{
            Close: function()
            {
                jQuery( this ).dialog('close');
            }
        }
    });
}

function init_scrollbar()
{
    var hm = jQuery(window).height();
    var hh = jQuery('.header').height();
    var hs = hm - hh;

    jQuery('.content-sr-new-style.container.container-sr .content .sidebarleft').css('height', hs + 'px');
    jQuery('.content-sr-new-style.container.container-sr .content .sidebarleft .some-area').css('height', hs + 'px');

    jQuery('#scrollbar-sidebar').mCustomScrollbar({
        autoHideScrollbar: true,
        theme: 'minimal',
        mouseWheelPixels: 300,
        onScroll: function() {
            console.log('scroll complated');
        },
        whileScrolling: function() {
            console.log('while scroll');
        }
    });
}

function init_sidebar()
{
    jQuery('.menubo li.arrowsub a').click(function() {
        var id = jQuery(this).find('.arrow').attr('rel');

        jQuery('#' + id).slideToggle('fast', function() {
            var display = jQuery('#' + id).css("display");

            if( display == 'block' )
            {
                jQuery('[rel=' + id + ']').addClass('arrowsub');
            }
            else
            {
                jQuery('[rel=' + id + ']').removeClass('arrowsub');
            }
        });
    });

    jQuery('.menubo li.arrowsub').each(function(index) {
        var n = jQuery('li.arrowsub ul:eq(' + index + ')').find('a.aktif').length;

        if( n > 0 )
        {
            jQuery(this).find('a:eq(0)').addClass('aktif');
            jQuery(this).find('span.arrow').addClass('arrowsub');

            jQuery('li.arrowsub ul:eq(' + index + ')').css('display', 'block');
        }
    });
}

function init_select_option()
{
    if( jQuery('.chzn-select').length > 0 )
    {
        jQuery('.chzn-select').chosen({
            search_contains: true
        });
    }
}

function init_schedules()
{
    init_number( '.text-rate' );

    jQuery('#ldaterange').on('apply.daterangepicker', function( ev, picker ) {
        var start = picker.startDate.format( 'MMMM DD, YYYY' );
        var end   = picker.endDate.format( 'MMMM DD, YYYY' );

        if( jQuery('#lduration').length > 0 )
        {
            var night = moment( end ).diff( moment( start ), 'days');
            var days  = night + 1;

            if( night == 0 )
            {
                night = 1;
            }
            
            jQuery('#lduration').val( days + 'D' + night + 'N' );
        }
    });
}

function init_packages()
{
    var dtype = jQuery('#ltype').val() || 0;
    
    jQuery('.list-period-type').hide();
    jQuery('.list-period-type-' + dtype).show();

    jQuery('#ltype').on( 'change', function(){
        var type = jQuery(this).val() || 0;

        jQuery('.list-period-type').hide();
        jQuery('.list-period-type-' + type).show();
    });

    jQuery( '[name=package_form] #lpost_id' ).on('change', function(){
        var frm = jQuery('[name=package_form]');
        var url = jQuery('#ajax_url').val();
        var prm = new Object;
            prm.pKEY        = 'get_accommodation_package_rate';
            prm.lpackage_id = frm.find('#lpackage_id').val();
            prm.lpost_id    = frm.find('#lpost_id').val();

        jQuery.ajax({
            url: url,
            data: prm,
            type: 'POST',
            dataType : 'json',
            beforeSend: function( xhr ){
                frm.parent().addClass('processing');
            },
            success: function( e ){
                if( e.result == 'success' )
                {
                    jQuery('.list-room-rate').html( e.data );

                    init_number( jQuery('.list-room-rate').find('.text-rate') );
                }
                else
                {
                    jQuery('.list-room-rate').html('');
                }
            },
            error: function( e ){
                show_popup( 'Something wrong, try again later' );
            },
            complete: function( e ){
                frm.parent().removeClass('processing');
            }
        });
    });

    init_number( '.text-rate' );
}

function init_rate()
{
    if( jQuery( '[name=rate_form] #laccommodation' ).length > 0 )
    {        
        jQuery( '[name=rate_form] #laccommodation' ).on('change', function(){
            var frm = jQuery('[name=rate_form]');
            var url = jQuery('#ajax_url').val();
            var prm = new Object;
                prm.pKEY = 'get_accommodation_rate';
                prm.rid  = jQuery('#lrate_id').val();
                prm.id   = jQuery(this).val();

            jQuery.ajax({
                url: url,
                data: prm,
                type: 'POST',
                dataType : 'json',
                beforeSend: function( xhr ){
                    frm.parent().addClass('processing');
                },
                success: function( e ){
                    if( e.result == 'success' )
                    {
                        jQuery('.list-room-rate').html( e.data );

                        init_number( jQuery('.list-room-rate').find('.text-rate') );

                        init_period_picker();

                        init_rate_period();
                    }
                    else
                    {
                        jQuery('.list-room-rate').html('');
                    }
                },
                error: function( e ){
                    show_popup( 'Something wrong, try again later' );
                },
                complete: function( e ){
                    frm.parent().removeClass('processing');
                }
            });
        });
    }
}

function init_rate_period()
{    
    jQuery( '[name=rate_form] #add_period' ).unbind();
    jQuery( '[name=rate_form] #add_period' ).on('click', function(){
        var frm = jQuery('[name=rate_form]');
        var url = jQuery('#ajax_url').val();

        jQuery.ajax({
            url: url,
            type: 'POST',
            dataType : 'html',
            data: { 
                pKEY : 'add_rate_period' 
            },
            beforeSend: function( xhr ){
                frm.parent().addClass('processing');
            },
            success: function( e ){                
                jQuery('.list-field-period-wrap').append( e );

                init_period_picker();
            },
            error: function( e ){
                show_popup( 'Something wrong, try again later' );
            },
            complete: function( e ){
                frm.parent().removeClass('processing');
            }
        });
    });
}

function init_period_picker()
{
    if( jQuery('.period-item .period-picker').length > 0 )
    {
        jQuery('.period-item').each( function( i, e ){
            var sel = jQuery(this);

            sel.find('.period-start').datepicker({ 
                changeYear: false, 
                dateFormat: 'dd MM',
                onSelect: function( dateText, inst ) {
                    var date  = sel.find('.period-start').datepicker('getDate');
                    var date2 = sel.find('.period-end').datepicker('getDate');

                    if( date2 == null )
                    {
                        date2 = date;
                    }

                    sel.find('.period-end').datepicker('setDate', date2);
                    sel.find('.period-end').datepicker('option', 'minDate', date);
                }
            }).focus(function () {
                jQuery('.ui-datepicker-year').hide();
            });

            sel.find('.period-end').datepicker({ 
                changeYear: false, 
                dateFormat: 'dd MM',
                onSelect: function( dateText, inst ) {
                    var date  = sel.find('.period-end').datepicker('getDate');
                    var date2 = sel.find('.period-start').datepicker('getDate');

                    if( date2 == null )
                    {
                        date2 = date;
                    }

                    sel.find('.period-start').datepicker('setDate', date2);
                    sel.find('.period-start').datepicker('option', 'maxDate', date);
                }
            }).focus(function () {
                jQuery('.ui-datepicker-year').hide();
            });
        });

        jQuery('.period-item .actiond2').unbind()
        jQuery('.period-item .actiond2').on('click', function(){
            var frm = jQuery('[name=rate_form]');
            var url = jQuery('#ajax_url').val();
            var sel = jQuery(this);

            jQuery.ajax({
                url: url,
                type: 'POST',
                dataType : 'json',
                data: { 
                    pKEY : 'delete_rate_period',
                    id   : sel.data('id')
                },
                beforeSend: function( xhr ){
                    frm.parent().addClass('processing');
                },
                success: function( e ){
                    if( e.result == 'success' )
                    {
                        sel.parent().remove();
                    }
                    else
                    {
                        show_popup( e.message );
                    }
                },
                error: function( e ){
                    show_popup( 'Something wrong, try again later' );
                },
                complete: function( e ){
                    frm.parent().removeClass('processing');
                }
            });
        });
    }
}

jQuery(document).ready(function(){
    init_select_option();
    init_scrollbar();
    init_sidebar();
});

jQuery(window).resize(function() {
    init_scrollbar();
});