function initTinyMce( selector )
{
    tinymce.init({
        automatic_uploads: true, 
        selector: selector,
        image_advtab: true,
        image_title: true,
        theme: 'modern',
        height: 300,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
    });
}

jQuery(function() {
    initTinyMce( '.tinymce' );
});