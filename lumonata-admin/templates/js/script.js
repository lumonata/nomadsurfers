jQuery(document).ready(function() {
  

  var dateToday = new Date(); 
  var dateFormat = "dd MM yy",
  from = $( "#from" )
  .datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    changeYear: true,
    numberOfMonths: 1,
  })
  .on( "change", function() {
    to.datepicker( "option", "minDate", getDate( this ) );
  }),
  to = $( "#to" ).datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    changeYear: true,
    numberOfMonths: 1
  })
  .on( "change", function() {
    from.datepicker( "option", "maxDate", getDate( this ) );
  });

  var dateFormat = "mm/dd/yy",
  from = $( "#checkin" )
  .datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    changeYear: true,
    numberOfMonths: 1,
    minDate: dateToday,
  })
  .on( "change", function() {
    to.datepicker( "option", "minDate", getDate( this ) );
  }),
  to = $( "#checkout" ).datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    changeYear: true,
    numberOfMonths: 1,
    minDate: dateToday,
  })
  .on( "change", function() {
    from.datepicker( "option", "maxDate", getDate( this ) );
  });

 
  $( "#datebook" ).datepicker({
    numberOfMonths: 1,
    minDate: dateToday,
    dateFormat: 'dd MM yy'
  });

  $( ".date" ).datepicker({
    numberOfMonths: 1,
    minDate: dateToday,
  });

  $(".birthday").datepicker({
    numberOfMonths: 1,
    changeMonth: true,
    changeYear: true,
		yearRange: "-100:+0",
    dateFormat: 'dd MM yy',
    maxDate: dateToday
  });


  function getDate( element ) {
    var date;
    try {
      date = $.datepicker.parseDate( dateFormat, element.value );
    } catch( error ) {
      date = null;
    }

    return date;
  }

  

  room_action();

  jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"></div><div class="quantity-button quantity-down"></div></div>').insertAfter('#num_guest');

  jQuery('.quantity').each(function() {
    var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

    btnUp.click(function() {
      console.log(1);
      var oldValue = parseFloat(input.val());
      if (oldValue >= max) {
        var newVal = oldValue;
      } else {
        var newVal = oldValue + 1;
      }
      spinner.find("input").val(newVal);
      spinner.find("input").trigger("change");
    });

    btnDown.click(function() {
      var oldValue = parseFloat(input.val());
      if (oldValue <= min) {
        var newVal = oldValue;
      } else {
        var newVal = oldValue - 1;
      }
      spinner.find("input").val(newVal);
      spinner.find("input").trigger("change");
    });

  });


  // /* ------------------ START HOME SLIDER ------------------- */
  // var item_length = $('.autoplay > div').length-1;
  // // var slider = $('.autoplay').slick({
  // //                 slidesToShow: 1,
  // //                 slidesToScroll: 1,
  // //                 autoplay: true,
  // //                 autoplaySpeed: 4000,
  // //                 arrows: false,
  // //                 speed: 1500,
  // //                 dots:false
  // //               });
  // var no = 1;
  // jQuery('.dot-'+no).addClass('selected');
  // slider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
  //     jQuery('.dot-'+no).removeClass('selected');
  //     jQuery('.dot-'+(no+1)).addClass('selected');
  //     //console.log(no);
  //     //
  //     if ((item_length - 1) === slider.slick('slickCurrentSlide')) {
  //       no = no+1;
  //       //console.log(no);
  //       no = 0;
  //     }
  //     else{
  //       no = no+1;
  //       //console.log(no);
  //       jQuery('.dot-'+(item_length+1)).removeClass('selected');
  //     }
  // });

})

function slider_go(text) {
  //jQuery("#"+text+'-go').click(function() {
    jQuery('html, body').animate({
        scrollTop: jQuery("#"+text).offset().top - 100
    }, 2000);
    //console.log('hoi');
  //});
}

function openNav() {
    var ww = jQuery(window).width();
    document.getElementById("mySidenav").style.width = "250px";
    jQuery('#overlay-close').fadeIn(300);
}

function room_action(){
  jQuery('#num_guest').click(function(){
    jQuery('#num_guest').val(1);
    //console.log(1);
  })
}

function popup(msg){
  jQuery('#content-popup').html(msg);
  jQuery('#overlay-close').fadeIn(300);
  jQuery('#popup').fadeIn(300);
}

function closelog(){
  jQuery('#overlay-close').fadeOut(300);
  jQuery('#container-popup-log').fadeOut(300);
  jQuery('#container-register').fadeOut(300);
  jQuery('#container-forgot-password').fadeOut(300);
  jQuery('#popup').fadeOut(300);
  document.getElementById("mySidenav").style.width = "0px";
}

function backtop(){
  jQuery('html').animate({scrollTop:0}, 600);//IE, FF
  jQuery('body').animate({scrollTop:0}, 600);//chrome, don't know if Safari works
}

function change(text){
  jQuery('.tab-text').removeClass('active');
  jQuery('.detail-show').fadeOut(300);
  jQuery('#'+text+'-tab').addClass('active');
  jQuery('#'+text+'-detail').fadeIn(300);
}

function gotoslick(index){
  jQuery('.autoplay').slick('slickGoTo', index);
}

function valid_email(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

/*LOGIN FUNCTION*/
function login_popup(){
  jQuery('#overlay-close').fadeIn(300);
  jQuery('#container-popup-log').fadeIn(300);
}

function forgot_password() {
  jQuery('#container-popup-log').fadeOut(300);
  jQuery('#container-register').fadeOut(300);
  jQuery('#overlay-close').fadeIn(300);
  jQuery('#container-forgot-password').fadeIn(300);
}

function register() {
  jQuery('#container-popup-log').fadeOut(300);
  jQuery('#container-forgot-password').fadeOut(300);
  jQuery('#overlay-close').fadeIn(300);
  jQuery('#container-register').fadeIn(300);
}

function login(event){
  var url = jQuery('#url_next').val()+'/plugin/signup_member/appAction.php';

  jQuery('#email,#password').removeClass('error');
  var email = jQuery('#email').val();
  var password = jQuery('#password').val();
  var error=0;

  if(email=='' && !valid_email(email)){
    jQuery('#email').addClass('error');
    error++;
  }
  if(password==''){
    jQuery('#password').addClass('error');
    error++;
  }

  if(error==0){

    var dt = new Object();
        dt.pKEY = 'login_action';
        dt.login_email = email;
        dt.login_password = password;

    var txt_button = jQuery(event.target).attr('data-onprocess');
    jQuery(event.target).text(txt_button);

    jQuery.post(url,dt,function(response){

      var res = jQuery.parseJSON(response);
      if(res.result=='success'){
        txt_button = jQuery(event.target).attr('data-label');
        jQuery(event.target).text(txt_button);
        window.location.href = res.url;
      }else{
        txt_button = jQuery(event.target).attr('data-label');
        jQuery(event.target).text(txt_button);
        //popup(res.msg);
        jQuery('#email').val('');
        jQuery('#password').val('');
      }
    })    
  }
}

function send_link_reset_password(event){
  var url = jQuery('#url_next').val()+'/plugin/header/appAction.php';
  var email = jQuery('#email_forgot').val();

  if(email=='') {
    jQuery('#email_forgot').addClass('error');
    return;
  }
  if(email!='' && !valid_email(email)) {
    jQuery('#email_forgot').addClass('error');
    return;
  }

  var dt = new Object();
      dt.pKEY = 'send_link_reset_password';
      dt.email = email;
    
  var txt_button = jQuery(event.target).attr('data-onprocess');
  jQuery(event.target).text(txt_button);

  jQuery.post(url,dt,function(response){
    var res = jQuery.parseJSON(response);
    //popup(res.msg);
    if(res.status=='success'){  
      txt_button = jQuery(event.target).attr('data-label');
      jQuery(event.target).text(txt_button);
      closelog();
      popup(res.msg);          
      setTimeout(function(){
        window.location.href = res.url;
      },6000);
    }
    else{
      txt_button = jQuery(event.target).attr('data-label');
      jQuery(event.target).text(txt_button);
      closelog();
      popup(res.msg);
    }
    jQuery('#email_forgot').val('');
  })
}

function reset_password(event){
  var url = jQuery('#url_next').val()+'/plugin/header/appAction.php';

  var require = '.require_pass';
  var error_id  = '';
  var error = 0;

  var pass = jQuery('#pass_change').val();
  var repeat_pass = jQuery('#conf_pass_change').val();

  jQuery(require).removeClass('error');

  jQuery(require).each(function(e,v){
    var value = jQuery(this).val();
    var id = jQuery(this).attr('id');
    if(value=='' || typeof value=='undefined'){
      jQuery(this).addClass('error');
      error_id = (error_id ==''? id:error_id);
      error++;  
    } 
  })

  if(pass!='' && pass!=repeat_pass){
    jQuery('#conf_pass_change').addClass('error');
    error++;
  }

  if(error==0){
    var dt = new Object();
        dt.pKEY = 'reset_password';
        dt.pass = pass;
        dt.code = jQuery('#code').val();

    var txt_button = jQuery(event.target).attr('data-txt_onprocess');
    jQuery(event.target).val(txt_button+'...');
            
    jQuery.post(url,dt,function(response){
      var res = jQuery.parseJSON(response);
      //popup(res.msg);
      if(res.status=='success'){  
        txt_button = jQuery(event.target).attr('data-value');
        jQuery(event.target).val(txt_button);
        jQuery(require).val('');
        popup(res.msg);          
        setTimeout(function(){
          window.location.href = res.url;
        },6000);
      }
      txt_button = jQuery(event.target).attr('data-value');
      jQuery(event.target).val(txt_button);
      jQuery(require).val('');
    })
  }
}

function sign_up(event){
  var url = jQuery('#url_next').val()+'/plugin/header/appAction.php';
  var require = '.req';
  var error_id  = '';
  var error = 0;
  var news = 0;

  var first_name        = jQuery('#f_name').val();
  var last_name         = jQuery('#l_name').val();
  var email             = jQuery('#email_reg').val();
  var confirm_email     = jQuery('#conf_email_reg').val();
  var password          = jQuery('#password_reg').val();
  var confirm_password  = jQuery('#conf_password_reg').val();
  var error=0;

  jQuery(require).removeClass('error');

  /*jQuery(require).each(function(e,v){
    var value = jQuery(this).val();
    var id = jQuery(this).attr('id');
    if(value=='' || typeof value=='undefined'){
      jQuery(this).addClass('error');
      error_id = (error_id ==''? id:error_id);
      error++;  
    } 
  })

  if(!valid_email(email)) {
    jQuery('#email').addClass('error');
    error_id = (error_id ==''? 'email':error_id);
    error++;
  }

  if(email!=''){
    if(email!=confirm_email){
      jQuery('#confirm_email_r').addClass('error');
      error_id = (error_id ==''? 'confirm_email':error_id);
      error++;
    }
  }

  if(password!=''){
    if(password!=confirm_password){
      jQuery('#confirm_password_r').addClass('error');
      error_id = (error_id ==''? 'confirm_password':error_id);
      error++;
    }
  }*/

  var term = (jQuery('#news').is(':checked')?true:false);
  if (term) {
    news = 1;
  }

  console.log(news);
  if(error==0){
    var dt =  new Object();
        dt.pKEY             = 'save_member_data';
        dt.first_name       = first_name;
        dt.last_name        = last_name;
        dt.email            = email;
        dt.confirm_email    = confirm_email;
        dt.password         = password;
        dt.confirm_password = confirm_password;
        dt.term             = news;

    var txt_button = jQuery(event.target).attr('data-onprocess');
    jQuery(event.target).text(txt_button);

    jQuery.post(url,dt,function(response){
      var res = jQuery.parseJSON(response);
      if(res.status=='success'){
        txt_button = jQuery(event.target).attr('data-label');
        jQuery(event.target).text(txt_button);
        closelog();
        popup(res.msg);
      }else{
        txt_button = jQuery(event.target).attr('data-label');
        jQuery(event.target).text(txt_button);
        closelog();
        popup(res.msg);
      }
    });
  }else{
    if(error_id!=''){
      //go_to_min_h(error_id,150)
      }
  } 
}

/*TOUR INQUIRY*/
function send_inq_tour(event){
  var url = jQuery('#url_next').val()+'/plugin/header/appAction.php';
  var id = jQuery('#id_tour').val();
  var require = '.require_t';
  jQuery(require).removeClass('error');

  var error = 0;
  jQuery(require).each(function(e,v){
    var value = jQuery(this).val();
    var id = jQuery(this).attr('id');
    if(value=='' || typeof value=='undefined'){
      jQuery(this).addClass('error');
      error++;  
    } 
  })

  if(error==0){
    var email = jQuery('#email_t').val();
    if(!valid_email(email)){
      jQuery('#email_t').addClass('error');
      error++;  
    } 
  }

  console.log(error);
  if(error==0){
    var dt          =  new Object();
    dt.pKEY         = 'send_inquiry_booking_tour';
    dt.id           = id;
    dt.name         = jQuery('#name_t').val();
    dt.email        = jQuery('#email_t').val();
    dt.date         = jQuery('#date_t').val();
    dt.adult        = jQuery('#adult_t').val();
    dt.children     = jQuery('#children_t').val();

    var btn_text = jQuery(event.target).attr('data-onprocess');
    jQuery(event.target).text(btn_text);
    jQuery.post(url,dt,function(response){
      var res = jQuery.parseJSON(response);
      if (res.status=='success') {
        btn_text = jQuery(event.target).attr('data-label');
        jQuery(event.target).text(btn_text);
        jQuery('#name_t').val('');
        jQuery('#email_t').val('');
        jQuery('#date_t').val('');
        jQuery('#adult_t').val(0);
        jQuery('#children_t').val(0);
        closelog();
      }
    });
  }

}

/*SUBSCRIBE FUNCTION*/
function subscribe(event){
  var url = jQuery('#url_next').val()+'/plugin/footer/appAction.php';
  //document.write(1);
  var require = '.require_sub';
  var error_id  = '';
  var error = 0;

  jQuery(require).removeClass('error');

  jQuery(require).each(function(e,v){
    var value = jQuery(this).val();
    var id = jQuery(this).attr('id');
    if(value=='' || typeof value=='undefined'){
      jQuery(this).addClass('error');
      error_id = (error_id ==''? id:error_id);
      error++;  
    } 
  })

  if(error==0){
    var email = jQuery('#email_sub').val();
    if(!valid_email(email)){
      jQuery('#email_sub').addClass('error');
      error++;  
    } 
  }

  if(error==0){

    var dt =  new Object();
    dt.pKEY             = 'mailchimp_subscribe';
    dt.name             = jQuery('#name_sub').val();
    dt.email            = jQuery('#email_sub').val();

    var txt_button = jQuery(event.target).attr('data-txt_onprocess');
    jQuery(event.target).text(txt_button);

    jQuery.post(url,dt,function(response){
      var res = jQuery.parseJSON(response);
      if(res.status=='success'){
        popup(res.msg);
        jQuery(require).val('');
        txt_button = jQuery(event.target).attr('data-value');
        jQuery(event.target).text(txt_button);
      }else{
        popup(res.msg);
        jQuery(require).val('');
        txt_button = jQuery(event.target).attr('data-value');
        jQuery(event.target).text(txt_button);
      }
    });
  }
}

