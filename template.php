<?php

$tp = new Template( THEME_DIR );
$gb = new globalFunctions();
$ac = new actions();

require( 'content.php' );

if( in_array( $mod, array( 'agent' ) ) )
{
    require_once( APPS_DIR . '/class.agents.php' );

    $obj = new agents( $ac, $path );

    $obj->load();
}
else if( in_array( $mod, array( 'member' ) ) )
{
    require_once( APPS_DIR . '/class.members.php' );

    $obj = new members( $ac, $path );

    $obj->load();
}
else
{
	$tp->set_file( 'home', 'index.html' );
	$tp->set_block( 'home', 'mainBlock', 'mBlock' );
	$tp->set_block( 'home', 'maintenanceBlock', 'mtBlock' );

	if( UNDER_CONSTRUCTION )
	{
		$tp->set_var( 'bemail', $gb->get_setting_value( 'email_booking' ) );
		
		$tp->Parse( 'mtBlock', 'maintenanceBlock', false );
	}
	else
	{
		$sphone = $gb->get_setting_value( 'phone_europe' );

		//-- ADD default JS assets
		$ac->add_actions( 'jvc', $gb->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js' ) );
		$ac->add_actions( 'jvc', $gb->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js' ) );
		$ac->add_actions( 'jvc', $gb->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js' ) );
		$ac->add_actions( 'jvc', $gb->get_jvs( 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js' ) );
		$ac->add_actions( 'jvc', $gb->get_jvs( 'https://cdn.jsdelivr.net/npm/mmenu-js@9.3.0/dist/mmenu.min.js' ) );
		$ac->add_actions( 'jvc', $gb->get_jvs( 'https://cdn.jsdelivr.net/npm/blazy@1.8.2/blazy.min.js' ) );

		//-- ADD default CSS assets
		$ac->add_actions( 'css', $gb->get_css( 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css' ) );
		$ac->add_actions( 'css', $gb->get_css( 'https://cdn.jsdelivr.net/npm/mmenu-js@9.3.0/dist/mmenu.min.css' ) );
	    $ac->add_actions( 'css', $gb->get_css( '//' . THEME_URL . '/assets/css/style.css' ) );

		$tp->set_var( 'search_txt', $gb->translate( 'search', 'SEARCH' ) );
		$tp->set_var( 'contact_btn_txt', $gb->translate( 'contact-us', 'Contact Us' ) );

		$tp->set_var( 'subscribe_title', $gb->translate( 'sign-up-for-last-minute-deals', 'SIGN-UP FOR LAST MINUTE DEALS' ) );
		$tp->set_var( 'subscribe_btn_txt', $gb->translate( 'sign-me-up', 'SIGN ME UP' ) );
		$tp->set_var( 'subscibe_loader_txt', $gb->translate( 'waiting', 'WAITING', 3 ) );
		$tp->set_var( 'subscibe_email', $gb->translate( 'email-2', 'Email' ) );

		$tp->set_var( 'site_url', SITE_URL );
		$tp->set_var( 'theme_url', THEME_URL );
		$tp->set_var( 'image_url', IMAGE_URL );
		$tp->set_var( 'home_url', $gb->get_home_url() );

		$tp->set_var( 'message', $gb->init_popup_message() );
		$tp->set_var( 'switcher_lang', $gb->init_switcher_lang() );
		$tp->set_var( 'bottom_menu', $gb->init_nav_menu( 'BOTTOM MENU' ) );
		$tp->set_var( 'mobile_menu', $gb->init_nav_menu( 'MOBILE MENU' ) );
		$tp->set_var( 'footer_menu_2', $gb->init_nav_menu( 'QUICK LINKS', true ) );
		$tp->set_var( 'footer_menu_1', $gb->init_nav_menu( 'WORK WITH US', true ) );
		$tp->set_var( 'footer_menu_3', $gb->init_nav_menu( 'USEFUL INFORMATION', true ) );

		$tp->set_var( 'fb_link', $gb->get_setting_value( 'fb_link' ) );
		$tp->set_var( 'ig_link', $gb->get_setting_value( 'ig_link' ) );
		$tp->set_var( 'yt_link', $gb->get_setting_value( 'yt_link' ) );	

		$sphone = $gb->get_setting_value( 'phone_europe' );

		$tp->set_var( 'bphone', $gb->get_format_phone_number( $sphone ) );
		$tp->set_var( 'tphone', $gb->get_format_phone_number( $sphone, 'tel' ) );
		$tp->set_var( 'ct_url', $gb->get_setting_value( 'contact_page_url' ) );
		$tp->set_var( 'bemail', $gb->get_setting_value( 'email_booking' ) );
		$tp->set_var( 'year', date( 'Y' ) );

		$tp->set_var( 'css', $ac->attemp_actions( 'css' ) );
		$tp->set_var( 'jvc', $ac->attemp_actions( 'jvc' ) );
		$tp->set_var( 'jvs', $ac->attemp_actions( 'jvs' ) );

		$tp->set_var( 'meta_title', $ac->attemp_actions( 'meta_title' ) );
		$tp->set_var( 'meta_description', $ac->attemp_actions( 'meta_description' ) );

		$tp->Parse( 'mBlock', 'mainBlock', false );
	}

	$tp->pparse( 'Output', 'home' );
}

?>