const form = jQuery("#cart-payment");

paypal.Buttons({
    createOrder: function( data ) {
        //-- Sets up the transaction when a payment button is clicked
        return fetch( form.data('url'), {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                ajax_key      : 'paypal_create_order',
                paymentSource : data.paymentSource,
                lbooking_id   : form.data('bid')
            }),
        }).then( ( res ) => {
            return res.json();
        }).then( ( data ) => {
            return data.id;
        });
    },
    onApprove: function( data ) {
        //-- Finalize the transaction after payer approval
        return fetch( form.data('url'), {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                ajax_key : 'paypal_capture_order',
                order_id : data.orderID
            }),
        }).then( ( res ) => {
            return res.json();
        }).then( ( data ) => {
            if( data.result == 'success' )
            {
               window.location = data.redirect_url; 
            }
            else
            {
                show_alert( data.message );
            }
        });
    },
    onError: function(error) {
        show_alert( 'Something wrong with your payment!' );
    },
}).render("#paypal-button-container");

//-- Create the Card Fields Component and define callbacks
const cardField = paypal.CardFields({
    style: {
        'input': {
            'font-weight' : 'lighter',
            'font-size'   : '16px',
            'color'       : '#000',
            'padding'     : '10px'
        },
    },
    createOrder: function( data ) {
        return fetch( form.data('url'), {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                ajax_key      : 'paypal_create_order',
                paymentSource : data.paymentSource,
                lbooking_id   : form.data('bid')
            }),
        }).then( ( res ) => {
            return res.json();
        }).then( ( data ) => {
            return data.id;
        });
    },
    onApprove: function( data ) {
        return fetch( form.data('url'), {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                ajax_key : 'paypal_capture_order',
                order_id : data.orderID
            }),
        }).then( ( res ) => {
            return res.json();
        }).then( ( data ) => {
            if( data.result == 'success' )
            {
               window.location = data.redirect_url; 
            }
            else
            {
                show_alert( data.message );
            }
        });
    },
    onError: function( error ) {
        show_alert( 'Something wrong with your payment!' );
    },
});

//-- Render each field after checking for eligibility
if( cardField.isEligible() )
{
    const nameField   = cardField.NameField();
    const numberField = cardField.NumberField();
    const cvvField    = cardField.CVVField();
    const expiryField = cardField.ExpiryField();

    nameField.render("#card-name-field-container");
    numberField.render("#card-number-field-container");
    cvvField.render("#card-cvv-field-container");
    expiryField.render("#card-expiry-field-container");
  
    //-- Add click listener to submit button and call the submit function on the CardField component
    document.getElementById("multi-card-field-button").disabled = false;
    document.getElementById("multi-card-field-button").addEventListener("click", () => {
        cardField.submit().then(() => {
            // submit successful
        });
    });
}