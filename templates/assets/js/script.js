function init_mobile_menu()
{
	var menu = new Mmenu( '#mmenu',{
		slidingSubmenus : false,
		theme: 'white',
		navbar: {
			add: false
		},
		navbars: [{
			position: "bottom",
			content: [ jQuery('.cloned').html()	]
        }]
	});

    jQuery('#mmenu .close').on('click', function( ev ){
		menu.API.close();
    });
}

function init_map()
{
    var mapblock   = document.getElementById('map-canvas');
    var coordinate = mapblock.getAttribute('data-coordinate');
    
    if( coordinate != '' )
    {
    	var arr = coordinate.split( ',' );
	    var lat = parseFloat( arr[ 0 ] );
		var lng = parseFloat( arr[ 1 ] );

	    var map = new google.maps.Map( mapblock, {
			streetViewControl: false,
			fullscreenControl: false,
			mapTypeControl: false,
			rotateControl: false,
			scaleControl: false,
			zoomControl: true,
			zoom: 10,
			center:{
				lat: lat, 
				lng: lng
			}
		});

		var marker = new google.maps.Marker({
			position: {
				lat: lat, 
				lng: lng
			},
			map: map
		});
	}
}

function init_map_advance()
{
    if( jQuery('.amaps').length > 0 )
    {
        var mapblock   = document.getElementById('amaps');
	    var coordinate = mapblock.getAttribute('data-coordinate');

	    if( coordinate != '' )
	    {
	    	var arr = coordinate.split( ',' );
		    var lat = parseFloat( arr[ 0 ] );
			var lng = parseFloat( arr[ 1 ] );

	        var map = new google.maps.Map( mapblock, {
				center: new google.maps.LatLng( lat, lng ),
	            streetViewControl: false,
	            fullscreenControl: false,
	            mapTypeControl: false,
	            rotateControl: false,
	            scaleControl: false,
	            scrollwheel: false,
	            zoomControl: true,
	            zoom: 12
	        });

	        init_markers( map );
	    }
    }

	if( jQuery('.scrollbar-inner').length > 0 )
	{
		jQuery('.scrollbar-inner').niceScroll({
            cursorcolor:'#191515',
            cursorborderradius:0, 
            autohidemode: false,
            cursorborder:'none',
            cursorwidth:'10px'
        });
	}

	jQuery('.sort-by-surf-type').on('click', function(){
		tinysort('.mapoption > .e-mo', {
			attr: 'data-surf-type'
		});
	});

	jQuery('.sort-by-certified').on('click', function(){
		tinysort('.mapoption > .e-mo', {
			attr: 'data-certified'
		});
	});

	jQuery('.sort-by-price').on('click', function(){
		tinysort('.mapoption > .e-mo', {
			attr: 'data-price'
		});
	});

	if( jQuery('.smaps').length > 0 )
    {
        var mapblock   = document.getElementById('smaps');
	    var coordinate = mapblock.getAttribute('data-coordinate');

	    if( coordinate != '' )
	    {
	    	var arr = coordinate.split( ',' );
		    var lat = parseFloat( arr[ 0 ] );
			var lng = parseFloat( arr[ 1 ] );

	        var map = new google.maps.Map( mapblock, {
				center: new google.maps.LatLng( lat, lng ),
				mapTypeId: 'satellite',
	            streetViewControl: false,
	            fullscreenControl: false,
	            mapTypeControl: false,
	            rotateControl: false,
	            scaleControl: false,
	            scrollwheel: false,
	            zoomControl: true,
	            zoom: 12
	        });

	        init_spot_markers( map );
	    }
    }

	jQuery('.filter select').on('change', function(){
		tinysort('.spot-item', {
			attr: 'data-' + jQuery(this).val()
		});
	});
}

function init_markers( map )
{
    var spointer = jQuery('.amaps').data('pin-active');
    var pointer  = jQuery('.amaps').data('pin');

    var bounds   = new google.maps.LatLngBounds();
	var markers  = new Array();

	jQuery('.mapoption .e-mo').each( function( i, e ){
        var sel  = jQuery(this);
        var cor  = sel.data('coordinate');
        var ttl  = sel.data('title');

	    if( cor != '' )
	    {
	    	var arr = cor.split( ',' );
		    var lat = parseFloat( arr[ 0 ] );
			var lng = parseFloat( arr[ 1 ] );

			var marker = new google.maps.Marker({    
                position: new google.maps.LatLng( lat, lng ),
                map: map,
                icon: {
                    url: pointer,
                    defurl: pointer,
                    acturl: spointer,
                    scaledSize: new google.maps.Size(26, 39)
                }
            });

            markers.push( marker );

            bounds.extend( marker.position );

            sel.on('click', function(el){
            	if( jQuery(el.target).closest('a').length == 0 )
            	{
            		if( isNaN( lat ) || isNaN( lng ) )
	            	{
	            		show_alert( 'This accommodation not registered on map list' );
	            	}
	                else
	                {
	                	map.panTo( new google.maps.LatLng( lat, lng ) );

		                sel.parent().find('.e-mo').removeClass('actv');

		                sel.addClass('actv');

		                set_marker( i, markers );
		            }

	                return false;
	            }
            });

            google.maps.event.addListener( marker, 'click', function( el ){
            	var scr = jQuery('.scrollbar-inner');

                scr.getNiceScroll(0).doScrollTop( sel.position().top );   

                map.panTo( new google.maps.LatLng( lat, lng ) );

                sel.parent().find('.e-mo').removeClass('actv');

                sel.addClass('actv'); 

                set_marker( i, markers );  
            });
	    }
    });

    map.fitBounds( bounds );
}

function init_spot_markers( map )
{
    var spointer = jQuery('.smaps').data('pin-active');
    var pointer  = jQuery('.smaps').data('pin');

    var bounds   = new google.maps.LatLngBounds();
	var markers  = new Array();

	jQuery('.spot-item').each( function( i, e ){
        var sel  = jQuery(this);
        var cor  = sel.data('coordinate');
        var ttl  = sel.data('title');

	    if( cor != '' )
	    {
	    	var arr = cor.split( ',' );
		    var lat = parseFloat( arr[ 0 ] );
			var lng = parseFloat( arr[ 1 ] );

			var marker = new google.maps.Marker({    
                position: new google.maps.LatLng( lat, lng ),
                map: map,
                icon: {
                    url: pointer,
                    defurl: pointer,
                    acturl: spointer,
                    scaledSize: new google.maps.Size(26, 39)
                }
            });

            markers.push( marker );

            bounds.extend( marker.position );

            google.maps.event.addListener( marker, 'click', function( el ){
                map.panTo( new google.maps.LatLng( lat, lng ) );

                sel.parent().find('.spot-item').removeClass('actv');

                sel.addClass('actv'); 

                set_marker( i, markers );  
            });
	    }
    });

    map.fitBounds( bounds );
}

/* 
| -----------------------------------------------------------------------------
| Set Marker Icon on Map
| -----------------------------------------------------------------------------
*/
function set_marker( idx, markers )
{
    unset_marker( markers );

    var pointer  = jQuery('.amaps').data('pin');
    var spointer = jQuery('.amaps').data('pin-active');
    
    markers[ idx ].setIcon({
        url: spointer,
        defurl: pointer,
        acturl: spointer,
        scaledSize: new google.maps.Size(26, 39)
    });

   	markers[ idx ].setZIndex( 1 );
}

/* 
| -----------------------------------------------------------------------------
| Unset Marker Icon on Map
| -----------------------------------------------------------------------------
*/
function unset_marker( markers )
{
    for( var i = 0; i < markers.length; i++)
    {
        markers[ i ].setIcon({
            url: markers[i].icon.defurl,
            defurl: markers[i].icon.defurl,
            acturl: markers[i].icon.acturl,
            scaledSize: markers[i].icon.scaledSize
        });

        markers[ i ].setZIndex( 0 );
    }
}

function init_scroll_top()
{
	jQuery('.gotop').on('click', function(){
		window.scrollTo({ top: 0, behavior: 'smooth' });
	})
}

function init_header_form()
{
	jQuery('.thedate').daterangepicker({
		autoUpdateInput: false,
		minDate: moment(),
		autoApply: true,
		drops: 'down',
		locale: {
		    cancelLabel: 'Clear',
		    monthNames: [ 
		    	'January',
		    	'February',
		    	'March',
		    	'April',
		    	'May',
		    	'June',
		    	'July',
		    	'August',
		    	'September',
		    	'October',
		    	'November',
		    	'December'
		    ],
		    daysOfWeek: [ 
		    	'Sun',
		    	'Mon',
		    	'Tue',
		    	'Wed',
		    	'Thu',
		    	'Fry',
		    	'Sat'
		    ],
		}
	}).on('apply.daterangepicker', function (ev, picker) {
		jQuery('.cin__').text( picker.startDate.format('DD MMM YYYY') );
		jQuery('.cou__').text( picker.endDate.format('DD MMM YYYY') );

		jQuery('.headform [name="checkin"]').val( picker.startDate.format('YYYY-MM-DD') );
		jQuery('.headform [name="checkout"]').val( picker.endDate.format('YYYY-MM-DD') );
	});

	jQuery('.thd_, .change-date').click(function(){
		jQuery('.thedate').trigger('click');
	});

	jQuery('.ifd:not(.exc)').click(function(e){
		var sel = jQuery(this).find('.drd_');

		jQuery('.drd_').not( sel ).removeClass('actv');

		jQuery(this).find('.drd_').toggleClass('actv');

		e.stopPropagation();
	});

	jQuery('.drd_:not(.ddp_) li:not(.dis_)').click(function(e){
		var dt = jQuery(this).data();

		jQuery(this).parents('.ifd').find('.hf_').text( dt.name );

		jQuery('[name=' + dt.type + ']').val( dt.id );

		jQuery('.ifd ul').removeClass('actv');
		
		e.stopPropagation();
	});

	jQuery('.ddp_ li:not(.dis_)').click(function(e){
		e.stopPropagation();
	});

	jQuery('html, body').click(function(e){
		jQuery('.ifd ul').removeClass('actv');
	});

	jQuery('.ppp, .mmm').click(function(){
		var th = jQuery(this);
		var ev = th.parent().find('p').text();
		var dt = th.data();
		var tt = 0;
		var mn = 0;

		if( dt.type == 'adult' )
		{
			mn = 1;
		}
		
		ev = parseInt( ev );

		if( th.hasClass('ppp') )
		{
			ev++;
		}
		else if( th.hasClass('mmm') )
		{
			if( ev != mn )
			{
				ev--;
			}
		}

		th.parent().find('p').text( ev );

		jQuery('.plmi p').each(function(){
			var txt = jQuery(this).text();

			tt += parseInt( txt );
		});
		
		th.parents('.ifd').find('.hf_').text( tt );

		jQuery('.headform [name="' + dt.type + '"]').val( ev );
	});

	// jQuery('.m-src,.mheadform,.ovlysd').click(function(){
	// 	jQuery('.c-ovl').toggleClass('actv');
	// })

	if( jQuery('.popup-search select').length > 0 )
	{
		jQuery('.popup-search select').select2({
			dropdownCssClass: 'select2-results-search',
			dropdownParent: jQuery('.popup-search')
		});

		jQuery('.popup-search [name=dest]').on('change.select2', function(){
			var txt = jQuery(this).find('option:selected').text();
			jQuery('[data-trigger="search"] span').text( txt );
		});
	}

	if( jQuery('.popup-search .field-date input').length > 0 )
	{
		jQuery('.popup-search .field-date input').daterangepicker({
			parentEl: '.popup-search',
			autoUpdateInput: false,
			minDate: moment(),
			autoApply: true,
			drops: 'down',
			locale: {
			    cancelLabel: 'Clear',
			    monthNames: [ 
			    	'January',
			    	'February',
			    	'March',
			    	'April',
			    	'May',
			    	'June',
			    	'July',
			    	'August',
			    	'September',
			    	'October',
			    	'November',
			    	'December'
			    ],
			    daysOfWeek: [ 
			    	'Sun',
			    	'Mon',
			    	'Tue',
			    	'Wed',
			    	'Thu',
			    	'Fry',
			    	'Sat'
			    ],
			}
		}).on('apply.daterangepicker', function (ev, picker) {
			jQuery('.field-date [name="checkin"]').val( picker.startDate.format('DD MMM YYYY') );
			jQuery('.field-date [name="checkout"]').val( picker.endDate.format('DD MMM YYYY') );
		});
	}

	if( jQuery('.popup-search .number-field').length > 0 )
	{
		jQuery('.popup-search .number-field').each(function() {
	        var spinner = jQuery(this),
	            input   = spinner.find('input[type="number"]'),
	            btnUp   = spinner.find('.up'),
	            btnDown = spinner.find('.down');

	        btnUp.click(function(){
	            if( !input.prop('disabled') )
	            {
	                var oldValue = parseFloat( input.val() );
	                var min      = input.attr('min');
	                var max      = input.attr('max');

	                if( oldValue >= max )
	                {
	                    var newVal = oldValue;
	                }
	                else
	                {
	                    var newVal = oldValue + 1;
	                }

	                spinner.find('input').val(newVal);
	                spinner.find('input').trigger('change');
	            }
	        });

	        btnDown.click(function() {
	            if( !input.prop('disabled') )
	            {
	                var oldValue = parseFloat( input.val() );
	                var min      = input.attr('min');
	                var max      = input.attr('max');

	                if( oldValue <= min )
	                {
	                  var newVal = oldValue;
	                }
	                else
	                {
	                  var newVal = oldValue - 1;
	                }

	                spinner.find('input').val(newVal);
	                spinner.find('input').trigger('change');
	            }
	        });
	    });
	}
}

function init_term_accordion()
{
	jQuery('.term-anchor').on('click', function(){
    	jQuery(this).parent().find('.term-body').toggleClass('term-body-opened');
    });
}

function init_lazy_load( prm )
{
	var bLazy = new Blazy( prm );

	return bLazy;
}

function init_home_slide()
{
	if( jQuery('.homeslide').length > 0 )
	{
		var hsLazy = init_lazy_load({ 
			container: '.homeslide',
			selector: '.lazy-load',
			loadInvisible: true
		});
		
		jQuery('.homeslide').slick({
            slidesToScroll: 1,
            slidesToShow: 1,
            slide: '.item',
            infinite: true,
            dots: false
        });
	}

	if( jQuery('.home-slider').length > 0 )
	{
		var hLazy = init_lazy_load({ 
			container: '.home-slider',
			selector: '.lazy-load',
			loadInvisible: true
		});

	    jQuery('.home-slider').each(function() {
	    	var sel = jQuery(this)
	        var num = 3;
	        var opt = new Array;

	        if( sel.hasClass('rvw__') )
	        {
	            opt.push({
					breakpoint: 769,
					settings: {
						centerPadding: '35px',
						focusOnSelect: true,
						centerMode: true,
						slidesToShow: 1
					}
				});
	        }
        	else
        	{
        		opt.push({
					breakpoint: 769,
					settings: {
						centerPadding: '35px',
						focusOnSelect: true,
						centerMode: true,
	        			slidesToShow: 1,
						infinite: true,
						arrows: false,
						dots: false
					}
				});
			}

	        if( sel.hasClass('nt5') )
	        {
	            num = 5;

	            opt.push({
					breakpoint: 1025,
					settings: {
						slidesToShow: 3,
					}
				});
	        }
	        else if( sel.hasClass('nt7') )
	        {
	            num = 7;

	            opt.push({
					breakpoint: 1025,
					settings: {
						slidesToShow: 5,
					}
				});
	        }

	        var next = sel.find('.arrow.n__');
	        var prev = sel.find('.arrow.l__');

	        sel.slick({
                slidesToShow: num,
                slidesToScroll: 1,
                slide: '.e-card',
                nextArrow: next,
                prevArrow: prev,
                responsive: opt,
                infinite: true,
                dots: false,
            });
	    }).on('afterChange', function( event, slick, direction ){
		  	hLazy.revalidate();
		});	
	}
}

function init_accommodation_slide()
{
	if( jQuery('.veneu-header-slider').length > 0 )
	{
		var vLazy = init_lazy_load({ 
			container: '.veneu-header-image',
			selector: '.lazy-load',
			loadInvisible: true
		});

    	jQuery('.veneu-header-thumb').slick({
			cssEase: 'ease-in',
			focusOnSelect: true,
			variableWidth: true,
		  	slidesToScroll: 1,
		  	slidesPerRow: 4,
			infinite: true,
		  	arrows: false,
 			rows: 4,
			responsive: [{
				breakpoint: 1025,
				settings: {
					variableWidth: true,
					centerMode: true,
		  			slidesToShow: 5,
		  			slidesPerRow: 1,
					infinite: true,
		  			rows: 1
				}
			}]
	    }).on('afterChange', function( event, slick, currentSlide ){
		  	vLazy.revalidate();
		});

        var prev = jQuery('.veneu-header-container-slider .vhs-left');
        var next = jQuery('.veneu-header-container-slider .vhs-right');
		
		jQuery('.veneu-header-slider').on('init', function( event, slick ){
			jQuery('.vh-thumb').on('click', function(){
				var sel = jQuery(this);
				var id  = jQuery(this).data('id');
				var idx = jQuery('.veneu-header-box[data-id=' + id + ']').parents('.slick-slide').data('slick-index');

			  	jQuery('.vh-thumb').removeClass('selected');

			  	sel.addClass('selected');

				slick.slickGoTo( idx );
			});
		});

 		jQuery('.veneu-header-slider').slick({
 			adaptiveHeight: true,
			cssEase: 'ease-in',
		  	slidesToScroll: 1,
		  	slidesToShow: 1,
		  	nextArrow: next,
	  		prevArrow: prev,
			infinite: true,
		  	fade: true,
	    }).on('afterChange', function( event, slick, currentSlide ){
		  	vLazy.revalidate();

		  	var id  = jQuery(slick.$slides.get(currentSlide)).find('.veneu-header-box').data('id');
		  	var now = jQuery('.veneu-header-thumb').slick('slickCurrentSlide');
		  	var idx = jQuery('.vh-thumb[data-id="' + id + '"]').parent().parent().data('slick-index');

		  	if( now != idx )
		  	{
		  		jQuery('.veneu-header-thumb').slick('slickGoTo', idx);
		  	}

		  	jQuery('.vh-thumb').removeClass('selected');
		  	jQuery('.vh-thumb[data-id="' + id + '"]').addClass('selected');
		});
  	}

  	if( jQuery('.surf-by-region-slider').length > 0 )
  	{
  		var rLazy = init_lazy_load({ 
			container: '.surf-by-region-slider',
			selector: '.lazy-load',
			loadInvisible: true
		});

	    jQuery('.surf-by-region-slider').each(function() {
	    	var sel  = jQuery(this)
	        var next = sel.parent().find('.arrow.n__');
	        var prev = sel.parent().find('.arrow.l__');
	        var nums = sel.find('.e-card').length;

	        if( nums > 3 )
	        {
	        	nums = 3;
	        }

	        sel.slick({
                slidesToShow: nums,
                slidesToScroll: 1,
                nextArrow: next,
                prevArrow: prev,
                infinite: true,
                dots: false,
                responsive: [
	                {
						breakpoint: 769,
						settings: {
                			slidesToShow: 1,
							centerMode: true
						}
					},
	                {
						breakpoint: 576,
						settings: {
                			slidesToShow: 1,
							centerMode: false,
							// variableWidth: true
						}
					}
				]
            });
	    }).on('afterChange', function( event, slick, direction ){
		  	rLazy.revalidate();
		});	
  	}
}

function init_staff_slide()
{
   	if( jQuery('.staff-slider').length > 0 )
   	{
		var sLazy = init_lazy_load({ 
			container: '.staff-slider',
			selector: '.lazy-load',
			loadInvisible: true
		});

    	jQuery('.staff-slider').each(function(){
    		var sel   = jQuery(this);
	        var prev  = sel.parent('.obig-staff').find('.ss-left');
	        var next  = sel.parent('.obig-staff').find('.ss-right');
	        var dnums = sel.find('.staff-slider-content').length;
	        var mnums = dnums;

	        if( dnums > 2 )
	        {
	        	dnums = 2;
	        }

	        if( mnums > 3 )
	        {
	        	mnums = 3;
	        }

     		sel.slick({
			  	slidesToShow: dnums,
			  	slidesToScroll: 1,
			  	nextArrow: next,
		  		prevArrow: prev,
				infinite: true,
                responsive: [
	                {
						breakpoint: 769,
						settings: {
			  				slidesToShow: mnums,
							infinite: false
						}
					},
	                {
						breakpoint: 576,
						settings: {
							adaptiveHeight: true,
			  				slidesToShow: 1,
							infinite: true
						}
					}
				]
		    }).on('afterChange', function( event, slick, direction ){
			  	sLazy.revalidate();
			});	
    	});
  	}
}

function init_related_post_slide()
{
   	if( jQuery('.trending-slider').length > 0 )
   	{
		var sLazy = init_lazy_load({ 
			container: '.trending-slider',
			selector: '.lazy-load',
			loadInvisible: true
		});

    	jQuery('.trending-slider').slick({
		  	slidesToScroll: 1,
		  	slidesToShow: 3,
		  	arrows: true,
            responsive: [
                {
					breakpoint: 769,
					settings: {
		  				slidesToShow: 2
					}
				},
                {
					breakpoint: 551,
					settings: {
		  				slidesToShow: 1
					}
				}
			]
	    }).on('afterChange', function( event, slick, direction ){
		  	sLazy.revalidate();
		});	
	}	
}

function init_blog_slide()
{
   	if( jQuery('.blog-slider').length > 0 )
   	{
		var sLazy = init_lazy_load({ 
			container: '.blog-slider',
			selector: '.lazy-load',
			loadInvisible: true
		});

    	jQuery('.blog-slider').each(function(){
    		var sel  = jQuery(this);
	        var next = sel.find('.arrow.n__');
	        var prev = sel.find('.arrow.l__');

     		sel.slick({
			  	slidesToScroll: 1,
                slide: '.e-blog',
			  	slidesToShow: 4,
			  	nextArrow: next,
		  		prevArrow: prev,
				infinite: true,
                responsive: [
	                {
						breakpoint: 1441,
						settings: {
			  				slidesToShow: 3
						}
					}
				]
		    }).on('afterChange', function( event, slick, direction ){
			  	sLazy.revalidate();
			});	
    	});
  	}
}

function init_tab_slide()
{
	if( jQuery('.fdo-slide').length > 0 )
	{		
		jQuery('.fdo-slide').on('init', function( event, slick ){
		    jQuery('.nav-fdo h2').on('click', function(){
		    	var to = jQuery(this).attr('ds');
		    	
		        jQuery('.fdo-slide').slick( 'slickGoTo', to );
		        jQuery('.nav-fdo h2').removeClass('active');
		        jQuery(this).addClass('active');
		    });
		});

	    jQuery('.fdo-slide').slick({
            adaptiveHeight: true,
            slidesToScroll: 1,
            cssEase: 'linear',
            draggable: false,
            slidesToShow: 1,
            arrows: false,
            dots: false,
            speed: 500,
            fade: true
        });
	}
}

function init_comment_slider()
{
  	if( jQuery('.comment-slider').length > 0 )
  	{
  		var rLazy = init_lazy_load({ 
			container: '.comment-slider',
			selector: '.lazy-load',
			loadInvisible: true
		});

	    jQuery('.comment-slider').each(function() {
	    	var sel  = jQuery(this)
	        var next = sel.parent().find('.arrow.n__');
	        var prev = sel.parent().find('.arrow.l__');

	        sel.slick({
                slide: '.e-comment',
                slidesToScroll: 1,
                slidesToShow: 1,
                nextArrow: next,
                prevArrow: prev,
                infinite: true,
                dots: false
            });
	    }).on('afterChange', function( event, slick, direction ){
		  	rLazy.revalidate();
		});	
  	}
}

function init_accommodation_form()
{
	daterangepicker.prototype.setMinDate = function( minDate ){
	    if (typeof minDate === 'string')
	        this.minDate = moment(minDate, this.locale.format);

	    if (typeof minDate === 'object')
	        this.minDate = moment(minDate);

	    if (!this.timePicker)
	        this.minDate = this.minDate.startOf('day');

	    if (this.timePicker && this.timePickerIncrement)
	        this.minDate.minute(Math.round(this.minDate.minute() / this.timePickerIncrement) * this.timePickerIncrement);

	    if (this.minDate && this.startDate.isBefore(this.minDate)) {
	        this.startDate = this.minDate.clone();
	        if (this.timePicker && this.timePickerIncrement)
	            this.startDate.minute(Math.round(this.startDate.minute() / this.timePickerIncrement) * this.timePickerIncrement);
	    }

	    if (this.startDate < this.minDate)
	        this.setStartDate(this.minDate);

	    if (!this.isShowing)
	        this.updateElement();

	    this.updateMonthsInView();
	}

	if( jQuery('.select-room').length > 0 )
	{
		jQuery('.select-room').select2({
			dropdownCssClass: 'select2-results-room',
			minimumResultsForSearch: 15,
		});
	}

	if( jQuery('.select-cabin').length > 0 )
	{
		jQuery('.select-cabin').select2({
			dropdownCssClass: 'select2-results-cabin',
			minimumResultsForSearch: 15,
		});
	}

	if( jQuery('.select-guest').length > 0 )
	{
		jQuery('.select-guest').select2({
			dropdownCssClass: 'select2-results-guest',
			minimumResultsForSearch: 15,
		});

		jQuery('.select-guest').on('change', function(){
			var frm = jQuery(this).parents('form');
			var num = jQuery(this).val();

			frm.find('[name=adult]').val( num );
		});
	}

	if( jQuery('.package-item').length > 0 )
	{
		jQuery('.package-item').each( function( i, ele ){
			var sel = jQuery(this);
	        var utl = sel.find('[name=checkout]');
	        var stt = sel.find('[name=checkin]');
	        var day = sel.find('[name=night]');

	        jQuery( sel ).find('.checkin').daterangepicker( {
                parentEl: jQuery( sel ).find('.checkin').parent(),
                autoUpdateInput: false,
                minDate: moment(),
                autoApply: true,
                locale:{
                    format: 'DD/MM/YYYY',
                },
            	maxSpan:{
            		days: day.val()
            	}
            }, function( start, end ) {
	            stt.val( start.format( 'YYYY-MM-DD' ) );
	            utl.val( end.format( 'YYYY-MM-DD' ) );
	        }).on('apply.daterangepicker', function( ev, picker ) {
	            var start = picker.startDate.format( 'DD/MM/YYYY' );
	            var end   = picker.endDate.format( 'DD/MM/YYYY' );

	            jQuery( this ).text( start );
	            jQuery( sel ).find('.checkout').text( end );
	        });

	        jQuery( sel ).find('.checkout').on('click', function(){
	        	jQuery( sel ).find('.checkin').data('daterangepicker').show();
	        });
		});
	}

	jQuery('#check-in').daterangepicker({
		parentEl: '.accommodation-form .osmall-container',
		singleDatePicker: true,
		autoUpdateInput: false,
		minDate: moment(),
		autoApply: true,
		drops: 'down',
		locale: {
		    cancelLabel: 'Clear',
		    monthNames: [ 
		    	'January',
		    	'February',
		    	'March',
		    	'April',
		    	'May',
		    	'June',
		    	'July',
		    	'August',
		    	'September',
		    	'October',
		    	'November',
		    	'December'
		    ],
		}
	}).on('apply.daterangepicker', function (ev, picker) {
		jQuery('.check-in-label').text( picker.startDate.format('DD MMM YYYY') );
		jQuery('.accommodation-form [name="checkin"]').val( picker.startDate.format('YYYY-MM-DD') );

		var checkin  = moment( picker.startDate );
		var checkout = moment( jQuery('#check-out').data('daterangepicker').startDate );

		jQuery('#check-out').data('daterangepicker').setMinDate( picker.startDate );

		if( checkout.diff( checkin ) < 0 )
		{
			jQuery('.check-out-label').text( picker.startDate.format('DD MMM YYYY') );
			jQuery('#check-out').data('daterangepicker').setStartDate( picker.startDate );
			jQuery('.accommodation-form [name="checkout"]').val( picker.startDate.format('YYYY-MM-DD') );
		}
	});

	jQuery('#check-out').daterangepicker({
		parentEl: '.accommodation-form .osmall-container',
		singleDatePicker: true,
		autoUpdateInput: false,
		minDate: moment(),
		autoApply: true,
		drops: 'down',
		locale: {
		    cancelLabel: 'Clear',
		    monthNames: [ 
		    	'January',
		    	'February',
		    	'March',
		    	'April',
		    	'May',
		    	'June',
		    	'July',
		    	'August',
		    	'September',
		    	'October',
		    	'November',
		    	'December'
		    ],
		}
	}).on('apply.daterangepicker', function (ev, picker) {
		jQuery('.check-out-label').text( picker.startDate.format('DD MMM YYYY') );
		jQuery('.accommodation-form [name="checkout"]').val( picker.startDate.format('YYYY-MM-DD') );
	});

	jQuery('#guest').on('click', function(e){
		jQuery(this).find('.ddp_').toggleClass('actv');

		e.stopPropagation();
	});

	jQuery('#guest .ppp, #guest .mmm').click(function(){
		var th = jQuery(this);
		var ev = th.parent().find('p').text();
		var dt = th.data();
		var tt = 0;
		var mn = 0;

		if( dt.type == 'adult' )
		{
			mn = 1;
		}
		
		ev = parseInt( ev ) || 0;

		if( th.hasClass('ppp') )
		{
			ev++;
		}
		else if( th.hasClass('mmm') )
		{
			if( ev != mn )
			{
				ev--;
			}
		}

		th.parent().find('p').text( ev );

		jQuery('#guest .plmi p').each(function(){
			var txt = jQuery(this).text();

			tt += parseInt( txt );
		});
		
		th.parents('#guest').find('label').text( tt + ' GUEST' );

		jQuery('.accommodation-form [name="guest"]').val( tt );
		jQuery('.accommodation-form [name="' + dt.type + '"]').val( ev );
	});

	jQuery('#guest .ddp_ li').click(function(e){
		e.stopPropagation();
	});

	jQuery('.select-cabin, .select-room').on('change', function(){
		var room = jQuery(this).val();

		if( jQuery(this).parents('tr').find('[name=room_id]').length > 0 )
		{
			jQuery(this).parents('tr').find('[name=room_id]').val( room );
		}
	});

	jQuery('.vtabel-guest-num').on('click', function(e){
		jQuery('.vtabel-guest-select-list').removeClass('actv');
		jQuery(this).parent().find('.vtabel-guest-select-list').toggleClass('actv');

		e.stopPropagation();
	});

	jQuery('.vtabel-guest-select .ppp, .vtabel-guest-select .mmm').click(function(){
		var th = jQuery(this);
		var ev = parseInt( th.parent().find('p').text() ) || 0;
		var dt = th.data();
		var tt = 0;
		var mn = 0;

		if( dt.type == 'adult' )
		{
			mn = 1;
		}

		if( th.hasClass('ppp') )
		{
			ev++;
		}
		else if( th.hasClass('mmm') )
		{
			if( ev != mn )
			{
				ev--;
			}
		}

		th.parent().find('p').text( ev );

		th.parents('.vtabel-guest-select-list').find('p').each(function(){
			var txt = jQuery(this).text();

			tt += parseInt( txt );
		});
		
		th.parents('.vtabel-guest-select').find('.vtabel-guest-num').val( tt );
		th.parents('.vtabel-guest-select').find('[name="' + dt.type + '"]').val( ev );
	});

	jQuery('.vtabel-guest-select-list li').click(function(e){
		e.stopPropagation();
	});

	jQuery('#oveneu-filter').click(function(){
	  	jQuery(this).parents('.oveneu-filter').find('ul').toggleClass('show');	
	});

	jQuery('.oveneu-filter li').click(function(){
		var list_text = jQuery(this).text();

		jQuery(this).parents('.oveneu-filter').find('[name=trip_filter]').val( list_text );
		jQuery(this).parents('.oveneu-filter').find('label').text( list_text );
		jQuery(this).parent().removeClass('show');
	});

	jQuery('html, body').click(function(e){
		jQuery('.vtabel-guest-select-list').removeClass('actv');
		jQuery('#guest .ddp_').removeClass('actv');
	});

	jQuery('.select-trip').on('click', function(e){
		var guest    = jQuery(this).parents('tr').find('[name=guest]').val();
		var checkin  = jQuery(this).parents('tr').find('[name=checkin]').val();
		var checkout = jQuery(this).parents('tr').find('[name=checkout]').val();
		var night    = jQuery(this).parents('tr').find('[name=night]');

		var error = [];

		if( guest == 0 )
		{
			error.push( 'Please select guest first' );
		}

		if( checkin == '' || checkout == '' )
		{
			error.push( 'Please choose check in/out date first' );
		}

		if( error.length > 0 )
		{
			show_alert( error.join('<br/>') );

			e.preventDefault();
		}
		else
		{
			var start = moment( checkin );
			var end   = moment( checkout );
			var min   = parseInt( night.val() );
			var num   = end.diff( start, 'days' );

			if( num < min )
			{
				show_alert( 'Minimum nights for this package is "' + min + ' nights", please adjust your dates before selecting the trip' );

				e.preventDefault();
			}
			else
			{
				jQuery(this).parents('tr').find('form button').trigger('click');
			}
		}
	});

	jQuery('[name=add_to_cart]').on('click', function(e){
		var guest    = jQuery('.accommodation-form [name=guest]').val();
		var checkin  = jQuery('.accommodation-form [name=checkin]').val();
		var checkout = jQuery('.accommodation-form [name=checkout]').val();

		var error = new Array;
		var night = new Array;

		if( guest == 0 )
		{
			error.push( 'Please fill your guest number' );
		}

		if( checkin == '' )
		{
			error.push( 'Please fill your check-in date' );
		}

		if( checkout == '' )
		{
			error.push( 'Please fill your check-out date' );
		}

		if( jQuery('.select-room').length > 0 )
		{
			var num   = 0;

			jQuery('.select-room').each(function(){
				var min_night = jQuery(this).parent().find('[name=room_min_night]').val();

				if( jQuery(this).val() > 0 )
				{
					night.push( min_night );

					num++;
				}
			});

			if( num == 0 )
			{
				error.push( 'Please select room first' );
			}
		}
		else
		{
			var min_night = jQuery('.accommodation-form [name=min_night]').val();

			night.push( min_night );
		}

		if( error.length > 0 )
		{
			show_alert( error.join( '<br/>' ) );

			e.preventDefault();
		}
		else
		{
			if( night.length > 0 )
			{
				var cin  = moment( checkin );
				var cout = moment( checkout );
				var diff = cout.diff( cin, 'days' );
				var min  = Math.max.apply( Math, night );

				if( min > 0 && diff < min )
				{
					show_alert( 'Sorry, the number of days on the date you selected is less than "' + min + ' nights", please adjust your date again' );

					e.preventDefault();
				}
			}
		}
	});
}

function show_alert( message )
{
	jQuery.fancybox.close();
	jQuery.fancybox.open( '<div class="popup-alert"><div class="alert">' + message + '</div></div>' );
}

function init_booking_payment()
{
	if( jQuery('#stripe-payment-element').length > 0 )
	{
		var pkey   = jQuery('#stripe-payment-element').data('public-key');
		var form   = jQuery('#cart-payment');
		var stripe = Stripe(pkey);
		
		jQuery.ajax({
            url: form.data('url'),
            data: {
            	ajax_key    : 'payment_intent',
            	lbooking_id : form.data('bid')
            },
            dataType : 'json',
            method: 'POST',
            beforeSend: function( xhr ){
            	form.find('[type=submit]').prop('disabled', true).text('WAIT');
            },
            success: function( e ){
            	if( typeof e.clientSecret != 'undefined' )
            	{
            		eval( 'init_stripe( form, stripe, e.clientSecret )' );
            	}
            	else
            	{
					show_alert( e.error );
            	}
            },
            complete: function( e ){
            	form.find('[type=submit]').prop('disabled', false).text('PROCEED PAYMENT');
            }
        });
	}
}

function init_stripe( form, stripe, clientSecret )
{
	var appearance = {
		theme: 'stripe',
		rules: {
			'.Label': {
				marginBottom: '10px'
			}
		}
	};

	var elements = stripe.elements({ clientSecret, appearance, locale: 'us' });

	var style = {
	    base: {
	        iconColor: '#666EE8',
	        color: '#000000',
	        fontWeight: 300,
	        fontFamily: 'Rubik, sans-serif',
	        fontSize: '16px',
	        '::placeholder': {
	            color: '#444444',
	        },
	    },
	};

  	elements.create('payment', { 
  		style: style  		
  	}).mount('#stripe-payment-element');

	form.on( 'submit', function( e ){
		e.preventDefault();

		stripe.confirmPayment({ 
			elements,
			confirmParams: {
				return_url: form.data('callback'),
			}
		}).then(function(result) {
		  	console.log(result);
		});
	});
}

function init_select2()
{
	if( jQuery('.select-opt').length > 0 )
	{
		jQuery('.select-opt').select2({
			minimumResultsForSearch: 15,
		});
	}
}

function init_anchor()
{
	if( jQuery('[data-anchor]').length > 0 )
	{
		jQuery('[data-anchor]').on('click', function(){
			var anchor = jQuery(this).data('anchor');

			if( jQuery(anchor).length > 0 )
			{
				var top = parseInt( jQuery('#mainav').outerHeight() ) || 0;
				var sub = parseInt( jQuery('.subheader').outerHeight() ) || 0;
				var fly = parseInt( jQuery('.veneu-header-menu-wrapp').outerHeight() ) || 0;

				jQuery('html,body').animate({
					scrollTop: jQuery(anchor).offset().top - ( top + sub + fly )
				},'slow');
			}
		});
	}
}

function init_booking_form()
{
	if( jQuery('.select-addons').length > 0 )
	{
		jQuery('.select-addons').select2({
			dropdownCssClass: 'select2-results-addons',
			minimumResultsForSearch: 15,
		});
	}

	jQuery('.addons-checkbox').on('click', function( e ){
		var prn = jQuery(this).parents('.e-dtc');
		var num = prn.find('.select-addons').val();

		if( this.checked === true )
		{
			if( num == 0 )
			{
				show_alert( 'Please select number of add-ons first' );

				jQuery(this).prop('checked', false);

				prn.find('.addons-overlay').removeClass('addons-overlay-selected');
			}
			else
			{
				prn.find('.addons-overlay').addClass('addons-overlay-selected');
			}
		}
		else
		{
			prn.find('.addons-overlay').removeClass('addons-overlay-selected');
		}
	});

	if( jQuery('#cart-guest').length > 0 )
	{
		jQuery('#cart-guest').validate({
			errorPlacement: function(error, element){
				return null
			},
			onfocusout: function( element ){
				if( jQuery(element).hasClass('phone') )
				{
					jQuery(element).rules('add', {
						pattern: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/
					});
				}

		        this.element( element );
		    },
		    ignore: 'textarea'
		});
	}

	if( jQuery('#cart-verify').length > 0 )
	{
		jQuery('#cart-verify').validate({
			errorPlacement: function(error, element){
				return null
			},
			onfocusout: function( element ){
				jQuery(element).rules('add', {
					equalTo : '[name=current_email]'
				});

		        this.element( element );
		    },
		    ignore: 'textarea'
		});
	}
}

function init_contact()
{
	if( jQuery('[name=contact_form]').length > 0 )
	{
		jQuery('[name=contact_form]').validate({
			errorPlacement: function( error, element ){
				return null
			},
			onfocusout: function( element ){				
				if( jQuery(element).attr('name') == 'phone_number' )
				{
					jQuery(element).rules('add', {
						pattern: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/
					});
				}

				if( jQuery(element).attr('name') == 'confirm_email' )
				{
					jQuery(element).rules('add', {
						equalTo : '[name=email]'
					});
				}

		        this.element( element );
		    },
		    submitHandler: function( element ){
		    	var form = jQuery(element);

				jQuery.ajax({
		            url: form.attr('action'),
		            data: form.serializeArray(),
		            dataType : 'json',
		            method: 'POST',
		            beforeSend: function( xhr ){
		            	form.find('[type=submit]').prop('disabled', true).text('WAIT');
		            },
		            success: function( e ){
		            	if( e.result == 'success' )
		            	{
		            		window.location = form.data('redirect');
		            	}
		            	else
		            	{
		            		show_alert( e.message );
		            	}
		            },
		            error: function( e ){
		            },
		            complete: function( e ){
		            	form.find('[type=submit]').prop('disabled', false).text('SEND');
		            }
		        });

		        return false;
		    }
		})
	}
}

function init_register()
{
	if( jQuery('[name=register_form]').length > 0 )
	{
		jQuery('[name=register_form]').validate({
			errorPlacement: function( error, element ){
				return null
			},
			onfocusout: function( element ){				
				if( jQuery(element).attr('name') == 'phone_number' )
				{
					jQuery(element).rules('add', {
						pattern: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/
					});
				}

				if( jQuery(element).attr('name') == 'confirm_email' )
				{
					jQuery(element).rules('add', {
						equalTo : '[name=email]'
					});
				}

		        this.element( element );
		    },
		    submitHandler: function( element ){
		    	var form = jQuery(element);

				jQuery.ajax({
		            url: form.attr('action'),
		            data: form.serializeArray(),
		            dataType : 'json',
		            method: 'POST',
		            beforeSend: function( xhr ){
		            	form.find('[type=submit]').prop('disabled', true).text('WAIT');
		            },
		            success: function( e ){
		            	if( e.result == 'success' )
		            	{
		            		window.location = form.data('redirect');
		            	}
		            	else
		            	{
		            		show_alert( e.message );
		            	}
		            },
		            error: function( e ){
		            },
		            complete: function( e ){
		            	form.find('[type=submit]').prop('disabled', false).text('SEND');
		            }
		        });

		        return false;
		    }
		})
	}
}

function init_franchises()
{
	if( jQuery('[name=franchises_form]').length > 0 )
	{
		jQuery('[name=franchises_form]').validate({
			errorPlacement: function( error, element ){
				return null
			},
			onfocusout: function( element ){				
				if( jQuery(element).attr('name') == 'phone_number' )
				{
					jQuery(element).rules('add', {
						pattern: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/
					});
				}

				if( jQuery(element).attr('name') == 'confirm_email' )
				{
					jQuery(element).rules('add', {
						equalTo : '[name=email]'
					});
				}

		        this.element( element );
		    },
		    submitHandler: function( element ){
		    	var form = jQuery(element);

				jQuery.ajax({
		            url: form.attr('action'),
		            data: form.serializeArray(),
		            dataType : 'json',
		            method: 'POST',
		            beforeSend: function( xhr ){
		            	form.find('[type=submit]').prop('disabled', true).text('WAIT');
		            },
		            success: function( e ){
		            	console.log(e);
		            	if( e.result == 'success' )
		            	{
		            		window.location = form.data('redirect');
		            	}
		            	else
		            	{
		            		show_alert( e.message );
		            	}
		            },
		            error: function( e ){
		            },
		            complete: function( e ){
		            	form.find('[type=submit]').prop('disabled', false).text('SEND');
		            }
		        });

		        return false;
		    }
		})
	}
}

function init_digital()
{
	if( jQuery('[name=digital_form]').length > 0 )
	{
		jQuery('[name=digital_form]').validate({
			errorPlacement: function( error, element ){
				return null
			},
			onfocusout: function( element ){	
		        this.element( element );
		    },
		    submitHandler: function( element ){
		    	var form = jQuery(element);
		    	var data = new FormData( form[0] );
		    	
				jQuery.ajax({
		            url: form.attr('action'),
		            data: data,
		            dataType : 'json',
		            method: 'POST',
		            processData: false,
		            contentType: false,
		            beforeSend: function( xhr ){
		            	form.find('[type=submit]').prop('disabled', true).text('WAIT');
		            },
		            success: function( e ){
		            	console.log(e);
		            	if( e.result == 'success' )
		            	{
		            		window.location = form.data('redirect');
		            	}
		            	else
		            	{
		            		show_alert( e.message );
		            	}
		            },
		            error: function( e ){
		            },
		            complete: function( e ){
		            	form.find('[type=submit]').prop('disabled', false).text('SEND');
		            }
		        });

		        return false;
		    }
		})
	}
}

function init_popup()
{
	jQuery('[data-trigger="search"]').on('click', function(){
		jQuery.fancybox.close();
		jQuery.fancybox.open({
			src  : '[data-act="search"]',
			type : 'inline',
			opts : {
				afterShow : function( instance, slide ) {
					jQuery('.field-btn button').on('click', function(){
						instance.close();
					})
				}
			}
		});
	});

	jQuery('[data-trigger="inquiry-private-charter"]').on('click', function(){
		jQuery.fancybox.close();
		jQuery.fancybox.open({
			src  : '[data-act="inquiry-private-charter"]',
			type : 'inline',
			opts : {
				afterShow : function( instance, slide ) {
					var form = jQuery(slide.$slide).find('form');

					form.find('.datepicker-from').daterangepicker({
						singleDatePicker: true,
						autoUpdateInput: false,
						minDate: moment(),
						autoApply: true,
						parentEl: form,
						drops: 'down',
						locale: {
						    cancelLabel: 'Clear',
						    monthNames: [ 
						    	'January',
						    	'February',
						    	'March',
						    	'April',
						    	'May',
						    	'June',
						    	'July',
						    	'August',
						    	'September',
						    	'October',
						    	'November',
						    	'December'
						    ],
						}
					}).on('apply.daterangepicker', function (ev, picker) {
						form.find('.datepicker-from').val( picker.startDate.format('DD MMM YYYY') );
						form.find('[name="date_from"]').val( picker.startDate.format('YYYY-MM-DD') );

						var checkin  = moment( picker.startDate );
						var checkout = moment( form.find('.datepicker-to').data('daterangepicker').startDate );

						form.find('.datepicker-to').data('daterangepicker').setMinDate( picker.startDate );

						if( checkout.diff( checkin ) < 0 )
						{
							form.find('.datepicker-to').data('daterangepicker').setStartDate( picker.startDate );
							form.find('.datepicker-to').val( picker.startDate.format('DD MMM YYYY') );
							form.find('[name="date_to"]').val( picker.startDate.format('YYYY-MM-DD') );
						}
					});

					form.find('.datepicker-to').daterangepicker({
						singleDatePicker: true,
						autoUpdateInput: false,
						minDate: moment(),
						autoApply: true,
						parentEl: form,
						drops: 'down',
						locale: {
						    cancelLabel: 'Clear',
						    monthNames: [ 
						    	'January',
						    	'February',
						    	'March',
						    	'April',
						    	'May',
						    	'June',
						    	'July',
						    	'August',
						    	'September',
						    	'October',
						    	'November',
						    	'December'
						    ],
						}
					}).on('apply.daterangepicker', function (ev, picker) {
						form.find('.datepicker-to').val( picker.startDate.format('DD MMM YYYY') );
						form.find('[name="date_to"]').val( picker.startDate.format('YYYY-MM-DD') );
					});

					form.validate({
						errorPlacement: function(error, element){
							return null
						},
						onfocusout: function( element ){				
							if( jQuery(element).attr('name') == 'phone' )
							{
								jQuery(element).rules('add', {
									pattern: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/
								});
							}

					        this.element( element );
					    },
					    submitHandler: function( element ){
					    	var frm = jQuery(element);

							jQuery.ajax({
					            url: frm.attr('action'),
					            data: frm.serializeArray(),
					            dataType : 'json',
					            method: 'POST',
					            beforeSend: function( xhr ){
					            	frm.find('[type=submit]').prop('disabled', true).text( frm.find('[type=submit]').data('process-label') );
					            },
					            success: function( e ){
					            	if( e.result == 'success' )
					            	{
										show_alert( frm.find('.message-text-success').html() );
					            	}
					            	else
					            	{
					            		show_alert( e.message );
					            	}
					            },
					            error: function( e ){
					            },
					            complete: function( e ){
					            	frm.find('[type=submit]').prop('disabled', false).text( frm.find('[type=submit]').data('label') );
					            }
					        });

					        return false;
					    }
					});
				}
			}
		});
	});

	jQuery('[data-trigger="inquiry-contact"]').on('click', function(){
		jQuery.fancybox.close();
		jQuery.fancybox.open({
			src  : '[data-act="inquiry-contact"]',
			type : 'inline',
			opts : {
				afterShow : function( instance, slide ) {
					var form = jQuery(slide.$slide).find('form');

					form.find('.datepicker-from').daterangepicker({
						singleDatePicker: true,
						autoUpdateInput: false,
						minDate: moment(),
						autoApply: true,
						parentEl: form,
						drops: 'down',
						locale: {
						    cancelLabel: 'Clear',
						    monthNames: [ 
						    	'January',
						    	'February',
						    	'March',
						    	'April',
						    	'May',
						    	'June',
						    	'July',
						    	'August',
						    	'September',
						    	'October',
						    	'November',
						    	'December'
						    ],
						}
					}).on('apply.daterangepicker', function (ev, picker) {
						form.find('.datepicker-from').val( picker.startDate.format('DD MMM YYYY') );
						form.find('[name="date_from"]').val( picker.startDate.format('YYYY-MM-DD') );

						var checkin  = moment( picker.startDate );
						var checkout = moment( form.find('.datepicker-to').data('daterangepicker').startDate );

						form.find('.datepicker-to').data('daterangepicker').setMinDate( picker.startDate );

						if( checkout.diff( checkin ) < 0 )
						{
							form.find('.datepicker-to').data('daterangepicker').setStartDate( picker.startDate );
							form.find('.datepicker-to').val( picker.startDate.format('DD MMM YYYY') );
							form.find('[name="date_to"]').val( picker.startDate.format('YYYY-MM-DD') );
						}
					});

					form.find('.datepicker-to').daterangepicker({
						singleDatePicker: true,
						autoUpdateInput: false,
						minDate: moment(),
						autoApply: true,
						parentEl: form,
						drops: 'down',
						locale: {
						    cancelLabel: 'Clear',
						    monthNames: [ 
						    	'January',
						    	'February',
						    	'March',
						    	'April',
						    	'May',
						    	'June',
						    	'July',
						    	'August',
						    	'September',
						    	'October',
						    	'November',
						    	'December'
						    ],
						}
					}).on('apply.daterangepicker', function (ev, picker) {
						form.find('.datepicker-to').val( picker.startDate.format('DD MMM YYYY') );
						form.find('[name="date_to"]').val( picker.startDate.format('YYYY-MM-DD') );
					});

					form.validate({
						errorPlacement: function(error, element){
							return null
						},
						onfocusout: function( element ){				
							if( jQuery(element).attr('name') == 'phone' )
							{
								jQuery(element).rules('add', {
									pattern: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/
								});
							}

					        this.element( element );
					    },
					    submitHandler: function( element ){
					    	var frm = jQuery(element);

							jQuery.ajax({
					            url: frm.attr('action'),
					            data: frm.serializeArray(),
					            dataType : 'json',
					            method: 'POST',
					            beforeSend: function( xhr ){
					            	frm.find('[type=submit]').prop('disabled', true).text( frm.find('[type=submit]').data('process-label') );
					            },
					            success: function( e ){
					            	if( e.result == 'success' )
					            	{
										show_alert( frm.find('.message-text-success').html() );
					            	}
					            	else
					            	{
					            		show_alert( e.message );
					            	}
					            },
					            error: function( e ){
					            },
					            complete: function( e ){
					            	frm.find('[type=submit]').prop('disabled', false).text( frm.find('[type=submit]').data('label') );
					            }
					        });

					        return false;
					    }
					});
				}
			}
		});
	});

	if( jQuery('#popup-alert').length > 0 )
	{
		jQuery.fancybox.open({
			src  : '#popup-alert',
			type : 'inline',
			opts : {
				afterShow : function( instance, slide ) {
					console.info( 'done!' );
				}
			}
		});
	}
}

function init_subscribe_newsletter()
{
	jQuery('[name=subscribe_newsletter]').on('submit', function(){
		var form = jQuery(this);

		jQuery.ajax({
            url: form.attr('action'),
            data: form.serializeArray(),
            dataType : 'json',
            method: 'POST',
            beforeSend: function( xhr ){
            	form.find('[type=submit]').prop('disabled', true).val('PROCESSING...');
            },
            success: function( e ){ 
                if( e.status == 'subscribed' )
                {
                	show_alert( 'Thank you for signup to our newsletter' );
                }
                else
                {
                	show_alert( e.detail );
                }
            },
            error: function( e ){
            	show_alert( 'Failed to sign up' );
            },
            complete: function( e ){
	            form.find('[type=submit]').prop('disabled', false).val('SIGN ME UP');
            }
        });

        return false;
	});
}

function init_register()
{
	jQuery('[name=form_register]').validate({
		errorPlacement: function(error, element){
			return null
		},
		onfocusout: function( element ){
			if( jQuery(element).attr('name') == 'confirm_password' )
			{
				jQuery(element).rules('add', {
					equalTo : '[name=user_password]'
				});
			}

	        this.element( element );
	    }
	});
}

function init_login()
{
	jQuery('[name=form_login]').validate({
		errorPlacement: function(error, element){
			return null
		},
		onfocusout: function( element ){
	        this.element( element );
	    }
	});
}

function init_forgot_password()
{
	jQuery('[name=form_forgot]').validate({
		errorPlacement: function(error, element){
			return null
		},
		onfocusout: function( element ){
	        this.element( element );
	    }
	});
}

function init_search_box()
{
	jQuery('.input-box input').on('focusin', function(){
		jQuery(this).parent().addClass('open');
	});

	jQuery('.input-box input').on('focusout', function(){
		jQuery(this).parent().removeClass('open');
	});

	jQuery('.m-src .input-box .search').on('click', function(){
		jQuery(this).parent().toggleClass('open');
	});
}