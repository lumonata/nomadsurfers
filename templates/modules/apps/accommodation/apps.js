function init_accommodation_table()
{
    if( jQuery('#table_accommodation').length > 0 )
    {
        var actable = jQuery('#table_accommodation').DataTable({
            lengthChange: false,
            deferRender: true,
            processing: true,
            serverSide: true,
            pageLength: 10,
            info: false,
            order: [],
            ajax: {
                url: jQuery('#ajax_url').val(),
                type: 'POST',
                data: function( d ){
                    d.search = jQuery('[name=asearchval]').val();
                    d.pKEY   = 'load';
                }
            },
            columns: [
                {
                    data: 'lstatus', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'ltitle', render: function ( data, type, row ){
                        var html =
                        '<a class="alg-i dnl flex" href="' + row.ldetaillink + '">'+
                            '<figure>'+
                                '<img src="' + row.lthumbnail + '" class="cover">'+
                            '</figure>'+
                            '<span>' + data + '</span>'+
                            '<span class="flex">' + row.lrating + '</span>'+
                        '</a>';

                        return html;
                    }
                },
                {
                    data: 'llisted', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'lpolicy', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'llocation', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'ldlu', render: function ( data, type, row ){
                        return data;
                    }
                }
            ],
            dom: 'tp'
        });

        actable.on( 'draw.dt', function () {
            init_table_action( this, actable );
        });

        jQuery('[name=asearchbtn]').on('click', function(){
            actable.ajax.reload();
        });
    }
}

function init_rooms()
{
    init_delete_room();
    init_room_img();
    init_inc();

    jQuery('.admmoreroom').on('click', function(){
        var url = jQuery('#ajax_url').val();
        var prt = jQuery(this).parent();
        var prm = new Object;
            prm.pKEY    = 'add_rooms';
            prm.post_id = jQuery('#lpost_id').val();

        jQuery.ajax({
            url: url,
            data: prm,
            type: 'POST',
            dataType : 'html',
            success: function(e){
                if( e != '' )
                {
                    jQuery('#section-rooms .note').remove();
                    jQuery('#section-rooms .cfield-wrap').append( e );

                    init_room_img( jQuery('.eachroomupload:last-of-type .addroomimage') );

                    init_number( jQuery('.eachroomupload:last-of-type .text-number') );

                    init_delete_room();

                    init_inc();
                }
                else
                {
                    alert( 'Something wrong on the request' );
                }
            },
            error: function(e){
               alert( 'Something wrong on the request' );
            }
        })
    });
}

function init_room_img( selector )
{
    if( typeof selector == 'undefined' )
    {
        selector = '.addroomimage';
    }

    if( jQuery( selector ).length > 0 )
    {
        jQuery( selector ).each( function( e ){
            var cont  = jQuery(this);
            var input = cont.find('input[type=file]');

            var types = input.data('filetype');
            var field = input.data('field');
            var hash  = input.data('hash');
            var dfsrc = input.data('src');

            var modid = jQuery('#file-wrap-' + hash).data('module');
            var appid = jQuery('#file-wrap-' + hash).data('id');
            var url   = jQuery('#ajax_url').val();

            input.fileupload({
                paramName: 'attachment',
                dataType: 'json',
                dropZone: null,
                formData: {
                    pKEY: 'upload_file',
                    file_type: types,
                    module_id: modid,
                    app_id: appid,
                    hash: hash
                },
                url: url,
                add: function( e, data ){
                    jQuery('#file-wrap-' + hash).find('.addroomimage').css('background-image', 'url(' + window.URL.createObjectURL( data.files[0] ) + ')');
                    jQuery('#file-wrap-' + hash).addClass('eachroomuploadwithimage');

                    data.context = jQuery('#file-wrap-' + hash);

                    delete_file( data.context );

                    data.submit();
                },
                fail: function( e, data ){
                    alert( 'Upload failed! Something wrong' );

                    data.context.find('.addroomimage').css('background-image', 'url(' + dfsrc + ')');
                    data.context.removeClass('eachroomuploadwithimage');
                    data.context.find('.delimg').attr('data-id', '' );
                    data.context.find('.rimage').val( '' );
                },
                done: function( e, data ){
                    if( data.textStatus == 'success' )
                    {
                        if( data.jqXHR.responseJSON.result == 'success' )
                        {
                            data.context.find('.delimg').attr('data-id', data.jqXHR.responseJSON.id );
                            data.context.find('.rimage').val( data.jqXHR.responseJSON.id );
                        }
                        else
                        {
                            if( data.jqXHR.responseJSON.result == 'not-supported' )
                            {
                                alert( 'Not supported file' );
                            }
                            else
                            {
                                alert( 'Upload failed! Something wrong' );
                            }

                            data.context.find('.addroomimage').css('background-image', 'url(' + dfsrc + ')');
                            data.context.removeClass('eachroomuploadwithimage');
                            data.context.find('.delimg').attr('data-id', '' );
                            data.context.find('.rimage').val( '' );
                        }
                    }
                    else
                    {
                        alert( 'Upload failed! Something wrong' );

                        data.context.find('.addroomimage').css('background-image', 'url(' + dfsrc + ')');
                        data.context.removeClass('eachroomuploadwithimage');
                        data.context.find('.delimg').attr('data-id', '' );
                        data.context.find('.rimage').val( '' );
                    }
                }
            });
        });
        
        delete_file( jQuery('.eachroomuploadwithimage') );
    }
}

function init_delete_room()
{
    jQuery('.eachroomupload .delete').unbind();
    jQuery('.eachroomupload .delete').on('click', function(){       
        var url = jQuery('#ajax_url').val();
        var prt = jQuery(this).parent();
        var prm = new Object;
            prm.pKEY = 'delete_rooms';
            prm.id   = jQuery(this).data('id');
            prm.mod  = jQuery(this).data('mod');

        jQuery.ajax({
            url: url,
            data: prm,
            type: 'POST',
            dataType : 'json',
            success: function(e){
                if( e.result == 'success' )
                {
                    prt.fadeOut(300, function(){
                        jQuery(this).remove();

                        if( jQuery( '#section-rooms .eachroomupload' ).length == 0 )
                        {
                            var note = jQuery('#section-rooms .cfield-wrap').data( 'note' );

                            jQuery('#section-rooms .cfield-wrap').append( '<p class="note">' + note + '</p>' );
                        }
                    });
                }
                else
                {
                    alert( 'Something wrong on the request' );
                }
            },
            error: function(e){
               alert( 'Something wrong on the request' );
            }
        })
    });
}

function init_addons()
{
    init_delete_addons();
    init_addons_img();

    jQuery('.admmoreaddons').on('click', function(){        
        var url = jQuery('#ajax_url').val();
        var prt = jQuery(this).parent();
        var prm = new Object;
            prm.pKEY    = 'add_addons';
            prm.post_id = jQuery('#lpost_id').val();

        jQuery.ajax({
            url: url,
            data: prm,
            type: 'POST',
            dataType : 'html',
            success: function(e){
                if( e != '' )
                {
                    jQuery('#section-add-ons .note').remove();
                    jQuery('#section-add-ons .cfield-wrap').append( e );

                    jQuery('.eachaddonsupload:last-of-type .sl2_').select2();

                    init_addons_img( jQuery('.eachaddonsupload:last-of-type .addaddonsimage') );

                    init_number( jQuery('.eachaddonsupload:last-of-type .text-number') );

                    init_delete_addons();
                }
                else
                {
                    alert( 'Something wrong on the request' );
                }
            },
            error: function(e){
               alert( 'Something wrong on the request' );
            }
        })
    });
}

function init_addons_img( selector )
{
    if( typeof selector == 'undefined' )
    {
        selector = '.addaddonsimage';
    }

    if( jQuery( selector ).length > 0 )
    {
        jQuery( selector ).each( function( e ){
            var cont  = jQuery(this);
            var input = cont.find('input[type=file]');

            var types = input.data('filetype');
            var field = input.data('field');
            var hash  = input.data('hash');
            var dfsrc = input.data('src');

            var modid = jQuery('#file-wrap-' + hash).data('module');
            var appid = jQuery('#file-wrap-' + hash).data('id');
            var url   = jQuery('#ajax_url').val();

            input.fileupload({
                paramName: 'attachment',
                dataType: 'json',
                dropZone: null,
                formData: {
                    pKEY: 'upload_file',
                    file_type: types,
                    module_id: modid,
                    app_id: appid,
                    hash: hash
                },
                url: url,
                add: function( e, data ){
                    jQuery('#file-wrap-' + hash).find('.addaddonsimage').css('background-image', 'url(' + window.URL.createObjectURL( data.files[0] ) + ')');
                    jQuery('#file-wrap-' + hash).addClass('eachaddonsuploadwithimage');

                    data.context = jQuery('#file-wrap-' + hash);

                    delete_file( data.context );

                    data.submit();
                },
                fail: function( e, data ){
                    alert( 'Upload failed! Something wrong' );

                    data.context.find('.addaddonsimage').css('background-image', 'url(' + dfsrc + ')');
                    data.context.removeClass('eachaddonsuploadwithimage');
                    data.context.find('.delimg').attr('data-id', '' );
                    data.context.find('.rimage').val( '' );
                },
                done: function( e, data ){
                    if( data.textStatus == 'success' )
                    {
                        if( data.jqXHR.responseJSON.result == 'success' )
                        {
                            data.context.find('.delimg').attr('data-id', data.jqXHR.responseJSON.id );
                            data.context.find('.rimage').val( data.jqXHR.responseJSON.id );
                        }
                        else
                        {
                            if( data.jqXHR.responseJSON.result == 'not-supported' )
                            {
                                alert( 'Not supported file' );
                            }
                            else
                            {
                                alert( 'Upload failed! Something wrong' );
                            }

                            data.context.find('.addaddonsimage').css('background-image', 'url(' + dfsrc + ')');
                            data.context.removeClass('eachaddonsuploadwithimage');
                            data.context.find('.delimg').attr('data-id', '' );
                            data.context.find('.rimage').val( '' );
                        }
                    }
                    else
                    {
                        alert( 'Upload failed! Something wrong' );

                        data.context.find('.addaddonsimage').css('background-image', 'url(' + dfsrc + ')');
                        data.context.removeClass('eachaddonsuploadwithimage');
                        data.context.find('.delimg').attr('data-id', '' );
                        data.context.find('.rimage').val( '' );
                    }
                }
            });
        });
        
        delete_file( jQuery('.eachaddonsuploadwithimage') );
    }
}

function init_delete_addons()
{
    jQuery('.eachaddonsupload .delete').unbind();
    jQuery('.eachaddonsupload .delete').on('click', function(){     
        var url = jQuery('#ajax_url').val();
        var prt = jQuery(this).parent();
        var prm = new Object;
            prm.pKEY = 'delete_addons';
            prm.id   = jQuery(this).data('id');
            prm.mod  = jQuery(this).data('mod');

        jQuery.ajax({
            url: url,
            data: prm,
            type: 'POST',
            dataType : 'json',
            success: function(e){
                if( e.result == 'success' )
                {
                    prt.fadeOut(300, function(){
                        jQuery(this).remove();

                        if( jQuery( '#section-add-ons .eachaddonsupload' ).length == 0 )
                        {
                            var note = jQuery('#section-add-ons .cfield-wrap').data( 'note' );

                            jQuery('#section-add-ons .cfield-wrap').append( '<p class="note">' + note + '</p>' );
                        }
                    });
                }
                else
                {
                    alert( 'Something wrong on the request' );
                }
            },
            error: function(e){
               alert( 'Something wrong on the request' );
            }
        })
    });
}

function init_activity()
{
    init_delete_activity();
    init_activity_img();

    jQuery('.admmoreactivity').on('click', function(){      
        var url = jQuery('#ajax_url').val();
        var prt = jQuery(this).parent();
        var prm = new Object;
            prm.pKEY    = 'add_activity';
            prm.post_id = jQuery('#lpost_id').val();

        jQuery.ajax({
            url: url,
            data: prm,
            type: 'POST',
            dataType : 'html',
            success: function(e){
                if( e != '' )
                {
                    jQuery(e).insertBefore( '.admmoreactivity' );

                    init_activity_img( jQuery('.eachactivityupload:last-of-type .addactivityimage') );

                    init_delete_activity();
                }
                else
                {
                    alert( 'Something wrong on the request' );
                }
            },
            error: function(e){
               alert( 'Something wrong on the request' );
            }
        })
    });
}

function init_activity_img( selector )
{
    if( typeof selector == 'undefined' )
    {
        selector = '.addactivityimage';
    }

    if( jQuery( selector ).length > 0 )
    {
        jQuery( selector ).each( function( e ){
            var cont  = jQuery(this);
            var input = cont.find('input[type=file]');

            var types = input.data('filetype');
            var field = input.data('field');
            var hash  = input.data('hash');
            var dfsrc = input.data('src');

            var url   = jQuery('#ajax_url').val();
            var appid = jQuery('#lpost_id').val();
            var modid = jQuery('#lmodule').val();

            input.fileupload({
                paramName: 'attachment',
                dataType: 'json',
                dropZone: null,
                formData: {
                    pKEY: 'upload_file',
                    file_type: types,
                    module_id: modid,
                    app_id: appid,
                    hash: hash
                },
                url: url,
                add: function( e, data ){
                    jQuery('#file-wrap-' + hash).find('.addactivityimage').css('background-image', 'url(' + window.URL.createObjectURL( data.files[0] ) + ')');
                    jQuery('#file-wrap-' + hash).addClass('eachactivityuploadwithimage');

                    data.context = jQuery('#file-wrap-' + hash);

                    delete_file( data.context );

                    data.submit();
                },
                fail: function( e, data ){
                    alert( 'Upload failed! Something wrong' );

                    data.context.find('.addactivityimage').css('background-image', 'url(' + dfsrc + ')');
                    data.context.removeClass('eachactivityuploadwithimage');
                    data.context.find('.delimg').attr('data-id', '' );
                    data.context.find('.rimage').val( '' );
                },
                done: function( e, data ){
                    if( data.textStatus == 'success' )
                    {
                        if( data.jqXHR.responseJSON.result == 'success' )
                        {
                            data.context.find('.delimg').attr('data-id', data.jqXHR.responseJSON.id );
                            data.context.find('.rimage').val( data.jqXHR.responseJSON.id );
                        }
                        else
                        {
                            if( data.jqXHR.responseJSON.result == 'not-supported' )
                            {
                                alert( 'Not supported file' );
                            }
                            else
                            {
                                alert( 'Upload failed! Something wrong' );
                            }

                            data.context.find('.addactivityimage').css('background-image', 'url(' + dfsrc + ')');
                            data.context.removeClass('eachactivityuploadwithimage');
                            data.context.find('.delimg').attr('data-id', '' );
                            data.context.find('.rimage').val( '' );
                        }
                    }
                    else
                    {
                        alert( 'Upload failed! Something wrong' );

                        data.context.find('.addactivityimage').css('background-image', 'url(' + dfsrc + ')');
                        data.context.removeClass('eachactivityuploadwithimage');
                        data.context.find('.delimg').attr('data-id', '' );
                        data.context.find('.rimage').val( '' );
                    }
                }
            });
        });
        
        delete_file( jQuery('.eachactivityuploadwithimage') );
    }
}

function init_delete_activity()
{
    jQuery('.eachactivityupload .delete').unbind();
    jQuery('.eachactivityupload .delete').on('click', function(){       
        jQuery(this).parent().fadeOut(300, function(){
            jQuery(this).remove();
        });
    });
}

function init_surf_info()
{
    init_delete_surf_info();

    jQuery('.admmoreinfo').on('click', function(){      
        var url = jQuery('#ajax_url').val();
        var prt = jQuery(this).parent();
        var prm = new Object;
            prm.pKEY    = 'add_surf_info';
            prm.post_id = jQuery('#lpost_id').val();

        jQuery.ajax({
            url: url,
            data: prm,
            type: 'POST',
            dataType : 'html',
            success: function(e){
                if( e != '' )
                {
                    jQuery(e).insertBefore( '.admmoreinfo' );

                    init_delete_surf_info();
                }
                else
                {
                    alert( 'Something wrong on the request' );
                }
            },
            error: function(e){
               alert( 'Something wrong on the request' );
            }
        })
    });
}

function init_delete_surf_info()
{
    jQuery('.eachsurfinfo .delete').unbind();
    jQuery('.eachsurfinfo .delete').on('click', function(){     
        jQuery(this).parent().fadeOut(300, function(){
            jQuery(this).remove();
        });
    });
}

function init_upload()
{
    if( jQuery('.chgimg').length > 0 )
    {
        jQuery('.chgimg').each( function( e ){
            var cont  = jQuery(this);
            var input = cont.find('input[type=file]');

            var url   = jQuery('#ajax_url').val();
            var appid = jQuery('#lpost_id').val();
            var modid = jQuery('#lmodule').val();

            var types = input.data('filetype');
            var field = input.data('field');
            var hash  = input.data('hash');
            var dfsrc = input.data('src');

            input.fileupload({
                paramName: 'attachment',
                dataType: 'json',
                dropZone: null,
                formData: {
                    pKEY: 'upload_file',
                    file_type: types,
                    module_id: modid,
                    app_id: appid,
                    hash: hash
                },
                url: url,
                add: function( e, data ){
                    jQuery('#file-wrap-' + hash).find('.cover').prop('src', window.URL.createObjectURL( data.files[0] ) );
                    jQuery('#file-wrap-' + hash).find('.delimg').removeClass('hidden');
                    jQuery('#file-wrap-' + hash).removeClass('uploaded');

                    data.context = jQuery('#file-wrap-' + hash);

                    delete_file( data.context );

                    data.submit();
                },
                always: function( e, data ){                
                    data.context.addClass('uploaded');
                },
                fail: function( e, data ){
                    alert( 'Upload failed! Something wrong' );

                    data.context.find('.delimg').attr('data-id', '' );
                    data.context.find('.delimg').addClass('hidden');
                    data.context.find('.cover').prop('src', dfsrc );
                },
                done: function( e, data ){
                    if( data.textStatus == 'success' )
                    {
                        if( data.jqXHR.responseJSON.result == 'success' )
                        {
                            data.context.find('.delimg').attr('data-id', data.jqXHR.responseJSON.id );
                            data.context.find('input').val( data.jqXHR.responseJSON.id );
                        }
                        else
                        {
                            if( data.jqXHR.responseJSON.result == 'not-supported' )
                            {
                                alert( 'Not supported file' );
                            }
                            else
                            {
                                alert( 'Upload failed! Something wrong' );
                            }

                            data.context.find('.delimg').attr('data-id', '' );
                            data.context.find('.delimg').addClass('hidden');
                            data.context.find('.cover').prop('src', dfsrc );
                        }
                    }
                    else
                    {
                        alert( 'Upload failed! Something wrong' );

                        data.context.find('.delimg').attr('data-id', '' );
                        data.context.find('.delimg').addClass('hidden');
                        data.context.find('.cover').prop('src', dfsrc );
                        data.context.find('input').val( '' );
                    }
                }
            });
        });
        
        delete_file( jQuery('.imgsingleupload') );
    }

    if( jQuery('.addbatchimage').length > 0 )
    {
        jQuery('.addbatchimage').each( function( e ){
            var cont  = jQuery(this);
            var drop  = cont.parent().parent().find('.file-drop-zone');
            var input = cont.parent().parent().find('input[type=file]');

            var url   = jQuery('#ajax_url').val();
            var appid = jQuery('#lpost_id').val();
            var modid = jQuery('#lmodule').val();

            var types = input.data('filetype');
            var field = input.data('field');
            var hash  = input.data('hash');

            if( typeof appid == 'undefined' )
            {
                appid = jQuery('#lterm_id').val();
            }

            var sel = jQuery('#file-drop-zone-' + hash);

            input.fileupload({
                paramName: 'attachment',
                dataType: 'json',
                dropZone: drop,
                formData: {
                    pKEY: 'upload_file',
                    file_type: types,
                    module_id: modid,
                    app_id: appid,
                    hash: hash
                },
                url: url,
                add: function( e, data ){
                    var temp = jQuery('#cloned-wrapp-' + hash).find('.item').clone();
                        temp.find('.cover').prop('src', window.URL.createObjectURL( data.files[0] ) );

                    temp.insertBefore( '#file-drop-zone-' + hash + ' .addbatchimage' );

                    data.context = sel.find('.item:last-of-type');

                    delete_drop_file( data.context );

                    data.submit();
                },
                fail: function( e, data ){
                    data.context.addClass('not-uploaded');
                },
                done: function( e, data ){
                    if( data.textStatus == 'success' )
                    {
                        if( data.jqXHR.responseJSON.result == 'success' )
                        {
                            data.context.find('input').attr('name', 'fields[ladditional][' + field + '][]');
                            data.context.find('.delimg').attr('data-id', data.jqXHR.responseJSON.id);
                            data.context.find('.cover').prop('src', data.jqXHR.responseJSON.src);
                            data.context.find('input').val( data.jqXHR.responseJSON.id );
                            data.context.addClass('uploaded');
                        }
                        else
                        {
                            data.context.addClass('not-uploaded');
                        }
                    }
                    else
                    {
                        data.context.addClass('not-uploaded');
                    }
                },
                stop: function( e ){
                    if( sel.find('.not-uploaded').length > 1 )
                    {
                        alert( 'Some files can\'t be uploaded, maybe because you uploaded a file with the wrong format or some other reason' );
                    }
                    else if( sel.find('.not-uploaded').length == 1 )
                    {
                        alert( 'Upload failed! Something wrong, maybe because you uploaded a file with the wrong format or some other reason' );
                    }

                    if( sel.find('.not-uploaded').length > 0 )
                    {
                        sel.find('.not-uploaded').each(function(){
                            jQuery(this).remove();
                        })
                    }
                }
            });
        });
        
        delete_drop_file( jQuery('.imgbatchupload .item') );

        jQuery('.addbatchimage').fancybox({
            baseClass: 'popup-upload-media',
            toolbar  : false,
            smallBtn : false,
            iframe : {
                preload : false
            }
        })
    }
}

function init_package()
{
    if( jQuery( '#section-package' ).length > 0 )
    {
        init_delete_package();
        init_package_type_action();
        init_package_date_range_picker();
        init_number( '.text-number-night' );

        jQuery('.admmorepackage').on('click', function(){
            var url = jQuery('#ajax_url').val();
            var prt = jQuery(this).parent();
            var prm = new Object;
                prm.pKEY    = 'add_package';
                prm.post_id = jQuery('#lpost_id').val();

            jQuery.ajax({
                url: url,
                data: prm,
                type: 'POST',
                dataType : 'html',
                success: function(e){
                    if( e != '' )
                    {
                        jQuery('#section-package .note').remove();
                        jQuery('#section-package .cfield-wrap').append( e );

                        init_package_date_range_picker( jQuery('.eachpackageupload:last-of-type') );

                        init_number( jQuery('.eachpackageupload:last-of-type .text-number-night') );

                        init_number( jQuery('.eachpackageupload:last-of-type .text-number') );

                        init_select2( jQuery('.eachpackageupload:last-of-type .sl2_') );

                        init_tinymce( '.eachpackageupload:last-of-type .tinymce' );

                        init_package_type_action();

                        init_delete_package();
                    }
                    else
                    {
                        alert( 'Something wrong on the request' );
                    }
                },
                error: function(e){
                   alert( 'Something wrong on the request' );
                }
            });
        });
    }
}

function init_delete_package()
{
    jQuery('.eachpackageupload .delete').unbind();
    jQuery('.eachpackageupload .delete').on('click', function(){       
        var url = jQuery('#ajax_url').val();
        var prt = jQuery(this).parent();
        var prm = new Object;
            prm.pKEY = 'delete_package';
            prm.id   = jQuery(this).data('id');

        jQuery.ajax({
            url: url,
            data: prm,
            type: 'POST',
            dataType : 'json',
            success: function(e){
                if( e.result == 'success' )
                {
                    prt.fadeOut(300, function(){
                        jQuery(this).remove();

                        if( jQuery( '#section-package .eachpackageupload' ).length == 0 )
                        {
                            var note = jQuery('#section-package .cfield-wrap').data( 'note' );

                            jQuery('#section-package .cfield-wrap').append( '<p class="note">' + note + '</p>' );
                        }
                    });
                }
                else
                {
                    alert( 'Something wrong on the request' );
                }
            },
            error: function(e){
               alert( 'Something wrong on the request' );
            }
        })
    });
}

function init_package_date_range_picker( parent, selector )
{
    if( typeof parent == 'undefined' )
    {
        parent = '.eachpackageupload';
    }

    if( typeof selector == 'undefined' )
    {
        selector = '.text-date';
    }

    if( jQuery( parent ).find( selector ).length > 0 )
    {
        jQuery( parent ).find( selector ).each( function( i, e ){
            var prt = jQuery( this ).parent().parent();
            var out = prt.find('.check-out-date').val();
            var ins = prt.find('.check-in-date').val();
            var opt = new Object;
                opt.locale = { format: 'MMM DD, YYYY' };
                opt.autoUpdateInput = false;
                opt.autoApply = true;

            if( ins != '' )
            {
                opt.startDate = moment( ins ).format( 'MMM DD, YYYY' );
            }

            if( out != '' )
            {
                opt.endDate = moment( out ).format( 'MMM DD, YYYY' );
            }

            jQuery( this ).daterangepicker( opt, function( start, end, label ) {
                var sdate = start.format( 'MMM DD, YYYY' );
                var edate = end.format( 'MMM DD, YYYY' );
                
                prt.find('.check-in-date').val( sdate );
                prt.find('.check-out-date').val( edate );

                prt.find('.check-in-date').data('daterangepicker').setStartDate( sdate );
                prt.find('.check-in-date').data('daterangepicker').setEndDate( edate );
                
                prt.find('.check-out-date').data('daterangepicker').setStartDate( sdate );
                prt.find('.check-out-date').data('daterangepicker').setEndDate( edate );
            });
        });
    }
}

function init_package_type_action()
{
    jQuery('.package-type').unbind();
    jQuery('.package-type').on('change', function(){
        var prnt = jQuery(this).parents('.eachpackageupload');
        var vals = jQuery(this).val();

        prnt.find('.text-date').val('');
        prnt.find('.text-date').data('daterangepicker').setEndDate( moment() );
        prnt.find('.text-date').data('daterangepicker').setStartDate( moment() );

        if( vals == 0 )
        {
            prnt.find('.text-date').prop('disabled', true);
            prnt.find('.text-number-night').prop('disabled', true);
        }
        else
        {
            prnt.find('.text-date').prop('disabled', false);
            prnt.find('.text-number-night').prop('disabled', false);
        }
    });
}

function init_schedule()
{
    if( jQuery( '#section-schedule' ).length > 0 )
    {
        init_delete_schedule();
        init_schedule_date_range_picker();

        jQuery('.admmoreschedule').on('click', function(){
            var url = jQuery('#ajax_url').val();
            var prt = jQuery(this).parent();
            var prm = new Object;
                prm.pKEY    = 'add_schedule';
                prm.post_id = jQuery('#lpost_id').val();

            jQuery.ajax({
                url: url,
                data: prm,
                type: 'POST',
                dataType : 'html',
                success: function(e){
                    if( e != '' )
                    {
                        jQuery('#section-schedule .note').remove();
                        jQuery('#section-schedule .cfield-wrap').append( e );

                        init_schedule_date_range_picker( jQuery('.eachscheduleupload:last-of-type') );

                        init_number( jQuery('.eachscheduleupload:last-of-type .text-number') );

                        init_select2( jQuery('.eachscheduleupload:last-of-type .sl2_') );

                        init_delete_schedule();
                    }
                    else
                    {
                        alert( 'Something wrong on the request' );
                    }
                },
                error: function(e){
                   alert( 'Something wrong on the request' );
                }
            })
        });
    }
}

function init_delete_schedule()
{
    jQuery('.eachscheduleupload .delete').unbind();
    jQuery('.eachscheduleupload .delete').on('click', function(){       
        var url = jQuery('#ajax_url').val();
        var prt = jQuery(this).parent();
        var prm = new Object;
            prm.pKEY = 'delete_schedule';
            prm.id   = jQuery(this).data('id');

        jQuery.ajax({
            url: url,
            data: prm,
            type: 'POST',
            dataType : 'json',
            success: function(e){
                if( e.result == 'success' )
                {
                    prt.fadeOut(300, function(){
                        jQuery(this).remove();

                        if( jQuery( '#section-schedule .eachscheduleupload' ).length == 0 )
                        {
                            var note = jQuery('#section-schedule .cfield-wrap').data( 'note' );

                            jQuery('#section-schedule .cfield-wrap').append( '<p class="note">' + note + '</p>' );
                        }
                    });
                }
                else
                {
                    alert( 'Something wrong on the request' );
                }
            },
            error: function(e){
               alert( 'Something wrong on the request' );
            }
        })
    });
}

function init_schedule_date_range_picker( parent, selector )
{
    if( typeof parent == 'undefined' )
    {
        parent = '.eachscheduleupload';
    }

    if( typeof selector == 'undefined' )
    {
        selector = '.text-date';
    }

    if( jQuery( parent ).find( selector ).length > 0 )
    {
        jQuery( parent ).find( selector ).each( function( i, e ){
            var prt = jQuery( this ).parent().parent();
            var out = prt.find('.check-out-date').val();
            var ins = prt.find('.check-in-date').val();
            var opt = new Object;
                opt.locale = { format: 'MMM DD, YYYY' };
                opt.autoUpdateInput = false;
                opt.autoApply = true;

            if( ins != '' )
            {
                opt.startDate = moment( ins ).format( 'MMM DD, YYYY' );
            }

            if( out != '' )
            {
                opt.endDate = moment( out ).format( 'MMM DD, YYYY' );
            }

            jQuery( this ).daterangepicker( opt, function( start, end, label ) {
                var sdate = start.format( 'MMM DD, YYYY' );
                var edate = end.format( 'MMM DD, YYYY' );
                
                prt.find('.check-in-date').val( sdate );
                prt.find('.check-out-date').val( edate );

                prt.find('.check-in-date').data('daterangepicker').setStartDate( sdate );
                prt.find('.check-in-date').data('daterangepicker').setEndDate( edate );
                
                prt.find('.check-out-date').data('daterangepicker').setStartDate( sdate );
                prt.find('.check-out-date').data('daterangepicker').setEndDate( edate );

                if( prt.find('.package-duration').length > 0 )
                {
                    var night = moment( edate ).diff( moment( sdate ), 'days');
                    var days  = night + 1;

                    if( night == 0 )
                    {
                        night = 1;
                    }
                    
                    prt.find('.package-duration').val( days + 'D' + night + 'N' );
                }
            });
        });
    }
}

function init_destination_option()
{
    var url = jQuery('#ajax_url').val();

    jQuery('#sl2_continent').select2({
        ajax: {
            url: url,
            delay: 100,
            type: 'POST',
            dataType: 'json',
            data: function( params ){
                return {
                    length: 10,
                    search: params.term,
                    pKEY: 'load-continent',
                    page: params.page || 1
                };
            },
            processResults: function( response, params ){
                params.page = params.page || 1;

                return {
                    results: response.results,
                    pagination: {
                        more: ( params.page * 10 ) < response.count
                    }
                }
            },
            cache: true
        }
    });

    jQuery('#sl2_country').select2({
        tags: true,
        ajax: {
            url: url,
            delay: 100,
            type: 'POST',
            dataType: 'json',
            data: function( params ){
                return {
                    continent: jQuery('#sl2_continent').val(),
                    page: params.page || 1,
                    pKEY: 'load-country',
                    search: params.term,
                    length: 10
                };
            },
            processResults: function( response, params ){
                params.page = params.page || 1;

                return {
                    results: response.results,
                    pagination: {
                        more: ( params.page * 10 ) < response.count
                    }
                }
            },
            cache: true
        }
    });

    jQuery('#sl2_state').select2({
        tags: true,
        ajax: {
            url: url,
            delay: 100,
            type: 'POST',
            dataType: 'json',
            data: function( params ){
                return {
                    country: jQuery('#sl2_country').val(),
                    page: params.page || 1,
                    pKEY: 'load-state',
                    search: params.term,
                    length: 10
                };
            },
            processResults: function( response, params ){
                params.page = params.page || 1;

                return {
                    results: response.results,
                    pagination: {
                        more: ( params.page * 10 ) < response.count
                    }
                }
            },
            cache: true
        }
    });

    jQuery('#sl2_city').select2({
        tags: true,
        ajax: {
            url: url,
            delay: 100,
            type: 'POST',
            dataType: 'json',
            data: function( params ){
                return {
                    state: jQuery('#sl2_state').val(),
                    page: params.page || 1,
                    pKEY: 'load-city',
                    search: params.term,
                    length: 10
                };
            },
            processResults: function( response, params ){
                params.page = params.page || 1;

                return {
                    results: response.results,
                    pagination: {
                        more: ( params.page * 10 ) < response.count
                    }
                }
            },
            cache: true
        }
    });

    jQuery('#sl2_continent').on('change.select2', function(){
        jQuery('#sl2_country').html('').trigger('change.select2');
        jQuery('#sl2_state').html('').trigger('change.select2');
        jQuery('#sl2_city').html('').trigger('change.select2');
    });
}