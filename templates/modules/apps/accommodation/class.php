<?php

class listings extends db
{
    private $template;
    private $actions;
    private $global;
    private $upload;
    private $fields;
    private $flash;
    private $terms;

    private $message;
    private $valid;
    private $notif;
    private $sess;
    private $post;
    private $lang;
    private $ref;
    private $get;

    public function __construct( $template_dir = '.', $actions )
    {
        parent::__construct();

        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->upload   = new upload();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }

    public function view( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $this->global->getPViewBothP( $mod, $usertype ) )
        {
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );
            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( HT_SERVER . MODULES_THEME_URL . '/apps/accommodation/apps.js' ) );
            $this->actions->add_actions( 'jvs', $this->init_view_js() );

            $this->template->Parse( 'vC', 'viewContent', false );
        }
        else
        {
            $this->template->set_var( 'error', '<div class="error">' . NO_AUTHO . '</div>' );
        }

        $this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    public function insert( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form-base.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->global->getPInsertBoth( $mod, $usertype ) )
        {
            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->create( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            //-- EXTRACT terms
            extract( $this->terms );

            $this->template->set_var( 'ltitle', $ltitle );
            $this->template->set_var( 'lpost_id', $lpost_id );
            $this->template->set_var( 'llang_id', $llang_id );
            $this->template->set_var( 'lorder_id', $lorder_id );
            $this->template->set_var( 'lcreated_by', $lcreated_by );

            $this->template->set_var( 'lpage_template', $this->get_page_template( $ladditional[ 'page_template' ] ) );
            $this->template->set_var( 'laccommodation_type', $this->get_accommodation_type_option( $accommodation_type ) );

            $this->template->set_var( 'message', $this->message );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js' ) );
            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css' ) );

            $this->template->Parse( 'fC', 'formContent', true );
        }
        else
        {
            $this->template->set_var( 'error', '<div class="error">' . NO_AUTHO . '</div>' );
        }

        $this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );

        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    public function edit( $mod, $usertype )
    {
        if( empty( $this->get ) === false )
        {
            $this->template->set_file( 'form', 'form-' . $this->get[ 'type' ] . '.html' );

            $this->template->set_block( 'form', 'formContent', 'fC' );
            $this->template->set_block( 'form', 'formBlock', 'fB' );

            if( $this->valid )
            {
                if( $this->global->getPEditBoth( $mod, $usertype ) )
                {
                    //-- EXECUTE update action
                    if( isset( $this->post[ 'save' ] ) )
                    {
                        $this->change( $mod );
                    }

                    //-- EXTRACT fields
                    extract( $this->fields );

                    //-- EXTRACT terms
                    extract( $this->terms );

                    $this->template->set_var( 'message', $this->message );

                    $this->template->set_var( 'ltitle', $ltitle );
                    $this->template->set_var( 'lmodule_id', $mod );
                    $this->template->set_var( 'lpost_id', $lpost_id );
                    $this->template->set_var( 'ldescription', $ldescription );

                    $this->template->set_var( 'calendar_link', HT_SERVER . SITE_URL . '/agent/calendar/' );
                    $this->template->set_var( 'calendar_class', $ladditional[ 'policy' ] == '0' ? '' : 'hidden' );
                    $this->template->set_var( 'permalink', HT_SERVER . SITE_URL . '/accommodation/' . $lsef_url . '/' );

                    $this->template->set_var( 'sidebar_menu', $this->global->getAgentSidebarMenuContent( $this->get ) );
                    $this->template->set_var( 'switcher_lang', $this->switcher_language( $llang_id, $this->ref, $this->get ) );

                    $this->template->set_var( 'laccommodation_galery', $this->get_accommodation_gallery( $ladditional[ 'accommodation_gallery' ] ) );
                    $this->template->set_var( 'laccommodation_image', $this->get_accommodation_image( $ladditional[ 'accommodation_image' ] ) );
                    $this->template->set_var( 'lhightlight', $this->get_accommodation_hightlight_option( $ladditional[ 'hightlight' ] ) );
                    $this->template->set_var( 'lamenities', $this->get_accommodation_amenities_option( $ladditional[ 'amenities' ] ) );

                    $this->template->set_var( 'ldietary', $this->get_accommodation_food_option( $ladditional[ 'food_dietary' ], 'dietary' ) );
                    $this->template->set_var( 'lmeals', $this->get_accommodation_food_option( $ladditional[ 'food_meals' ], 'meals' ) );

                    $this->template->set_var( 'lthingstodo', $this->get_accommodation_things_to_do( $ladditional[ 'things_to_do' ] ) );
                    $this->template->set_var( 'lsurfinfo', $this->get_accommodation_surfing_info( $ladditional[ 'surfing_info' ] ) );

                    $this->template->set_var( 'lwhats_included_activities', $this->get_accommodation_whats_included_option( $ladditional[ 'whats_included_activities' ], 'activities' ) );
                    $this->template->set_var( 'lwhats_included_transport', $this->get_accommodation_whats_included_option( $ladditional[ 'whats_included_transport' ], 'transport' ) );
                    $this->template->set_var( 'lwhats_included_stay', $this->get_accommodation_whats_included_option( $ladditional[ 'whats_included_stay' ], 'stay' ) );

                    $this->template->set_var( 'laccommodation_add_ons', $this->get_accommodation_add_ons( $lpost_id, $accommodation_add_ons ) );
                    $this->template->set_var( 'laccommodation_type_of_surf', $this->get_accommodation_surf_trip_option( $surf_trip ) );
                    $this->template->set_var( 'laccommodation_rooms', $this->get_accommodation_rooms( $lpost_id, $room_type ) );
                    $this->template->set_var( 'laccommodation_schedule', $this->get_accommodation_schedule( $lpost_id ) );
                    $this->template->set_var( 'laccommodation_package', $this->get_accommodation_package( $lpost_id ) );
                    $this->template->set_var( 'laccommodation_venue', $this->get_accommodation_venue( $lpost_id ) );
                    $this->template->set_var( 'lstatus', $this->get_accommodation_status_option( $lstatus ) );


                    $this->template->set_var( 'accommodation_continent', $this->get_accommodation_location_option( $ladditional[ 'accommodation_continent' ] ) );
                    $this->template->set_var( 'accommodation_country', $this->get_accommodation_location_option( $ladditional[ 'accommodation_country' ] ) );
                    $this->template->set_var( 'accommodation_state', $this->get_accommodation_location_option( $ladditional[ 'accommodation_state' ] ) );
                    $this->template->set_var( 'accommodation_city', $this->get_accommodation_location_option( $ladditional[ 'accommodation_city' ] ) );

                    $this->template->set_var( 'lcancellation_policy', $this->get_cancellation_policy_option( $ladditional[ 'cancellation_policy' ] ) );
                    $this->template->set_var( 'lcheck_out_time', $this->get_check_in_out_time_option( $ladditional[ 'check_out_time' ] ) );
                    $this->template->set_var( 'lcheck_in_time', $this->get_check_in_out_time_option( $ladditional[ 'check_in_time' ] ) );
                    $this->template->set_var( 'lpayment_policy', $this->get_payment_policy_option( $ladditional[ 'payment_policy' ] ) );
                    $this->template->set_var( 'lpayment_method', $this->get_payment_method_option( $ladditional ) );

                    $this->template->set_var( 'accommodation_address', $ladditional[ 'accommodation_address' ] );
                    $this->template->set_var( 'closer_airport_name', $ladditional[ 'closer_airport_name' ] );
                    $this->template->set_var( 'closer_airport_city', $ladditional[ 'closer_airport_city' ] );
                    $this->template->set_var( 'accommodation_text', $ladditional[ 'accommodation_text' ] );
                    $this->template->set_var( 'arrival_by_plane', $ladditional[ 'arrival_by_plane' ] );
                    $this->template->set_var( 'arrival_by_train', $ladditional[ 'arrival_by_train' ] );
                    $this->template->set_var( 'arrival_by_boat', $ladditional[ 'arrival_by_boat' ] );
                    $this->template->set_var( 'arrival_by_bus', $ladditional[ 'arrival_by_bus' ] );
                    $this->template->set_var( 'map_coordinate', $ladditional[ 'map_coordinate' ] );
                    $this->template->set_var( 'post_code', $ladditional[ 'post_code' ] );
                    $this->template->set_var( 'food', $ladditional[ 'food' ] );

                    $this->template->set_var( 'last_minute_discount', $ladditional[ 'last_minute_discount' ] );
                    $this->template->set_var( 'early_bird_discount', $ladditional[ 'early_bird_discount' ] );
                    $this->template->set_var( 'bi_weekly_discount', $ladditional[ 'bi_weekly_discount' ] );
                    $this->template->set_var( 'monthly_discount', $ladditional[ 'monthly_discount' ] );
                    $this->template->set_var( 'min_age', $ladditional[ 'min_age' ] );
                    $this->template->set_var( 'max_age', $ladditional[ 'max_age' ] );

                    if( $ladditional[ 'page_template' ] == '0' )
                    {
                        $this->template->Parse( 'fMR', 'formMultiRoom', true );
                    }
                    elseif( $ladditional[ 'page_template' ] == '1' )
                    {
                        $this->template->Parse( 'fBC', 'formBoatCharter', true );
                    }
                    elseif( $ladditional[ 'page_template' ] == '2' )
                    {
                        $this->template->Parse( 'fSC', 'formSurfCamps', true );
                    }
                    elseif( $ladditional[ 'page_template' ] == '3' )
                    {
                        $this->template->Parse( 'fEP', 'formEntirePlace', true );
                    }

                    $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

                    $this->template->Parse( 'fC', 'formContent', true );
                }
                else
                {
                    $this->template->set_var( 'error', '<div class="error">' . NO_AUTHO . '</div>' );
                }
            }
            else
            {
                $this->template->set_var( 'error', '<div class="error">' . NOT_FOUND . '</div>' );
            }

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery-ui-dist@1.13.0/jquery-ui.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/momentjs/latest/moment.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js' ) );
            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.css' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js' ) );
            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js' ) );            //-- ADD default CSS assets
            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/blueimp-file-upload@10.32.0/js/jquery.fileupload.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/tinymce@5.10.2/tinymce.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/jquery.validate.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery-validation@1.19.3/dist/additional-methods.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( HT_SERVER . MODULES_THEME_URL . '/apps/accommodation/apps.js' ) );

            if( $this->get[ 'type' ] == 'general' )
            {
                $this->actions->add_actions( 'jvs', $this->init_general_js() );
            }
            elseif( $this->get[ 'type' ] == 'details' )
            {
                $this->actions->add_actions( 'jvs', $this->init_detail_js() );
            }
            elseif( $this->get[ 'type' ] == 'activity' )
            {
                $this->actions->add_actions( 'jvs', $this->init_activity_js() );
            }
            elseif( $this->get[ 'type' ] == 'settings' )
            {
                $this->actions->add_actions( 'jvs', $this->init_settings_js() );
            }

            $this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );

            return $this->template->Parse( 'fB', 'formBlock', false );
        }
    }

    public function load()
    {
        $cols = array(
            0 => 'a.lstatus',
            1 => 'a.ltitle',
            5 => 'a.ldlu'
        );

        //-- Query Limit
        $limit = parent::prepare_query( ' LIMIT ' . $this->post[ 'start' ] . ', ' . $this->post[ 'length' ] );

        //-- Query Order By
        if( empty( $this->post[ 'order' ] ) )
        {
            $order = parent::prepare_query( ' ORDER BY a.lorder_id ASC' );
        }
        else
        {
            $order = array();

            foreach( $this->post[ 'order' ] as $i => $od )
            {
                if( isset( $cols[ $od[ 'column' ] ] ) )
                {
                    $order[] = $cols[ $od[ 'column' ] ] . ' ' . $od[ 'dir' ];
                }
            }

            if( empty( $order ) )
            {
                $order = parent::prepare_query( ' ORDER BY a.lorder_id DESC' );
            }
            else
            {
                $order = parent::prepare_query( ' ORDER BY ' . implode( ', ', $order ) );
            }
        }

        //-- Main Conditional
        $where = parent::prepare_query( ' WHERE a.lcreated_by = %s AND a.ltype = %s AND a.llang_id = %d', $this->sess[ 'username' ], 'accommodation', $this->lang );

        //-- Query Additional Where
        $h = array();

        if( $this->post[ 'search' ] != '' )
        {
            $s = array();

            foreach( $cols as $col )
            {
                $s[] = parent::prepare_query( $col . ' LIKE %s', '%' . $this->post[ 'search' ] . '%' );
            }

            $h[] = sprintf( '(%s)', implode( ' OR ', $s ) );
        }

        if( empty( $h ) === false )
        {
            $having = ' HAVING ' . implode( ' AND ', $h );
        }
        else
        {
            $having = '';
        }

        //-- Main Query
        $qm = 'SELECT
                a.lstatus,
                a.ltitle,
                a.lpost_id,
                a.ltype,
                a.ldlu
              FROM lumonata_post AS a' . $where . $having . $order;
        $rm = parent::query( $qm );
        $nm = parent::num_rows( $rm );

        $qf = $qm . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        $data = array();

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
                //-- GET additional field
                $sa = 'SELECT * FROM lumonata_additional_field AS a WHERE a.lapp_id = %d AND a.lmodule_id = ( SELECT a2.lmodule_id FROM lumonata_module AS a2 WHERE a2.lapps = %s )';
                $qa = parent::prepare_query( $sa, $df[ 'lpost_id' ], $df[ 'ltype' ] );
                $ra = parent::query( $qa );

                if( parent::num_rows( $ra ) > 0 )
                {
                    while( $da = parent::fetch_assoc( $ra ) )
                    {
                        $df[ $da[ 'ladditional_key' ] ] = $da[ 'ladditional_value' ];
                    }
                }

                $cr = $this->global->calculateRate( $df[ 'lpost_id' ] );

                if( isset( $cr->rating ) )
                {
                    $lrating = sprintf( '%s <img src="//%s/asset/img/star.svg">', $cr['rating'], THEME_URL );
                }
                else
                {
                    $lrating = '';
                }

                if( $df[ 'lstatus' ] == 1 )
                {
                    $lstatus = sprintf( '<span class="%s">%s</span>', 'sts__ actv', 'Active' );
                }
                else
                {
                    $lstatus = sprintf( '<span class="%s">%s</span>', 'sts__', 'Inactive' );
                }

                if( isset( $df[ 'policy' ] ) && $df[ 'policy' ] == '0' )
                {
                    $lpolicy = 'Automatic';
                }
                else
                {
                    $lpolicy = 'Manual';
                }

                $llisted = 'Yes';

                $data[] = array(
                    'ldetaillink' => HT_SERVER . SITE_URL . '/agent/listings/edit/?id=' . $df[ 'lpost_id' ] . '&type=general',
                    'lthumbnail'  => $this->global->get_attachment_url( $df[ 'accommodation_image' ], 237, 165 ),
                    'llocation'   => $this->global->getLocations( $df ),
                    'ldlu'        => date( 'F d, Y', $df[ 'ldlu' ] ),
                    'lpost_id'    => $df[ 'lpost_id' ],
                    'ltitle'      => $df[ 'ltitle' ],
                    'lstatus'     => $lstatus,
                    'lrating'     => $lrating,
                    'llisted'     => $llisted,
                    'lpolicy'     => $lpolicy,
                );
            }
        }

        return json_encode( array(
            'draw'            => intval( $this->post[ 'draw' ] ),
            'recordsFiltered' => intval( $nm ),
            'recordsTotal'    => intval( $nf ),
            'data'            => $data
        ) );
    }

    public function load_continent()
    {
        $result = array();

        if( isset( $this->post[ 'search' ] ) && !empty( $this->post[ 'search' ] ) )
        {
            $s = 'SELECT a.*
                  FROM lumonata_post AS a
                  LEFT JOIN lumonata_additional_field AS b ON b.lapp_id = a.lpost_id
                  WHERE a.ltype = %s AND b.ladditional_key = %s AND b.ladditional_value = %s AND a.ltitle LIKE %s';
            $q = parent::prepare_query( $s, 'destination', 'destination_type', '0', '%' . $this->post[ 'search' ] . '%' );
        }
        else
        {
            $s = 'SELECT a.*
                  FROM lumonata_post AS a
                  LEFT JOIN lumonata_additional_field AS b ON b.lapp_id = a.lpost_id
                  WHERE a.ltype = %s AND b.ladditional_key = %s AND b.ladditional_value = %s';
            $q = parent::prepare_query( $s, 'destination', 'destination_type', '0' );
        }

        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_array( $r ) )
            {
                $result[] = array( 'id' => $d[ 'lpost_id' ], 'text' => $d[ 'ltitle' ] );
            }
        }

        return json_encode( array( 'results' => $result ) );
    }

    public function load_country()
    {
        $result = array();

        if( isset( $this->post[ 'continent' ] ) && !empty( $this->post[ 'continent' ] ) )
        {
            if( isset( $this->post[ 'search' ] ) && !empty( $this->post[ 'search' ] ) )
            {
                $s = 'SELECT a.*
                      FROM lumonata_post AS a
                      LEFT JOIN lumonata_additional_field AS b ON b.lapp_id = a.lpost_id
                      WHERE a.ltype = %s AND a.lparent_id = %s AND b.ladditional_key = %s AND b.ladditional_value = %s AND a.ltitle LIKE %s';
                $q = parent::prepare_query( $s, 'destination', $this->post[ 'continent' ], 'destination_type', '1', '%' . $this->post[ 'search' ] . '%' );
            }
            else
            {
                $s = 'SELECT a.*
                      FROM lumonata_post AS a
                      LEFT JOIN lumonata_additional_field AS b ON b.lapp_id = a.lpost_id
                      WHERE a.ltype = %s AND a.lparent_id = %s AND b.ladditional_key = %s AND b.ladditional_value = %s';
                $q = parent::prepare_query( $s, 'destination', $this->post[ 'continent' ], 'destination_type', '1' );
            }

            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                while( $d = parent::fetch_array( $r ) )
                {
                    $result[] = array( 'id' => $d[ 'lpost_id' ], 'text' => $d[ 'ltitle' ] );
                }
            }
        }

        return json_encode( array( 'results' => $result ) );
    }

    public function load_state()
    {
        $result = array();

        if( isset( $this->post[ 'country' ] ) && !empty( $this->post[ 'country' ] ) )
        {
            if( isset( $this->post[ 'search' ] ) && !empty( $this->post[ 'search' ] ) )
            {
                $s = 'SELECT a.*
                      FROM lumonata_post AS a
                      LEFT JOIN lumonata_additional_field AS b ON b.lapp_id = a.lpost_id
                      WHERE a.ltype = %s AND a.lparent_id = %s AND b.ladditional_key = %s AND b.ladditional_value = %s AND a.ltitle LIKE %s';
                $q = parent::prepare_query( $s, 'destination', $this->post[ 'country' ], 'destination_type', '2', '%' . $this->post[ 'search' ] . '%' );
            }
            else
            {
                $s = 'SELECT a.*
                      FROM lumonata_post AS a
                      LEFT JOIN lumonata_additional_field AS b ON b.lapp_id = a.lpost_id
                      WHERE a.ltype = %s AND a.lparent_id = %s AND b.ladditional_key = %s AND b.ladditional_value = %s';
                $q = parent::prepare_query( $s, 'destination', $this->post[ 'country' ], 'destination_type', '2' );
            }

            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                while( $d = parent::fetch_array( $r ) )
                {
                    $result[] = array( 'id' => $d[ 'lpost_id' ], 'text' => $d[ 'ltitle' ] );
                }
            }
        }

        return json_encode( array( 'results' => $result ) );
    }

    public function load_city()
    {
        $result = array();

        if( isset( $this->post[ 'state' ] ) && !empty( $this->post[ 'state' ] ) )
        {
            if( isset( $this->post[ 'search' ] ) && !empty( $this->post[ 'search' ] ) )
            {
                $s = 'SELECT a.*
                      FROM lumonata_post AS a
                      LEFT JOIN lumonata_additional_field AS b ON b.lapp_id = a.lpost_id
                      WHERE a.ltype = %s AND a.lparent_id = %s AND b.ladditional_key = %s AND b.ladditional_value = %s AND a.ltitle LIKE %s';
                $q = parent::prepare_query( $s, 'destination', $this->post[ 'state' ], 'destination_type', '3', '%' . $this->post[ 'search' ] . '%' );
            }
            else
            {
                $s = 'SELECT a.*
                      FROM lumonata_post AS a
                      LEFT JOIN lumonata_additional_field AS b ON b.lapp_id = a.lpost_id
                      WHERE a.ltype = %s AND a.lparent_id = %s AND b.ladditional_key = %s AND b.ladditional_value = %s';
                $q = parent::prepare_query( $s, 'destination', $this->post[ 'state' ], 'destination_type', '3' );
            }

            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                while( $d = parent::fetch_array( $r ) )
                {
                    $result[] = array( 'id' => $d[ 'lpost_id' ], 'text' => $d[ 'ltitle' ] );
                }
            }
        }

        return json_encode( array( 'results' => $result ) );
    }

    public function validate( $data = array(), $is_edit = false )
    {
        $error = array();

        if( $is_edit )
        {
            foreach( $data as $field => $dt )
            {
                if( $field == 'ladditional' )
                {
                    foreach( $dt as $idx => $d )
                    {
                        if( $idx == 'accommodation_country' && $d == '' )
                        {
                            array_push( $error, 'Please choose your listing country/region' );
                        }
                        elseif( $idx == 'accommodation_state' && $d == '' )
                        {
                            array_push( $error, 'Please choose your listing state/province' );
                        }
                        elseif( $idx == 'accommodation_city' && $d == '' )
                        {
                            array_push( $error, 'Please choose your listing city/area' );
                        }
                    }
                }
            }
        }
        else
        {
            foreach( $data as $field => $dt )
            {
                if( $field == 'ltitle' && $dt == '' )
                {
                    array_push( $error, 'Listing name must have value' );
                }
                elseif( $field == 'ladditional' )
                {
                    foreach( $dt as $idx => $d )
                    {
                        if( $idx == 'title_part_1' && $d == '' )
                        {
                            array_push( $error, 'Please describe your listing' );
                        }
                        elseif( $idx == 'title_part_2' && $d == '' )
                        {
                            array_push( $error, 'Fill the closest beach of your listing' );
                        }
                        elseif( $idx == 'title_part_3' && $d == '' )
                        {
                            array_push( $error, 'Fill most prominent feature of your listing' );
                        }
                        elseif( $idx == 'title_part_4' && $d == '' )
                        {
                            array_push( $error, 'Fill exact location of your listing' );
                        }
                    }
                }
            }
        }

        return $error;
    }

    public function create( $mod )
    {
        $terms = $this->terms;
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- MERGE lsef_url INTO data array
            if( empty( $data[ 'lsef_url' ] ) )
            {
                $arr = array();

                foreach( array( 'title_part_1', 'title_part_2', 'title_part_3', 'title_part_4' ) as $dt )
                {
                    if( isset( $data[ 'ladditional' ][ $dt ] ) )
                    {
                        $arr[] = $data[ 'ladditional' ][ $dt ];
                    }
                }

                $data = array_merge( $data, array( 'lsef_url' => $this->sef_title( implode( ' ', $arr ) ) ) );
            }
            else
            {
                $data = array_merge( $data, array( 'lsef_url' => $this->sef_url( $data[ 'lsef_url' ] ) ) );
            }

            //-- INSERT lumonata_post
            $param  = array_diff_key( $data, array_flip( array( 'lpost_id', 'ladditional' ) ) );
            $result = parent::insert( 'lumonata_post', array_filter( $param ) );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to add new data', 'post' => $data ) );

                $this->global->saveLog( sprintf( "[%s] - %s\n", $this->sess[ 'username' ], $result[ 'message' ] ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/agent/listings/add/' );

                exit;
            }
            else
            {
                $lapp_id = parent::insert_id();

                //-- SYNC lumonata_post_relationship
                if( empty( $terms ) === false )
                {
                    foreach( $terms as $rule => $term )
                    {
                        if( is_array( $term ) )
                        {
                            foreach( $term as $t )
                            {
                                parent::insert( 'lumonata_post_relationship', array(
                                    'lpost_id' => $lapp_id,
                                    'lterm_id' => $t,
                                ) );
                            }
                        }
                        else
                        {
                            parent::insert( 'lumonata_post_relationship', array(
                                'lpost_id' => $lapp_id,
                                'lterm_id' => $term,
                            ) );
                        }
                    }
                }

                //-- SYNC lumonata_additional_field
                if( isset( $data[ 'ladditional' ] ) )
                {
                    foreach( $data[ 'ladditional' ] as $key => $value )
                    {
                        if( is_array( $value ) )
                        {
                            $value = json_encode( $value );
                        }

                        parent::insert( 'lumonata_additional_field', array(
                            'ladditional_value' => $value,
                            'ladditional_key'   => $key,
                            'lapp_id'           => $lapp_id,
                            'lmodule_id'        => $mod,
                        ) );
                    }
                }

                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully add new data' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/agent/listings/edit/?type=general&id=' . $lapp_id );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/agent/listings/add/' );

            exit;
        }
    }

    public function change( $mod )
    {
        $terms = $this->terms;
        $data  = $this->fields;
        $error = $this->validate( $data, true );

        if( empty( $error ) )
        {
            parent::begin();

            $commit = 1;

            //-- UPDATE lumonata_post
            $param = array_diff_key( $data, array_flip( array( 'lpost_id', 'ladditional' ) ) );
            $where = array( 'lpost_id' => $this->get[ 'id' ] );

            $result = parent::update( 'lumonata_post', $param, $where );

            if( is_array( $result ) )
            {
                $this->global->saveLog( sprintf( "[%s] - %s\n", $this->sess[ 'username' ], $result[ 'message' ] ) );

                $commit = 0;
            }
            else
            {
                //-- SYNC lumonata_post_relationship
                if( empty( $terms ) === false )
                {
                    $rt = parent::delete( 'lumonata_post_relationship', array( 'lpost_id' => $this->get[ 'id' ] ) );

                    if( is_array( $rt ) )
                    {
                        $commit = 0;
                    }
                    else
                    {
                        foreach( $terms as $rule => $term )
                        {
                            if( is_array( $term ) )
                            {
                                foreach( $term as $t )
                                {
                                    $rd = parent::insert( 'lumonata_post_relationship', array(
                                        'lpost_id' => $this->get[ 'id' ],
                                        'lterm_id' => $t,
                                    ) );

                                    if( is_array( $rd ) )
                                    {
                                        $commit = 0;

                                        break;
                                    }
                                }
                            }
                            else
                            {
                                $rd = parent::insert( 'lumonata_post_relationship', array(
                                    'lpost_id' => $this->get[ 'id' ],
                                    'lterm_id' => $term,
                                ) );

                                if( is_array( $rd ) )
                                {
                                    $commit = 0;
                                }
                            }
                        }
                    }
                }

                //-- SYNC lumonata_additional_field
                if( isset( $data[ 'ladditional' ] ) )
                {
                    $desmodule = $this->global->getFields( 'lumonata_module', 'lmodule_id', array( 'lapps' => 'destination' ) );
                    $continent = $data[ 'ladditional' ][ 'accommodation_continent' ];
                    $country   = '';
                    $state     = '';
                    $city      = '';

                    foreach( $data[ 'ladditional' ] as $key => $value )
                    {
                        if( is_array( $value ) )
                        {
                            $value = json_encode( $value );
                        }

                        if( $key == 'accommodation_country' )
                        {
                            $country = $this->global->getFields( 'lumonata_post', 'lpost_id', array( 'lpost_id' => $value, 'ltype' => 'destination' ) );

                            if( $country == '' )
                            {
                                $rr = parent::insert( 'lumonata_post', array(
                                    'lcreated_by'   => $this->sess[ 'username' ],
                                    'lusername'     => $this->sess[ 'username' ],
                                    'lsef_url'      => $this->sef_url( $value ),
                                    'llang_id'      => $this->lang,
                                    'ltype'         => 'destination',
                                    'lcreated_date' => time(),
                                    'ldlu'          => time(),
                                    'ltitle'        => $value,
                                    'lparent_id'    => $continent,
                                    'lstatus'       => 1
                                ) );

                                if( !is_array( $rr ) )
                                {
                                    $country = parent::insert_id();
                                    $value   = $country;

                                    parent::insert( 'lumonata_additional_field', array(
                                        'ladditional_key'   => 'destination_type',
                                        'lmodule_id'        => $desmodule,
                                        'lapp_id'           => $country,
                                        'ladditional_value' => 1,
                                    ) );
                                }
                            }
                            else
                            {
                                $country = $value;
                            }
                        }

                        if( $key == 'accommodation_state' )
                        {
                            $state = $this->global->getFields( 'lumonata_post', 'lpost_id', array( 'lpost_id' => $value, 'ltype' => 'destination' ) );

                            if( $state == '' )
                            {
                                $rr = parent::insert( 'lumonata_post', array(
                                    'lcreated_by'   => $this->sess[ 'username' ],
                                    'lusername'     => $this->sess[ 'username' ],
                                    'lsef_url'      => $this->sef_url( $value ),
                                    'llang_id'      => $this->lang,
                                    'ltype'         => 'destination',
                                    'lcreated_date' => time(),
                                    'ldlu'          => time(),
                                    'ltitle'        => $value,
                                    'lparent_id'    => $country,
                                    'lstatus'       => 1
                                ) );

                                if( !is_array( $rr ) )
                                {
                                    $state = parent::insert_id();
                                    $value = $state;

                                    parent::insert( 'lumonata_additional_field', array(
                                        'ladditional_key'   => 'destination_type',
                                        'lmodule_id'        => $desmodule,
                                        'lapp_id'           => $state,
                                        'ladditional_value' => 2,
                                    ) );
                                }
                            }
                            else
                            {
                                $state = $value;
                            }
                        }

                        if( $key == 'accommodation_city' )
                        {
                            $city = $this->global->getFields( 'lumonata_post', 'lpost_id', array( 'lpost_id' => $value, 'ltype' => 'destination' ) );

                            if( $city == '' )
                            {
                                $rr = parent::insert( 'lumonata_post', array(
                                    'lcreated_by'   => $this->sess[ 'username' ],
                                    'lusername'     => $this->sess[ 'username' ],
                                    'lsef_url'      => $this->sef_url( $value ),
                                    'llang_id'      => $this->lang,
                                    'ltype'         => 'destination',
                                    'lcreated_date' => time(),
                                    'ldlu'          => time(),
                                    'ltitle'        => $value,
                                    'lparent_id'    => $state,
                                    'lstatus'       => 1
                                ) );

                                if( !is_array( $rr ) )
                                {
                                    $city  = parent::insert_id();
                                    $value = $city;

                                    parent::insert( 'lumonata_additional_field', array(
                                        'ladditional_key'   => 'destination_type',
                                        'lmodule_id'        => $desmodule,
                                        'lapp_id'           => $city,
                                        'ladditional_value' => 3,
                                    ) );
                                }
                            }
                            else
                            {
                                $city = $value;
                            }
                        }

                        //-- Check Additonal Field Exist or Not
                        $aid = $this->global->getValueField3( 'lumonata_additional_field', 'ladditional_id', 'lapp_id', $this->get[ 'id' ], 'ladditional_key', $key, 'lmodule_id', $mod );

                        if( empty( $aid ) )
                        {
                            $ra = parent::insert( 'lumonata_additional_field', array(
                                'lapp_id'           => $this->get[ 'id' ],
                                'ladditional_value' => $value,
                                'ladditional_key'   => $key,
                                'lmodule_id'        => $mod,
                            ) );

                            if( is_array( $ra ) )
                            {
                                $commit = 0;

                                break;
                            }
                        }
                        else
                        {
                            $param = array( 'ladditional_value' => $value );
                            $where = array( 'ladditional_id' => $aid );

                            $ra = parent::update( 'lumonata_additional_field', $param, $where );

                            if( is_array( $ra ) )
                            {
                                $commit = 0;

                                break;
                            }
                        }
                    }
                }

                //-- SYNC room type
                if( isset( $this->post[ 'rooms' ] ) )
                {
                    foreach( $this->post[ 'rooms' ] as $tid => $room )
                    {
                        if( !empty( $room[ 'lname' ] ) && !empty( $room[ 'ladditional' ][ 'room_price' ] ) )
                        {
                            //-- UPDATE lumonata_post_terms
                            $param = array_diff_key( $room, array_flip( array( 'ladditional' ) ) );
                            $where = array( 'lterm_id' => $tid );

                            $result = parent::update( 'lumonata_post_terms', $param, $where );

                            if( is_array( $result ) )
                            {
                                $commit = 0;

                                break;
                            }

                            //-- SYNC lumonata_additional_field
                            if( isset( $room[ 'ladditional' ] ) )
                            {
                                foreach( $room[ 'ladditional' ] as $key => $value )
                                {
                                    if( is_array( $value ) )
                                    {
                                        $value = json_encode( $value );
                                    }

                                    //-- GET additional id & module id
                                    $mod = $this->global->getFields( 'lumonata_module', 'lmodule_id', array( 'lapps' => 'accommodation_room_type' ) );
                                    $aid = $this->global->getFields( 'lumonata_additional_field', 'ladditional_id', array( 'lterm_id' => $tid, 'ladditional_key' => $key, 'lmodule_id' => $mod ) );

                                    if( empty( $aid ) )
                                    {
                                        $ra = parent::insert( 'lumonata_additional_field', array(
                                            'ladditional_value' => $value,
                                            'ladditional_key'   => $key,
                                            'lmodule_id'        => $mod,
                                            'lterm_id'          => $tid,
                                        ) );

                                        if( is_array( $ra ) )
                                        {
                                            $commit = 0;

                                            break;
                                        }
                                    }
                                    else
                                    {
                                        $param = array( 'ladditional_value' => $value );
                                        $where = array( 'ladditional_id' => $aid );

                                        $ra = parent::update( 'lumonata_additional_field', $param, $where );

                                        if( is_array( $ra ) )
                                        {
                                            $commit = 0;

                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //-- SYNC addons
                if( isset( $this->post[ 'addons' ] ) )
                {
                    foreach( $this->post[ 'addons' ] as $tid => $addons )
                    {
                        if( !empty( $addons[ 'lname' ] ) && !empty( $addons[ 'ladditional' ][ 'addons_price' ] ) )
                        {
                            //-- UPDATE lumonata_post_terms
                            $param = array_diff_key( $addons, array_flip( array( 'ladditional' ) ) );
                            $where = array( 'lterm_id' => $tid );

                            $result = parent::update( 'lumonata_post_terms', $param, $where );

                            if( is_array( $result ) )
                            {
                                $commit = 0;

                                break;
                            }

                            //-- SYNC lumonata_additional_field
                            if( isset( $addons[ 'ladditional' ] ) )
                            {
                                foreach( $addons[ 'ladditional' ] as $key => $value )
                                {
                                    if( is_array( $value ) )
                                    {
                                        $value = json_encode( $value );
                                    }

                                    //-- GET additional id & module id
                                    $mod = $this->global->getFields( 'lumonata_module', 'lmodule_id', array( 'lapps' => 'accommodation_add_ons' ) );
                                    $aid = $this->global->getFields( 'lumonata_additional_field', 'ladditional_id', array( 'lterm_id' => $tid, 'ladditional_key' => $key, 'lmodule_id' => $mod ) );

                                    if( empty( $aid ) )
                                    {
                                        $ra = parent::insert( 'lumonata_additional_field', array(
                                            'ladditional_value' => $value,
                                            'ladditional_key'   => $key,
                                            'lmodule_id'        => $mod,
                                            'lterm_id'          => $tid,
                                        ) );

                                        if( is_array( $ra ) )
                                        {
                                            $commit = 0;

                                            break;
                                        }
                                    }
                                    else
                                    {
                                        $param = array( 'ladditional_value' => $value );
                                        $where = array( 'ladditional_id' => $aid );

                                        $ra = parent::update( 'lumonata_additional_field', $param, $where );

                                        if( is_array( $ra ) )
                                        {
                                            $commit = 0;

                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //-- SYNC schedule
                if( isset( $this->post[ 'schedules' ] ) )
                {
                    foreach( $this->post[ 'schedules' ] as $sid => $schedule )
                    {
                        //-- UPDATE lumonata_schedules
                        if( isset( $schedule[ 'lcheck_in' ] ) && isset( $schedule[ 'lcheck_out' ] ) )
                        {
                            $schedule[ 'lduration' ]  = $this->global->getNumDayNight( $schedule[ 'lcheck_in' ], $schedule[ 'lcheck_out' ] );
                            $schedule[ 'lcheck_out' ] = date( 'Y-m-d', strtotime( $schedule[ 'lcheck_out' ] ) );
                            $schedule[ 'lcheck_in' ]  = date( 'Y-m-d', strtotime( $schedule[ 'lcheck_in' ] ) );
                        }

                        //-- SET param
                        $param = array_diff_key( $schedule, array_flip( array( 'ladditional' ) ) );

                        //-- CHECK this new item or existing
                        $num = $this->global->getRowsNumber( 'lumonata_schedules', array( 'lschedule_id' => $sid ) );

                        if( $num > 0 )
                        {
                            $result = parent::update( 'lumonata_schedules', $param, array( 'lschedule_id' => $sid ) );
                        }
                        else
                        {
                            $result = parent::insert( 'lumonata_schedules', $param );
                        }

                        if( is_array( $result ) )
                        {
                            $commit = 0;

                            break;
                        }
                    }
                }

                //-- SYNC package
                if( isset( $this->post[ 'packages' ] ) )
                {
                    foreach( $this->post[ 'packages' ] as $pid => $package )
                    {
                        //-- UPDATE lumonata_packages
                        if( isset( $package[ 'ldate_start' ] ) && isset( $package[ 'ldate_end' ] ) )
                        {
                            $package[ 'ldate_end' ]   = date( 'Y-m-d', strtotime( $package[ 'ldate_end' ] ) );
                            $package[ 'ldate_start' ] = date( 'Y-m-d', strtotime( $package[ 'ldate_start' ] ) );
                        }

                        //-- SET param
                        $param = array_diff_key( $package, array_flip( array( 'ladditional' ) ) );

                        //-- CHECK this new item or existing
                        $num = $this->global->getRowsNumber( 'lumonata_packages', array( 'lpackage_id' => $pid ) );

                        if( $num > 0 )
                        {
                            $result = parent::update( 'lumonata_packages', $param, array( 'lpackage_id' => $pid ) );
                        }
                        else
                        {
                            $result = parent::insert( 'lumonata_packages', $param );
                        }

                        if( is_array( $result ) )
                        {
                            $commit = 0;

                            break;
                        }
                    }
                }

                if( $commit == 0 )
                {
                    parent::rollback();

                    $this->flash->add( array( 'type' => 'error', 'message' => 'Something wrong! Please try again', 'post' => $data ) );

                    header( 'location: ' . HT_SERVER . SITE_URL . '/agent/listings/edit/?' . http_build_query( $this->get ) );

                    exit;
                }
                else
                {
                    parent::commit();

                    $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully updated existing data' ) );

                    header( 'location: ' . HT_SERVER . SITE_URL . '/agent/listings/edit/?' . http_build_query( $this->get ) );

                    exit;
                }
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/agent/listings/edit/?' . http_build_query( $this->get ) );

            exit;
        }
    }

    public function upload_file()
    {
        if( isset( $_FILES[ 'attachment' ] ) && $_FILES[ 'attachment' ][ 'error' ] == 0 )
        {
            global $db;

            extract( $this->post );

            $g = new globalAdmin();
            $u = new upload();

            $file_name = $_FILES[ 'attachment' ][ 'name' ];
            $file_size = $_FILES[ 'attachment' ][ 'size' ];
            $file_type = $_FILES[ 'attachment' ][ 'type' ];
            $file_tmp  = $_FILES[ 'attachment' ][ 'tmp_name' ];

            $sef_img = $this->upload->file_name_filter( $file_name ) . '-' . time();
            $file    = $this->upload->rename_file( $file_name, $sef_img );

            $this->upload->upload_constructor( IMAGE_DIR . '/Uploads/' );

            if( $this->post[ 'file_type' ] == 'images' )
            {
                if( in_array( $file_type, array( 'image/jpg', 'image/jpeg', 'image/pjpeg', 'image/gif', 'image/png' ) ) )
                {
                    $this->upload->upload_resize( $file_name, $sef_img, $file_tmp, $file_type, 940, 940, 1 );
                }
                else
                {
                    return json_encode( array( 'result' => 'not-supported' ) );
                }
            }
            elseif( $this->post[ 'file_type' ] == 'word' && !in_array( $file_type, array( 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ) ) )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }
            elseif( $this->post[ 'file_type' ] == 'pdf' && $file_type != 'application/pdf' )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }
            elseif( $this->post[ 'file_type' ] == 'svg' && $file_type != 'image/svg+xml' )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }

            if( $this->upload->upload_file( $file_name, $sef_img, $file_tmp, 0 ) )
            {
                $order_id = $this->global->setCode( 'lumonata_attachment', 'lorder_id' );

                $r = $db->insert( 'lumonata_attachment', array(
                    'lmodule_id'    => $this->post[ 'module_id' ],
                    'lcreated_by'   => $this->sess[ 'username' ],
                    'lusername'     => $this->sess[ 'username' ],
                    'llang_id'      => $this->lang,
                    'lmimetype'     => $file_type,
                    'lorder_id'     => $order_id,
                    'lsef_url'      => $sef_img,
                    'lapp_id'       => $app_id,
                    'ltitle'        => $title,
                    'lattach'       => $file,
                    'lcreated_date' => time(),
                    'ldlu'          => time(),
                    'lstatus'       => 2
                ) );

                if( is_array( $r ) )
                {
                    $this->upload->delete_file_thumb( $file );

                    return json_encode( array( 'result' => 'failed' ) );
                }
                else
                {
                    $id = $db->insert_id();

                    if( $file_type == 'image/jpg' || $file_type == 'image/jpeg' || $file_type == 'image/pjpeg' || $file_type == 'image/gif' || $file_type == 'image/png' )
                    {
                        //-- If file type is an image show the thumbnail
                        $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=115&h=115&src=' . HT_SERVER . IMAGE_URL . '/Uploads/' . $file;
                    }
                    else
                    {
                        if( in_array( $file_type, array( 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ) ) )
                        {
                            //-- Word file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-doc.png';
                        }
                        elseif( $file_type == 'application/pdf' )
                        {
                            //-- PDF file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-pdf.png';
                        }
                        elseif( $file_type == 'image/svg+xml' )
                        {
                            //-- SVG file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-svg.png';
                        }
                        else
                        {
                            //-- Other file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-file.png';
                        }
                    }

                    return json_encode( array( 'result' => 'success', 'src' => $src, 'id' => $id ) );
                }
            }
            else
            {
                $this->upload->delete_thumb( $file );

                return json_encode( array( 'result' => 'failed' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'error' ) );
        }
    }

    public function delete_file()
    {
        if( isset( $this->post[ 'lattach_id' ] ) && !empty( $this->post[ 'lattach_id' ] ) )
        {
            $image = $this->global->getValueField( 'lumonata_attachment', 'lattach', 'lattach_id', $this->post[ 'lattach_id' ] );

            $result = parent::delete( 'lumonata_attachment', array( 'lattach_id' => $this->post[ 'lattach_id' ] ) );

            if( is_array( $result ) )
            {
                return json_encode( array( 'result' => 'error' ) );
            }
            else
            {
                if( empty( $image ) === false )
                {
                    $this->upload->upload_constructor( IMAGE_DIR . '/Uploads/' );
                    $this->upload->delete_file_thumb( $image );
                }

                return json_encode( array( 'result' => 'success' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
    }

    public function add_rooms()
    {
        $r = parent::insert( 'lumonata_post_terms', array(
            'lusername'     => $this->sess[ 'username' ],
            'lcreated_by'   => $this->sess[ 'username' ],
            'llang_id'      => $this->lang,
            'lcreated_date' => time(),
            'ldlu'          => time(),
            'lgroup'        => 'accommodation',
            'lrule'         => 'room_type',
            'lorder_id'     => 0,
            'lstatus'       => 2
        ) );

        if( !is_array( $r ) )
        {
            $mod = $this->global->getFields( 'lumonata_module', 'lmodule_id', array( 'lapps' => 'accommodation_room_type' ) );
            $tid = parent::insert_id();

            //-- ADD additional fields
            foreach( array( 'room_type_image', 'room_price', 'room_allotment' ) as $key )
            {
                parent::insert( 'lumonata_additional_field', array(
                    'ladditional_key' => $key,
                    'lterm_id'        => $tid,
                    'lmodule_id'      => $mod
                ) );
            }

            //-- ADD post relationship
            parent::insert( 'lumonata_post_relationship', array( 'lterm_id' => $tid, 'lpost_id' => $this->post[ 'post_id' ] ) );

            //-- SET content
            $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );
            $sign = $this->global->get_currency_sign( $this->post[ 'post_id' ] );

            $content = '
            <div id="file-wrap-' . $hash . '" data-id="' . $tid . '" data-module="' . $mod . '" class="eachroomupload flex">
                <div class="addfile_ addroomimage black_ wh-set" style="background-image:url(https://dummyimage.com/149x70/000000/000000);">
                    <a class="delimg"><img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del-w.svg"></a>
                    <div class="inner">
                        <strong>+</strong><br/>Upload Photo
                        <input type="hidden" class="rimage" name="rooms[' . $tid . '][ladditional][room_type_image]" value="" autocomplete="off">
                        <input type="file" name="file_' . $hash . '" data-field="room_type_image" data-hash="' . $hash . '" autocomplete="off" data-filetype="images" data-src="https://dummyimage.com/149x70/000000/000000">
                    </div>
                </div>
                <div class="cfield wh-set wh-set-medium">
                    <input type="hidden" name="rooms[' . $tid . '][lstatus]" value="1" required>
                    <input type="text" name="rooms[' . $tid . '][lname]" placeholder="Room Name" value="" required>
                </div>
                <div class="cfield wh-set">
                    <input type="text" name="rooms[' . $tid . '][ladditional][room_price]" class="text text-number" data-a-sign="' . $sign . '" autocomplete="off" data-a-sep="." data-a-dec="," data-m-dec="0" value="0">
                </div>
                <div class="cfield">
                    <div class="plumin">
                        <i class="plm min_">-</i>
                        <input type="hidden" name="rooms[' . $tid . '][ladditional][room_allotment]" value="1">
                        <span><b>1</b> room</span>
                        <i class="plm plu_">+</i>
                    </div>
                </div>
                <a href="javascript:;" class="delete" data-id="' . $tid . '" data-mod="' . $mod . '">
                    <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del.svg">
                </a>
            </div>';

            return $content;
        }
    }

    public function delete_rooms()
    {
        return $this->delete_terms();
    }

    public function add_schedule()
    {
        $r = parent::insert( 'lumonata_schedules', array(
            'lusername'     => $this->sess[ 'username' ],
            'lcreated_by'   => $this->sess[ 'username' ],
            'lpost_id'      => $this->post[ 'post_id' ],
            'llang_id'      => $this->lang,
            'lcheck_in'     => date( 'Y-m-d' ),
            'lcheck_out'    => date( 'Y-m-d' ),
            'lcreated_date' => time(),
            'ldlu'          => time(),
            'lorder_id'     => 0,
            'lstatus'       => 1
        ) );

        if( !is_array( $r ) )
        {
            $sign = $this->global->get_currency_sign( $this->post[ 'post_id' ] );
            $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );
            $sid  = parent::insert_id();

            //-- SET schedule content
            $content = '
            <div id="sc-wrap-' . $hash . '" data-id="' . $sid . '" class="eachscheduleupload flex">
                <div class="cfield wh-set dt-set">
                    <input type="text" name="schedules[' . $sid . '][lcheck_in]" class="text text-date check-in-date" placeholder="Departure Date" autocomplete="off" value="' . date( 'M d, Y' ) . '" required />
                </div>
                <div class="cfield wh-set dt-set">
                    <input type="text" name="schedules[' . $sid . '][lcheck_out]" class="text text-date check-out-date" placeholder="Return Date" autocomplete="off" value="' . date( 'M d, Y' ) . '" required />
                </div>
                <div class="cfield wh-set larger_">
                    <input type="text" name="schedules[' . $sid . '][ldeparture]" class="text" placeholder="Departure/Arrival" autocomplete="off" required />
                </div>
                <div class="cfield wh-set larger_">
                    <input type="hidden" name="schedules[' . $sid . '][lstatus]" value="1">
                    <input type="text" name="schedules[' . $sid . '][litinerary]" class="text" placeholder="Itinerary" autocomplete="off" required />
                </div>
                <div class="cfield wh-set smaller_">
                    <input type="hidden" name="schedules[' . $sid . '][lstatus]" value="1">
                    <input type="text" name="schedules[' . $sid . '][lprice]" class="text text-number" data-a-sign="' . $sign . '" autocomplete="off" data-a-sep="." data-a-dec="," data-m-dec="0" value="0">
                </div>
                <a href="javascript:;" class="delete" data-id="' . $sid . '">
                    <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del.svg">
                </a>
            </div>';

            return $content;
        }
    }

    public function add_package()
    {
        $r = parent::insert( 'lumonata_packages', array(
            'lusername'     => $this->sess[ 'username' ],
            'lcreated_by'   => $this->sess[ 'username' ],
            'lpost_id'      => $this->post[ 'post_id' ],
            'llang_id'      => $this->lang,
            'ldate_start'   => date( 'Y-m-d' ),
            'ldate_end'     => date( 'Y-m-d' ),
            'lcreated_date' => time(),
            'ldlu'          => time(),
            'lorder_id'     => 0,
            'lstatus'       => 1
        ) );

        if( !is_array( $r ) )
        {
            $sign = $this->global->get_currency_sign( $this->post[ 'post_id' ] );
            $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );
            $pid  = parent::insert_id();

            //-- SET package content
            $content = '
            <div id="sc-wrap-' . $hash . '" data-id="' . $pid . '" class="eachpackageupload flex flex-wrap">
                <div class="cfield wh-set wh_70_prc">
                    <input type="text" name="packages[' . $pid . '][lname]" class="text" placeholder="Name" autocomplete="off" required />
                </div>
                <div class="cfield wh-set larger_">
                    <select class="sl2_ package-type" name="packages[' . $pid . '][ltype]" autocomplete="off" required>
                        <option value="0">All time periods</option>
                        <option value="1">Use a date range</option>
                    </select>
                </div>
                <div class="cfield wh-set dt-set">
                    <input type="text" name="packages[' . $pid . '][ldate_start]" class="text text-date check-in-date" placeholder="Start Date" autocomplete="off" disabled required />
                </div>
                <div class="cfield wh-set dt-set">
                    <input type="text" name="packages[' . $pid . '][ldate_end]" class="text text-date check-out-date" placeholder="End Date" autocomplete="off" disabled required />
                </div>
                <div class="cfield wh-set xs_">
                    <input type="text" name="packages[' . $pid . '][lnight]" class="text text-number-night" value="2" autocomplete="off" data-a-sign=" nights" data-a-sep="." data-a-dec="," data-a-pos="s" data-m-dec="0" data-a-min="2" required />
                </div>
                <div class="cfield wh-set xs_">
                    <input type="hidden" name="packages[' . $pid . '][lstatus]" value="1">
                    <input type="text" name="packages[' . $pid . '][lprice]" class="text text-number" autocomplete="off" value="0" data-a-sign="' . $sign . '" data-a-sep="." data-a-dec="," data-a-pos="p" data-m-dec="0" required />
                </div>
                <div class="cfield" style="width:95%;">
                    <div class="tinymce-wrapper">
                        <textarea id="ldescription-' . $pid . '" class="textarea tinymce" name="packages[' . $pid . '][ldescription]" autocomplete="off"></textarea>
                    </div>
                </div>
                <a href="javascript:;" class="delete" data-id="' . $pid . '">
                    <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del.svg">
                </a>
            </div>';

            return $content;
        }
    }

    public function add_addons()
    {
        $r = parent::insert( 'lumonata_post_terms', array(
            'lusername'     => $this->sess[ 'username' ],
            'lcreated_by'   => $this->sess[ 'username' ],
            'llang_id'      => $this->lang,
            'lcreated_date' => time(),
            'ldlu'          => time(),
            'lrule'         => 'accommodation_add_ons',
            'lgroup'        => 'accommodation',
            'lorder_id'     => 0,
            'lstatus'       => 2
        ) );

        if( !is_array( $r ) )
        {
            $mod = $this->global->getFields( 'lumonata_module', 'lmodule_id', array( 'lapps' => 'accommodation_add_ons' ) );
            $tid = parent::insert_id();

            //-- ADD additional fields
            foreach( array( 'accommodation_add_ons_image', 'addons_price', 'addons_type_of_price', 'accommodation_add_ons_brief' ) as $key )
            {
                parent::insert( 'lumonata_additional_field', array(
                    'ladditional_key' => $key,
                    'lterm_id'        => $tid,
                    'lmodule_id'      => $mod
                ) );
            }

            //-- ADD post relationship
            parent::insert( 'lumonata_post_relationship', array( 'lterm_id' => $tid, 'lpost_id' => $this->post[ 'post_id' ] ) );

            //-- SET content
            $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );
            $sign = $this->global->get_currency_sign( $this->post[ 'post_id' ] );

            $content = '
            <div id="file-wrap-' . $hash . '" data-id="' . $tid . '" data-module="' . $mod . '" class="eachaddonsupload flex">
                <div class="cfield wh-set larger_">
                    <input type="hidden" name="addons[' . $tid . '][lstatus]" value="1">
                    <input type="text" name="addons[' . $tid . '][lname]" placeholder="Add-ons item name">
                </div>
                <div class="cfield wh-set wh-set-textarea">
                    <textarea name="addons[' . $tid . '][ladditional][accommodation_add_ons_brief]" placeholder="Short description"></textarea>
                </div>
                <div class="cfield wh-set smaller_">
                    <input type="text" name="addons[' . $tid . '][ladditional][addons_price]" class="text text-number" data-a-sign="' . $sign . '" autocomplete="off" data-a-sep="." data-a-dec="," data-m-dec="0" value="0">
                </div>
                <div class="cfield wh-set xs_">
                    <select name="addons[' . $tid . '][ladditional][addons_type_of_price]" id="" class="sl2_">
                        <option value="0">Per Item</option>
                        <option value="1">Per Hour</option>
                    </select>
                </div>
                <div class="addfile_ addaddonsimage smaller_ black_ wh-set" style="background-image:url(https://dummyimage.com/110x70/000000/000000);">
                    <a class="delimg"><img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del-w.svg"></a>
                    <div class="inner">
                        <strong>+</strong><br/>Upload Photo
                        <input type="hidden" class="rimage" name="addons[' . $tid . '][ladditional][accommodation_add_ons_image]" value="" autocomplete="off">
                        <input type="file" name="file_' . $hash . '" data-field="accommodation_add_ons_image" data-hash="' . $hash . '" autocomplete="off" data-filetype="images" data-src="https://dummyimage.com/110x70/000000/000000">
                    </div>
                </div>
                <a href="javascript:;" class="delete closer_" data-id="' . $tid . '" data-mod="' . $mod . '">
                    <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del.svg" alt="">
                </a>
            </div>';

            return $content;
        }
    }

    public function delete_schedule()
    {
        $s = 'DELETE FROM lumonata_schedules WHERE lschedule_id = %d';
        $q = parent::prepare_query( $s, $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            return json_encode( array( 'result' => 'success' ) );
        }
    }

    public function delete_package()
    {
        $s = 'DELETE FROM lumonata_packages WHERE lpackage_id = %d';
        $q = parent::prepare_query( $s, $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            return json_encode( array( 'result' => 'success' ) );
        }
    }

    public function delete_addons()
    {
        return $this->delete_terms();
    }

    public function add_activity()
    {
        $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );

        $content = '
        <div id="file-wrap-' . $hash . '" class="eachactivityupload flex">
            <div class="cfield wh-set larger_">
                <input type="text" name="fields[ladditional][things_to_do][' . $hash . '][to_do_title]" placeholder="Activity name">
            </div>
            <div class="cfield wh-set larger_ more_">
                <textarea name="fields[ladditional][things_to_do][' . $hash . '][to_do_desc]" placeholder="Short description"></textarea>
            </div>                
            <div class="addfile_ addactivityimage smaller_ black_ wh-set" style="background-image:url(https://dummyimage.com/110x70/000000/000000);">
                <a class="delimg"><img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del-w.svg"></a>
                <div class="inner">
                    <strong>+</strong><br/>Upload Photo
                    <input type="hidden" class="rimage" name="fields[ladditional][things_to_do][' . $hash . '][to_do_img]" value="" autocomplete="off">
                    <input type="file" name="file_' . $hash . '" data-field="to_do_img" data-hash="' . $hash . '" autocomplete="off" data-filetype="images" data-src="https://dummyimage.com/110x70/000000/000000">
                </div>
            </div>
            <a href="javascript:;" class="delete closer_">
                <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del.svg">
            </a>
        </div>';

        return $content;
    }

    public function add_surf_info()
    {
        $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );

        $content = '
        <div id="file-wrap-' . $hash . '" class="eachsurfinfo flex">
            <div class="inner">
                <input type="text" name="fields[ladditional][surfing_info][' . $hash . '][surf_info_title]" placeholder="Surf spot">
                <textarea name="fields[ladditional][surfing_info][' . $hash . '][surf_info_desc]" id="" cols="30" rows="10"></textarea>
            </div>
            <a href="javascript:;" class="delete closer_">
                <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del.svg">
            </a>
        </div>';

        return $content;
    }

    public function delete_terms()
    {
        parent::begin();

        $commit = 1;

        $s = 'DELETE FROM lumonata_post_terms WHERE lterm_id = %d';
        $q = parent::prepare_query( $s, $this->post[ 'id' ] );
        $r = parent::query( $q );

        if( is_array( $r ) )
        {
            $commit = 0;
        }
        else
        {
            $s = 'SELECT * FROM lumonata_attachment AS a WHERE a.lapp_id = %d AND a.lmodule_id = %d';
            $q = parent::prepare_query( $s, $this->post[ 'id' ], $this->post[ 'mod' ] );
            $r = parent::query( $q );

            while( $d = parent::fetch_array( $r ) )
            {
                $r2 = parent::delete( 'lumonata_attachment', array( 'lattach_id' => $d[ 'lattach_id' ] ) );

                if( is_array( $r2 ) )
                {
                    $commit = 0;

                    break;
                }
                else
                {
                    if( empty( $d[ 'lattach' ] ) === false )
                    {
                        $this->upload->upload_constructor( IMAGE_DIR . '/Uploads/' );
                        $this->upload->delete_file_thumb( $d[ 'lattach' ] );
                    }
                }
            }
        }

        if( $commit == 0 )
        {
            parent::rollback();

            return json_encode( array( 'result' => 'failed' ) );
        }
        else
        {
            parent::commit();

            return json_encode( array( 'result' => 'success' ) );
        }
    }

    public function sef_url( $string = '', $table = 'lumonata_post', $nfield = 'ltitle', $sfield = 'lsef_url' )
    {
        $num = $this->global->getNumRows( $table, $nfield, $string );

        if( $num > 0 )
        {
            for( $i = 2; $i <= $num + 1; $i++ )
            {
                $sef = $this->global->sef_url( $string, $table, $nfield, $sfield ) . '-' . $i;

                if( $this->global->getNumRows( $table, $sfield, $sef ) < 1 )
                {
                    $sef = $sef;

                    break;
                }
            }
        }
        else
        {
            $sef = $this->global->sef_url( $string, $table, $nfield, $sfield );
        }

        return $sef;
    }

    public function sef_title( $string = '', $table = 'lumonata_post', $sfield = 'lsef_url' )
    {
        $sef = $this->global->sef_url( $string, $table, $sfield );

        $s = 'SELECT COUNT(a.lpost_id) AS num FROM lumonata_post AS a WHERE a.lsef_url LIKE %s';
        $q = parent::prepare_query( $s, '%' . $sef . '%' );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $d = parent::fetch_assoc( $r );

            if( $d['num'] > 1 );
            {
                $sef = $sef . '-' . time();
            }
        }

        return $sef;
    }

    public function translation_on_post( $lref_id )
    {
        $s = 'SELECT a.llang_id FROM lumonata_post AS a WHERE a.lref_id IS NOT NULL AND a.lref_id = %d ORDER BY a.lorder_id';
        $q = parent::prepare_query( $s, $lref_id );
        $r = parent::query( $q );

        $translation = array();

        if( parent::num_rows( $r ) > 0 )
        {
            while( $d = parent::fetch_assoc( $r ) )
            {
                array_push( $translation, $d[ 'llang_id' ] );
            }
        }

        return $translation;
    }

    public function switcher_language( $llang_id, $lref_id, $prm = array() )
    {
        require_once( INCLUDE_DIR . '/vendor/autoload.php' );

        $s = 'SELECT * FROM lumonata_language AS a WHERE a.lstatus = 1 ORDER BY a.lorder_id';
        $q = parent::prepare_query( $s );
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $plang = $this->global->getFields( 'lumonata_post', 'llang_id', array( 'lpost_id' => $lref_id ) );
            $dlang = $this->global->getSettingValue( 'llanguage' );
            $trans = $this->translation_on_post( $lref_id );

            $option   = array();
            $selected = '';

            if( isset( $prm[ 'lang' ] ) )
            {
                $lang = $prm[ 'lang' ];
            }
            else
            {
                $lang = $dlang;
            }

            while( $d = parent::fetch_assoc( $r ) )
            {
                try
                {
                    $c = country( $d[ 'lcountry_code' ] );

                    if( in_array( $d[ 'llang_id' ], $trans ) )
                    {
                        $pid = $this->global->getFields( 'lumonata_post', 'lpost_id', array( 'lref_id' => $lref_id, 'llang_id' => $d[ 'llang_id' ] ) );

                        $link = sprintf( '//%s/agent/listings/edit/?%s', SITE_URL, http_build_query( array_merge( $prm, array( 'id' => $pid, 'lang' => $d[ 'llang_id' ] ) ) ) );
                    }
                    elseif( $plang == $d[ 'llang_id' ] )
                    {
                        $link = sprintf( '//%s/agent/listings/edit/?%s', SITE_URL, http_build_query( array_merge( $prm, array( 'id' => $lref_id, 'lang' => $d[ 'llang_id' ] ) ) ) );
                    }
                    else
                    {
                        $link = sprintf( '//%s/agent/listings/add/?%s', SITE_URL, http_build_query( array_merge( $prm, array( 'ref' => $lref_id, 'lang' => $d[ 'llang_id' ] ) ) ) );
                    }

                    if( $lang == $d[ 'llang_id' ] || $dlang == $d[ 'llang_id' ] )
                    {
                        $selected = sprintf( '<b><i class="sl-flag"><img src="data:image/svg+xml;base64,%s" alt="" width="24" /></i><span>%s</span></b>', base64_encode( $c->getFlag() ), $d[ 'llanguage' ] );
                    }

                    $option[] = sprintf( '<li><a href="%s"><i class="sl-flag"><img src="data:image/svg+xml;base64,%s" alt="" width="24" /></i><span>%s</span></a></li>', $link, base64_encode( $c->getFlag() ), $d[ 'llanguage' ] );
                }
                catch( Exception $e )
                {
                    //-- do nothing
                }
            }

            if( empty( $option ) === false )
            {
                return '
                <div class="switch-language">
                    <div class="sl-nav">
                        <ul>
                            <li>
                                ' . $selected . '
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                <div class="triangle"></div>
                                <ul>' . implode( "\n", $option ) . '</ul>
                            </li>
                        </ul>
                    </div>
                </div>';
            }
        }
    }

    public function language()
    {
        if( isset( $_GET[ 'lang' ] ) && $_GET[ 'lang' ] != '' )
        {
            $this->lang = $_GET[ 'lang' ];
        }
        else
        {
            $this->lang = $this->global->getSettingValue( 'llanguage' );
        }
    }

    public function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();
        $this->ref     = null;
        $this->valid   = true;

        if( empty( $_GET ) )
        {
            $this->get = array();
        }
        else
        {
            $this->get = $_GET;
        }

        if( isset( $this->get[ 'id' ] ) )
        {
            $s = 'SELECT * FROM lumonata_post AS a WHERE a.lpost_id = %d';
            $q = parent::prepare_query( $s, $this->get[ 'id' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $d = parent::fetch_array( $r );

                $fields = array(
                    'lcreated_by'   => $d[ 'lcreated_by' ],
                    'lusername'     => $d[ 'lusername' ],
                    'llang_id'      => $d[ 'llang_id' ],
                    'lpost_id'      => $d[ 'lpost_id' ],
                    'lref_id'       => $d[ 'lref_id' ],
                    'lcreated_date' => $d[ 'lcreated_date' ],
                    'ldlu'          => $d[ 'ldlu' ],
                    'ltitle'        => $d[ 'ltitle' ],
                    'ldescription'  => $d[ 'ldescription' ],
                    'lmeta_title'   => $d[ 'lmeta_title' ],
                    'lmeta_key'     => $d[ 'lmeta_key' ],
                    'lmeta_desc'    => $d[ 'lmeta_desc' ],
                    'limage'        => $d[ 'limage' ],
                    'lsef_url'      => $d[ 'lsef_url' ],
                    'lorder_id'     => $d[ 'lorder_id' ],
                    'lparent_id'    => $d[ 'lparent_id' ],
                    'ltype'         => $d[ 'ltype' ],
                    'lstatus'       => $d[ 'lstatus' ]
                );

                if( is_null( $d[ 'lref_id' ] ) )
                {
                    $this->ref = $d[ 'lpost_id' ];
                }
                else
                {
                    $this->ref = $d[ 'lref_id' ];
                }

                //-- GET post additional fields
                $fields[ 'ladditional' ] = array(
                    'accommodation_address' => '',
                    'accommodation_country' => '',
                    'accommodation_state'   => '',
                    'accommodation_city'    => '',
                    'closer_airport_name'   => '',
                    'closer_airport_city'   => '',
                    'arrival_by_plane'      => '',
                    'arrival_by_train'      => '',
                    'arrival_by_boat'       => '',
                    'arrival_by_bus'        => '',
                    'map_coordinate'        => '',
                    'post_code'             => '',
                );

                $s = 'SELECT a.ladditional_value, a.ladditional_key FROM lumonata_additional_field AS a WHERE a.lapp_id = %d';
                $q = parent::prepare_query( $s, $this->get[ 'id' ] );
                $r = parent::query( $q );

                if( parent::num_rows( $r ) > 0 )
                {
                    while( $d = parent::fetch_array( $r ) )
                    {
                        $fields[ 'ladditional' ][ $d[ 'ladditional_key' ] ] = $d[ 'ladditional_value' ];
                    }
                }
            }
            else
            {
                $this->valid = false;
            }

            //-- GET post term relationship
            $s = 'SELECT a.lterm_id, b.lrule FROM lumonata_post_relationship AS a LEFT JOIN lumonata_post_terms AS b ON a.lterm_id = b.lterm_id WHERE a.lpost_id = %d';
            $q = parent::prepare_query( $s, $this->get[ 'id' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                while( $d = parent::fetch_array( $r ) )
                {
                    $terms[ $d[ 'lrule' ] ][] = $d[ 'lterm_id' ];
                }
            }
            else
            {
                $terms = array(
                    'accommodation_add_ons' => '',
                    'accommodation_type'    => '',
                    'surf_trip'             => '',
                    'room_type'             => ''
                );
            }
        }
        else
        {
            if( isset( $_GET[ 'ref' ] ) )
            {
                $this->ref = $_GET[ 'ref' ];
            }

            $fields = array(
                'lcreated_by'   => $this->sess[ 'username' ],
                'lusername'     => $this->sess[ 'username' ],
                'llang_id'      => $this->lang,
                'lref_id'       => $this->ref,
                'lpost_id'      => time(),
                'lcreated_date' => time(),
                'ldlu'          => time(),
                'ltype'         => 'accommodation',
                'ltitle'        => '',
                'ldescription'  => '',
                'lmeta_title'   => '',
                'lmeta_key'     => '',
                'lmeta_desc'    => '',
                'limage'        => '',
                'lsef_url'      => '',
                'lorder_id'     => 0,
                'lparent_id'    => 0,
                'lstatus'       => 0,
                'ladditional'   => array(
                    'page_template' => '',
                    'title_part_1'  => '',
                    'title_part_2'  => '',
                    'title_part_3'  => '',
                    'title_part_4'  => '',
                    'fb_link'       => '',
                    'website'       => '',
                    'commission'    => '',
                    'price_type'    => '',
                    'policy'        => ''
                )
            );

            $terms = array(
                'accommodation_add_ons' => '',
                'accommodation_type'    => '',
                'surf_trip'             => '',
                'room_type'             => ''
            );
        }

        if( empty( $_POST ) )
        {
            if( empty( $this->notif ) === false && isset( $this->notif[ 'post' ] ) )
            {
                $this->fields = array_merge( $fields, $this->notif[ 'post' ] );
            }
            else
            {
                $this->fields = $fields;
            }

            $this->terms = $terms;

            $this->post = array();
        }
        else
        {
            if( isset( $_POST[ 'fields' ] ) )
            {
                $this->fields = array_merge( $fields, $_POST[ 'fields' ] );
            }
            else
            {
                $this->fields = $fields;
            }

            if( isset( $_POST[ 'terms' ] ) )
            {
                $this->terms = array_merge( $terms, $_POST[ 'terms' ] );
            }
            else
            {
                $this->terms = $terms;
            }

            $this->post = array_diff_key( $_POST, array_flip( array( 'fields', 'terms' ) ) );
        }
    }

    public function terms_data( $conditions = array() )
    {
        //-- PREPARE parameter
        $w = array();

        if( is_array( $conditions ) && empty( $conditions ) === false )
        {
            foreach( $conditions as $field => $value )
            {
                if( !preg_match( '/(<|>|!|=|\sIS NULL|\sIS NOT NULL|\sEXISTS|\sBETWEEN|\sLIKE|\sIN\s*\(|\s)/i', trim( $field ) ) )
                {
                    $field .= ' = ';
                }

                $w[] = parent::prepare_query( $field . '%s', $value );
            }
        }

        if( empty( $w ) === false )
        {
            $where = ' WHERE '. implode( ' AND ', $w );
        }
        else
        {
            $where = '';
        }

        $q = 'SELECT a.lterm_id, a.lname FROM lumonata_post_terms AS a' . $where;
        $r = parent::query( $q );

        if( parent::num_rows( $r ) > 0 )
        {
            $option = array();

            while( $d = parent::fetch_array( $r ) )
            {
                $option[ $d[ 'lterm_id' ] ] = $d[ 'lname' ];
            }

            return $option;
        }
    }

    public function get_accommodation_image( $data = '' )
    {
        $attach = $this->global->getValueField( 'lumonata_attachment', 'lattach', 'lattach_id', $data );
        $hash   = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );

        if( empty( $attach ) || ( !empty( $attach ) && !file_exists( IMAGE_DIR . '/Uploads/' . $attach ) ) )
        {
            $content = '
            <div id="file-wrap-' . $hash . '" class="imguploadpreview imgsingleupload flex">
                <figure>
                    <img src="https://dummyimage.com/286x143/c1c1c1/ffffff" alt="" class="cover">
                    <a href="javascript:;" class="delimg hidden" data-id="">
                        <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del-w.svg">
                    </a>
                </figure>
                <a href="javascript:;" class="chgimg lineyellowbtn">
                    <span>Change Photo</span>
                    <input type="hidden" name="fields[ladditional][accommodation_image]" value="" autocomplete="off">
                    <input type="file" name="file_' . $hash . '" data-field="accommodation_image" data-hash="' . $hash . '" autocomplete="off" data-filetype="images" data-src="https://dummyimage.com/286x143/c1c1c1/ffffff">
                </a>
            </div>';
        }
        else
        {
            $content = '
            <div id="file-wrap-' . $hash . '" class="imguploadpreview imgsingleupload flex">
                <figure>
                    <img src="' . HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=286&h=143&src=' . HT_SERVER . SITE_URL . '/images/Uploads/' . $attach . '" alt="" class="cover">
                    <a href="javascript:;" class="delimg" data-id="' . $data . '">
                        <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del-w.svg">
                    </a>
                </figure>
                <a href="javascript:;" class="chgimg lineyellowbtn">
                    <span>Change Photo</span>
                    <input type="hidden" name="fields[ladditional][accommodation_image]" value="' . $data . '" autocomplete="off">
                    <input type="file" name="file_' . $hash . '" data-field="accommodation_image" data-hash="' . $hash . '" autocomplete="off" data-filetype="images" data-src="https://dummyimage.com/286x143/c1c1c1/ffffff">
                </a>
            </div>';
        }

        return $content;
    }

    public function get_accommodation_gallery( $data = '' )
    {
        $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );
        $mod  = $this->global->getModuleID( 'accommodation' );
        $data = json_decode( $data, true );

        $items = '';

        if( $data !== null && json_last_error() === JSON_ERROR_NONE )
        {
            foreach( $data as $attach_id )
            {
                $attach = $this->global->getValueField( 'lumonata_attachment', 'lattach', 'lattach_id', $attach_id );

                if( !empty( $attach ) && file_exists( IMAGE_DIR . '/Uploads/' . $attach ) )
                {
                    $items .= ' 
                    <figure class="item">
                        <input type="hidden" name="fields[ladditional][accommodation_gallery][]" value="' . $attach_id . '" autocomplete="off">
                        <img src="' . HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=100&h=100&src=' . HT_SERVER . SITE_URL . '/images/Uploads/' . $attach . '" class="cover" alt="" />
                        <a href="javascript:;" class="delimg" data-id="' . $attach_id . '">
                            <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del-w.svg">
                        </a>
                    </figure>';
                }
            }
        }

        $content = '
        <div class="imguploadpreview imgbatchupload small_ flex">
            <div id="file-drop-zone-' . $hash . '" class="file-drop-zone">
                ' . $items . '
                <a class="addfile_ addbatchimage" data-hash="' . $hash . '" data-field="accommodation_gallery" data-fancybox data-type="iframe" href="' . HT_SERVER . SITE_URL . '/agent/upload-media/?id=' . $this->get[ 'id' ] . '&mod=' . $mod . '">
                    <strong>+</strong>
                    UPLOAD MORE<br>PHOTOS
                </a>
            </div>           
            <div id="cloned-wrapp-' . $hash . '" class="cloned-wrapp hidden">
                <figure class="item">
                    <input type="hidden" autocomplete="off">
                    <img src="https://dummyimage.com/286x143/c1c1c1/ffffff" alt="" class="cover">
                    <a href="javascript:;" class="delimg">
                        <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del-w.svg">
                    </a>
                </figure>
            </div>
        </div>';

        return $content;
    }

    public function get_accommodation_rooms( $post_id, $id = array() )
    {
        $template = $this->global->getFields( 'lumonata_additional_field', 'ladditional_value', array( 'ladditional_key' => 'page_template', 'lapp_id' => $post_id ) );

        if( in_array( $template, array( '0', '1', '2' ) ) )
        {
            //-- SET label and title
            if( $template == '1' )
            {
                $label = 'cabin';
                $title = 'Cabins';
            }
            else
            {
                $label = 'room';
                $title = 'Rooms';
            }

            if( empty( $id ) )
            {
                $content = '
                <div id="section-rooms" class="cfield">
                    <label><strong>' . $title . '</strong><br/>Manage all ' . $label . 's in this accommodation</label>
                    <div class="roomsupload">
                        <div class="cfield-wrap" data-note="You can manage ' . $label . ' by click the link below">
                            <p class="note">You can manage ' . $label . ' by click the link below</p>
                        </div>
                        <a href="javascript:;" class="admmorebtn admmoreroom">+ add more ' . $label . '</a>
                    </div>
                </div>';

                return $content;
            }
            else
            {
                $r = parent::query( 'SELECT * FROM lumonata_post_terms AS a WHERE a.lterm_id IN(' . implode( ',', $id ) . ')' );

                if( parent::num_rows( $r ) > 0 )
                {
                    $items = '';

                    while( $d = parent::fetch_assoc( $r ) )
                    {
                        $ra = $this->global->getFields( 'lumonata_additional_field', '*', array( 'lterm_id' => $d[ 'lterm_id' ] ), false );

                        if( parent::num_rows( $ra ) > 0 )
                        {
                            while( $da = parent::fetch_array( $ra ) )
                            {
                                $d[ 'ladditional' ][ $da[ 'ladditional_key' ] ] = $da[ 'ladditional_value' ];
                            }
                        }

                        $attc = $this->global->getFields( 'lumonata_attachment', 'lattach', array( 'lattach_id' => $d[ 'ladditional' ][ 'room_type_image' ] ) );
                        $mod  = $this->global->getFields( 'lumonata_module', 'lmodule_id', array( 'lapps' => 'accommodation_room_type' ) );
                        $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );
                        $sign = $this->global->get_currency_sign( $post_id );

                        if( empty( $attc ) || ( !empty( $attc ) && !file_exists( IMAGE_DIR . '/Uploads/' . $attc ) ) )
                        {
                            $bgimg = 'https://dummyimage.com/149x70/000000/000000';
                            $class = 'eachroomupload flex';
                        }
                        else
                        {
                            $bgimg = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=149&h=70&src=' . HT_SERVER . SITE_URL . '/images/Uploads/' . $attc;
                            $class = 'eachroomupload eachroomuploadwithimage flex';
                        }

                        $alot  = empty( $d[ 'ladditional' ][ 'room_allotment' ] ) ? 1 : $d[ 'ladditional' ][ 'room_allotment' ];
                        $price = empty( $d[ 'ladditional' ][ 'room_price' ] ) ? 0 : $d[ 'ladditional' ][ 'room_price' ];

                        if( $alot > 1 )
                        {
                            $rlabel = '<span><b>' . $alot . '</b> rooms</span>';
                        }
                        else
                        {
                            $rlabel = '<span><b>' . $alot . '</b> room</span>';
                        }

                        $items .= '
                        <div id="file-wrap-' . $hash . '" data-id="' . $d[ 'lterm_id' ] . '" data-module="' . $mod . '" class="' . $class . '">
                            <div class="addfile_ addroomimage black_ wh-set" style="background-image:url(' . $bgimg . ');">
                                <a class="delimg" data-id="' . $d[ 'ladditional' ][ 'room_type_image' ] . '">
                                    <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del-w.svg">
                                </a>
                                <div class="inner">
                                    <strong>+</strong><br/>Upload Photo
                                    <input type="hidden" class="rimage" name="rooms[' . $d[ 'lterm_id' ] . '][ladditional][room_type_image]" value="' . $d[ 'ladditional' ][ 'room_type_image' ] . '" autocomplete="off">
                                    <input type="file" name="file_' . $hash . '" data-field="room_type_image" data-hash="' . $hash . '" autocomplete="off" data-filetype="images" data-src="https://dummyimage.com/149x70/000000/000000">
                                </div>
                            </div>
                            <div class="cfield wh-set wh-set-medium">
                                <input type="hidden" name="rooms[' . $d[ 'lterm_id' ] . '][lstatus]" value="1" required>
                                <input type="text" name="rooms[' . $d[ 'lterm_id' ] . '][lname]" placeholder="Room Name" value="' . $d[ 'lname' ] . '" required>
                            </div>
                            <div class="cfield wh-set">
                                <input type="text" name="rooms[' . $d[ 'lterm_id' ] . '][ladditional][room_price]" class="text text-number" data-a-sign="' . $sign . '" autocomplete="off" data-a-sep="." data-a-dec="," data-m-dec="0" value="' . $price . '">
                            </div>
                            <div class="cfield">
                                <div class="plumin">
                                    <i class="plm min_">-</i>
                                    <input type="hidden" name="rooms[' . $d[ 'lterm_id' ] . '][ladditional][room_allotment]" value="' . $alot . '">
                                    ' . $rlabel . '
                                    <i class="plm plu_">+</i>
                                </div>
                            </div>
                            <a href="javascript:;" class="delete" data-id="' . $d[ 'lterm_id' ] . '" data-mod="' . $mod . '">
                                <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del.svg" alt="">
                            </a>
                        </div>';
                    }

                    $content = '
                    <div id="section-rooms" class="cfield">
                        <label><strong>' . $title . '</strong><br/>Manage all ' . $label . 's in this accommodation</label>
                        <div class="roomsupload">
                            <div class="cfield-wrap" data-note="You can manage ' . $label . ' by click the link below">
                                ' . $items . '
                            </div>
                            <a href="javascript:;" class="admmorebtn admmoreroom">+ add more ' . $label . '</a>
                        </div>
                    </div>';

                    return $content;
                }
            }
        }
    }

    public function get_accommodation_venue( $post_id )
    {
        $template = $this->global->getFields( 'lumonata_additional_field', 'ladditional_value', array( 'ladditional_key' => 'page_template', 'lapp_id' => $post_id ) );

        if( in_array( $template, array( '3' ) ) )
        {
            $sign  = $this->global->get_currency_sign( $post_id );
            $price = 0;

            if( isset( $data[ 'room_price' ] ) && $data[ 'room_price' ] != '' )
            {
                $price = $data[ 'room_price' ];
            }

            $content = '            
            <div id="section-venue" class="cfield">
                <label><strong>Venue</strong></label>
                <div class="cfield wh-set auto">
                    <span class="label">Price per night</span>
                    <input type="text" name="fields[ladditional][room_price]" class="text text-number" data-a-sign="' . $sign . '" autocomplete="off" data-a-sep="." data-a-dec="," data-m-dec="0" value="' . $price . '" required />
                </div>
            </div>';

            return $content;
        }
    }

    public function get_accommodation_package( $post_id )
    {
        $template = $this->global->getFields( 'lumonata_additional_field', 'ladditional_value', array( 'ladditional_key' => 'page_template', 'lapp_id' => $post_id ) );

        if( in_array( $template, array( '0', '2', '3' ) ) )
        {
            //-- GET schedule
            $s = 'SELECT * FROM lumonata_packages AS a WHERE a.lpost_id = %d AND a.llang_id = %d';
            $q = parent::prepare_query( $s, $post_id, $this->lang );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $sign = $this->global->get_currency_sign( $post_id );
                $item = '';

                while( $d = parent::fetch_array( $r ) )
                {
                    //-- SET content
                    $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );

                    if( $d[ 'ltype' ] == 0 )
                    {
                        $start = '';
                        $end   = '';
                        $attr  = 'disabled';
                        $night = 0;
                    }
                    else
                    {
                        $start = empty( $d[ 'ldate_start' ] ) ? '' : date( 'M d, Y', strtotime( $d[ 'ldate_start' ] ) );
                        $end   = empty( $d[ 'ldate_end' ] ) ? '' : date( 'M d, Y', strtotime( $d[ 'ldate_end' ] ) );
                        $attr  = '';
                        $night = 2;
                    }

                    $item .= '
                    <div id="sc-wrap-' . $hash . '" data-id="' . $d[ 'lpackage_id' ] . '" class="eachpackageupload flex flex-wrap">
                        <div class="cfield wh-set wh_70_prc">
                            <input type="text" name="packages[' . $d[ 'lpackage_id' ] . '][lname]" class="text" placeholder="Name" value="' . $d[ 'lname' ] . '" autocomplete="off" required />
                        </div>
                        <div class="cfield wh-set larger_">
                            <select class="sl2_ package-type" name="packages[' . $d[ 'lpackage_id' ] . '][ltype]" autocomplete="off" required>
                                <option value="0"' . ( $d[ 'ltype' ] == 0 ? ' selected=""' : '' ) . '>All time periods</option>
                                <option value="1"' . ( $d[ 'ltype' ] == 1 ? ' selected=""' : '' ) . '>Use a date range</option>
                            </select>
                        </div>
                        <div class="cfield wh-set dt-set">
                            <input type="text" name="packages[' . $d[ 'lpackage_id' ] . '][ldate_start]" class="text text-date check-in-date" placeholder="Start Date" value="' . $start . '" ' . $attr . ' autocomplete="off" required />
                        </div>
                        <div class="cfield wh-set dt-set">
                            <input type="text" name="packages[' . $d[ 'lpackage_id' ] . '][ldate_end]" class="text text-date check-out-date" placeholder="End Date" value="' . $end . '" ' . $attr . ' autocomplete="off" required />
                        </div>
                        <div class="cfield wh-set xs_">
                            <input type="text" name="packages[' . $d[ 'lpackage_id' ] . '][lnight]" class="text text-number-night" ' . $attr . ' autocomplete="off" value="' . $d[ 'lnight' ] . '" data-a-sign=" nights" data-a-sep="." data-a-dec="," data-a-pos="s" data-m-dec="0" data-a-min="' . $night . '" required />
                        </div>
                        <div class="cfield wh-set xs_">
                            <input type="hidden" name="packages[' . $d[ 'lpackage_id' ] . '][lstatus]" value="1">
                            <input type="text" name="packages[' . $d[ 'lpackage_id' ] . '][lprice]" class="text text-number" autocomplete="off" value="' . $d[ 'lprice' ] . '" data-a-sign="' . $sign . '" data-a-sep="." data-a-dec="," data-a-pos="p" data-m-dec="0" required />
                        </div>
                        <div class="cfield" style="width:95%;">
                            <div class="tinymce-wrapper">
                                <textarea id="ldescription-' . $d[ 'lpackage_id' ] . '" class="textarea tinymce" name="packages[' . $d[ 'lpackage_id' ] . '][ldescription]" autocomplete="off">' . $d[ 'ldescription' ] . '</textarea>
                            </div>
                        </div>
                        <a href="javascript:;" class="delete" data-id="' . $d[ 'lpackage_id' ] . '">
                            <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del.svg">
                        </a>
                    </div>';
                }

                //-- SET packages content
                $content = '
                <div id="section-package" class="cfield">
                    <label><strong>Packages</strong><br/>You can set plans for all time periods or within a specific date range</label>
                    <div class="mepetfield">
                        <div class="cfield-wrap" data-note="You can manage package by click the link below">
                            ' . $item . '
                        </div>
                        <a href="javascript:;" class="admmorebtn admmorepackage">+ add more package</a>
                    </div>
                </div>';

                return $content;
            }
            else
            {
                //-- SET packages content
                $content = '
                <div id="section-package" class="cfield">
                    <label><strong>Packages</strong><br/>You can set plans for all time periods or within a specific date range</label>
                    <div class="mepetfield">
                        <div class="cfield-wrap" data-note="You can manage package by click the link below">
                            <p class="note">You can manage package by click the link below</p>
                        </div>
                        <a href="javascript:;" class="admmorebtn admmorepackage">+ add more package</a>
                    </div>
                </div>';

                return $content;
            }
        }
    }

    public function get_accommodation_schedule( $post_id )
    {
        $template   = $this->global->getFields( 'lumonata_additional_field', 'ladditional_value', array( 'ladditional_key' => 'page_template', 'lapp_id' => $post_id ) );
        $price_type = $this->global->getFields( 'lumonata_additional_field', 'ladditional_value', array( 'ladditional_key' => 'price_type', 'lapp_id' => $post_id ) );

        if( $template == '1' && $price_type == '1' )
        {
            //-- GET schedule
            $s = 'SELECT * FROM lumonata_schedules AS a WHERE a.lpost_id = %d AND a.llang_id = %d';
            $q = parent::prepare_query( $s, $post_id, $this->lang );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $sign = $this->global->get_currency_sign( $post_id );
                $item = '';

                while( $d = parent::fetch_array( $r ) )
                {
                    //-- SET content
                    $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );

                    $item .= '
                    <div id="sc-wrap-' . $hash . '" data-id="' . $d[ 'lschedule_id' ] . '" class="eachscheduleupload flex">
                        <div class="cfield wh-set dt-set">
                            <input type="text" name="schedules[' . $d[ 'lschedule_id' ] . '][lcheck_in]" class="text text-date check-in-date" placeholder="Departure Date" value="' . ( empty( $d[ 'lcheck_in' ] ) ? '' : date( 'M d, Y', strtotime( $d[ 'lcheck_in' ] ) ) ) . '" autocomplete="off" required />
                        </div>
                        <div class="cfield wh-set dt-set">
                            <input type="text" name="schedules[' . $d[ 'lschedule_id' ] . '][lcheck_out]" class="text text-date check-out-date" placeholder="Return Date" value="' . ( empty( $d[ 'lcheck_out' ] ) ? '' : date( 'M d, Y', strtotime( $d[ 'lcheck_out' ] ) ) ) . '" autocomplete="off" required />
                        </div>
                        <div class="cfield wh-set larger_">
                            <input type="text" name="schedules[' . $d[ 'lschedule_id' ] . '][ldeparture]" class="text" placeholder="Departure/Arrival" autocomplete="off" value="' . $d[ 'ldeparture' ] . '" required />
                        </div>
                        <div class="cfield wh-set larger_">
                            <input type="text" name="schedules[' . $d[ 'lschedule_id' ] . '][litinerary]" class="text" placeholder="Itinerary" autocomplete="off" value="' . $d[ 'litinerary' ] . '" required />
                        </div>
                        <div class="cfield wh-set smaller_">
                            <input type="hidden" name="schedules[' . $d[ 'lschedule_id' ] . '][lstatus]" value="1">
                            <input type="text" name="schedules[' . $d[ 'lschedule_id' ] . '][lprice]" class="text text-number" data-a-sign="' . $sign . '" autocomplete="off" data-a-sep="." data-a-dec="," data-m-dec="0" value="0">
                        </div>
                        <a href="javascript:;" class="delete" data-id="' . $d[ 'lschedule_id' ] . '">
                            <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del.svg">
                        </a>
                    </div>';
                }

                //-- SET schedule content
                $content = '
                <div id="section-schedule" class="cfield">
                    <label><strong>Timetable</strong><br/>Please enter itineraries and separate them with commas</label>
                    <div class="mepetfield">
                        <div class="cfield-wrap" data-note="You can manage schedule by click the link below">
                            ' . $item . '
                        </div>
                        <a href="javascript:;" class="admmorebtn admmoreschedule">+ add more schedule</a>
                    </div>
                </div>';

                return $content;
            }
            else
            {
                $content = '
                <div id="section-schedule" class="cfield">
                    <label><strong>Timetable</strong><br/>Please enter itineraries and separate them with commas</label>
                    <div class="mepetfield">
                        <div class="cfield-wrap" data-note="You can manage schedule by click the link below">
                            <p class="note">You can manage schedule by click the link below</p>
                        </div>
                        <a href="javascript:;" class="admmorebtn admmoreschedule">+ add more schedule</a>
                    </div>
                </div>';

                return $content;
            }
        }
    }

    public function get_accommodation_add_ons( $post_id, $id = array() )
    {
        if( empty( $id ) )
        {
            $content = '<p class="note">You can manage add-ons by click the link below</p>';
        }
        else
        {
            $r = parent::query( 'SELECT * FROM lumonata_post_terms AS a WHERE a.lterm_id IN(' . implode( ',', $id ) . ')' );

            if( parent::num_rows( $r ) > 0 )
            {
                while( $d = parent::fetch_assoc( $r ) )
                {
                    $ra = $this->global->getFields( 'lumonata_additional_field', '*', array( 'lterm_id' => $d[ 'lterm_id' ] ), false );

                    if( parent::num_rows( $ra ) > 0 )
                    {
                        while( $da = parent::fetch_array( $ra ) )
                        {
                            $d[ 'ladditional' ][ $da[ 'ladditional_key' ] ] = $da[ 'ladditional_value' ];
                        }
                    }

                    $attc = $this->global->getFields( 'lumonata_attachment', 'lattach', array( 'lattach_id' => $d[ 'ladditional' ][ 'accommodation_add_ons_image' ] ) );
                    $mod  = $this->global->getFields( 'lumonata_module', 'lmodule_id', array( 'lapps' => 'accommodation_add_ons' ) );
                    $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );
                    $sign = $this->global->get_currency_sign( $post_id );

                    if( empty( $attc ) || ( !empty( $attc ) && !file_exists( IMAGE_DIR . '/Uploads/' . $attc ) ) )
                    {
                        $bgimg = 'https://dummyimage.com/110x70/000000/000000';
                        $class = 'eachaddonsupload flex';
                    }
                    else
                    {
                        $bgimg = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=110&h=70&src=' . HT_SERVER . SITE_URL . '/images/Uploads/' . $attc;
                        $class = 'eachaddonsupload eachaddonsuploadwithimage flex';
                    }

                    $brief = empty( $d[ 'ladditional' ][ 'accommodation_add_ons_brief' ] ) ? '' : $d[ 'ladditional' ][ 'accommodation_add_ons_brief' ];
                    $types = empty( $d[ 'ladditional' ][ 'addons_type_of_price' ] ) ? 0 : $d[ 'ladditional' ][ 'addons_type_of_price' ];
                    $price = empty( $d[ 'ladditional' ][ 'addons_price' ] ) ? 0 : $d[ 'ladditional' ][ 'addons_price' ];

                    $content = '
                    <div id="file-wrap-' . $hash . '" data-id="' . $d[ 'lterm_id' ] . '" data-module="' . $mod . '" class="' . $class . '">
                        <div class="cfield wh-set larger_">
                            <input type="hidden" name="addons[' . $d[ 'lterm_id' ] . '][lstatus]" value="1">
                            <input type="text" name="addons[' . $d[ 'lterm_id' ] . '][lname]" placeholder="Add-ons item name" value="' . $d[ 'lname' ] . '">
                        </div>
                        <div class="cfield wh-set wh-set-textarea">
                            <textarea name="addons[' . $d[ 'lterm_id' ] . '][ladditional][accommodation_add_ons_brief]" placeholder="Short description">' . $brief . '</textarea>
                        </div>
                        <div class="cfield wh-set smaller_">
                            <input type="text" name="addons[' . $d[ 'lterm_id' ] . '][ladditional][addons_price]" class="text text-number" data-a-sign="' . $sign . '" autocomplete="off" data-a-sep="." data-a-dec="," data-m-dec="0" value="' . $price . '">
                        </div>
                        <div class="cfield wh-set xs_">
                            <select name="addons[' . $d[ 'lterm_id' ] . '][ladditional][addons_type_of_price]" id="" class="sl2_">
                                <option value="0" ' . ( $types == 0 ? 'selected' : '' ) . '>Per Item</option>
                                <option value="1" ' . ( $types == 1 ? 'selected' : '' ) . '>Per Hour</option>
                            </select>
                        </div>
                        <div class="addfile_ addaddonsimage smaller_ black_ wh-set" style="background-image:url(' . $bgimg . ');">
                            <a class="delimg"><img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del-w.svg"></a>
                            <div class="inner">
                                <strong>+</strong><br/>Upload Photo
                                <input type="hidden" class="rimage" name="addons[' . $d[ 'lterm_id' ] . '][ladditional][accommodation_add_ons_image]" value="" autocomplete="off">
                                <input type="file" name="file_' . $hash . '" data-field="accommodation_add_ons_image" data-hash="' . $hash . '" autocomplete="off" data-filetype="images" data-src="https://dummyimage.com/110x70/000000/000000">
                            </div>
                        </div>
                        <a href="javascript:;" class="delete closer_" data-id="' . $d[ 'lterm_id' ] . '" data-mod="' . $mod . '">
                            <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del.svg" alt="">
                        </a>
                    </div>';
                }
            }
        }

        return $content;
    }

    public function get_page_template( $template = '' )
    {
        return $this->global->set_static_option( array(
            0 => 'Multiple Room',
            1 => 'Boat Charter',
            2 => 'Surf Camps',
            3 => 'Entire Place'
        ), $template, false );
    }

    public function get_continent_option( $continent_id = '' )
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = "destination" AND a.llang_id = %d AND ( SELECT a2.ladditional_value FROM lumonata_additional_field AS a2 WHERE a2.lapp_id = a.lpost_id AND a2.ladditional_key = %s ) = %d';
        $q = parent::prepare_query( $s, $this->lang, 'destination_type', 0 );

        return $this->global->set_option( $q, 'lpost_id', 'ltitle', $continent_id, false );
    }

    public function get_destination_option( $dest_id = '' )
    {
        $s = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a WHERE a.ltype = %s AND a.lstatus = %d AND a.llang_id = %d';
        $q = parent::prepare_query( $s, 'destination', 1, $this->lang );

        return $this->global->set_option( $q, 'lpost_id', 'ltitle', $dest_id, true, null, null );
    }

    public function get_accommodation_type_option( $lacco_type = null )
    {
        $s = 'SELECT a.lterm_id, a.lname FROM lumonata_post_terms AS a WHERE a.lrule = %s AND a.lgroup = %s AND a.llang_id = %d';
        $q = parent::prepare_query( $s, 'accommodation_type', 'accommodation', $this->lang );

        return $this->global->set_option( $q, 'lterm_id', 'lname', $lacco_type, false );
    }

    public function get_accommodation_surf_trip_option( $id = array() )
    {
        $s = 'SELECT a.lterm_id, a.lname FROM lumonata_post_terms AS a WHERE a.lrule = %s AND a.lgroup = %s AND a.llang_id = %d';
        $q = parent::prepare_query( $s, 'surf_trip', 'accommodation', $this->lang );
        $r = parent::query( $q );

        $content = '';

        while( $d = parent::fetch_array( $r ) )
        {
            if( !empty( $id ) && in_array( $d[ 'lterm_id' ], $id ) )
            {
                $checked = ' checked="checked"';
            }
            else
            {
                $checked = '';
            }

            $content .= '
            <div class="form-group">
                <input name="terms[surf_trip][]" type="checkbox" value="' . $d[ 'lterm_id' ] . '" id="cb' . $d[ 'lterm_id' ] . '"' . $checked . ' autocomplete="off">
                <label for="cb' . $d[ 'lterm_id' ] . '">' . $d[ 'lname' ] . '</label>
            </div>';
        }

        return $content;
    }

    public function get_accommodation_status_option( $lstatus = 1 )
    {
        return '
        <div class="radioinline flex">
            <div class="con-r">
                <input type="radio" name="fields[lstatus]" id="radio-active" class="radioinput" value="1" ' . ( $lstatus == 1 ? 'checked' : '' ) . '>
                <label for="radio-active" class="radiolabel">Active</label>
            </div>
            <div class="con-r">
                <input type="radio" name="fields[lstatus]" id="radio-inactive" class="radioinput" value="0" ' . ( $lstatus == 0 ? 'checked' : '' ) . '>
                <label for="radio-inactive" class="radiolabel">Inactive</label>
            </div>
        </div>';
    }

    public function get_accommodation_things_to_do( $data )
    {
        $content = '';

        if( empty( $data ) )
        {
            $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );

            $content .= '
            <div id="file-wrap-' . $hash . '" class="eachactivityupload flex">
                <div class="cfield wh-set larger_">
                    <input type="text" name="fields[ladditional][things_to_do][' . $hash . '][to_do_title]" placeholder="Activity name">
                </div>
                <div class="cfield wh-set larger_ more_">
                    <textarea name="fields[ladditional][things_to_do][' . $hash . '][to_do_desc]" placeholder="Short description"></textarea>
                </div>                
                <div class="addfile_ addactivityimage smaller_ black_ wh-set" style="background-image:url(https://dummyimage.com/110x70/000000/000000);">
                    <a class="delimg"><img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del-w.svg"></a>
                    <div class="inner">
                        <strong>+</strong><br/>Upload Photo
                        <input type="hidden" class="rimage" name="fields[ladditional][things_to_do][' . $hash . '][to_do_img]" value="" autocomplete="off">
                        <input type="file" name="file_' . $hash . '" data-field="to_do_img" data-hash="' . $hash . '" autocomplete="off" data-filetype="images" data-src="https://dummyimage.com/110x70/000000/000000">
                    </div>
                </div>
                <a href="javascript:;" class="delete closer_">
                    <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del.svg">
                </a>
            </div>';
        }
        else
        {
            $data = json_decode( $data, true );

            if( $data !== null && json_last_error() === JSON_ERROR_NONE )
            {
                foreach( $data as $groupidx => $group )
                {
                    $attc = $this->global->getFields( 'lumonata_attachment', 'lattach', array( 'lattach_id' => $group[ 'to_do_img' ] ) );

                    if( empty( $attc ) || ( !empty( $attc ) && !file_exists( IMAGE_DIR . '/Uploads/' . $attc ) ) )
                    {
                        $bgimg = 'https://dummyimage.com/110x70/000000/000000';
                        $class = 'eachactivityupload flex';
                    }
                    else
                    {
                        $bgimg = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=110&h=70&src=' . HT_SERVER . SITE_URL . '/images/Uploads/' . $attc;
                        $class = 'eachactivityupload eachactivityuploadwithimage flex';
                    }

                    $content .= '
                    <div id="file-wrap-' . $groupidx . '" class="' . $class . '">
                        <div class="cfield wh-set larger_">
                            <input type="text" name="fields[ladditional][things_to_do][' . $groupidx . '][to_do_title]" value="' . $group[ 'to_do_title' ] . '" placeholder="Activity name">
                        </div>
                        <div class="cfield wh-set larger_ more_">
                            <textarea name="fields[ladditional][things_to_do][' . $groupidx . '][to_do_desc]" placeholder="Short description">' . $group[ 'to_do_desc' ] . '</textarea>
                        </div>                
                        <div class="addfile_ addactivityimage smaller_ black_ wh-set" style="background-image:url(' . $bgimg . ');">
                            <a class="delimg"><img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del-w.svg"></a>
                            <div class="inner">
                                <strong>+</strong><br/>Upload Photo
                                <input type="hidden" class="rimage" name="fields[ladditional][things_to_do][' . $groupidx . '][to_do_img]" value="' . $group[ 'to_do_img' ] . '" autocomplete="off">
                                <input type="file" name="file_' . $groupidx . '" data-field="to_do_img" data-hash="' . $groupidx . '" autocomplete="off" data-filetype="images" data-src="https://dummyimage.com/110x70/000000/000000">
                            </div>
                        </div>
                        <a href="javascript:;" class="delete closer_">
                            <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del.svg">
                        </a>
                    </div>';
                }
            }
            else
            {
                $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );

                $content .= '
                <div id="file-wrap-' . $hash . '" class="eachactivityupload flex">
                    <div class="cfield wh-set larger_">
                        <input type="text" name="fields[ladditional][things_to_do][' . $hash . '][to_do_title]" placeholder="Activity name">
                    </div>
                    <div class="cfield wh-set larger_ more_">
                        <textarea name="fields[ladditional][things_to_do][' . $hash . '][to_do_desc]" placeholder="Short description"></textarea>
                    </div>                
                    <div class="addfile_ addactivityimage smaller_ black_ wh-set" style="background-image:url(https://dummyimage.com/110x70/000000/000000);">
                        <a class="delimg"><img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del-w.svg"></a>
                        <div class="inner">
                            <strong>+</strong><br/>Upload Photo
                            <input type="hidden" class="rimage" name="fields[ladditional][things_to_do][' . $hash . '][to_do_img]" value="" autocomplete="off">
                            <input type="file" name="file_' . $hash . '" data-field="to_do_img" data-hash="' . $hash . '" autocomplete="off" data-filetype="images" data-src="https://dummyimage.com/110x70/000000/000000">
                        </div>
                    </div>
                    <a href="javascript:;" class="delete closer_">
                        <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del.svg">
                    </a>
                </div>';
            }
        }

        return $content;
    }

    public function get_accommodation_surfing_info( $data )
    {
        $content = '';

        if( empty( $data ) )
        {
            $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );

            $content .= '
            <div id="file-wrap-' . $hash . '" class="eachsurfinfo flex">
                <div class="inner">
                    <input type="text" name="fields[ladditional][surfing_info][' . $hash . '][surf_info_title]" placeholder="Surf spot">
                    <textarea name="fields[ladditional][surfing_info][' . $hash . '][surf_info_desc]" id="" cols="30" rows="10"></textarea>
                </div>
                <a href="javascript:;" class="delete closer_">
                    <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del.svg">
                </a>
            </div>';
        }
        else
        {
            $data = json_decode( $data, true );

            if( $data !== null && json_last_error() === JSON_ERROR_NONE )
            {
                foreach( $data as $groupidx => $group )
                {
                    $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );

                    $content .= '
                    <div id="file-wrap-' . $hash . '" class="eachsurfinfo flex">
                        <div class="inner">
                            <input type="text" name="fields[ladditional][surfing_info][' . $groupidx . '][surf_info_title]" value="' . $group[ 'surf_info_title' ] . '" placeholder="Surf spot">
                            <textarea name="fields[ladditional][surfing_info][' . $groupidx . '][surf_info_desc]" id="" cols="30" rows="10">' . $group[ 'surf_info_desc' ] . '</textarea>
                        </div>
                        <a href="javascript:;" class="delete closer_">
                            <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del.svg">
                        </a>
                    </div>';
                }
            }
            else
            {
                $hash = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );

                $content .= '
                <div id="file-wrap-' . $hash . '" class="eachsurfinfo flex">
                    <div class="inner">
                        <input type="text" name="fields[ladditional][surfing_info][' . $hash . '][surf_info_title]" placeholder="Surf spot">
                        <textarea name="fields[ladditional][surfing_info][' . $hash . '][surf_info_desc]" id="" cols="30" rows="10"></textarea>
                    </div>
                    <a href="javascript:;" class="delete closer_">
                        <img src="' . HT_SERVER . MODULES_THEME_URL . '/asset/img/del.svg">
                    </a>
                </div>';
            }
        }

        return $content;
    }

    public function get_accommodation_hightlight_option( $data )
    {
        $content = '';

        if( empty( $data ) )
        {
            for( $i = 0; $i < 8 ; $i++ )
            {
                $content .= '<input type="text" class="hl" name="fields[ladditional][hightlight][' . substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 ) . '][name]" placeholder="Hightlight ' . ( $i + 1 ) . '">';
            }
        }
        else
        {
            $data = json_decode( $data, true );

            if( $data !== null && json_last_error() === JSON_ERROR_NONE )
            {
                $i = 0;

                foreach( $data as $groupidx => $group )
                {
                    $content .= '<input type="text" class="hl" name="fields[ladditional][hightlight][' . $groupidx . '][name]" placeholder="Hightlight ' . ( $i + 1 ) . '" value="' . $group[ 'name' ] . '">';

                    $i++;
                }
            }
            else
            {
                for( $i = 0; $i < 8 ; $i++ )
                {
                    $content .= '<input type="text" class="hl" name="fields[ladditional][hightlight][' . substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 ) . '][name]" placeholder="Hightlight ' . ( $i + 1 ) . '">';
                }
            }
        }

        return $content;
    }

    public function get_accommodation_amenities_option( $data )
    {
        $terms = $this->terms_data( array(
            'lgroup'   => 'accommodation',
            'lrule'    => 'amenities',
            'llang_id' => $this->lang,
            'lstatus'  => 1
        ) );

        if( empty( $terms ) === false )
        {
            $choose = json_decode( $data, true );

            if( $choose === null || json_last_error() !== JSON_ERROR_NONE )
            {
                $choose = array();
            }

            foreach( $terms as $id => $name )
            {
                $checked = '';

                if( in_array( $id, $choose ) )
                {
                    $checked = ' checked="checked"';
                }

                $content = '
                <div class="form-group">
                    <input name="fields[ladditional][amenities][]" value="' . $id . '" type="checkbox" id="inc' . $id . '"' . $checked . ' autocomplete="off">
                    <label for="inc' . $id . '">' . $name . '</label>
                </div>';
            }
        }

        return $content;
    }

    public function get_accommodation_whats_included_option( $data, $type = 'stay' )
    {
        $terms = $this->terms_data( array(
            'llang_id' => $this->lang,
            'lgroup'   => 'inclusions',
            'lrule'    => $type,
            'lstatus'  => 1
        ) );

        $content = '';

        if( empty( $terms ) === false )
        {
            $choose = json_decode( $data, true );

            if( $choose === null || json_last_error() !== JSON_ERROR_NONE )
            {
                $choose = array();
            }

            foreach( $terms as $id => $name )
            {
                $checked = '';

                if( in_array( $id, $choose ) )
                {
                    $checked = ' checked="checked"';
                }

                $content .= '
                <div class="form-group">
                    <input name="fields[ladditional][whats_included_' . $type . '][]" value="' . $id . '" type="checkbox" id="inc' . $type . $id . '"' . $checked . ' autocomplete="off">
                    <label for="inc' . $type . $id . '">' . $name . '</label>
                </div>';
            }
        }

        return $content;
    }

    public function get_accommodation_food_option( $data, $type )
    {
        $terms = $this->terms_data( array(
            'llang_id' => $this->lang,
            'lgroup'   => 'inclusions',
            'lrule'    => $type,
            'lstatus'  => 1
        ) );

        $content = '';

        if( empty( $terms ) === false )
        {
            $choose = json_decode( $data, true );

            if( $choose === null || json_last_error() !== JSON_ERROR_NONE )
            {
                $choose = array();
            }

            foreach( $terms as $id => $name )
            {
                $checked = '';

                if( in_array( $id, $choose ) )
                {
                    $checked = ' checked="checked"';
                }

                $content .= '
                <div class="form-group">
                    <input name="fields[ladditional][food_' . $type . '][]" value="' . $id . '" type="checkbox" id="inc' . $type . $id . '"' . $checked . ' autocomplete="off">
                    <label for="inc' . $type . $id . '">' . $name . '</label>
                </div>';
            }
        }

        return $content;
    }

    public function get_check_in_out_time_option( $data = '' )
    {
        $tstart = strtotime( '00:00' );
        $tend   = strtotime( '23:30' );
        $tnow   = $tstart;
        $option = '';

        if( empty( $data ) )
        {
            $data = '12:00';
        }

        while( $tnow <= $tend )
        {
            $tformat = date( 'H:i', $tnow );
            $tnow    = strtotime( '+30 minutes', $tnow );

            $option .= '<option value="' . $tformat . '" ' . ( $data == $tformat ? 'selected' : '' ) . '>' . $tformat . '</option>';
        }

        return $option;
    }

    public function get_payment_method_option( $data = array() )
    {
        if( empty( $data[ 'payment_method' ] ) )
        {
            $data[ 'payment_method' ] = '0';
        }

        return '
        <div class="con-r">
            <input type="radio" name="fields[ladditional][payment_method]" id="radio-bt" class="radioinput radioinput-method" ' . ( $data[ 'payment_method' ] == '0' ? 'checked' : '' ) . ' value="0" autocomplete="off">
            <label for="radio-bt" class="radiolabel">Bank Transfer</label>
            <div class="' . ( $data[ 'payment_method' ] == '0' ? 'bank-detail' : 'bank-detail hidden' ) . '">
                <textarea name="fields[ladditional][bank_detail]" placeholder="Please fill with your bank account detail">' . $data[ 'bank_detail' ] . '</textarea>
            </div>
        </div>
        <div class="con-r">
            <input type="radio" name="fields[ladditional][payment_method]" id="radio-ua" class="radioinput radioinput-method" ' . ( $data[ 'payment_method' ] == '1' ? 'checked' : '' ) . ' value="1" autocomplete="off">
            <label for="radio-ua" class="radiolabel">Upon Arrival</label>
        </div>';
    }

    public function get_payment_policy_option( $data )
    {
        if( empty( $data ) )
        {
            $data = '0';
        }

        return '
        <div class="con-r">
            <input type="radio" name="fields[ladditional][payment_policy]" id="radio-immed" class="radioinput" ' . ( $data == '0' ? 'checked' : '' ) . ' value="0" autocomplete="off">
            <label for="radio-immed" class="radiolabel">Immediately after reservation</label>
        </div>
        <div class="con-r">
            <input type="radio" name="fields[ladditional][payment_policy]" id="radio-2m" class="radioinput" ' . ( $data == '1' ? 'checked' : '' ) . ' value="1" autocomplete="off">
            <label for="radio-2m" class="radiolabel">2 months prior to arrival</label>
        </div>
        <div class="con-r">
            <input type="radio" name="fields[ladditional][payment_policy]" id="radio-1m" class="radioinput" ' . ( $data == '2' ? 'checked' : '' ) . ' value="2" autocomplete="off">
            <label for="radio-1m" class="radiolabel">1 month prior to arrival</label>
        </div>
        <div class="con-r">
            <input type="radio" name="fields[ladditional][payment_policy]" id="radio-ua" class="radioinput" ' . ( $data == '3' ? 'checked' : '' ) . ' value="3" autocomplete="off">
            <label for="radio-ua" class="radiolabel">Upon arrival</label>
        </div>';
    }

    public function get_accommodation_location_option( $id )
    {
        if( empty( $id ) )
        {
            return '<option value=""></option>';
        }
        else
        {
            return '<option value="' . $id . '" selected>' . $this->global->getFields( 'lumonata_post', 'ltitle', array( 'lpost_id' => $id ) ) . '</option>';
        }
    }

    public function get_cancellation_policy_option( $data )
    {
        if( empty( $data ) )
        {
            $data = '0';
        }

        return '
        <div class="con-r">
            <input type="radio" name="fields[ladditional][cancellation_policy]" id="radio-flex" class="radioinput" ' . ( $data == '0' ? 'checked' : '' ) . ' value="0" autocomplete="off">
            <label for="radio-flex" class="radiolabel">Flexible</label>
        </div>
        <div class="con-r">
            <input type="radio" name="fields[ladditional][cancellation_policy]" id="radio-moderate" class="radioinput" ' . ( $data == '1' ? 'checked' : '' ) . ' value="1" autocomplete="off">
            <label for="radio-moderate" class="radiolabel">Moderate</label>
        </div>
        <div class="con-r">
            <input type="radio" name="fields[ladditional][cancellation_policy]" id="radio-strict" class="radioinput" ' . ( $data == '2' ? 'checked' : '' ) . ' value="2" autocomplete="off">
            <label for="radio-strict" class="radiolabel">Strict</label>
        </div>';
    }

    public function init_view_js()
    {
        return '
        <script>
            jQuery(document).ready(function(){
                init_accommodation_table();
            });
        </script>';
    }

    public function init_general_js()
    {
        return '
        <script>
            jQuery(document).ready(function(){
                init_destination_option();
                init_tinymce();
                init_upload();
            });
        </script>';
    }

    public function init_detail_js()
    {
        return '
        <script>
            jQuery(document).ready(function(){
                init_schedule();
                init_package();
                init_select2();
                init_tinymce();
                init_number();
                init_addons();
                init_rooms();
            });
        </script>';
    }

    public function init_activity_js()
    {
        return '
        <script>
            jQuery(document).ready(function(){    
                init_surf_info();
                init_activity();
            });
        </script>';
    }

    public function init_settings_js()
    {
        return '
        <script>
            jQuery(document).ready(function(){    
                init_number();
            });
        </script>';
    }

    public function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'load' )
            {
                echo $this->load();
            }

            if( $this->post[ 'pKEY' ] == 'load-continent' )
            {
                echo $this->load_continent();
            }

            if( $this->post[ 'pKEY' ] == 'load-country' )
            {
                echo $this->load_country();
            }

            if( $this->post[ 'pKEY' ] == 'load-state' )
            {
                echo $this->load_state();
            }

            if( $this->post[ 'pKEY' ] == 'load-city' )
            {
                echo $this->load_city();
            }

            if( $this->post[ 'pKEY' ] == 'upload_file' )
            {
                echo $this->upload_file();
            }

            if( $this->post[ 'pKEY' ] == 'delete_file' )
            {
                echo $this->delete_file();
            }

            if( $this->post[ 'pKEY' ] == 'add_rooms' )
            {
                echo $this->add_rooms();
            }

            if( $this->post[ 'pKEY' ] == 'delete_rooms' )
            {
                echo $this->delete_rooms();
            }

            if( $this->post[ 'pKEY' ] == 'add_schedule' )
            {
                echo $this->add_schedule();
            }

            if( $this->post[ 'pKEY' ] == 'delete_schedule' )
            {
                echo $this->delete_schedule();
            }

            if( $this->post[ 'pKEY' ] == 'add_package' )
            {
                echo $this->add_package();
            }

            if( $this->post[ 'pKEY' ] == 'delete_package' )
            {
                echo $this->delete_package();
            }

            if( $this->post[ 'pKEY' ] == 'add_addons' )
            {
                echo $this->add_addons();
            }

            if( $this->post[ 'pKEY' ] == 'delete_addons' )
            {
                echo $this->delete_addons();
            }

            if( $this->post[ 'pKEY' ] == 'add_activity' )
            {
                echo $this->add_activity();
            }

            if( $this->post[ 'pKEY' ] == 'add_surf_info' )
            {
                echo $this->add_surf_info();
            }
        }

        exit;
    }
}
