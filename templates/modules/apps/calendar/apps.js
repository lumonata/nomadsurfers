var calendar;
var price;

function init_view_js()
{
    if( jQuery('#wrap-list-form-calendar').length > 0 )
    {
    	var selector = document.getElementById('wrap-list-form-calendar');

	    calendar = new FullCalendar.Calendar( selector, {
	        initialView: 'dayGridMonth',
	        showNonCurrentDates: false,
	        headerToolbar: false,
	        selectable: true,
	        footer:true,
	        events: function( e, successCallback, failureCallback ) {
	            var url = jQuery('#ajax_url').val();
	            var prm = new Object;
	                prm.pKEY    = 'availability_calendar';
		            prm.acco_id = jQuery('[name=acco_id]:checked').val();
		            prm.room_id = jQuery('[name=room_id]:checked').val();
	                prm.start   = moment( e.start ).format( 'YYYY-MM-DD' );
	                prm.end     = moment( e.end ).subtract(1, 'days').format( 'YYYY-MM-DD' );

	            jQuery.ajax({
	                url: url,
	                data: prm,
	                type: 'POST',
	                dataType : 'json',
	                success: function( e ){
	                    if( e.length > 0 )
	                    {
	                        successCallback( e );
	                    }
	                },
	                error: function( e ){
	                    alert( 'Something wrong please reload the page!' );
	                }
	            });
	        },
	        eventDidMount: function( arg ) {
	            var ele = jQuery(arg.el).parents('.fc-day');
	            var dta = arg.event.extendedProps;

	            if( dta.status == 1 )
	            {
	                ele.addClass('fc-available');
	            }
	            else if( dta.status == 2 )
	            {
	                ele.addClass('fc-blocked');
	            }
	            else if( dta.status == 3 )
	            {
	            	jQuery.each( getDates( arg.event.start, moment( arg.event.end ).subtract(1, 'days') ), function(i, e){
	                	jQuery('.fc-day[data-date="' + e + '"]').addClass('fc-booked');
	            	});
	            }
	        },
	        eventWillUnmount: function( arg ) {
	            var ele = jQuery(arg.el).parents('.fc-day');
	            var dta = arg.event.extendedProps;

	            ele.removeClass('fc-available');
	            ele.removeClass('fc-blocked');

	            if( dta.status == 3 )
	            {
	            	jQuery.each( getDates( arg.event.start, moment( arg.event.end ).subtract(1, 'days') ), function(i, e){
	                	jQuery('.fc-day[data-date="' + e + '"]').removeClass('fc-booked');
	            	});
	            }
	        },
	        eventContent: function( arg ){
		        var ele = document.createElement('div');
	            var dta = arg.event.extendedProps;

	            if( dta.status == 3 )
	            {
	            	arg.backgroundColor = '#FFC200';
	            	arg.borderColor     = '#FFC200';

		            ele.className = 'book-event clearfix';
		            ele.innerHTML = 
		            '<figure><img src="https://ui-avatars.com/api/?name=' + dta.name + '&background=random" /></figure>'+
		            '<p class="book-name">' + dta.name + '</p>';

		            return { domNodes: [ ele ] };
	            }
	            else if( dta.status == 2 )
	            {
	            	arg.backgroundColor = '#DDDDDD';
	            	arg.borderColor     = '#DDDDDD';
	            	arg.textColor       = '#DDDDDD';
	            }
	            else if( dta.status == 1 )
	            {
	            	arg.backgroundColor = 'transparent';
	            	arg.borderColor     = 'transparent';
	            	arg.textColor       = '#000000';

	            	ele.className = 'other-event clearfix';
		            ele.innerHTML = '<small>' + dta.price + '</small>';

		            return { domNodes: [ ele ] };
	            }
	        },
	        datesSet: function( info ) {
	        	var next = moment( calendar.getDate() ).add( 1, 'month');
	        	var prev = moment( calendar.getDate() ).subtract( 1, 'month');

				jQuery('.thecalendar .prev span').text( prev.format('MMMM YYYY') );
				jQuery('.thecalendar .next span').text( next.format('MMMM YYYY') );
			},
	        selectAllow: function( e ){
	            return moment().diff( e.start, 'days' ) <= 0;
	        },
        	select: function( e ){
                var months = moment( e.start ).format( 'MMMM' );
                var lstart = moment( e.start ).format( 'DD' );
                var lend   = moment( e.end ).subtract(1, 'days').format( 'DD' );
                var years  = moment( e.end ).format( 'YYYY' );

                jQuery('[name="lstart"]').val( moment( e.start ).format( 'YYYY-MM-DD' ) );
                jQuery('[name="lend"]').val( moment( e.end ).subtract(1, 'days').format( 'YYYY-MM-DD' ) );

                if( lstart == lend )
                {
                	jQuery('.selected-date').text( months + ' ' + lstart + ', ' + years );
                }
                else
                {
                	jQuery('.selected-date').text( months + ' ' + lstart + '-' + lend + ', ' + years );
                }
        	}
	    });

	    calendar.render();

	    init_price();

	    jQuery('[name=acco_id]').on('change', function(){
	    	var input = jQuery(this).parent().parent().find('li:first-child input');

	    	if( input.length > 0 )
	    	{
	    		input.prop('checked', true);
	    	}

	    	jQuery(this).parents('.profmen').find('li').removeClass('sub-actv');
	    	jQuery(this).parent().parent().find('li:first-child').addClass('sub-actv');

	        eval( 'refetch()' );
	    });

	    jQuery('[name=room_id]').on('change', function(){
	    	jQuery(this).parents('.profmen').find('li').removeClass('sub-actv');
	    	jQuery(this).parent().parent().addClass('sub-actv');

	        eval( 'refetch()' );
	    });

	    jQuery('.thecalendar .prev').on('click', function(){
	    	calendar.prev();
	    });

	    jQuery('.thecalendar .next').on('click', function(){
	    	calendar.next();
	    });

	    jQuery('.update-availability').on('click', function(){
            var form    = jQuery('[name="availability"]');
	    	var acco_id = jQuery('[name="acco_id"]:checked').val();
	    	var room_id = jQuery('[name="room_id"]:checked').val();
	    	var lstatus = jQuery('[name="lstatus"]:checked').val();
	    	var lrate   = AutoNumeric.getNumber('[name=lrate]');
	    	var lstart  = form.find('[name="lstart"]').val();
            var lend    = form.find('[name="lend"]').val();

            if( lstart == '' || lend == '' )
            {
            	alert( 'No date selected' );
            }
            else if( acco_id == '' )
            {
            	alert( 'No listing selected' );
            }
            else if( lrate <= 0 && lstatus == 1 )
            {
            	alert( 'Nightly price must be greater than 0' );
            }
            else
            {
            	jQuery('[name="lpost_id"]').val( acco_id );
            	jQuery('[name="lterm_id"]').val( room_id );

            	jQuery.ajax({
	                data: AutoNumeric.getAutoNumericElement('[name=lrate]').formArrayNumericString(),
	                url: jQuery('#ajax_url').val(),
	                dataType : 'json',
	                method: 'POST',
	                beforeSend: function( xhr ){
	                    
	                },
	                success: function( e ){
	                	if( e.result == 'success' )
	                    {
	                    	eval( 'refetch()' );
	                    }
	                    else
	                    {
	                    	alert( e.message );
	                    }
	                },
	                error: function( e ){
	                    alert( 'Change rate and availability is failed' );
	                },
	                complete: function( e ){
	                }
	            });
            }

            return false;
	    });
    }
}

function getDates(startDate, stopDate) {
    var dateArray = [];
    var currentDate = moment(startDate);
    var stopDate = moment(stopDate);
    while (currentDate <= stopDate) {
        dateArray.push( moment(currentDate).format('YYYY-MM-DD') )
        currentDate = moment(currentDate).add(1, 'days');
    }
    return dateArray;
}

function init_price()
{
	if( jQuery('[name=acco_id]:checked').length > 0 )
    {
    	var sign = jQuery('[name=acco_id]:checked').data('sign');
	}
	else
	{
		var sign = '$ ';
	}

    var pdta = jQuery('.bnrr-input').data();

    price = new AutoNumeric( '.bnrr-input', {	    	
       	roundingMethod      : AutoNumeric.options.roundingMethod.halfUpSymmetric,
        digitGroupSeparator : pdta.aSep,
        decimalCharacter    : pdta.aDec,
        decimalPlaces       : pdta.mDec,
        currencySymbol      : sign,
        unformatOnSubmit    : true,
    });
}

function refrest_price()
{
    var sign = jQuery('[name=acco_id]:checked').data('sign');

	price.update({ currencySymbol : sign }).set( 0 );
}

function refetch()
{
	calendar.removeAllEvents();
    calendar.refetchEvents();
    refrest_price();
}