<?php

class calendar extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }

    function view( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $this->global->getPViewBothP( $mod, $usertype ) )
        {
            $data = $this->global->getPosts( array( 'lcreated_by' => $this->sess[ 'username' ], 'ltype' => 'accommodation', 'llang_id' => $this->lang, 'lstatus' => 1 ), false );
            
            $this->template->set_var( 'listing', $this->get_all_listing( $data ) );
            $this->template->set_var( 'listing_num', $this->get_num_of_listing( $data ) );
            $this->template->set_var( 'ajax_url', HT_SERVER . SITE_URL . '/agent/calendar/request/' );
            
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/autonumeric@4.6.0/dist/autoNumeric.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/fullcalendar@6.1.4/index.global.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/moment@2.29.1/moment.min.js' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js' ) );
            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css' ) );

            $this->actions->add_actions( 'css', $this->global->get_css( HT_SERVER . MODULES_THEME_URL . '/asset/css/calendar.css' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( HT_SERVER . MODULES_THEME_URL . '/apps/calendar/apps.js' ) );
            $this->actions->add_actions( 'jvs', $this->init_view_js() );

            $this->template->Parse( 'vC', 'viewContent', false );
        }
        else
        {
            $this->template->set_var( 'error', '<div class="error">' . NO_AUTHO . '</div>' );
        }
        
        $this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function init_view_js()
    {
        return '
        <script>
            jQuery(document).ready(function(){
                init_view_js();
            });
        </script>';
    }

    function get_num_of_listing( $data )
    {
        if( empty( $data ) === false )
        {
            $num = 0;

            foreach( $data as $i => $dt )
            {
                if( $dt[ 'policy' ] == 0 )
                {
                    $num++;
                }
            }

            if( $num > 1 )
            {
                return $num . ' Listings';
            }
            else
            {
                return $num . ' Listing';
            }
        }
        else
        {
            return 'No Listing';
        }
    }

    function get_all_listing( $data )
    {
        if( empty( $data ) === false )
        {
            $content = '
            <ul>';

                foreach( $data as $i => $dt )
                {
                    if( $dt[ 'policy' ] == 1 )
                    {
                        continue;
                    }

                    $content .= '
                    <li>
                        <a href="javascript:;" title="' . $dt[ 'ltitle' ] . '" class="pmh_ ' . ( $i == 0 ? 'actv' : '' ) . '">
                            <input type="radio" name="acco_id" value="' . $dt[ 'lpost_id' ] . '" ' . ( $i == 0 ? 'checked' : '' ) . ' data-sign="' . $this->global->get_currency_sign( $dt[ 'lpost_id' ] ) . '" autocomplete="off" />
                            <span>' . $dt[ 'ltitle' ] . '</span>
                            <img src="//' . MODULES_THEME_URL . '/asset/img/arw.svg">
                        </a>';

                        if( !empty( $dt[ 'room_type' ] ) )
                        {
                            $content .= '
                            <div class="ddnav ' . ( $i == 0 ? 'actv' : '' ) . '">
                                <ul>';

                                    foreach( $dt[ 'room_type' ] as $ii => $rm )
                                    {      
                                        $content .= '                                  
                                        <li' . ( $i == 0 && $ii == 0 ? ' class="sub-actv"' : '' ) . '>
                                            <a href="javascript:;" title="' . $rm[ 'lname' ] . '">
                                                <input type="radio" name="room_id" value="' . $rm[ 'lterm_id' ] . '" ' . ( $i == 0 && $ii == 0 ? 'checked' : '' ) . ' autocomplete="off" />
                                                <span>' . $rm[ 'lname' ] . '</span>
                                            </a>
                                        </li>';
                                    }

                                    $content .= '
                                </ul>
                            </div>';
                        }
                        else
                        {
                            $content .= '
                            <div class="ddnav hidden">
                                <ul>
                                    <li' . ( $i == 0 ? ' class="sub-actv"' : '' ) . '>
                                        <a href="javascript:;" title="">
                                            <input type="radio" name="room_id" value="" ' . ( $i == 0 ? 'checked' : '' ) . ' autocomplete="off" />
                                        </a>
                                    </li>
                                </ul>
                            </div>';
                        }

                        $content .= '
                    </li>';
                }

                $content .= '
            </ul>';

            return $content;
        }
    }

    function availability_calendar()
    {
        $acco_id = $this->post[ 'acco_id' ];
        $room_id = $this->post[ 'room_id' ];

        $acco  = $this->global->getPosts( array( 'lpost_id' => $acco_id ) );
        $sign  = $this->global->get_currency_sign( $acco_id );

        $list  = array();
        $rdata = array();

        $price = 0;

        if( empty( $acco[ 'room_type' ] ) )
        {
            $price = number_format( $acco[ 'room_price' ], 0 );
        }
        else
        {
            foreach( $acco[ 'room_type' ] as $a )
            {
                if( $a[ 'lterm_id' ] == $room_id && isset( $a[ 'room_price' ] ) )
                {
                    $price = number_format( $a[ 'room_price' ], 0 );

                    break;
                }
            }
        }

        //-- GET booking data
        $sa = 'SELECT * FROM lumonata_booking AS a WHERE a.lpost_id = %d';
        $qa = parent::prepare_query( $sa, $acco_id );
        $ra = parent::query( $qa );
        
        if( parent::num_rows( $ra ) > 0 )
        {
            while( $da = parent::fetch_array( $ra ) )
            {
                $da[ 'lrooms' ] = json_decode( $da[ 'lrooms' ], true );

                if( $da[ 'lrooms' ] !== null && json_last_error() === JSON_ERROR_NONE )
                {
                    foreach( $da[ 'lrooms' ] as $id => $rm )
                    {
                        if( $room_id == $id )
                        {
                            $bgap   = new DateInterval('P1D');  
                            $bstart = new DateTime( date( 'Y-m-d', $da[ 'lcheck_in' ] ) );
                            $bend   = new DateTime( date( 'Y-m-d', $da[ 'lcheck_out' ] ) );
                            $brange = new DatePeriod( $bstart, $bgap, $bend->add( $bgap ) );

                            foreach( $brange as $bdta )
                            {
                                $rdata[] = $bdta->format( 'Y-m-d' );
                            }

                            $list[] = array(
                                'name'   => $da[ 'lfullname' ],
                                'end'    => $bend->format( 'Y-m-d' ),
                                'start'  => $bstart->format( 'Y-m-d' ),
                                'price'  => sprintf( '%s %s', $sign, number_format( $rm[ 'price' ], 0 ) ),
                                'status' => 3,
                            ); 
                        }
                    }
                }
            }
        }

        $gap   = new DateInterval('P1D');  
        $start = new DateTime( $this->post[ 'start' ] );
        $end   = new DateTime( $this->post[ 'end' ] );
        $range = new DatePeriod( $start, $gap, $end->add( $gap ) );

        foreach( $range as $d )
        {
            $params  = array( 'ldate' => $d->format( 'Y-m-d' ) );

            if( empty( $acco_id ) === false )
            {
                $params = array_merge( $params, array( 'lpost_id' => $acco_id ) );
            }

            if( empty( $room_id ) === false )
            {
                $params = array_merge( $params, array( 'lterm_id' => $room_id ) );
            }

            $dt = $this->global->getFields( 'lumonata_calendar', '*', $params );

            if( empty( $dt ) === false )
            {
                $list[] = array( 
                    'start'  => $d->format( 'Y-m-d' ),
                    'status' => intval( $dt[ 'lstatus' ] ),
                    'price'  => sprintf( '%s %s', $sign, number_format( $dt[ 'lrate' ], 0 ) )
                ); 
            }
            else
            {
                if( in_array( $d->format( 'Y-m-d' ), $rdata ) === false )
                {
                    $list[] = array( 
                        'start'  => $d->format( 'Y-m-d' ),
                        'price'  => sprintf( '%s %s', $sign, number_format( $price, 0 ) ),
                        'status' => 1
                    );
                }
                else
                {
                    $list[] = array( 
                        'status' => 3
                    );
                }
            }
        }

        return json_encode( $list );
    }

    function update_availability()
    {
        $gap   = new DateInterval('P1D');  
        $start = new DateTime( $this->post[ 'lstart' ] );
        $end   = new DateTime( $this->post[ 'lend' ] );
        $range = new DatePeriod( $start, $gap, $end->add( $gap ) );

        if( empty( $range ) === false )
        {
            parent::begin();

            $commit = 1;

            $param = array_diff_key( $this->post, array_flip( array( 'pKEY', 'lstart', 'lend', 'lrate_change', 'room_allotment' ) ) );
            $param = array_filter( $param );

            foreach( $range as $d )
            {
                $param[ 'ldate' ] = $d->format( 'Y-m-d' );

                $cid = $this->global->getFields( 'lumonata_calendar', 'lcalendar_id', array(
                    'lpost_id' => $this->post[ 'lpost_id' ],
                    'lterm_id' => $this->post[ 'lterm_id' ],
                    'ldate'    => $param[ 'ldate' ],
                ));

                if( empty( $cid ) )
                {
                    $r = parent::insert( 'lumonata_calendar', $param );
                }
                else
                {
                    $r = parent::update( 'lumonata_calendar', $param, array( 'lcalendar_id' => $cid ) );
                }

                if( is_array( $r ) )
                {
                    $commit = 0;
                }
            }

            if( $commit == 0 )
            {
                parent::rollback();

                return json_encode( array( 'result' => 'failed', 'message' => 'Failed to change rate/availability' ) ); 
            }
            else
            {
                parent::commit();

                return json_encode( array( 'result' => 'success', 'message' => 'Successfully change rate/availability' ) ); 
            }
        }
        else
        {
            return json_encode( array( 'result' => 'failed', 'message' => 'No date selected' ) ); 
        }
    }

    function language()
    {
        if( isset( $_GET[ 'lang' ] ) && $_GET[ 'lang' ] != '' )
        {
            $this->lang = $_GET[ 'lang' ];
        }
        else
        {
            $this->lang = $this->global->getSettingValue( 'llanguage' );
        }
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();
        $this->post    = $_POST;
    }

    function request( $mod, $usertype )
    {
        if( $this->post[ 'pKEY' ] == 'availability_calendar' )
        {
            echo $this->availability_calendar();
        }

        if( $this->post[ 'pKEY' ] == 'update_availability' )
        {
            echo $this->update_availability();
        }        
    }
}