jQuery(document).ready(function(){
    var url = jQuery('#ajax_url').val();
    var prm = new Object;
        prm.pKEY = 'get_statistic_chart';

    jQuery.ajax({
        url: url,
        data: prm,
        type: 'POST',
        dataType : 'json',
        success: function( data ){
        	console.log(data);
        	if( data != '' )
        	{
        		var label = new Array;
        		var dtset = new Array;

        		jQuery.each( data.booking, function( i, e ){
        			label.push( i );
        			dtset.push( e );
        		});
	  			
	  			new Chart( document.getElementById( 'booking-chart' ).getContext( '2d' ), {
	  				type: 'bar',
					data: {
						labels: label,
						datasets: [{
							backgroundColor: '#FBC00A',
							data: dtset
		                }]
					},
	                options: {
	                    plugins: {
							legend: {
								display: false
							}
	                    },
	                    scales: {
							y: {
								beginAtZero: true
							}
	                    }
	                },
	  			});

        		var label = new Array;
        		var dtset = new Array;

        		jQuery.each( data.earnings, function( i, e ){
        			label.push( i );
        			dtset.push( e );
        		});
	  			
	  			new Chart( document.getElementById( 'earnings-chart' ).getContext( '2d' ), {
	  				type: 'bar',
					data: {
						labels: label,
						datasets: [{
							backgroundColor: '#FBC00A',
							data: dtset
		                }]
					},
	                options: {
	                    plugins: {
							legend: {
								display: false
							}
	                    },
	                    scales: {
							y: {
								beginAtZero: true
							}
	                    }
	                },
	  			});
        	}
        },
        error: function(e){
           alert( 'Something wrong on the request' );
        }
    });
});