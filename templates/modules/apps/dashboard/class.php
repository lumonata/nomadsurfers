<?php

class dashboard extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }

    function view( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewLoopContent', 'vlC' );
        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $this->global->getPViewBothP( $mod, $usertype ) )
        {
            //-- Query Limit
            $limit  = 3;
            $offset = 0;

            //-- Main Query
            $s = 'SELECT * FROM lumonata_booking AS a 
                  WHERE a.lagent = %s AND a.lstatus = 0 
                  ORDER BY a.lcreated_date DESC LIMIT %d, %d';
            $q = parent::prepare_query( $s, $this->sess[ 'username' ], $offset, $limit );
            $r = parent::query( $q );
            $n = parent::num_rows( $r );

            if( $n > 0 )
            {
                while( $d = parent::fetch_array( $r ) )
                {
                    $post = $this->global->getPosts( array( 'lpost_id' => $d[ 'lpost_id' ] ) );

                    if( !empty( $d[ 'lrooms' ] ) )
                    {
                        $rooms = json_decode( $d[ 'lrooms' ], true );
                        $nums  = 0;

                        if( $rooms !== null && json_last_error() === JSON_ERROR_NONE )
                        {
                            foreach( $rooms as $id => $room )
                            {
                                $nums += intval( $room[ 'num' ] );
                            }
                        }

                        $this->template->set_var( 'num_of_room', sprintf( 'ROOM NO. %s', $nums ) );
                        $this->template->set_var( 'room_name', $room[ 'name' ] );
                    }

                    if( $post[ 'policy' ] == '1' && $d[ 'lstatus' ] == 0 )
                    {
                        $this->template->set_var( 'confirm_btn_cls', 'dbtn y_' );
                    }
                    else
                    {
                        $this->template->set_var( 'confirm_btn_cls', 'dbtn y_ hidden' );
                    }

                    $this->template->set_var( 'thumbnail', $this->global->get_attachment_url( $post[ 'accommodation_image' ], 237, 165 ) );
                    $this->template->set_var( 'date', $this->global->get_format_date_range( $d[ 'lcheck_in' ], $d[ 'lcheck_out' ] ) );
                    $this->template->set_var( 'detailink', HT_SERVER . SITE_URL . '/agent/booking-details/' . $d[ 'lbooking_id' ] );
                    $this->template->set_var( 'nights', $this->global->get_format_nights( $d[ 'lnights' ] ) );
                    $this->template->set_var( 'guest', sprintf( '%s %s', $d[ 'lfname' ], $d[ 'llname' ] ) );
                    $this->template->set_var( 'property', $post[ 'ltitle' ] );

                    $this->template->Parse( 'vlC', 'viewLoopContent', true );
                }

                $this->template->set_var( 'notif', sprintf( 'You have %s new requests to confirm', $n ) );
            }
            else
            {                
                $this->template->set_var( 'notif', 'You don\'t have any new request' );
            }

            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->template->set_var( 'current_month', date( 'F' ) );
            $this->template->set_var( 'view_num', $this->global->get_property_view( $this->sess[ 'username' ] ) );
            $this->template->set_var( 'booking_num', $this->global->get_booking_num( $this->sess[ 'username' ] ) );
            $this->template->set_var( 'total_review', $this->global->get_total_review( $this->sess[ 'username' ] ) );
            $this->template->set_var( 'earnings', $this->global->get_monthly_earnings( $this->sess[ 'username' ] ) );
            $this->template->set_var( 'overall_rating', $this->global->get_overall_rating( $this->sess[ 'username' ] ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/chart.js@3.4.1/dist/chart.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( HT_SERVER . MODULES_THEME_URL . '/apps/dashboard/apps.js' ) );

            $this->template->Parse( 'vC', 'viewContent', false );
        }
        else
        {
            $this->template->set_var( 'error', '<div class="error">' . NO_AUTHO . '</div>' );
        }
        
        $this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function get_statistic_chart()
    {
        $start = new DateTime( 'first day of this month - 5 months' );
        $end   = new DateTime( 'this month' );
        $intv  = new DateInterval( 'P1M' );

        $period = new DatePeriod( $start, $intv, $end );
        $list   = array();

        foreach( $period as $dt )
        {
            $s = 'SELECT FROM_UNIXTIME( a.lcreated_date, %s ) AS fdate
                  FROM lumonata_booking AS a 
                  WHERE FROM_UNIXTIME( a.lcreated_date, %s ) = %s AND a.lagent = %s';
            $q = parent::prepare_query( $s, '%b', '%Y-%m', $dt->format( 'Y-m' ), $this->sess[ 'username' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                while( $d = parent::fetch_array( $r ) )
                {
                    if( isset( $list[ 'booking' ][ $d[ 'fdate' ] ] ) )
                    {
                        $list[ 'booking' ][ $d[ 'fdate' ] ] += 1;
                    }
                    else
                    {
                        $list[ 'booking' ][ $d[ 'fdate' ] ] = 1;
                    }
                }
            }
            else
            {
                $list[ 'booking' ][ $dt->format( 'M' ) ] = 0;
            }

            $s = 'SELECT 
                    a.ldeposit,
                    FROM_UNIXTIME( a.lcreated_date, %s ) AS fdate
                  FROM lumonata_booking AS a 
                  WHERE FROM_UNIXTIME( a.lcreated_date, %s ) = %s 
                  AND a.lstatus_payment = 1 AND a.lagent = %s';
            $q = parent::prepare_query( $s, '%b', '%Y-%m', $dt->format( 'Y-m' ), $this->sess[ 'username' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                while( $d = parent::fetch_array( $r ) )
                {
                    if( isset( $list[ 'earnings' ][ $d[ 'fdate' ] ] ) )
                    {
                        $list[ 'earnings' ][ $d[ 'fdate' ] ] += floatval( $d[ 'ldeposit' ] );
                    }
                    else
                    {
                        $list[ 'earnings' ][ $d[ 'fdate' ] ] = floatval( $d[ 'ldeposit' ] );
                    }
                }
            }
            else
            {
                $list[ 'earnings' ][ $dt->format( 'M' ) ] = 0;
            }
        }

        return json_encode( $list );
    }

    function language()
    {
        $this->lang = $this->global->getSettingValue( 'llanguage' );
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();
        $this->post    = $_POST;
    }

    function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'get_statistic_chart' )
            {
                echo $this->get_statistic_chart();
            }
        }
    }
}