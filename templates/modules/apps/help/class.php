<?php

class help extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->upload   = new upload();
        $this->actions  = $actions;

        $this->param();
    }

    function view( $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        $sett = $this->global->getSettingValue();

        $this->template->set_var( 'name', $sett['name'] );
        $this->template->set_var( 'phone', $sett['phone'] );
        $this->template->set_var( 'email', $sett['email'] );
        $this->template->set_var( 'address', $sett['address'] );

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();
    }
}