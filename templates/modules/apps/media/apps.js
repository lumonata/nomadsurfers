function init_upload_media()
{
	jQuery('.form-upload-media input[type=file]').fileupload({
		url: jQuery('.form-upload-media').attr('action'),
        dataType: 'json',
		formData: function( form ){
			return form.serializeArray();
		},
	    add: function( e, data ){
	        var filetype = /^image\/(gif|jpe?g|png)$/i;
	    	var errors   = new Array;

	        if( data.originalFiles[ 0 ][ 'type' ].length && !filetype.test( data.originalFiles[ 0 ][ 'type' ] ) )
	        {
	            errors.push( 'Not an accepted file type' );
	        }

	        if( data.originalFiles[ 0 ][ 'size' ] > 2097152 )
	        {
	            errors.push( 'Filesize is too big' );
	        }

	        if( errors.length > 0 )
	        {
	            alert( errors.join( "\n" ) );
	        }
	        else
	        {	            
		    	//-- CLONE item
	            var temp = jQuery('#cloned-wrapp').find('.item').clone();
	           	
	           	//-- SET image src and insert to container
	           	temp.find('.cover').prop('src', window.URL.createObjectURL( data.files[0] ) );
	           	temp.insertBefore( '#file-drop-zone .drag-drop' );

	           	//-- HIDE and SHOW button
		        jQuery('.action').addClass('show-action');
	           	jQuery('.drag-drop').addClass('hide')

	           	//-- SET data content
	            data.context = jQuery('#file-drop-zone .item:last-of-type');

	            //-- INIT upload function
	            data.context.find('.upload').on('click', function(){
	            	data.submit();
	            });

	            //-- INIT delete function
	            data.context.find('.delimg').on('click', function(){
	            	jQuery(this).parent().remove();

	            	if( jQuery('#file-drop-zone .item').length == 0 )
	            	{
		        		jQuery('.action').removeClass('show-action');
	            		jQuery('.drag-drop').removeClass('hide')
	            	}
	            });
	        }
	    },
        progressall: function( e, data ){
		    var progress = parseInt( data.loaded / data.total * 100, 10 );
		    
		    jQuery('.progressbar span').text( progress + '%' ).css('width', progress + '%' );
		},
	    start: function( e ){
		   	jQuery('.progressbar').addClass('show-action');
        },
        done: function( e, data ){
	        if( data.textStatus == 'success' )
	        {
	            if( data.jqXHR.responseJSON.result == 'success' )
	            {
            		var hash  = parent.jQuery('.addbatchimage').data('hash');
            		var field = parent.jQuery('.addbatchimage').data('field');
                    var temp  = parent.jQuery('#cloned-wrapp-' + hash).find('.item').clone();

                    temp.insertBefore( '#file-drop-zone-' + hash + ' .addbatchimage' );

                    var sel = parent.jQuery('#file-drop-zone-' + hash + ' .item:last-of-type');

                    parent.delete_drop_file( sel );

                    sel.find('input').attr( 'name', 'fields[ladditional][' + field + '][]');
                    sel.find('.delimg').attr('data-id', data.jqXHR.responseJSON.id);
                    sel.find('.cover').prop('src', data.jqXHR.responseJSON.src);
                    sel.find('input').val( data.jqXHR.responseJSON.id );
                    sel.addClass('uploaded');

        			data.context.find('.delimg').trigger('click');
        		}
        	}
        },
        stop: function( e ){
        	if( jQuery('#file-drop-zone .item').length > 0 )
        	{
        		alert( 'Some files can\'t be uploaded, maybe because you uploaded a file with the wrong format or some other reason' );
        	}
        }
	});

	jQuery('.upload-media').click(function(){
	    jQuery('#file-drop-zone .upload').trigger('click');
	});

    jQuery('.h-title a').on('click', function(){
    	if( typeof parent.jQuery.fancybox !== 'undefined' )
        {
        	parent.jQuery.fancybox.getInstance().close();
        }
    })
}