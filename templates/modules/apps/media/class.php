<?php

class media extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->upload   = new upload();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }

    function view()
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formBlock', 'fB' );

        $this->template->set_var( 'lapp_id', $this->get[ 'id' ] );
        $this->template->set_var( 'lmodule_id', $this->get[ 'mod' ] );

        $this->template->set_var( 'img_url', MODULES_THEME_URL . '/asset/img/' );
        $this->template->set_var( 'ajax_url', HT_SERVER . SITE_URL . '/agent/upload-media/request/' );

        $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/blueimp-file-upload@10.32.0/css/jquery.fileupload-ui.min.css' ) );

        $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/jquery-ui-dist@1.13.0/jquery-ui.min.js' ) );
        $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/blueimp-file-upload@10.32.0/js/jquery.fileupload.min.js' ) );
        $this->actions->add_actions( 'jvs', $this->global->get_jvs( '//' . MODULES_THEME_URL . '/apps/media/apps.js ' ) );
        $this->actions->add_actions( 'jvs', $this->init_js() );

        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function upload_media()
    {
        if( isset( $_FILES[ 'files' ] ) && $_FILES[ 'files' ][ 'error' ] == 0 )
        {
            global $db;

            $file_name = $_FILES[ 'files' ][ 'name' ];
            $file_size = $_FILES[ 'files' ][ 'size' ];
            $file_type = $_FILES[ 'files' ][ 'type' ];
            $file_tmp  = $_FILES[ 'files' ][ 'tmp_name' ];

            $sef_img  = $this->upload->file_name_filter( $file_name ) . '-' . time();
            $file     = $this->upload->rename_file( $file_name, $sef_img );

            $this->upload->upload_constructor( IMAGE_DIR . '/Uploads/' );

            if( $this->post[ 'file_type' ] == 'images' )
            {
                if( in_array( $file_type, array( 'image/jpg', 'image/jpeg', 'image/pjpeg', 'image/gif', 'image/png' ) ) )
                {
                    $this->upload->upload_resize( $file_name, $sef_img, $file_tmp, $file_type, 940, 940, 1 );
                }
                else
                {
                    return json_encode( array( 'result' => 'not-supported' ) );
                }
            }
            elseif( $this->post[ 'file_type' ] == 'word' && !in_array( $file_type, array( 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ) ) )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }
            elseif( $this->post[ 'file_type' ] == 'pdf' && $file_type != 'application/pdf' )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }
            elseif( $this->post[ 'file_type' ] == 'svg' && $file_type != 'image/svg+xml' )
            {
                return json_encode( array( 'result' => 'not-supported' ) );
            }

            if( $this->upload->upload_file( $file_name, $sef_img, $file_tmp, 0 ) )
            {
                $order_id = $this->global->setCode( 'lumonata_attachment', 'lorder_id' );

                $r = $db->insert( 'lumonata_attachment', array(
                    'lmodule_id'    => $this->post[ 'module_id' ],
                    'lapp_id'       => $this->post[ 'lapp_id' ],
                    'lcreated_by'   => $this->sess[ 'username' ],
                    'lusername'     => $this->sess[ 'username' ],
                    'llang_id'      => $this->lang,
                    'lmimetype'     => $file_type,
                    'lorder_id'     => $order_id,
                    'lsef_url'      => $sef_img,
                    'ltitle'        => $title,
                    'lattach'       => $file,
                    'lcreated_date' => time(),
                    'ldlu'          => time(),
                    'lstatus'       => 2
                ));

                if( is_array( $r ) )
                {
                    $this->upload->delete_file_thumb( $file );

                    return json_encode( array( 'result' => 'failed' ) );
                }
                else
                {
                    $id = $db->insert_id();

                    if( $file_type == 'image/jpg' || $file_type == 'image/jpeg' || $file_type == 'image/pjpeg' || $file_type == 'image/gif' || $file_type == 'image/png' )
                    {
                        //-- If file type is an image show the thumbnail
                        $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=115&h=115&src=' . HT_SERVER . IMAGE_URL . '/Uploads/' . $file;
                    }
                    else
                    {
                        if( in_array( $file_type, array( 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ) ) )
                        {
                            //-- Word file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-doc.png';
                        }
                        elseif( $file_type == 'application/pdf' )
                        {
                            //-- PDF file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-pdf.png';
                        }
                        elseif( $file_type == 'image/svg+xml' )
                        {
                            //-- SVG file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-svg.png';
                        }
                        else
                        {
                            //-- Other file type
                            $src = HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=40&h=40&src=' . HT_SERVER . ADMIN_THEME_IMAGE_URL . '/media-file.png';
                        }
                    }

                    return json_encode( array( 'result' => 'success', 'src' => $src, 'id' => $id ) );
                }
            }
            else
            {
                $this->upload->delete_thumb( $file );

                return json_encode( array( 'result' => 'failed' ) );
            }
        }
        else
        {
            return json_encode( array( 'result' => 'error' ) );
        }
    }

    function init_js()
    {
        return '
        <script>
            jQuery(document).ready(function(){    
                init_upload_media();
            });
        </script>';
    }

    function request()
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'upload_media' )
            {
                echo $this->upload_media();
            }
        }

        exit;
    }

    function language()
    {
        if( isset( $_GET[ 'lang' ] ) && $_GET[ 'lang' ] != '' )
        {
            $this->lang = $_GET[ 'lang' ];
        }
        else
        {
            $this->lang = $this->global->getSettingValue( 'llanguage' );
        }
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();

        if( empty( $_GET ) )
        {
            $this->get = array();
        }
        else
        {
            $this->get = $_GET;
        }

        if( empty( $_POST ) )
        {
            $this->post = array();
        }
        else
        {
            $this->post = $_POST;
        }
    }
}