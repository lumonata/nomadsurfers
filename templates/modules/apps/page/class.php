<?php

class page extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->upload   = new upload();
        $this->actions  = $actions;

        $this->param();
    }

    function view( $usertype, $type = null )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $type == 1 )
        {
            $sett = $this->global->getSettingValue();
            $mess = sprintf( 'Sorry, it seems the booking code you want to access is not registered in our system.<br/>If you have any questions regarding the issue, please contact us via email at<br/><a href="mailto:%s">%s</a>', $sett[ 'email' ], $sett[ 'email' ] );

            $this->template->set_var( 'title', 'INVALID BOOKING' );
            $this->template->set_var( 'message', $mess );
        }
        elseif( $type == 2 )
        {
            $sett = $this->global->getSettingValue();
            $mess = sprintf( 'Sorry, it seems the booking code you want to access is already been processed before.<br/>If you have any questions regarding the issue, please contact us via email at<br/><a href="mailto:%s">%s</a>', $sett[ 'email' ], $sett[ 'email' ] );

            $this->template->set_var( 'title', 'PROCESSED BOOKING' );
            $this->template->set_var( 'message', $mess );
        }
        else
        {
            $this->template->set_var( 'title', 'NOT FOUND' );
            $this->template->set_var( 'message', 'It seems that the page you are looking for was not found' );
        }

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();
    }
}