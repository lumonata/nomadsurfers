function init_profile_js()
{
    jQuery('#uploadava').on('change', function( e ){
        var reader = new FileReader();

        reader.onload = function(){
           jQuery('#fileoutput').attr( 'src', reader.result );
           jQuery('.rmvava').addClass('hidden');
        };

        reader.readAsDataURL( e.target.files[0] );
    });

    jQuery('.rmvava').on('click', function(){
    	var url = jQuery('#ajax_url').val();
        var prm = new Object;
            prm.pKEY     = 'delete_avatar';
            prm.luser_id = jQuery('#luser_id').val();
            prm.avatar   = jQuery('.avatar-img').val();

        jQuery.ajax({
            url: url,
            data: prm,
            type: 'POST',
            dataType : 'json',
            success: function( e ){
                if( e.result == 'success' )
                {
                    jQuery('#fileoutput').attr( 'src', e.src );

                    jQuery('.avatar-img').val('');
                    jQuery('.rmvava').addClass('hidden');
                }
                else if( e.result == 'empty' )
                {
                    alert( 'No photo uploaded' );
                }
                else
                {
                    alert( 'Failed to remove your profile picture' );
                }
            },
            error: function( e ){
                alert( 'Failed to remove your profile picture' );
            }
        });
    });
}

function init_security_js()
{

}