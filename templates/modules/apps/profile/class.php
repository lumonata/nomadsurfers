<?php

class profile extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->upload   = new upload();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }

    function profile( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );
        
        $this->template->set_block( 'form', 'securityContent', 'sC' );
        $this->template->set_block( 'form', 'profileContent', 'pC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->global->getPViewBothP( $mod, $usertype ) )
        {
            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->change( $mod, $usertype );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->template->set_var( 'lphone', $lphone );
            $this->template->set_var( 'lemail', $lemail );
            $this->template->set_var( 'labout', $labout );
            $this->template->set_var( 'luser_id', $luser_id );
            $this->template->set_var( 'lwebsite', $lwebsite );
            $this->template->set_var( 'llastname', $llastname );
            $this->template->set_var( 'lfacebook', $lfacebook );
            $this->template->set_var( 'lfirstname', $lfirstname );
            $this->template->set_var( 'linstagram', $linstagram );
            $this->template->set_var( 'lbirthdate_day', empty( $lbirthdate ) ? '' : date( 'd', $lbirthdate ) );
            $this->template->set_var( 'lbirthdate_year', empty( $lbirthdate ) ? '' : date( 'Y', $lbirthdate ) );
            $this->template->set_var( 'lbirthdate_month', empty( $lbirthdate ) ? '' : date( 'm', $lbirthdate ) );

            $this->template->set_var( 'limage', $this->get_avatar_image( $limage ) );
            $this->template->set_var( 'lgender', $this->get_gender_option( $lgender ) );

            $this->template->set_var( 'message', $this->message );

            if( $usertype == 2 )
            {
                $this->template->set_var( 'ladditional_field_css', '' );
                $this->template->set_var( 'panel_url', SITE_URL . '/agent' );

                $this->template->Parse( 'aC', 'agentMenuContent', false );
            }
            else
            {
                $this->template->set_var( 'ladditional_field_css', 'hidden' );
                $this->template->set_var( 'panel_url', SITE_URL . '/member' );

                $this->template->Parse( 'mC', 'memberMenuContent', false );
            }

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( HT_SERVER . MODULES_THEME_URL . '/apps/profile/apps.js' ) );
            $this->actions->add_actions( 'jvs', $this->init_profile_js() );

            $this->template->Parse( 'pC', 'profileContent', false );
        }
        else
        {
            $this->template->set_var( 'error', '<div class="error">' . NO_AUTHO . '</div>' );
        }

        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function security( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'securityContent', 'sC' );
        $this->template->set_block( 'form', 'profileContent', 'pC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->global->getPViewBothP( $mod, $usertype ) )
        {
            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->change( $mod, $usertype, true );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'luser_id', $luser_id );
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->template->set_var( 'message', $this->message );

            if( $usertype == 2 )
            {
                $this->template->set_var( 'panel_url', SITE_URL . '/agent' );

                $this->template->Parse( 'aC', 'agentMenuContent', false );
            }
            else
            {
                $this->template->set_var( 'panel_url', SITE_URL . '/member' );

                $this->template->Parse( 'mC', 'memberMenuContent', false );
            }

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( HT_SERVER . MODULES_THEME_URL . '/apps/profile/apps.js' ) );
            $this->actions->add_actions( 'jvs', $this->init_security_js() );

            $this->template->Parse( 'sC', 'securityContent', false );
        }
        else
        {
            $this->template->set_var( 'error', '<div class="error">' . NO_AUTHO . '</div>' );
        }

        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function validate( $data = array() )
    {
        $error = array();

        foreach( $data as $field => $dt )
        {
            if( $field == 'lfirstname' && $dt == '' )
            {
                array_push( $error, 'First name must have value' );
            }

            if( $field == 'llastname' && $dt == '' )
            {
                array_push( $error, 'Last name must have value' );
            }

            if( $field == 'lemail' && $dt == '' )
            {
                array_push( $error, 'Email address must have value' );
            }

            if( $field == 'lphone' && $dt == '' )
            {
                array_push( $error, 'Phone number must have value' );
            }

            if( $field == 'lpassword' && $dt != '' )
            {
            	if( empty( $data[ 'lopassword' ] ) )
            	{
                    array_push( $error, 'Fill old password with your current password' );
            	}
            	else
            	{
            		$cpass = $this->global->getFields( 'lumonata_user', 'lpassword', array( 'luser_id' => $this->sess[ 'userid' ] ) );

	                if( md5( $data[ 'lopassword' ] ) != $cpass )
	                {
	                    array_push( $error, 'Fill old password with your current password' );
	                }
            	}

                if( $dt != $data[ 'lcpassword' ] )
                {
                    array_push( $error, 'Confirm your password with same value' );
                }
            }
        }

        return $error;
    }

    function set_param( $data, $update = false )
    {
        //-- Remove confirm password
        unset( $data[ 'lcpassword' ] );

        //-- Remove old password
        unset( $data[ 'lopassword' ] );

        //-- Encrypt password
        if( empty( $data[ 'lpassword' ] ) === false )
        {
        	$data[ 'lpassword' ] = md5( $data[ 'lpassword' ] );
        }

        if( empty( $data[ 'lbirthdate' ] ) === false )
        {
        	krsort( $data[ 'lbirthdate' ] );

        	$data[ 'lbirthdate' ] = strtotime( implode( '-', $data[ 'lbirthdate' ] ) );
        }

        //-- Add image if exist
        if( isset( $_FILES[ 'limage' ] ) && $_FILES[ 'limage' ][ 'error' ] == 0 )
        {
            $image_name = $_FILES[ 'limage' ][ 'name' ];
            $image_size = $_FILES[ 'limage' ][ 'size' ];
            $image_type = $_FILES[ 'limage' ][ 'type' ];
            $image_tmp  = $_FILES[ 'limage' ][ 'tmp_name' ];

            $sef_img = $this->upload->file_name_filter( $image_name ) . '-' . time();
            $image   = $this->upload->rename_file( $image_name, $sef_img );

            if( $image_type == 'image/jpg' || $image_type == 'image/jpeg' || $image_type == 'image/pjpeg' || $image_type == 'image/gif' || $image_type == 'image/png' )
            {
                $this->upload->upload_constructor( IMAGE_DIR . '/Users/' );

                if( $this->upload->upload_resize( $image_name, $sef_img, $image_tmp, $image_type, 167, 167 ) )
                {
                    if( $this->upload->upload_file( $image_name, $sef_img, $image_tmp, 0 ) )
                    {
                        if( empty( $data[ 'limage' ] ) === false )
                        {
                            $this->upload->delete_file_thumb( $data[ 'limage' ] );
                        }

                        $data[ 'limage' ] = $image;
                    }
                    else
                    {
                    	$this->upload->delete_file_thumb( $image );
                    }
                }
            }
        }

        //-- Remove empty value, except 0 & "0"
        $data = array_filter( $data, function( $var ){ 
            return $var !== NULL && $var !== FALSE && $var !== ''; 
        });

        return $data;
    }

    function change( $mod, $usertype, $update_security = false )
    {
        $error = $this->validate( $this->fields );
        $data  = $this->fields;

        if( empty( $error ) )
        {
            //-- UPDATE lumonata_user
            $result = parent::update( 'lumonata_user', $this->set_param( $data, true ), array( 'luser_id' => $data[ 'luser_id' ] ) );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to update your data', 'post' => $data ) );
            }
            else
            {
                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully updated your data' ) );
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );
        }

        if( $usertype == 2 )
        {
            if( $update_security )
            {
                header( 'location: ' . HT_SERVER . SITE_URL . '/agent/security/' );

                exit;
            }
            else
            {                	
                header( 'location: ' . HT_SERVER . SITE_URL . '/agent/profile/' );

                exit;
            }
        }
        else
        {
            if( $update_security )
            {
                header( 'location: ' . HT_SERVER . SITE_URL . '/member/security/' );

                exit;
            }
            else
            {                   
                header( 'location: ' . HT_SERVER . SITE_URL . '/member/profile/' );

                exit;
            }
        }
    }

    function delete_avatar()
    {
    	if( isset( $this->post[ 'avatar' ] ) && !empty( $this->post[ 'avatar' ] ) )
        {
        	$this->upload->upload_constructor( IMAGE_DIR . '/Users/' );

            $this->upload->delete_file_thumb( $this->post[ 'avatar' ] );

            if( file_exists( IMAGE_DIR . '/Users/' . $this->post[ 'avatar' ] ) )
            {
                return json_encode( array( 'result' => 'failed' ) );
            }
            else
            {
            	parent::update( 'lumonata_user', array( 'limage' => '' ), array( 'luser_id' => $this->post[ 'luser_id' ] ) );

            	return json_encode( array( 'result' => 'success', 'src' => HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=167&h=167&src=' . HT_SERVER . SITE_URL . '/images/Users/default.png' ) );
            }
        }
        else
        {
        	return json_encode( array( 'result' => 'empty' ) );
        }
    }

    function get_avatar_image( $image = '' )
    {
        $avatar = '
        <div class="cutright">
            <figure class="pfava">
            	<input class="avatar-img" value="' . $image . '" name="fields[limage]" type="hidden" autocomplete="off" />';

		        if( empty( $image ) || ( !empty( $image ) && !file_exists( IMAGE_DIR . '/Users/' . $image ) ) )
		        {
		            $avatar .= '<img id="fileoutput" src="' . HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=167&h=167&src=' . HT_SERVER . SITE_URL . '/images/Users/default.png" alt="" />';
		        }
		        else
		        {
		            $avatar .= '<img id="fileoutput" src="' . HT_SERVER . ADMIN_URL . '/functions/mthumb.php?w=167&h=167&src=' . HT_SERVER . SITE_URL . '/images/Users/' . $image . '" alt="" />';
		        }

                $avatar .= '
            </figure>
            <label for="uploadava" class="upload-avatar">
                Edit profile picture
                <input type="file" name="limage" placeholder="Upload Avatar photo" id="uploadava">
            </label>';

            if( empty( $image ) || ( !empty( $image ) && !file_exists( IMAGE_DIR . '/Users/' . $image ) ) )
		    {
		    	$avatar .= '
            	<a href="javascript:;" class="rmvava hidden">Remove photo</a>';
            }
            else
            {
		    	$avatar .= '
            	<a href="javascript:;" class="rmvava">Remove photo</a>';
            }

            $avatar .= '
        </div>';

        return $avatar;
    }

    function get_gender_option( $gender = 0 )
    {
    	return '    	
        <div class="radioinline flex">
            <div class="con-r">
                <input type="radio" name="fields[lgender]" id="radio-male" class="radioinput" value="0" ' . ( $gender == 0 ? 'checked' : '' ) . '>
                <label for="radio-male" class="radiolabel">Male</label>
            </div>
            <div class="con-r">
                <input type="radio" name="fields[lgender]" id="radio-female" class="radioinput" value="1" ' . ( $gender == 1 ? 'checked' : '' ) . '>
                <label for="radio-female" class="radiolabel">Female</label>
            </div>
            <div class="con-r">
                <input type="radio" name="fields[lgender]" id="radio-other" class="radioinput" value="2" ' . ( $gender == 2 ? 'checked' : '' ) . '>
                <label for="radio-other" class="radiolabel">Other</label>
            </div>
        </div>';
    }

    function init_profile_js()
    {
        return '
        <script>
            jQuery(document).ready(function(){
                init_profile_js();
            });
        </script>';
    }

    function init_security_js()
    {
        return '
        <script>
            jQuery(document).ready(function(){
                init_security_js();
            });
        </script>';
    }

    function language()
    {
        $this->lang = $this->global->getSettingValue( 'llanguage' );
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();
        $this->valid   = true;

        if( isset( $this->sess[ 'username' ] ) )
        {
            $s = 'SELECT * FROM lumonata_user AS a WHERE a.lusername = %s';
            $q = parent::prepare_query( $s, $this->sess[ 'username' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $d = parent::fetch_array( $r );

                $fields = array(
                    'lcreated_by'   => $d[ 'lcreated_by' ],
                    'lcreated_date' => $d[ 'lcreated_date' ],
                    'ldlu'          => $d[ 'ldlu' ],
                    'luser_id'      => $d[ 'luser_id' ],
                    'lusername'     => $d[ 'lusername' ],
                    'lfirstname'    => $d[ 'lfirstname' ],
                    'llastname'     => $d[ 'llastname' ],
                    'lemail'        => $d[ 'lemail' ],
                    'laddress'      => $d[ 'laddress' ],
                    'lwebsite'      => $d[ 'lwebsite' ],
                    'lcompany'      => $d[ 'lcompany' ],
                    'ljob_title'    => $d[ 'ljob_title' ],
                    'lphone'        => $d[ 'lphone' ],
                    'lfax'          => $d[ 'lfax' ],
                    'lpostal_code'  => $d[ 'lpostal_code' ],
                    'lcity'         => $d[ 'lcity' ],
                    'lname'         => $d[ 'lname' ],
					'lbirthdate'    => $d[ 'lbirthdate' ],
					'labout'        => $d[ 'labout' ],
					'lfacebook'     => $d[ 'lfacebook' ],
					'linstagram'    => $d[ 'linstagram' ],
	                'lgender'       => $d[ 'lgender' ],
					'lusertype_id'  => $d[ 'lusertype_id' ],
                    'lagent_type'   => $d[ 'lagent_type' ],
                    'lcountry_id'   => $d[ 'lcountry_id' ],
                    'lorder_id'     => $d[ 'lorder_id' ],
                    'lstatus'       => $d[ 'lstatus' ],
                    'limage'        => $d[ 'limage' ],           
					'lpassword'     => '',
					'lcpassword'    => '',
					'lopassword'    => '',
                );
           	}
            else
            {
                $this->valid = false;
            }
        }
        else
        {
            $fields = array(
                'lcreated_by'   => $this->sess[ 'username' ],
                'lcreated_date' => time(),
                'ldlu'          => time(),
                'luser_id'      => time(),
                'lusername'     => '',
                'lfirstname'    => '',
                'llastname'     => '',
                'lemail'        => '',
                'laddress'      => '',
                'lwebsite'      => '',
                'lcompany'      => '',
                'ljob_title'    => '',
                'lphone'        => '',
                'lfax'          => '',
                'lpostal_code'  => '',
                'lcity'         => '',
                'lname'         => '',
				'lbirthdate'    => '',
				'labout'        => '',
				'lfacebook'     => '',
				'linstagram'    => '',
				'lpassword'     => '',
				'lcpassword'    => '',
				'lopassword'    => '',
                'limage'        => '',
				'lusertype_id'  => 2,
                'lgender'       => 0,
                'lagent_type'   => 0,
                'lcountry_id'   => 0,
                'lorder_id'     => 0,
                'lstatus'       => 1,
            );
        }

        if( empty( $_POST ) )
        {
            if( empty( $this->notif ) === false && isset( $this->notif[ 'post' ] ) )
            {
                $this->fields = array_merge( $fields, $this->notif[ 'post' ] );
            }
            else
            {
                $this->fields = $fields;
            }

            $this->post = $_POST;
        }
        else
        {
            if( isset( $_POST[ 'fields' ] ) )
            {
                $this->fields = array_merge( $fields, $_POST[ 'fields' ] );
            }
            else
            {
                $this->fields = $fields;
            }

            $this->post = array_diff_key( $_POST, array_flip( array( 'fields' ) ) );
        }
    }

    function request( $mod, $usertype )
    {
        if( $this->post[ 'pKEY' ] == 'delete_avatar' )
        {
            echo $this->delete_avatar();
        }        
    }
}