jQuery(document).ready(function(){
    init_rsv_date_range_picker();
    init_rsv_select2_ajax();
    init_rsv_table();
    init_rsv_popup();
});

function init_rsv_select2_ajax()
{
    if( jQuery('.sl2_ajax').length > 0 )
    {
        jQuery('.sl2_ajax').select2({
            allowClear: true,
            // ajax: {
            //     url: jQuery('#ajax_url').val(),
            //     dataType: 'json',
            //     type: 'post',
            //     delay: 100,
            //     data: function( params ){
            //         return {
            //             pKEY: 'load-accommodation',
            //             page: params.page || 1,
            //             search: params.term,
            //             length: 10,
            //         };
            //     },
            //     processResults: function( response, params ){
            //         params.page = params.page || 1;

            //         return {
            //             results: response.data,
            //             pagination: {
            //                 more: ( params.page * 10 ) < response.count
            //             }
            //         }
            //     },
            //     cache: true
            // }
        });
    }
}

function init_rsv_table()
{
    if( jQuery('#table_pcbooking').length > 0 )
    {
        var pctable = jQuery('#table_pcbooking').DataTable({
            lengthChange: false,
            deferRender: true,
            processing: true,
            serverSide: true,
            pageLength: 10,
            info: false,
            order: [],
            ajax: {
                url: jQuery('#ajax_url').val(),
                type: 'POST',
                data: function( d ){
                    d.search  = jQuery('[name=pcbsearchval]').val();
                    d.pKEY    = 'load';
                    d.lstatus = 0;
                }
            },
            columns: [
                {
                    data: 'lbooking_id', render: function ( data, type, row ){
                        var html = 
                        '<div class="td1 cutkform">'+
                            '<div class="form-group">'+
                                '<input id="pc_cb_' + data + '" name="bid[]" type="checkbox" class="batch-checkbox-select" value="' + data + '" id="cb' + data + '"  autocomplete="off">'+
                                '<label for="pc_cb_' + data + '"></label>'+
                            '</div>'+
                        '</div>';

                        return html;
                    },
                    className: 'text-center'
                },
                {
                    data: 'lbooking_code', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'lfullname', render: function ( data, type, row ){
                        var html = 
                        '<span>'+
                            '<b>' + data + '</b><br>'+ row.lguest_num
                        '</span>';

                        return html;
                    }
                },
                {
                    data: 'lcheck_in', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'lcheck_out', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'lcreated_date', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'ltitle', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'ltotal', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'ldetaillink', render: function ( data, type, row ){
                        return '<a href="' + data + '" class="linebtn">View Details</a>';
                    },
                    className: 'text-center'
                }
            ],
            dom: 'tp'
        });

        pctable.on( 'draw.dt', function () {
            init_table_action( this, pctable );
        });

        jQuery('[name=pcbsearchbtn]').on('click', function(){
            pctable.ajax.reload();
        });
    }

    if( jQuery('#table_ppbooking').length > 0 )
    {
        var pptable = jQuery('#table_ppbooking').DataTable({
            lengthChange: false,
            deferRender: true,
            processing: true,
            serverSide: true,
            pageLength: 10,
            info: false,
            order: [],
            ajax: {
                url: jQuery('#ajax_url').val(),
                type: 'POST',
                data: function( d ){
                    d.search          = jQuery('[name=ppbsearchval]').val();
                    d.pKEY            = 'load';
                    d.lstatus         = 1;
                    d.lstatus_payment = 0;
                }
            },
            columns: [
                {
                    data: 'lbooking_id', render: function ( data, type, row ){
                        var html = 
                        '<div class="td1 cutkform">'+
                            '<div class="form-group">'+
                                '<input id="pp_cb_' + data + '" name="bid[]" type="checkbox" class="batch-checkbox-select" value="' + data + '" id="cb' + data + '"  autocomplete="off">'+
                                '<label for="pp_cb_' + data + '"></label>'+
                            '</div>'+
                        '</div>';

                        return html;
                    },
                    className: 'text-center'
                },
                {
                    data: 'lbooking_code', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'lfullname', render: function ( data, type, row ){
                        var html = 
                        '<span>'+
                            '<b>' + data + '</b><br>2 adults'+
                        '</span>';

                        return html;
                    }
                },
                {
                    data: 'lcheck_in', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'lcheck_out', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'lcreated_date', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'ltitle', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'ltotal', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'ldetaillink', render: function ( data, type, row ){
                        return '<a href="' + data + '" class="linebtn">View Details</a>';
                    },
                    className: 'text-center'
                }
            ],
            dom: 'tp'
        });

        pptable.on( 'draw.dt', function () {
            init_table_action( this, pptable );
        });

        jQuery('[name=ppbsearchbtn]').on('click', function(){
            pptable.ajax.reload();
        });
    }

    if( jQuery('#table_reservations').length > 0 )
    {
        var rtable = jQuery('#table_reservations').DataTable({
            lengthChange: false,
            deferRender: true,
            processing: true,
            serverSide: true,
            pageLength: 10,
            info: false,
            order: [],
            ajax: {
                url: jQuery('#ajax_url').val(),
                type: 'POST',
                data: function( d ){
                    d.search  = jQuery('[name=rsearchval]').val();
                    d.acco_id = jQuery('[name=acco_id]').val();
                    d.lstatus = jQuery('[name=status]').val();
                    d.dstart  = jQuery('[name=start]').val();
                    d.dend    = jQuery('[name=end]').val();
                    d.pKEY    = 'load';
                }
            },
            columns: [
                {
                    data: 'lbooking_code', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'lfullname', render: function ( data, type, row ){
                        var html = 
                        '<span>'+
                            '<b>' + data + '</b><br>2 adults'+
                        '</span>';

                        return html;
                    }
                },
                {
                    data: 'lcheck_in', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'lcheck_out', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'lcreated_date', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'ltitle', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'ltotal', render: function ( data, type, row ){
                        return data;
                    }
                },
                {
                    data: 'ldetaillink', render: function ( data, type, row ){
                        return '<a href="' + data + '" class="linebtn">View Details</a>';
                    },
                    className: 'text-center'
                }
            ],
            dom: 'tp'
        });

        rtable.on( 'draw.dt', function () {
            init_table_action( this, rtable );
        });

        jQuery('[name=rbsearchbtn]').on('click', function(){
            rtable.ajax.reload();
        });
    }
}

function init_rsv_date_range_picker()
{
    if( jQuery('[name="period"]').length > 0 )
    {
        jQuery('[name="period"]').daterangepicker({ 
            autoUpdateInput: false,
            locale:{
                format: 'DD/MM/YY'
            }
        }).on('apply.daterangepicker', function(e, picker){
            var start = picker.startDate.format( picker.locale.format );
            var end   = picker.endDate.format( picker.locale.format );

            picker.element.val( start + ' - ' + end );
            
            jQuery('[name="start"]').val( picker.startDate.format( 'YYYY-MM-DD' ) );
            jQuery('[name="end"]').val( picker.endDate.format( 'YYYY-MM-DD' ) );
        }).on('cancel.daterangepicker', function(e, picker){
            picker.element.val('');

            jQuery('[name="start"]').val('');
            jQuery('[name="end"]').val('');
        });
    }
}

function init_rsv_popup()
{
    jQuery('.nbtn-confirm-booking').on('click', function(){
        var id  = jQuery(this).data('id');
        var bid = jQuery('#table_pcbooking tbody input[type=checkbox]:checked');

        if( bid.length > 0 || typeof id != 'undefined' )
        {
            jQuery.fancybox.open({
                src  : '#popup-confirm-booking',
                type : 'inline',
                opts : {
                    closeExisting: true,
                    afterShow : function( instance, current ){
                        current.$slide.find('.btn-confirm-no').unbind();
                        current.$slide.find('.btn-confirm-no').on('click', function(){
                            instance.close();
                        });

                        current.$slide.find('.btn-confirm-yes').unbind();
                        current.$slide.find('.btn-confirm-yes').on('click', function(){
                            var arr = new Array;

                            if( bid.length == 0 )
                            {
                                arr.push( id );

                                update_rsv_status( instance, current, arr, 1, true );
                            }
                            else
                            {
                                bid.each( function( i, e ){
                                    arr.push( jQuery(this).val() );
                                });

                                update_rsv_status( instance, current, arr, 1 );
                            }
                        });
                    }
                }
            });
        }
        else
        {
            alert( 'Please select "Booking ID" that you want to confirm first!' );
        }
    });

    jQuery('.nbtn-refuse-booking').on('click', function(){
        var id  = jQuery(this).data('id');
        var bid = jQuery('#table_pcbooking tbody input[type=checkbox]:checked');

        if( bid.length > 0 || typeof id != 'undefined' )
        {
            jQuery.fancybox.open({
                src  : '#popup-refuse-booking',
                type : 'inline',
                opts : {
                    closeExisting: true,
                    afterShow : function( instance, current ){
                        current.$slide.find('.btn-refuse-no').unbind();
                        current.$slide.find('.btn-refuse-no').on('click', function(){
                            instance.close();
                        });

                        current.$slide.find('.btn-refuse-yes').unbind();
                        current.$slide.find('.btn-refuse-yes').on('click', function(){
                            var arr = new Array;

                            if( bid.length == 0 )
                            {
                                arr.push( id );

                                update_rsv_status( instance, current, arr, 2, true );
                            }
                            else
                            {
                                bid.each( function( i, e ){
                                    arr.push( jQuery(this).val() );
                                });

                                update_rsv_status( instance, current, arr, 2 );
                            }
                        });
                    }
                }
            });
        }
        else
        {
            alert( 'Please select "Booking ID" that you want to refuse first!' );
        }
    });

    jQuery('.nbtn-change-date').on('click', function(){
        var id  = jQuery(this).data('id');
        var bid = jQuery('#table_pcbooking tbody input[type=checkbox]:checked');

        if( bid.length == 1 || typeof id != 'undefined' )
        {
            jQuery.fancybox.open({
                src  : '#popup-change-date',
                type : 'inline',
                opts : {
                    closeExisting: true,
                    afterShow : function( instance, current ){
                        current.$slide.find('[name="lcheck_in"], [name="lcheck_out"]').daterangepicker({
                            parentEl: current.$slide,
                            autoUpdateInput: false,
                            autoApply: true,
                            locale:{
                                format: 'DD MMMM YYYY'
                            }
                        }, function( start, end, label ) {
                            var sdate = start.format( 'DD MMMM YYYY' );
                            var edate = end.format( 'DD MMMM YYYY' );
                            
                            current.$slide.find('[name="lcheck_in"]').val( sdate );
                            current.$slide.find('[name="lcheck_out"]').val( edate );

                            current.$slide.find('[name="lcheck_in"]').data('daterangepicker').setStartDate( sdate );
                            current.$slide.find('[name="lcheck_in"]').data('daterangepicker').setEndDate( edate );
                            
                            current.$slide.find('[name="lcheck_out"]').data('daterangepicker').setStartDate( sdate );
                            current.$slide.find('[name="lcheck_out"]').data('daterangepicker').setEndDate( edate );
                        });

                        current.$slide.find('.btn-send').unbind();
                        current.$slide.find('.btn-send').on('click', function(){
                            if( bid.length == 0 )
                            {
                                update_rsv_date( instance, current, id, true );
                            }
                            else
                            {
                                update_rsv_date( instance, current, bid.val() );
                            }
                        });
                    }
                }
            });
        }
        else if( bid.length > 1 )
        {
            alert( 'Sorry, you can only change one reservation at a time!' );
        }
        else
        {
            alert( 'Please select "Booking ID" that you want to change first!' );
        }
    });
}

function update_rsv_date( instance, current, bid, reload )
{
    var url = jQuery('#ajax_url').val();
    var prm = new Object;
        prm.pKEY        = 'update-date';
        prm.lcheck_out  = current.$slide.find('[name=lcheck_out]').val();
        prm.lcheck_in   = current.$slide.find('[name=lcheck_in]').val();
        prm.lmessage    = current.$slide.find('[name=lmessage]').val();
        prm.lbooking_id = bid;

    jQuery.ajax({
        url: url,
        data: prm,
        type: 'POST',
        dataType : 'json',
        beforeSend: function(xhr){
            current.$slide.find('.popup').addClass('popup-processing');
        },
        success: function(e){
            if( e.result == 'success' )
            {
                if( jQuery('#table_pcbooking').length > 0 )
                {
                    jQuery('#table_pcbooking').DataTable().ajax.reload();
                }

                if( jQuery('#table_ppbooking').length > 0 )
                {
                    jQuery('#table_ppbooking').DataTable().ajax.reload();
                }

                instance.close();
            
                if( reload )
                {
                    location.reload();
                }
                else
                {
                    alert( e.message );
                }
            }
            else
            {
                alert( e.message );
            }
        },
        error: function(e){
           alert( 'Something wrong on the request' );
        }
    }).always(function() {
        current.$slide.find('.popup').removeClass('popup-processing');
    });
}

function update_rsv_status( instance, current, bid, status, reload )
{
    var url = jQuery('#ajax_url').val();
    var prm = new Object;
        prm.pKEY        = 'update-status';
        prm.lstatus     = status;
        prm.lbooking_id = bid;

    jQuery.ajax({
        url: url,
        data: prm,
        type: 'POST',
        dataType : 'json',
        beforeSend: function(xhr){
            current.$slide.find('.popup').addClass('popup-processing');
        },
        success: function(e){
            if( e.result == 'success' )
            {
                if( jQuery('#table_pcbooking').length > 0 )
                {
                    jQuery('#table_pcbooking').DataTable().ajax.reload();
                }

                if( jQuery('#table_ppbooking').length > 0 )
                {
                    jQuery('#table_ppbooking').DataTable().ajax.reload();
                }

                instance.close();
            
                if( reload )
                {
                    location.reload();
                }
                else
                {
                    alert( e.message );
                }
            }
            else
            {
                alert( e.message );
            }
        },
        error: function(e){
           alert( 'Something wrong on the request' );
        }
    }).always(function() {
        current.$slide.find('.popup').removeClass('popup-processing');
    });
}