<?php

class reservations extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }

    function view( $mod, $usertype, $type = 'booking' )
    {
        $this->template->set_file( 'view', 'view-' . $type . '.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $this->global->getPViewBothP( $mod, $usertype ) )
        {
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );
            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/momentjs/latest/moment.min.js' ) );
            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js' ) );
            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.css' ) );

            if( $type == 'booking' )
            {
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js' ) );
                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css' ) );
            }
            else
            {
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js' ) );
                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css' ) );
            }

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( HT_SERVER . MODULES_THEME_URL . '/apps/reservations/apps.js' ) );

            $this->template->Parse( 'vC', 'viewContent', false );
        }
        else
        {
            $this->template->set_var( 'error', '<div class="error">' . NO_AUTHO . '</div>' );
        }
        
        $this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function details( $bid, $mod, $usertype )
    {
        $d = $this->global->getBooking( $bid );

        if( empty( $d ) )
        {
            header( 'Location:' . HT_SERVER . SITE_URL . '/not-found/' );

            exit;
        }
        else
        {
            if( $d[ 'lstatus' ] == 0 || ( $d[ 'lstatus' ] == 1 && $d[ 'lstatus_payment' ] == 0 ) )
            {
                $type = 'request';
            }
            else
            {
                $type = 'booking';
            }

            $this->template->set_file( 'detail', 'detail-' . $type . '.html' );

            $this->template->set_block( 'detail', 'detailContent', 'dC' );
            $this->template->set_block( 'detail', 'detailBlock', 'dB' );

            if( $this->global->getPViewBothP( $mod, $usertype ) )
            {
                $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );
                
                if( $d[ 'lstatus' ] == 0 )
                {
                    $this->template->set_var( 'ptitle', 'BOOKING REQUEST DETAILS - ID ' . $d[ 'lbooking_code' ] );                
                    $this->template->set_var( 'action_class', '' );
                }
                elseif( $d[ 'lstatus' ] == 1 && $d[ 'lstatus_payment' ] == 0 )
                {
                    $this->template->set_var( 'ptitle', 'PENDING PAYMENT DETAILS - ID ' . $d[ 'lbooking_code' ] );
                    $this->template->set_var( 'action_class', 'hidden' );                  
                }
                else
                {
                    $this->template->set_var( 'ptitle', 'RESERVATIONS DETAILS - ID ' . $d[ 'lbooking_code' ] );
                    $this->template->set_var( 'action_class', 'hidden' );             
                }

                if( $d[ 'lrooms' ] != '' )
                {
                    $list = array();

                    foreach( $d[ 'lrooms' ] as $dt )
                    {
                        $list[] = sprintf( '%s %s<span>%s</span>', $dt[ 'num' ], $dt[ 'name' ], $this->global->get_format_price( $dt[ 'total' ], $d[ 'lbooking_id' ] ) );
                    }

                    $this->template->set_var( 'rooms', implode( '<br/>', $list ) );
                }
                else
                {
                    $this->template->set_var( 'rooms', '-' );
                }

                if( $d[ 'laddons' ] != '' )
                {
                    $list = array();

                    foreach( $d[ 'laddons' ] as $dt )
                    {
                        $list[] = sprintf( '%s %s<span>%s</span>', $dt[ 'num' ], $dt[ 'name' ], $this->global->get_format_price( $dt[ 'total' ], $d[ 'lbooking_id' ] ) );
                    }

                    $this->template->set_var( 'addons', implode( '<br/>', $list ) );
                }
                else
                {
                    $this->template->set_var( 'addons', '-' );
                    $this->template->set_var( 'addons_price', '' );
                }

                $this->template->set_var( 'bid', $d[ 'lbooking_id' ] );
                $this->template->set_var( 'note', empty( $d[ 'lnote' ] ) ? '-' : nl2br( $d[ 'lnote' ] ) ) ;
                $this->template->set_var( 'guest', sprintf( '%s %s', $d[ 'ltitle' ], $d[ 'lfullname' ] ) );

                $this->template->set_var( 'listing_name', $d[ 'lposts_data' ][ 'ltitle' ] );
                $this->template->set_var( 'listing_location', $this->global->getLocations( $d[ 'lposts_data' ] ) );

                $this->template->set_var( 'check_in', date( 'M d, Y', $d[ 'lcheck_in' ] ) );
                $this->template->set_var( 'check_out', date( 'M d, Y', $d[ 'lcheck_out' ] ) );

                $this->template->set_var( 'total', $this->global->get_format_price( $d[ 'ltotal' ], $d[ 'lbooking_id' ] ) );
                $this->template->set_var( 'deposit', $this->global->get_format_price( $d[ 'ldeposit' ], $d[ 'lbooking_id' ] ) );
                $this->template->set_var( 'balance', $this->global->get_format_price( $d[ 'lbalance' ], $d[ 'lbooking_id' ] ) );
                $this->template->set_var( 'guest_num', $this->global->get_format_guest_num( $d[ 'ladult' ], $d[ 'lchild' ], $d[ 'linfant' ] ) );

                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/momentjs/latest/moment.min.js' ) );
                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js' ) );
                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.css' ) );

                $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js' ) );
                $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/@fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css' ) );

                $this->actions->add_actions( 'jvs', $this->global->get_jvs( HT_SERVER . MODULES_THEME_URL . '/apps/reservations/apps.js' ) );

                $this->template->Parse( 'dC', 'detailContent', false );
            }
            else
            {
                $this->template->set_var( 'error', '<div class="error">' . NO_AUTHO . '</div>' );
            }
        
            $this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );

            return $this->template->Parse( 'dB', 'detailBlock', false );
        }
    }

    function load()
    {
        $cols  = array(
            1 => 'a.lbooking_code',
            2 => 'a.lfullname',
            3 => 'a.lcheck_in',
            4 => 'a.lcheck_out',
            5 => 'a.lcreated_date',
            6 => 'b.ltitle',
            7 => 'a.ltotal',
        );

        //-- Query Limit
        $limit = parent::prepare_query( ' LIMIT ' . $this->post[ 'start' ] . ', ' . $this->post[ 'length' ] );
    
        //-- Query Order By
        if( empty( $this->post[ 'order' ] ) )
        {
            $order = parent::prepare_query( ' ORDER BY a.lbooking_id DESC' );
        }
        else
        {
            $order = array();

            foreach( $this->post[ 'order' ] as $i => $od )
            {
                if( isset( $cols[ $od[ 'column' ] ] ) )
                {
                    $order[] = $cols[ $od[ 'column' ] ] . ' ' . $od[ 'dir' ];
                }
            }

            if( empty( $order ) )
            {
                $order = parent::prepare_query( ' ORDER BY a.lbooking_id DESC' );
            }
            else
            {
                $order = parent::prepare_query( ' ORDER BY ' . implode( ', ', $order ) );
            }
        }

        //-- Main Conditional
        $where = parent::prepare_query( ' WHERE b.lcreated_by = %s', $this->sess[ 'username' ] );

        //-- Query Additional Where
        $h = array();

        if( $this->post[ 'lstatus' ] != '' )
        {
            $h[] = parent::prepare_query( 'a.lstatus = %d', $this->post[ 'lstatus' ] );
        }
        
        if( $this->post[ 'lstatus_payment' ] != '' )
        {
            $h[] = parent::prepare_query( 'a.lstatus_payment = %d', $this->post[ 'lstatus_payment' ] );
        }

        if( $this->post[ 'acco_id' ] != '' )
        {
            $h[] = parent::prepare_query( 'a.lpost_id = %d', $this->post[ 'acco_id' ] );
        }

        if( $this->post[ 'dstart' ] != '' && $this->post[ 'dend' ] != '' )
        {
            $h[] = parent::prepare_query( '( DATE_FORMAT( FROM_UNIXTIME( a.lcreated_date ), %s ) >= %s AND DATE_FORMAT( FROM_UNIXTIME( a.lcreated_date ), %s ) <= %s )', '%Y-%m-%d', date( 'Y-m-d', strtotime( $this->post[ 'dstart' ] ) ), '%Y-%m-%d', date( 'Y-m-d', strtotime( $this->post[ 'dend' ] ) ) );
        }

        if( $this->post[ 'search' ] != '' )
        {
            $s = array();

            foreach( $cols as $col )
            {
                $s[] = parent::prepare_query( $col . ' LIKE %s', '%' . $this->post[ 'search' ] . '%' );
            }

            $h[] = sprintf( '(%s)', implode( ' OR ', $s ) );
        }

        if( empty( $h ) === false )
        {
            $having = ' HAVING ' . implode( ' AND ', $h );
        }
        else
        {
            $having = '';
        }

        //-- Main Query
        $qm = 'SELECT
                a.lbooking_id,
                a.lbooking_code,
                a.lstatus_payment,
                a.lstatus,
                a.lcreated_date,
                a.lcheck_in,
                a.lcheck_out,
                a.ladult,
                a.lchild,
                a.linfant,
                a.lfullname,
                a.lpost_id,
                a.ltotal,
                b.ltitle,
                b.lcreated_by
              FROM lumonata_booking AS a 
              LEFT JOIN lumonata_post AS b ON a.lpost_id = b.lpost_id' . $where . $having . $order;
        $rm = parent::query( $qm );
        $nm = parent::num_rows( $rm );

        $qf = $qm . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        $data = array();

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
                $data[] = array(
                    'ldetaillink'   => HT_SERVER . SITE_URL . '/agent/booking-details/' . $df[ 'lbooking_id' ],
                    'lguest_num'    => $this->global->get_format_guest_num( $df[ 'ladult' ], $df[ 'lchild' ], $df[ 'linfant' ] ),
                    'ltotal'        => $this->global->get_format_price( $df[ 'ltotal' ] ),
                    'lcreated_date' => date( 'M d, Y', $df[ 'lcreated_date' ] ),
                    'lcheck_out'    => date( 'M d, Y', $df[ 'lcheck_out' ] ),
                    'lcheck_in'     => date( 'M d, Y', $df[ 'lcheck_in' ] ),
                    'lbooking_code' => $df[ 'lbooking_code' ],
                    'lbooking_id'   => $df[ 'lbooking_id' ],
                    'lfullname'     => $df[ 'lfullname' ],
                    'ltitle'        => $df[ 'ltitle' ],
                );
            }
        }

        return json_encode( array(
            'draw'            => intval( $this->post[ 'draw' ] ),
            'recordsFiltered' => intval( $nm ),
            'recordsTotal'    => intval( $nf ),
            'data'            => $data
        ));
    }

    function load_accommodation()
    {
        //-- Main Order
        $order = parent::prepare_query( ' ORDER BY a.ltitle ASC' );

        //-- Main Conditional
        $where = parent::prepare_query( ' WHERE a.lcreated_by = %s AND a.ltype = %s', $this->sess[ 'username' ], 'accommodation' );

        //-- Limit
        $limit = parent::prepare_query( ' LIMIT %d, %d', ( ( $this->post[ 'page' ] - 1 ) * $this->post[ 'length' ] ), $this->post[ 'length' ] );

        //-- Query Additional Where
        $having = '';

        if( $this->post[ 'search' ] != '' )
        {
            $having = parent::prepare_query( ' HAVING a.ltitle LIKE %s', '%' . $this->post[ 'search' ] . '%' );
        }

        //-- Main Query
        $qm = 'SELECT a.lpost_id, a.ltitle FROM lumonata_post AS a' . $where . $having . $order;
        $rm = parent::query( $qm );
        $nm = parent::num_rows( $rm );

        //-- Paging Query
        $qf = $qm . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        $data = array();

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
                $data[] = array(
                    'id'   => $df[ 'lpost_id' ],
                    'text' => $df[ 'ltitle' ]
                );
            }
        }

        return json_encode( array(
            'data'  => $data,
            'count' => $nm
        ));
    }

    function update_date()
    {
        if( empty( $this->post[ 'lbooking_id' ] ) )
        {
            return json_encode( array( 'result'  => 'failed', 'message' => 'Please select "Booking ID" that you want to update first!' ) );
        }
        else
        {
            parent::begin();

            $commit = 1;

            $r = parent::update( 'lumonata_booking', array( 'lstatus' => 3 ), array( 'lbooking_id' => $this->post[ 'lbooking_id' ] ) );

            if( is_array( $r ) )
            {
                $commit = 0;
            }
            else
            {       
                //-- GET module
                $mod = $this->global->getFields( 'lumonata_module', 'lmodule_id', array( 'lappsagent' => 'reservations' ) );

                //-- CHECK data already exist or not
                $n = $this->global->getRowsNumber( 'lumonata_additional_field', array( 
                    'lapp_id'         => $this->post[ 'lbooking_id' ], 
                    'ladditional_key' => 'alternative_rsv_date', 
                    'lmodule_id'      => $mod
                ));

                if( $n > 0 )
                {
                    //-- UPDATE exist data
                    $r = parent::update( 'lumonata_additional_field', array(  
                        'lapp_id'           => $this->post[ 'lbooking_id' ],
                        'ladditional_key'   => 'alternative_rsv_date',
                        'lmodule_id'        => $mod,
                        'ladditional_value' => json_encode( array(
                            'lcheck_out' => strtotime( $this->post[ 'lcheck_out' ] ),
                            'lcheck_in'  => strtotime( $this->post[ 'lcheck_in' ] ),
                            'lmessage'   => $this->post[ 'lmessage' ],
                        ))
                    ), array( 
                        'lapp_id'         => $this->post[ 'lbooking_id' ],
                        'ladditional_key' => 'alternative_rsv_date',
                        'lmodule_id'      => $mod,
                    ));
                }
                else
                {
                    //-- INSERT new data
                    $r = parent::insert( 'lumonata_additional_field', array( 
                        'lapp_id'           => $this->post[ 'lbooking_id' ],
                        'ladditional_key'   => 'alternative_rsv_date',
                        'lmodule_id'        => $mod,
                        'ladditional_value' => json_encode( array(
                            'lcheck_out' => strtotime( $this->post[ 'lcheck_out' ] ),
                            'lcheck_in'  => strtotime( $this->post[ 'lcheck_in' ] ),
                            'lmessage'   => $this->post[ 'lmessage' ],
                        ))
                    ));
                }

                if( is_array( $r ) )
                {
                    $commit = 0;
                }
            }

            if( $commit == 0 )
            {
                parent::rollback();

                return json_encode( array( 'result' => 'failed', 'message' => 'Something wrong! Can\'t set new alternative date for this booking! please try again later' ) );
            }
            else
            {
                parent::commit();

                $this->global->sendBookingChangedDateEmail( $this->post[ 'lbooking_id' ] );

                return json_encode( array( 'result' => 'success', 'message' => 'New alternative date has been set! System will be sending notification to your client' ) );
            }
        }
    }

    function update_status()
    {
        if( empty( $this->post[ 'lbooking_id' ] ) )
        {
            return json_encode( array( 'result'  => 'failed', 'message' => 'Please select "Booking ID" that you want to confirm first!' ) );
        }
        else
        {
            parent::begin();

            $commit = 1;

            foreach( $this->post[ 'lbooking_id' ] as $bid )
            {
                $r = parent::update( 'lumonata_booking', array( 'lstatus' => $this->post[ 'lstatus' ] ), array( 'lbooking_id' => $bid ) );

                if( is_array( $r ) )
                {
                    $commit = 0;
                }
                else
                {
                    if( $this->post[ 'lstatus' ] == 1 )
                    {
                        $this->global->sendBookingAcceptedEmail( $bid );
                    }
                    else
                    {
                        $this->global->sendBookingRefusedEmail( $bid );
                    }
                }
            }

            if( $commit == 0 )
            {
                parent::rollback();

                if( $this->post[ 'lstatus' ] == 1 )
                {
                    return json_encode( array( 'result' => 'failed', 'message' => 'Something wrong! Booking data that you choose can\'t be confirm, please try again later' ) );
                }
                else
                {
                    return json_encode( array( 'result' => 'failed', 'message' => 'Something wrong! Booking data that you choose can\'t be refuse, please try again later' ) );
                }
            }
            else
            {
                parent::commit();

                if( $this->post[ 'lstatus' ] == 1 )
                {
                    return json_encode( array( 'result' => 'success', 'message' => 'Booking data that you choose has been confirmed! System will be sending invoice to your client' ) );
                }
                else
                {
                    return json_encode( array( 'result' => 'success', 'message' => 'Booking data that you choose has been refused! System will be sending notification to your client' ) );
                }
            }
        }
    }

    function language()
    {
        $this->lang = $this->global->getSettingValue( 'llanguage' );
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();
        $this->post    = $_POST;
    }

    function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'load' )
            {
                echo $this->load();
            }

            if( $this->post[ 'pKEY' ] == 'load-accommodation' )
            {
                echo $this->load_accommodation();
            }

            if( $this->post[ 'pKEY' ] == 'update-status' )
            {
                echo $this->update_status();
            }

            if( $this->post[ 'pKEY' ] == 'update-date' )
            {
                echo $this->update_date();
            }
        }
    }
}