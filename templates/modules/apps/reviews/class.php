<?php

class reviews extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }

    function view( $mod, $usertype )
    {
        if( isset( $this->fields ) )
        {
            if( empty( $this->fields[ 'lreview_id' ] ) )
            {
                return $this->insert( $mod, $usertype );
            }
            else
            {
                return $this->edit( $mod, $usertype );
            }
        }
        else
        {
            header( 'Location:' . HT_SERVER . SITE_URL . '/not-found/' );

            exit;
        }
    }

    function insert( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->global->getPInsertBoth( $mod, $usertype ) )
        {
            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->create( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'lpost_id', $lpost_id );
            $this->template->set_var( 'lreview_id', $lreview_id );
            $this->template->set_var( 'lbooking_id', $lbooking_id );
            $this->template->set_var( 'lreview_note', $lreview_note );
            $this->template->set_var( 'lreview_title', $lreview_title );

            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'lname', $this->global->get_user_name( $this->sess[ 'userid' ] ) );
            $this->template->set_var( 'ltitle', strtoupper( $this->global->getListingTitle( $this->global->getPosts( array( 'lpost_id' => $lpost_id ) ) ) ) );

            $this->template->set_var( 'lstaff_score', $this->set_score_option( 'lstaff_score', $lstaff_score ) );
            $this->template->set_var( 'loverall_score', $this->set_score_option( 'loverall_score', $loverall_score ) );
            $this->template->set_var( 'laccuracy_score', $this->set_score_option( 'laccuracy_score', $laccuracy_score ) );
            $this->template->set_var( 'llocation_score', $this->set_score_option( 'llocation_score', $llocation_score ) );
            $this->template->set_var( 'lactivities_score', $this->set_score_option( 'lactivities_score', $lactivities_score ) );
            $this->template->set_var( 'lcleanliness_score', $this->set_score_option( 'lcleanliness_score', $lcleanliness_score ) );

            $this->template->Parse( 'fC', 'formContent', false );
        }
        else
        {
            $this->template->set_var( 'error', '<div class="error">' . NO_AUTHO . '</div>' );
        }
    
        $this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );

        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function edit( $mod, $usertype )
    {
        $this->template->set_file( 'form', 'form.html' );

        $this->template->set_block( 'form', 'formContent', 'fC' );
        $this->template->set_block( 'form', 'formBlock', 'fB' );

        if( $this->global->getPEditBoth( $mod, $usertype ) )
        {
            //-- EXECUTE save action
            if( isset( $this->post[ 'save' ] ) )
            {
                $this->change( $mod );
            }

            //-- EXTRACT fields
            extract( $this->fields );

            $this->template->set_var( 'lpost_id', $lpost_id );
            $this->template->set_var( 'lreview_id', $lreview_id );
            $this->template->set_var( 'lbooking_id', $lbooking_id );
            $this->template->set_var( 'lreview_note', $lreview_note );
            $this->template->set_var( 'lreview_title', $lreview_title );

            $this->template->set_var( 'message', $this->message );
            $this->template->set_var( 'lname', $this->global->get_user_name( $this->sess[ 'userid' ] ) );
            $this->template->set_var( 'ltitle', strtoupper( $this->global->getListingTitle( $this->global->getPosts( array( 'lpost_id' => $lpost_id ) ) ) ) );

            $this->template->set_var( 'lstaff_score', $this->set_score_option( 'lstaff_score', $lstaff_score ) );
            $this->template->set_var( 'loverall_score', $this->set_score_option( 'loverall_score', $loverall_score ) );
            $this->template->set_var( 'laccuracy_score', $this->set_score_option( 'laccuracy_score', $laccuracy_score ) );
            $this->template->set_var( 'llocation_score', $this->set_score_option( 'llocation_score', $llocation_score ) );
            $this->template->set_var( 'lactivities_score', $this->set_score_option( 'lactivities_score', $lactivities_score ) );
            $this->template->set_var( 'lcleanliness_score', $this->set_score_option( 'lcleanliness_score', $lcleanliness_score ) );

            $this->template->Parse( 'fC', 'formContent', false );
        }
        else
        {
            $this->template->set_var( 'error', '<div class="error">' . NO_AUTHO . '</div>' );
        }
    
        $this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );

        return $this->template->Parse( 'fB', 'formBlock', false );
    }

    function validate( $data = array() )
    {
        $error = array();

        foreach( $data as $field => $dt )
        {
            if( $field == 'lreview_note' && $dt == '' )
            {
                array_push( $error, 'You must write your review' );
            }
        }

        return $error;
    }

    function create( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- INSERT lumonata_reviews
            $param  = array_diff_key( $data, array_flip( array( 'lreview_id' ) ) );

            $result = parent::insert( 'lumonata_reviews', $param );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Failed to add new review', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/member/trip-review/' . $this->bid );

                exit;
            }
            else
            {
                $this->flash->add( array( 'type' => 'success', 'message' => 'Successfully write new review' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/member/trip-review/' . $this->bid );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/member/trip-review/' . $this->bid );

            exit;
        }
    }

    function change( $mod )
    {
        $data  = $this->fields;
        $error = $this->validate( $data );

        if( empty( $error ) )
        {
            //-- UPDATE lumonata_reviews
            $param  = array_diff_key( $data, array_flip( array( 'lreview_id' ) ) );
            $where  = array( 'lreview_id' => $data[ 'lreview_id' ] );

            $result = parent::update( 'lumonata_reviews', $param, $where );

            if( is_array( $result ) )
            {
                $this->flash->add( array( 'type' => 'error', 'message' => 'Your review failed to update', 'post' => $data ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/member/trip-review/' . $this->bid );

                exit;
            }
            else
            {
                $this->flash->add( array( 'type' => 'success', 'message' => 'Your review has been updated' ) );

                header( 'location: ' . HT_SERVER . SITE_URL . '/member/trip-review/' . $this->bid );

                exit;
            }
        }
        else
        {
            $this->flash->add( array( 'type' => 'error', 'message' => $error, 'post' => $data ) );

            header( 'location: ' . HT_SERVER . SITE_URL . '/member/trip-review/' . $this->bid );

            exit;
        }
    }

    function set_score_option( $name, $score = 0 )
    {
        $random = substr( base_convert( md5( rand() ), 16, 32 ), 0, 12 );
        $option = '
        <ul class="rate-area">
            <input type="radio" id="5-' . $random . '" name="fields[' . $name . ']" value="5" ' . ( $score == 5 ? 'checked' : '' ) . ' /><label for="5-' . $random . '" title="Amazing">&nbsp;</label>
            <input type="radio" id="4-' . $random . '" name="fields[' . $name . ']" value="4" ' . ( $score == 4 ? 'checked' : '' ) . ' /><label for="4-' . $random . '" title="Good">&nbsp;</label>
            <input type="radio" id="3-' . $random . '" name="fields[' . $name . ']" value="3" ' . ( $score == 3 ? 'checked' : '' ) . ' /><label for="3-' . $random . '" title="Average">&nbsp;</label>
            <input type="radio" id="2-' . $random . '" name="fields[' . $name . ']" value="2" ' . ( $score == 2 ? 'checked' : '' ) . ' /><label for="2-' . $random . '" title="Not Good">&nbsp;</label>
            <input type="radio" id="1-' . $random . '" name="fields[' . $name . ']" value="1" ' . ( $score == 1 ? 'checked' : '' ) . ' /><label for="1-' . $random . '" title="Bad">&nbsp;</label>
        </ul>';

        return $option;
    }

    function language()
    {
        $this->lang = $this->global->getSettingValue( 'llanguage' );
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();
        $this->bid     = null;

        $path = explode( '/', ltrim( $_SERVER['REQUEST_URI'], '/' ) );

        if( isset( $path[ 2 ] ) && !empty( $path[ 2 ] ) )
        {
            $this->bid = $path[ 2 ];

            $s = 'SELECT * FROM lumonata_reviews AS a WHERE a.lbooking_id = %d AND a.luser_id = %d';
            $q = parent::prepare_query( $s, $this->bid, $this->sess[ 'userid' ] );
            $r = parent::query( $q );

            if( parent::num_rows( $r ) > 0 )
            {
                $d = parent::fetch_array( $r );

                $fields = array(
                    'luser_id'           => $d[ 'luser_id' ],
                    'lcreated_date'      => $d[ 'lcreated_date' ],
                    'lreview_id'         => $d[ 'lreview_id' ],
                    'lname'              => $d[ 'lname' ],
                    'lpost_id'           => $d[ 'lpost_id' ],
                    'lbooking_id'        => $d[ 'lbooking_id' ],
                    'lreview_title'      => $d[ 'lreview_title' ],
                    'lreview_note'       => $d[ 'lreview_note' ],
                    'laccuracy_score'    => $d[ 'laccuracy_score' ],
                    'llocation_score'    => $d[ 'llocation_score' ],
                    'lactivities_score'  => $d[ 'lactivities_score' ],
                    'lcleanliness_score' => $d[ 'lcleanliness_score' ],
                    'lstaff_score'       => $d[ 'lstaff_score' ],
                    'loverall_score'     => $d[ 'loverall_score' ],
                    'lstatus'            => $d[ 'lstatus' ],
                    'ldlu'               => time(),
                );
            }
            else
            {
                $d = $this->global->getBooking( $this->bid );

                $fields = array(
                    'lname'              => $this->global->get_user_name( $this->sess[ 'userid' ] ),
                    'luser_id'           => $this->sess[ 'userid' ],
                    'lbooking_id'        => $this->bid,
                    'lpost_id'           => $d[ 'lposts_data' ][ 'lpost_id' ],
                    'lcreated_date'      => time(),
                    'ldlu'               => time(),
                    'lreview_id'         => '',
                    'lreview_title'      => '', 
                    'lreview_note'       => '', 
                    'laccuracy_score'    => 0,
                    'llocation_score'    => 0,
                    'lactivities_score'  => 0,
                    'lcleanliness_score' => 0,
                    'lstaff_score'       => 0,
                    'loverall_score'     => 0,
                    'lstatus'            => 1
                );
            }

            if( empty( $_POST ) )
            {
                if( empty( $this->notif ) === false && isset( $this->notif[ 'post' ] ) )
                {
                    $this->fields = array_merge( $fields, $this->notif[ 'post' ] );
                }
                else
                {
                    $this->fields = $fields;
                }

                $this->post = array();
            }
            else
            {
                if( isset( $_POST[ 'fields' ] ) )
                {
                    $this->fields = array_merge( $fields, $_POST[ 'fields' ] );
                }
                else
                {
                    $this->fields = $fields;
                }

                $this->post = array_diff_key( $_POST, array_flip( array( 'fields' ) ) );
            }
        }
    }

    function request( $mod, $usertype )
    {

    }
}