jQuery(document).ready(function(){
    init_trip_select2();
    init_trip_table();
});

function init_trip_select2()
{
    if( jQuery('.sl2_ajax').length > 0 )
    {
        jQuery('.sl2_ajax').select2({
            allowClear: true
        });
    }
}

function init_trip_table()
{
    if( jQuery('#table_trips').length > 0 )
    {
        var pctable = jQuery('#table_trips').DataTable({
            lengthChange: false,
            deferRender: true,
            processing: true,
            serverSide: true,
            pageLength: 10,
            info: false,
            order: [],
            ajax: {
                url: jQuery('#ajax_url').val(),
                type: 'POST',
                data: function( d ){
                    d.lfilter = jQuery('[name=filter]').val();
                    d.pKEY   = 'load';
                }
            },
            columns: [
                {
                    data: 'ltripstatus', render: function ( data, type, row ){
                        var html =
                        '<div class="td1">'+
                            '<h3>' + data + '</h3>'+
                        '</div>';

                        return html;
                    }
                },
                {
                    data: 'ltripdate', render: function ( data, type, row ){
                        var html =
                        '<a class="dnl flex" href="' + row.laccommlink + '" target="_blank">'+
                            '<figure>'+
                                '<img src="' + row.lthumbnail + '" class="cover">'+
                            '</figure>'+
                            '<span>' + data + '<br/>' + row.ltitle + '</span>'+
                        '</a>';

                        return html;
                    }
                },
                {
                    data: 'lhost', render: function ( data, type, row ){
                        var html =
                        '<div class="host flex">'+
                            '<figure>'+
                                '<img src="' + row.lagentimg + '" class="cover">'+
                            '</figure>'+
                            '<span>' + data + '<br/>' + row.lagentphone + '</span>'+
                        '</div>';

                        return html;
                    }
                },
                {
                    data: 'ltotal', render: function ( data, type, row ){
                        var html = 
                        '<span>' + data + '</span>';

                        return html;
                    }
                },
                {
                    data: 'ldetaillink', render: function ( data, type, row ){
                        return '<a href="' + data + '" class="linebtn">View Details</a>';
                    },
                    className: 'text-center'
                }
            ],
            dom: 'tp'
        });

        pctable.on( 'draw.dt', function () {
            init_table_action( this, pctable );
        });

        jQuery('[name=filter]').on('change', function(){
            pctable.ajax.reload();
        });
    }
}