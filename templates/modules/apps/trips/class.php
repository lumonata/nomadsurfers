<?php

class trips extends db
{
    function __construct( $template_dir = '.', $actions = '' )
    {
        parent::__construct();
        
        $this->template = new Template( $template_dir );
        $this->flash    = new flash_message();
        $this->global   = new globalAdmin();
        $this->actions  = $actions;

        $this->language();
        $this->param();
    }

    function view( $mod, $usertype )
    {
        $this->template->set_file( 'view', 'view.html' );

        $this->template->set_block( 'view', 'viewContent', 'vC' );
        $this->template->set_block( 'view', 'viewBlock', 'vB' );

        if( $this->global->getPViewBothP( $mod, $usertype ) )
        {
            $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js' ) );
            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( 'https://cdn.jsdelivr.net/npm/datatables.net@1.11.3/js/jquery.dataTables.min.js' ) );
            $this->actions->add_actions( 'css', $this->global->get_css( 'https://cdn.jsdelivr.net/npm/datatables.net-dt@1.11.3/css/jquery.dataTables.min.css' ) );

            $this->actions->add_actions( 'jvs', $this->global->get_jvs( HT_SERVER . MODULES_THEME_URL . '/apps/trips/apps.js' ) );

            $this->template->Parse( 'vC', 'viewContent', false );
        }
        else
        {
            $this->template->set_var( 'error', '<div class="error">' . NO_AUTHO . '</div>' );
        }
        
        $this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );

        return $this->template->Parse( 'vB', 'viewBlock', false );
    }

    function details( $bid, $mod, $usertype )
    {
        $d = $this->global->getBooking( $bid );

        if( empty( $d ) )
        {
            header( 'Location:' . HT_SERVER . SITE_URL . '/not-found/' );

            exit;
        }
        else
        {
            $this->template->set_file( 'detail', 'detail.html' );

            $this->template->set_block( 'detail', 'detailContent', 'dC' );
            $this->template->set_block( 'detail', 'detailBlock', 'dB' );

            if( $this->global->getPViewBothP( $mod, $usertype ) )
            {
                $this->template->set_var( 'ajax_url', $this->global->getAjaxUrl( $mod, $usertype ) );

                if( $d[ 'lstatus' ] == 1 && $d[ 'lcheck_in' ] > time() )
                {
                    $this->template->set_var( 'ptitle', 'UPCOMING TRIP DETAILS - ID ' . $d[ 'lbooking_code' ] );                
                    $this->template->set_var( 'action_class', 'hidden' );
                }
                elseif( $d[ 'lstatus' ] == 0 )
                {
                    $this->template->set_var( 'ptitle', 'PENDING CONFIRMATION TRIP DETAILS - ID ' . $d[ 'lbooking_code' ] );                
                    $this->template->set_var( 'action_class', 'hidden' );
                }
                elseif( $d[ 'lstatus' ] == 1 && $d[ 'lstatus_payment' ] == 0 )
                {
                    $this->template->set_var( 'ptitle', 'PENDING PAYMENT TRIP DETAILS - ID ' . $d[ 'lbooking_code' ] );                
                    $this->template->set_var( 'action_class', 'hidden' );
                }
                elseif( $d[ 'lstatus' ] == 1 && $d[ 'lstatus_payment' ] == 1 && $d[ 'lcheck_out' ] < time() )
                {  
                    $this->template->set_var( 'ptitle', 'COMPLETED TRIP DETAILS - ID ' . $d[ 'lbooking_code' ] );
                    $this->template->set_var( 'action_class', '' );

                    $n = $this->global->getRowsNumber( 'lumonata_reviews', array( 
                        'luser_id'    => $this->sess[ 'userid' ],
                        'lbooking_id' => $bid
                    ));

                    if( $n > 0 )
                    {
                        $this->template->set_var( 'action_text', 'VIEW YOUR REVIEW' );
                    }
                    else
                    {
                        $this->template->set_var( 'action_text', 'WRITE A REVIEW' );
                    }
                }

                if( $d[ 'lrooms' ] != '' )
                {
                    $list = array();

                    foreach( $d[ 'lrooms' ] as $dt )
                    {
                        $list[] = sprintf( '%s %s<span>%s</span>', $dt[ 'num' ], $dt[ 'name' ], $this->global->get_format_price( $dt[ 'total' ], $d[ 'lbooking_id' ] ) );
                    }

                    $this->template->set_var( 'rooms', implode( '<br/>', $list ) );
                }
                else
                {
                    $this->template->set_var( 'rooms', '-' );
                }

                if( $d[ 'laddons' ] != '' )
                {
                    $list = array();

                    foreach( $d[ 'laddons' ] as $dt )
                    {
                        $list[] = sprintf( '%s %s<span>%s</span>', $dt[ 'num' ], $dt[ 'name' ], $this->global->get_format_price( $dt[ 'total' ], $d[ 'lbooking_id' ] ) );
                    }

                    $this->template->set_var( 'addons', implode( '<br/>', $list ) );
                }
                else
                {
                    $this->template->set_var( 'addons', '-' );
                    $this->template->set_var( 'addons_price', '' );
                }

                $this->template->set_var( 'bid', $d[ 'lbooking_id' ] );
                $this->template->set_var( 'note', empty( $d[ 'lnote' ] ) ? '-' : nl2br( $d[ 'lnote' ] ) ) ;
                $this->template->set_var( 'guest', sprintf( '%s %s', $d[ 'ltitle' ], $d[ 'lfullname' ] ) );

                $this->template->set_var( 'listing_name', $this->global->getListingTitle( $d[ 'lposts_data' ] ) );
                $this->template->set_var( 'listing_location', $this->global->getLocations( $d[ 'lposts_data' ] ) );

                $this->template->set_var( 'check_in', date( 'M d, Y', $d[ 'lcheck_in' ] ) );
                $this->template->set_var( 'check_out', date( 'M d, Y', $d[ 'lcheck_out' ] ) );

                $this->template->set_var( 'total', $this->global->get_format_price( $d[ 'ltotal' ], $d[ 'lbooking_id' ] ) );
                $this->template->set_var( 'deposit', $this->global->get_format_price( $d[ 'ldeposit' ], $d[ 'lbooking_id' ] ) );
                $this->template->set_var( 'balance', $this->global->get_format_price( $d[ 'lbalance' ], $d[ 'lbooking_id' ] ) );
                $this->template->set_var( 'guest_num', $this->global->get_format_guest_num( $d[ 'ladult' ], $d[ 'lchild' ], $d[ 'linfant' ] ) );

                $this->template->set_var( 'review_url', HT_SERVER . SITE_URL . '/member/trip-review/' . $d[ 'lbooking_id' ] );

                $this->actions->add_actions( 'jvs', $this->global->get_jvs( HT_SERVER . MODULES_THEME_URL . '/apps/trips/apps.js' ) );

                $this->template->Parse( 'dC', 'detailContent', false );
            }
            else
            {
                $this->template->set_var( 'error', '<div class="error">' . NO_AUTHO . '</div>' );
            }
        
            $this->template->set_var( 'avatar', $this->global->get_user_avatar( $this->sess[ 'userid' ], 46, 46 ) );

            return $this->template->Parse( 'dB', 'detailBlock', false );
        }
    }

    function load()
    {
        //-- Query Limit
        $limit = parent::prepare_query( ' LIMIT ' . $this->post[ 'start' ] . ', ' . $this->post[ 'length' ] );

        //-- Query Order By
        $order = parent::prepare_query( ' ORDER BY a.lbooking_id DESC' );

        //-- Main Conditional
        $where = parent::prepare_query( ' WHERE a.lcreated_by = %s', $this->sess[ 'username' ] );

        //-- Query Additional Where
        $h = array();

        if( $this->post[ 'lfilter' ] == 'uc' )
        {
            $h[] = parent::prepare_query( 'a.lstatus = %d AND a.lcheck_in > %d', 1, time() );
        }
        elseif( $this->post[ 'lfilter' ] == 'pc' )
        {
            $h[] = parent::prepare_query( 'a.lstatus = %d', 0 );
        }
        elseif( $this->post[ 'lfilter' ] == 'pp' )
        {
            $h[] = parent::prepare_query( 'a.lstatus = %d AND a.lstatus_payment = %d', 1, 0 );
        }
        elseif( $this->post[ 'lfilter' ] == 'cp' )
        {
            $h[] = parent::prepare_query( 'a.lstatus = %d AND a.lstatus_payment = %d AND a.lcheck_out < %d', 1, 1, time() );
        }

        if( empty( $h ) === false )
        {
            $having = ' HAVING ' . implode( ' AND ', $h );
        }
        else
        {
            $having = '';
        }

        //-- Main Query
        $qm = 'SELECT
                a.lbooking_id,
                a.lstatus_payment,
                a.lstatus,
                a.lcheck_in,
                a.lcheck_out,
                a.lpost_id,
                a.lagent,
                a.ltotal
              FROM lumonata_booking AS a 
              LEFT JOIN lumonata_post AS b ON a.lpost_id = b.lpost_id' . $where . $having . $order;
        $rm = parent::query( $qm );
        $nm = parent::num_rows( $rm );

        $qf = $qm . $limit;
        $rf = parent::query( $qf );
        $nf = parent::num_rows( $rf );

        $data = array();

        if( $nf > 0 )
        {
            while( $df = parent::fetch_array( $rf ) )
            {
                $user_data = $this->global->getFields( 'lumonata_user', array( 'luser_id', 'lname', 'lphone' ), array( 'lusername' => $df[ 'lagent' ] ) );
                $tripdate  = $this->global->get_format_date_range( $df[ 'lcheck_in' ], $df[ 'lcheck_out' ] );
                $post_data = $this->global->getPosts( array( 'lpost_id' => $df[ 'lpost_id' ] ) );

                if( $df[ 'lstatus' ] == 1 && $df[ 'lcheck_in' ] > time() )
                {
                    $tripstatus = 'UPCOMING';
                }
                elseif( $df[ 'lstatus' ] == 0 )
                {
                    $tripstatus = 'PENDING CONFIRMATION';
                }
                elseif( $df[ 'lstatus' ] == 1 && $df[ 'lstatus_payment' ] == 0 )
                {
                    $tripstatus = 'PENDING PAYMENT';
                }
                elseif( $df[ 'lstatus' ] == 1 && $df[ 'lstatus_payment' ] == 1 && $df[ 'lcheck_out' ] < time() )
                {
                    $tripstatus = 'COMPLETED';
                }

                if( empty( $post_data[ 'accommodation_image' ] ) )
                {
                    $gallery = json_decode( $post_data[ 'accommodation_gallery' ], true );

                    if( is_array( $gallery ) && !empty( $gallery ) )
                    {
                        $thumbnail = reset( $gallery );
                    }
                    else
                    {
                        $thumbnail = null;
                    }
                }
                else
                {
                    $thumbnail = $post_data[ 'accommodation_image' ];
                }

                $data[] = array(
                    'laccommlink' => HT_SERVER . SITE_URL . '/accommodation/' . $post_data[ 'lsef_url' ] . '/',
                    'ldetaillink' => HT_SERVER . SITE_URL . '/member/trip-details/' . $df[ 'lbooking_id' ],
                    'lagentimg'   => $this->global->get_user_avatar( $user_data[ 'luser_id' ], 46, 46 ),
                    'ltotal'      => $this->global->get_format_price( $df[ 'ltotal' ], $df[ 'lpost_id' ] ),
                    'lthumbnail'  => $this->global->get_attachment_url( $thumbnail, 79, 54 ),
                    'ltitle'      => $this->global->getListingTitle( $post_data ),
                    'lagentphone' => $user_data[ 'lphone' ],
                    'lhost'       => $user_data[ 'lname' ],
                    'lbooking_id' => $df[ 'lbooking_id' ],
                    'ltripstatus' => $tripstatus,
                    'ltripdate'   => $tripdate
                );
            }
        }

        return json_encode( array(
            'draw'            => intval( $this->post[ 'draw' ] ),
            'recordsFiltered' => intval( $nm ),
            'recordsTotal'    => intval( $nf ),
            'data'            => $data
        ));
    }

    function language()
    {
        $this->lang = $this->global->getSettingValue( 'llanguage' );
    }

    function param()
    {
        $this->notif   = $this->flash->render();
        $this->message = $this->flash->message( $this->notif );
        $this->sess    = $this->global->getCurrentSession();
        $this->post    = $_POST;
    }

    function request( $mod, $usertype )
    {
        if( isset( $this->post[ 'pKEY' ] ) )
        {
            if( $this->post[ 'pKEY' ] == 'load' )
            {
                echo $this->load();
            }
        }
    }
}