jQuery(document).ready(function(){
	// set max checkbox
	$('.maxSet').each(function(){
		var limit = $(this).attr('data-max'),
			name  = $(this).attr('data-name');

	   $(this).find('input').on('change', function(evt) {
		   // if($(this).siblings(':checked').length >= 1) {
		   //     // this.checked = false;
		   //     $(this).prop('checked', false);
		   // }

		   if ($('input[name='+name+']:checked').length > limit) {
		        $(this).prop('checked', false);
		        alert("allowed only "+limit);
		    }
		});
	});	

    $('.radioinput-method').on('change', function(){
        if( this.value == 0 ){
            $('.bank-detail').removeClass('hidden');
        }else{
            $('.bank-detail').addClass('hidden');
        }
    });

    init_select2();
	excol();
});

function excol(){
	if( $('.pmh_').length > 0 ){
		$('.ddnav.actv').css('max-height', $('.ddnav.actv ul').outerHeight()+'px');
	}
	$('.pmh_').click(function(){

		var th = $(this);
  		if (th.hasClass('actv')) {
	      th.removeClass('actv');
	      th.parent().find('.ddnav').css('max-height','0px');
	    }else{
	      $('.ddnav').css('max-height','0px');
	      $('.pmh_').removeClass('actv');

	      th.addClass('actv');
	      th.parent().find('.ddnav').css('max-height', th.parent().find('.ddnav ul').outerHeight()+'px');
	    }

	});
}

function init_tinymce( selector, options )
{
    if( typeof selector == 'undefined' )
    {
        selector = '.tinymce';
    }

    if( typeof options == 'undefined' )
    {
        if( window.location.pathname.search( 'agent' ) > 0  )
        {
            content_css = [ '../../../lumonata-admin/templates/assets/tinymce.css' ];
        }
        else
        {
            content_css = [ 'templates/assets/tinymce.css' ];
        }

        tinymce.init({
            remove_script_host : false,
            content_css: content_css,
            automatic_uploads: true, 
            relative_urls : false,
            convert_urls : true,
            selector: selector,
            image_advtab: true,
            image_title: true,
            height: 300,
            plugins: [
                'advlist autolink lists image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools toc'
            ],
            templates: [
                {
                    title: 'What\'s Include List',
                    description: 'Adds a 3 box colomn with title and list',
                    content: '<div class="obig-include"><div><h4>Title 1</h4><ul><li>Item 1</li></ul></div><div><h4>Title 2</h4><ul><li>Item 2</li></ul></div><div><h4>Title 3</h4><ul><li>Item 3</li></ul></div></div>'
                },
                {
                    title: 'Food/Meals Include List',
                    description: 'Adds a 2 box colomn with title and list',
                    content: '<div class="obig-meals"><div><label>Title 1</label><ul><li>Item 1</li></ul></div><div><label>Title 2</label><ul><li>Item 2</li></ul></div>'
                }
            ]
        });
    }
    else
    {
        options.selector = selector;
        
        tinymce.init( options );
    }
}

function init_number( selector, types )
{
    if( typeof selector == 'undefined' )
    {
        selector = '.text-number';
    }

    if( jQuery( selector ).length > 0 )
    {
        var opt = Array();
        var dom = Array();

        jQuery( selector ).each( function( e ){
            var data = jQuery(this).data();
            var prm  = new Object();
               	prm.roundingMethod   = AutoNumeric.options.roundingMethod.halfUpSymmetric;
                prm.unformatOnSubmit = true;
                
            if( typeof data.aSep != 'undefined' )
            {
                prm.digitGroupSeparator = data.aSep;
            }
                
            if( typeof data.aDec != 'undefined' )
            {
                prm.decimalCharacter = data.aDec;
            }
                
            if( typeof data.mDec != 'undefined' )
            {
                prm.decimalPlaces = data.mDec;
            }

            if( typeof data.aMin != 'undefined' )
            {
                prm.minimumValue = data.aMin;
            }

            if( typeof data.aMax != 'undefined' )
            {
                prm.maximumValue = data.aMax;
            }

            if( typeof data.aSign != 'undefined' )
            {
                prm.currencySymbol = data.pSign;
            }
            else
            {
                prm.currencySymbol = '';
            }

        	if( typeof data.aPos != 'undefined' )
            {
            	prm.currencySymbolPlacement = data.aPos;
            }

            dom.push( this );
            opt.push( prm );
        });
        
        new AutoNumeric.multiple( dom, opt );
    }
}

function init_inc()
{
	jQuery('.min_').unbind();
	jQuery('.min_').on('click', function(){
		var prt = jQuery(this).parent();
		var val = parseInt( prt.find('input').val() ) || 1;
		var min = val - 1;

		if( min < 1 )
		{
			prt.find('input').val(1);
			prt.find('span').html('<b>1</b> room');
		}
		else
		{
			prt.find('input').val(min);
			prt.find('span').html('<b>' + min + '</b> ' + ( min > 1 ? 'rooms' : 'room' ));
		}
	});

	jQuery('.plu_').unbind();
	jQuery('.plu_').on('click', function(){
		var prt = jQuery(this).parent();
		var val = parseInt( prt.find('input').val() ) || 1;
		var max = val + 1;

		prt.find('input').val(max);
		prt.find('span').html('<b>' + max + '</b> ' + ( max > 1 ? 'rooms' : 'room' ));
	});
}

function delete_file( sel )
{
    sel.find('.delimg').unbind('click');
    sel.find('.delimg').on('click', function(){
        var id = jQuery(this).attr('data-id');

        if( typeof id != 'undefined'  )
        {
            var prt = jQuery(this).parent().parent();
            var url = jQuery('#ajax_url').val();
            var prm = new Object;
                prm.pKEY       = 'delete_file';
                prm.lattach_id = id;

            jQuery.ajax({
                url: url,
                data: prm,
                type: 'POST',
                dataType : 'json',
                success: function(e){
                    if( e.result == 'success' )
                    {
                    	if( prt.hasClass('eachroomupload') )
                        {
                        	prt.find('.addroomimage').css('background-image', 'url(' + sel.find('input[type=file]').data('src') + ')');
                        	prt.removeClass('eachroomuploadwithimage');
                        	sel.find('.delimg').attr('data-id', '');
                        	sel.find('.rimage').val( '' );
                        }
                        else if( prt.hasClass('eachaddonsupload') )
                        {
                        	prt.find('.addaddonsimage').css('background-image', 'url(' + sel.find('input[type=file]').data('src') + ')');
                        	prt.removeClass('eachaddonsuploadwithimage');
                        	sel.find('.delimg').attr('data-id', '');
                        	sel.find('.rimage').val( '' );
                        }
                        else if( prt.hasClass('eachactivityupload') )
                        {
                        	prt.find('.addactivityimage').css('background-image', 'url(' + sel.find('input[type=file]').data('src') + ')');
                        	prt.removeClass('eachactivityuploadwithimage');
                        	sel.find('.delimg').attr('data-id', '');
                        	sel.find('.rimage').val( '' );
                        }
                        else
                        {
                        	prt.find('.cover').prop('src', sel.find('input[type=file]').data('src') );
                        	sel.find('.delimg').attr('data-id', '').addClass('hidden');
                        }
                    }
                    else
                    {
                        alert( e.message );
                    }
                },
                error: function(e){
                   alert( 'Something wrong on the request' );
                }
            })
        }
        else
        {
            alert( 'Something wrong on the request' );
        }
    });
}

function delete_drop_file( sel )
{
    sel.find('.delimg').unbind('click');
    sel.find('.delimg').on('click', function(){
        var id = jQuery(this).attr('data-id');

        if( typeof id == 'undefined'  )
        {
            jQuery(this).parent().fadeOut(80, function(){
                jQuery(this).remove();
            });
        }
        else
        {
            var url = jQuery('#ajax_url').val();
            var prt = jQuery(this).parent();
            var prm = new Object;
                prm.pKEY       = 'delete_file';
                prm.lattach_id = id;

            jQuery.ajax({
                url: url,
                data: prm,
                type: 'POST',
                dataType : 'json',
                success: function(e){
                    if( e.result == 'success' )
                    {
                        prt.fadeOut(80, function(){
                            jQuery(this).remove();
                        });
                    }
                    else
                    {
                        alert( e.message );
                    }
                },
                error: function(e){
                   alert( 'Something wrong on the request' );
                }
            })
        }
    });
}

function init_select2( selector )
{
    if( typeof selector == 'undefined' )
    {
        selector = '.sl2_';
    }

    if( jQuery( selector ).length > 0 )
    {
        jQuery( selector ).select2();
    }
}

function init_table_action( selector, table )
{
    jQuery(selector).closest('.dataTables_wrapper').find('.dataTables_paginate').toggle( table.page.info().pages > 1 );

    bach_checkbox( selector );
}

function check_list( data, parent )
{
    var selector = parent + ' input[type=checkbox]';

    if( data.checked )
    {
        jQuery(selector).each(function(){
            jQuery(this).prop('checked', true);
        });
    }
    else
    {
        jQuery(selector).each(function(){
            jQuery(this).prop('checked', false);
        });
    }
}

function bach_checkbox( selector )
{
    jQuery(selector).find( '.batch-checkbox-select' ).on( 'change', function(){
        var chk = jQuery(selector).find( '.batch-checkbox-select:checked' ).length;
        var len = jQuery(selector).find( '.batch-checkbox-select' ).length;

        if( chk == len )
        {
            jQuery(selector).find('.checkall').prop('checked', true);
        }
        else
        {
            jQuery(selector).find('.checkall').prop('checked', false);
        }
    });
}